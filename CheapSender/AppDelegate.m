//
//  AppDelegate.m
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "AppDelegate.h"
#import "CSGeneralConstants.h"
#import "UtilHelper.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import "DataModels.h"
#import "SingletonClass.h"
//#import <Stripe/Stripe.h>
#import "PushNotificationHandler.h"
@import Stripe;
#import <FBSDKCoreKit/FBSDKCoreKit.h>


@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:STRIPE_PUBLISHABLE_KEY];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
    
    [GMSServices provideAPIKey:GOOGLE_MAPS_API_KEY];
    [GMSPlacesClient provideAPIKey:GOOGLE_MAPS_API_KEY];
    
    [GIDSignIn sharedInstance].clientID = @"116433212886-h0k02qamdfedevta2e1bd359l35vd4iu.apps.googleusercontent.com";
    [GIDSignIn sharedInstance].delegate = self;
        
    _iosDeviceToken = [UtilHelper getValueFromNSUserDefaultForKey:@"token"];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.appController = [AppController new];
    [self.appController initWithWindow:self.window];
    [self.appController navigationController];
    
    //Gets the info for login screen, if it returns 1 or -1 then it will show login view screen
//    NSInteger shouldShowTutorialView = [[UtilHelper getValueFromNSUserDefaultForKey:SHOW_TUTORIAL_SCREEN] intValue];
    
    NSInteger userInfo = [[UtilHelper getValueFromNSUserDefaultForKey:LOGON_USER] intValue];
    
    //If app opens first time then, it will load tutorial screen
//    if(shouldShowTutorialView == 1 || shouldShowTutorialView == -1)
//    {
//        [self.appController showTutorialView];
//    }
//    else if (userInfo == -1 || userInfo == 0)
//    {
//        [self.appController showStartupView];
//        
//    } else {
//        
//        if (userInfo == 1) {
//            
//            [[AppDelegate sharedAppDelegate].appController loadTabbarController];
//            
//        } else if (userInfo == 2) {
//            
//            [[AppDelegate sharedAppDelegate].appController loadCourierTabbarController];
//        }
//    }
    
    
    //Changed Oct 7 as told by Anar
    if (userInfo == 1)
    {
        [[AppDelegate sharedAppDelegate].appController loadTabbarController];
    }
    else if (userInfo == 2)
    {
        [[AppDelegate sharedAppDelegate].appController loadCourierTabbarController];
    }
    else
    {
        [self.appController showSplashView];
    }
    
    //// Disable back gesture
    if ([self.appController.navController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.appController.navController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    //Push Notification Setup for iOS 10
    if(SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0"))
    {
        //Manages the notification-related activities
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error)
         {
             if( !error )
             {
//                 [[UIApplication sharedApplication] registerForRemoteNotifications];
             }
         }];
    }
    else
    {
        //Push Notification settings for iOS less than 10
//        [application registerForRemoteNotifications];
        if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
#ifdef __IPHONE_8_0
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeAlert
                                                                                                 | UIUserNotificationTypeBadge
                                                                                                 | UIUserNotificationTypeSound) categories:nil];
            //Registers your preferred options for notifying the user.
           //[application registerUserNotificationSettings:settings];
#endif
        }
    }
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = NO;
    
    if ([[url scheme] isEqualToString:@"senpex"]) {
                
        NSString *packUrl = [url absoluteString];
        
        NSArray *items = [packUrl componentsSeparatedByString:@"="];
        
        if(items.count > 1)
        {
            NSString *packId = items[1];
            
            NSInteger userLogin = [[UtilHelper getValueFromNSUserDefaultForKey:LOGON_USER] intValue];
            
            if (userLogin == 2) {
                
                if (![packId isEqualToString:@"0"]) {
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:COURIER_ACTIVE_LIST_UPDATED object:@{}];
                    
                    NSDictionary* userInfo = @{@"package_id": packId};
                    [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_ORDER_DETAILS
                                                                        object:@{}
                                                                      userInfo:userInfo];
                    
                    if ([AppDelegate sharedAppDelegate].isNavigateToOrderDetails == 1 && [AppDelegate sharedAppDelegate].isNeedToOrderDetails == 0) {
                        
                        [[AppDelegate sharedAppDelegate].appController showCourierSendingsDetail:packId];
                    }
                }
            }
        }
    }else{
       
        [[GIDSignIn sharedInstance] handleURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
        
        handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                      openURL:url
                                                            sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                                   annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    }
    
    // Add any custom logic here.
    return handled;
}

//Shared object of appDelegate
+ (AppDelegate *)sharedAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window{
    
    if (self.shouldRotate)
        return UIInterfaceOrientationMaskAllButUpsideDown;
    else
        return UIInterfaceOrientationMaskPortrait;
}

#pragma mark--
#pragma mark -- PushNotification delegates methods

- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
//    [PushNotificationHandler GetPushNotificationInfo:notification.request.content.userInfo];
    completionHandler(UNNotificationPresentationOptionAlert);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)(void))completionHandler{
    [PushNotificationHandler GetPushNotificationInfo:response.notification.request.content.userInfo];
    completionHandler();
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    //    NSLog(@"Did Register for Remote Notifications with Device Token (%@)", deviceToken);
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    _iosDeviceToken = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    [UtilHelper saveAndUpdateNSUserDefaultWithObject:_iosDeviceToken forKey:@"token"];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    //    NSLog(@"Did Fail to Register for Remote Notifications");
    //    NSLog(@"%@, %@", error, error.localizedDescription);
    self.iosDeviceToken = [UtilHelper getValueFromNSUserDefaultForKey:@"token"];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [PushNotificationHandler GetPushNotificationInfo:userInfo];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    
    [AppDelegate sharedAppDelegate].isNavigateToChatDetails = 1;
    [AppDelegate sharedAppDelegate].isNavigateToOrderDetails = 1;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [FBSDKAppEvents activateApp];
    
    if ([UIApplication sharedApplication].applicationIconBadgeNumber > 0) {
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
    
    [AppDelegate sharedAppDelegate].isNavigateToChatDetails = 0;
    [AppDelegate sharedAppDelegate].isNavigateToOrderDetails = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.entermarkets.OTW" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CheapSender" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"OTW.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

//#pragma mark - Core Data stack
//
//@synthesize persistentContainer = _persistentContainer;
//
//- (NSPersistentContainer *)persistentContainer {
//    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
//    @synchronized (self) {
//        if (_persistentContainer == nil) {
//            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"CheapSender"];
//            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
//                if (error != nil) {
//                    // Replace this implementation with code to handle the error appropriately.
//                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//
//                    /*
//                     Typical reasons for an error here include:
//                     * The parent directory does not exist, cannot be created, or disallows writing.
//                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
//                     * The device is out of space.
//                     * The store could not be migrated to the current model version.
//                     Check the error message to determine what the actual problem was.
//                     */
//                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
//                    abort();
//                }
//            }];
//        }
//    }
//
//    return _persistentContainer;
//}
//
//#pragma mark - Core Data Saving support
//
//- (void)saveContext {
//    NSManagedObjectContext *context = self.persistentContainer.viewContext;
//    NSError *error = nil;
//    if ([context hasChanges] && ![context save:&error]) {
//        // Replace this implementation with code to handle the error appropriately.
//        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
//        abort();
//    }
//}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    NSLog(@"sdfsd");
}
- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
    NSLog(@"error");
}

@end
