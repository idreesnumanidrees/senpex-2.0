//
//  LoginViewController.m
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "LoginViewController.h"
#import "StartUpViewController.h"
#import <Stripe/Stripe.h>

@interface LoginViewController ()
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfEmail;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfPassword;

@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnGoogle;
@property (strong, nonatomic) IBOutlet UIButton *btnFacebook;
@property (strong, nonatomic) IBOutlet UIButton *btnSignUp;
@property (strong, nonatomic) NSArray *textFieldArray;
@property (strong, nonatomic) IBOutlet UIView *containerView;

@end

@implementation LoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [UIApplication.sharedApplication registerForRemoteNotifications];
    [self prepareUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tfEmail.text= EMPTY_STRING;
    self.tfPassword.text = EMPTY_STRING;
//    [self testUser];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI {
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;

    self.textFieldArray = @[_tfEmail,_tfPassword];
    
    if (IS_IPHONE_5) {
        
        self.btnLogin = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnLogin withRadius:22 color:UIColor.clearColor width:0];
        self.btnGoogle = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnGoogle withRadius:22 color:UIColor.clearColor width:0];
        self.btnFacebook = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnFacebook withRadius:22 color:UIColor.clearColor width:0];
        
    } else {
        
        self.btnLogin = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnLogin withRadius:_btnLogin.frame.size.height/2 color:UIColor.clearColor width:0];
        self.btnGoogle = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnGoogle withRadius:_btnGoogle.frame.size.height/2 color:UIColor.clearColor width:0];
        self.btnFacebook = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnFacebook withRadius:_btnFacebook.frame.size.height/2 color:UIColor.clearColor width:0];
    }
}

- (void)testUser {
    // For courier
//    self.tfEmail.text = @"senpex.courier@mail.com";
    // For sender
    self.tfEmail.text = @"mammedov.anar@gmail.com";
    self.tfPassword.text = @"123456789";
    
    _btnLogin.selected = NO;
    _btnLogin.backgroundColor = LOGIN_ACTIVE_COLOR;
}

//Tells the responder when one or more fingers touch down in a view or window.
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    //Notifies the receiver that it has been asked to relinquish its status as first responder in its window.
    [self.containerView endEditing:YES];
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
    BOOL isActive = NO;
    for (JVFloatLabeledTextField *tfText in self.textFieldArray) {        
        if (tfText.text.length > 0) {
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
        } else {
            isActive = NO;
            break;
        }
    }
    if (isActive) {
        _btnLogin.backgroundColor = LOGIN_ACTIVE_COLOR;
        _btnLogin.selected = NO;
    }else {
        _btnLogin.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnLogin.selected = YES;
    }
    return YES;
}

#pragma mark -
#pragma mark - IBActions

- (IBAction)signUpAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showStartupView];
}

- (IBAction)forgotPassword:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showResetPasswordView:NO];
}

- (IBAction)loginAction:(id)sender {
    
    [self.view endEditing:YES];
    if (_btnLogin.selected == NO)
        [self validation];
}
- (IBAction)fbLoginAction:(UIButton *)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorNative;
    [login logOut];
    [login logInWithReadPermissions:@[@"public_profile", @"email"]
                 fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    NSLog(@"Process error");
                                } else if (result.isCancelled){
                                    NSLog(@"Cancelled");
                                } else {
                                    NSLog(@"Logged in");
                                    
                                    [self requestToFBLoginUser: result.token.tokenString];
                                }
                            }];
}

- (IBAction)googleLoginAction:(UIButton *)sender {
    [GIDSignIn sharedInstance].delegate = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [[GIDSignIn sharedInstance] signIn];
}

- (IBAction)backToStartUp:(id)sender {
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    BOOL isStartUpViewFound = NO;
    
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[StartUpViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:YES];
            isStartUpViewFound = YES;
        }
    }
    
    if (!isStartUpViewFound) {
        StartUpViewController *startupView = [[AppDelegate sharedAppDelegate].appController.mainStoryBoard instantiateViewControllerWithIdentifier:@"StartUpViewController"];
        [[AppDelegate sharedAppDelegate].appController.navController pushViewController:startupView animated:NO];
    }
}

#pragma mark-- Google Delegate

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error{
    NSString *googleToken = user.authentication.idToken;
    
    //Added by idrees (Tarik i added this because app was crashing when i click on cancel button)
    if (googleToken != nil) {
        
        [self requestToGoogleLoginUser:googleToken];
    }
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)validation {
    if (![UtilHelper validateEmail:self.tfEmail.text])
        [UtilHelper showApplicationAlertWithMessage:@"Please enter valid email" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    else if (![UtilHelper isMatchPasswordLength:self.tfPassword.text])
        [UtilHelper showApplicationAlertWithMessage:@"Password should be more than 4 characters" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    else {
        if (![UtilHelper isNetworkAvailable]) {
            [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
        
        [self showProgressHud];
        [self requestToLoginUser];
    }
}

- (void)requestToLoginUser {
    [WebServicesClient LoginUser:self.tfEmail.text
                        password:self.tfPassword.text
                     LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                    LogUserAgent:[[UIDevice currentDevice] systemVersion]
                   LogDeviceType:LOG_DEVICE_TYPE
                      SessionKey:@""
               completionHandler:^(UserModel *user, NSError *error) {
                   
                   [self hideProgressHud];
                   if (!error) {
                       if(user.genConfig.sTRIPEPUBLICAPIKEY != nil)
                           [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:user.genConfig.sTRIPEPUBLICAPIKEY];
                       else {
                           [UtilHelper showApplicationAlertWithMessage:@"There is a glitch in the system. Please inform the Senpex team regarding this issue."
                                                          withDelegate:nil
                                                               withTag:0
                                                     otherButtonTitles:@[@"Ok"]];
                           return ;
                       }

                       [SingletonClass sharedInstance].user = user;
                       [AppDelegate sharedAppDelegate].user = user;
                       [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"Login User Name"];
                       [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"LoginEmail"];
                       [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"MakeFastOrder"];
                       [AppDelegate sharedAppDelegate].sessionKey = user.logSessionKey;
                       
                       [UtilHelper saveAndUpdateNSUserDefaultWithObject:user.logSessionKey forKey:SESSION_KEY];
                       
                       [UtilHelper saveAndUpdateNSUserDefaultWithObject:user.internalBaseClassIdentifier forKey:SENDER_ID];
                       
                       [CourierInfoDB insertAndupdateCourierWithId:user.internalBaseClassIdentifier
                                                              Name:user.name
                                                           SurName:user.surname
                                                             Email:user.email
                                                           Address:user.addressActual
                                                         ImageName:user.selfImg
                                                              Rate:user.userRate
                                                           Balance:@"100"
                                                          IsUpdate:NO];
                       
                       [SenderInfoDB insertAndupdateSenderWithId:user.internalBaseClassIdentifier
                                                            Name:user.name
                                                         SurName:user.surname
                                                           Email:user.email
                                                         Address:user.addressActual
                                                       ImageName:user.selfImg IsUpdate:NO];
                       
                       self.tfEmail.text = EMPTY_STRING;
                       self.tfPassword.text = EMPTY_STRING;
                       
                       if (user.userProfiles.count > 0) {
                           UserProfiles *userProfile = user.userProfiles[0];
                           
                           //USER IS SENDER AND COURIER
                           if ([userProfile.userType isEqualToString:@"1"]) {
                               //usertype = 1 means user is both sender and courier
                               [self saveCardNumber:user.userProfiles];
                               [[AppDelegate sharedAppDelegate].appController loadTabbarController];
                           }
                           //USER IS COURIER
                           else if ([userProfile.userType isEqualToString:@"3"]) {
                               //usertype = 3 means user is courier
                               
                               if([userProfile.approvedByAdmin isEqualToString:@"-1"]) {
                                   [AppDelegate sharedAppDelegate].userId = userProfile.userId;
                                   
                                   //Continue with registration since courier has not completed registration.
                                   int noOfStepsCompleted = [self getNoOfStepsCourierHasCompletedOnRegistration:userProfile.userId];
                               
                                   if(noOfStepsCompleted != 5)
                                       [self getAllDict];
                                   
                                   switch (noOfStepsCompleted) {
                                       case 1:
                                           [[AppDelegate sharedAppDelegate].appController showSignUpIDCardVC:NO];
                                           break;
                                       case 2:
                                            [[AppDelegate sharedAppDelegate].appController showTakePhotoView:NO];
                                           break;
                                       case 3:
                                           [[AppDelegate sharedAppDelegate].appController showTransportTypeVC:NO isComingFromVehicleView:NO];
                                           break;
                                       case 4:
                                          [[AppDelegate sharedAppDelegate].appController showInsuranceCardView:NO];
                                           break;
                                       case 5:
                                           [UtilHelper showApplicationAlertWithMessage:@"We have sent activation link to your email address."
                                                                          withDelegate:nil
                                                                               withTag:0
                                                                     otherButtonTitles:@[@"Ok"]];
                                           break;
                                       default:
                                           break;
                                   }
                               } else if([userProfile.approvedByAdmin isEqualToString:@"0"]) {
                                   //Please wait. Senpex team is reviewing
                                   
                                   UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thank you!" message:@"Your application is under review and a representative from Senpex will get back to you soon." delegate:nil
                                                                             cancelButtonTitle:nil otherButtonTitles:nil, nil];
                                       [alertView addButtonWithTitle:@"Ok"];
                                       [alertView show];

                                  // sent an email in background for email verification. Also show alert. “Please verify your email.”
                               } else if([userProfile.approvedByAdmin isEqualToString:@"1"]) {
                                   //Admin has approved the courier
                                   
                                   //Check if email is verified or not
                                   if ([user.isApproved isEqualToString:@"0"]) {
                                       // email not verified
                                       // “Please verify your email.”
                                       [UtilHelper showApplicationAlertWithMessage:@"We have sent activation link to your email address."
                                                                      withDelegate:nil
                                                                           withTag:0                                                                 otherButtonTitles:@[@"Ok"]];

                                   } else {
                                       //Email verified
                                       //Allow login
                                       [[AppDelegate sharedAppDelegate].appController loadCourierTabbarController];
                                       [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:2]         forKey:LOGON_USER];
                                   }
                               } else if([userProfile.approvedByAdmin isEqualToString:@"2"]) {
                                   // admin rejected this courier
                                   //show alert about this.
                                   
                                   [UtilHelper showApplicationAlertWithMessage:@"The Senpex Team has rejected your registration. Kindly contact the team to fix the issue."
                                                                  withDelegate:nil
                                                                       withTag:0
                                                             otherButtonTitles:@[@"Ok"]];
                               }
                           }
                           //USER IS SENDER
                           else if ([userProfile.userType isEqualToString:@"2"])
                           {
                               //usertype = 2 , means user is sender
                               if ([user.isApproved isEqualToString:@"0"]) {
                                   [UtilHelper showApplicationAlertWithMessage:@"We have sent activation link to your email address."
                                                                  withDelegate:nil
                                                                       withTag:0
                                                             otherButtonTitles:@[@"Ok"]];
                               } else {
                                   [[AppDelegate sharedAppDelegate].appController loadTabbarController];
                                   [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:1] forKey:LOGON_USER];
                                   [self saveCardNumber:user.userProfiles];
                                   
                                   //Update card number
//                                   CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
//                                   
//                                   if (cardInfo != nil) {
//                                       [CardInfoDB insertAndupdateCardInfoWithId:cardInfo.cardId
//                                                                      CardNumber:EMPTY_STRING
//                                                                     StripeToken:EMPTY_STRING
//                                                                        IsUpdate:YES];
//                                   }
                               }
                           }
                       } else {
                           
                           [UtilHelper showApplicationAlertWithMessage:@"User can not be loggedin. Please contact administrator" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                       }
                   } else {
                       if (error.code == 1) {
                           [UtilHelper showApplicationAlertWithMessage:@"Wrong username or password." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                       } else {
                           [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                       }
                   }
               }];
}

- (void)requestToFBLoginUser:(NSString *)fbtoken{
    [WebServicesClient FBLogin:fbtoken
                   LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                  LogUserAgent:[[UIDevice currentDevice] systemVersion]
                 LogDeviceType:LOG_DEVICE_TYPE
               completionHandler:^(UserModel *user, NSError *error) {
                   
                   [self hideProgressHud];
                   if (!error) {
                       if(user.genConfig.sTRIPEPUBLICAPIKEY != nil)
                           [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:user.genConfig.sTRIPEPUBLICAPIKEY];
                       else {
                           [UtilHelper showApplicationAlertWithMessage:@"There is a glitch in the system. Please inform the Senpex team regarding this issue."
                                                          withDelegate:nil
                                                               withTag:0
                                                     otherButtonTitles:@[@"Ok"]];
                           return ;
                       }
                       
                       [SingletonClass sharedInstance].user = user;
                       [AppDelegate sharedAppDelegate].user = user;
                       [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"Login User Name"];
                       [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"LoginEmail"];
                       [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"MakeFastOrder"];
                       [AppDelegate sharedAppDelegate].sessionKey = user.logSessionKey;
                       
                       [UtilHelper saveAndUpdateNSUserDefaultWithObject:user.logSessionKey forKey:SESSION_KEY];
                       
                       [UtilHelper saveAndUpdateNSUserDefaultWithObject:user.internalBaseClassIdentifier forKey:SENDER_ID];
                       
                       [CourierInfoDB insertAndupdateCourierWithId:user.internalBaseClassIdentifier
                                                              Name:user.name
                                                           SurName:user.surname
                                                             Email:user.email
                                                           Address:user.addressActual
                                                         ImageName:user.selfImg
                                                              Rate:user.userRate
                                                           Balance:@"100"
                                                          IsUpdate:NO];
                       
                       [SenderInfoDB insertAndupdateSenderWithId:user.internalBaseClassIdentifier
                                                            Name:user.name
                                                         SurName:user.surname
                                                           Email:user.email
                                                         Address:user.addressActual
                                                       ImageName:user.selfImg IsUpdate:NO];
                       
                       self.tfEmail.text = EMPTY_STRING;
                       self.tfPassword.text = EMPTY_STRING;
                       
                       if (user.userProfiles.count > 0) {
                           UserProfiles *userProfile = user.userProfiles[0];
                           
                           //USER IS SENDER AND COURIER
                           if ([userProfile.userType isEqualToString:@"1"]) {
                               //usertype = 1 means user is both sender and courier
                               [self saveCardNumber:user.userProfiles];
                               [[AppDelegate sharedAppDelegate].appController loadTabbarController];
                           }
                           //USER IS COURIER
                           else if ([userProfile.userType isEqualToString:@"3"]) {
                               //usertype = 3 means user is courier
                               
                               if([userProfile.approvedByAdmin isEqualToString:@"-1"]) {
                                   [AppDelegate sharedAppDelegate].userId = userProfile.userId;
                                   
                                   //Continue with registration since courier has not completed registration.
                                   int noOfStepsCompleted = [self getNoOfStepsCourierHasCompletedOnRegistration:userProfile.userId];
                                   
                                   if(noOfStepsCompleted != 5)
                                       [self getAllDict];
                                   
                                   switch (noOfStepsCompleted) {
                                       case 0:
                                           [[AppDelegate sharedAppDelegate].appController showSignUpCourierView];
                                           break;
                                       case 1:
                                           [[AppDelegate sharedAppDelegate].appController showSignUpIDCardVC:NO];
                                           break;
                                       case 2:
                                           [[AppDelegate sharedAppDelegate].appController showTakePhotoView:NO];
                                           break;
                                       case 3:
                                           [[AppDelegate sharedAppDelegate].appController showTransportTypeVC:NO isComingFromVehicleView:NO];
                                           break;
                                       case 4:
                                           [[AppDelegate sharedAppDelegate].appController showInsuranceCardView:NO];
                                           break;
                                       case 5:
                                           [UtilHelper showApplicationAlertWithMessage:@"We have sent activation link to your email address."
                                                                          withDelegate:nil
                                                                               withTag:0
                                                                     otherButtonTitles:@[@"Ok"]];
                                           
                                           break;
                                       default:
                                           break;
                                   }
                               } else if([userProfile.approvedByAdmin isEqualToString:@"0"]) {
                                   //Please wait. Senpex team is reviewing
                                   [UtilHelper showApplicationAlertWithMessage:@"The Senpex Team is reviewing the details you provided. Once we complete our review, we will inform you, and you will be able to accept orders."
                                                                  withDelegate:nil
                                                                       withTag:0
                                                             otherButtonTitles:@[@"Ok"]];
                                   
                                   // sent an email in background for email verification. Also show alert. “Please verify your email.”
                               } else if([userProfile.approvedByAdmin isEqualToString:@"1"]) {
                                   //Admin has approved the courier
                                   
                                   //Check if email is verified or not
                                   if ([user.isApproved isEqualToString:@"0"]) {
                                       // email not verified
                                       // “Please verify your email.”
                                       [UtilHelper showApplicationAlertWithMessage:@"We have sent an email to your inbox. Please verify your email."
                                                                      withDelegate:nil
                                                                           withTag:0                                                                 otherButtonTitles:@[@"Ok"]];
                                       
                                   } else {
                                       //Email verified
                                       //Allow login
                                       [[AppDelegate sharedAppDelegate].appController loadCourierTabbarController];
                                       [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:2]         forKey:LOGON_USER];
                                   }
                               } else if([userProfile.approvedByAdmin isEqualToString:@"2"]) {
                                   // admin rejected this courier
                                   //show alert about this.
                                   
                                   [UtilHelper showApplicationAlertWithMessage:@"The Senpex Team has rejected your registration. Kindly contact the team to fix the issue."
                                                                  withDelegate:nil
                                                                       withTag:0
                                                             otherButtonTitles:@[@"Ok"]];
                               }
                           }
                           //USER IS SENDER
                           else if ([userProfile.userType isEqualToString:@"2"])
                           {
                               //usertype = 2 , means user is sender
                               if ([user.isApproved isEqualToString:@"0"]) {
                                   [UtilHelper showApplicationAlertWithMessage:@"We have sent an email to your inbox. Please verify your email."
                                                                  withDelegate:nil
                                                                       withTag:0
                                                             otherButtonTitles:@[@"Ok"]];
                               } else {
                                   [[AppDelegate sharedAppDelegate].appController loadTabbarController];
                                   [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:1] forKey:LOGON_USER];
                                   [self saveCardNumber:user.userProfiles];
                                   
                                   //Update card number
                                   //                                   CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
                                   //
                                   //                                   if (cardInfo != nil) {
                                   //                                       [CardInfoDB insertAndupdateCardInfoWithId:cardInfo.cardId
                                   //                                                                      CardNumber:EMPTY_STRING
                                   //                                                                     StripeToken:EMPTY_STRING
                                   //                                                                        IsUpdate:YES];
                                   //                                   }
                               }
                           }
                       } else {
                           
                           [UtilHelper showApplicationAlertWithMessage:@"User can not be loggedin. Please contact administrator" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                       }
                   } else {
                       if (error.code == 1) {
                           [UtilHelper showApplicationAlertWithMessage:@"Wrong username or password." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                       } else {
                           [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                       }
                   }
               }];
}

- (void)requestToGoogleLoginUser:(NSString *)googletoken{
    [WebServicesClient GoogleLogin:googletoken
                   LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                  LogUserAgent:[[UIDevice currentDevice] systemVersion]
                 LogDeviceType:LOG_DEVICE_TYPE
             completionHandler:^(UserModel *user, NSError *error) {
                 
                 [self hideProgressHud];
                 if (!error) {
                     if(user.genConfig.sTRIPEPUBLICAPIKEY != nil)
                         [[STPPaymentConfiguration sharedConfiguration] setPublishableKey:user.genConfig.sTRIPEPUBLICAPIKEY];
                     else {
                         [UtilHelper showApplicationAlertWithMessage:@"There is a glitch in the system. Please inform the Senpex team regarding this issue."
                                                        withDelegate:nil
                                                             withTag:0
                                                   otherButtonTitles:@[@"Ok"]];
                         return ;
                     }
                     
                     [SingletonClass sharedInstance].user = user;
                     [AppDelegate sharedAppDelegate].user = user;
                     [[NSUserDefaults standardUserDefaults] setObject:user.name forKey:@"Login User Name"];
                     [[NSUserDefaults standardUserDefaults] setObject:user.email forKey:@"LoginEmail"];
                     [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"MakeFastOrder"];
                     [AppDelegate sharedAppDelegate].sessionKey = user.logSessionKey;
                     
                     [UtilHelper saveAndUpdateNSUserDefaultWithObject:user.logSessionKey forKey:SESSION_KEY];
                     
                     [UtilHelper saveAndUpdateNSUserDefaultWithObject:user.internalBaseClassIdentifier forKey:SENDER_ID];
                     
                     [CourierInfoDB insertAndupdateCourierWithId:user.internalBaseClassIdentifier
                                                            Name:user.name
                                                         SurName:user.surname
                                                           Email:user.email
                                                         Address:user.addressActual
                                                       ImageName:user.selfImg
                                                            Rate:user.userRate
                                                         Balance:@"100"
                                                        IsUpdate:NO];
                     
                     [SenderInfoDB insertAndupdateSenderWithId:user.internalBaseClassIdentifier
                                                          Name:user.name
                                                       SurName:user.surname
                                                         Email:user.email
                                                       Address:user.addressActual
                                                     ImageName:user.selfImg IsUpdate:NO];
                     
                     self.tfEmail.text = EMPTY_STRING;
                     self.tfPassword.text = EMPTY_STRING;
                     
                     if (user.userProfiles.count > 0) {
                         UserProfiles *userProfile = user.userProfiles[0];
                         
                         //USER IS SENDER AND COURIER
                         if ([userProfile.userType isEqualToString:@"1"]) {
                             //usertype = 1 means user is both sender and courier
                             [self saveCardNumber:user.userProfiles];
                             [[AppDelegate sharedAppDelegate].appController loadTabbarController];
                         }
                         //USER IS COURIER
                         else if ([userProfile.userType isEqualToString:@"3"]) {
                             //usertype = 3 means user is courier
                             
                             if([userProfile.approvedByAdmin isEqualToString:@"-1"]) {
                                 [AppDelegate sharedAppDelegate].userId = userProfile.userId;
                                 
                                 //Continue with registration since courier has not completed registration.
                                 int noOfStepsCompleted = [self getNoOfStepsCourierHasCompletedOnRegistration:userProfile.userId];
                                 
                                 if(noOfStepsCompleted != 5)
                                     [self getAllDict];
                                 
                                 switch (noOfStepsCompleted) {
                                     case 0:
                                         [[AppDelegate sharedAppDelegate].appController showSignUpCourierView];
                                         break;
                                     case 1:
                                         [[AppDelegate sharedAppDelegate].appController showSignUpIDCardVC:NO];
                                         break;
                                     case 2:
                                         [[AppDelegate sharedAppDelegate].appController showTakePhotoView:NO];
                                         break;
                                     case 3:
                                         [[AppDelegate sharedAppDelegate].appController showTransportTypeVC:NO isComingFromVehicleView:NO];
                                         break;
                                     case 4:
                                         [[AppDelegate sharedAppDelegate].appController showInsuranceCardView:NO];
                                         break;
                                     case 5:
                                         [UtilHelper showApplicationAlertWithMessage:@"We have sent an email to your inbox. Please verify your email."
                                                                        withDelegate:nil
                                                                             withTag:0
                                                                   otherButtonTitles:@[@"Ok"]];
                                         
                                         break;
                                     default:
                                         break;
                                 }
                             } else if([userProfile.approvedByAdmin isEqualToString:@"0"]) {
                                 //Please wait. Senpex team is reviewing
                                 [UtilHelper showApplicationAlertWithMessage:@"The Senpex Team is reviewing the details you provided. Once we complete our review, we will inform you, and you will be able to accept orders."
                                                                withDelegate:nil
                                                                     withTag:0
                                                           otherButtonTitles:@[@"Ok"]];
                                 
                                 // sent an email in background for email verification. Also show alert. “Please verify your email.”
                             } else if([userProfile.approvedByAdmin isEqualToString:@"1"]) {
                                 //Admin has approved the courier
                                 
                                 //Check if email is verified or not
                                 if ([user.isApproved isEqualToString:@"0"]) {
                                     // email not verified
                                     // “Please verify your email.”
                                     [UtilHelper showApplicationAlertWithMessage:@"We have sent activation link to your email address."
                                                                    withDelegate:nil
                                                                         withTag:0                                                                 otherButtonTitles:@[@"Ok"]];
                                     
                                 } else {
                                     //Email verified
                                     //Allow login
                                     [[AppDelegate sharedAppDelegate].appController loadCourierTabbarController];
                                     [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:2]         forKey:LOGON_USER];
                                 }
                             } else if([userProfile.approvedByAdmin isEqualToString:@"2"]) {
                                 // admin rejected this courier
                                 //show alert about this.
                                 
                                 [UtilHelper showApplicationAlertWithMessage:@"The Senpex Team has rejected your registration. Kindly contact the team to fix the issue."
                                                                withDelegate:nil
                                                                     withTag:0
                                                           otherButtonTitles:@[@"Ok"]];
                             }
                         }
                         //USER IS SENDER
                         else if ([userProfile.userType isEqualToString:@"2"])
                         {
                             //usertype = 2 , means user is sender
                             if ([user.isApproved isEqualToString:@"0"]) {
                                 [UtilHelper showApplicationAlertWithMessage:@"We have sent activation link to your email address."
                                                                withDelegate:nil
                                                                     withTag:0
                                                           otherButtonTitles:@[@"Ok"]];
                             } else {
                                 [[AppDelegate sharedAppDelegate].appController loadTabbarController];
                                 [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:1] forKey:LOGON_USER];
                                 [self saveCardNumber:user.userProfiles];
                                 
                                 //Update card number
                                 //                                   CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
                                 //
                                 //                                   if (cardInfo != nil) {
                                 //                                       [CardInfoDB insertAndupdateCardInfoWithId:cardInfo.cardId
                                 //                                                                      CardNumber:EMPTY_STRING
                                 //                                                                     StripeToken:EMPTY_STRING
                                 //                                                                        IsUpdate:YES];
                                 //                                   }
                             }
                         }
                     } else {
                         
                         [UtilHelper showApplicationAlertWithMessage:@"User can not be loggedin. Please contact administrator" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                     }
                 } else {
                     if (error.code == 1) {
                         [UtilHelper showApplicationAlertWithMessage:@"Wrong username or password." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                     } else {
                         [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                     }
                 }
             }];
}


- (void)saveCardNumber:(NSArray *)userProfile {
    //Check if the user has stripe token
    //if the user has stripe token then add card number to database
    UserProfiles *userProfileObj = userProfile[0];
    if(userProfileObj != nil) {
        NSString *strCustId = userProfileObj.stripeCustId;
        NSString *cardNumber = EMPTY_STRING;
        
        if([userProfileObj.cardLast4 isEqualToString:EMPTY_STRING] || userProfileObj.cardLast4 == nil) {
        //if([strCustId isEqualToString:EMPTY_STRING])
            cardNumber = EMPTY_STRING;
        } else {
            cardNumber = userProfileObj.cardLast4;
            cardNumber = [NSString stringWithFormat:@"****%@",cardNumber];
        }
        
        CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
        
        if (cardInfo != nil) {
            [CardInfoDB insertAndupdateCardInfoWithId:cardInfo.cardId
                                           CardNumber:cardNumber
                                          StripeToken:EMPTY_STRING
                                             IsUpdate:YES];
        } else {
            [CardInfoDB insertAndupdateCardInfoWithId:1
                                           CardNumber:cardNumber
                                          StripeToken:EMPTY_STRING
                                             IsUpdate:NO];
        }
    }
}
#pragma mark - GIDSIGN
// Implement these methods only if the GIDSignInUIDelegate is not a subclass of
// UIViewController.

// Stop the UIActivityIndicatorView animation that was started when the user
// pressed the Sign In button
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
//    [myActivityIndicator stopAnimating];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn
dismissViewController:(UIViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
