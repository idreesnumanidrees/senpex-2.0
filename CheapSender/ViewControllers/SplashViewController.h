//
//  SplashViewController.h
//  CheapSender
//
//  Created by Tarik on 3/26/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SplashViewController : BaseViewController <UIScrollViewDelegate>

@end
