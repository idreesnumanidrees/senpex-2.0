//
//  SenderSignUpConfirmVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/23.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SenderSignUpConfirmVC.h"
#import "LoginViewController.h"

@interface SenderSignUpConfirmVC ()
@property (weak, nonatomic) IBOutlet UIButton *btnGoToEmailApp;

@end

@implementation SenderSignUpConfirmVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI {
    self.btnGoToEmailApp = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnGoToEmailApp withRadius:_btnGoToEmailApp.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    //When registerSender webservice is called with parameter called "send_approve_mail" = "1"
    //then we dont need to call this service again.
    //If "send_approve_mail" = "0" then we can call this service
   // [self requestForSendApproveEmailToSender];
}

#pragma mark
#pragma mark --IBActions
- (IBAction)goToEmailAppAction:(id)sender {
    
}

- (IBAction)closeAction:(id)sender {
   
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    BOOL isLoginInStack = NO;
    
    for (UIViewController *aViewController in allViewControllers) {
        
        if ([aViewController isKindOfClass:[LoginViewController class]]) {
            
            isLoginInStack = YES;
            [self.navigationController popToViewController:aViewController animated:YES];
        }
    }
    
    if (!isLoginInStack) {
        
        [[AppDelegate sharedAppDelegate].appController showLoginView];
    }
}

- (void)requestForSendApproveEmailToSender
{
    [WebServicesClient SendApproveEmailToSender:[AppDelegate sharedAppDelegate].sessionKey
                                     LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                    LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                   LogDeviceType:LOG_DEVICE_TYPE
                               completionHandler:^(BOOL result, NSError *error) {
                                   
                                   if (!error) {
                                       
//                                       NSLog(@"Email sent on user eEmil");
                                   }
                               }];
}

@end
