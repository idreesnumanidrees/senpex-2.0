//
//  VerificationVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "VerificationVC.h"

@interface VerificationVC ()

@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@end

@implementation VerificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareUI];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    
    self.btnNext = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnNext withRadius:_btnNext.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
}


#pragma mark
#pragma mark --Actions

- (IBAction)signUpAction:(id)sender {
   [[AppDelegate sharedAppDelegate].appController showVerificationConfirmationnView];
}

#pragma mark
#pragma mark - TableView Data source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = 5;
    
    if (count > 0) {
        
        self.tableView.hidden = NO;
        self.emptyNotificationView.hidden = YES;
        
    } else {
        
        self.tableView.hidden = YES;
        self.emptyNotificationView.hidden = NO;
    }
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"VerificationCell";
    UITableViewCell *cell = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UILabel *typeLbl = (UILabel *)[cell viewWithTag:11];
    UILabel *statusLbl = (UILabel *)[cell viewWithTag:12];
    
    
    switch (indexPath.row) {
        case 0:
            typeLbl.text =   @"Sign up";
            statusLbl.text =   @"Competed";
            statusLbl.textColor = [UIColor colorWithRed:17.0f/255.0f green:167.0f/255.0f blue:170.0f/255.0f alpha:1];
            break;
        case 1:
            typeLbl.text =  @"Driver license";
            statusLbl.text =   @"Competed";
            statusLbl.textColor = [UIColor colorWithRed:17.0f/255.0f green:167.0f/255.0f blue:170.0f/255.0f alpha:1];
            break;
        case 2:
            typeLbl.text =  @"Your photo";

            statusLbl.text =   @"Not competed";
            statusLbl.textColor = [UIColor colorWithRed:156.0f/255.0f green:156.0f/255.0f blue:156.0f/255.0f alpha:1];
            break;
        case 3:
            typeLbl.text =  @"Transportation type";

            statusLbl.text =   @"Competed";
            statusLbl.textColor = [UIColor colorWithRed:17.0f/255.0f green:167.0f/255.0f blue:170.0f/255.0f alpha:1];
            break;
        case 4:
            typeLbl.text =  @"Insurance card";
            
            statusLbl.text =   @"Competed";
            statusLbl.textColor = [UIColor colorWithRed:17.0f/255.0f green:167.0f/255.0f blue:170.0f/255.0f alpha:1];
            break;
        default:
            typeLbl.text =  @"";

            statusLbl.text =   @"Delivered";
            statusLbl.textColor = [UIColor colorWithRed:24.0f/255.0f green:213.0f/255.0f blue:100.0f/255.0f alpha:1];
            break;
    }

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


@end
