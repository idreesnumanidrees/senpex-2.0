//
//  VehiclesVC.h
//  CheapSender
//
//  Created by Idrees on 2017/07/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface VehiclesVC : BaseViewController

@property (assign, nonatomic) BOOL isComingFromProfile;

@end
