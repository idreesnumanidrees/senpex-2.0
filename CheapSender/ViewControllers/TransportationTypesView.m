//
//  TransportationTypesView.m
//  CheapSender
//
//  Created by apple on 3/25/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import "TransportationTypesView.h"

@interface TransportationTypesView ()

@property (weak, nonatomic) IBOutlet UIButton *btnCar;
@property (weak, nonatomic) IBOutlet UIButton *btnVan;
@property (weak, nonatomic) IBOutlet UIButton *btnTruck;
@property (weak, nonatomic) IBOutlet UIButton *btnScooter;
@property (weak, nonatomic) IBOutlet UIButton *btnBike;
@property (weak, nonatomic) IBOutlet UIButton *btnWalk;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@property (assign, nonatomic) BOOL isWalkSelected;
@property (strong, nonatomic) NSString *vehicleId;
@property (strong, nonatomic) NSString *vehicleName;
@property (strong, nonatomic) NSString *vehicleImageName;
@property (weak, nonatomic) IBOutlet UIButton *laterButton;

@end

@implementation TransportationTypesView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareUI];
}

- (void)prepareUI
{
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    self.btnNext = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnNext withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    
    _vehicleName = EMPTY_STRING;
    _vehicleImageName = EMPTY_STRING;
    
    if (_isComingFromProfile) {
        
        _laterButton.hidden = YES;
        _headingTopConstraint.constant = 36;
        _stepLabel.hidden = YES;

    } else {
        
//        [self updateCountOfStepsCompletedByCourierInRegistration:@"3"];
    }
    
    //Request for get users transport
    if ([AppDelegate sharedAppDelegate].allDictData == nil) {
        
        [self getAllDict];
    }
}

//MARK: - IBActions

- (IBAction)vehiclesSelectionTapped:(UIButton *)sender {
    
    if (sender == _btnCar) {
        
        _vehicleName = @"Car";
        _vehicleImageName = @"car";
        _vehicleId = [self getVehicleId:@"Car"];
        _isWalkSelected = NO;
        
    } else if (sender == _btnVan) {
        
        _vehicleName = @"Van";
        _vehicleImageName = @"van";
        _vehicleId = [self getVehicleId:@"Van"];
        _isWalkSelected = NO;
        
    }else if (sender == _btnTruck) {
        
        _vehicleName = @"Truck";
        _vehicleImageName = @"truck";
        _vehicleId = [self getVehicleId:@"Truck"];
        _isWalkSelected = NO;
        
    }else if (sender == _btnScooter) {
        
        _vehicleName = @"Scooter";
        _vehicleImageName = @"scooter";
        _vehicleId = [self getVehicleId:@"Scooter"];
        _isWalkSelected = NO;
        
    }else if (sender == _btnBike) {
        
        //        _vehicleName = @"Bike";
        //        _vehicleImageName = @"motor-Cycle";
        
        if (_isComingFromVehicleView) {
            
            [UtilHelper showApplicationAlertWithMessage:@"Sorry! You can not select bike here" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
            return;
        }
        
        _vehicleId = [self getVehicleId:@"Byke"];
        _isWalkSelected = YES;
        
    }else if (sender == _btnWalk) {
        
        if (_isComingFromVehicleView) {
            
            [UtilHelper showApplicationAlertWithMessage:@"Sorry! You can not select Walking here" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
            return;
        }
        _vehicleId = [self getVehicleId:@"Walking"];
        _isWalkSelected = YES;
    }
    
    _btnCar.alpha = 0.3;_btnVan.alpha = 0.3;_btnTruck.alpha = 0.3;_btnScooter.alpha = 0.3;_btnBike.alpha = 0.3; _btnWalk.alpha = 0.3;
    sender.alpha = 1.0;
    _btnNext.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
    _btnNext.selected = NO;
}

- (NSString *)getVehicleId:(NSString *)vehicleN
{
    NSString *vehicleIdFromWeb = EMPTY_STRING;
    if ([AppDelegate sharedAppDelegate].allDictData != nil) {
        
        for (GetDictByName *getDictByName in [AppDelegate sharedAppDelegate].allDictData) {
            
            if ([getDictByName.dicName isEqualToString:@"Transport_types"]) {
               
                if ([getDictByName.listName isEqualToString:vehicleN]) {
                    
                    vehicleIdFromWeb = getDictByName.listId;
                    break;
                }
            }
        }
    }
    return vehicleIdFromWeb;
}

- (IBAction)btnNextTapped:(id)sender {
    
    if (_isWalkSelected) {
        
        [self requestForAddNewTransport];
    }
    else
    {
        if (_vehicleName == EMPTY_STRING) {
            
            [UtilHelper showApplicationAlertWithMessage:@"Please select the vehicle" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            
        } else {
            
            [AppDelegate sharedAppDelegate].vehicleId = _vehicleId;
            [AppDelegate sharedAppDelegate].vehicleName = _vehicleName;
            [AppDelegate sharedAppDelegate].vehicleImageName = _vehicleImageName;
            
            if (_isComingFromProfile) {
            [[AppDelegate sharedAppDelegate].appController showAddTransportView:YES Transport:nil];
            }else{
                [[AppDelegate sharedAppDelegate].appController showAddTransportView:NO Transport:nil];
            }
        }
    }
}

- (void)requestForAddNewTransport
{
    [self showProgressHud];
    [WebServicesClient AddNewTransport:_vehicleId
                         TransportYear:@""
                        TransportModel:@""
                         TransportMake:@""
                           PlateNumber:@""
                       IsUpdateVehicel:_isComingFromProfile
                           transportId:@""
                         LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                     completionHandler:^(NSString *insertedId, NSError *error) {
                         
                         [self hideProgressHud];

                         if (!error) {
     
                                 if (!_isComingFromProfile) {
                                     
                                     [[AppDelegate sharedAppDelegate].appController showInsuranceCardView:NO];
                                 }else{
                                     
                                     [UtilHelper showApplicationAlertWithMessage:@"Transport type added successfully!" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                                 }
                             
                         } else {
                             
                             [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                         }
                     }];
}

- (void)updateCountOfStepsCompletedByCourierInRegistration:(NSString *)stepCount
{
    //Registration of courier completed step1
    BOOL isCourierInfoAlreadyExistsInPhoneMemory = NO;
    
    //Check if this courier user exists or not in NSUSerDefaults
    NSMutableArray *arrayOfCouriers = [[UtilHelper getUserFromNSUserDefaultForKey:@"COURIER_USERS"] mutableCopy];
    
    
    //check if courierArray is nil. This happens when array is not created at all.
    if(arrayOfCouriers == nil)
    {
        NSLog(@"Userdefaults was empty and a new array created");
        arrayOfCouriers = [[NSMutableArray alloc]init];
    }
    else
    {
        //check if this user id has some value or not
        //If exists, then update information.
        for (NSMutableDictionary *dict in arrayOfCouriers)
        {
            if ([dict objectForKey:[AppDelegate sharedAppDelegate].userId] != nil)
            {
                NSLog(@"this user has already done %@ steps of reg", stepCount);
                
                NSMutableDictionary * dictionaryCopy = [dict mutableCopy];
                [dictionaryCopy setObject:stepCount
                                   forKey:[AppDelegate sharedAppDelegate].userId];
                
                [arrayOfCouriers replaceObjectAtIndex:[arrayOfCouriers indexOfObject:dict]
                                           withObject:dictionaryCopy];
                
                [UtilHelper saveAndUpdateNSUserDefaultWithObject:arrayOfCouriers
                                                          forKey:@"COURIER_USERS"];
                return;
                
            }
            
        }
    }
    //if not exists, then create new
    
    NSMutableDictionary *courierDict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:stepCount,[AppDelegate sharedAppDelegate].userId, nil];
    [arrayOfCouriers addObject:courierDict];
    [UtilHelper saveAndUpdateNSUserDefaultWithObject:arrayOfCouriers
                                              forKey:@"COURIER_USERS"];
    
    NSLog(@"This is a new user in registration. completed %@ steps of registration", stepCount);
    
}

- (int)getNoOfStepsCourierHasCompletedOnRegistration:(NSString *)userId
{
    int noOfStepsCompleted = 0;
    NSLog(@"checking if userid %@ exists in userdefaults", userId);
    
    
    
    NSMutableArray *arrayOfCouriers = [[UtilHelper getUserFromNSUserDefaultForKey:@"COURIER_USERS"] mutableCopy];
    
    
    //check if courierArray is nil. This happens when array is not created at all.
    if(arrayOfCouriers == nil)
    {
        noOfStepsCompleted = 0;
    }
    else
    {
        
        //check no of stpes this user has completed
        for (NSMutableDictionary *dict in arrayOfCouriers)
        {
            if ([dict objectForKey:userId] != nil)
            {
                NSLog(@"no of steps completed is %@", [dict valueForKey:userId]);
                
                return [[dict valueForKey:userId] intValue];
                
            }
        }
    }
    return noOfStepsCompleted;
}


@end
