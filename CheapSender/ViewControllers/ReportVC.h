//
//  ReportVC.h
//  Demo
//
//  Created by Rajesh on 04/05/17.
//  Copyright © 2017 Rajesh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ReportVC : BaseViewController

@property (strong, nonatomic) NSString *packId;

@end
