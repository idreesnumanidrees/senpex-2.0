//
//  TransportationTypesView.h
//  CheapSender
//
//  Created by apple on 3/25/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface TransportationTypesView : BaseViewController

@property (assign, nonatomic) BOOL isComingFromProfile;
@property (assign, nonatomic) BOOL isComingFromVehicleView;
@property (weak, nonatomic) IBOutlet UILabel *stepLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingTopConstraint;

@end
