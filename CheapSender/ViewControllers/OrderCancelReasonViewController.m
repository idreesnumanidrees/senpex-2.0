//
//  ViewController.m
//  Demo
//
//  Created by Rajesh on 03/05/17.
//  Copyright © 2017 Rajesh. All rights reserved.
//

#import "OrderCancelReasonViewController.h"

@interface OrderCancelReasonViewController () {
    NSUInteger selectedIndex;
}

@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UITableView *reasonTableView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (nonatomic, strong) NSArray *optionsArr;

@end

@implementation OrderCancelReasonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [_textView setTextContainerInset:UIEdgeInsetsMake(10, 18, 10, 18)];
    
    _sendBtn.clipsToBounds = YES;
    _sendBtn.layer.cornerRadius = _sendBtn.frame.size.height/2;
    
    _optionsArr = [NSArray arrayWithObjects:@"Didn't liked courier",@"Ordered by mistake",@"Waited for a long time",@"The courier asked", nil];
    
    selectedIndex = 2;
    
   // self.preferredStatusBarStyle = UIStatusBarStyleLightContent;
}

- (IBAction)sendBtnClicked:(id)sender {
}


#pragma mark - Table view data source


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_optionsArr count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReasonCell" forIndexPath:indexPath];
    UILabel *lbl = (UILabel *)[cell viewWithTag:11];
    UIImageView *checkImage = (UIImageView *)[cell viewWithTag:12];
    lbl.text = [_optionsArr objectAtIndex:indexPath.row];
    
    if (indexPath.row == selectedIndex) {
        checkImage.hidden = NO;
        lbl.highlighted = YES;
    } else {
        checkImage.hidden = YES;
        lbl.highlighted = NO;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectedIndex =  indexPath.row;
    [tableView reloadData];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
