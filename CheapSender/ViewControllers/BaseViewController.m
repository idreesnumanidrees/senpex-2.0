//
//  BaseViewController.m
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "MBProgressHUD.h"
#import "LoginViewController.h"
#import <MapKit/MapKit.h>

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSAttributedString *)setFontWithSize:(CGFloat)fontSize withWebFontText:(const char *)webFontString
{
    NSMutableDictionary *attributesDictionary = [NSMutableDictionary dictionary];
    [attributesDictionary setObject:[UIFont fontWithName:@"Material-Design-Icons" size:fontSize] forKey:NSFontAttributeName];
    
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithString:[NSString stringWithUTF8String:webFontString]
                                            attributes:attributesDictionary ];
    return attributedString;
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showProgressHud
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //NSLog(@"Show indicator");
}

- (void)hideProgressHud
{
    //dispatch_async(dispatch_get_main_queue(), ^{
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        //NSLog(@"hide indicator");
   // });
}

- (void)senderLogout
{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:STOP_TIMER_AND_LOCATION_UPDATOR object:nil];
    
    [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:0] forKey:LOGON_USER];
    [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:0] forKey:COURIER_AVAILABLE_STATUS];
    
    BOOL isLoginViewFound = NO;
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[LoginViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:YES];
            isLoginViewFound = YES;
        }
    }
    
    if (!isLoginViewFound) {
        
        LoginViewController *loginView = [[AppDelegate sharedAppDelegate].appController.mainStoryBoard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [[AppDelegate sharedAppDelegate].appController.navController pushViewController:loginView
                                                                               animated:NO];
    }
}

- (void)getAllDict
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetAllDics:[AppDelegate sharedAppDelegate].iosDeviceToken
                         LogUserAgent:[[UIDevice currentDevice] systemVersion]
                        LogDeviceType:LOG_DEVICE_TYPE
                           SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                    completionHandler:^(NSMutableArray *dictionaries, NSError *error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            if (!error) {
                                
                                [AppDelegate sharedAppDelegate].allDictData = dictionaries;
                            } else {
                                
                                if (error.code == 3) { } else {
                                    
                                    [self getAllDict];
                                }
                            }
                        });
                    }];
    });
}


- (void)updateCountOfStepsCompletedByCourierInRegistration:(NSString *)stepCount
{
    //Registration of courier completed step1
    BOOL isCourierInfoAlreadyExistsInPhoneMemory = NO;
    
    //Check if this courier user exists or not in NSUSerDefaults
    NSMutableArray *arrayOfCouriers = [[UtilHelper getUserFromNSUserDefaultForKey:@"COURIER_USERS"] mutableCopy];
    
    //check if courierArray is nil. This happens when array is not created at all.
    if(arrayOfCouriers == nil)
    {
        NSLog(@"Userdefaults was empty and a new array created");
        arrayOfCouriers = [[NSMutableArray alloc]init];
    }
    else
    {
        //check if this user id has some value or not
        //If exists, then update information.
        for (NSMutableDictionary *dict in arrayOfCouriers)
        {
            if ([dict objectForKey:[AppDelegate sharedAppDelegate].userId] != nil)
            {
                NSLog(@"this user has already done %@ steps of reg", stepCount);

                NSMutableDictionary * dictionaryCopy = [dict mutableCopy];
                [dictionaryCopy setObject:stepCount
                                   forKey:[AppDelegate sharedAppDelegate].userId];
                
                [arrayOfCouriers replaceObjectAtIndex:[arrayOfCouriers indexOfObject:dict]
                                           withObject:dictionaryCopy];
                
                [UtilHelper saveAndUpdateNSUserDefaultWithObject:arrayOfCouriers
                                                          forKey:@"COURIER_USERS"];
                return;
                
            }
            
        }
    }
    //if not exists, then create new
    
    NSMutableDictionary *courierDict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:stepCount,[AppDelegate sharedAppDelegate].userId, nil];
    [arrayOfCouriers addObject:courierDict];
    [UtilHelper saveAndUpdateNSUserDefaultWithObject:arrayOfCouriers
                                              forKey:@"COURIER_USERS"];
    
    NSLog(@"This is a new user in registration. completed %@ steps of registration", stepCount);

}

- (int)getNoOfStepsCourierHasCompletedOnRegistration:(NSString *)userId
{
    int noOfStepsCompleted = 0;
    NSLog(@"checking if userid %@ exists in userdefaults", userId);
    
    NSMutableArray *arrayOfCouriers = [[UtilHelper getUserFromNSUserDefaultForKey:@"COURIER_USERS"] mutableCopy];
    
    
    //check if courierArray is nil. This happens when array is not created at all.
    if(arrayOfCouriers == nil)
    {
        noOfStepsCompleted = 0;
    }
    else
    {
        
        //check no of stpes this user has completed
        for (NSMutableDictionary *dict in arrayOfCouriers)
        {
            if ([dict objectForKey:userId] != nil)
            {
                NSLog(@"no of steps completed is %@", [dict valueForKey:userId]);

                return [[dict valueForKey:userId] intValue];
                
            }
        }
    }
    return noOfStepsCompleted;
}

- (void)openMapsSheetWithLat:(float)lat andLng:(float)lng
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"" message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{}];
    }]];
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Navigate in Apple maps" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        
        CLLocationCoordinate2D coordinate =
        CLLocationCoordinate2DMake(lat, lng);
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
        
        // Distructive button tapped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Navigate in Google maps" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
//        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"comgooglemaps://"]]) {
//
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?center=\%f,%f&zoom=14&views=traffic&q=%f,%f", lat, lng, lat, lng]];
            [[UIApplication sharedApplication] openURL:url];
            
//        } else {
//            
//            [UtilHelper showApplicationAlertWithMessage:@"Please install the google maps app from apple store." withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
//        }
        // OK button tapped.
        [self dismissViewControllerAnimated:YES completion:^{}];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Navigate in Waze" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
//        if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"waze://"]]) {
//
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"https://waze.com/ul?ll=%f,%f&navigate=yes",lat,lng]];
            [[UIApplication sharedApplication] openURL:url];
            
//        } else {
//            [UtilHelper showApplicationAlertWithMessage:@"Please install the waze maps app from apple store." withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
//        }
        // OK button tapped.
        [self dismissViewControllerAnimated:YES completion:^{}];
    }]];
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)setStatusBarBackgroundColor:(UIColor *)color {
    
    UIView *statusBar = [[[UIApplication sharedApplication] valueForKey:@"statusBarWindow"] valueForKey:@"statusBar"];
    
    if ([statusBar respondsToSelector:@selector(setBackgroundColor:)]) {
        statusBar.backgroundColor = color;
    }
}

@end
