//
//  SignUpTakePhotoVC.h
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface SignUpTakePhotoVC : BaseViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIAlertViewDelegate>
{


}
@property (strong, nonatomic) UIDatePicker *picker;
@property (assign, nonatomic) BOOL isTimeTaken;
@property (assign, nonatomic) BOOL isComingFromProfile;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingTopConstaint;

@end
