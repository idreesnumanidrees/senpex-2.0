//
//  CourierFAQVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface CourierFAQVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *topView;
@property (weak, nonatomic) IBOutlet UIView *navigationView;

@property (nonatomic, retain) NSString *currentAppUserType;
@end
