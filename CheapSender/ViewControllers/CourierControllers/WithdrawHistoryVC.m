//
//  WithdrawHistoryVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "WithdrawHistoryVC.h"
#import "WithdrawHistoryTableCell.h"

@interface WithdrawHistoryVC () {
    
    float totalPirce;
    int deliveryCount;
}

@property (weak, nonatomic) IBOutlet UITableView *paymentTableView;
@property (strong, nonatomic) NSMutableArray *payoutHistory;
@property (weak, nonatomic) IBOutlet UIView *emptyHistoryView;
@property (weak, nonatomic) IBOutlet UILabel *totalAmoutLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalDeliveriesLabel;

@end

@implementation WithdrawHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.totalDeliveriesLabel.text = @"0 Deliveries";
    self.totalAmoutLabel.text = @"$0";
    
    totalPirce = 0;
    deliveryCount = 0;
    
    self.emptyHistoryView.hidden = YES;
    [self requestForPayoutHistory];
//    [self requestForEstimatedNextPayment];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- TableView Delegate and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_payoutHistory.count > 0) {
        
        self.paymentTableView.hidden = NO;
        self.emptyHistoryView.hidden = YES;
        
    } else {
        
        self.paymentTableView.hidden = YES;
        self.emptyHistoryView.hidden = NO;
    }
    
    return _payoutHistory.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WithdrawHistoryTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WithdrawHistoryTableCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    PayoutHistory *paymentHistory = _payoutHistory[indexPath.row];
    
    cell.price.text = [NSString stringWithFormat:@"$%@", paymentHistory.paymentAmount];
    
    NSDate *notiDate = [NSDate dateFromDateString:paymentHistory.payoutDate];
    NSString *stringDate = [UtilHelper time:notiDate];
    cell.date.text = stringDate;
    
   // cell.date.text = paymentHistory.payoutDate;
    
    cell.desription.hidden = YES;
        
    if ([paymentHistory.isPaid isEqualToString:@"1"]) {
        
        cell.progressStatus.text = @"Paid out";
        cell.progressStatus.textColor = [UIColor colorWithRed:24.0f/255.0f green:213.0f/255.0f blue:100.0f/255.0f alpha:1.0f];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PayoutHistory *paymentHistory = _payoutHistory[indexPath.row];
    [[AppDelegate sharedAppDelegate].appController showCourierPayoutDetails:paymentHistory.internalBaseClassIdentifier];
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestForPayoutHistory
{
    [self showProgressHud];
    [WebServicesClient GetMyPayouts:[AppDelegate sharedAppDelegate].iosDeviceToken
                        LogUserAgent:[[UIDevice currentDevice] systemVersion]
                       LogDeviceType:LOG_DEVICE_TYPE
                          SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                   completionHandler:^(NSMutableArray *payoutHistory,NSString *totalAmount,NSString* deliveryCount, NSError *error) {
                       
                       [self hideProgressHud];
                       
                       if (!error) {
                           
                           self->_payoutHistory = payoutHistory;
                           
                           if(self->_payoutHistory.count > 0) {
                               
                               self.totalDeliveriesLabel.text = [NSString stringWithFormat:@"%@ Deliveries", deliveryCount];
                               
                               float amount = [totalAmount floatValue];
                               self.totalAmoutLabel.text = [NSString stringWithFormat:@"$%.02f", amount];;
                               
//                               for (PayoutHistory *paymentHistory in self->_payoutHistory) {
//
//                                   if (paymentHistory.paymentAmount != nil) {
//
//                                       self->totalPirce += [paymentHistory.paymentAmount floatValue];
//                                       self.totalAmoutLabel.text = [NSString stringWithFormat:@"$%.02f", self->totalPirce];
//                                   }
//
//                                   if (paymentHistory.deliveryCount != nil) {
//
//                                       self->deliveryCount += [paymentHistory.deliveryCount floatValue];
//                                       self.totalDeliveriesLabel.text = [NSString stringWithFormat:@"%i Deliveries", self->deliveryCount];
//                                   }
//                               }
                           }
                           
                           [self->_paymentTableView reloadData];
                           
                       } else if (error.code == 3) {
                           
                           [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                           
                       } else {
                           
                           [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                       }
                   }];
    
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestForEstimatedNextPayment
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetEstimatedNextPayment:[AppDelegate sharedAppDelegate].iosDeviceToken
                                      LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                     LogDeviceType:LOG_DEVICE_TYPE
                                        SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                 completionHandler:^(NSDictionary *paymentDate, NSError *error) {
                                     
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         
                                         if (!error) {
                                             
                                             if (paymentDate != nil) {
                                                 
                                                 self.totalAmoutLabel.text = [NSString stringWithFormat:@"$%@", paymentDate[@"total_payment"]];
                                                 self.totalDeliveriesLabel.text = [NSString stringWithFormat:@"%@ Deliveries", paymentDate[@"total_count"]];
                                             }
                                             
                                         } else {
                                             
                                             if (error.code == 3) {
                                                 
                                                 [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again."
                                                                                withDelegate:self
                                                                                     withTag:1001 otherButtonTitles:@[@"Ok"]];
                                             }
                                             else {
                                                 
                                                 [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                                             }
                                         }
                                     });
                                 }];
    });
}


@end
