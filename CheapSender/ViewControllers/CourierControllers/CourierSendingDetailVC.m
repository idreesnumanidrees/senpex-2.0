//
//  CourierSendingDetailVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/14.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierSendingDetailVC.h"
#import "DeliveredOrderCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "LPPhotoViewer.h"

@interface CourierSendingDetailVC ()
{
    BOOL isDeliveredUp;
    BOOL isDeliveryTookImage;
    BOOL scrollDownOnConfirm;
}
@end

@implementation CourierSendingDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

#pragma mark --
#pragma mark -- Helpers

- (void)prepareUI
{
    self.topBarPackView = (UIView *)[UtilHelper makeImageRounded:(UIImageView *)self.topBarPackView];
    
    self.btnConfirmOrder = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnConfirmOrder withRadius:19.0 color:COMMON_ICONS_COLOR width:0];
    self.cancelCButton = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.cancelCButton withRadius:19.0 color:COMMON_ICONS_COLOR width:0];

    self.addImagesButton = (UIButton *)[UtilHelper makeBorderCornerCurved:(UIView *)self.addImagesButton withRadius:5.0 color:[UIColor colorWithRed:85.0/255.0 green:85.0/255.0 blue:85.0/255.0 alpha:1.0] width:1];
    
    self.deliveryAlphabetLabel = (UILabel *)[UtilHelper makeCornerCurved:self.deliveryAlphabetLabel withRadius:9.0 color:nil width:0];
    
    self.btnDelivery = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnDelivery withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    
    self.cancelDeliveryView = [UtilHelper makeCornerCurved:self.cancelDeliveryView withRadius:7.0 color:COMMON_ICONS_COLOR width:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    [_commentTextView setTextContainerInset:UIEdgeInsetsMake(10, 16, 10, 16)];
    
    _commentTextView.placeholder = @"   Add a comment";
    _lblComment.text = @"";
    
    _statusView.hidden = YES;
    _btnDelivery.selected = YES;
    _btnDelivery.alpha = 0.6;
    isDeliveryTookImage = NO;
    
    if ([AppDelegate sharedAppDelegate].allDictData.count > 0) {
        
        NSPredicate *senderRepResPred = [NSPredicate predicateWithFormat:@"dicShort contains[c] %@", @"c_report_reasons"];
        NSMutableArray *petTypes = [[[AppDelegate sharedAppDelegate].allDictData filteredArrayUsingPredicate:senderRepResPred] mutableCopy];
        _pickerOptionsList = petTypes;
    }
    
    self.selectedReasonIndex = 0;
    
    //Delivered order images
    _deliveredOrdersImages = [NSMutableArray new];
    
    [self requestForGetPackDetails];
    [self initializeMapView];
}

- (void)viewDidAppear:(BOOL)animated
{
    //Add observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshOrderDetails:)
                                                 name:REFRESH_ORDER_DETAILS
                                               object:nil];
    
    [AppDelegate sharedAppDelegate].isNeedToChatDetails = 1;
    [self setStatusBarBackgroundColor:[UIColor clearColor]];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [AppDelegate sharedAppDelegate].isNeedToChatDetails = 0;
}


//Initializing and allocating periodicWebCallManager class
- (void)initializeMapView
{
    //Assigning delegates
    self.mapView.delegate = self;
    [self.mapView initWithCustomStyle];
}

- (void)refreshOrderDetails:(NSNotification *) notification
{
    NSNumber* packageId = 0;
    if ([notification.name isEqualToString:REFRESH_ORDER_DETAILS])
    {
        NSDictionary* userInfo = notification.userInfo;
        packageId = (NSNumber*)userInfo[@"package_id"];
        NSLog (@"Successfully received test notification! %i", packageId.intValue);
    }
    
    //Refresh UI only when push message received for this particular package
    if(self.packDetails != nil &&
       packageId != 0 &&
       packageId.intValue == [self.packDetails.internalBaseClassIdentifier intValue])
    {
        //make webservicecall to refresh package only when pack status is not taken or when order is not confirmed.
        if([self.packDetails.packStatus isEqualToString:@"30"] ||
           [self.packDetails.packStatus isEqualToString:@"40"] ||
           [self.packDetails.packStatus isEqualToString:@"50"] ||
           [self.packDetails.packStatus isEqualToString:@"90"] ||
           [self.packDetails.packStatus isEqualToString:@"100"])
        {
            return;
        }
        else
        {
            [self requestForGetPackDetails];
        }
    }
}

- (void)textChanged:(NSNotification *)notification
{
    if([[_commentTextView text] length] == 0) {
        
        //Your feedback
        //        _lblComment.text = @"Add a comment";
        _btnDelivery.selected = YES;
        _btnDelivery.alpha = 0.6;
    }
    else
    {
        //        _lblComment.text = @"";
        _btnDelivery.selected = NO;
        _btnDelivery.alpha = 1;
    }
}

#pragma mark --
#pragma mark -- Actions

- (IBAction)openExternalMapsTapped:(id)sender {
    
    float lat = 0;
    float lng = 0;
    if ([_packDetails.packStatus isEqualToString:@"20"]) {
        
        lat = [_packDetails.packFromLat floatValue];
        lng = [_packDetails.packFromLng floatValue];
    } else {
        
        lat = [_packDetails.packToLat floatValue];
        lng = [_packDetails.packToLng floatValue];
    }
    [self openMapsSheetWithLat:lat andLng:lng];
}

- (IBAction)openExternalMapsFromTapped:(id)sender
{
    float lat = [_packDetails.packFromLat floatValue];
    float lng = [_packDetails.packFromLng floatValue];
    [self openMapsSheetWithLat:lat andLng:lng];
}

- (IBAction)openExternalMapsToTapped:(id)sender
{
    float lat = [_packDetails.packToLat floatValue];
    float lng = [_packDetails.packToLng floatValue];
    [self openMapsSheetWithLat:lat andLng:lng];
}

- (IBAction)confirmOrderAction:(id)sender
{
    NSString *message = @"Did you review all order detail?";
    
    if ([_packDetails.sendCatId isEqualToString:@"3"] || [_packDetails.sendCatId isEqual:@3]) {
        
        message = @"A driver has to pay for an item. He/she will be reimbursed for purchasing when Senpex will provide payment for delivery. Please confirm.";
    }
    
    [UtilHelper showApplicationAlertWithMessage:message
                                   withDelegate:self
                                        withTag:89
                              otherButtonTitles:@[@"No", @"Yes"]];
}

- (IBAction)viewOnMapAction:(id)sender {
    
    if ([_packDetails.packStatus isEqualToString:@"10"]||
        [_packDetails.packStatus isEqualToString:@"40"] ||
        [self.packDetails.packStatus isEqualToString:@"50"] ||
        [self.packDetails.packStatus isEqualToString:@"90"] ||
        [self.packDetails.packStatus isEqualToString:@"100"]) {
        
        [[AppDelegate sharedAppDelegate].appController showViewOnMap:@"ViewFromSender" PackDetails:_packDetails isComingFromCourierApp:YES];
    }
    else
    {
        [[AppDelegate sharedAppDelegate].appController showViewOnMap:@"track" PackDetails:_packDetails isComingFromCourierApp:YES];
    }
}

- (IBAction)chatAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showChatCourierDetails:nil PackDetails:_packDetails];
}

- (IBAction)senderProfileAction:(id)sender {
    
    if (_packDetails.packOwnerId != nil) {
        
        if (![_packDetails.packStatus isEqualToString:@"10"]) {
            
            [[AppDelegate sharedAppDelegate].appController showSenderProfileVC:_packDetails Conversation:nil isComingFromChat:NO];
        }
    }
}

- (IBAction)callCourierAction:(id)sender {
    
    NSString *cleanedString = [[_packDetails.packOwnerCell componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    
    NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEncodingWithAllowedCharacters:set];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [WebServicesClient AddCallHistory:self->_packDetails.acceptedCourierId
                                    ReciverId:self->_packDetails.packOwnerId
                                     CallDate:[NSString stringWithFormat:@"%@", [NSDate date]]
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                                   SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                            completionHandler:^(BOOL isCallAdded, NSError *error) {
                                
                            }];
        });
    }
}

- (IBAction)receiverCallAction:(id)sender {
    
    NSString *cleanedString = [[_packDetails.receiverPhoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    
    NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEncodingWithAllowedCharacters:set];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

- (IBAction)cancelCTapped:(id)sender {
    
    [self cancelCOrder];
}

//******************************************Cancel Order View***********************
- (IBAction)cancelDeliveryAction:(id)sender {
    
    self.cancelDeliveryView.hidden = NO;
    self.transparentView.hidden = NO;
    [self.containerView bringSubviewToFront:self.transparentView];
    [self.containerView bringSubviewToFront:self.cancelDeliveryView];
    self.cancelDeliveryViewBottomConstraint.constant = 0;
    [self animateOnViewUpdated];
}

- (IBAction)closeCancelDeliveryAction:(id)sender {
    
    self.cancelDeliveryViewBottomConstraint.constant = -360;
    [self animateOnViewUpdated];
    self.cancelDeliveryView.hidden = YES;
    self.transparentView.hidden = YES;
}

- (IBAction)cancelDeliveryRedAction:(id)sender {
    
    if (!_btnDelivery.selected) {
        
        [UtilHelper showApplicationAlertWithMessage:@"This cancellation with negatively affect your Senpex rating. Are you sure?"
                                       withDelegate:self
                                            withTag:88
                                  otherButtonTitles:@[@"No", @"Yes"]];
    }
}

- (IBAction)customerCancelledAction:(id)sender {
    
    [UtilHelper showApplicationAlertWithMessage:@"Are you sure that the package was not given?"
                                   withDelegate:self
                                        withTag:188
                              otherButtonTitles:@[@"No", @"Yes"]];
}

- (IBAction)cancelOrderOptionsAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select reason" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.delegate = self;
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
    alert.tag = 1;
    picker.delegate = self;
    [picker selectRow:self.selectedReasonIndex inComponent:0 animated:YES];
    [alert addSubview:picker];
    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
    [alert setValue:picker forKey:@"accessoryView"];
    [alert show];
}

- (IBAction)tookOrderAction:(id)sender {
    
    //    [UtilHelper showApplicationAlertWithMessage:@"Are you sure you have taken the package?"
    //                                   withDelegate:self
    //                                        withTag:901
    //                              otherButtonTitles:@[@"No", @"Yes"]];
}

- (IBAction)deliveredPack:(id)sender {
    
    if ([_packDetails.packStatus isEqualToString:@"20"]) {
        
        //        if (_deliveredOrdersImages.count > 0) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Make sure and double check again with sender you have picked up all items. Are you sure all items were taken?"
                                       withDelegate:self
                                            withTag:901
                                  otherButtonTitles:@[@"No", @"Yes"]];
        //        } else {
        //
        //            [UtilHelper showApplicationAlertWithMessage:@"Please take at least one picture before picked up." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        //        }
    }
    else if ([_packDetails.packStatus isEqualToString:@"30"]) {
        
        if ([_packDetails.packStatus isEqualToString:@"40"]) {
            return;
        }
        
        //        if (_deliveredOrdersImages.count > 1) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Are you sure you have delivered all items?"
                                       withDelegate:self
                                            withTag:902
                                  otherButtonTitles:@[@"No", @"Yes"]];
        //        } else {
        //
        //            [UtilHelper showApplicationAlertWithMessage:@"Please take at least one picture before delivery." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        //        }
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return self.pickerOptionsList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    GetDictByName *dictName = _pickerOptionsList[row];
    return dictName.listName;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.selectedReasonIndex = row;
    GetDictByName *dictName = _pickerOptionsList[row];
    _lblReason.text = dictName.listName;
    _lblReason.textColor = [UIColor blackColor];
}

#pragma mark UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {
        
        GetDictByName *dictName = _pickerOptionsList[self.selectedReasonIndex];
        _lblReason.text = dictName.listName;
        _lblReason.textColor = [UIColor blackColor];
        
    }
    else if (alertView.tag == 1001)
    {
        [self senderLogout];
    }
    else if (alertView.tag == 100)
    {
        [self backAction:nil];
    }
    else if (alertView.tag == 88)
    {
        if (buttonIndex == 1) {
            
            [self requestForCancelOrder];
        }
    }
    else if (alertView.tag == 89)
    {
        if (buttonIndex == 1) {
            
            [self requestForConfirmOrder];
        }
    }
    else if (alertView.tag == 188)
    {
        if (buttonIndex == 1) {
            
            [self requestForCustomerCancelled];
        }
    }
    else if (alertView.tag == 901)
    {
        if (buttonIndex == 1)
        {
            [self requestForTookPack];
        }
    }
    else if (alertView.tag == 902)
    {
        if (buttonIndex == 1)
        {
            [self requestForDelivered];
        }
    }
}

//****************************************** Delivered Order View ***********************

- (IBAction)addOrderPhotoAction:(id)sender {
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Media" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library",@"Take Photo",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    actionSheet.tag = 100;
    [actionSheet showInView:self.view];
}

#pragma
#pragma  mark action sheet delegate method

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if (actionSheet.tag == 100) {
        
        if(buttonIndex == 1) {
            
            //        NSLog(@"Take Photo Button Pressed");
            [self pickMediaFromSource:UIImagePickerControllerSourceTypeCamera];
        }
        else if(buttonIndex == 0)
        {
            [self pickMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
        }
    } else if (actionSheet.tag == 101) {
        
        
    }
}

#pragma
#pragma  mark camera delegate method
- (void)pickMediaFromSource:(UIImagePickerControllerSourceType)sourceType{
    @autoreleasepool {
        if([UIImagePickerController isSourceTypeAvailable:sourceType])
        {
            
            NSArray *mediaTypes=[UIImagePickerController availableMediaTypesForSourceType:sourceType];
            UIImagePickerController *picker=[[UIImagePickerController alloc]init];
            picker.mediaTypes=mediaTypes;
            picker.delegate=self;
            picker.allowsEditing=YES;
            //            picker.allowsEditing=NO;
            picker.sourceType=sourceType;
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            [UtilHelper showApplicationAlertWithMessage:@"Camera is not found" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
        }
    }
}

#pragma mark -
#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)])
    {
        //        NSLog(@"Image");
        //image
        @autoreleasepool {
            
            if ([_packDetails.packStatus isEqualToString:@"30"]) {
                isDeliveryTookImage = YES;
            }
            
            UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
            [self requestForDeliveryImage:chosenImage];
        }
    }
    else
    {
        //        NSLog(@"Video");
        //video
        [UtilHelper showApplicationAlertWithMessage:@"Video does not support." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

/***********************************************End Image Pager *************************/

- (void)populateFields:(PackDetailsModel *)packDetails {
    
    [self hideProgressHud];
    self.receiverViewHeightConstraint.constant = 50;
    self.receiverView.hidden= NO;
    
    _mainScrollView.frame = CGRectMake(0, 104, self.view.bounds.size.width, self.view.bounds.size.height-104);
    
    _sendingLabel.text = packDetails.sendName;
    _priceLabel.text = [NSString stringWithFormat:@"$%@", packDetails.courierProfitD];
    
    if (![packDetails.tips isEqualToString:EMPTY_STRING] && packDetails.tips != nil) {
        _tipLabel.text = [NSString stringWithFormat:@"Tip:$%@", packDetails.tips];
    }
    
    _fromAddressLabel.text = packDetails.packFromText;
    _toAddressLabel.text = packDetails.packToText;
    //    _sizeLabel.text = packDetails.packSizeText;
    //    _lbsLabel.text = packDetails.packWeight;
    
    //_packageStutusLabel.text = packDetails.statusCourier;
    _courierNameLabel.text = packDetails.receiverName;
    _packOwnerName.text = packDetails.packOwnerName;
    
    _payByCourierPriceLabel.text = [NSString stringWithFormat:@"$%@", packDetails.itemValue];
    _courierWaitTimeLabel.text = [NSString stringWithFormat:@"%@ mins", packDetails.waitingTime];
    
    _orderIdLabel.text = [NSString stringWithFormat:@"Order ID: %@", packDetails.internalBaseClassIdentifier];
    
    _milesLabel.text = [NSString stringWithFormat:@"Miles: %@", packDetails.distance];
    //    _itemValueLabel.text = [NSString stringWithFormat:@"$%@", packDetails.itemValue];
    
    _sizeLabel.text = packDetails.packSizeText;
    
    if (packDetails.tariffText != nil && ![packDetails.tariffText isEqualToString:@""] && ![packDetails.tariffText isEqualToString:@"<null>"]) {
        
        _deliveryTime.text = [NSString stringWithFormat:@"%@", packDetails.tariffText];
    } else {
        _deliveryTime.text = EMPTY_STRING;
    }
    
    self.packBoughtViewHeightConstraint.constant = 0;
    self.packValueViewHeightConstraint.constant = 0;
    
    if ([packDetails.sendCatId isEqualToString:@"2"] || [packDetails.sendCatId isEqual:@2]) {
        
        _topbarPackImage.image = [UIImage imageNamed:@"pack-icon"];
        self.packValueViewHeightConstraint.constant = 0;
        _containerHightConstraint.constant = 695;
        
    } else if ([packDetails.sendCatId isEqualToString:@"1"] || [packDetails.sendCatId isEqual:@1]) {
        
        _topbarPackImage.image = [UIImage imageNamed:@"ups-status"];
        self.packValueViewHeightConstraint.constant = 50;
        _containerHightConstraint.constant = 745;
        
    } else if ([packDetails.sendCatId isEqualToString:@"3"] || [packDetails.sendCatId isEqual:@3]) {
        
        _topbarPackImage.image = [UIImage imageNamed:@"Store-status"];
        self.packBoughtViewHeightConstraint.constant = 100;
        _containerHightConstraint.constant = 790;
        
    } else if ([packDetails.sendCatId isEqualToString:@"4"] || [packDetails.sendCatId isEqual:@4]) {
        
        _topbarPackImage.image = [UIImage imageNamed:@"return-services"];
        self.packValueViewHeightConstraint.constant = 0;
        _containerHightConstraint.constant = 695;
    }
    
    //Pack Status color
    if ([packDetails.packStatus isEqualToString:@"10"]) {
        
        //pending pack
        self.receiverView.hidden = YES;
        self.receiverViewHeightConstraint.constant = 0;
        
        //        _recieverCallButton.hidden = YES;
        //        _btnCall.hidden = YES;
        //        _btnChat.hidden = YES;
        _confirmView.hidden = NO;
        _statusView.hidden = YES;
        _containerHightConstraint.constant = 620;

        if ([packDetails.sendCatId isEqualToString:@"3"] || [packDetails.sendCatId isEqual:@3]) {
            
            _containerHightConstraint.constant = 720;
        }
        
        if (packDetails.packImages.count > 0) {
            
            _containerHightConstraint.constant = 690;
            
            if ([packDetails.sendCatId isEqualToString:@"3"] || [packDetails.sendCatId isEqual:@3]) {
                
                _containerHightConstraint.constant = 780;
            }
            
            _confirmButtonTopConstraint.constant = 170;
            _packImagesScrollView.hidden = NO;
            
            int imagesOffSet = 0;
            for (int i = 0; i < [packDetails.packImages count]; i++) {
                
                //We'll create an imageView object in every 'page' of our scrollView.
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(imagesOffSet,2,70, 70)];
                img.layer.masksToBounds = YES;
                img.layer.cornerRadius = 7.0f;
                [_packImagesScrollView addSubview:img];
                
                UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(imagesOffSet,2,70, 70)];
                [button addTarget:self action:@selector(showPackImages:) forControlEvents:UIControlEventTouchUpInside];
                button.tag = i;
                button.backgroundColor = [UIColor clearColor];
                [_packImagesScrollView addSubview:button];
                
                PackImages *imageObject = packDetails.packImages[i];
                
                [img setShowActivityIndicatorView:YES];
                [img setIndicatorStyle:UIActivityIndicatorViewStyleGray];
                NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, imageObject.packImg];
                [img sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
                imagesOffSet+=78;
            }
            //Set the content size of our scrollview according to the total width of our imageView objects.
            _packImagesScrollView.contentSize = CGSizeMake(imagesOffSet, 70);
        }
        
        if ([packDetails.sendCatId isEqualToString:@"1"] || [packDetails.sendCatId isEqual:@1]) {
            
            float packValue =  self.containerHightConstraint.constant;
            packValue = packValue + 50;
            self.containerHightConstraint.constant = packValue;
        }
    }
    else if ([packDetails.packStatus isEqualToString:@"20"]) {
        
        //Courier selected
        //        _btnCall.hidden = NO;
        //        _btnChat.hidden = NO;
        _statusView.hidden = NO;
        _confirmView.hidden = YES;
        
        _packageStutusLabel.text = packDetails.statusCourier;
        _pickupLocationLabel.text = @"Pick up location";
        
        _addressLabel.text = packDetails.packFromText;
        [_btnPickup setTitle:@"Item Picked Up" forState:UIControlStateNormal];
        [_btnNotGiven setTitle:@"Not Given" forState:UIControlStateNormal];
        
        if ([packDetails.sendCatId isEqualToString:@"3"] || [packDetails.sendCatId isEqual:@3]) {
            
            _topbarPackImage.image = [UIImage imageNamed:@"Store-status"];
            self.packBoughtViewHeightConstraint.constant = 100;
            _containerHightConstraint.constant = 825;
        }else {
            _containerHightConstraint.constant = 730;
        }
        
        if ([packDetails.sendCatId isEqualToString:@"1"] || [packDetails.sendCatId isEqual:@1]) {
            
            float packValue =  self.containerHightConstraint.constant;
            packValue = packValue + 50;
            self.containerHightConstraint.constant = packValue;
        }
        
        //Version(1.5)
        //        NSDate *deliveryDate = [NSDate dateFromDateString:packDetails.insertedDate];
        //        _deliveryTime.text = [deliveryDate getTimeFromDate];
        
        [_showMapButton setTitle:@"Live Tracking" forState:UIControlStateNormal];
    }
    else if ([packDetails.packStatus isEqualToString:@"30"]) {
        
        //30 pack taken
        //        _btnCall.hidden = NO;
        //        _btnChat.hidden = NO;
        _statusView.hidden = NO;
        _confirmView.hidden = YES;
        _btnNotGiven.hidden = YES;
        _cancelPackButton.hidden = YES;
        _dotedViewHeightConstraint.constant = 155.5;
        _deliveredTopConstraint.constant = 18;
        _addressLabel.text = packDetails.packToText;
        _packageStutusLabel.text = packDetails.statusCourier;
        _pickupLocationLabel.text = @"Delivery Location";
        _pickupLocationLabel.textColor = [UIColor colorWithRed:20.0/255.0 green:191.0/255.0 blue:255.0/255.0 alpha:1.0f];
        _deliveryAlphabetLabel.backgroundColor = [UIColor colorWithRed:20.0/255.0 green:191.0/255.0 blue:255.0/255.0 alpha:1.0f];
        _packageStutusLabel.textColor = [UIColor colorWithRed:20.0/255.0 green:191.0/255.0 blue:255.0/255.0 alpha:1.0f];
        _addressLabel.text = packDetails.packToText;
        _deliveryAlphabetLabel.text = @"B";
        
        _btnPickup.backgroundColor = [UIColor colorWithRed:20.0/255.0 green:191.0/255.0 blue:255.0/255.0 alpha:1.0f];
        [_btnPickup setTitle:@"Delivered" forState:UIControlStateNormal];
        [_showMapButton setTitle:@"Live Tracking" forState:UIControlStateNormal];
        
    }
    else if ([packDetails.packStatus isEqualToString:@"40"]) {

        //pack delivered
        _statusView.hidden = NO;
        _confirmView.hidden = YES;
        _cancelPackButton.hidden = YES;
        _btnNotGiven.hidden = YES;
        _dotedViewHeightConstraint.constant = 155.5;
        
        //About New version
        _addressLabel.text = packDetails.packToText;
        _packageStutusLabel.text = @"Delivered";
        _pickupLocationLabel.text = @"Delivery Location";
        _pickupLocationLabel.textColor = [UIColor colorWithRed:20.0/255.0 green:191.0/255.0 blue:255.0/255.0 alpha:1.0f];;
        _deliveryAlphabetLabel.backgroundColor = [UIColor colorWithRed:20.0/255.0 green:191.0/255.0 blue:255.0/255.0 alpha:1.0f];;
        _packageStutusLabel.textColor = [UIColor colorWithRed:20.0/255.0 green:191.0/255.0 blue:255.0/255.0 alpha:1.0f];
        _addressLabel.text = packDetails.packToText;
        _deliveryAlphabetLabel.text = @"B";
        
        _successfullyDeliveredLabel.hidden = NO;
        _successfullyDeliveredLabel.text = @"Item is Delivered";
        _successfullyDeliveredLabel.textColor = [UIColor colorWithRed:20.0/255.0 green:191.0/255.0 blue:255.0/255.0 alpha:1.0f];
        
        _btnPickup.hidden = YES;
        _btnNotGiven.hidden = YES;
        
        NSDate *dateInUTC = [NSDate dateFromDateString:_packDetails.lastOperationTime];
        NSString *dateInLocalTimeZone = [dateInUTC getDateTimeInLocalTimeZone];
        
        self.deliveryDateLabel.hidden = NO;
        self.deliveryDateLabel.text = [NSString stringWithFormat:@"at: %@", dateInLocalTimeZone];
        
        //Version(1.5)
        //        NSDate *deliveryDate = [NSDate dateFromDateString:packDetails.deliveryTime];
        //        _deliveryTime.text = [deliveryDate getTimeFromDate];
        [_showMapButton setTitle:@"Show on map" forState:UIControlStateNormal];
        
    }
    else if ([packDetails.packStatus isEqualToString:@"50"]) {
        
        //pack not given
        _statusView.hidden = YES;
        _confirmView.hidden = YES;
        _packNotDeliveredLabel.hidden = NO;
        _packNotDeliveredLabel.text = _packDetails.statusCourier;
        _packNotDeliveredLabel.textColor = ORANGE_LINE_COLOR;
        _containerHightConstraint.constant = 535;

        if ([packDetails.sendCatId isEqualToString:@"3"] || [packDetails.sendCatId isEqual:@3]) {
            
            _containerHightConstraint.constant = 635;
        }
        
        if ([packDetails.sendCatId isEqualToString:@"1"] || [packDetails.sendCatId isEqual:@1]) {
            
            float packValue =  self.containerHightConstraint.constant;
            packValue = packValue + 50;
            self.containerHightConstraint.constant = packValue;
        }
        
        [_showMapButton setTitle:@"Show on map" forState:UIControlStateNormal];

    }
    else if ([packDetails.packStatus isEqualToString:@"100"] || [packDetails.packStatus isEqualToString:@"90"]) {
        //100 cancelled pack
        // 90 reported courier
        _statusView.hidden = YES;
        _confirmView.hidden = YES;
        _packNotDeliveredLabel.hidden = NO;
        _packNotDeliveredLabel.text = _packDetails.statusCourier;
        _packNotDeliveredLabel.textColor = PACK_RED_STATUS_COLOR;
        _containerHightConstraint.constant = 535;
        if ([packDetails.sendCatId isEqualToString:@"3"] || [packDetails.sendCatId isEqual:@3]) {
            
            _containerHightConstraint.constant = 635;
        }
        
        if ([packDetails.sendCatId isEqualToString:@"1"] || [packDetails.sendCatId isEqual:@1]) {
            
            float packValue =  self.containerHightConstraint.constant;
            packValue = packValue + 50;
            self.containerHightConstraint.constant = packValue;
        }
        [_showMapButton setTitle:@"Show on map" forState:UIControlStateNormal];
    }
    
    float packValue =  self.containerHightConstraint.constant;
    packValue = packValue +_heightForDesc;
    self.containerHightConstraint.constant = packValue;
    
    if (!scrollDownOnConfirm) {
        
        if ([packDetails.descText isEqualToString:EMPTY_STRING] || packDetails.descText == nil) {
            _descHeightConstraint.constant = 0;
        }else{
            _descriptionLabel.text = packDetails.descText;
        }
        
        [self addReadMoreStringToUILabel:_descriptionLabel];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self scrollToBottom];
    });
    
    [self readMoreDidClickedGesture];
    [self addMarkers];
}

- (void)scrollToBottom
{
    if (_containerView.frame.size.height > _mainScrollView.bounds.size.height) {
        
        int statusBar = 0;
        if (IS_IPHONE_X) {
            statusBar = 34;
        }
        
        [self.mainScrollView setContentOffset:CGPointMake(0, 0) animated:NO];
        CGPoint bottomOffset = CGPointMake(0, _containerView.frame.size.height - (_mainScrollView.bounds.size.height-statusBar));
        [self.mainScrollView setContentOffset:bottomOffset animated:NO];
    }
}

- (void)addReadMoreStringToUILabel:(UILabel *)label
{
    scrollDownOnConfirm = YES;
    NSString *readMoreText = @" ...More";
    NSInteger lengthForString = label.text.length;
    
    if (lengthForString >= 80)
    {
        NSInteger lengthForVisibleString = [UtilHelper fitString:label.text intoLabel:label];
        NSMutableString *mutableString = [[NSMutableString alloc] initWithString:label.text];
        NSString *trimmedString = [mutableString stringByReplacingCharactersInRange:NSMakeRange(lengthForVisibleString, (label.text.length - lengthForVisibleString)) withString:@""];
        NSInteger readMoreLength = readMoreText.length;
        NSString *trimmedForReadMore = [trimmedString stringByReplacingCharactersInRange:NSMakeRange((trimmedString.length - readMoreLength), readMoreLength) withString:@""];
        NSMutableAttributedString *answerAttributed = [[NSMutableAttributedString alloc] initWithString:trimmedForReadMore attributes:@{ NSFontAttributeName : label.font}];
        
        NSMutableAttributedString *readMoreAttributed = [[NSMutableAttributedString alloc] initWithString:readMoreText
                                                                                               attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"SFUIText-Regular" size:14], NSForegroundColorAttributeName :ORANGE_COLOR}];
        [answerAttributed appendAttributedString:readMoreAttributed];
        label.attributedText = answerAttributed;
        
        UITapGestureRecognizer *readMoreGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(readMoreDidClickedGesture)];
        readMoreGesture.numberOfTapsRequired = 1;
        [label addGestureRecognizer:readMoreGesture];
        label.userInteractionEnabled = YES;
    }
    else {
        
        //        NSLog(@"No need for 'Read More'...");
    }
}

- (void)addReadLessStringToUILabel:(UILabel *)label
{
    NSString *readMoreText = @"";
    NSMutableString *mutableString = [[NSMutableString alloc] initWithString:label.text];
    NSMutableAttributedString *answerAttributed = [[NSMutableAttributedString alloc] initWithString:mutableString attributes:@{ NSFontAttributeName : label.font}];
    
    NSMutableAttributedString *readMoreAttributed = [[NSMutableAttributedString alloc] initWithString:readMoreText attributes:@{ NSFontAttributeName : [UIFont fontWithName:@"SFUIText-Regular" size:14], NSForegroundColorAttributeName :ORANGE_COLOR}];
    
    [answerAttributed appendAttributedString:readMoreAttributed];
    label.attributedText = answerAttributed;
    
    UITapGestureRecognizer *readMoreGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(readLessDidClickedGesture)];
    readMoreGesture.numberOfTapsRequired = 1;
    [label addGestureRecognizer:readMoreGesture];
    label.userInteractionEnabled = YES;
}

- (void)readMoreDidClickedGesture
{
    _descriptionLabel.text = _packDetails.descText;
    [self getHeightDescLabel];
    [self addReadLessStringToUILabel:_descriptionLabel];
}

- (void)getHeightDescLabel
{
    CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
    CGSize expectedLabelSize = [_packDetails.descText sizeWithFont:_descriptionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:_descriptionLabel.lineBreakMode];
    
    _heightForDesc = expectedLabelSize.height-33.5;
    _descHeightConstraint.constant = expectedLabelSize.height;
    _containerHightConstraint.constant += _heightForDesc;
    [self animateOnViewUpdated];
}

- (void)readLessDidClickedGesture
{
    _descriptionLabel.text = EMPTY_STRING;
    _descHeightConstraint.constant = 33.5;
    _containerHightConstraint.constant -= _heightForDesc;
    //    _deliveredViewTopConstraint.constant -= _heightForDesc;
    _heightForDesc = 0;
    [self animateOnViewUpdated];
    [self performSelector:@selector(showMoreText) withObject:nil afterDelay:0.1];
}

- (void)showMoreText {
    
    _descriptionLabel.text = _packDetails.descText;
    [self addReadMoreStringToUILabel:_descriptionLabel];
}

- (void)showPackImages:(UIButton *)sender
{
    if (_packDetails.packImages.count > 0) {
        
        NSMutableArray *imgArr = [NSMutableArray new];
        
        for (PackImages *addImage in _packDetails.packImages) {
            
            NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, addImage.packImg];
            [imgArr addObject:finalUrl];
        }
        
        LPPhotoViewer *pvc = [[LPPhotoViewer alloc] init];
        pvc.imgArr = imgArr;
        pvc.currentIndex = sender.tag;
        [self presentViewController:pvc animated:YES completion:nil];
    }
}

#pragma mark--
#pragma mark-- Server Side handling
- (void)requestForGetPackDetails
{
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    [self showProgressHud];
    [WebServicesClient GetPackDetails:_packId
                        IncludeImages:@"1"
                       IncludeBidders:@"1"
                                Start:@"0"
                                Count:@"0"
                        LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                          LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                         LogUserAgent:[[UIDevice currentDevice] systemVersion]
                        LogDeviceType:LOG_DEVICE_TYPE
                    completionHandler:^(PackDetailsModel *packDetails, NSError *error) {
                        
                        [self hideProgressHud];
                        
                        if (!error) {
                            
                            self.packDetails = packDetails;
                            self.deliveredOrdersImages = [NSMutableArray new];
                            [self requestForAllDeliveredImages];
                            [self populateFields:packDetails];
                            
                        } else if (error.code == 3) {
                            
                            [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                            
                        }else if (error.code == 101) {
                            
                            [UtilHelper showApplicationAlertWithMessage:@"Another driver took that order or canceled by customer." withDelegate:self withTag:100 otherButtonTitles:@[@"Ok"]];
                            
                        } else {
                            
                            [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:100 otherButtonTitles:@[@"Ok"]];
                        }
                    }];
}

- (void)requestForConfirmOrder
{
    [self showProgressHud];
    
    [WebServicesClient BeCourierOfPack:_packDetails.internalBaseClassIdentifier
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                            SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                     completionHandler:^(BOOL isPackCancelled, NSError *error) {
                         
                         [self hideProgressHud];
                         
                         if (!error) {
                             
                             [self requestForGetPackDetails];
                             [[NSNotificationCenter defaultCenter] postNotificationName:COURIER_ACTIVE_LIST_UPDATED object:@{}];
                             
                         }
                         else if (error.code == 14) {
                             
                             [UtilHelper showApplicationAlertWithMessage:@"Sorry, another driver has already confirmed to pick up. Shortly, you will get new orders."
                                                            withDelegate:self withTag:0
                                                       otherButtonTitles:@[@"Ok"]];
                             
                             
                         }
                         else if (error.code == 3) {
                             
                             [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                             
                         } else {
                             
                             [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                         }
                     }];
}

- (void)cancelCOrder
{
    [self showProgressHud];
    [WebServicesClient CancelPackageRequest:_packDetails.internalBaseClassIdentifier
                                LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                               LogUserAgent:[[UIDevice currentDevice] systemVersion]
                              LogDeviceType:LOG_DEVICE_TYPE
                                 SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                          completionHandler:^(BOOL isPackCancelled, NSError *error) {
                              
                              [self hideProgressHud];
                              if (!error) {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:@"You have canceled the job. No worries, Shortly, we will send a new order. Thanks, Senpex team." withDelegate:self withTag:100 otherButtonTitles:@[@"Ok"]];
                                  [[NSNotificationCenter defaultCenter] postNotificationName:COURIER_ACTIVE_LIST_UPDATED object:@{}];
                                  
                              }
                              else if (error.code == 14) {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:@"You have already canceled the job. No worries, Shortly, we will send a new order. Thanks, Senpex team."
                                                                 withDelegate:self withTag:0
                                                            otherButtonTitles:@[@"Ok"]];
                                  
                                  
                              }
                              else if (error.code == 3) {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                  
                              } else {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                              }
                          }];
}


//Report Problem
- (void)requestForCancelOrder
{
    [self showProgressHud];
    
    GetDictByName *dictName = _pickerOptionsList[self.selectedReasonIndex];
    
    [WebServicesClient CourierReportProblem:_packDetails.internalBaseClassIdentifier
                                 ReasonText:_commentTextView.text
                                   ReasonId:dictName.listId
                                LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                               LogUserAgent:[[UIDevice currentDevice] systemVersion]
                              LogDeviceType:LOG_DEVICE_TYPE
                                 SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                          completionHandler:^(BOOL isPackCancelled, NSError *error) {
                              
                              [self hideProgressHud];
                              
                              if (!error) {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:@"You have canceled the job. No worries, Shortly, we will send a new order. Thanks, Senpex team." withDelegate:self withTag:100 otherButtonTitles:@[@"Ok"]];
                                  [[NSNotificationCenter defaultCenter] postNotificationName:COURIER_ACTIVE_LIST_UPDATED object:@{}];
                              }
                              else if (error.code == 3) {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                  
                              } else {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                              }
                          }];
}

- (void)requestForCancelPack
{
    [self showProgressHud];
    [WebServicesClient CancelPack:_packDetails.internalBaseClassIdentifier
                      LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                     LogUserAgent:[[UIDevice currentDevice] systemVersion]
                    LogDeviceType:LOG_DEVICE_TYPE
                       SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                completionHandler:^(BOOL isPackCancelled, NSError *error) {
                    
                    [self hideProgressHud];
                    
                    if (!error) {
                        
                        [UtilHelper showApplicationAlertWithMessage:@"You have canceled the job. No worries, Shortly, we will send a new order. Thanks, Senpex team." withDelegate:self withTag:100 otherButtonTitles:@[@"Ok"]];
                        [[NSNotificationCenter defaultCenter] postNotificationName:COURIER_ACTIVE_LIST_UPDATED object:@{}];
                    }
                    else if (error.code == 3) {
                        
                        [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                        
                    } else {
                        
                        [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                    }
                }];
}

//Pack Not Given Request
- (void)requestForCustomerCancelled
{
    [self showProgressHud];
    
    [WebServicesClient PackDidnotGiven:_packDetails.internalBaseClassIdentifier
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                            SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                     completionHandler:^(BOOL isImageDeleted, NSError *error) {
                         
                         [self hideProgressHud];
                         
                         if (!error) {
                             
                             [UtilHelper showApplicationAlertWithMessage:@"You have canceled the job. No worries, Shortly, we will send a new order. Thanks, Senpex team." withDelegate:self withTag:100 otherButtonTitles:@[@"Ok"]];
                             [[NSNotificationCenter defaultCenter] postNotificationName:COURIER_ACTIVE_LIST_UPDATED object:@{}];
                         }
                         else if (error.code == 3) {
                             
                             [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                             
                         } else {
                             
                             [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                         }
                     }];
}

- (void)requestForTookPack
{
    [self showProgressHud];
    
    [WebServicesClient TakePack:_packDetails.internalBaseClassIdentifier
                    LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                   LogUserAgent:[[UIDevice currentDevice] systemVersion]
                  LogDeviceType:LOG_DEVICE_TYPE
                     SessionKey:[AppDelegate sharedAppDelegate].sessionKey
              completionHandler:^(BOOL isTookPack, NSError *error) {
                  
                  [self hideProgressHud];
                  
                  if (!error) {
                      
                      [self requestForGetPackDetails];
                      [[NSNotificationCenter defaultCenter] postNotificationName:COURIER_ACTIVE_LIST_UPDATED object:@{}];
                  }
                  else if (error.code == 3) {
                      
                      [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                      
                  } else {
                      
                      [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                  }
              }];
}

- (void)requestForDelivered
{
    [self showProgressHud];
    
    [WebServicesClient DeliverPack:_packDetails.internalBaseClassIdentifier
                       LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                      LogUserAgent:[[UIDevice currentDevice] systemVersion]
                     LogDeviceType:LOG_DEVICE_TYPE
                        SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                 completionHandler:^(BOOL isDelivered, NSError *error) {
                     
                     [self hideProgressHud];
                     
                     if (!error) {
                         
                         [self requestForGetPackDetails];
                         [[NSNotificationCenter defaultCenter] postNotificationName:COURIER_ACTIVE_LIST_UPDATED object:@{}];
                         
                     }
                     else if (error.code == 3) {
                         
                         [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                         
                     } else {
                         
                         [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                     }
                 }];
}

- (void)requestForDeliveryImage:(UIImage *)image
{
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    NSData* imageData = UIImageJPEGRepresentation(image, 0.5);
    NSString *image1Base64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString *stringBase = @"data:image/png;base64,";
    NSString *finalBase64 = [stringBase stringByAppendingString:image1Base64];
    
    [self showProgressHud];
    
    [WebServicesClient AddDeliveryImg:_packDetails.internalBaseClassIdentifier
                      PackImageBase64:finalBase64
                          LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                         LogUserAgent:[[UIDevice currentDevice] systemVersion]
                        LogDeviceType:LOG_DEVICE_TYPE
                           SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                    completionHandler:^(AddImageObject *addImage, NSError *error) {
                        
                        [self hideProgressHud];
                        
                        if (!error) {
                            
                            if (addImage.isUploaded) {
                                
                                AddImageObject *addImageObject = [AddImageObject new];
                                addImageObject.isUploaded = YES;
                                addImageObject.imageIdFromServer = addImage.insertedId;
                                
                                addImageObject.insertedId = [NSString stringWithFormat:@"%@", [[NSDate date] getDateTimeInUTC]];
                                addImageObject.imageName = addImage.imageName;
                                addImageObject.image = image;
                                [self->_deliveredOrdersImages addObject:addImageObject];
                                [self->_collectionView reloadData];
                            }
                        }
                        else if (error.code == 3) {
                            
                            [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                            
                        } else {
                            
                            [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                        }
                    }];
}

- (void)requestForDeleteDeliveryImage:(AddImageObject *)imageObject
{
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    [WebServicesClient DeleteDeliveryImg:imageObject.imageIdFromServer
                           PackImageName:imageObject.imageName
                             LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                            LogUserAgent:[[UIDevice currentDevice] systemVersion]
                           LogDeviceType:LOG_DEVICE_TYPE
                              SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                       completionHandler:^(BOOL isImageDeleted, NSError *error) {
                           
                           [self hideProgressHud];
                           
                           if (!error) {
                               
                               if (isImageDeleted) {
                                   
                                   [self->_deliveredOrdersImages removeObject:imageObject];
                                   
                                   if (self->_deliveredOrdersImages.count == 0) {
                                       
                                       //                                       [self loadDeliveredImagesTableView];
                                   }
                                   
                                   [_collectionView reloadData];
                               }
                           }
                           else if (error.code == 3) {
                               
                               [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                               
                           } else {
                               
                               [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                           }
                       }];
}

- (void)requestForAllDeliveredImages
{
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetAllDeliveryImgs:_packDetails.internalBaseClassIdentifier
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                                   SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                            completionHandler:^(NSMutableArray *deliveryImages, NSError *error) {
                                
                                [self hideProgressHud];
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    
                                    if (!error) {
                                        
                                        if (deliveryImages.count > 0) {
                                            
                                            [_deliveredOrdersImages addObjectsFromArray:deliveryImages];
                                            [_collectionView reloadData];
                                            //                                            [self loadDeliveredImagesTableView];
                                            //                                            [_tableview reloadData];
                                            
                                        } else {
                                            
                                            //                                            [self loadDeliveredImagesTableView];
                                        }
                                    }
                                });
                            }];
    });
}

- (void)animateOnViewUpdated
{
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

#pragma mark UICollectionView Methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger count = _deliveredOrdersImages.count;
    
    if(_deliveredOrdersImages.count > 0)
    {
        _collectionView.hidden = NO;
        
        if (IS_IPHONE_5) {
            
            //            _addImageXConstraint.constant = 108;
            
        }else if (IS_IPHONE_6P) {
            
            //            _addImageXConstraint.constant = 152;
        }else{
            
            //            _addImageXConstraint.constant = 134;
        }
    }
    else
    {
        //        _addImageXConstraint.constant = 0;
        _collectionView.hidden = YES;
    }
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MediaUploadingViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MediaUploadingViewCell" forIndexPath:indexPath];
    cell.mediaCellDelegate = self;
    AddImageObject *addImageObject = _deliveredOrdersImages[indexPath.row];
    [cell prepareUIForRowWithImage:addImageObject index:indexPath.row];
    
    if (addImageObject.image != nil) {
        [cell.activityIndicatorView stopAnimating];
        [cell.activityIndicatorView startAnimating];
    } else {
        
        //Shows indicatore on imageView
        [cell.imageView setShowActivityIndicatorView:YES];
        [cell.imageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, addImageObject.imageName];
        //Loads image
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:finalUrl] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
    }
    
    if(addImageObject.isUploaded == NO)
    {
        [cell.activityIndicatorView stopAnimating];
        [cell.activityIndicatorView startAnimating];
        cell.btnCancel.hidden = YES;
    }
    else {
        
        [cell.activityIndicatorView startAnimating];
        [cell.activityIndicatorView stopAnimating];
        cell.btnCancel.hidden = NO;
    }
    
    cell.btnCancel.hidden = NO;
    if([_packDetails.packStatus isEqualToString:@"40"]) {
        cell.btnCancel.hidden = YES;
    }
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_deliveredOrdersImages.count > 0) {
        
        NSMutableArray *imgArr = [NSMutableArray new];
        
        for (AddImageObject *addImage in _deliveredOrdersImages) {
            
            NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, addImage.imageName];
            [imgArr addObject:finalUrl];
        }
        
        LPPhotoViewer *pvc = [[LPPhotoViewer alloc] init];
        pvc.imgArr = imgArr;
        pvc.currentIndex = indexPath.row;
        [self presentViewController:pvc animated:YES completion:nil];
    }
}

- (void)cancelForCellRow:(NSInteger)row
{
    AddImageObject *addedImage = _deliveredOrdersImages[row];
    [self requestForDeleteDeliveryImage:addedImage];
}

//MARK: -Add Markers
- (void)addMarkers
{
    NSMutableArray *markerArray = [NSMutableArray new];
    CSUser *deliveryMarker = [CSUser new];
    CSUser *packMarker = [CSUser new];
    
    [_mapView clear];
    
    if (_packDetails != nil) {
        
        deliveryMarker.userId = 0;
        deliveryMarker.imageName = @"delivered-location-marker";
        deliveryMarker.latitude = [_packDetails.packToLat doubleValue];
        deliveryMarker.longitude = [_packDetails.packToLng doubleValue];
        deliveryMarker.marker = [_mapView addMapPinForUser:deliveryMarker];
        
        packMarker.userId = 1;
        packMarker.imageName = @"pick-location-marker";
        packMarker.latitude = [_packDetails.packFromLat doubleValue];
        packMarker.longitude = [_packDetails.packFromLng doubleValue];
        packMarker.marker = [_mapView addMapPinForUser:packMarker];
        
        [markerArray addObject:deliveryMarker];
        [markerArray addObject:packMarker];
        
        //GMSCoordinateBounds represents a rectangular bounding box on the Earth's surface.
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        
        for (CSUser *user in markerArray) {
            
            //Returns a GMSCoordinateBounds representing the current bounds extended to include the passed-in coordinate
            if((user.latitude != 0 && user.longitude != 0)) {
                bounds = [bounds includingCoordinate:user.marker.position];
            }
        }
        _mapView.padding = UIEdgeInsetsMake(20, 0, 0, 0);
        [_mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
        //        [_mapView showAllMarkersInMapViewWithBound:bounds];
        
        [self drawRoute];
    }
}

- (void)drawRoute
{
    CLLocation *userPack = [[CLLocation alloc] initWithLatitude:[_packDetails.packFromLat doubleValue] longitude:[_packDetails.packFromLng doubleValue]];
    
    CLLocation *deliveryPack = [[CLLocation alloc] initWithLatitude:[_packDetails.packToLat doubleValue] longitude:[_packDetails.packToLng doubleValue]];
    
    [self.mapView fetchPolylineWithOrigin:userPack color:ORANGE_LINE_COLOR destination:deliveryPack completionHandler:^(GMSPolyline *polyline)
     {
         if(polyline)
         {
             polyline.map = nil;
             polyline.map = self.mapView;
         }
     }];
}


@end
