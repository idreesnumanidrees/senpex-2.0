//
//  MyProfileCourierVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "MyProfileCourierVC.h"
#import "ProfileTableViewCell.h"
#import "HCSStarRatingView.h"
#import "LoginViewController.h"

@interface MyProfileCourierVC ()

//Outlets
@property (weak, nonatomic) IBOutlet UIButton *btnGoOffline;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabelTitle;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *imagesArray;
@property (nonatomic, strong) NSArray *menuNamesArray;

@property (strong, nonatomic) IBOutlet HCSStarRatingView *showRateView;

@end

@implementation MyProfileCourierVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareUI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark--
#pragma mark -- Helpers
- (void)prepareUI
{
    self.menuNamesArray = @[@"Stars Earned",@"Personal Information",@"Wallet",@"Bank Account Details",@"Settings",@"Feedback",@"FAQ", @"Log out"];
    self.imagesArray = @[@"Rating-courier-icon",@"personalInfo",@"payments",@"Balance-courier-Icon",@"settings",@"Feedback-courier-icon", @"faq",@"logout" ];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.profileImageView = (UIImageView *)[UtilHelper makeCornerCurved:self.profileImageView withRadius:self.profileImageView.frame.size.width/2 color:nil width:0];
    
    _showRateView.minimumValue = 0;
    _showRateView.maximumValue = 5;
    
    _balanceLabelTitle.hidden = YES;
    _balanceLabel.hidden = YES;
    
    self.btnGoOffline = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnGoOffline withRadius:12.5 color:[UIColor grayColor] width:0];
    
    // [self requestToLoginUserInfo];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSString *senderId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
    
    //Update database
    CourierInfoDB *courierInfo =  [CourierInfoDB getCourierInfoDB:[senderId intValue]];
    
    self.nameLabel.text = courierInfo.name;
    self.rateLabel.text = [NSString stringWithFormat:@"(%0.01f)",[courierInfo.rate floatValue]];
    self.typeLabel.text = @"Courier";
    _showRateView.value = [courierInfo.rate floatValue];
    [self loadCourierProfileImage:courierInfo.imageName];
    
    [self requestToLoginUserInfo];
    
    //Added 07-07-2018
    int courierWorkAvailabilityStatus = [[UtilHelper getValueFromNSUserDefaultForKey:COURIER_AVAILABLE_STATUS] intValue];
    
    if(courierWorkAvailabilityStatus == 1)
    {
        self.btnGoOffline.backgroundColor = ONLINE_BUTTON_SELECTION;
        [self.btnGoOffline setTitle:@"Go offline" forState:UIControlStateNormal];
    }
    else
    {
        self.btnGoOffline.selected = YES;
        self.btnGoOffline.backgroundColor = OFFLINE_BUTTON_SELECTION;
        [self.btnGoOffline setTitle:@"Start" forState:UIControlStateNormal];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self requestForEstimatedNextPayment];

}

-(void)loadCourierProfileImage:(NSString *)profileImage
{
    //Shows indicatore on imageView
    [_profileImageView setShowActivityIndicatorView:YES];
    [_profileImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, profileImage];
    
    [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    
    //Loads image
//    [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
}

#pragma mark -
#pragma mark - IBActions

- (IBAction)showcourierPersonalInfoAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showcourierPersonalInfoVC];
    
}
- (IBAction)profileAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showPersonalEditView];
}

- (IBAction)goOfflineAction:(UIButton *)sender {
    
    [self showProgressHud];
    
    int courierWorkAvailabilityStatus = [[UtilHelper getValueFromNSUserDefaultForKey:COURIER_AVAILABLE_STATUS] intValue];

    if (courierWorkAvailabilityStatus==1) {
        
        [self requestForUpdateAvailStatus:@"2"];

    } else {
       
        [self requestForUpdateAvailStatus:@"1"];
    }
}

#pragma mark -
#pragma mark - TableView Delegate and DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuNamesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProfileTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.imageIcon.image = [UIImage imageNamed:self.imagesArray[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (indexPath.row == self.menuNamesArray.count-1) {
        //last row
        cell.menuItemNameLabel.textColor = [UIColor blackColor];
        cell.menuItemNameLabel.font = [UIFont fontWithName:@"SFUIText-Regular" size:14];
    }else {
        cell.menuItemNameLabel.textColor = [UIColor blackColor];
        cell.menuItemNameLabel.font = [UIFont fontWithName:@"SFUIText-Regular" size:14];
    }
    cell.menuItemNameLabel.text = self.menuNamesArray[indexPath.row];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        
        [[AppDelegate sharedAppDelegate].appController showCourierRatingsView];
    }
    else if (indexPath.row == 1) {
        
        [[AppDelegate sharedAppDelegate].appController showcourierPersonalInfoVC];

    }else if (indexPath.row == 2) {
        
        [[AppDelegate sharedAppDelegate].appController showWithdrawHistoryView];
        
    }else if (indexPath.row == 3) {
        
        [[AppDelegate sharedAppDelegate].appController showAccountInfoView];
        
    }else if (indexPath.row == 4) {
        
        [[AppDelegate sharedAppDelegate].appController showCourierSettingsView];
        
    }else if (indexPath.row == 5) {
        
        [[AppDelegate sharedAppDelegate].appController showCourierFeedbackView];
        
    }else if (indexPath.row == 6) {
        
        [[AppDelegate sharedAppDelegate].appController showCourierFAQView:@"3"];
        
    }else if (indexPath.row == 7) {
        
        
        int courierWorkAvailabilityStatus = [[UtilHelper getValueFromNSUserDefaultForKey:COURIER_AVAILABLE_STATUS] intValue];

        if (courierWorkAvailabilityStatus==1) {
            
            [UtilHelper showApplicationAlertWithMessage:@"Before logout, you must stop working."
                                           withDelegate:self
                                                withTag:0
                                      otherButtonTitles:@[@"Ok"]];
            return;
        }
        
        [UtilHelper showApplicationAlertWithMessage:@"Are you sure you want to logout?"
                                       withDelegate:self
                                            withTag:1002
                                  otherButtonTitles:@[@"No", @"Yes"]];
    }
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1002)
    {
        if (buttonIndex == 1) {
            
            [self showProgressHud];
            [WebServicesClient LogoutUser:[AppDelegate sharedAppDelegate].iosDeviceToken
                             LogUserAgent:[[UIDevice currentDevice] systemVersion]
                            LogDeviceType:LOG_DEVICE_TYPE
                               SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                        completionHandler:^(BOOL isLogout, NSError *error) {
                            
                            [self hideProgressHud];
                            
                            if (!error) {
                                
                                [self senderLogout];
                                
                            }
                            else if (error.code == 3) {
                                
                                [self senderLogout];
                            }
                            else
                            {
                                [self senderLogout];
                            }
                        }];
        }
    }
    else if(alertView.tag == 1001) {
        
        [self senderLogout];

    }
}

- (void)backToLogin
{
    
    [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:0] forKey:LOGON_USER];
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[LoginViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:YES];
        }
    }
}

- (void)requestForEstimatedNextPayment
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetEstimatedNextPayment:[AppDelegate sharedAppDelegate].iosDeviceToken
                                      LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                     LogDeviceType:LOG_DEVICE_TYPE
                                        SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                 completionHandler:^(NSDictionary *paymentDate, NSError *error) {
                                     
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         
                                         if (!error) {
                                             
                                             if (paymentDate != nil) {
                                                 
                                                 self.balanceLabel.text = [NSString stringWithFormat:@"$%@", paymentDate[@"total_payment"]];
                                                 
                                                 self->_balanceLabelTitle.hidden = NO;
                                                 self->_balanceLabel.hidden = NO;
                                                 
                                             } else {
                                                 self->_balanceLabelTitle.hidden = YES;
                                                 self->_balanceLabel.hidden = YES;
                                                 self.balanceLabel.text = EMPTY_STRING;
                                             }
                                             
                                         } else {
                                             
                                             if (error.code == 3) {
                                                 
//                                                 [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again."
//                                                                                withDelegate:self
//                                                                                     withTag:1001 otherButtonTitles:@[@"Ok"]];
                                             }
                                             else {
                                                 
                                                // [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                                             }
                                         }
                                     });
                                 }];
    });
    
}

- (void)requestToLoginUserInfo
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetUserInfo:[AppDelegate sharedAppDelegate].sessionKey
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                     completionHandler:^(UserModel *user, NSError *error) {
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             if (!error) {
                                 
                                 [self populateUserdata:user];
                             }
                             else if (error.code == 3) {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                 
                             } else {
                                 
//                                 [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                             }
                         });
                     }];
    });
}

- (void)populateUserdata:(UserModel *)user
{
    self.rateLabel.text = [NSString stringWithFormat:@"(%0.01f)",[user.userRate floatValue]];
    self.typeLabel.text = @"Courier";
    _showRateView.value = [user.userRate floatValue];
    [self loadCourierProfileImage:user.selfImg];
    
    [CourierInfoDB insertAndupdateCourierWithId:user.internalBaseClassIdentifier
                                           Name:user.name
                                        SurName:user.surname
                                          Email:user.email
                                        Address:user.addressActual
                                      ImageName:user.selfImg
                                           Rate:user.userRate
                                        Balance:@"100"
                                       IsUpdate:YES];
}

#pragma mark--
#pragma mark-- Server Side handling //***************************************************************

- (void)requestForUpdateAvailStatus:(NSString *)status
{
    [WebServicesClient UpdateUserAvailStatus:status
                               LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                 LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                LogUserAgent:[[UIDevice currentDevice] systemVersion]
                               LogDeviceType:LOG_DEVICE_TYPE
                           completionHandler:^(BOOL result, NSError *error) {
                               
                               [self hideProgressHud];
                               
                               if (!error) {
                                   
                                   if ([self.btnGoOffline.titleLabel.text isEqualToString:@"Go offline"]) {
                                       
                                       self.btnGoOffline.backgroundColor = OFFLINE_BUTTON_SELECTION;
                                       [self.btnGoOffline setTitle:@"Start" forState:UIControlStateNormal];
                                       
                                       [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:0]
                                                                                 forKey:COURIER_AVAILABLE_STATUS];
                                       
                                   } else {
                                       
                                       [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:1]
                                                                                 forKey:COURIER_AVAILABLE_STATUS];
                                       
                                       self.btnGoOffline.backgroundColor = ONLINE_BUTTON_SELECTION;
                                       [self.btnGoOffline setTitle:@"Go offline" forState:UIControlStateNormal];
                                   }
                               }
                               else if (error.code == 3) {
                                   
                                   [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                   
                               } else {
                                   
                                   [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                               }
                           }];
}



@end
