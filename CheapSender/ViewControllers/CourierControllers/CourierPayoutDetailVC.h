//
//  CourierPayoutDetailVC.h
//  CheapSender
//
//  Created by Idrees on 2017/08/27.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface CourierPayoutDetailVC : BaseViewController

@property (nonatomic, strong) NSString *payoutId;

@end
