//
//  CourierPopularQuestionsVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface CourierPopularQuestionsVC : BaseViewController

@property (nonatomic, strong) NSString *faqId;
@property (nonatomic, strong) NSString *from;
@property (weak, nonatomic) IBOutlet UIImageView *topView;
@property (weak, nonatomic) IBOutlet UIView *navigationView;


@end
