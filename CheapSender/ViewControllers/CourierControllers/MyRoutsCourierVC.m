//
//  MyRoutsCourierVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "MyRoutsCourierVC.h"
#import "MyRouteTableCell.h"

@interface MyRoutsCourierVC ()
@property (weak, nonatomic) IBOutlet UIView *emptyPotentialRoutesView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet NSMutableArray *routes;


@end

@implementation MyRoutsCourierVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self requestForRoutes];
}

//MARK: - IBActions
- (IBAction)addRouteTapped:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showSaveRoutView:nil];
}

#pragma mark --
#pragma mark -- TableView Delegate and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _routes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyRouteTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyRouteTableCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.weekView.hidden = YES;
    cell.dateTimeLabel.hidden = YES;
    cell.timeLabel.hidden = YES;

    [cell.btnMonday setBackgroundImage:nil forState:UIControlStateNormal];
    [cell.btnTuesday setBackgroundImage:nil forState:UIControlStateNormal];
    [cell.btnWednesday setBackgroundImage:nil forState:UIControlStateNormal];
    [cell.btnThursday setBackgroundImage:nil forState:UIControlStateNormal];
    [cell.btnFriday setBackgroundImage:nil forState:UIControlStateNormal];
    [cell.btnSaturday setBackgroundImage:nil forState:UIControlStateNormal];
    [cell.btnSunday setBackgroundImage:nil forState:UIControlStateNormal];
    
    cell.btnMonday.selected = NO;
    cell.btnTuesday.selected = NO;
    cell.btnWednesday.selected = NO;
    cell.btnThursday.selected = NO;
    cell.btnFriday.selected = NO;
    cell.btnSaturday.selected = NO;
    cell.btnSunday.selected = NO;
    
    NSDictionary *routeDic = _routes[indexPath.row];
    
    cell.fromLocationLabel.text = routeDic[@"from_text"];
    cell.toLocationLabel.text = routeDic[@"to_text"];
    
    if ([routeDic[@"rec_type"] isEqualToString:@"1"]) {
        
        cell.weekView.hidden = NO;
        cell.timeLabel.hidden = NO;
        
        NSDate *dateFromString = [NSDate dateFromDateString:routeDic[@"route_date"]];
        NSString *timeString = [dateFromString getTimeInUTC];
        
        cell.timeLabel.text = timeString;
        
        NSArray *weekdays = routeDic[@"user_route_recs"];
        
        for (NSDictionary *day in weekdays) {
            
            if ([day[@"day_of_week"] isEqualToString:@"1"]) {
                
                cell.btnMonday.selected = YES;
                [cell.btnMonday setBackgroundImage:[UIImage imageNamed:@"selectedDay-icon"] forState:UIControlStateSelected];
                
            } else if ([day[@"day_of_week"] isEqualToString:@"2"]) {
                
                cell.btnTuesday.selected = YES;
                [cell.btnTuesday setBackgroundImage:[UIImage imageNamed:@"selectedDay-icon"] forState:UIControlStateSelected];

            } else if ([day[@"day_of_week"] isEqualToString:@"3"]) {
                
                cell.btnWednesday.selected = YES;
                [cell.btnWednesday setBackgroundImage:[UIImage imageNamed:@"selectedDay-icon"] forState:UIControlStateSelected];

            } else if ([day[@"day_of_week"] isEqualToString:@"4"]) {
                
                cell.btnThursday.selected = YES;
                [cell.btnThursday setBackgroundImage:[UIImage imageNamed:@"selectedDay-icon"] forState:UIControlStateSelected];

            } else if ([day[@"day_of_week"] isEqualToString:@"5"]) {
                
                cell.btnFriday.selected = YES;
                [cell.btnFriday setBackgroundImage:[UIImage imageNamed:@"selectedDay-icon"] forState:UIControlStateSelected];

            } else if ([day[@"day_of_week"] isEqualToString:@"6"]) {
                
                cell.btnSaturday.selected = YES;
                [cell.btnSaturday setBackgroundImage:[UIImage imageNamed:@"selectedDay-icon"] forState:UIControlStateSelected];

            } else if ([day[@"day_of_week"] isEqualToString:@"7"]) {
                
                cell.btnSunday.selected = YES;
                [cell.btnSunday setBackgroundImage:[UIImage imageNamed:@"selectedDay-icon"] forState:UIControlStateSelected];
            }
        }
    } else {
        
        cell.dateTimeLabel.hidden = NO;
        
        NSDate *dateFromString = [NSDate dateFromDateString:routeDic[@"route_date"]];
        NSString *dateString = [dateFromString getTimeDayFromDate];
        NSString *timeString = [dateFromString getTimeFromDateWithoutDayAndUTC];
        cell.dateTimeLabel.text = [NSString stringWithFormat:@"%@    Till %@", dateString, timeString];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *routeDic = _routes[indexPath.row];
    [[AppDelegate sharedAppDelegate].appController showSaveRoutView:routeDic];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140.0;
}

//MARK: -Request for Routes

- (void)requestForRoutes
{
    [self showProgressHud];
    
    NSString *courierId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
    [WebServicesClient GetUserRoutes:courierId
                       logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                         LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                        LogUserAgent:[[UIDevice currentDevice] systemVersion]
                       LogDeviceType:LOG_DEVICE_TYPE
                   completionHandler:^(NSMutableArray *routesList, NSError *error) {
                       
                       [self hideProgressHud];
                       
                       if (!error) {
                           
                           if (routesList.count > 0) {
                               
                               _routes = routesList;
                               self->_emptyPotentialRoutesView.hidden = YES;
                               
                           } else {
                               
                               self->_emptyPotentialRoutesView.hidden = NO;
                               _routes = [NSMutableArray new];
                           }
                           [self->_tableView reloadData];
                    
                       }
                       else if (error.code == 3) {
                           
                           [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                       }
                       else
                       {
                           [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                       }
                   }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }
}


@end
