//
//  SaveMyRouteView.m
//  CheapSender
//
//  Created by apple on 3/29/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import "SaveMyRouteView.h"
#import <FSCalendar/FSCalendar.h>

@interface SaveMyRouteView ()

@property (weak, nonatomic) IBOutlet UIButton *btnSaveRout;
@property (weak, nonatomic) IBOutlet UIButton *onTimeRouteButton;
@property (weak, nonatomic) IBOutlet UIButton *updateButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *selectDateTimeButton;

@property (weak, nonatomic) IBOutlet UIView *editView;

@property (weak, nonatomic) IBOutlet UIScrollView *innerScrollView;

@property (weak, nonatomic) IBOutlet UILabel *headingLabel;

@property (weak, nonatomic) IBOutlet UIButton *btnCar;
@property (weak, nonatomic) IBOutlet UIButton *btnVan;
@property (weak, nonatomic) IBOutlet UIButton *btnTruck;
@property (weak, nonatomic) IBOutlet UIButton *btnScooter;
@property (weak, nonatomic) IBOutlet UIButton *btnBike;
@property (weak, nonatomic) IBOutlet UIButton *btnWalk;
@property (strong, nonatomic) UIDatePicker *picker;

@property (nonatomic, strong)NSDate *pickerSelectedDate;

@property (assign, nonatomic) BOOL isRecursive;

@property (assign, nonatomic) float from_latitude;
@property (assign, nonatomic) float from_longitude;
@property (assign, nonatomic) float to_latitude;
@property (assign, nonatomic) float to_longitude;
@property (strong, nonatomic) NSString *transportType;

@property (strong, nonatomic) NSString *dateTimeString;
@property (nonatomic, strong) NSArray *validationArray;
@property (nonatomic, strong) NSMutableArray *daysArray;
@property (weak, nonatomic) IBOutlet UIView *weekdaysView;

@end

@implementation SaveMyRouteView

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)viewWillAppear:(BOOL)animated {
    
    //Called PlaceSearchTextField optional method
    [self placeTextFieldOptionals];
}

- (void)viewDidAppear:(BOOL)animated {
    
    //Called PlaceSearchTextField optional method
    //    [self placeTextFieldOptionals];
}

- (void)prepareUI {
    
    self.btnSaveRout = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnSaveRout withRadius:24.0 color:COMMON_ICONS_COLOR width:0];
    self.updateButton = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.updateButton withRadius:24.0 color:COMMON_ICONS_COLOR width:0];
    self.deleteButton = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.deleteButton withRadius:24.0 color:COMMON_ICONS_COLOR width:0];
    
    self.validationArray = @[_fromPlaceSearchTextField,_toPlaceSearchTextField];
    _from_latitude = 0;
    _from_longitude = 0;
    _to_latitude = 0;
    _to_longitude = 0;
    _deleteButton.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
    
    _isRecursive = NO;
    _onTimeRouteButton.selected = YES;
    _onTimeRouteButton.backgroundColor = [UIColor colorWithRed:17.0/255.0 green:167.0/255.0 blue:170.0/255.0 alpha:1.0f];
    [_selectDateTimeButton setTitle:@"Select date and time" forState:UIControlStateNormal];
    _weekdaysView.hidden = YES;

    _transportType = EMPTY_STRING;
    _dateTimeString = EMPTY_STRING;
    _btnSaveRout.selected = YES;
    _daysArray = [NSMutableArray new];
    
    //Request for get users transport
    if ([AppDelegate sharedAppDelegate].allDictData == nil) {
        
        [self getAllDict];
    }
    
    //MARK: --About Update Route
    if (_routeDict != nil) {
        
        [self populateDataForUpdate];
    } else {
        _pickerSelectedDate = [NSDate date];
    }
}

//Setup of Optional Properties of MVPlaceSearchTextField
- (void)placeTextFieldOptionals
{
    //Optional Properties of MVPlaceSearchTextField
    _fromPlaceSearchTextField.autoCompleteRegularFontName =  @"SFUIText-Bold";
    _fromPlaceSearchTextField.autoCompleteBoldFontName = @"SFUIText-Regular";
    _fromPlaceSearchTextField.autoCompleteTableCornerRadius=0.0;
    //    _fromPlaceSearchTextField.autoCompleteRowHeight=30;
    
    _fromPlaceSearchTextField.autoCompleteTableCellTextColor=[UIColor colorWithWhite:0.131 alpha:1.000];
    _fromPlaceSearchTextField.autoCompleteFontSize=14;
    _fromPlaceSearchTextField.backgroundColor = [UIColor whiteColor];
    //    _fromPlaceSearchTextField.autoCompleteTableBorderWidth=1.0;
    _fromPlaceSearchTextField.showTextFieldDropShadowWhenAutoCompleteTableIsOpen=YES;
    _fromPlaceSearchTextField.autoCompleteShouldHideOnSelection=YES;
    _fromPlaceSearchTextField.autoCompleteShouldHideClosingKeyboard=YES;
    _fromPlaceSearchTextField.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
    _fromPlaceSearchTextField.clearButtonMode = UITextFieldViewModeAlways;
    //    _fromPlaceSearchTextField = (MVPlaceSearchTextField *)[UtilHelper shadowView:(UIView *)self.fromPlaceSearchTextField];
    
    //Placeholder Pedding in MVPlaceSearchTextField
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _fromPlaceSearchTextField.leftView = paddingView;
    _fromPlaceSearchTextField.leftViewMode = UITextFieldViewModeAlways;
    
    //Optional Properties of MVPlaceSearchTextField
    _toPlaceSearchTextField.autoCompleteRegularFontName =  @"SFUIText-Bold";
    _toPlaceSearchTextField.autoCompleteBoldFontName = @"SFUIText-Regular";
    _toPlaceSearchTextField.autoCompleteTableCornerRadius=0.0;
    //    _toPlaceSearchTextField.autoCompleteRowHeight=30;
    _toPlaceSearchTextField.autoCompleteTableCellTextColor=[UIColor colorWithWhite:0.131 alpha:1.000];
    _toPlaceSearchTextField.autoCompleteFontSize=14;
    _toPlaceSearchTextField.backgroundColor = [UIColor whiteColor];
    //    _toPlaceSearchTextField.autoCompleteTableBorderWidth=1.0;
    _toPlaceSearchTextField.showTextFieldDropShadowWhenAutoCompleteTableIsOpen=YES;
    _toPlaceSearchTextField.autoCompleteShouldHideOnSelection=YES;
    _toPlaceSearchTextField.autoCompleteShouldHideClosingKeyboard=YES;
    _toPlaceSearchTextField.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
    _toPlaceSearchTextField.clearButtonMode = UITextFieldViewModeAlways;
    //    _toPlaceSearchTextField = (MVPlaceSearchTextField *)[UtilHelper shadowView:(UIView *)self.toPlaceSearchTextField];
    
    //Placeholder Pedding in MVPlaceSearchTextField
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _toPlaceSearchTextField.leftView = paddingView2;
    _toPlaceSearchTextField.leftViewMode = UITextFieldViewModeAlways;
}

//MARK: -IBActions
- (IBAction)selectDaysTapped:(UIButton *)sender {
    
    //    if (_dateTimeString == EMPTY_STRING) {
    //
    //        [UtilHelper showApplicationAlertWithMessage:@"Please first select time for route" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    //        return;
    //    }
    //
    //    if (_isRecursive == NO) {
    //
    //        [UtilHelper showApplicationAlertWithMessage:@"Sorry, you can not select days, you have selected one time route." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    //        return;
    //    }
    
    sender.selected = !sender.selected;
    
    if (sender.selected == YES) {
        
        [sender setBackgroundImage:[UIImage imageNamed:@"selectedDay-icon"] forState:UIControlStateSelected];
        [_daysArray addObject:[NSNumber numberWithInteger:sender.tag]];
        
    } else {
        
        if ([_daysArray containsObject:[NSNumber numberWithInteger:sender.tag]]) {
            
            [sender setBackgroundImage:nil forState:UIControlStateNormal];
            
            if (_daysArray.count > 0) {
                [_daysArray removeObject:[NSNumber numberWithInteger:sender.tag]];
            }
        }
    }
    [self checkActiveButton:@"123"];
}

- (IBAction)selectVehicleTypeTapped:(UIButton *)sender {
    
    _btnCar.alpha = 0.3;_btnVan.alpha = 0.3;_btnTruck.alpha = 0.3;_btnScooter.alpha = 0.3;_btnBike.alpha = 0.3; _btnWalk.alpha = 0.3;
    sender.alpha = 1.0;
    
    if (_btnCar.alpha == 1.0) {
        
        _transportType = [self getVehicleId:@"Car"];
        
    } else if (_btnVan.alpha == 1.0) {
        
        _transportType = [self getVehicleId:@"Van"];
        
    } else if (_btnTruck.alpha == 1.0) {
        
        _transportType = [self getVehicleId:@"Truck"];
        
    } else if (_btnScooter.alpha == 1.0) {
        
        _transportType = [self getVehicleId:@"Scooter"];
        
    } else if (_btnBike.alpha == 1.0) {
        
        _transportType = [self getVehicleId:@"Byke"];
        
    }else if (_btnWalk.alpha == 1.0) {
        
        _transportType = [self getVehicleId:@"Walking"];
    }
    [self checkActiveButton:@"123"];
}

- (IBAction)selectDateTimeTapped:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select route date and time \n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    _picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 50, 270, 212)];
    _picker.backgroundColor = [UIColor colorWithRed:232.0/255 green:232.0/255 blue:232.0/255 alpha:1.0];
    [_picker setDatePickerMode:UIDatePickerModeDateAndTime];
    [_picker setMinimumDate:[NSDate date]];
    
    if (_isRecursive) {
        
        alert.title = @"Select route time";
        [_picker setDatePickerMode:UIDatePickerModeDateAndTime];
        
    } else {
        
        [_picker setDatePickerMode:UIDatePickerModeDateAndTime];
    }
    
    if (_pickerSelectedDate != nil) {
        
        [_picker setDate:_pickerSelectedDate];
    }
    [alert.view addSubview:_picker];

    
    UIAlertAction* selectButton = [UIAlertAction
                                   actionWithTitle:@"Select"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                       //Handle your yes please button action here
                                      [self updateTekenTime];
                                   }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
    
    [alert addAction:cancelButton];
    [alert addAction:selectButton];
    
    UIPopoverPresentationController *popoverController = alert.popoverPresentationController;
    popoverController.sourceView = self.view;
    popoverController.sourceRect = [self.view bounds];
    [self presentViewController:alert  animated:YES completion:^{
    }];
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select route date and time" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
//    alert.delegate = self;
//    alert.tag = 100;
//    _picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
//
//    if (_isRecursive) {
//
//        alert.title = @"Select route time";
//        [_picker setDatePickerMode:UIDatePickerModeTime];
//
//    } else {
//
//        [_picker setDatePickerMode:UIDatePickerModeDateAndTime];
//    }
//    [_picker setMinimumDate:[NSDate date]];
//    [_picker setDate:_pickerSelectedDate];
//    [alert addSubview:_picker];
//    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
//    [alert setValue:_picker forKey:@"accessoryView"];
//    [alert show];
}

- (IBAction)oneTimeSelection:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    _dateTimeString = EMPTY_STRING;
    
    for (UIButton *v in _weekdaysView.subviews) {
        if ([v isKindOfClass:[UIButton class]]) {
            v.selected = NO;
            [v setBackgroundImage:nil forState:UIControlStateNormal];
        }
    }
    [_daysArray removeAllObjects];
    
    if (sender.selected) {
        
        _isRecursive = NO;
        
        sender.backgroundColor = [UIColor colorWithRed:17.0/255.0 green:167.0/255.0 blue:170.0/255.0 alpha:1.0f];
        [_selectDateTimeButton setTitle:@"Select date and time" forState:UIControlStateNormal];
        _weekdaysView.hidden = YES;
        
    } else {
        
        _isRecursive = NO;
        [_selectDateTimeButton setTitle:@"Select time" forState:UIControlStateNormal];
        sender.backgroundColor = [UIColor clearColor];
        _weekdaysView.hidden = NO;
    }
    
    [self checkActiveButton:@"abc"];
}

- (IBAction)saveRoutTapped:(id)sender {
    [self addUpdateMethod];
}

- (IBAction)updateTapped:(id)sender {
    
    [self addUpdateMethod];
}

- (void)addUpdateMethod
{
    if (_isRecursive == YES) {
        
        if (_daysArray.count == 0) {
            
            [UtilHelper showApplicationAlertWithMessage:@"Sorry, please select minimum one day, you have selected repeate days option" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
    }
    [self requestForAddUserRoute];
}

- (IBAction)deleteTapped:(id)sender {
    
    [UtilHelper showApplicationAlertWithMessage:@"Are you sure, you want to delete this route?"
                                   withDelegate:self
                                        withTag:88
                              otherButtonTitles:@[@"No", @"Yes"]];
}


//MARK: - Helpers methods
- (NSString *)getVehicleId:(NSString *)vehicleN
{
    NSString *vehicleIdFromWeb = EMPTY_STRING;
    if ([AppDelegate sharedAppDelegate].allDictData != nil) {
        
        for (GetDictByName *getDictByName in [AppDelegate sharedAppDelegate].allDictData) {
            
            if ([getDictByName.dicName isEqualToString:@"Transport_types"]) {
                
                if ([getDictByName.listName isEqualToString:vehicleN]) {
                    
                    vehicleIdFromWeb = getDictByName.listId;
                    break;
                }
            }
        }
    }
    return vehicleIdFromWeb;
}

- (void)populateDataForUpdate {
    
    _headingLabel.text = @"Edit Route";
    _editView.hidden = NO;
    _btnSaveRout.hidden = YES;
    for (NSDictionary *dict in _routeDict[@"user_route_recs"]) {
        
        [_daysArray addObject:[NSNumber numberWithInt:[dict[@"day_of_week"] intValue]]];
    }
    
    _from_latitude =[_routeDict[@"from_lat"] floatValue];
    _from_longitude = [_routeDict[@"from_lng"] floatValue];
    _to_latitude = [_routeDict[@"to_lat"] floatValue];
    _to_longitude = [_routeDict[@"to_lng"] floatValue];
    
    _fromPlaceSearchTextField.text = _routeDict[@"from_text"];
    _toPlaceSearchTextField.text = _routeDict[@"to_text"];
    _dateTimeString = _routeDict[@"route_date"];
    
    _isRecursive = [_routeDict[@"rec_type"] boolValue];
    
    if (_isRecursive == YES) {
        
        _onTimeRouteButton.selected = NO;
        _onTimeRouteButton.backgroundColor = [UIColor clearColor];
        _weekdaysView.hidden = NO;
        
        NSDate *dateFromString = [NSDate dateFromDateString:_routeDict[@"route_date"]];
        _dateTimeString = [dateFromString getDateTimeInLocalTimeZone];
        
        NSString *timeString = [dateFromString getTimeInUTC];
        [_selectDateTimeButton setTitle:timeString forState:UIControlStateNormal];
        
        for (UIButton *v in _weekdaysView.subviews) {
            if ([v isKindOfClass:[UIButton class]]) {
                
                if ([_daysArray containsObject:[NSNumber numberWithInt:v.tag]]) {
                    v.selected = YES;
                    [v setBackgroundImage:[UIImage imageNamed:@"selectedDay-icon"] forState:UIControlStateSelected];
                }else {
                    
                    v.selected = NO;
                    [v setBackgroundImage:nil forState:UIControlStateNormal];
                }
            }
        }
    } else {
        
        NSDate *dateFromString = [NSDate dateFromDateString:_routeDict[@"route_date"]];
        _pickerSelectedDate = dateFromString;
        
        NSString *dateString = [dateFromString getTimeDayFromDate];
        NSString *timeString = [dateFromString getTimeFromDateWithoutDayAndUTC];
        
        _dateTimeString = [dateFromString getDateTimeInLocalTimeZone];
        [_selectDateTimeButton setTitle:[NSString stringWithFormat:@"%@, %@", dateString, timeString] forState:UIControlStateNormal];
        
        _weekdaysView.hidden = YES;
        _onTimeRouteButton.backgroundColor = [UIColor colorWithRed:17.0/255.0 green:167.0/255.0 blue:170.0/255.0 alpha:1.0f];
        _onTimeRouteButton.selected = YES;
    }
    
    _transportType = _routeDict[@"transport_type"];
    if ([_transportType isEqualToString:@"1"]) {
        
        _btnCar.alpha = 1.0f;
        
    } else if ([_transportType isEqualToString:@"2"]) {
        
        _btnVan.alpha = 1.0f;
        
    } else if ([_transportType isEqualToString:@"3"]) {
        
        _btnTruck.alpha = 1.0f;
        
    } else if ([_transportType isEqualToString:@"4"]) {
        
        _btnScooter.alpha = 1.0f;
        
    } else if ([_transportType isEqualToString:@"5"]) {
        
        _btnBike.alpha = 1.0f;
        
    }else if ([_transportType isEqualToString:@"7"]) {
        
        _btnWalk.alpha = 1.0f;
    }
    
    [self checkActiveButton:@"123"];
}

//MARK: --Search Place from google
#pragma mark
#pragma mark - Place search Textfield Delegates

- (void)placeSearch:(MVPlaceSearchTextField *)textField ResponseForSelectedPlace:(GMSPlace *)responseDict{
    
    if (textField == _fromPlaceSearchTextField) {
        
        _fromPlaceSearchTextField.text = responseDict.formattedAddress;
        _from_latitude = responseDict.coordinate.latitude;
        _from_longitude = responseDict.coordinate.longitude;
        [self checkActiveButton:_fromPlaceSearchTextField.text];
        
    } else if (textField == _toPlaceSearchTextField) {
        
        _toPlaceSearchTextField.text = responseDict.formattedAddress;
        _to_latitude = responseDict.coordinate.latitude;
        _to_longitude = responseDict.coordinate.longitude;
        [self checkActiveButton:_fromPlaceSearchTextField.text];
    }
    [textField resignFirstResponder];
}

- (void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField
{}

- (void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField
{}

- (void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
    cell.contentView.backgroundColor = [UIColor whiteColor];
}

//MARK: - Alertview delegates
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
    }
    else if (alertView.tag == 100) {
        
        if (buttonIndex == 1) {
            
            [self updateTekenTime];
        }
    }
    else if (alertView.tag == 1111)
    {
        [self backAction:nil];
    }
    else if (alertView.tag == 88) {
        if (buttonIndex == 1) {
            [self requestForDeleteRoute];
        }
    }
}

- (void)updateTekenTime
{
    //getMMDDYYYYDate
    NSDate *date = self.picker.date;
    _pickerSelectedDate = date;
    _dateTimeString = [date getDateTimeInUTC];
    
    if (_isRecursive) {
        
        NSString *timeString = [date getTimeFromDateWithoutDayAndUTC];
        [_selectDateTimeButton setTitle:timeString forState:UIControlStateNormal];
        
    } else {
        
        NSString *dateString = [date getTimeDayFromDate];
        NSString *timeString = [date getTimeFromDateWithoutDayAndUTC];
        [_selectDateTimeButton setTitle:[NSString stringWithFormat:@"%@, %@", dateString, timeString] forState:UIControlStateNormal];
    }
    
    [self checkActiveButton:@"123"];
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)checkActiveButton:(NSString *)numberOFCharacter
{
    BOOL isActive = NO;
    
    for (JVFloatLabeledTextField *tfText in self.validationArray) {
        
        if (tfText.text.length > 0) {
            
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
            
        } else {
            
            isActive = NO;
            break;
        }
    }
    
    if (_from_latitude == 0 || _from_latitude == 0 || _to_latitude == 0 || _to_latitude == 0 || _transportType == EMPTY_STRING || _dateTimeString == EMPTY_STRING) {
        
        isActive = NO;
    }
    
    if (isActive) {
        
        _btnSaveRout.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
        _btnSaveRout.selected = NO;
        
        _updateButton.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
        _updateButton.selected = NO;
        
    }else {
        
        _updateButton.backgroundColor = LOGIN_DISABLE_COLOR;
        _updateButton.selected = YES;
        
        _btnSaveRout.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnSaveRout.selected = YES;
    }
}

- (void)requestForAddUserRoute
{
    NSString *daysOfWeek = EMPTY_STRING;
    if (_daysArray.count > 0) {
        
        daysOfWeek = [_daysArray componentsJoinedByString:@","];
    }
    
    if (_routeDict != nil) {
        
        if (![self validationForWeb]) {
            return;
        }
        [self showProgressHud];
        [WebServicesClient UpdateUserRoutes:[NSString stringWithFormat:@"%f", _from_latitude]
                                    fromLng:[NSString stringWithFormat:@"%f", _from_longitude]
                                      toLat:[NSString stringWithFormat:@"%f", _to_longitude]
                                      toLng:[NSString stringWithFormat:@"%f", _to_latitude]
                                fromAddress:[NSString stringWithFormat:@"%@", _fromPlaceSearchTextField.text]
                                  toAddress:[NSString stringWithFormat:@"%@", _toPlaceSearchTextField.text]
                              recursiveType:[NSString stringWithFormat:@"%d", _isRecursive]
                                  routeDate:_dateTimeString
                                  dayOfWeek:daysOfWeek
                              transportType:_transportType
                                    routeId:_routeDict[@"id"]
                              logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                               LogUserAgent:[[UIDevice currentDevice] systemVersion]
                              LogDeviceType:LOG_DEVICE_TYPE
                          completionHandler:^(BOOL isAddedRoute, NSError *error) {
                              
                              [self hideProgressHud];
                              
                              if (!error) {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:@"Route Updated Successfully." withDelegate:self withTag:1111 otherButtonTitles:@[@"Ok"]];
                                  
                              } else {
                                  
                                  if (error.code == 3) {
                                      
                                      [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                  }
                              }
                          }];
    }else {
        
        if (![self validationForWeb]) {
            return;
        }
        [self showProgressHud];
        [WebServicesClient AddUserRoutes:[NSString stringWithFormat:@"%f", _from_latitude]
                                 fromLng:[NSString stringWithFormat:@"%f", _from_longitude]
                                   toLat:[NSString stringWithFormat:@"%f", _to_longitude]
                                   toLng:[NSString stringWithFormat:@"%f", _to_latitude]
                             fromAddress:[NSString stringWithFormat:@"%@", _fromPlaceSearchTextField.text]
                               toAddress:[NSString stringWithFormat:@"%@", _toPlaceSearchTextField.text]
                           recursiveType:[NSString stringWithFormat:@"%d", _isRecursive]
                               routeDate:_dateTimeString
                               dayOfWeek:daysOfWeek
                           transportType:_transportType
                           logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                             LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                            LogUserAgent:[[UIDevice currentDevice] systemVersion]
                           LogDeviceType:LOG_DEVICE_TYPE
                       completionHandler:^(BOOL isAddedRoute, NSError *error) {
                           
                           [self hideProgressHud];
                           
                           if (!error) {
                               
                               [UtilHelper showApplicationAlertWithMessage:@"Route Added Successfully." withDelegate:self withTag:1111 otherButtonTitles:@[@"Ok"]];
                               
                           } else {
                               
                               if (error.code == 3) {
                                   
                                   [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                               }
                           }
                       }];
    }
}

- (void)requestForDeleteRoute
{
    [self showProgressHud];
    
    [WebServicesClient DeleteUserRoutes:_routeDict[@"id"]
                          logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                            LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                           LogUserAgent:[[UIDevice currentDevice] systemVersion]
                          LogDeviceType:LOG_DEVICE_TYPE
                      completionHandler:^(BOOL isDeletedRoute, NSError *error) {
                          
                          [self hideProgressHud];
                          
                          if (!error) {
                              
                              [UtilHelper showApplicationAlertWithMessage:@"Route Deleted Successfully." withDelegate:self withTag:1111 otherButtonTitles:@[@"Ok"]];
                              
                          } else {
                              
                              if (error.code == 3) {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                              }
                          }
                      }];
}

- (BOOL)validationForWeb
{
    if (_from_latitude == 0 || _to_latitude == 0) {
        [UtilHelper showApplicationAlertWithMessage:@"Please enter from and to addresses." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return NO;
    }
    else if (_dateTimeString == EMPTY_STRING) {
        
        if (_isRecursive == YES) {
            [UtilHelper showApplicationAlertWithMessage:@"Please select time" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        }else{
            [UtilHelper showApplicationAlertWithMessage:@"Please select date and time" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        }
        return NO;
        
    } else if (_transportType == EMPTY_STRING) {
        [UtilHelper showApplicationAlertWithMessage:@"Please select size transportation type." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return NO;
    }
    return YES;
}


@end
