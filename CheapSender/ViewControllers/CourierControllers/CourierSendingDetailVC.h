//
//  CourierSendingDetailVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/14.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "AUIAutoGrowingTextView.h"
#import "UIImageView+AFNetworking.h"
#import "CourierListData.h"
#import "GoogleMapHelper.h"
#import "MediaUploadingViewCell.h"

@interface CourierSendingDetailVC : BaseViewController  <MediaUploadCellDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate, UIAlertViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, GMSMapViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIButton *btnChat;
@property (weak, nonatomic) IBOutlet UIButton *btnCall;

@property (weak, nonatomic) IBOutlet UIButton *btnConfirmOrder;

//Cancel order reason
@property (weak, nonatomic) IBOutlet UILabel *lblReason;
@property (nonatomic, assign) NSInteger selectedReasonIndex;
@property (nonatomic, strong) NSArray *pickerOptionsList;

//Arrays
@property (strong, nonatomic) NSArray *imagesData;

//Cancel Delivery view outlets
@property (weak, nonatomic) IBOutlet UIView *cancelDeliveryView;
@property (weak, nonatomic) IBOutlet AUIAutoGrowingTextView *commentTextView;
@property (weak, nonatomic) IBOutlet UIButton *btnDelivery;


@property (weak, nonatomic) IBOutlet UILabel *lblComment;
@property (weak, nonatomic) IBOutlet UIView *transparentView;

//Successfully Delivered view outlets
@property (weak, nonatomic) IBOutlet UILabel *successfullyDeliveredLabel;

//Delivered View outlets

@property (nonatomic, strong) NSMutableArray *deliveredOrdersImages;
@property (nonatomic, assign) CGFloat heightForDesc;

@property (assign, nonatomic) NSString *packId;
@property (strong, nonatomic) PackDetailsModel *packDetails;

//Outlets
@property (weak, nonatomic) IBOutlet UILabel *sendingLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *fromAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *toAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *packageStutusLabel;
@property (weak, nonatomic) IBOutlet UILabel *milesLabel;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryTime;

@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;


//Courier related outlets
@property (weak, nonatomic) IBOutlet UILabel *courierNameLabel;

//Constraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelDeliveryViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *scrollViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *receiverViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *receiverView;

//Version1.5
@property (weak, nonatomic) IBOutlet UIView *topBarPackView;
@property (weak, nonatomic) IBOutlet UIImageView *topbarPackImage;
@property (strong, nonatomic) IBOutlet GoogleMapHelper *mapView;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UILabel *pickupLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryAlphabetLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) MediaUploadingViewCell *mediaCell;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *addImagesButton;
@property (weak, nonatomic) IBOutlet UIButton *btnPickup;
@property (weak, nonatomic) IBOutlet UIButton *btnNotGiven;
@property (weak, nonatomic) IBOutlet UIButton *recieverCallButton;
@property (weak, nonatomic) IBOutlet UILabel *packOwnerName;
@property (weak, nonatomic) IBOutlet UILabel *deliveryDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *packNotDeliveredLabel;
@property (weak, nonatomic) IBOutlet UIView *confirmView;

@property (weak, nonatomic) IBOutlet UIScrollView *packImagesScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIView *packValueView;
@property (weak, nonatomic) IBOutlet UIView *packBoughtView;
@property (weak, nonatomic) IBOutlet UILabel *payByCourierPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *courierWaitTimeLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelPackButton;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;

@property (weak, nonatomic) IBOutlet UIButton *cancelCButton;
@property (weak, nonatomic) IBOutlet UIButton *showMapButton;

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *packValueViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *packBoughtViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmButtonTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerHightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dotedViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deliveredTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descHeightConstraint;


@end
