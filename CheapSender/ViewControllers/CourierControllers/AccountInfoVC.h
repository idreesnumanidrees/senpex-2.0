//
//  AccountInfoVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface AccountInfoVC : BaseViewController

@property (weak, nonatomic) IBOutlet UIView *accountView;
@property (weak, nonatomic) IBOutlet UILabel *lblBalance;
@property (weak, nonatomic) IBOutlet UIImageView *accountImageView;
@property (weak, nonatomic) IBOutlet UILabel *cardNumber;
@property (weak, nonatomic) IBOutlet UILabel *estimatedNextDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *verifiedLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *estimatedDateTopConstraint;

@property (weak, nonatomic) IBOutlet UIButton *addAccountButton;

@end
