//
//  MyDeliveryCourierVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "GoogleMapHelper.h"
#import "LocationManagerService.h"
#import "UpdateLocationPeriodically.h"

//#import <MapKit/MapKit.h>

@interface MyDeliveryCourierVC : BaseViewController <GMSMapViewDelegate, LocationManagerServiceDelegate, UpdateLocationPeriodicallyDelegate>

//UIButton Outlets
@property (weak, nonatomic) IBOutlet UIButton *btnGoOffline;
@property (weak, nonatomic) IBOutlet UIButton *btnNew;
@property (weak, nonatomic) IBOutlet UIButton *btnActive;
@property (weak, nonatomic) IBOutlet UIButton *btnCompleted;

@property (weak, nonatomic) IBOutlet UILabel *lblEmptyTasks;


//Property for mapView
@property (strong, nonatomic) IBOutlet GoogleMapHelper *mapView;
@property (nonatomic, strong) NSMutableArray *markerArray;

//LocationManagerService instance variable
@property (strong, nonatomic) LocationManagerService *locationManagerService;

//LocationManagerService instance variable
@property (strong, nonatomic) UpdateLocationPeriodically *updateLocationPeriodically;
@property (strong, nonatomic) CLLocation *userLocation;

@property (assign, nonatomic) BOOL isStoppedWork;
@property (assign, nonatomic) BOOL isWorkedFirst;

@property (weak, nonatomic) IBOutlet UIView *listEmptyView;

//UIView Outlets
@property (weak, nonatomic) IBOutlet UIView *waitingResponseView;
@property (weak, nonatomic) IBOutlet UIView *goOffView;

//TableView outlets
@property (weak, nonatomic) IBOutlet UITableView *ordersTableView;
@property (nonatomic, assign) int cellCount;
@property (weak, nonatomic) IBOutlet UITableView *activeTableView;

//Web related
@property (strong, nonatomic) NSMutableArray *activePacks;
@property (strong, nonatomic) NSMutableArray *completedPacks;
@property (strong, nonatomic) NSMutableArray *NewPacks;

@property (assign, nonatomic) BOOL moreLoadOn;
@property (assign, nonatomic) int startCount;

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderTableViewTopConstraint;

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *refreshNewOrderBottomConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *refreshRightConstraint;


//Version 1.5
@property (weak, nonatomic) IBOutlet UIButton *confirmMilesButton;
@property (weak, nonatomic) IBOutlet UIButton *selectRangeButton;
@property (weak, nonatomic) IBOutlet UIView *selectRangeContainer;




@end
