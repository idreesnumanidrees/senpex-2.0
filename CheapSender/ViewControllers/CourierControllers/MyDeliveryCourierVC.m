//
//  MyDeliveryCourierVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "MyDeliveryCourierVC.h"
#import "OrdersTableCell.h"
#import "CourierActiveCell.h"
#import "MyDeliverySelection.h"
#import "CourierListData.h"
#import "MilesSelectionCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

static int timerValue = 60;

@interface MyDeliveryCourierVC ()

@property (nonatomic, assign) MyDeliverySelection myDeliverySelection;
@property (nonatomic, assign) MyDeliverySelection myDeliverySelectionSubState;
@property (strong, nonatomic) NSMutableArray *orderCells;
@property (strong, nonatomic) NSMutableArray *packageIdsThatAreOnDeleteQueue;
@property (nonatomic) int milesCourierGetOrders;
//@property (nonatomic, strong) AVAudioPlayer *player;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic) int selectedIndex;
@property (nonatomic) int confirmOrderIndex;

@property (nonatomic) BOOL isWentToDisappear;
@property (nonatomic) BOOL isOffscreen;

//MARK: -Version1.5
@property (strong, nonatomic) NSMutableArray *milesArrayFromDic;

@property (weak, nonatomic) IBOutlet UIButton *refreshButton;


@end

@implementation MyDeliveryCourierVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [UIApplication sharedApplication].statusBarHidden = NO;
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    [self prepareUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //    if (_NewPacks.count > 0) {
    //
    //        if (_myDeliverySelection == GoOnline) {
    //
    //            if (_myDeliverySelectionSubState == New) {
    //                if (_NewPacks.count > 0) {
    //                    [_player play];
    //                }
    //            }
    //        }
    //    }
    
    if (_isWentToDisappear) {
        [self firstSetUp];
    } else {
        _isWentToDisappear = YES;
    }
    
    _isOffscreen = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    _isOffscreen = YES;
    //    [_player pause];
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
        //Commented by riyas and idrees
        // _orderCells = [NSMutableArray array];
    }
    return self;
}

#pragma mark --
#pragma mark -- Helpers

- (void)prepareUI
{
    //MARK- 1.5
    //    NSString *soundFilePath = [NSString stringWithFormat:@"%@/Timer-sound.mp3",[[NSBundle mainBundle] resourcePath]];
    //    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    //
    //    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    //    _player.numberOfLoops = -1; //Infinite
    //    [_player play];
    //    [_player pause];
    
    _packageIdsThatAreOnDeleteQueue = [NSMutableArray new];
    //Add observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(courierActiveListUpdated:)
                                                 name:COURIER_ACTIVE_LIST_UPDATED object:nil];
    
    //Session key from UserDefaults
    if ([AppDelegate sharedAppDelegate].sessionKey == nil) {
        
        NSString *sessionKey = [UtilHelper getValueFromNSUserDefaultForKey:SESSION_KEY];
        [AppDelegate sharedAppDelegate].sessionKey = sessionKey;
    }
    
    self.btnGoOffline = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnGoOffline withRadius:12.5 color:[UIColor grayColor] width:0];
    
    self.goOffView = (UIView *)[UtilHelper makeCornerCurved:(UIView *)self.goOffView withRadius:5.0 color:[UIColor whiteColor] width:0];
    self.waitingResponseView = (UIView *)[UtilHelper makeCornerCurved:(UIView *)self.waitingResponseView withRadius:22.5 color:[UIColor whiteColor] width:0];
    
    self.selectRangeContainer = (UIView *)[UtilHelper makeCornerCurved:(UIView *)self.selectRangeContainer withRadius:5 color:[UIColor whiteColor] width:0];
    self.selectRangeButton = (UIButton *)[UtilHelper makeCornerCurved:(UIButton *)self.selectRangeButton withRadius:22.5 color:[UIColor whiteColor] width:0];
    
    self.btnActive = (UIButton *)[UtilHelper makeCornerCurved:(UIButton *)self.btnActive withRadius:15.0 color:[UIColor whiteColor] width:0];
    self.btnCompleted = (UIButton *)[UtilHelper makeCornerCurved:(UIButton *)self.btnCompleted withRadius:15.0 color:[UIColor whiteColor] width:0];
    self.btnNew = (UIButton *)[UtilHelper makeCornerCurved:(UIButton *)self.btnNew withRadius:15.0 color:[UIColor whiteColor] width:0];
    
    self.goOffView.hidden = YES;
    _myDeliverySelection = GoOnline;
    
    self.markerArray = [NSMutableArray new];
    self.ordersTableView.hidden = YES;
    self.activeTableView.hidden = YES;
    self.listEmptyView.hidden = YES;
    [self performSelector:@selector(intializaServices) withObject:nil afterDelay:0.2];
    
    //Add observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopUpdateLocationAndTimer)
                                                 name:STOP_TIMER_AND_LOCATION_UPDATOR object:nil];
    
    //Add observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cancelOrderCallFromTableCell:)
                                                 name:ORDER_CANCEL_FROM_NEW_PACKS object:nil];
    
    _markerArray = [NSMutableArray new];
    self.selectRangeButton.hidden = YES;
    self.waitingResponseView.hidden = YES;
    
    //Radious settings
    _milesCourierGetOrders = [[UtilHelper getUserFromNSUserDefaultForKey:@"distance_mile"] intValue];
    _selectedIndex = [[UtilHelper getUserFromNSUserDefaultForKey:@"miles_index"] intValue];
    
    if (_milesCourierGetOrders == 0) {
        
        _milesCourierGetOrders = 5;
        _selectedIndex = 1;
    }
    
    if ([AppDelegate sharedAppDelegate].allDictData == nil) {
        [self getAllDict];
    }
    
    //New change 07-07-2018
    _confirmOrderIndex = 0;
    
    if (IS_IPHONE_5 || IS_IPHONE_4_OR_LESS) {
        
        _refreshRightConstraint.constant = 3;
    }
    
    _refreshButton.hidden = YES;
    
    [self firstSetUp];
}

- (void)intializaServices
{
    //Calls current location manager class initial method
    [self initializeLocationManagerService];
    [self initializeUpdateLocationPeriodically];
    
    _cellCount = 0;
    
    //Initialize the mapview
    [self initializeMapView];
    //    [self firstSetUp];
}

- (void)firstSetUp
{
    int courierWorkAvailabilityStatus = [[UtilHelper getValueFromNSUserDefaultForKey:COURIER_AVAILABLE_STATUS] intValue];
    
    if(courierWorkAvailabilityStatus == 1)
    {
        self.btnGoOffline.backgroundColor = ONLINE_BUTTON_SELECTION;
        [self.btnGoOffline setTitle:@"Go offline" forState:UIControlStateNormal];
        _myDeliverySelection = GoOnline;
        [self showHideViews];
        _isStoppedWork = NO;
        _refreshButton.hidden = NO;
        
        if (_myDeliverySelection == GoOnline) {
            
            [self newTapSelection];
            [self requestCourierPacks:_myDeliverySelectionSubState];
        }
    }
    else
    {
        self.waitingResponseView.hidden = YES;
        self.selectRangeButton.hidden = YES;
        self.btnGoOffline.selected = YES;
        self.btnGoOffline.backgroundColor = OFFLINE_BUTTON_SELECTION;
        [self.btnGoOffline setTitle:@"Start" forState:UIControlStateNormal];
        _myDeliverySelection = GoOffline;
        [self showHideViews];
        _isStoppedWork = YES;
        _refreshButton.hidden = YES;
        
        [_NewPacks removeAllObjects];
        [_activePacks removeAllObjects];
        [_completedPacks removeAllObjects];
        
        //        [_player pause];
    }
}

- (void)courierActiveListUpdated:(NSNotification *) notification
{
    if (_myDeliverySelection == GoOnline) {
        
        if (_myDeliverySelectionSubState == New) {
            
            if (_myDeliverySelection == GoOnline) {
                
                [self newTapSelection];
                [self requestCourierPacks:_myDeliverySelectionSubState];
            }
        }
        else if (_myDeliverySelectionSubState == Active)
        {
            if (_myDeliverySelection == GoOnline) {
                
                [self activeTapSelection];
                [self requestCourierPacks:_myDeliverySelectionSubState];
            }
        }
        else if (_myDeliverySelectionSubState == Completed)
        {
            if (_myDeliverySelection == GoOnline) {
                
                [self completedTapSelection];
                [self requestCourierPacks:_myDeliverySelectionSubState];
            }
        }
    }
}

//Initializing and allocating periodicWebCallManager class
- (void)initializeMapView
{
    //Assigning delegates
    self.mapView.delegate = self;
    [self.mapView initWithCustomStyle];
}

- (void)drawCircle {
    
    CLLocationCoordinate2D location =  [AppDelegate sharedAppDelegate].coureirLocation.coordinate;
    
    [self.mapView clear];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = location;
    marker.icon = [UIImage imageNamed:@"currentLocation-marker"];
    marker.map = self.mapView;
    
    CLLocationDistance radius = (_milesCourierGetOrders * 1609.34);
    GMSCircle* circle = [GMSCircle circleWithPosition: location radius: radius];
    circle.fillColor = [UIColor colorWithRed:17.0/255.0 green:167.0/255.0 blue:170.0/255.0 alpha:0.2];
    circle.strokeColor = [UIColor colorWithRed:17.0/255.0 green:167.0/255.0 blue:170.0/255.0 alpha:0.7];
    circle.strokeWidth = 2;
    circle.map = self.mapView ;
}

- (void)stopUpdateLocationAndTimer
{
    [_locationManagerService stopUpdatingLocation];
    [_updateLocationPeriodically stopUpdatePendingPacks];
    [_updateLocationPeriodically stopUpdateHistoricalLocation];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

//Initializing and allocating current location manager class
- (void)initializeLocationManagerService
{
    //Allocating and Initializing LocationManagerService class
    _locationManagerService = [LocationManagerService new];
    
    //Assigning delegates of LocationManagerService
    _locationManagerService.locationManagerDelagete = self;
    
    //Initialize setup method for CLLocation
    [_locationManagerService initialize];
}

- (void)notifyOnGPSDisable
{
    //    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Location Services Disabled!"
    //                                                        message:@"Please enable Location Based Services"
    //                                                       delegate:self
    //                                              cancelButtonTitle:@"Settings"
    //                                              otherButtonTitles:@"Cancel", nil];
    //
    //    //TODO if user has not given permission to device
    //     alertView.tag = 1002;
    //    [alertView show];
}


#pragma mark
#pragma mark -- LocationManagerService Delegates

- (void)notifyToMapViewWithLocation:(CLLocation *)currentLocation;
{
    _userLocation = currentLocation;
    [AppDelegate sharedAppDelegate].coureirLocation = currentLocation;
    if (!_isWorkedFirst) {
        
        [_mapView showUserLocation:_userLocation];
        
        //        [self notifyForUpdatePendingPacksMap];
        
        self.mapView.camera = [GMSCameraPosition cameraWithTarget:currentLocation.coordinate zoom:9];
        [self updateMiles];
        
        _isWorkedFirst = YES;
        [self updateUserGeoLocation];
    }
}

//Initializing and allocating current location manager class
- (void)initializeUpdateLocationPeriodically
{
    //Allocating and Initializing LocationManagerService class
    _updateLocationPeriodically = [UpdateLocationPeriodically new];
    
    //Assigning delegates of LocationManagerService
    _updateLocationPeriodically.updateLocationPeriodicallyDelegate = self;
    
    //Initialize setup method for CLLocation
    [_updateLocationPeriodically startUpdateLocationThread];
    [_updateLocationPeriodically startUpdatePendingPacks];
}

#pragma mark
#pragma mark -- UpdateLocationPeriodically Delegates

- (void)notifyForLocationUpdate
{
    [self updateUserGeoLocation];
}

- (void)updateUserGeoLocation
{
    [self drawCircle];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient UpdateUserGeoLocation:[NSString stringWithFormat:@"%f",self->_userLocation.coordinate.latitude]
                                         UserLng:[NSString stringWithFormat:@"%f",self->_userLocation.coordinate.longitude]
                                   LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                     LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                    LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                   LogDeviceType:LOG_DEVICE_TYPE
                               completionHandler:^(BOOL result, NSError *error) {
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       
                                   });
                               }];
    });
}

- (void)notifyForUpdatePendingPacksMap
{
    //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    //
    //        [WebServicesClient CourierGetPendingPacks:@"0"
    //                                            count:@"100"
    //                                      LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
    //                                     LogUserAgent:LOG_DEVICE_TYPE
    //                                    LogDeviceType:[[UIDevice currentDevice] systemVersion]
    //                                       SessionKey:[AppDelegate sharedAppDelegate].sessionKey
    //                                completionHandler:^(CourierPacksList *courierPackList, NSError *error) {
    //
    //                                    dispatch_async(dispatch_get_main_queue(), ^{
    //
    //                                        if (!error) {
    //
    //                                            if (courierPackList.data.count > 0) {
    //
    //                                                for (CourierListData *courierData in courierPackList.data) {
    //
    //                                                    CSUser *marker = [CSUser new];
    //                                                    marker.userId = [courierData.dataIdentifier intValue];
    //                                                    marker.imageName = @"Sender-marker-icon";
    //                                                    marker.latitude = [courierData.packFromLat doubleValue];
    //                                                    marker.longitude = [courierData.packFromLng doubleValue];
    //                                                    marker.marker = [_mapView addMapPinForUser:marker];
    //                                                    [_markerArray addObject:marker];
    //                                                }
    //
    //                                                //GMSCoordinateBounds represents a rectangular bounding box on the Earth's surface.
    //                                                GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    //
    //                                                for (CSUser *user in self.markerArray) {
    //
    //                                                    //Returns a GMSCoordinateBounds representing the current bounds extended to include the passed-in coordinate
    //                                                    if((user.latitude != 0 && user.longitude != 0)) {
    //                                                        bounds = [bounds includingCoordinate:user.marker.position];
    //                                                    }
    //                                                }
    //                                                _mapView.padding = UIEdgeInsetsMake(140, 0, 200, 0);
    //                                                [self.mapView showAllMarkersInMapViewWithBound:bounds];
    //                                            }
    //                                        }
    //                                    });
    //                                }];
    //    });
}

#pragma mark --
#pragma mark -- IBActions //*****************************************************Start Tabs **********

- (IBAction)refreshNewOrdersTapped:(id)sender {
    
    [self newOrdersRequest];
}

- (IBAction)selectRangeTapped:(id)sender {
    
    _refreshButton.hidden = YES;
    if ([AppDelegate sharedAppDelegate].allDictData != nil) {
        
        _milesArrayFromDic = [self getMilesArray];
        
        if (_milesArrayFromDic.count > 0) {
            
            self.selectRangeButton.hidden = YES;
            self.selectRangeContainer.hidden = NO;
            
            [_collectionView reloadData];
        }
    }
}

- (IBAction)confirmMilesTapped:(id)sender {
    
    self.selectRangeContainer.hidden = YES;
    self.selectRangeButton.hidden = NO;
    [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:_milesCourierGetOrders] forKey:@"distance_mile"];
    
    [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:_selectedIndex] forKey:@"miles_index"];
    
    [self showProgressHud];
    [self updateMiles];
    
}

- (void)updateMiles
{
    _refreshButton.hidden = NO;
    [WebServicesClient UpdateNearestDistance:[NSString stringWithFormat:@"%i", _milesCourierGetOrders]
                               logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                 LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                LogUserAgent:[[UIDevice currentDevice] systemVersion]
                               LogDeviceType:LOG_DEVICE_TYPE
                           completionHandler:^(BOOL isUpdated, NSError *error) {
                               [self hideProgressHud];
                               if (!error) {
                                   [self drawCircle];
                               }
                           }];
}

- (IBAction)cancelMileTapped:(id)sender {
    self.selectRangeContainer.hidden = YES;
    self.selectRangeButton.hidden = NO;
    _refreshButton.hidden = NO;
}

- (IBAction)goOfflineAction:(UIButton *)sender {
    
    [self showProgressHud];
    
    if (!_isStoppedWork) {
        
        //Currently working. now need to stop working.
        _myDeliverySelectionSubState = New;
        self.activeTableView.hidden = YES;
        self.waitingResponseView.hidden = YES;
        [self topButtonsStateChanged:_btnNew];
        [self requestForUpdateAvailStatus:@"2"];
        //        [_player pause];
        
    } else {
        
        [self requestForUpdateAvailStatus:@"1"];
    }
}

- (IBAction)newAction:(UIButton *)sender {
    
    [self newOrdersRequest];
}

- (void)newOrdersRequest
{
    //    if (_NewPacks.count > 0) {
    //        [_player play];
    //    }else {
    //        [_player pause];
    //    }
    if (_myDeliverySelection == GoOnline) {
        
        _refreshButton.hidden = NO;
        [self newTapSelection];
        [self showProgressHud];
        [self requestCourierPacks:_myDeliverySelectionSubState];
    }
}

- (void)newTapSelection
{
    
    _startCount = 0;
    if(_NewPacks!= nil)
    {
        //        [_NewPacks removeAllObjects];
        //        [self.ordersTableView reloadData];
    }
    else
    {
        _NewPacks = [NSMutableArray new];
    }
    
    _selectRangeButton.hidden = YES;
    _selectRangeContainer.hidden =YES;
    
    if (_NewPacks.count == 0) {
        
        _selectRangeButton.hidden = NO;
        self.refreshNewOrderBottomConstraint.constant = 22;
    }
    
    _myDeliverySelectionSubState = New;
    [self topButtonsStateChanged:_btnNew];
    [self showHideViews];
}

- (IBAction)activeAction:(UIButton *)sender {
    
    //    [_player pause];
    if (_myDeliverySelection == GoOnline) {
        
        _refreshButton.hidden = YES;
        _selectRangeButton.hidden = YES;
        _selectRangeContainer.hidden =YES;
        [self activeTapSelection];
        [self showProgressHud];
        [self requestCourierPacks:_myDeliverySelectionSubState];
    }
}

- (void)activeTapSelection
{
    _startCount = 0;
    _activePacks = nil;
    _activePacks = [NSMutableArray new];
    _myDeliverySelectionSubState = Active;
    //NSLog(@"reloaddata 409");
    
    [self.activeTableView reloadData];
    
    [self topButtonsStateChanged:_btnActive];
    [self showHideViews];
}

- (IBAction)completedAction:(UIButton *)sender {
    
    //    [_player pause];
    if (_myDeliverySelection == GoOnline) {
        
        _refreshButton.hidden = YES;
        _selectRangeButton.hidden = YES;
        _selectRangeContainer.hidden =YES;
        [self completedTapSelection];
        [self showProgressHud];
        [self requestCourierPacks:_myDeliverySelectionSubState];
    }
}

- (void)completedTapSelection
{
    _startCount = 0;
    _completedPacks = nil;
    _completedPacks = [NSMutableArray new];
    _myDeliverySelectionSubState = Completed;
    // NSLog(@"reloaddata 431");
    
    [self.activeTableView reloadData];
    
    [self topButtonsStateChanged:_btnCompleted];
    [self showHideViews];
}

- (void)topButtonsStateChanged:(UIButton *)sender
{
    self.btnNew.selected = NO;
    self.btnActive.selected = NO;
    self.btnCompleted.selected = NO;
    
    self.btnNew.backgroundColor = [UIColor clearColor];
    self.btnActive.backgroundColor = [UIColor clearColor];
    self.btnCompleted.backgroundColor = [UIColor clearColor];
    
    sender.selected = !sender.selected;
    sender.backgroundColor = [UIColor colorWithRed:91.0/255.0 green:106.0/255.0 blue:134.0/255.0 alpha:1.0f];
    
}

- (void)showHideViews
{
    if (_myDeliverySelection == GoOnline) {
        
        self.listEmptyView.hidden = YES;
        
        if (_myDeliverySelectionSubState == New) {
            
            self.activeTableView.hidden = YES;
            self.ordersTableView.hidden = NO;
            self.mapView.hidden = NO;
            self.goOffView.hidden = YES;
        }
        else if (_myDeliverySelectionSubState == Active || _myDeliverySelectionSubState == Completed)
        {
            self.selectRangeButton.hidden = YES;
            self.activeTableView.hidden = NO;
            self.ordersTableView.hidden = YES;
            self.mapView.hidden = YES;
            self.goOffView.hidden = YES;
            
            if (_myDeliverySelectionSubState == Active) {
                
                if (_activePacks.count == 0) {
                    
                    self.listEmptyView.hidden = NO;
                    self.lblEmptyTasks.text = @"List of confirmed tasks is empty.";
                }
            }
            
            if (_myDeliverySelectionSubState == Completed) {
                
                if (_completedPacks.count == 0) {
                    
                    self.listEmptyView.hidden = NO;
                    self.lblEmptyTasks.text = @"List of completed tasks is empty.";
                }
            }
        }
    } else {
        
        self.listEmptyView.hidden = YES;
        self.activeTableView.hidden = YES;
        self.ordersTableView.hidden = YES;
        self.mapView.hidden = NO;
        self.goOffView.hidden = NO;
    }
}

#pragma mark //*****************************************************End Tabs ***********
#pragma mark
#pragma mark - TableView Delegates //***************************************************************

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_myDeliverySelectionSubState == New) {
        
        _cellCount = (int)_NewPacks.count;
        _startCount = _cellCount;
        
        if (_cellCount > 0) {
            
            self.waitingResponseView.hidden = YES;
            self.selectRangeButton.hidden = YES;
            self.ordersTableView.hidden = NO;
            
        } else {
            
            if (_myDeliverySelection == GoOnline) {
                
                self.waitingResponseView.hidden = NO;
                self.selectRangeButton.hidden = NO;
                
            } else {
                
                self.waitingResponseView.hidden = YES;
                self.selectRangeButton.hidden = YES;
            }
            
            self.ordersTableView.hidden = YES;
        }
        
        if (_cellCount == 1) {
            
            self.refreshNewOrderBottomConstraint.constant = 235;
            
            if (IS_IPHONE_X) {
                
                self.orderTableViewTopConstraint.constant = 355;
            }
            else if (IS_IPHONE_6P) {
                
                self.orderTableViewTopConstraint.constant = 320;
                
            } else if (IS_IPHONE_5) {
                
                self.orderTableViewTopConstraint.constant = 170;
                
            } else if (IS_IPHONE_4_OR_LESS) {
                
                self.orderTableViewTopConstraint.constant = 82;
                
            } else {
                
                self.orderTableViewTopConstraint.constant = 265;
            }
            
        } else if (_cellCount > 1) {
            
            self.orderTableViewTopConstraint.constant = 10;
            self.refreshNewOrderBottomConstraint.constant = 22;
        }
        
    } else if (_myDeliverySelectionSubState == Active) {
        
        _cellCount = (int)_activePacks.count;
        _startCount = _cellCount;
        
    }else if (_myDeliverySelectionSubState == Completed) {
        
        _cellCount = (int)_completedPacks.count;
        _startCount = _cellCount;
    }
    
    NSLog(@"_cellcount %d",_cellCount);
    return _cellCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.ordersTableView) {
        
        //OrdersTableCell *orderCell = nil;
        
        //        if ([self.orderCells count] > indexPath.row)
        //        {
        //            orderCell = [self.orderCells objectAtIndex:indexPath.row];
        //
        //        } else {
        
        static NSString *cellIdentifier = @"OrdersTableCell";
        OrdersTableCell *orderCell = (OrdersTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        CourierListData *courierListData = _NewPacks[indexPath.row];
        
        //NSLog(@"courierListData %p  indexpath.row %d",&courierListData, indexPath.row);
        //Commented by riyas and idrees
        //[self.orderCells addObject:orderCell];
        orderCell.tag = (int)courierListData.dataIdentifier;
        
        if(courierListData.isPackCancelled == NO)
        {
            int interval = [self comapreTwoDates:[NSDate date] savedDate:courierListData.dateTime];
            int finalInterval = timerValue - interval;
            [orderCell startTimer:courierListData.dataIdentifier
                    timerInterval:finalInterval];
            
            orderCell.successfullView.hidden = YES;
            orderCell.anotherCourierView.hidden = YES;
            orderCell.cancelPackageView.hidden = YES;
            orderCell.topView.hidden = NO;
            orderCell.bottomView.alpha = 1.0;
            orderCell.cancelButton.hidden = YES;
            orderCell.confirmButton.hidden = NO;
            
            orderCell.userName.text = courierListData.sendName;
            orderCell.senderFromLabel.text = courierListData.packFromText;
            orderCell.senderToLabel.text = courierListData.packToText;
            orderCell.milesLabel.text = [NSString stringWithFormat:@"%@ miles", courierListData.distance];
            orderCell.priceLabel.text = [NSString stringWithFormat:@"$%@", courierListData.courierProfit];
            orderCell.packLabel.text = courierListData.sendCatText;
            
            if (courierListData.tariffText != nil || ![courierListData.tariffText isEqualToString:@""]) {
                
                orderCell.statusLabel.text = [NSString stringWithFormat:@"%@", courierListData.tariffText];
                
            } else if (courierListData.takenTime != nil){
                
                //Version(1.5)
                NSDate *deliveryDate = [NSDate dateFromDateString:courierListData.takenTime];
                orderCell.statusLabel.text = [deliveryDate getTimeFromDateWithoutDayAndUTC];
            }
            
            orderCell.confirmButton.tag = indexPath.row;
            orderCell.cancelButton.tag = indexPath.row;
            
            orderCell.openExternalMapsFrom.tag = indexPath.row;
            orderCell.openExternalMapsTo.tag = indexPath.row;
            
            [orderCell.openExternalMapsFrom addTarget:self action:@selector(openExternalMapsFromTapped:) forControlEvents:UIControlEventTouchUpInside];
            [orderCell.openExternalMapsTo addTarget:self action:@selector(openExternalMapsToTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            [orderCell.confirmButton addTarget:self action:@selector(confirmOrderAction:) forControlEvents:UIControlEventTouchUpInside];
            [orderCell.cancelButton addTarget:self action:@selector(cancelOrderAction:) forControlEvents:UIControlEventTouchUpInside];
            
            //Icon for status type
            if ([courierListData.sendCatId isEqualToString:@"2"] || [courierListData.sendCatId isEqual:@2]) {
                
                orderCell.packImageView.image = [UIImage imageNamed:@"pack-icon"];
                
            } else if ([courierListData.sendCatId isEqualToString:@"1"] || [courierListData.sendCatId isEqual:@1]) {
                
                orderCell.packImageView.image = [UIImage imageNamed:@"ups-status"];
                
            } else if ([courierListData.sendCatId isEqualToString:@"3"] || [courierListData.sendCatId isEqual:@3]) {
                
                orderCell.packImageView.image = [UIImage imageNamed:@"Store-status"];
                
            }else if ([courierListData.sendCatId isEqualToString:@"4"] || [courierListData.sendCatId isEqual:@4]) {
                
                orderCell.packImageView.image = [UIImage imageNamed:@"return-services"];
            }
            
            //Pack Status color
            if ([courierListData.packStatus isEqualToString:@"10"]) {
                
                orderCell.topView.hidden = NO;
            }
            else if ([courierListData.packStatus isEqualToString:@"100"]) {
                
                orderCell.anotherCourierView.hidden = NO;
                orderCell.bottomView.alpha = 0.4;
            }
            
            //Shows indicatore on imageView
            [orderCell.profileImageView setShowActivityIndicatorView:YES];
            [orderCell.profileImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, courierListData.packOwnerSelfImage];
            
            //Loads image
            [orderCell.profileImageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
        }
        else
        {
            //package is already sent for cancellation
            //Change the view of the cell
            
            orderCell.successfullView.hidden = YES;
            orderCell.anotherCourierView.hidden = YES;
            orderCell.topView.hidden = YES;
            orderCell.cancelButton.hidden = YES;
            orderCell.confirmButton.hidden = YES;
            orderCell.cancelPackageView.hidden = NO;
            orderCell.bottomView.alpha = 0.4;
        }
        return orderCell;
        
    } else {
        
        static NSString *cellIdentifier = @"CourierActiveCell";
        CourierActiveCell *activeCell = (CourierActiveCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        CourierListData *courierListData;
        
        if (_myDeliverySelectionSubState == Active) {
            
            courierListData = _activePacks[indexPath.row];
            
        } else if (_myDeliverySelectionSubState == Completed) {
            
            courierListData = _completedPacks[indexPath.row];
        }
        activeCell.senderTextLabel.text = courierListData.sendName;
        activeCell.senderFromLabel.text = courierListData.packFromText;
        activeCell.senderToLabel.text = courierListData.packToText;
        activeCell.priceLabel.text = [NSString stringWithFormat:@"$%@",courierListData.courierProfit];
        activeCell.packStatusLabel.text = courierListData.statusCourier;
        activeCell.milesLabel.text = [NSString stringWithFormat:@"%@ miles", courierListData.distance];
        
        //Version(1.5)
        if (_myDeliverySelectionSubState == Completed) {
            
            activeCell.timeLabel.textColor = [UIColor grayColor];
            NSDate *deliveryDate = [NSDate dateFromDateString:courierListData.lastOperationTimeC];
            activeCell.timeLabel.text = [deliveryDate getDateTimeInLocalTimeZone];
            
        } else {
            
            activeCell.timeLabel.textColor = [UIColor grayColor];
            NSDate *deliveryDate = [NSDate dateFromDateString:courierListData.insertedDate];
            activeCell.timeLabel.text = [deliveryDate getDateTimeInLocalTimeZone];
            
            //            NSDate *takenDate = [NSDate dateFromDateString:courierListData.takenTime];
            //            if ([courierListData.takenAsap isEqualToString:@"1"] || [courierListData.takenAsap isEqual:@1]) {
            //
            //                activeCell.timeLabel.text = @"Urgent";
            //                activeCell.timeLabel.textColor = YELLOW_COLOR;
            //
            //            } else {
            //                NSString *timeString = [takenDate getTimeFromDateWithoutDayAndUTC];
            //                //        _takenDateLabel.text = [takenDate getTimeDayFromDate];
            //                activeCell.timeLabel.text = timeString;
            //            }
        }
        
        //Shows indicatore on imageView
        [activeCell.imgPackImage setShowActivityIndicatorView:YES];
        [activeCell.imgPackImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSString *imageUrl = courierListData.packImg;
        
        NSString *imageExtension = [UtilHelper getImageFileType:imageUrl];
        
        NSString *finalUrl = [NSString stringWithFormat:@"%@%@.thumb.%@", DOWNLOAD_BASE_IMAGE_URL, imageUrl,imageExtension];
        
        //Loads image
        [activeCell.imgPackImage sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"packing-Icon"] options:SDWebImageRefreshCached];
        
        //Pack status colors
        activeCell.packStatusLabel.textColor = ORANGE_LINE_COLOR;
        
        if ([courierListData.packStatus isEqualToString:@"10"] || [courierListData.packStatus isEqualToString:@"0"]) {
            
            activeCell.packStatusLabel.textColor = ORANGE_LINE_COLOR;
        }
        else if ([courierListData.packStatus isEqualToString:@"20"] || [courierListData.packStatus isEqualToString:@"40"] || [courierListData.packStatus isEqualToString:@"30"]) {
            
            activeCell.packStatusLabel.textColor = PACK_GREEN_STATUS_COLOR;
        }
        else if ([courierListData.packStatus isEqualToString:@"100"] || [courierListData.packStatus isEqualToString:@"90"]) {
            
            activeCell.packStatusLabel.textColor = PACK_RED_STATUS_COLOR;
        }
        return activeCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.ordersTableView) {
        
        return 230;
        
    } else {
        
        return 140;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CourierListData *courierListData;
    
    if (_myDeliverySelectionSubState == Active) {
        
        courierListData = _activePacks[indexPath.row];
        
    } else if (_myDeliverySelectionSubState == Completed) {
        
        courierListData = _completedPacks[indexPath.row];
        
    } else if (_myDeliverySelectionSubState == New) {
        
        courierListData = _NewPacks[indexPath.row];
    }
    
    [[AppDelegate sharedAppDelegate].appController showCourierSendingsDetail:courierListData.dataIdentifier];
    
    if (tableView == self.ordersTableView) {
        
        _isOffscreen = YES;
        
    } else {
        
        //        [[AppDelegate sharedAppDelegate].appController showCourierSendingsDetail:courierListData.dataIdentifier];
        _isWentToDisappear = NO;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetY = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height;
    
    if (_myDeliverySelectionSubState != New) {
        
        if (offsetY > contentHeight - 2500) {
            
            if (_moreLoadOn) {
                
                _moreLoadOn = NO;
                [self requestCourierPacks:_myDeliverySelectionSubState];
            }
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    } else if (alertView.tag == 1002){
        
        //        if(buttonIndex == 0)//Settings button pressed
        //        {
        //                //This will open ios devices location settings
        //                NSString* url = @"prefs:root=LOCATION_SERVICES";
        //
        //                if (@available(iOS 9, *)) {
        //
        //                    url = @"App-Prefs:root=Privacy&path=LOCATION";
        //                }
        //
        //                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: url]];
        //        }
    } else if (alertView.tag == 89) {
        
        if (buttonIndex == 1) {
            
            [self confirmOrder];
        }
    }
}

- (void)cancelOrderCallFromTableCell:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.object;
    
    for (CourierListData *courierListData in _NewPacks) {
        
        if ([courierListData.dataIdentifier intValue] == [userInfo[@"Tag"] intValue]) {
            
            //            [PackageDB deletePackage:[courierListData.dataIdentifier intValue]];
            [self cancelOrder:courierListData];
            //          [_NewPacks removeObject:courierListData];
        }
    }
}

- (void)confirmOrderAction:(UIButton *)sender
{
    
    _isWentToDisappear = NO;
    _isOffscreen = YES;
    
    CourierListData *courierListData = _NewPacks[sender.tag];
    [[AppDelegate sharedAppDelegate].appController showCourierSendingsDetail:courierListData.dataIdentifier];
    
    //    _confirmOrderIndex = (int)sender.tag;
    //
    //    NSString *message = @"Did you review all order detail?";
    //
    //    if ([courierListData.sendCatId isEqualToString:@"3"] || [courierListData.sendCatId isEqual:@3]) {
    //
    //        message = @"A driver has to pay for an item. He/she will be reimbursed for purchasing when Senpex will provide payment for delivery. Please confirm. Then";
    //    }
    //
    //    [UtilHelper showApplicationAlertWithMessage:message
    //                                   withDelegate:self
    //                                        withTag:89
    //                              otherButtonTitles:@[@"No", @"Yes"]];
}

- (void)confirmOrder {
    
    CourierListData *courierListData = _NewPacks[_confirmOrderIndex];
    [PackageDB deletePackage:[courierListData.dataIdentifier intValue]];
    [self showProgressHud];
    
    [WebServicesClient BeCourierOfPack:courierListData.dataIdentifier
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                            SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                     completionHandler:^(BOOL isPackCancelled, NSError *error) {
                         
                         [self hideProgressHud];
                         
                         if (!error) {
                             
                             [self->_NewPacks removeObject:courierListData];
                             //NSLog(@"reloaddata 793");
                             [self.ordersTableView reloadData];
                             
                             //                             if (self->_NewPacks.count == 0) {
                             //                                 [self->_player pause];
                             //                             }
                         }
                         else if (error.code == 3) {
                             
                             [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                         }
                         else if (error.code == 14) {
                             
                             [UtilHelper showApplicationAlertWithMessage:@"Sorry, another driver has already confirmed to pick up. Shortly, you will get new orders."
                                                            withDelegate:self
                                                                 withTag:0
                                                       otherButtonTitles:@[@"Ok"]];
                             
                             [self->_NewPacks removeObject:courierListData];
                             [self.ordersTableView reloadData];
                         }
                         else {
                             
                             [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                         }
                     }];
}

- (void)cancelOrderAction:(UIButton *)sender
{
    CourierListData *courierListData = _NewPacks[sender.tag];
    //  [PackageDB deletePackage:[courierListData.dataIdentifier intValue]];
    [self cancelOrder:courierListData];
}

- (void)cancelOrder:(CourierListData *)courierListData
{
    // [self showProgressHud];
    
    //5003
    if(![_packageIdsThatAreOnDeleteQueue containsObject:courierListData.dataIdentifier])
    {
        courierListData.isPackCancelled = YES;
        
        // [self showProgressHud];
        //Add this packageId to the array to understand that this package is being deleted.
        [_packageIdsThatAreOnDeleteQueue addObject:courierListData.dataIdentifier];
        if(_myDeliverySelectionSubState == New)
        {
            [self.ordersTableView reloadData];
        }
        
        NSLog(@"cancel order package id %@", courierListData.dataIdentifier);
        [WebServicesClient CancelPackageRequest:courierListData.dataIdentifier
                                    LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                   LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                  LogDeviceType:LOG_DEVICE_TYPE
                                     SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                              completionHandler:^(BOOL isPackCancelled, NSError *error) {
                                  
                                  //[self hideProgressHud];
                                  
                                  if (!error) {
                                      
                                      //Add this packageId to the array to understand that this package is being deleted.
                                      [self->_packageIdsThatAreOnDeleteQueue removeObject:courierListData.dataIdentifier];
                                      
                                      [self->_NewPacks removeObject:courierListData];
                                      [PackageDB deletePackage:[courierListData.dataIdentifier intValue]];
                                      // [_NewPacks removeObject:packData];
                                      
                                      //NSLog(@"reloaddata 832");
                                      if(_myDeliverySelectionSubState == New)
                                      {
                                          //                                          if (self->_NewPacks.count == 0) {
                                          //                                              [self->_player pause];
                                          //                                          }
                                          [self.ordersTableView reloadData];
                                      }
                                      
                                  }
                                  else if (error.code == 3) {
                                      
                                      [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                      
                                  } else {
                                      
                                      [self->_packageIdsThatAreOnDeleteQueue removeObject:courierListData.dataIdentifier];
                                      courierListData.isPackCancelled = NO;
                                      if(self->_myDeliverySelectionSubState == New)
                                      {
                                          [self.ordersTableView reloadData];
                                      }
                                      
                                      NSLog(@"cacnelled order issue  %@",courierListData.dataIdentifier);
                                      
                                      NSLog(@"error message %@",error.domain);
                                  }
                              }];
    }
    else
    {
        NSLog(@"this package is already send for deletion  %@",courierListData.dataIdentifier);
        
    }
}

- (void)openExternalMapsFromTapped:(UIButton *)sender
{
    CourierListData *courierListData = _NewPacks[sender.tag];
    float lat = [courierListData.packFromLat floatValue];
    float lng = [courierListData.packFromLng floatValue];
    [self openMapsSheetWithLat:lat andLng:lng];
}

- (void)openExternalMapsToTapped:(UIButton *)sender
{
    CourierListData *courierListData = _NewPacks[sender.tag];
    float lat = [courierListData.packToLat floatValue];
    float lng = [courierListData.packToLng floatValue];
    [self openMapsSheetWithLat:lat andLng:lng];
}

#pragma mark--
#pragma mark-- Server Side handling //***************************************************************

- (void)requestForUpdateAvailStatus:(NSString *)status
{
    [WebServicesClient UpdateUserAvailStatus:status
                               LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                 LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                LogUserAgent:[[UIDevice currentDevice] systemVersion]
                               LogDeviceType:LOG_DEVICE_TYPE
                           completionHandler:^(BOOL result, NSError *error) {
                               
                               [self hideProgressHud];
                               
                               if (!error) {
                                   
                                   //                                   self.btnGoOffline.selected = !self.btnGoOffline.selected;
                                   
                                   if ([self.btnGoOffline.titleLabel.text isEqualToString:@"Go offline"]) {
                                       
                                       _refreshButton.hidden = YES;
                                       self.btnGoOffline.backgroundColor = OFFLINE_BUTTON_SELECTION;
                                       [self.btnGoOffline setTitle:@"Start" forState:UIControlStateNormal];
                                       _myDeliverySelection = GoOffline;
                                       [self showHideViews];
                                       _isStoppedWork = YES;
                                       
                                       [_NewPacks removeAllObjects];
                                       [_activePacks removeAllObjects];
                                       [_completedPacks removeAllObjects];
                                       
                                       //NSLog(@"reloaddata 879");
                                       [_ordersTableView reloadData];
                                       [_activeTableView reloadData];
                                       
                                       self.waitingResponseView.hidden = YES;
                                       self.selectRangeButton.hidden = YES;
                                       self.selectRangeContainer.hidden = YES;
                                       
                                       [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:0]
                                                                                 forKey:COURIER_AVAILABLE_STATUS];
                                       
                                   } else {
                                       
                                       _refreshButton.hidden = NO;
                                       [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:1]
                                                                                 forKey:COURIER_AVAILABLE_STATUS];
                                       
                                       self.btnGoOffline.backgroundColor = ONLINE_BUTTON_SELECTION;
                                       [self.btnGoOffline setTitle:@"Go offline" forState:UIControlStateNormal];
                                       
                                       _myDeliverySelection = GoOnline;
                                       [self showHideViews];
                                       _isStoppedWork = NO;
                                       
                                       //By default new orders should be set as selected when courier turns online.
                                       self.selectRangeContainer.hidden = YES;
                                       
                                       if (_NewPacks.count == 0) {
                                           
                                           self.waitingResponseView.hidden = NO;
                                           self.selectRangeButton.hidden = NO;
                                       }
                                       
                                       dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                                           
                                           if (_myDeliverySelection == GoOnline) {
                                               
                                               [self newTapSelection];
                                               [self requestCourierPacks:_myDeliverySelectionSubState];
                                           }
                                       });
                                   }
                               }
                               else if (error.code == 3) {
                                   
                                   [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                   
                               } else {
                                   
                                   [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                               }
                           }];
}

- (void)requestCourierPacks:(MyDeliverySelection)courierPack
{
    [WebServicesClient CourierGetPacksList:[NSString stringWithFormat:@"%i", _startCount]
                                     count:@"10"
                                      List:courierPack
                               LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                              LogUserAgent:[[UIDevice currentDevice] systemVersion]
                             LogDeviceType:LOG_DEVICE_TYPE
                                SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                         completionHandler:^(CourierPacksList *courierPackList, NSError *error) {
                             
                             if (!error) {
                                 
                                 if (self->_myDeliverySelectionSubState == courierPack) {
                                     
                                     [self hideProgressHud];
                                     
                                     if (courierPackList.data.count > 0 && courierPackList!= nil) {
                                         
                                         self->_moreLoadOn = YES;
                                         self.listEmptyView.hidden = YES;
                                         
                                         NSMutableArray *packs = [NSMutableArray new];
                                         
                                         if (self->_myDeliverySelectionSubState == Active) {
                                             
                                             [packs addObjectsFromArray:self->_activePacks];
                                             [packs addObjectsFromArray:courierPackList.data];
                                             self->_activePacks = [packs mutableCopy];
                                             //NSLog(@"reloaddata 952");
                                             
                                             [self->_activeTableView reloadData];
                                             
                                             if (self->_activePacks.count == 0) {
                                                 
                                                 self.listEmptyView.hidden = NO;
                                                 self.lblEmptyTasks.text = @"List of confirmed tasks is empty.";
                                             }
                                             
                                         }
                                         else if (self->_myDeliverySelectionSubState == Completed) {
                                             
                                             [packs addObjectsFromArray:self->_completedPacks];
                                             [packs addObjectsFromArray:courierPackList.data];
                                             self->_completedPacks = [packs mutableCopy];
                                             //NSLog(@"reloaddata 962");
                                             
                                             [self->_activeTableView reloadData];
                                             
                                             if (self->_completedPacks.count == 0) {
                                                 
                                                 self.listEmptyView.hidden = NO;
                                                 self.lblEmptyTasks.text = @"List of completed tasks is empty.";
                                             }
                                         }
                                         else if (self->_myDeliverySelectionSubState == New) {
                                             
                                             self->_NewPacks = [(NSMutableArray *)courierPackList.data mutableCopy];
                                             
                                             //Commented by riyas and idrees
                                             //                                         for (CourierListData *courierListData in _NewPacks) {
                                             //
                                             //                                             for (OrdersTableCell *orderCell in _orderCells) {
                                             //
                                             //                                                 if (orderCell.tag != (int)courierListData.dataIdentifier) {
                                             //                                                     [_orderCells removeObject:orderCell];
                                             //                                                     break;
                                             //                                                 }
                                             //                                             }
                                             //                                         }
                                             
                                             //DataBase operations
                                             NSMutableArray *dbPakcages = [PackageDB getPackageList];
                                             
                                             if (dbPakcages.count > 0) {
                                                 
                                                 for (CourierListData *packData in self->_NewPacks) {
                                                     
                                                     BOOL isFound = YES;
                                                     for (PackageInfo *packInfo in dbPakcages) {
                                                         
                                                         if ([packData.dataIdentifier intValue] == packInfo.packId) {
                                                             
                                                             isFound = YES;
                                                             break;
                                                             
                                                         } else {
                                                             
                                                             isFound = NO;
                                                         }
                                                     }
                                                     
                                                     if (isFound == NO) {
                                                         
                                                         [PackageDB insertAndupdateLastPackage:[packData.dataIdentifier intValue]
                                                                                    timerValue:timerValue
                                                                                      dateTime:[NSDate date]
                                                                                      IsUpdate:NO];
                                                     }
                                                 }
                                                 
                                                 NSMutableArray *dbPakcages2 = [PackageDB getPackageList];
                                                 
                                                 for (PackageInfo *packInfo in dbPakcages2) {
                                                     
                                                     for (CourierListData *packData in self->_NewPacks) {
                                                         
                                                         if ([packData.dataIdentifier intValue] == packInfo.packId) {
                                                             
                                                             int interval = [self comapreTwoDates:[NSDate date] savedDate:packInfo.dateTime];
                                                             
                                                             int finalInterval = timerValue - interval;
                                                             
                                                             if (finalInterval > 0) {
                                                                 
                                                                 packData.timeInterval = finalInterval;
                                                                 packData.dateTime = packInfo.dateTime;
                                                                 
                                                             } else {
                                                                 
                                                                 [self cancelOrder:packData];
                                                                 //                                                             [PackageDB deletePackage:packInfo.packId];
                                                                 //                                                             [_NewPacks removeObject:packData];
                                                             }
                                                             
                                                             break;
                                                         }
                                                     }
                                                 }
                                             } else {
                                                 
                                                 for (CourierListData *packData in _NewPacks) {
                                                     
                                                     [PackageDB insertAndupdateLastPackage:[packData.dataIdentifier intValue]
                                                                                timerValue:timerValue
                                                                                  dateTime:[NSDate date]
                                                                                  IsUpdate:NO];
                                                     packData.timeInterval = timerValue;
                                                     packData.dateTime = [NSDate date];
                                                 }
                                             }
                                             //NSLog(@"reloaddata 1056");
                                             
                                             //                                             if (_NewPacks.count > 0) {
                                             //
                                             //                                                 if (!_isOffscreen) {
                                             //                                                     [_player play];
                                             //                                                 }
                                             //                                             }else {
                                             //                                                 [_player pause];
                                             //                                             }
                                             
                                             [_ordersTableView reloadData];
                                         }
                                     }
                                     else if (_myDeliverySelectionSubState == New)
                                     {
                                         //NSLog(@"reloaddata 1063");
                                         
                                         if(courierPackList.data.count == 0)
                                         {
                                             [_NewPacks removeAllObjects];
                                         }
                                         
                                         [_ordersTableView reloadData];
                                     }
                                 }
                                 
                             }
                             else {
                                 
                                 [self hideProgressHud];
                                 
                                 if (error.code == 3) {
                                     
                                     [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                 }
                             }
                         }];
}

- (int)comapreTwoDates:(NSDate *)date savedDate:(NSDate *)savedDate
{
    NSTimeInterval seconds = [date timeIntervalSinceDate:savedDate];
    return (int)seconds;
}


//MARK - Version 1.5

- (NSMutableArray *)getMilesArray
{
    NSMutableArray *milesArray = [NSMutableArray new];
    
    if ([AppDelegate sharedAppDelegate].allDictData != nil) {
        
        for (GetDictByName *getDictByName in [AppDelegate sharedAppDelegate].allDictData) {
            
            if ([getDictByName.dicShort isEqualToString:@"near_miles"]) {
                
                [milesArray addObject:getDictByName.listId];
            }
        }
    }
    return milesArray;
}


#pragma mark UICollectionView Methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _milesArrayFromDic.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    MilesSelectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MilesSelectionCell" forIndexPath:indexPath];
    
    cell.milesButton.selected = NO;
    cell.milesLabel.text = [NSString stringWithFormat:@"%@ miles", _milesArrayFromDic[indexPath.row]];
    
    if (_selectedIndex == indexPath.row) {
        cell.milesButton.selected = YES;
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedIndex = (int)indexPath.row;
    _milesCourierGetOrders = [_milesArrayFromDic[indexPath.row] intValue];
    [_collectionView reloadData];
}


@end
