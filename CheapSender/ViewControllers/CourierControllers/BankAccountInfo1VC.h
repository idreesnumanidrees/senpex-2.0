//
//  BankAccountInfo1VC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface BankAccountInfo1VC : BaseViewController <UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UIButton *btnNext;

//TextField outlets

@property (strong, nonatomic) BankAccountDetail *bankAccountDetails;


@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfCity;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfLine1;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfLine2;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfState;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfPostalCode;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfDate;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfRouting;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfAccountNumber;

@property (strong, nonatomic) UIDatePicker *picker;

@end
