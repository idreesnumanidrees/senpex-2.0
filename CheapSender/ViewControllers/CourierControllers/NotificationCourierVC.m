//
//  NotificationCourierVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "NotificationCourierVC.h"
#import "NotificationListCell.h"

@interface NotificationCourierVC ()

//Web related
@property (assign, nonatomic) BOOL moreLoadOn;
@property (assign, nonatomic) int startCount;
@property (strong, nonatomic) NSMutableArray *notifications;
@property (strong, nonatomic) NSMutableDictionary *heightAtIndexPath;

@end

@implementation NotificationCourierVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareUI
{
    self.heightAtIndexPath = [NSMutableDictionary new];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 100; //Set this to any value that works for you.
    
    self.tableView.hidden = YES;
    [self showProgressHud];
}

- (void)viewWillAppear:(BOOL)animated
{
    _startCount = 0;
    [self requestToNotificationList:YES];
    
    [[[AppDelegate sharedAppDelegate].appController.courierTabbar.tabBar.items objectAtIndex:3] setBadgeValue:nil];
}

#pragma mark
#pragma mark - TableView Data source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSInteger count = _notifications.count;
    _startCount = (int)count;
    
    if (count > 0) {
        
        self.tableView.hidden = NO;
        self.emptyNotificationView.hidden = YES;
        
    } else {
        
        self.tableView.hidden = YES;
        self.emptyNotificationView.hidden = NO;
    }
    
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"NotificationListCell";
    NotificationListCell *cell = (NotificationListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.nextButton.hidden = YES;
    cell.dotView.hidden = YES;
    cell.dateLeadingConstraint.constant = 90;
    
    Notification *notification = _notifications[indexPath.row];
    cell.lblMessage.text = notification.notText;
    
    NSDate *notiDate = [NSDate dateFromDateString:notification.insertedDate];
    
    NSString *stringDate = [UtilHelper time:notiDate];
    cell.lblDate.text = stringDate;
    
    if ([notification.isRead isEqualToString:@"0"]) {
        
        //        cell.nextButton.hidden = NO;
        //        cell.dotView.hidden = NO;
        //        cell.dateLeadingConstraint.constant = 104;
        
    } else {
        
        cell.lblDate.textColor = [UIColor colorWithRed:161.0f/255.0f green:161.0f/255.0f blue:161.0f/255.0f alpha:1.0f];
    }
    
    //Change placeholder image based on the notification type
//    if([notification.linkModule isEqualToString:@"view_chat_message"])
//    {
//        cell.profileImageView.image = [UIImage imageNamed:@"profilePhoto"];
//    }
//    else if([notification.linkModule isEqualToString:@"from_admin"])
//    {
//        cell.profileImageView.image = [UIImage imageNamed:@"AppIcon"];
//    }
//    else
//    {
//        cell.profileImageView.image = [UIImage imageNamed:@"notificationIconUnSelected"];
//    }
    
    if ([notification.linkModule isEqualToString:@"view_pack"] || [notification.linkModule isEqualToString:@"new_pack"]) {
        
        cell.profileImageView.image = [UIImage imageNamed:@"courier-view-pack-nots"];
    }
    else if ([notification.linkModule isEqualToString:@"view_bank_account"]) {
        
        cell.profileImageView.image = [UIImage imageNamed:@"view_bank_account-nots"];
    }
    else if([notification.linkModule isEqualToString:@"from_admin"])
    {
        cell.profileImageView.image = [UIImage imageNamed:@"AppIcon"];
    }
    else if ([notification.linkModule isEqualToString:@"view_payout_details"]) {
        
        cell.profileImageView.image = [UIImage imageNamed:@"view_payout_details-nots"];
    }
    else if ([notification.linkModule isEqualToString:@"view_chat_message"]) {
        
        cell.profileImageView.image = [UIImage imageNamed:@"profilePhoto"];
    }else
    {
        cell.profileImageView.image = [UIImage imageNamed:@"notificationIconUnSelected"];
    }
    
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *height = @(cell.frame.size.height);
    [self.heightAtIndexPath setObject:height forKey:indexPath];
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Notification *notification = _notifications[indexPath.row];
    
    if ([notification.linkEnabled isEqualToString:@"1"]) {
        
        if ([notification.linkModule isEqualToString:@"view_pack"] || [notification.linkModule isEqualToString:@"new_pack"]) {
            
            [[AppDelegate sharedAppDelegate].appController showCourierSendingsDetail:notification.linkId];
        }
        else if ([notification.linkModule isEqualToString:@"view_bank_account"]) {
            
            [[AppDelegate sharedAppDelegate].appController showAccountInfoView];
        }
        else if ([notification.linkModule isEqualToString:@"view_payout_details"]) {
            
            [[AppDelegate sharedAppDelegate].appController showWithdrawHistoryView];
        }
        else if ([notification.linkModule isEqualToString:@"view_chat_message"]) {
            
            CourierConversations *courier = [CourierConversations new];
            courier.receiverId = notification.userId;
            courier.viewId = notification.linkId;
            [[AppDelegate sharedAppDelegate].appController showChatCourierDetails:courier PackDetails:nil];
        }
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient MarkNotAs:notification.internalBaseClassIdentifier
                              IsRead:@"1"
                       logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                         LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                        LogUserAgent:[[UIDevice currentDevice] systemVersion]
                       LogDeviceType:LOG_DEVICE_TYPE
                   completionHandler:^(BOOL isRead, NSError *error) {
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           
                           if (!error) {
                               
                               notification.isRead = @"1";
                               [_tableView reloadData];
                               
                           } else {
                               
                               if (error.code == 3) {
                                   
                                   [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                               }
                           }
                       });
                   }];
    });
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetY = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height;
    
    if (offsetY > contentHeight - 2000) {
        
        if (_moreLoadOn) {
            
            _moreLoadOn = NO;
            [self requestToNotificationList:NO];
        }
    }
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestToNotificationList:(BOOL)isCallingfromViewWill
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetUserNotifications:@"-1"
                                         Status:@"-1"
                                          Count:@"10"
                                          Start:[NSString stringWithFormat:@"%d", _startCount]
                                    LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                   LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                  LogDeviceType:LOG_DEVICE_TYPE
                                     SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                              completionHandler:^(NSMutableArray *notificationList, NSError *error) {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      [self hideProgressHud];
                                      
                                      if (!error) {
                                          
                                          if (notificationList.count > 0) {
                                              
                                              if (isCallingfromViewWill) {
                                                  _notifications = [NSMutableArray new];
                                              }
                                              
                                              _moreLoadOn = YES;
                                              NSMutableArray *serverRatings = [NSMutableArray new];
                                              [serverRatings addObjectsFromArray:_notifications];
                                              [serverRatings addObjectsFromArray:notificationList];
                                              _notifications = [serverRatings mutableCopy];
                                              [_tableView reloadData];
                                          }
                                          
                                      } else {
                                          
                                          if (error.code == 3) {
                                              
                                              [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                          }
                                      }
                                  });
                              }];
    });
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
    }
}


@end
