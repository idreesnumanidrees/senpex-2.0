//
//  BankAccountInfo2VC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BankAccountInfo2VC.h"

@interface BankAccountInfo2VC ()

@end

@implementation BankAccountInfo2VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark--
#pragma mark -- Helpers

- (void)prepareUI
{
     self.btnNext = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnNext withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
}

#pragma mark--
#pragma mark -- IBActions

- (IBAction)nextAction:(id)sender {
    
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
