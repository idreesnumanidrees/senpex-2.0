//
//  CourierRatingsVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierRatingsVC.h"
#import "CourierRatingsCell.h"

@interface CourierRatingsVC ()

@property (weak, nonatomic) IBOutlet UILabel *rateCountLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UITableView *paymentTableView;

//Web related
@property (assign, nonatomic) BOOL moreLoadOn;
@property (assign, nonatomic) int startCount;
@property (strong, nonatomic) NSMutableArray *ratings;

@property (weak, nonatomic) IBOutlet UILabel *totalDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *thisMonthLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratedLabel;

@end

@implementation CourierRatingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareUI
{
    _startCount = 0;
    _ratings = [NSMutableArray new];
    
    [self showProgressHud];
    [self requestForRatedCourierList];
    [self requestToUserinfo];
}

#pragma mark --
#pragma mark -- TableView Delegate and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _ratings.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CourierRatingsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CourierRatingsCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    RateUser *rateUser = _ratings[indexPath.row];
    
    cell.ratingView.value = [rateUser.userRate floatValue];
    cell.lblRatingCount.text = rateUser.userRate;
    cell.name.text = rateUser.name;
    
    //Shows indicatore on imageView
    [cell.profileImageView setShowActivityIndicatorView:YES];
    [cell.profileImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *imageUrl = rateUser.selfImg;
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, imageUrl];
    
    //Loads image
    [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 61.0;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetY = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height;
    
    if (offsetY > contentHeight - 2000) {
        
        if (_moreLoadOn) {
            
            _moreLoadOn = NO;
            _startCount += 10;
            [self requestForRatedCourierList];
        }
    }
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestForRatedCourierList
{
    
    if (![UtilHelper isNetworkAvailable]) {
        [self hideProgressHud];
        return;
    }
    
    [WebServicesClient GetUsersRates:[NSString stringWithFormat:@"%d", _startCount]
                               Count:@"10"
                       logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                         LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                        LogUserAgent:[[UIDevice currentDevice] systemVersion]
                       LogDeviceType:LOG_DEVICE_TYPE
                   completionHandler:^(NSMutableArray *courierRateList, NSError *error) {
                       
                       [self hideProgressHud];
                       
                       if (!error) {
                           
                           if (courierRateList.count > 0) {
                               
                               _moreLoadOn = YES;
                               NSMutableArray *serverRatings = [NSMutableArray new];
                               [serverRatings addObjectsFromArray:_ratings];
                               [serverRatings addObjectsFromArray:courierRateList];
                               _ratings = [serverRatings mutableCopy];
                               [_paymentTableView reloadData];
                           }
                           
                       } else {
                           
                           if (error.code == 3) {
                               
                               [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                           }
                       }
                   }];
}

- (void)requestToUserinfo
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSString *courierId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
        [WebServicesClient GetUserInfoById:courierId
                             LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                               LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                              LogUserAgent:[[UIDevice currentDevice] systemVersion]
                             LogDeviceType:LOG_DEVICE_TYPE
                         completionHandler:^(UserModel *user, NSError *error) {
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 
                                 if (!error) {
                                     
                                     _ratingView.value = [user.userRate floatValue];
                                     _rateCountLabel.text = [NSString stringWithFormat:@"%0.01f",[user.userRate floatValue]];
                                     _ratedLabel.text = [NSString stringWithFormat:@"%0.01f",[user.userRate floatValue]];
                                     
                                 } else {
                                     
                                     if (error.code == 3) {
                                         
                                         [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                         
                                     } else {
                                         
                                         [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                                     }
                                 }
                             });
                         }];
    });
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }
}


@end
