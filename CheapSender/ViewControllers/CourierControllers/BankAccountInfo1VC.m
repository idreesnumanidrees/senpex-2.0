//
//  BankAccountInfo1VC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BankAccountInfo1VC.h"

@interface BankAccountInfo1VC ()

@property (strong, nonatomic) NSArray *textFieldArray;

@property (strong, nonatomic) NSString *dateForServer;

@property (nonatomic, strong)NSDate *pickerSelectedDate;


@end

@implementation BankAccountInfo1VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark--
#pragma mark -- Helpers

- (void)prepareUI
{
    self.btnNext = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnNext withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    
//    self.textFieldArray = @[_tfCity,_tfLine1,_tfLine2,_tfState,_tfPostalCode, _tfDate, _tfRouting, _tfAccountNumber];
    self.textFieldArray = @[_tfCity,_tfLine1,_tfState,_tfPostalCode, _tfDate, _tfRouting, _tfAccountNumber];

    _btnNext.backgroundColor = LOGIN_DISABLE_COLOR;
    _btnNext.selected = YES;
    
    if (_bankAccountDetails != nil) {
        
        _tfCity.text = _bankAccountDetails.city;
        _tfLine1.text = _bankAccountDetails.line1;
        _tfLine2.text = _bankAccountDetails.line1;
        _tfState.text = _bankAccountDetails.state;
        _tfPostalCode.text = _bankAccountDetails.postalCode;
        _dateForServer = _bankAccountDetails.accdate;
        _tfRouting.text = _bankAccountDetails.routeNumber;
        _tfAccountNumber.text = _bankAccountDetails.accountNumber;
        
        NSDate *expiryDate = [NSDate justDateFromDateString:_bankAccountDetails.accdate];
        _pickerSelectedDate = expiryDate;
        
        _tfDate.text = [expiryDate getMMDDYYYYDate];
        
        [_btnNext setTitle:@"Update" forState:UIControlStateNormal];
        [self checkActiveButton:self.tfDate.text];
    }else {
        _pickerSelectedDate = [NSDate date];
    }
}

#pragma mark--
#pragma mark -- IBActions

- (IBAction)nextAction:(id)sender {
    
    if (!_btnNext.selected) {
        
        [self requestToUpdateBankAccount];
    }
    //    [[AppDelegate sharedAppDelegate].appController showBankAccountInfo2View];
}

- (IBAction)selectDateAction:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Select Your Date of Birth \n\n\n\n\n\n\n\n\n" message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    _picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 50, 270, 212)];
    _picker.backgroundColor = [UIColor colorWithRed:232.0/255 green:232.0/255 blue:232.0/255 alpha:1.0];
    [_picker setDatePickerMode:UIDatePickerModeDate];
    [_picker setMaximumDate:[NSDate date]];
    
    if (_pickerSelectedDate != nil) {
        
        [_picker setDate:_pickerSelectedDate];
    }
    
    [alert.view addSubview:_picker];
    UIAlertAction* selectButton = [UIAlertAction
                                   actionWithTitle:@"Select"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       
                                       //Handle your yes please button action here
                                       [self getBirthdayDate];
                                   }];
    
    UIAlertAction* cancelButton = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction * action) {
                                       //Handle no, thanks button
                                   }];
    
    [alert addAction:cancelButton];
    [alert addAction:selectButton];
    
    UIPopoverPresentationController *popoverController = alert.popoverPresentationController;
    popoverController.sourceView = self.view;
    popoverController.sourceRect = [self.view bounds];
    [self presentViewController:alert  animated:YES completion:^{
    }];
    
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select Your Date of Birth" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Select", nil];
//    alert.delegate = self;
//    alert.tag = 100;
//    _picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
//    _picker.backgroundColor = [UIColor colorWithRed:232.0/255 green:232.0/255 blue:232.0/255 alpha:1.0];
//
//    [_picker setDatePickerMode:UIDatePickerModeDate];
//    [_picker setMaximumDate:[NSDate date]];
//    [alert addSubview:_picker];
//    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
//    [alert setValue:_picker forKey:@"accessoryView"];
//    [alert show];
}

- (void)getBirthdayDate
{
    NSDate *date = self.picker.date;
    _pickerSelectedDate = date;
    _dateForServer = [date getYYYYMMDDDate];
    NSString *dateString = [date getMMDDYYYYDate];
    self.tfDate.text = dateString;
    [self checkActiveButton:self.tfDate.text];
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == _tfAccountNumber || textField == _tfRouting) {
        textField.secureTextEntry = NO;
    } 
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    [self checkActiveButton:numberOFCharacter];
    return YES;
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }
    else if (alertView.tag == 999)
    {
        [self backAction:nil];
    }
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)checkActiveButton:(NSString *)numberOFCharacter
{
    BOOL isActive = NO;
    
    for (JVFloatLabeledTextField *tfText in self.textFieldArray) {
        
        if (tfText.text.length > 0) {
            
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
            
        } else {
            
            isActive = NO;
            break;
        }
    }
    
    if (isActive) {
        
        _btnNext.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
        _btnNext.selected = NO;
        
    }else {
        
        _btnNext.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnNext.selected = YES;
    }
}

- (void)requestToUpdateBankAccount
{
    
    [self showProgressHud];
    
    if (![UtilHelper isNetworkAvailable]) {
        [self hideProgressHud];
        return;
    }
    
    [WebServicesClient UpdateBankAccount:self.tfCity.text
                                   Line1:self.tfLine1.text
                                   Line2:@"REMOVED TEMPORARILY"//self.tfLine2.text
                                   State:self.tfState.text
                              PostalCode:self.tfPostalCode.text
                                    Date:_dateForServer
                             RouteNumber:self.tfRouting.text
                           AccountNumber:self.tfAccountNumber.text
                           logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                             LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                            LogUserAgent:[[UIDevice currentDevice] systemVersion]
                           LogDeviceType:LOG_DEVICE_TYPE
                       completionHandler:^(BOOL isInserted, NSError *error) {
                           
                           [self hideProgressHud];
                           
                           if (!error) {
                               
                               [UtilHelper showApplicationAlertWithMessage:@"Bank Account details have been successfully added."
                                                              withDelegate:self withTag:999
                                                         otherButtonTitles:@[@"Ok"]];
                               
                           } else {
                               
                               if (error.code == 3) {
                                   
                                   [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                               }
                               else {
                                   
                                   [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                               }
                           }
                       }];
}

@end
