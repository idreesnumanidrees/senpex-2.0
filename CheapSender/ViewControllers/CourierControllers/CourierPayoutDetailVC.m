//
//  CourierPayoutDetailVC.m
//  CheapSender
//
//  Created by Idrees on 2017/08/27.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierPayoutDetailVC.h"
#import "WithdrawHistoryTableCell.h"

@interface CourierPayoutDetailVC ()
@property (weak, nonatomic) IBOutlet UITableView *paymentTableView;
@property (strong, nonatomic) NSMutableArray *payoutHistory;
@end

@implementation CourierPayoutDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self requestForPayoutHistory];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- TableView Delegate and data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _payoutHistory.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    WithdrawHistoryTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WithdrawHistoryTableCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    PayoutDetails *payout = _payoutHistory[indexPath.row];
    
    cell.price.text = [NSString stringWithFormat:@"$%@", payout.paymentAmount];
    cell.date.text = payout.sendName;
    cell.desription.hidden = YES;
    
//    if ([payout.packStatus isEqualToString:@"0"]) {
//        //draft pack
//        cell.progressStatus.text = @"Draft pack";
//        cell.progressStatus.textColor = ORANGE_LINE_COLOR;
//    }
//
//    //Pack Status color
//    if ([payout.packStatus isEqualToString:@"10"]) {
//        //pending pack
//        cell.progressStatus.text = @"Paid pack";
//        cell.progressStatus.textColor = ORANGE_LINE_COLOR;
//    }
//    else if ([payout.packStatus isEqualToString:@"20"]) {
//        //Courier selected
//        cell.progressStatus.text = @"Courier selected";
//        cell.progressStatus.textColor = ORANGE_LINE_COLOR;
//    }
//    else if ([payout.packStatus isEqualToString:@"30"]) {
//
//        //30 pack taken
//        cell.progressStatus.text = @"Pack is given";
//        cell.progressStatus.textColor = ORANGE_LINE_COLOR;
//    }
//    else if ([payout.packStatus isEqualToString:@"40"]) {
//        //pack delivered
//        cell.progressStatus.text = @"Delivered";
//        cell.progressStatus.textColor = PACK_GREEN_STATUS_COLOR;
//    }
//    else if ([payout.packStatus isEqualToString:@"50"]) {
//        //pack not given
//        cell.progressStatus.text = @"Pack not given";
//        cell.progressStatus.textColor = ORANGE_LINE_COLOR;
//
//    }
//    else if ([payout.packStatus isEqualToString:@"90"]) {
//        // 90 reported courier
//        cell.progressStatus.text = @"Reported pack";
//        cell.progressStatus.textColor = PACK_RED_STATUS_COLOR;
//    }
//    else if ([payout.packStatus isEqualToString:@"100"])
//    {
//        cell.progressStatus.text = @"Cancelled pack";
//        cell.progressStatus.textColor = PACK_RED_STATUS_COLOR;
//
//    }
    
    if ([payout.paymentModule isEqualToString:@"1"]) {
        cell.progressStatus.textColor = PACK_GREEN_STATUS_COLOR;
    } else if ([payout.paymentModule isEqualToString:@"3"]){
        cell.progressStatus.textColor = ORANGE_LINE_COLOR;
    } else {
        cell.progressStatus.textColor = PACK_RED_STATUS_COLOR;
    }
    
    cell.progressStatus.text = payout.paymentModuleText;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestForPayoutHistory
{
    [self showProgressHud];
    [WebServicesClient GetPayoutDetails:_payoutId
                            LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                           LogUserAgent:[[UIDevice currentDevice] systemVersion]
                          LogDeviceType:LOG_DEVICE_TYPE
                             SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                      completionHandler:^(NSMutableArray *payoutDetails, NSError *error) {
                          
                          [self hideProgressHud];
                          
                          if (!error) {
                              
                              self->_payoutHistory = payoutDetails;
                              [self->_paymentTableView reloadData];
                              
                          } else if (error.code == 3) {
                              
                              [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                              
                          } else {
                              
                              [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                          }

                      }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }
}


@end
