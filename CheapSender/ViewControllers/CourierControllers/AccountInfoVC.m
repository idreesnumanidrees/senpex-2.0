//
//  AccountInfoVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "AccountInfoVC.h"

@interface AccountInfoVC ()
@property (strong, nonatomic) BankAccountDetail *bankAccountDetails;

@end

@implementation AccountInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark--
#pragma mark -- Helpers

- (void)prepareUI
{
    self.accountView = [UtilHelper makeCornerCurved:self.accountView withRadius:7.0 color:BORDER_COLOR width:1];
    
    self.estimatedDateTopConstraint.constant = -250;
    self.addAccountButton.hidden = NO;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self requestForGetBankAccount];
}

#pragma mark--
#pragma mark -- IBActions

- (IBAction)addAccountDetailsAction:(id)sender {
    
    if (_bankAccountDetails == nil) {
        
        [[AppDelegate sharedAppDelegate].appController showBankAccountInfo1View:_bankAccountDetails];
    } else {
        
        [[AppDelegate sharedAppDelegate].appController showBankAccountInfo1View:_bankAccountDetails];
    }
}

- (IBAction)editAccountDetailsAction:(id)sender {
    
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestForEstimatedNextPayment
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetEstimatedNextPayment:[AppDelegate sharedAppDelegate].iosDeviceToken
                                      LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                     LogDeviceType:LOG_DEVICE_TYPE
                                        SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                 completionHandler:^(NSDictionary *paymentDate, NSError *error) {
                                     
                                     dispatch_async(dispatch_get_main_queue(), ^{
                                         
                                         if (!error) {
                                             
                                             if (paymentDate != nil) {
                                                 
                                                 self.estimatedNextDateLabel.text = [NSString stringWithFormat:@"Estimated date is %@",paymentDate[@"payment_date"]];
                                                 
                                                 self.lblBalance.text = [NSString stringWithFormat:@"$%@", paymentDate[@"total_payment"]];
                                                 
                                             } else {
                                                 
                                                 self.estimatedNextDateLabel.text = [NSString stringWithFormat:@"Estimated date is not mentioned"];
                                                 self.lblBalance.text = @"$0";
                                             }
                                             
                                         } else {
                                             
                                             if (error.code == 3) {
                                                 
                                                 [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again."
                                                                                withDelegate:self
                                                                                     withTag:1001 otherButtonTitles:@[@"Ok"]];
                                             }
                                             else {
                                                 
                                                 [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                                             }
                                         }
                                     });
                                 }];
    });
}

- (void)requestForGetBankAccount
{
    [self showProgressHud];
    
    if (![UtilHelper isNetworkAvailable]) {
        [self hideProgressHud];
        return;
    }
    
    [WebServicesClient GetBankAccount:[AppDelegate sharedAppDelegate].iosDeviceToken
                         LogUserAgent:[[UIDevice currentDevice] systemVersion]
                        LogDeviceType:LOG_DEVICE_TYPE
                           SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                    completionHandler:^(BankAccountDetail *accountDetails, NSError *error) {
                        
                        [self hideProgressHud];
                        
                        if (!error) {
                            
                            if (accountDetails != nil) {
                                
                                if ((accountDetails.accountNumber != nil) && ![accountDetails.accountNumber isEqualToString:EMPTY_STRING])
                                {
                                    _bankAccountDetails = accountDetails;
                                    self.estimatedDateTopConstraint.constant = 24;
                                    
                                    NSString *cardNumber = [accountDetails.accountNumber substringFromIndex: [accountDetails.accountNumber length] - 4];
                                    
                                    NSString *appendString = [NSString stringWithFormat:@"****%@",cardNumber];
                                    
                                    self.cardNumber.text = appendString;
                                    self.verifiedLabel.text = accountDetails.isVerified;
                                    [self requestForEstimatedNextPayment];
                                    [self animateOnViewUpdated];
                                    
                                    [self.addAccountButton setTitle:@"Edit Bank Account information" forState:UIControlStateNormal];
                                    
//                                    self.addAccountButton.hidden = YES;

                                } else {
                                    
//                                    self.addAccountButton.hidden = NO;
                                }
                            } else {
                                
//                                self.addAccountButton.hidden = NO;
                            }
                            
                        } else {
                            
                            if (error.code == 3) {
                                
                                [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                            }
                            else {
                                
                                [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                            }
                        }
                    }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }
}

- (void)animateOnViewUpdated
{
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

@end
