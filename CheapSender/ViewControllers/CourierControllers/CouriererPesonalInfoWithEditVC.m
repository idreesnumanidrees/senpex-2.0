//
//  CouriererPesonalInfoWithEditVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CouriererPesonalInfoWithEditVC.h"

@interface CouriererPesonalInfoWithEditVC ()
@property (strong, nonatomic) UserModel *user;

@end

@implementation CouriererPesonalInfoWithEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark--
#pragma mark --Helpers

- (void)prepareUI
{
    self.profileImageView = (UIImageView *)[UtilHelper makeCornerCurved:self.profileImageView withRadius:50.0 color:nil width:0];
    
    _tfEmail.userInteractionEnabled = NO;
    _tfPassword.userInteractionEnabled = NO;
    _tfSenderIdCode.userInteractionEnabled = NO;
    _tfSignUpDate.userInteractionEnabled = NO;
    _btnSignUpDate.userInteractionEnabled = NO;
    
    [self requestToLoginUserInfo];
    [self disabledView:NO];
}

- (void)disabledView:(BOOL)isEnabled
{
    _tfFirstName.userInteractionEnabled = isEnabled;
    _tfLastName.userInteractionEnabled = isEnabled;
    _tfMobilePhone.userInteractionEnabled = isEnabled;
    _tfAddress.userInteractionEnabled = isEnabled;
    _btnFemale.userInteractionEnabled = isEnabled;
    _btnMale.userInteractionEnabled = isEnabled;
    _btnChangeProfileImage.userInteractionEnabled = isEnabled;
}

#pragma mark--
#pragma mark --IBActions
- (IBAction)editSaveAction:(id)sender {
    
    self.btnEditSave.selected = !self.btnEditSave.selected;
    [self disabledView:self.btnEditSave.selected];
    
    if (!self.btnEditSave.selected) {
        
        if ([UtilHelper validatePhone:_tfMobilePhone.text]) {
            [self requestForUpdateSender];
        }
        
    }
}

- (IBAction)genderAction:(UIButton *)sender {
    
    sender.selected = YES;
    sender.alpha = 1;
    sender.backgroundColor = [UIColor colorWithRed:17.0/255.0 green:167.0/255.0 blue:170.0/255.0 alpha:1.0f];
    
    if (sender.tag == 100) {
        
        _btnFemale.selected = NO;
        _btnFemale.alpha = 0.41;
        _btnFemale.backgroundColor = [UIColor clearColor];
        _gender = 1;
        
    } else {
        
        _btnMale.selected = NO;
        _btnMale.alpha = 0.41;
        _btnMale.backgroundColor = [UIColor clearColor];
        _gender = 2;
    }
}

- (IBAction)changeProfile:(id)sender
{
    [self.view endEditing:YES];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Media" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library",@"Take Photo",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:self.view];
}

- (IBAction)signUpDateAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select Taken Date" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    alert.delegate = self;
    alert.tag = 100;
    _picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
    
    [_picker setDatePickerMode:UIDatePickerModeDate];
    [_picker setMinimumDate:[NSDate date]];
    [_picker addTarget:self action:@selector(updateTakenDateTime:)
      forControlEvents:UIControlEventValueChanged];
    [alert addSubview:_picker];
    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
    [alert setValue:_picker forKey:@"accessoryView"];
    [alert show];
}

- (IBAction)updateTakenDateTime:(id)sender
{
    [self updateTekenTime];
}

- (void)updateTekenTime
{
    NSDate *date = self.picker.date;
    NSString *timeString = [date getMMDDYYYYDate];
    _tfSignUpDate.text = timeString;
}


#pragma
#pragma  mark action sheet delegate method

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex==1) {
        
//        NSLog(@"Take Photo Button Pressed");
        [self pickMediaFromSource:UIImagePickerControllerSourceTypeCamera];
    }
    else if(buttonIndex==0)
    {
        [self pickMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

#pragma mark -
#pragma mark - AlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
    }
}

#pragma
#pragma  mark camera delegate method
- (void)pickMediaFromSource:(UIImagePickerControllerSourceType)sourceType{
    @autoreleasepool {
        if([UIImagePickerController isSourceTypeAvailable:sourceType])
        {
            
            NSArray *mediaTypes=[UIImagePickerController availableMediaTypesForSourceType:sourceType];
            UIImagePickerController *picker=[[UIImagePickerController alloc]init];
            picker.mediaTypes=mediaTypes;
            picker.delegate=self;
            picker.allowsEditing=YES;
            //            picker.allowsEditing=NO;
            picker.sourceType=sourceType;
            [self presentViewController:picker animated:YES completion:NULL];
            
        }
        else
        {
            [UtilHelper showApplicationAlertWithMessage:@"Camera is not found" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
        }
    }
}

#pragma mark -
#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)])
    {
//        NSLog(@"Image");
        //image
        @autoreleasepool {
            UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
            _profileImageView.image = chosenImage;
            [self requestToUploadSelfImage];
        }
    }
    else
    {
//        NSLog(@"Video");
        //video
        [UtilHelper showApplicationAlertWithMessage:@"Video does not support." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)populateUserdata:(UserModel *)user
{
    _tfFirstName.text = user.name;
    _tfLastName.text = user.surname;
    _tfEmail.text = user.email;
    _tfPassword.text = @"123456";
    _tfMobilePhone.text = user.cell;
    _tfAddress.text = user.addressActual;
    
    NSDate *date = [NSDate dateFromDateString:user.insertedDate];
    _tfSignUpDate.text = [date getMMDDYYYYDate];
    
    _gender = [user.gender integerValue];
    
    if ([user.gender isEqualToString:@"1"]) {
        [self genderAction:_btnMale];
    } else if ([user.gender isEqualToString:@"2"]) {
        [self genderAction:_btnFemale];
    }
    
    [self loadSenderProfileImage:user.selfImg];
}

- (void)loadSenderProfileImage:(NSString *)profileImage
{
    //Shows indicatore on imageView
    [_profileImageView setShowActivityIndicatorView:YES];
    [_profileImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, profileImage];
    
    //Loads image
//    [_profileImageView sd_setImageWithURL:[NSURL URLWithString:finalUrl] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    
      [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
}

- (void)requestToLoginUserInfo
{
    [self showProgressHud];
    [WebServicesClient GetUserInfo:[AppDelegate sharedAppDelegate].sessionKey
                       LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                      LogUserAgent:[[UIDevice currentDevice] systemVersion]
                     LogDeviceType:LOG_DEVICE_TYPE
                 completionHandler:^(UserModel *user, NSError *error) {
                     
                     [self hideProgressHud];
                     
                     if (!error) {
                         
                         _user = user;
                         [self populateUserdata:user];
                         
                     }
                     else if (error.code == 3) {
                         
                         [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                         
                     } else {
                         
                         [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                     }
                 }];
}

- (void)requestForUpdateSender
{
    [self showProgressHud];
    [WebServicesClient UpdateCourierGenInfo:_tfFirstName.text
                                    Surname:_tfLastName.text
                                       Cell:_tfMobilePhone.text
                                    Address:_tfAddress.text
                                     Gender:[NSString stringWithFormat:@"%ld", (long)_gender]
                              LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                               LogUserAgent:[[UIDevice currentDevice] systemVersion]
                              LogDeviceType:LOG_DEVICE_TYPE
                          completionHandler:^(BOOL result, NSError *error) {
                              
                              [self hideProgressHud];
                              
                              if (!error) {
                                  
                                  [CourierInfoDB insertAndupdateCourierWithId:_user.internalBaseClassIdentifier
                                                                         Name:_user.name
                                                                      SurName:_user.surname
                                                                        Email:_user.email
                                                                      Address:_user.addressActual
                                                                    ImageName:EMPTY_STRING
                                                                         Rate:_user.userRate
                                                                      Balance:@"100"
                                                                     IsUpdate:YES];
                              }
                              else if (error.code == 3) {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                  
                              } else {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                              }
                          }];
}

- (void)requestToUploadSelfImage
{
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    NSData* imageData = UIImageJPEGRepresentation(_profileImageView.image, 0.5);
    NSString *image1Base64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString *stringBase = @"data:image/png;base64,";
    NSString *finalBase64 = [stringBase stringByAppendingString:image1Base64];
    
    
    [WebServicesClient SelfImage:finalBase64
                   LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                     LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                    LogUserAgent:[[UIDevice currentDevice] systemVersion]
                   LogDeviceType:LOG_DEVICE_TYPE
               completionHandler:^(NSString *imageName, NSError *error) {
                   
                   [self hideProgressHud];
                   
                   if (!error) {
                       
                       [CourierInfoDB insertAndupdateCourierWithId:_user.internalBaseClassIdentifier
                                                              Name:_user.name
                                                           SurName:_user.surname
                                                             Email:_user.email
                                                           Address:_user.addressActual
                                                         ImageName:imageName
                                                              Rate:_user.userRate
                                                           Balance:@"100"
                                                          IsUpdate:YES];
                       
                   }  else if (error.code == 3) {
                       
                       [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                       
                   } else {
                       
                       [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                   }
               }];
}




@end
