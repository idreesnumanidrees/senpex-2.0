//
//  SignUpCourierVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/09.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SignUpCourierVC.h"

@interface SignUpCourierVC ()

//Outlets
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckbox;

@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftFirstName;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftLastName;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftEmail;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftPassword;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftMobileNumber;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *refranceNameField;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *refrancePhoneField;

//Validation related
@property (nonatomic, strong) NSArray *validationArray;
@property (nonatomic, assign) NSInteger termConditionSelection;

@end

@implementation SignUpCourierVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
    
    
//    _ftFirstName.text = @"Riyas";
//    _ftLastName.text = @"test registration step count";
//    _ftMobileNumber.text = @"122233";
//    _refranceNameField.text = @"sdfsdf";
//    _refrancePhoneField.text = @"123432";
//    _ftEmail.text = @"riyastest33@mailinator.com";
//    _ftPassword.text = @"123456789";
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    
    self.btnNext = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnNext withRadius:_btnNext.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    //Server Related
    _termConditionSelection = 0;
    _btnNext.selected = YES;
    
    [_btnCheckbox setImage:[UIImage imageNamed:@"checkbox-selected"] forState:UIControlStateNormal];
    _termConditionSelection = 1;
    _btnCheckbox.tag = 1;
    
    self.validationArray = @[_ftFirstName,_ftLastName,_ftEmail,_ftPassword,_ftMobileNumber,_refranceNameField,_refrancePhoneField];
    _ftEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
}

- (void)viewWillAppear:(BOOL)animated
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

#pragma mark
#pragma mark --Actions

- (IBAction)checkUnCheckAction:(id)sender {
    NSUInteger tagValue = [sender tag];
    if (tagValue) {
        [sender setImage:[UIImage imageNamed:@"checkbox-non"] forState:UIControlStateNormal];
        _termConditionSelection = 0;
        
    } else {
        [sender setImage:[UIImage imageNamed:@"checkbox-selected"] forState:UIControlStateNormal];
        _termConditionSelection = 1;
    }
    
    [(UIButton *)sender setTag:!tagValue];
    [self checkActiveButton:@"123"];
    
}

- (IBAction)loginAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)openCourierRuleAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showCourierRuleView];
}

- (IBAction)openSignUpIDCardAction:(id)sender {
    
    
    if (_btnNext.selected == NO) {
        
        [self validation];
    }
}

- (IBAction)termConditionAction:(id)sender {
        
    [[AppDelegate sharedAppDelegate].appController showTermsView:@"courier_rule"];
}

 - (IBAction)laterTapped:(id)sender
{
    [[AppDelegate sharedAppDelegate].appController showSignUpIDCardVC:NO];
}


#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    [self checkActiveButton:numberOFCharacter];
    return YES;
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)checkActiveButton:(NSString *)numberOFCharacter
{
    BOOL isActive = NO;
    
    for (JVFloatLabeledTextField *tfText in self.validationArray) {
        
        if (tfText.text.length > 0) {
            
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
            
        } else {
            
            isActive = NO;
            break;
        }
    }
    
    if (_termConditionSelection == 0) {
        
        isActive = NO;
    }
    
    if (isActive) {
        
        _btnNext.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
        _btnNext.selected = NO;
        
    }else {
        
        _btnNext.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnNext.selected = YES;
    }
}

- (void)validation
{
    if (![UtilHelper validateEmail:self.ftEmail.text]) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Please enter valid email" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    else if (![UtilHelper isMatchPasswordLength:self.ftPassword.text]) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Password should be more than 6 characters" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        
    } else {
        
        if (![UtilHelper isNetworkAvailable]) {
            
            [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
        
        [self showProgressHud];
        [self requestToRegisterUser];
    }
}

- (void)requestToRegisterUser
{
    [WebServicesClient RegisterCourierGenInfo:_ftFirstName.text
                                      Surname:_ftLastName.text
                                         Cell:_ftMobileNumber.text
                                  ReferalName:_refranceNameField.text
                                  ReferalCell:_refrancePhoneField.text
                                        Email:_ftEmail.text
                                     Password:_ftPassword.text
                              SendApproveMail:@"0"
                                LogSessionKey:@""
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                            completionHandler:^(NSString *sessionKey, NSString *userId, NSError *error) {
                                
                                [self hideProgressHud];
                                
                                if (!error) {
                                    
                                    [AppDelegate sharedAppDelegate].sessionKey = sessionKey;
                                    [AppDelegate sharedAppDelegate].userId = userId;
                                    
                                    [[AppDelegate sharedAppDelegate].appController showSignUpIDCardVC:NO];
                                    
                                    [self updateCountOfStepsCompletedByCourierInRegistration:@"1"];

                                    
                                    [self getAllDict];
                                    
                                } else {
                                    
                                    if ([error.domain isEqualToString:@"This email is already exist"])
                                    {
                                        
                                         [UtilHelper showApplicationAlertWithMessage:@"This email already exists. Your sign up process is done incompletely. Please press Log in below to continue your courier registration process." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                                        
                                    } else if ([error.domain isEqualToString:@"EmailAlreadyExists"])
                                    {
                                        
                                        [UtilHelper showApplicationAlertWithMessage:@"This email already exists. Your sign up process is done incompletely. Please press Log in below to continue your courier registration process." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                                        
                                    }else {
                                        
                                         [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                                    }
                                }
                            }];
}

@end
