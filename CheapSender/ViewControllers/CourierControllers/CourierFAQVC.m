//
//  CourierFAQVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierFAQVC.h"
#import "FAQTableCell.h"

@interface CourierFAQVC ()

@property (nonatomic, strong) NSArray *faqheaders;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation CourierFAQVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 60; //Set this to any value that works for you.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark--
#pragma mark -- Helpers
- (void)prepareUI
{
    if ([_currentAppUserType isEqualToString:@"2"]) {
        [_topView setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:124.0f/255.0f blue:31.0f/255.0f alpha:1.0f]];
        [_navigationView setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:124.0f/255.0f blue:31.0f/255.0f alpha:1.0f]];
    }
    [self requestForGetFaqHeaders];
}

#pragma mark -
#pragma mark - TableView Delegate and DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.faqheaders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    FAQTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FAQTableCell" forIndexPath:indexPath];
    
    NSDictionary *faqHeader = _faqheaders[indexPath.row];
    cell.name.text = faqHeader[@"faq_name"];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;

    //return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *faqHeader = _faqheaders[indexPath.row];
    if ([_currentAppUserType isEqualToString:@"2"]) {
        [[AppDelegate sharedAppDelegate].appController showCourierPopularQuestionView:faqHeader[@"faq_id"] From:@"Sender"];
    } else {
        [[AppDelegate sharedAppDelegate].appController showCourierPopularQuestionView:faqHeader[@"faq_id"] From:@"Courier"];
    }
    
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestForGetFaqHeaders
{
    [self showProgressHud];
    
    [WebServicesClient GetFaqHeaders:_currentAppUserType
                       logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                         LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                        LogUserAgent:[[UIDevice currentDevice] systemVersion]
                       LogDeviceType:LOG_DEVICE_TYPE
                   completionHandler:^(NSMutableArray *faqHeaders, NSError *error) {
                       
                       [self hideProgressHud];
                       
                       if (!error) {
                           
                           self.faqheaders = faqHeaders;
                           [_tableView reloadData];
                           
                       } else if (error.code == 3) {
                           
                           [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                           
                       } else {
                           
                           [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                       }
                   }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }
}


@end
