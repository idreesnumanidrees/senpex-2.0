//
//  SaveMyRouteView.h
//  CheapSender
//
//  Created by apple on 3/29/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "MVPlaceSearchTextField.h"

@interface SaveMyRouteView : BaseViewController<PlaceSearchTextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *fromPlaceSearchTextField;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *toPlaceSearchTextField;

@property (strong, nonatomic) NSDictionary *routeDict;


@end
