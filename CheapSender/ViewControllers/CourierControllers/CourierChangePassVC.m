//
//  CourierChangePassVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/30.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierChangePassVC.h"

@interface CourierChangePassVC ()
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfCurrentPassword;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfNewPassword;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfReTypeNewPassword;

@property (weak, nonatomic) IBOutlet UIButton *btnSaveChanges;
@property (weak, nonatomic) IBOutlet UIButton *btnForgotPassword;

@property (strong, nonatomic) NSArray *textFieldArray;

@end

@implementation CourierChangePassVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- Helpers

- (void)prepareUI
{
    self.btnSaveChanges = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnSaveChanges withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    
    self.btnForgotPassword = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnForgotPassword withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    
    _btnSaveChanges.selected = YES;
    
    self.textFieldArray = @[_tfCurrentPassword,_tfNewPassword, _tfReTypeNewPassword];
}

#pragma mark --
#pragma mark -- IBActions
- (IBAction)savechangesAction:(id)sender {
    
    if (_btnSaveChanges.selected == NO) {
        
        [self validation];
    }
}


- (IBAction)forgotPasswordAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showResetPasswordView:YES];
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    BOOL isActive = NO;
    
    for (JVFloatLabeledTextField *tfText in self.textFieldArray) {
        
        if (tfText.text.length > 0) {
            
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
            
        } else {
            
            isActive = NO;
            break;
        }
    }
    if (isActive) {
        
        _btnSaveChanges.selected = NO;
        
    }else {
        
        _btnSaveChanges.selected = YES;
    }
    return YES;
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)validation
{
    if (![UtilHelper isMatchPassword:_tfNewPassword.text confirmPassword:_tfReTypeNewPassword.text]) {
        
        [UtilHelper showApplicationAlertWithMessage:@"New password and re-type password should same." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    else {
        
        if (![UtilHelper isNetworkAvailable]) {
            
            [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
        
        [self showProgressHud];
        [self requestToChangePasswod];
    }
}

- (void)requestToChangePasswod
{
    [WebServicesClient ChangePassword2:_tfCurrentPassword.text
                           NewPassword:_tfNewPassword.text
                        ReTypePassword:_tfReTypeNewPassword.text
                         logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                     completionHandler:^(BOOL result, NSError *error) {
                         
                         [self hideProgressHud];
                         
                         if (!error) {
                             
                             [UtilHelper showApplicationAlertWithMessage:@"Password is changed successfully!" withDelegate:self withTag:1002 otherButtonTitles:@[@"Ok"]];
                             
                         }
                         else {
                             
                             if (error.code == 3) {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                 
                             } else {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                             }
                         }
                     }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
    }
    else if(alertView.tag == 1002)
    {
        [self backAction:nil];
    }
}


@end
