//
//  CourierSettingsVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierSettingsVC.h"

@interface CourierSettingsVC ()
@property (weak, nonatomic) IBOutlet UIButton *btnGoogle;
@property (weak, nonatomic) IBOutlet UIButton *btnApple;
@property (weak, nonatomic) IBOutlet UIButton *btnWaze;

@end

@implementation CourierSettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark--
#pragma mark --Helpers

- (void)prepareUI
{
    
}


#pragma mark--
#pragma mark --IBActions
- (IBAction)notificationAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showCourierSettingsNotifyView];
}

- (IBAction)changePassAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showCourierChangePassVC];
}

- (IBAction)MapsSelectionTapped:(UIButton *)sender {
    
    _btnWaze.alpha = 0.4;
    _btnGoogle.alpha = 0.4;
    _btnApple.alpha = 0.4;
    
    sender.alpha = 1.0;

}

@end
