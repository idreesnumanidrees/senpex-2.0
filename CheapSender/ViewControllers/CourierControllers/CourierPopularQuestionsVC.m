//
//  CourierPopularQuestionsVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierPopularQuestionsVC.h"
#import "TermsRulesTableCell.h"

@interface CourierPopularQuestionsVC ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *faqs;
@property (strong, nonatomic) NSMutableDictionary *heightAtIndexPath;

@end

@implementation CourierPopularQuestionsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareUI
{
    self.heightAtIndexPath = [NSMutableDictionary new];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    if ([_from isEqualToString:@"Sender"]) {
        [_topView setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:124.0f/255.0f blue:31.0f/255.0f alpha:1.0f]];
        [_navigationView setBackgroundColor:[UIColor colorWithRed:255.0f/255.0f green:124.0f/255.0f blue:31.0f/255.0f alpha:1.0f]];
    }
    
    [self requestForGetFaq];
}

#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _faqs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TermsRulesTableCell";
    TermsRulesTableCell *cell = (TermsRulesTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSDictionary *ruleDict = _faqs[indexPath.row];
    
    cell.headingLabel.text = ruleDict[@"faq_name"];
    cell.bodyLabel.text = ruleDict[@"faq_text"];
    
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *height = [self.heightAtIndexPath objectForKey:indexPath];
    
    if(height) {
        return height.floatValue;
    } else {
        return UITableViewAutomaticDimension;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *height = @(cell.frame.size.height);
    [self.heightAtIndexPath setObject:height forKey:indexPath];
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestForGetFaq
{
    [self showProgressHud];
    
    [WebServicesClient GetFaqById:_faqId
                    logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                      LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                     LogUserAgent:[[UIDevice currentDevice] systemVersion]
                    LogDeviceType:LOG_DEVICE_TYPE
                completionHandler:^(NSMutableArray *faqFaq, NSError *error) {
                    
                    [self hideProgressHud];
                    
                    if (!error) {
                        
                        if (faqFaq.count > 0) {
                            
                            _faqs = faqFaq;
                            [self.tableView reloadData];
                        }
                        
                    } else if (error.code == 3) {
                        
                        [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                        
                    } else {
                        
                        [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                    }
                }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }
}



@end
