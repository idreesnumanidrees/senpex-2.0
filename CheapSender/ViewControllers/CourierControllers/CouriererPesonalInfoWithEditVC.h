//
//  CouriererPesonalInfoWithEditVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface CouriererPesonalInfoWithEditVC : BaseViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIAlertViewDelegate>

//UIButton outlets
@property (weak, nonatomic) IBOutlet UIButton *btnEditSave;
@property (weak, nonatomic) IBOutlet UIButton *btnMale;
@property (weak, nonatomic) IBOutlet UIButton *btnFemale;
@property (weak, nonatomic) IBOutlet UIButton *btnChangeProfileImage;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUpDate;

@property (assign, nonatomic) NSInteger gender;

//Outlets
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;

//TextField outlets
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfFirstName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfLastName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfEmail;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfPassword;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfMobilePhone;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfAddress;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfSenderIdCode;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfSignUpDate;

@property (strong, nonatomic) UIDatePicker *picker;


@end
