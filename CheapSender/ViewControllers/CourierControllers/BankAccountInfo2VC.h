//
//  BankAccountInfo2VC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface BankAccountInfo2VC : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

//TextField outlets
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfBusinessName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfBusinessTextId;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfDate;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfCountry;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfCity;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfState;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfAddress;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfPostalCode;


@end
