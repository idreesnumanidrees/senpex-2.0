//
//  CourierPersonalInfoVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/29.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierPersonalInfoVC.h"
#import "CourierPersonalInfoCell.h"

@interface CourierPersonalInfoVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) NSArray *contentsArray;
@property (nonatomic, strong) NSArray *contentsImagesArray;

@end

@implementation CourierPersonalInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark -- Helpers

- (void)prepareUI
{
    self.contentsArray = @[@"Profile Details",@"Drivers License",@"Vehicle",@"Vehicle Insurance"];
    self.contentsImagesArray = @[@"user-icon-P",@"id-card-P",@"vehicles-P",@"auto-insurance-P"];

}

#pragma mark --
#pragma mark -- UITableView Delegates and data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1; 
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.contentsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CourierPersonalInfoCell";
    CourierPersonalInfoCell *cell = (CourierPersonalInfoCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];

    cell.lblTitle.text = self.contentsArray[indexPath.row];
    cell.profilePicImageView.image = [UIImage imageNamed:self.contentsImagesArray[indexPath.row]];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        
        [[AppDelegate sharedAppDelegate].appController showCourierPersonalInfoEdit];
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    }
    else if (indexPath.row == 1) {
        
        [[AppDelegate sharedAppDelegate].appController showSignUpIDCardVC:YES];
        
    } else if (indexPath.row == 2) {
        
        [[AppDelegate sharedAppDelegate].appController showVehiclesVC:YES];
        
    }else if (indexPath.row == 3) {
        
        [[AppDelegate sharedAppDelegate].appController showInsuranceCardView:YES];
    }
}

@end
