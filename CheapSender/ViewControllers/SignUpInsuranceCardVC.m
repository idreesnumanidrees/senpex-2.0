//
//  SignUpInsuranceCardVC.m
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SignUpInsuranceCardVC.h"

@interface SignUpInsuranceCardVC () {
//    NSUInteger currentTagValue;
    
}

//Outlets
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *expPeriodTextField;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *policyNoTextField;
@property (strong, nonatomic) IBOutlet UILabel *stepLabel;

//Validation related
@property (nonatomic, strong) NSArray *validationArray;
@property (strong, nonatomic) NSString *deliveryDateTimeForWeb;

//Version1.5
@property (weak, nonatomic) IBOutlet UIButton *crossButton;
@property (weak, nonatomic) IBOutlet UIButton *laterButton;
@property (weak, nonatomic) IBOutlet UIScrollView *innerScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *addPicButton;

@property (strong, nonatomic) NSMutableArray *imagesArray;

//Constraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addPicTopConstraint;

@end

@implementation SignUpInsuranceCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    _frontImageView.hidden = YES;
//    _backImageView.hidden = YES;
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;

    self.btnRegister = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnRegister withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    
    //Web Related
    _validationArray = @[_expPeriodTextField, _policyNoTextField];
    
    _btnRegister.selected = YES;
    
    if (_isComingFromProfile) {
        
        _laterButton.hidden = YES;
        _stepLabel.hidden = YES;
        [self.btnRegister setTitle:@"Update" forState:UIControlStateNormal];
        _headingTopConstraint.constant = 36;
        [self requestToGetUserInsurance];
    }
    else
    {
//        [self updateCountOfStepsCompletedByCourierInRegistration:@"4"];
    }
    
    _imagesArray = [NSMutableArray new];
}

#pragma mark
#pragma mark --Actions

- (IBAction)laterBtnAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showVerificationConfirmationnView];
}

- (IBAction)signUpAction:(id)sender {
    
    if (!_btnRegister.selected) {
        
        [self showProgressHud];
        [self requestToUpdateInsGen];
    }
}

- (IBAction)addImageButtonTapped:(id)sender {
    
    [self.view endEditing:YES];
    
    if (_imagesArray.count < 2) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Media" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library",@"Take Photo",nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [actionSheet showInView:self.view];
        
    } else {
        
        [UtilHelper showApplicationAlertWithMessage:@"Sorry! You can not select more than 2 images" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
    }
}


//Camera Btutton
- (IBAction)cameraAction:(id)sender {
    
//    [self.view endEditing:YES];
//    currentTagValue = [sender tag];
//
//
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Media" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library",@"Take Photo",nil];
//    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
//    [actionSheet showInView:self.view];
    
}

- (IBAction)deleteImageBtnClicked:(id)sender {
    
    [_imagesArray removeObjectAtIndex:_pageControl.currentPage];
    [self.pageControl setNumberOfPages:_imagesArray.count];
    
    [self checkActiveButton:@"123"];
    
    for (UIView *v in _innerScrollView.subviews) {
        if ([v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
    }
    
    if (_imagesArray.count > 0)
    {
        int imagesOffSet = 0;
        for (int i = 0; i < [_imagesArray count]; i++) {
            //We'll create an imageView object in every 'page' of our scrollView.
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(imagesOffSet,0,241, 155)];
            img.layer.masksToBounds = YES;
            img.layer.cornerRadius = 7.0f;
            [self.innerScrollView addSubview:img];
            
            AddImageObject *imageObject = _imagesArray[i];
            if (imageObject.isUploaded == NO) {
                
                img.image = imageObject.image;
                
            } else {
                
                NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, imageObject.imageName];
                [img sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
                imageObject.image = img.image;
            }
            imagesOffSet+=241;
        }
        //Set the content size of our scrollview according to the total width of our imageView objects.
        _innerScrollView.contentSize = CGSizeMake(imagesOffSet, 155);
        
    } else {
        
        self.innerScrollView.hidden = YES;
        self.pageControl.hidden = YES;
        _crossButton.hidden = YES;
        _addPicTopConstraint.constant = 10;
    }
}

#pragma
#pragma  mark action sheet delegate method

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex==1) {
        
//        NSLog(@"Take Photo Button Pressed");
        [self pickMediaFromSource:UIImagePickerControllerSourceTypeCamera];
    }
    else if(buttonIndex==0)
    {
        [self pickMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

#pragma
#pragma  mark camera delegate method
- (void)pickMediaFromSource:(UIImagePickerControllerSourceType)sourceType{
    @autoreleasepool {
        if([UIImagePickerController isSourceTypeAvailable:sourceType])
        {
            
            NSArray *mediaTypes=[UIImagePickerController availableMediaTypesForSourceType:sourceType];
            UIImagePickerController *picker=[[UIImagePickerController alloc]init];
            picker.mediaTypes=mediaTypes;
            picker.delegate=self;
            picker.allowsEditing=YES;
            //            picker.allowsEditing=NO;
            picker.sourceType=sourceType;
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            [UtilHelper showApplicationAlertWithMessage:@"Camera is not found" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
        }
    }
}

#pragma mark -
#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)])
    {
        //image
        @autoreleasepool {
            UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
        
            AddImageObject *addImage = [AddImageObject new];
            addImage.image = chosenImage;
            addImage.isUploaded = NO;
            [_imagesArray addObject:addImage];
            [self.pageControl setNumberOfPages:_imagesArray.count];
            
            [self checkActiveButton:@"123"];
            
            int imagesOffSet = 0;
            for (int i = 0; i < [_imagesArray count]; i++) {
                //We'll create an imageView object in every 'page' of our scrollView.
                
                UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(imagesOffSet,0,241, 155)];
                
                img.contentMode = UIViewContentModeScaleAspectFill;
                img.layer.masksToBounds = YES;
                img.layer.cornerRadius = 7.0f;
                [self.innerScrollView addSubview:img];
                
                AddImageObject *imageObject = _imagesArray[i];
                if (imageObject.isUploaded == NO) {
                    
                    img.image = imageObject.image;
                    
                } else {
                    
                    NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, imageObject.imageName];
                    [img sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
                    imageObject.image = img.image;
                }
                
                imagesOffSet+=241;
            }
            
            //Set the content size of our scrollview according to the total width of our imageView objects.
            _innerScrollView.contentSize = CGSizeMake(imagesOffSet, 155);
            _innerScrollView.hidden = NO;
            _pageControl.hidden = NO;
            _crossButton.hidden = NO;
            _addPicTopConstraint.constant = 70;
            [_addPicButton setTitle:@"+ Add picture" forState:UIControlStateNormal];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [_innerScrollView setContentOffset:CGPointMake(_innerScrollView.contentSize.width - _innerScrollView.bounds.size.width, 0) animated:YES];
            });
        }
    }
    else
    {
//        NSLog(@"Video");
        //video
        [UtilHelper showApplicationAlertWithMessage:@"Video does not support." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark -
#pragma mark - Text Field Delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    if (textField == _expPeriodTextField) {
        [self.view endEditing:YES];
        [self takenTimeAction:0];
        return NO;
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    [self checkActiveButton:numberOFCharacter];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)takenTimeAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select Expire Period Date" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Select", nil];
    alert.delegate = self;
    alert.tag = 100;
    _picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
    _picker.backgroundColor = [UIColor colorWithRed:232.0/255 green:232.0/255 blue:232.0/255 alpha:1.0];
    
    [_picker setDatePickerMode:UIDatePickerModeDate];
    [_picker setMinimumDate:[NSDate date]];
    [_picker addTarget:self action:@selector(updateTakenDateTime:)
      forControlEvents:UIControlEventValueChanged];
    [alert addSubview:_picker];
    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
    [alert setValue:_picker forKey:@"accessoryView"];
    [alert show];
    _isTimeTaken = YES;
}

- (IBAction)updateTakenDateTime:(id)sender
{
    //    [self updateTekenTime];
}

- (void)updateTekenTime
{
    NSDate *date = self.picker.date;
    NSString *timeString = [date getMMDDYYYYDate];
    
    _deliveryDateTimeForWeb = [date getDateTimeInUTC];
    
    _expPeriodTextField.text = timeString;
    [self checkActiveButton:_expPeriodTextField.text];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }else {
        
        if (buttonIndex == 1) {
            [self updateTekenTime];
        }
    }
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)checkActiveButton:(NSString *)numberOFCharacter
{
    BOOL isActive = NO;
    
    for (JVFloatLabeledTextField *tfText in self.validationArray) {
        
        if (tfText.text.length > 0) {
            
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
            
        } else {
            
            isActive = NO;
            break;
        }
    }
    
    if (_imagesArray.count == 0) {
        
        isActive = NO;
    }
    
    if (isActive) {
        
        _btnRegister.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
        _btnRegister.selected = NO;
        
    }else {
        
        _btnRegister.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnRegister.selected = YES;
    }
}

- (void)requestToUpdateInsGen
{
    
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    if(_imagesArray != nil && _imagesArray.count > 0)
    {
        
        int tempIndex = 0;
        for (UIImageView *v in _innerScrollView.subviews) {
            if ([v isKindOfClass:[UIImageView class]]) {
                
                if (tempIndex < 2) {
                    
                    AddImageObject *image1 = _imagesArray[tempIndex];
                    if (image1.image == nil) {
                        image1.image = v.image;
                    }
                }
                tempIndex++;
            }
        }
        
        NSString *finalBase641 = EMPTY_STRING;
        NSString *finalBase642 = EMPTY_STRING;

        if (_imagesArray.count >= 1) {
            
            AddImageObject *imageObject = _imagesArray[0];
            NSData* imageData1 = UIImageJPEGRepresentation(imageObject.image, 0.5);
            NSString *image1Base64 = [imageData1 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            NSString *stringBase1 = @"data:image/png;base64,";
            finalBase641 = [stringBase1 stringByAppendingString:image1Base64];
        }
        
        if (_imagesArray.count > 1) {
            
            AddImageObject *imageObject = _imagesArray[1];
            NSData* imageData2 = UIImageJPEGRepresentation(imageObject.image, 0.5);
            NSString *image2Base64 = [imageData2 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            NSString *stringBase2 = @"data:image/png;base64,";
            finalBase642 = [stringBase2 stringByAppendingString:image2Base64];
        }
        
        if ([AppDelegate sharedAppDelegate].userId == nil) {
            
            NSString *userId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
            [AppDelegate sharedAppDelegate].userId = userId;
        }
        
        [WebServicesClient AddUserIns:[AppDelegate sharedAppDelegate].userId
                InsurancePolicyNumber:_policyNoTextField.text
                  InsuranceExpiryDate:_deliveryDateTimeForWeb
                      InsuranceImage1:finalBase641
                      InsuranceImage2:finalBase642
                        LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                          LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                         LogUserAgent:[[UIDevice currentDevice] systemVersion]
                        LogDeviceType:LOG_DEVICE_TYPE
                    completionHandler:^(BOOL result, NSError *error) {
                        
                        [self hideProgressHud];
                        
                        if (!error) {
                            
                            if (!_isComingFromProfile) {
                                [[AppDelegate sharedAppDelegate].appController showVerificationConfirmationnView];
                                [self updateCountOfStepsCompletedByCourierInRegistration:@"5"];

                            }
                            
                        } else if (error.code == 3) {
                            
                            [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                            
                        } else {
                            
                            [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                        }
                        
                    }];
            
    }
    else
    {
        [self hideProgressHud];
        
        [UtilHelper showApplicationAlertWithMessage:@"One or more images have not been downloaded. Please contact administrator or try visitng the page after sometime"
                                       withDelegate:nil
                                            withTag:0
                                  otherButtonTitles:@[@"Ok"]];
        
    }
}

- (void)requestToGetUserInsurance
{
    [self showProgressHud];
    
    NSString *userId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
    
    [WebServicesClient GetUserIns:userId
                    LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                      LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                     LogUserAgent:[[UIDevice currentDevice] systemVersion]
                    LogDeviceType:LOG_DEVICE_TYPE
                completionHandler:^(GetCourierInsuranceGen *courierInsGen, NSError *error) {
                    
                    [self hideProgressHud];
                    
                    if (!error) {
                        
                        [self populateUserInsuranceData:courierInsGen];
                    }
                    else if (error.code == 3) {
                        
                        [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                        
                    } else {
                        
                        [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                    }
                }];
}

- (void)populateUserInsuranceData:(GetCourierInsuranceGen *)getCourierIns
{
    
    if (getCourierIns.insImg1 != nil) {
        
        AddImageObject *imageObject = [AddImageObject new];
        imageObject.isUploaded = YES;
        imageObject.imageName = getCourierIns.insImg1;
        [_imagesArray addObject:imageObject];
    }
    
    if (getCourierIns.insImg2 != nil && ![getCourierIns.insImg2 isEqualToString:EMPTY_STRING]) {
        
        AddImageObject *imageObject = [AddImageObject new];
        imageObject.isUploaded = YES;
        imageObject.imageName = getCourierIns.insImg2;
        [_imagesArray addObject:imageObject];
    }
    
    if (_imagesArray.count > 0)
    {
        [self.pageControl setNumberOfPages:_imagesArray.count];
        int imagesOffSet = 0;
        
        for (int i = 0; i < [_imagesArray count]; i++) {
            //We'll create an imageView object in every 'page' of our scrollView.
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(imagesOffSet,0,241, 155)];
            img.layer.masksToBounds = YES;
            img.layer.cornerRadius = 7.0f;
            [self.innerScrollView addSubview:img];
            
            AddImageObject *imageObject = _imagesArray[i];
            if (imageObject.isUploaded == NO) {
                
                img.image = imageObject.image;
                
            } else {
                
                [img setShowActivityIndicatorView:YES];
                [img setIndicatorStyle:UIActivityIndicatorViewStyleGray];
                NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, imageObject.imageName];
                [img sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
                imageObject.image = img.image;
            }
            imagesOffSet+=241;
        }
        
        //Set the content size of our scrollview according to the total width of our imageView objects.
        _innerScrollView.contentSize = CGSizeMake(imagesOffSet, 155);
        _innerScrollView.hidden = NO;
        _pageControl.hidden = NO;
        _crossButton.hidden = NO;
        _addPicTopConstraint.constant = 70;
        [_addPicButton setTitle:@"+ Add picture" forState:UIControlStateNormal];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [_innerScrollView setContentOffset:CGPointMake(_innerScrollView.contentSize.width - _innerScrollView.bounds.size.width, 0) animated:YES];
        });
    }
    
    if (getCourierIns.policyNumber != nil) {
        
        _policyNoTextField.text = getCourierIns.policyNumber;
    }
    
    if (getCourierIns.expDate != nil && ![getCourierIns.expDate isEqualToString:@"1970-01-01"]) {
        
        NSDate *date = [NSDate justDateFromDateString:getCourierIns.expDate];
        _deliveryDateTimeForWeb = [date getDateTimeInUTC];
        _expPeriodTextField.text = [date getMMDDYYYYDate];
    }
    
    _stepLabel.hidden = YES;
    [self.btnRegister setTitle:@"Update" forState:UIControlStateNormal];
    [self checkActiveButton:@"234"];
}

#pragma mark
#pragma mark -- LocationManagerService Delegates
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.innerScrollView.frame.size.width;
    int page = floor((self.innerScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
}

@end
