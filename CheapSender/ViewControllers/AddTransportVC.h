//
//  AddTransportVC.h
//  CheapSender
//
//  Created by admin on 4/22/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "AUIAutoGrowingTextView.h"
#import "MediaUploadingViewCell.h"



@interface AddTransportVC : BaseViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIAlertViewDelegate>

@property (assign, nonatomic) BOOL isComingFromProfile;
@property (strong, nonatomic) GetTransports *transport;

@end
