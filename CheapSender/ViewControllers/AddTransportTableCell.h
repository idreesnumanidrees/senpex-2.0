//
//  AddTransportTableCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSGeneralConstants.h"
#import "JVFloatLabeledTextField.h"

@protocol AddNewTransportDelegate <NSObject>

- (void)checkValidation:(NSString *)text;

@end

@interface AddTransportTableCell : UITableViewCell <UIScrollViewDelegate>

@property (nonatomic, assign) id <AddNewTransportDelegate> Delegate;

@property (weak, nonatomic) IBOutlet UIImageView *selectedVehicleImageView;
@property (weak, nonatomic) IBOutlet UILabel *selectedVehicleLabel;

@property ( nonatomic)BOOL firstCellIsHidden;
@property ( nonatomic)BOOL seconCellIsHidden;

@property (nonatomic, assign) NSInteger transportTypeIndex;

//Fields
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfMake;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfModel;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfYear;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfPlateNumber;

//Version1.5
@property (weak, nonatomic) IBOutlet UIScrollView *innerScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *deleteImageButton;

@property (weak, nonatomic) IBOutlet UIButton *backButtonTapped;
@property (weak, nonatomic) IBOutlet UIButton *laterButtonTapped;

- (void)transportTypesSetup;
- (IBAction)imagePickerBtnClicked:(id)sender;

- (void)refreshTableView;

- (void)setupForImages:(NSMutableArray *)mediaArray;

@end
