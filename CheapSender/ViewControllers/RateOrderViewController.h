//
//  RateOrderViewController.h
//  CheapSender
//
//  Created by Ambika Vijay on 4/28/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface RateOrderViewController : BaseViewController<UITextViewDelegate>

@property (nonatomic, strong) PackDetailsModel *packDetails;

@end
