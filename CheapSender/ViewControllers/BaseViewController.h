//
//  BaseViewController.h
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "AppController.h"
#import "UtilHelper.h"
#import "AppDelegate.h"
#import "CSGeneralConstants.h"
#import "JVFloatLabeledTextField.h"
#import "WebServicesClient.h"
#import "NSDate+Utilities.h"
#import "IQKeyboardManager.h"
#import "DataModels.h"
#import "SingletonClass.h"
#import "CSWebConstants.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SenderInfoDB.h"
#import "CourierInfoDB.h"
#import "CardInfoDB.h"
#import "FBEncryptorAES.h"
#import "LastChatMessageDB.h"
#import "PackageInfo.h"
#import "PackageDB.h"

@interface BaseViewController : UIViewController <UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

- (NSAttributedString *)setFontWithSize:(CGFloat)fontSize
                        withWebFontText:(const char *)webFontString;

- (IBAction)backAction:(id)sender;

- (void)showProgressHud;
- (void)hideProgressHud;

- (void)senderLogout;

- (void)getAllDict;

- (void)updateCountOfStepsCompletedByCourierInRegistration:(NSString *)stepCount;
- (int)getNoOfStepsCourierHasCompletedOnRegistration:(NSString *)userId;

- (void)openMapsSheetWithLat:(float)lat andLng:(float)lng;

- (void)setStatusBarBackgroundColor:(UIColor *)color;

@end
