//
//  StartUpViewController.m
//  CheapSender
//
//  Created by admin on 4/17/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "StartUpViewController.h"

@interface StartUpViewController ()

@property (strong, nonatomic) IBOutlet UIButton *btnSender;
@property (strong, nonatomic) IBOutlet UIButton *btnCourier;
@property (strong, nonatomic) IBOutlet UIButton *btnFastOrder;

@end

@implementation StartUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [UIView setAnimationsEnabled:YES];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    if (IS_IPHONE_5) {
        
        self.btnSender = (UIButton *) [UtilHelper makeCornerCurved: (UIView *)self.btnSender withRadius:22 color: UIColor.clearColor width: 0];
//        self.btnCourier = (UIButton *) [UtilHelper makeCornerCurved: (UIView *)self.btnCourier withRadius:22 color: UIColor.clearColor width: 0];
        self.btnFastOrder = (UIButton *) [UtilHelper makeCornerCurved: (UIView *)self.btnFastOrder withRadius:22  color: UIColor.clearColor width: 0];
        
    }else {
        
        self.btnSender = (UIButton *) [UtilHelper makeCornerCurved: (UIView *)self.btnSender withRadius: self.btnSender.frame.size.height/2 color: UIColor.clearColor width: 0];
//        self.btnCourier = (UIButton *) [UtilHelper makeCornerCurved: (UIView *)self.btnCourier withRadius: self.btnCourier.frame.size.height/2  color: UIColor.clearColor width: 0];
        self.btnFastOrder = (UIButton *) [UtilHelper makeCornerCurved: (UIView *)self.btnFastOrder withRadius: self.btnFastOrder.frame.size.height/2  color: UIColor.clearColor width: 0];
    }
}

#pragma mark
#pragma mark --IBActions
- (IBAction)loginAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showLoginView];
}

- (IBAction)becomeSenderAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showRegisterView];
}

- (IBAction)becomeCourierAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showSignUpCourierView];
}

- (IBAction)makeFastOrderAction:(id)sender {
    
    if (![UtilHelper isNetworkAvailable]) {
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"MakeFastOrder"];
    NSString *send_cat_id = [[NSString alloc]initWithFormat:@"%d", (int)2];
    [[AppDelegate sharedAppDelegate].appController showNewOrderPackInfoView:@"" Send_Cat_ID:send_cat_id SenderData:nil isBackHidden:YES];
}

@end
