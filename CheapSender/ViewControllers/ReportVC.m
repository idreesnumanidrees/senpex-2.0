//
//  ReportVC.m
//  Demo
//
//  Created by Rajesh on 04/05/17.
//  Copyright © 2017 Rajesh. All rights reserved.
//

#import "ReportVC.h"
#import "AUIAutoGrowingTextView.h"

@interface ReportVC () {
    
    NSUInteger selectedIndex;
}
@property (weak, nonatomic) IBOutlet UILabel *placeholderLbl;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet AUIAutoGrowingTextView *textView;
@property (nonatomic, strong)NSArray *optionsArr;
@property (weak, nonatomic) IBOutlet UITableView *reportTableView;

@end

@implementation ReportVC

- (IBAction)sendBtnClicked:(id)sender {
    
    if (_sendBtn.enabled) {
        
        [self requestForSenderReportProblem];
    }
}

- (IBAction)backBtnClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.

    _reportTableView.sectionHeaderHeight = 0;
    
//    _optionsArr = [NSArray arrayWithObjects:@"Personal reason",@"Unprofessional courier",@"Other", nil];
    
    if ([AppDelegate sharedAppDelegate].allDictData.count > 0) {
        
        NSPredicate *senderRepResPred = [NSPredicate predicateWithFormat:@"dicShort contains[c] %@", @"s_report_reasons"];
        NSMutableArray *senderRepResArr = [[[AppDelegate sharedAppDelegate].allDictData filteredArrayUsingPredicate:senderRepResPred] mutableCopy];
        _optionsArr = senderRepResArr;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    [_textView setTextContainerInset:UIEdgeInsetsMake(10, 18, 10, 18)];
}

- (void)textChanged:(NSNotification *)notification
{
    if([[_textView text] length] == 0) {
        _sendBtn.enabled = NO;
        _sendBtn.backgroundColor = [UIColor colorWithRed:185.0f/255.0f green:192.0f/255.0f blue:200.0f/255.0f alpha:1];
//Your feedback
        _placeholderLbl.text = @"Your feedback";

    }
    else
    {
        _sendBtn.enabled = YES;
        _sendBtn.backgroundColor = [UIColor colorWithRed:53.0f/255.0f green:66.0f/255.0f blue:88.0f/255.0f alpha:1];
        _placeholderLbl.text = @"";
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return .1;
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    
    return .1;
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_optionsArr count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReasonCell" forIndexPath:indexPath];
    UILabel *lbl = (UILabel *)[cell viewWithTag:11];
    UIImageView *checkImage = (UIImageView *)[cell viewWithTag:12];
    
    GetDictByName *dictName = _optionsArr[indexPath.row];
    lbl.text = dictName.listName;
    
    if (indexPath.row == selectedIndex) {
        checkImage.hidden = NO;
        lbl.highlighted = YES;
    } else {
        checkImage.hidden = YES;
        lbl.highlighted = NO;
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex =  indexPath.row;
    [tableView reloadData];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)requestForSenderReportProblem
{
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    [self showProgressHud];
    
    GetDictByName *dictByName = _optionsArr[selectedIndex];

    [WebServicesClient SenderReportProblem:_packId
                                ReasonText:_textView.text
                                  ReasonId:dictByName.listId
                               LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                              LogUserAgent:[[UIDevice currentDevice] systemVersion]
                             LogDeviceType:LOG_DEVICE_TYPE
                                SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                         completionHandler:^(BOOL isPackCancelled, NSError *error) {
                             
                             [self hideProgressHud];

                             if (!error) {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:@"Thanks for reporting an issue about our courier. Senpex Staff will review your feedback and will come back to you."
                                                                withDelegate:self
                                                                     withTag:2
                                                           otherButtonTitles:@[@"OK"]];
                                 
                                 [[NSNotificationCenter defaultCenter] postNotificationName:SENDER_ACTIVE_LIST_UPDATED object:@{}];
                                 [AppDelegate sharedAppDelegate].isNeedToRefreshPackDetails = 1;
                                 
                             } else if (error.code == 3) {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                 
                             } else {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                             }
                         }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{

    if(alertView.tag == 2)
    {
        [self.navigationController popViewControllerAnimated:YES];

    }
    else if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }
}



@end
