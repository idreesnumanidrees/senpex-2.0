//
//  SignUpIDCardVC.h
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface SignUpIDCardVC : BaseViewController <UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIDatePicker *picker;
@property (assign, nonatomic) BOOL isTimeTaken;
@property (weak, nonatomic) IBOutlet UIButton *btnMale;
@property (weak, nonatomic) IBOutlet UIButton *btnFemale;

@property (assign, nonatomic) BOOL isComingFromProfile;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *innerViewTopConstraint;

@end
