//
//  VerificationConfirmationVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "VerificationConfirmationVC.h"

@interface VerificationConfirmationVC ()
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;

@end

@implementation VerificationConfirmationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareUI];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    
    self.btnLogin = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnLogin withRadius:_btnLogin.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    //When registerCourier webservice is called with parameter called "send_approve_mail" = "1"
    //the we dont need to call this service again.
    //If "send_approve_mail" = "0" then we can call this service
    [self requestForSendApproveEmailToCourier];
}


#pragma mark
#pragma mark --Actions

- (IBAction)signUpAction:(id)sender {
 //   [[AppDelegate sharedAppDelegate].appController showVerificationView];
    
}

- (IBAction)goToLoginActionAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showLoginView];
}

- (void)requestForSendApproveEmailToCourier
{
    [WebServicesClient SendApproveEmailToCourier:[AppDelegate sharedAppDelegate].sessionKey
                                     LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                    LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                   LogDeviceType:LOG_DEVICE_TYPE
                               completionHandler:^(BOOL result, NSError *error) {
                                   
                                   if (!error) {
                                       
                                       [self updateCountOfStepsCompletedByCourierInRegistration:@"6"];
//                                       NSLog(@"Email sent on user eEmil");
                                   }
                               }];
}

@end
