//
//  AcceptedCourierViewController.m
//  CheapSender
//
//  Created by Riyas Abdul Rahman on 5/1/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "AcceptedCourierViewController.h"

@interface AcceptedCourierViewController ()
@property (weak, nonatomic) IBOutlet UIButton *gotoOrderBtn;
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;

@end

@implementation AcceptedCourierViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self prepareUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)prepareUI
{
    self.gotoOrderBtn = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.gotoOrderBtn
                                                      withRadius:25.0
                                                           color:LOGIN_ACTIVE_COLOR
                                                           width:0];
    
    self.profilePic = (UIImageView *)[UtilHelper makeImageRounded:self.profilePic];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
