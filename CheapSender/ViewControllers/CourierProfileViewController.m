//
//  CourierProfileViewController.m
//  CheapSender
//
//  Created by Ambika on 28/04/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierProfileViewController.h"
#import "CourierProfileCell.h"
#import "HCSStarRatingView.h"

NSString *titles[3] = {@"Mobile" , @"How can i deliver", @"My own transport"};

@interface CourierProfileViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *statusDot;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRateView;
@property (weak, nonatomic) IBOutlet UILabel *lblStarRate;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UIButton *trackButton;
@property (weak, nonatomic) IBOutlet UILabel *chatLabel;
@property (weak, nonatomic) IBOutlet UILabel *trackLabel;

@property (strong, nonatomic) NSMutableArray *userData;
@property (nonatomic, strong) NSString *cellNumber;

@property (nonatomic, strong) NSString *courierId;
@property (nonatomic, strong) NSString *senderId;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *callButtonXConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *callLabelXConstraint;

@end

@implementation CourierProfileViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareUI
{
    if (_isComingFromChatDetails) {
        
        _chatButton.hidden = YES;
        _trackButton.hidden = YES;
        _chatLabel.hidden = YES;
        _trackLabel.hidden = YES;
    }
    
    _starRateView.minimumValue = 0;
    _starRateView.maximumValue = 5;
    
    if (_packDetails != nil) {
        
        if ([_packDetails.packStatus isEqualToString:@"40"]||[_packDetails.packStatus isEqualToString:@"40"] ||
            [self.packDetails.packStatus isEqualToString:@"50"] ||
            [self.packDetails.packStatus isEqualToString:@"90"] ||
            [self.packDetails.packStatus isEqualToString:@"100"]) {
            
            _trackButton.hidden = YES;
            _trackLabel.hidden = YES;
        }
        
        _starRateView.value = [_packDetails.acceptedCourierRate floatValue];
        _lblStarRate.text = [NSString stringWithFormat:@"(%@)", _packDetails.acceptedCourierRate];
        _lblName.text = _packDetails.acceptedCourierName;
        
        if ([_packDetails.acceptedCourierAvailStatus isEqualToString:@"1"])
        {
            _lblStatus.text = @"Online";
            _lblStatus.textColor = [UIColor whiteColor];
            _statusDot.backgroundColor = [UIColor greenColor];
        }
        else if ([_packDetails.acceptedCourierAvailStatus isEqualToString:@"2"])
        {
            _lblStatus.text = @"Offline";
            _lblStatus.textColor = [UIColor whiteColor];
            _statusDot.backgroundColor = [UIColor grayColor];
        }
        else if ([_packDetails.acceptedCourierAvailStatus isEqualToString:@"3"])
        {
            _lblStatus.text = @"Busy";
            _lblStatus.textColor = [UIColor whiteColor];
            _statusDot.backgroundColor = [UIColor redColor];
        }
        
        _courierId = _packDetails.acceptedCourierId;
        _senderId = _packDetails.packOwnerId;
        _cellNumber = _packDetails.acceptedCourierCell;
    }
    else {
        
        _courierId = _conversation.receiverId;
        
        if(_courierId == nil || _courierId == 0)
        {
            _courierId = _conversation.viewId;
        }
        _senderId = _conversation.senderId;
    }
    
    [self requestToUserinfo];
}

- (void)loadSenderProfileImage:(NSString *)profileImage
{
    //Shows indicatore on imageView
    [_profileImage setShowActivityIndicatorView:YES];
    [_profileImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, profileImage];
    
    [_profileImage sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    
    //Loads image
//    [_profileImage sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
}

#pragma mark - User Actions

- (IBAction)showcourierPersonalInfoAction:(id)sender {
    
    //[[AppDelegate sharedAppDelegate].appController showcourierPersonalInfoVC];
    
}

- (IBAction)chatAction:(id)sender {
    
    CourierConversations *conversationSender = [CourierConversations new];
    if (_packDetails != nil) {
        
        conversationSender.receiverId = _packDetails.acceptedCourierId;
        conversationSender.viewId = _packDetails.acceptedCourierId;
        conversationSender.viewName = _packDetails.acceptedCourierName;
        conversationSender.viewImg = _packDetails.acceptedCourierSelfImg;
        conversationSender.senderId = _packDetails.packOwnerId;
        
        [[AppDelegate sharedAppDelegate].appController ShowChatDetailView:conversationSender
                                                              PackDetails:nil];
        
    } else {
        
        conversationSender.receiverId = _conversation.receiverId;
        conversationSender.viewId = _conversation.viewId;
        conversationSender.viewName = _conversation.viewName;
        conversationSender.viewImg = _conversation.viewImg;
        conversationSender.senderId = _conversation.senderId;
        
        [[AppDelegate sharedAppDelegate].appController showChatCourierDetails:conversationSender
                                                                  PackDetails:nil];
    }
}

- (IBAction)callAction:(id)sender {
    
    NSString *cleanedString = [[_cellNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [WebServicesClient AddCallHistory:_senderId
                                    ReciverId:_courierId
                                     CallDate:[NSString stringWithFormat:@"%@", [NSDate date]]
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                                   SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                            completionHandler:^(BOOL isCallAdded, NSError *error) {
                                
                            }];
        });
    }
}

- (IBAction)trackAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showViewOnMap:@"track" PackDetails:_packDetails isComingFromCourierApp:NO];
}
- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true ];
}


#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CourierProfileCell";
    
    CourierProfileCell *cell = (CourierProfileCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.lblTitle.text = titles[indexPath.row];
    
    if (indexPath.row == 0) {
        cell.lblData.text = _userData[indexPath.row];
    }
    if (indexPath.row == 1) {
        cell.lblData.text = _userData[indexPath.row];
    }
    if (indexPath.row == 2) {
        cell.lblData.text = _userData[indexPath.row];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestToUserinfo
{
    [self showProgressHud];
    
    [WebServicesClient GetUserInfoById:_courierId
                         LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                     completionHandler:^(UserModel *user, NSError *error) {
                         
                         [self hideProgressHud];
                         
                         if (!error) {
                             
                             _cellNumber  = user.cell;
                             _starRateView.value = [user.userRate floatValue];
                             _lblStarRate.text = [NSString stringWithFormat:@"(%@)", user.userRate];
                             _lblName.text = user.name;
                             [self loadSenderProfileImage:user.selfImg];
                             
                             if ([user.availStatus isEqualToString:@"1"])
                             {
                                 _lblStatus.text = @"Online";
                                 _statusDot.backgroundColor = [UIColor greenColor];
                             }
                             else if ([user.availStatus isEqualToString:@"2"])
                             {
                                 _lblStatus.text = @"Offline";
                                 _statusDot.backgroundColor = [UIColor grayColor];
                             }
                             else if ([user.availStatus isEqualToString:@"3"])
                             {
                                 _lblStatus.text = @"Busy";
                                 _statusDot.backgroundColor = [UIColor redColor];
                             }
                             
                         } else {
                             
                             if (error.code == 3) {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                 
                             } else {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                             }
                         }
                     }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
    }
}

@end
