//
//  ResetPasswordViewController.m
//  CheapSender
//
//  Created by admin on 4/18/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "ResetPasswordViewController.h"

@interface ResetPasswordViewController ()
@property (strong, nonatomic) IBOutlet UIButton *btnResetPassword;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfResetPassword;

@property (strong, nonatomic) NSArray *textFieldArray;

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    
    if (IS_IPHONE_5) {
        
        self.btnResetPassword = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnResetPassword withRadius:22 color:COMMON_ICONS_COLOR width:0];
        
    }else {
        
        self.btnResetPassword = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnResetPassword withRadius:_btnResetPassword.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    }
    
    self.textFieldArray = @[_tfResetPassword];

    _btnResetPassword.backgroundColor = LOGIN_DISABLE_COLOR;
    _btnResetPassword.selected = YES;
    
    _tfResetPassword.autocapitalizationType = UITextAutocapitalizationTypeNone;
}

//Tells the responder when one or more fingers touch down in a view or window.
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    //Notifies the receiver that it has been asked to relinquish its status as first responder in its window.
    [self.view endEditing:YES];
}

#pragma mark
#pragma mark --Actions
- (IBAction)resetPasswordAction:(id)sender {
    
    if (_btnResetPassword.selected == NO) {
        
        [self validation];
    }
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    BOOL isActive = NO;
    
    for (JVFloatLabeledTextField *tfText in self.textFieldArray) {
        
        if (tfText.text.length > 0) {
            
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
            
        } else {
            
            isActive = NO;
            break;
        }
    }
    if (isActive) {
        
        _btnResetPassword.backgroundColor = LOGIN_ACTIVE_COLOR;
        _btnResetPassword.selected = NO;
        
    }else {
        
        _btnResetPassword.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnResetPassword.selected = YES;
    }
    return YES;
}


#pragma mark--
#pragma mark-- Server Side handling

- (void)validation
{
    if (![UtilHelper validateEmail:self.tfResetPassword.text]) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Please enter valid email" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    else {
        
        if (![UtilHelper isNetworkAvailable]) {
            
            [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
        
        [self showProgressHud];
        [self requestToResetPassword];
    }
}

- (void)requestToResetPassword
{
    
    [WebServicesClient SendForgotPasswordEmail:_tfResetPassword.text
                                 LogSessionKey:@""
                                   LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                  LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                 LogDeviceType:LOG_DEVICE_TYPE
                             completionHandler:^(BOOL result, NSError *error) {
                                 
                                 [self hideProgressHud];
                                 
                                 if (!error) {
                                     
                                     [[AppDelegate sharedAppDelegate].appController showSenderResetPassConfirmVC:_isComingFromChangePassword];
                                 }
                                 else {
                                     
                                      [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                                 }
                             }];
}


@end
