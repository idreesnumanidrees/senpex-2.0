//
//  BidsViewController.m
//  CheapSender
//
//  Created by Ambika on 27/04/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BidsViewController.h"
#import "BidViewCell.h"

@interface BidsViewController ()

@end

@implementation BidsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"BidViewCell";
    
    BidViewCell *cell = (BidViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (indexPath.row == 1) {
        cell.lblName.text =  @"Mike Muraski";
//        cell.starRating.value = 4;
    } else if (indexPath.row == 2) {
        cell.lblName.text =  @"Steev Bareel";
//        cell.starRating.value = 2.5;
    }
//    NSString* rating = [NSString stringWithFormat:@"%0.1f", cell.starRating.value];
//    NSString*  startRating = [NSString stringWithFormat:@"(%@)",rating];
//    cell.lblRating.text = startRating;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 82;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [[AppDelegate sharedAppDelegate].appController showAcceptedCourierView];
}

#pragma mark - User Action

- (IBAction)acceptBidAction:(id)sender {
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true ];
}

@end
