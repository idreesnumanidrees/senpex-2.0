//
//  SenderResetPassConfirmVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/23.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface SenderResetPassConfirmVC : BaseViewController

@property (nonatomic, assign) BOOL isComingFromChangePassword;

@end
