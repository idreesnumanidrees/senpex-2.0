//
//  PayementConfirmationViewController.m
//  TestProject
//
//  Created by Ambika on 02/05/17.
//  Copyright © 2017 ZCO. All rights reserved.
//

#import "PayementConfirmationViewController.h"
#import "PaymentConfirmationCell.h"
#import "CardDetailsViewCell.h"
#import "CardDetailsEditCell.h"
#import "PaymentUpdatedCell.h"



@interface PayementConfirmationViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewheightConstraint;
@property (strong, nonatomic) IBOutlet UILabel *lblPPaymentStatus;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *addYourCardBottomConstraint;
@property (weak, nonatomic) IBOutlet UIView *acceptanceView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) UIPickerView *picker;

@end

@implementation PayementConfirmationViewController

NSString *imageNames[5] = {@"card" , @"name_surname", @"name_surname", @"Delivery time", @"password"};
NSString *placeHolder[5] = {@"Card number" , @"Cardholder name", @"Billing address", @"Exp.date", @"CVV"};
int numberOfRows = 4;
int currentMonth;
int currentYear;
NSArray *months;
NSMutableArray *years;

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.screenType = paymentConfirmationRequest;
    switch (_screenType) {
            case paymentConfirmationDenied:
            [_lblPPaymentStatus setHidden:false ];
            numberOfRows = 3;
            _tableViewheightConstraint.constant = numberOfRows * PaymentConfirmationRowHeight;
            break;
            case paymentConfirmationAccepted:
            [_acceptanceView setHidden:false ];
            numberOfRows = 3;
            _tableViewheightConstraint.constant = numberOfRows * PaymentConfirmationRowHeight;
            break;
            case paymentConfirmationRequest:
            numberOfRows = 4;
            _addYourCardBottomConstraint.constant = 25;
            _tableViewheightConstraint.constant = numberOfRows * PaymentConfirmationRowHeight;
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (_screenType) {
        case paymentConfirmationDenied:
        case paymentConfirmationAccepted:
            return [self showCellToViewDetails:tableView indexPath:indexPath];
            break;
        case paymentConfirmationRequest:
            return [self showCellToEnterDetails:tableView indexPath:indexPath];
            break;
            
        default:
            return [self showCellToEnterDetails:tableView indexPath:indexPath];
            break;
    }
    
    
    static NSString *cellIdentifier = @"TableCell";
    PaymentConfirmationCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return PaymentConfirmationRowHeight;
}
-(UITableViewCell *)showCellToEnterDetails:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == (numberOfRows - 1)){
        static NSString *cellIdentifier = @"CardDetailsEditCell";
        CardDetailsEditCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.textFieldCvv.keyboardType = UIKeyboardTypeNumberPad;
        return cell;
    }
    static NSString *cellIdentifier = @"PaymentConfirmationCell";
    PaymentConfirmationCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.iconView.image = [UIImage imageNamed:imageNames[indexPath.row]];
    cell.valueField.placeholder = placeHolder[indexPath.row];
    switch(indexPath.row){
            case 0:
            cell.valueField.keyboardType = UIKeyboardTypeNumberPad;
            break;
        }
    return cell;
}
-(UITableViewCell *)showCellToViewDetails:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == (numberOfRows - 1)){
        static NSString *cellIdentifier = @"CardDetailsViewCell";
        CardDetailsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        cell.textFieldCvv.text = _model.cardCvv;
        cell.lblExpDate.text = _model.cardExpDate;
        
        return cell;
    }
    static NSString *cellIdentifier = @"PaymentUpdatedCell";
    PaymentUpdatedCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.iconView.image = [UIImage imageNamed:imageNames[indexPath.row]];
    cell.lblTitle.text = placeHolder[indexPath.row];
    NSString *detail;
    switch(indexPath.row){
            case 0:
            detail = _model.cardNumber;
            break;
            case 1:
            [cell.cardTypeView setHidden:true];
            detail = _model.cardHolderName ;
            break;
            
    }
    cell.lblValue.text = detail;

    return cell;

    
}
- (IBAction)expDateButtonAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select Expiry Date" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alert.delegate = self;
    alert.tag = 100;
    _picker = [[UIPickerView alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
    [self initPickerData ];
    _picker.delegate = self;
    _picker.dataSource = self;

    [alert addSubview:_picker];
    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
    [alert setValue:_picker forKey:@"accessoryView"];
    [alert show];
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:true ];
}
- (IBAction)payNowBtnAction:(id)sender {
    
    PaymentModel * model = [self getCardDetails];
    if(model == nil){
        return ;
    }
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PayementConfirmationViewController *vc = (PayementConfirmationViewController *)[sb instantiateViewControllerWithIdentifier:@"PayementConfirmationViewController"];
    vc.screenType = paymentConfirmationDenied;
    vc.model = model;
    [self.navigationController pushViewController:vc animated:true];
}
- (IBAction)payPalButtonAction:(id)sender {
    
    PaymentModel * model = [self getCardDetails];
    if(model == nil){
        return ;
    }
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    PayementConfirmationViewController *vc = (PayementConfirmationViewController *)[sb instantiateViewControllerWithIdentifier:@"PayementConfirmationViewController"];
    vc.screenType = paymentConfirmationAccepted;
    vc.model = model;
    [self.navigationController pushViewController:vc animated:true];
}

- (PaymentModel *)getCardDetails {
    
    PaymentModel *model = [PaymentModel new];
    NSArray *cells = _tableView.visibleCells;
    for(UITableViewCell *cell in cells)
    {
        NSIndexPath *indexPath = [_tableView indexPathForCell:cell];
                switch(indexPath.row){
                case 0:
                if([((PaymentConfirmationCell *)cell) isEmpty]){
                    return nil;
                }
                model.cardNumber = ((PaymentConfirmationCell *)cell).valueField.text;
                break;
                case 1:
                if([((PaymentConfirmationCell *)cell) isEmpty]){
                    return nil;
                }
                model.cardHolderName = ((PaymentConfirmationCell *)cell).valueField.text;
                break;
                case 2:
                
                if([((PaymentConfirmationCell *)cell) isEmpty]){
                    return nil;
                }
                model.billingAddress = ((PaymentConfirmationCell *)cell).valueField.text;
                break;
                
                case 3:
                    {
                        NSString *expDate = ((CardDetailsEditCell *)cell).btnExpDate.titleLabel.text;
                        NSRange range = [expDate rangeOfString:@"^\\s*" options:NSRegularExpressionSearch];
                        NSString *result = [expDate stringByReplacingCharactersInRange:range withString:@""];
                        if([result  isEqual: @"Exp.date"] ){
                            [self expDateButtonAction:((CardDetailsEditCell *)cell).btnExpDate];
                            return nil ;
                        }
                        model.cardExpDate = result;
                    }
                //model.cardExpDate = ((CardDetailsEditCell *)cell).btnExpDate.titleLabel.text;
                model.cardCvv = ((CardDetailsEditCell *)cell).textFieldCvv.text;
                break;
        }
    }
    
    return model;

}

-(void)initPickerData
{
    months = [[NSArray alloc] initWithObjects:@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"June", @"July", @"Aug", @"Sept", @"Oct", @"Nov", @"Dec", nil];
    
    NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
    formatter.dateFormat        = @"MM-yyyy";
    NSString *string            = [formatter stringFromDate:[NSDate date]];
    
    NSArray *comps = [string componentsSeparatedByString:@"-"];
    currentMonth    = [[comps objectAtIndex:0] intValue] - 1;
    currentYear     = [[comps objectAtIndex:1] intValue];
    
    years   = [[NSMutableArray alloc] init];
    for (int i = 0; i < 12; i++)
    {
        [years addObject:[NSString stringWithFormat:@"%i", currentYear + i]];
    }
}

#pragma mark - Picker View Delegates -

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return 12;
    }
    return [years count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [months objectAtIndex:row];
    }
    return [years objectAtIndex:row];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        NSString *year = [[self pickerView:_picker titleForRow:[_picker selectedRowInComponent:1] forComponent:1] substringWithRange:NSMakeRange(2,2)];

        NSString *dateText = [NSString stringWithFormat:@"   %@/%@",
                              [NSString stringWithFormat: @"%ld", ([_picker selectedRowInComponent:0] + 1)],year
                              ];
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:0];
        CardDetailsEditCell *cardDetailsCell = (CardDetailsEditCell *)[_tableView cellForRowAtIndexPath:indexPath];
        [(cardDetailsCell.btnExpDate) setTitle:dateText forState:UIControlStateNormal];
    }

}




@end
