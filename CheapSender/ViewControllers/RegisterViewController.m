//
//  RegisterViewController.m
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "PlacesViewController.h"

@interface RegisterViewController ()<PlacesViewControllerDelegate>

//Outlets
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftFirstName;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftLastName;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftEmail;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftPassword;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftMobileNumber;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *ftCompanyName;
@property (strong, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) NSArray *textFieldArray;

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    
    if (IS_IPHONE_5) {
        
        _btnRegister = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnRegister withRadius:18 color:COMMON_ICONS_COLOR width:0];
        
    }else {
        
        _btnRegister = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnRegister withRadius:_btnRegister.frame.size.height/2-8 color:COMMON_ICONS_COLOR width:0];
    }
    
    self.textFieldArray = @[_ftFirstName, _ftLastName, _ftEmail, _ftPassword, _ftMobileNumber];

    _btnRegister.selected = YES;
    _ftEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
}

#pragma mark
#pragma mark --Actions
- (IBAction)loginAction:(id)sender {
    
    //    [self.navigationController popViewControllerAnimated:YES];
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    BOOL isLoginInStack = NO;
    
    for (UIViewController *aViewController in allViewControllers) {
        
        if ([aViewController isKindOfClass:[LoginViewController class]]) {
            
            isLoginInStack = YES;
            [self.navigationController popToViewController:aViewController animated:YES];
        }
    }
    
    if (!isLoginInStack) {
        
        [[AppDelegate sharedAppDelegate].appController showLoginView];
    }
}

- (IBAction)signUpAction:(id)sender {
    
    if (_btnRegister.selected == NO) {
        
        [self validation];
    }
}
/*
- (IBAction)addAddressAction:(id)sender {
    PlacesViewController *placesView = [[AppDelegate sharedAppDelegate].appController.mainStoryBoard instantiateViewControllerWithIdentifier:@"PlacesViewController"];
    placesView.placesDelegate = self;
    placesView.headingName = @"Add Address";
    [[AppDelegate sharedAppDelegate].appController.navController pushViewController:placesView animated:NO];
}
*/
#pragma mark -
#pragma mark - PlacesViewControllerDelegate

- (void)placeMarker:(GMSPlace *)responseDict
{
//    self.ftAddress.text = responseDict.formattedAddress;
//    [self checkActiveButton:self.ftAddress.text];
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
    
    
//    NSInteger nextTag = textField.tag + 1;
//    // Try to find next responder
//    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
//    if (nextResponder) {
//        // Found next responder, so set it.
//        [nextResponder becomeFirstResponder];
//    } else {
//        // Not found, so remove keyboard.
//        [textField resignFirstResponder];
//    }
//    return NO; // We do not want UITextField to insert line-breaks.
  
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
   
    BOOL isActive = NO;
    for (JVFloatLabeledTextField *tfText in self.textFieldArray) {
        if (tfText.text.length > 0) {
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
        } else {
            isActive = NO;
            break;
        }
    }
    
    if (isActive) {
        _btnRegister.backgroundColor = LOGIN_ACTIVE_COLOR;
        _btnRegister.selected = NO;
    }else {
        _btnRegister.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnRegister.selected = YES;
    }
    return YES;
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)validation
{
    if (![UtilHelper validateEmail:self.ftEmail.text]) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Please enter valid email" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        
    }
    else if (![UtilHelper isMatchPasswordLength:self.ftPassword.text]) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Password should be more than 6 characters" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        
    } else {
        
        if (![UtilHelper isNetworkAvailable]) {
            
            [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
        
        [self showProgressHud];
        [self requestToRegisterUser];
    }
}

- (void)requestToRegisterUser
{
    [WebServicesClient RegisterSender:_ftFirstName.text
                              Surname:_ftLastName.text
                                 Cell:_ftMobileNumber.text
                                Email:_ftEmail.text
                             Password:_ftPassword.text
                              Address:@""
                          CompanyName:_ftCompanyName.text? _ftCompanyName.text: @""
                          LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                         LogUserAgent:[[UIDevice currentDevice] systemVersion]
                        LogDeviceType:LOG_DEVICE_TYPE
                     SendApproveEmail:@"1"
                    completionHandler:^(UserModel *user, NSError *error) {
                        
                        [self hideProgressHud];
                        
                        if (!error) {
                            
                            [AppDelegate sharedAppDelegate].sessionKey = user.logSessionKey;
                            [[AppDelegate sharedAppDelegate].appController showSenderSignUpConfirmVC];
                            
                        } else {
                            
                            [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                        }
                    }];
}



@end
