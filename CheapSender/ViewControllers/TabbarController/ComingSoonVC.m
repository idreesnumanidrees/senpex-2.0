//
//  ComingSoonVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/22.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "ComingSoonVC.h"

@interface ComingSoonVC ()

@end

@implementation ComingSoonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    self.btnComingSoon = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnComingSoon withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
}

- (IBAction)callUsAction:(id)sender {
    
    NSString *cleanedString = [[@"+1 669 777 5733" componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}


@end
