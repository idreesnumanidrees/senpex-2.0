//
//  ChatListVCViewController.m
//  CheapSender
//
//  Created by Idrees on 2017/05/02.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "ChatListVC.h"
#import "ChatListCell.h"

@interface ChatListVC ()

@property (assign, nonatomic) BOOL moreLoadOn;
@property (assign, nonatomic) int startCount;
@property (strong, nonatomic) NSMutableArray *conversationList;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *noConversationFound;

@property (assign, nonatomic) BOOL isFirstTime;

@property (weak, nonatomic) IBOutlet UIView *emptyChatView;


@end

@implementation ChatListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    IQKeyboardManager.sharedManager.enable = YES;
    
    if (_isFirstTime) {
        
        [self requestToConversationList];
    }
    
    [[[AppDelegate sharedAppDelegate].appController.senderTabbar.tabBar.items objectAtIndex:1] setBadgeValue:nil];
}

- (void)updateConversationList:(NSNotification *) notification
{
    [self requestToConversationList];
}

- (void)prepareUI
{
    //Add observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateConversationList:)
                                                 name:UPDATE_CONVERSATION_LIST object:nil];
    self.tableView.hidden = YES;
    //self.noConversationFound.hidden = YES;
    self.emptyChatView.hidden = YES;
    _startCount = 0;
    _conversationList = [NSMutableArray new];
    [self showProgressHud];
    [self requestToConversationList];
}

#pragma mark
#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_conversationList.count > 0) {
        
        self.tableView.hidden = NO;
        //self.noConversationFound.hidden = YES;
        self.emptyChatView.hidden = YES;
    } else {
        
        self.tableView.hidden = YES;
        //self.noConversationFound.hidden = NO;
        self.emptyChatView.hidden = NO;

    }
    
    return _conversationList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"ChatListCell";
    ChatListCell *cell = (ChatListCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.countView.hidden = YES;
    cell.lblMessage.textColor = [UIColor colorWithRed:161.0f/255.0f green:161.0f/255.0f blue:161.0f/255.0f alpha:1.0f];
    
    CourierConversations *courierConversation = _conversationList[indexPath.row];
    cell.lblUserName.text = courierConversation.viewName;
    cell.lblMessage.text = courierConversation.chatText;
    
    NSDate *notiDate = [NSDate dateFromDateString:courierConversation.insertedDate];
    NSString *stringDate = [UtilHelper time:notiDate];
    cell.lblDate.text = stringDate;
    
    NSString *imageUrl = courierConversation.viewImg;
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, imageUrl];
    
    //Loads image
    [cell.profileImageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
        
    //DB Operations
    LastChatMessageDB *lastMessageDB = [LastChatMessageDB getLastMessage:courierConversation.viewId];
    
    if (lastMessageDB != nil) {
        
        if (![lastMessageDB.message isEqualToString:courierConversation.chatText]) {
            
            cell.countView.hidden = NO;
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 75;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CourierConversations *courierConversation = _conversationList[indexPath.row];
    [[AppDelegate sharedAppDelegate].appController ShowChatDetailView:courierConversation PackDetails:nil];
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//    CGFloat offsetY = scrollView.contentOffset.y;
//    CGFloat contentHeight = scrollView.contentSize.height;
//
//    if (offsetY > contentHeight - 2000) {
//
//        if (_moreLoadOn) {
//
//            _moreLoadOn = NO;
//            _startCount += 10;
//            [self requestToConversationList];
//        }
//    }
//}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestToConversationList
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetChatConversations:@"100"
                                          Start:[NSString stringWithFormat:@"%d", _startCount]
                                    LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                   LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                  LogDeviceType:LOG_DEVICE_TYPE
                                     SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                              completionHandler:^(NSMutableArray *chatConversationList, NSError *error) {
                                  
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                      _isFirstTime = YES;
                                      [self hideProgressHud];
                                      
                                      if (!error) {
                                          
                                          if (chatConversationList.count > 0) {
                                              
                                              _moreLoadOn = YES;
                                              NSMutableArray *serverRatings = [NSMutableArray new];
//                                              [serverRatings addObjectsFromArray:_conversationList];
                                              [serverRatings addObjectsFromArray:chatConversationList];
                                              _conversationList = [serverRatings mutableCopy];
                                              [_tableView reloadData];
                                          }
                                          
                                      } else {
                                          
                                          if (error.code == 3) {
                                              
                                              [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                          }
                                      }
                                  });
                              }];
    });
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
    }
}


@end
