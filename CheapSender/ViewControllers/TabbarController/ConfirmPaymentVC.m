//
//  ConfirmPaymentVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/23.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "ConfirmPaymentVC.h"

@interface ConfirmPaymentVC ()

@property (weak, nonatomic) IBOutlet UIButton *btnGoToOrder;
@property (weak, nonatomic) IBOutlet UILabel *orderNoLable;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation ConfirmPaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI {
    self.btnGoToOrder = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnGoToOrder withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    
    _orderNoLable.text = [NSString stringWithFormat:@"Your order ID: %@", _packId];
    if (_asktofriend) {
        _descriptionLabel.text = @"We have sent the receiver to make the payment.";
    }
    if (_fastOrder) {
        _descriptionLabel.text = @"We sent you a temporary password. Please check your email and login review your order.";
        [_btnGoToOrder setTitle:@"Login" forState:UIControlStateNormal];
    }
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:SENDER_ACTIVE_LIST_UPDATED object:@{}];
}

#pragma mark
#pragma mark --IBActions

- (IBAction)backAction:(id)sender
{
//    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
//    
//    for (UIViewController *aViewController in allViewControllers) {
//        if ([aViewController isKindOfClass:[UITabBarController class]]) {
//            [self.navigationController popToViewController:aViewController
//                                                  animated:YES];
//        }
//    }
    
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[UITabBarController class]]) {
            
            UITabBarController *tabbar = (UITabBarController *)aViewController;
            NSString *restorIdentity = tabbar.restorationIdentifier;
            
            if ([restorIdentity isEqualToString:@"sender"]) {
                [self.navigationController popToViewController:aViewController animated:YES];
                tabbar.selectedIndex = 0;
            }
        }
    }
}

- (IBAction)goToOrderDetailAction:(id)sender {
    if (_fastOrder) {
        [[AppDelegate sharedAppDelegate].appController showLoginView];
    } else {
        [[AppDelegate sharedAppDelegate].appController showSendingDetailView:nil PackId:_packId FromReceiver:false];
    }
    
}
- (IBAction)closeAction:(UIButton *)sender {
    if (_fastOrder) {
        [[AppDelegate sharedAppDelegate].appController showLoginView];
    } else {
//        [[AppDelegate sharedAppDelegate].appController showSendingView];
        [[AppDelegate sharedAppDelegate].appController loadTabbarController];
    }
}



@end
