//
//  SendingsVC.m
//  CheapSender
//
//  Created by admin on 4/20/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SendingsVC.h"
#import "SendingsActiveCell.h"
#import "SenderPackList.h"
#import <MapKit/MapKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SVPulsingAnnotationView/SVPulsingAnnotationView.h>

@interface SendingsVC () {
    int count;
    CLLocationManager *locationManager;
    CLLocation *location;
}

//Outlets
@property (weak, nonatomic) IBOutlet UIButton *btnPlaceOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnActive;
@property (weak, nonatomic) IBOutlet UIButton *btnCompleted;
@property (weak, nonatomic) IBOutlet UIButton *btnDrafts;
@property (weak, nonatomic) IBOutlet UIButton *btnReceive;
@property (weak, nonatomic) IBOutlet UIView *emptyView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *receivebuttonWidth;

@property (weak, nonatomic) IBOutlet MKMapView *mapView_1;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

//Web related
@property (strong, nonatomic) NSMutableArray *activePacks;
@property (strong, nonatomic) NSMutableArray *completedPacks;
@property (strong, nonatomic) NSMutableArray *draftPacks;
@property (strong, nonatomic) NSMutableArray *receivePacks;
@property (assign, nonatomic) SenderPackList senderPackStatus;
@property (assign, nonatomic) BOOL moreLoadOn;
@property (assign, nonatomic) NSInteger startCount;

@end

@implementation SendingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prefersStatusBarHidden];
    
    [self prepareUI];
    locationManager = [[CLLocationManager alloc] init];
    [locationManager requestAlwaysAuthorization];
    
    if ([CLLocationManager locationServicesEnabled]) {
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.delegate = self;
        [locationManager startUpdatingLocation];
    }
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
//    [_mapView addGestureRecognizer:tapGesture];
//    _googlemapView.myLocationEnabled = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    self.btnPlaceOrder = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnPlaceOrder withRadius:_btnPlaceOrder.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    self.btnPlaceOrder.layer.masksToBounds = false;
    self.btnPlaceOrder.layer.shadowColor = UIColor.blackColor.CGColor;
    self.btnPlaceOrder.layer.shadowOpacity = 0.5;
    self.btnPlaceOrder.layer.shadowOffset = CGSizeMake(2, 2);
    self.btnPlaceOrder.layer.shadowRadius = 5;
        
    self.btnActive = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnActive withRadius:_btnActive.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    self.btnCompleted = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnCompleted withRadius:_btnCompleted.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    self.btnDrafts = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnDrafts withRadius:_btnDrafts.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    self.btnReceive = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnReceive withRadius:_btnReceive.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    count = 0;
    
    //Session key from UserDefaults
    if ([AppDelegate sharedAppDelegate].sessionKey == nil) {
        NSString *sessionKey = [UtilHelper getValueFromNSUserDefaultForKey:SESSION_KEY];
        [AppDelegate sharedAppDelegate].sessionKey = sessionKey;
    }
    
    [self firstSetUp];
    //    self.tableView.tableFooterView = [self tableFooterView];
    
    [self getAllDict];
    
    //Add observer
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(senderActiveListUpdated:) name:SENDER_ACTIVE_LIST_UPDATED object:nil];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:36.778259 longitude:-119.417931 zoom: 6];
    [_googlemapView animateToCameraPosition:camera];
    
}

- (void)firstSetUp {
    _startCount = 0;
    _tableView.editing = NO;
    _activePacks = [NSMutableArray new];
    [self showProgressHud];
    [self selectionOfButton:self.btnActive];
    _senderPackStatus = SENDER_ACTIVE;
    [self requestSenderPacks:_senderPackStatus];
}

- (void)senderActiveListUpdated:(NSNotification *) notification {
    if (_senderPackStatus == SENDER_ACTIVE)
        [self activeAction:_btnActive];
    else if (_senderPackStatus == SENDER_COMPLETED)
        [self completedAction:_btnCompleted];
    else if (_senderPackStatus == SENDER_DRAFT)
        [self draftAction:_btnDrafts];
    else if (_senderPackStatus == SENDER_RECEIVE)
        [self receiveAction:_btnReceive];
}

#pragma mark
#pragma mark --Actions
- (IBAction)placeNewOrderAction:(id)sender {
//    [[AppDelegate sharedAppDelegate].appController showNewOrderView:YES];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]) {
        NSString *send_cat_id = [[NSString alloc]initWithFormat:@"%d", (int)2];
        [[AppDelegate sharedAppDelegate].appController showNewOrderPackInfoView:@"" Send_Cat_ID:send_cat_id SenderData:nil isBackHidden:YES];
    } else {
        //        [self createPackRequest];
        NSString *send_cat_id = [[NSString alloc]initWithFormat:@"%d", (int)2];
        [[AppDelegate sharedAppDelegate].appController showNewOrderPackInfoView:@"" Send_Cat_ID:send_cat_id SenderData:nil isBackHidden:YES];
    }
}

- (IBAction)activeAction:(id)sender {
    _emptyView.hidden = true;
    [self.view layoutIfNeeded];
    _googlemapView.hidden = false;
    _startCount = 0;
    _tableView.editing = NO;
    _activePacks = [NSMutableArray new];
    _senderPackStatus = SENDER_ACTIVE;
    [self selectionOfButton:(UIButton *)sender];
    [_tableView reloadData];
    
    [self showProgressHud];
    [self requestSenderPacks:_senderPackStatus];
}

- (IBAction)completedAction:(id)sender {
    _emptyView.hidden = false;
    _googlemapView.hidden = true;
    _startCount = 0;
    _tableView.editing = NO;
    _completedPacks = [NSMutableArray new];
    _senderPackStatus = SENDER_COMPLETED;

    [self selectionOfButton:(UIButton *)sender];
    [_tableView reloadData];
    
    [self showProgressHud];
    [self requestSenderPacks:_senderPackStatus];
}

- (IBAction)draftAction:(id)sender {
    _emptyView.hidden = false;
    _googlemapView.hidden = true;
    _startCount = 0;
    _tableView.editing = NO;
    _draftPacks = [NSMutableArray new];
    _senderPackStatus = SENDER_DRAFT;
    [self selectionOfButton:(UIButton *)sender];
    [_tableView reloadData];
    
    [self showProgressHud];
    [self requestSenderPacks:_senderPackStatus];
}
- (IBAction)receiveAction:(UIButton *)sender {
    _emptyView.hidden = false;
    _googlemapView.hidden = true;
    _startCount = 0;
    _tableView.editing = NO;
    _receivePacks = [NSMutableArray new];
    _senderPackStatus = SENDER_RECEIVE;
    [self selectionOfButton:(UIButton *)sender];
    [_tableView reloadData];
    
    [self showProgressHud];
    [self requestSenderPacks:_senderPackStatus];
}

- (void)selectionOfButton:(UIButton *)button {
    self.btnActive.selected = NO;
    self.btnCompleted.selected = NO;
    self.btnDrafts.selected = NO;
    self.btnReceive.selected = NO;
    button.selected = YES;
}

#pragma mark
#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_senderPackStatus == SENDER_ACTIVE) {
        count = (int)_activePacks.count;
        _startCount = count;
    } else if (_senderPackStatus == SENDER_COMPLETED) {
        count = (int)_completedPacks.count;
        _startCount = count;
    }else if (_senderPackStatus == SENDER_DRAFT) {
        count = (int)_draftPacks.count;
        _startCount = count;
    } else if (_senderPackStatus == SENDER_RECEIVE) {
        count = (int)_receivePacks.count;
        _startCount = count;
    }

    if (count != 0){
        _emptyView.hidden = true;
        tableView.hidden = false;
    } else {
        tableView.hidden = true;
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"SendingsActiveCell";
    
    SendingsActiveCell *cell = (SendingsActiveCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SendingsActiveCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    SenderData *senderPackData;
    
    if (_senderPackStatus == SENDER_ACTIVE)
        senderPackData = _activePacks[indexPath.row];
    else if (_senderPackStatus == SENDER_COMPLETED)
        senderPackData = _completedPacks[indexPath.row];
    else if (_senderPackStatus == SENDER_DRAFT)
        senderPackData = _draftPacks[indexPath.row];
    else if (_senderPackStatus == SENDER_RECEIVE)
        senderPackData = _receivePacks[indexPath.row];
    
    cell.senderTextLabel.text = senderPackData.sendName;
    
    if (senderPackData.packFromText == nil)
        cell.senderFromLabel.text = @"Place not Assigned";
    else
        cell.senderFromLabel.text = senderPackData.packFromText;
    
    if (senderPackData.packToText == nil)
        cell.senderToLabel.text = @"Place not Assigned";
    else
        cell.senderToLabel.text = senderPackData.packToText;
    
    cell.priceLabel.hidden = senderPackData.packPrice == nil;
    
    cell.priceLabel.text = [NSString stringWithFormat:@"$%@",senderPackData.packPrice];
    cell.packStatusLabel.text = senderPackData.statusSender;
    
    NSLog(@"package inserted time %@", senderPackData.insertedDate);
    NSDate *date = [NSDate dateFromDateString:senderPackData.insertedDate];
    cell.dateTimeLabel.text = [date getDateTimeInLocalTimeZone];
    
    //Shows indicatore on imageView
    [cell.imgPackImage setShowActivityIndicatorView:YES];
    [cell.imgPackImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *imageUrl = senderPackData.packImg;
    NSString *imageExtension = [UtilHelper getImageFileType:imageUrl];
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@.thumb.%@", DOWNLOAD_BASE_IMAGE_URL, imageUrl,imageExtension];
    
    //Loads image
    [cell.imgPackImage sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"packing-Icon"] options:SDWebImageRefreshCached];
    
    //Pack status colors
    cell.packStatusLabel.textColor = ORANGE_LINE_COLOR;
    
    if ([senderPackData.packStatus isEqualToString:@"10"] || [senderPackData.packStatus isEqualToString:@"0"])
        cell.packStatusLabel.textColor = ORANGE_LINE_COLOR;
    else if ([senderPackData.packStatus isEqualToString:@"20"] || [senderPackData.packStatus isEqualToString:@"40"] || [senderPackData.packStatus isEqualToString:@"30"])
        cell.packStatusLabel.textColor = PACK_GREEN_STATUS_COLOR;
    else if ([senderPackData.packStatus isEqualToString:@"100"] || [senderPackData.packStatus isEqualToString:@"90"])
        cell.packStatusLabel.textColor = PACK_RED_STATUS_COLOR;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 143;
}

- (UIView *)tableFooterView {
    UIView *headerView = [[UIView alloc] init];
    [headerView setBackgroundColor:[UIColor clearColor]];
    
    headerView.frame = CGRectMake(0, 0, self.tableView.frame.size.width, 40);
    
    UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [indicator startAnimating];
    [indicator setCenter:headerView.center];
    [indicator setColor:ORANGE_COLOR];
    
    indicator.hidesWhenStopped = YES;
    indicator.tag = 1;
    
    indicator.frame = CGRectMake((self.tableView.frame.size.width - 40)/2, 0 , 40, 40);
    [headerView addSubview:indicator];
    
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    SenderData *senderPackData;
    
    if (_senderPackStatus == SENDER_ACTIVE) {
        senderPackData = _activePacks[indexPath.row];
        [[AppDelegate sharedAppDelegate].appController showSendingDetailView:senderPackData PackId:senderPackData.dataIdentifier FromReceiver:false];
    } else if (_senderPackStatus == SENDER_COMPLETED) {
        senderPackData = _completedPacks[indexPath.row];
        [[AppDelegate sharedAppDelegate].appController showSendingDetailView:senderPackData PackId:senderPackData.dataIdentifier FromReceiver:false];
    }else if (_senderPackStatus == SENDER_DRAFT) {
        senderPackData = _draftPacks[indexPath.row];
        
//        if ([senderPackData.sendCatText isEqualToString:@"Pet"])
//            [[AppDelegate sharedAppDelegate].appController showNewOrderPetInfoView:EMPTY_STRING SenderData:senderPackData];
//        else if ([senderPackData.sendCatText isEqualToString:@"Package"])
        if ([senderPackData.packStatus isEqualToString:@"5"]) {
            [[AppDelegate sharedAppDelegate].appController showSendingDetailView:senderPackData PackId:senderPackData.dataIdentifier FromReceiver:false];
        } else {
            [[AppDelegate sharedAppDelegate].appController showNewOrderPackInfoView:EMPTY_STRING Send_Cat_ID:senderPackData.sendCatId SenderData:senderPackData isBackHidden:YES];
        }
        
    } else if (_senderPackStatus == SENDER_RECEIVE) {
        senderPackData = _receivePacks[indexPath.row];
//        [[AppDelegate sharedAppDelegate].appController showSendingDetailView:senderPackData PackId:senderPackData.dataIdentifier];
        if ([senderPackData.packStatus isEqualToString:@"5"]) {
            [[AppDelegate sharedAppDelegate].appController showNewOrderDestinationView:senderPackData.dataIdentifier PackPrice:senderPackData.packPrice Send_Cat_ID:senderPackData.sendCatId ItemPrice:senderPackData.itemValue SenderData:senderPackData];
        } else {
            [[AppDelegate sharedAppDelegate].appController showSendingDetailView:senderPackData PackId:senderPackData.dataIdentifier FromReceiver:true];
        }
        
    }
}

//Edit Stuff
- (NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Delete"  handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        //insert your deleteAction here
        SenderData *senderPackData = _draftPacks[indexPath.row];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            [WebServicesClient DeleteMyDraftPack:senderPackData.dataIdentifier
                                   logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                     LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                    LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                   LogDeviceType:LOG_DEVICE_TYPE
                               completionHandler:^(BOOL isDeleted, NSError *error) {
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                       if (!error)
                                           [[NSNotificationCenter defaultCenter] postNotificationName:@"ORDERPLACED"  object:nil];
                                       else if (error.code == 3)
                                           [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                   });
                               }];
        });
        
        [_draftPacks removeObject:senderPackData];
        [_tableView reloadData];
    }];
    
    deleteAction.backgroundColor = ORANGE_LINE_COLOR;
    return @[deleteAction];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete)
        NSLog(@"delete");
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return _senderPackStatus == SENDER_DRAFT;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    CGFloat contentHeight = scrollView.contentSize.height;
    
    if (offsetY > contentHeight - 2500) {
        if (_moreLoadOn) {
            _moreLoadOn = NO;
            if (_senderPackStatus == SENDER_ACTIVE) {
                _startCount = _activePacks.count;
            } else if (_senderPackStatus == SENDER_COMPLETED) {
                _startCount = _completedPacks.count;
            }else if (_senderPackStatus == SENDER_DRAFT) {
                _startCount = (int)_draftPacks.count;
            } else if (_senderPackStatus == SENDER_RECEIVE) {
                _startCount = (int)_receivePacks.count;
            }
            [self requestSenderPacks:_senderPackStatus];
        }
    }
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestSenderPacks:(SenderPackList)senderPack {
    
    if (![UtilHelper isNetworkAvailable]) {
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        [self hideProgressHud];
        return;
    }
    
    [WebServicesClient SenderGetPacksList:[NSString stringWithFormat:@"%i", _startCount]
                                    count:@"10"
                                     List:senderPack
                              LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                             LogUserAgent:[[UIDevice currentDevice] systemVersion]
                            LogDeviceType:LOG_DEVICE_TYPE
                               SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                        completionHandler:^(SenderPacksList *senderPackList, NSError *error) {
                            if (!error) {
                                if (self.senderPackStatus == senderPack) {
                                    [self hideProgressHud];
                                    
                                    self.moreLoadOn = YES;
                                    NSMutableArray *packs = [NSMutableArray new];
                                    if (self.senderPackStatus == SENDER_ACTIVE) {
                                        [packs addObjectsFromArray:self.activePacks];
                                        [packs addObjectsFromArray:senderPackList.data];
                                        self.activePacks = [packs mutableCopy];
                                    } else if (self.senderPackStatus == SENDER_COMPLETED) {
                                        [packs addObjectsFromArray:self.completedPacks];
                                        [packs addObjectsFromArray:senderPackList.data];
                                        self.completedPacks = [packs mutableCopy];
                                    } else if (self.senderPackStatus == SENDER_DRAFT) {
                                        [packs addObjectsFromArray:self.draftPacks];
                                        [packs addObjectsFromArray:senderPackList.data];
                                        self.draftPacks = [packs mutableCopy];
                                    } else if (self.senderPackStatus == SENDER_RECEIVE) {
                                        [packs addObjectsFromArray:self.receivePacks];
                                        [packs addObjectsFromArray:senderPackList.data];
                                        self.receivePacks = [packs mutableCopy];
                                    }
                                    [self.tableView reloadData];
                                }
                            } else {
                                [self hideProgressHud];
                                if (error.code == 3)
                                    [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                            }
                        }];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1001)
        [self senderLogout];
}

// Below by Tarik 3/25
// CLLocationManager Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations {
    if (locations.firstObject != nil) {
        location = locations.firstObject;
//        [[CLGeocoder alloc] reverseGeocodeLocation:location completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
//            if (error != nil) return;
//            if (placemarks != nil && placemarks.count > 0)
//                [locationManager stopUpdatingLocation];
//            [self centerMapOnLocation:location];
//        }];
    }
    
    CLLocation *location1 = locations.lastObject;

//    GMSCameraPosition *camera = GMSCameraPosition.cameraWithLatitude((location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude!, zoom 17.0;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location1.coordinate.latitude longitude:location1.coordinate.longitude zoom: 11.5];
    [_googlemapView animateToCameraPosition:camera];
    
    CLLocationCoordinate2D position = CLLocationCoordinate2DMake(location1.coordinate.latitude, location1.coordinate.longitude);
    GMSMarker * marker = [GMSMarker markerWithPosition:position];
    marker.icon = [UIImage imageNamed: @"annotation"];
    marker.groundAnchor = CGPointMake(0.5, 0.5);
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = _googlemapView;

    [locationManager stopUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusDenied) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Location Access Denied" message:@"This app needs to access your location" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
        UIAlertAction *openAction = [UIAlertAction actionWithTitle:@"Go to Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
        }];
        
        [alertController addAction: cancelAction];
        [alertController addAction: openAction];
        [self presentViewController:alertController animated:true completion:nil];
    }
}

-(void)centerMapOnLocation:(CLLocation*)location {
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location.coordinate, 10000, 10000);
//    [_googlemapView setRegion:region animated:true];
}

-(void)handleTap:(UITapGestureRecognizer*)tapGesture{
//    [_mapView removeAnnotations:_mapView.annotations];
//    CGPoint point = [tapGesture locationInView:_mapView];
//    CLLocationCoordinate2D coordinate = [_mapView convertPoint:point toCoordinateFromView:_mapView];
    MKPointAnnotation* annotation = [[MKPointAnnotation alloc]init];
//    annotation.coordinate = coordinate;
//    [_mapView addAnnotation:annotation];
//    [self centerMapOnLocation: [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude]];
}

-(MKAnnotationView*)mapView:(MKMapView*)mapView viewForAnnotation:(nonnull id<MKAnnotation>)annotation {
//    SVPulsingAnnotationView *pulsingView = (SVPulsingAnnotationView*)[_mapView dequeueReusableAnnotationViewWithIdentifier:@"annotation"];
//    if (pulsingView == nil)
//        pulsingView = [[SVPulsingAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"annotation"];
//    else
//        pulsingView.annotation = annotation;
//    pulsingView.annotationColor = [UIColor colorWithRed:0 green:0.678 blue:0 alpha:1];
//    pulsingView.canShowCallout = true;
//
//    if ([annotation isKindOfClass:[MKUserLocation classForCoder]])
//        return nil;
//
//    for (UIView *view in [pulsingView subviews])
//        [view removeFromSuperview];
//
//    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 35, 35)];
//    imageView.image = [UIImage imageNamed:@"annotation"];
//    imageView.contentMode = UIViewContentModeScaleAspectFit;
//    [pulsingView addSubview:imageView];
//    CGPoint center = pulsingView.center;
//    pulsingView.frame = imageView.frame;
//    pulsingView.center = center;
//    return pulsingView;
    return nil;
}

@end
