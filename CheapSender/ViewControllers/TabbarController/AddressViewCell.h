//
//  AddressViewCell.h
//  CheapSender
//
//  Created by Monkey on 4/3/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVPlaceSearchTextField.h"

@interface AddressViewCell : UITableViewCell<PlaceSearchTextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *ovalImageView;
@property (strong, nonatomic) IBOutlet UIImageView *topDotsImageView;
@property (strong, nonatomic) IBOutlet UIImageView *botDotsImageView;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnRemove;
@property (strong, nonatomic) IBOutlet UILabel *lblSeparator;
//@property (strong, nonatomic) IBOutlet MVPlaceSearchTextField *addressEditor;

@end
