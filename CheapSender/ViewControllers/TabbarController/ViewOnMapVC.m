//
//  ViewOnMapVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/03.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "ViewOnMapVC.h"
#import "UpdateCourierLocationMarker.h"

@interface ViewOnMapVC ()<UpdateCourierLocationMarkerDelegate>

@property (nonatomic, strong) CSUser *courierMarker;
@property (strong, nonatomic) UpdateCourierLocationMarker *updateCourierLocationMarker;
@property (nonatomic, strong) GMSPolyline *polyline;

@property (strong, nonatomic) NSString *distance;
@property (strong, nonatomic) NSString *distanceTime;
@property (nonatomic, strong) NSTimer *updateCourierLocationPeriodicallyTimer;

@end

#define FETCH_COURIER_LOCATION_INTERVAL 60.0

@implementation ViewOnMapVC

//This method will start the thread for update location timer.ƒ
- (void)startFetchingCourierLocation
{
    [self GetCourierGeoLocation];
    //Timer invalidate check
//    if(![_updateCourierLocationPeriodicallyTimer isValid]) {
//        //Creates and returns a new NSTimer object and schedules it on the current run loop in the default mode.
//        _updateCourierLocationPeriodicallyTimer = [NSTimer scheduledTimerWithTimeInterval:FETCH_COURIER_LOCATION_INTERVAL
//                                                                                   target:self
//                                                                                 selector:@selector(GetCourierGeoLocation)
//                                                                                 userInfo:nil
//                                                                                  repeats:YES];
//    }
}

//This method will stop the thread for update location timer.
- (void)stopFetchingCourierLocation {
    [_updateCourierLocationPeriodicallyTimer invalidate];
    _updateCourierLocationPeriodicallyTimer = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _etaLabel.text = @"Time: ";
    _milesLabel.text = @"Miles: ";
    
    [self prepareUI];
    if ([_packDetails.packStatus isEqualToString:@"20"] || [_packDetails.packStatus isEqualToString:@"30"]) {
        NSInteger userInfo = [[UtilHelper getValueFromNSUserDefaultForKey:LOGON_USER] intValue];
        if (userInfo == 1)
            [self startFetchingCourierLocation];
        
        _lblHeadingTitle.text = @"Live Tracking";
    } else {
        
        _lblHeadingTitle.text = @"Map View";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_updateCourierLocationMarker stopUpdateLocationThread];
    [self stopFetchingCourierLocation];
}

#pragma mark -
#pragma mark - Helpers

- (void)prepareUI
{
    self.etaView = [UtilHelper makeCornerCurved:self.etaView withRadius:10.0 color:COMMON_ICONS_COLOR width:0];
    
    _markerArray = [NSMutableArray new];
    
    //Initialize the mapview
    [self initializeMapView];
    
    //Please dont comment this code, I am using this for driver side, you can do your work in else part if you have any problem, or check the isComingFromCourierApp status.
    if (_isComingFromCourierApp) {
        
        _navigationView.backgroundColor = [UIColor colorWithRed:67.0f/255.0f green:84.0f/255.0f blue:108.0f/255.0f alpha:1.0f];
        [self setStatusBarBackgroundColor:[UIColor colorWithRed:67.0f/255.0f green:84.0f/255.0f blue:108.0f/255.0f alpha:1.0f]];
    }
    
    if (_courierListData != nil) {
    } else
        [self addMarkers];
}

//Initializing and allocating periodicWebCallManager class
- (void)initializeMapView {
    //Assigning delegates
    self.googleMapView.delegate = self;
    [self.googleMapView initWithCustomStyle];
}

//Initializing and allocating current location manager class
- (void)initializeUpdateLocationPeriodically
{
    //Allocating and Initializing LocationManagerService class
    _updateCourierLocationMarker = [UpdateCourierLocationMarker new];
    
    //Assigning delegates of LocationManagerService
    _updateCourierLocationMarker.updateCourierLocationMarkerDelegate = self;
    
    //Initialize setup method for CLLocation
    [_updateCourierLocationMarker startUpdateLocationThread];
}

- (void)addMarkers {
    
    CSUser *deliveryMarker = [CSUser new];
    CSUser *packMarker = [CSUser new];
    _courierMarker = [CSUser new];
    
    if (_packDetails != nil) {
        
        [self drawRoute];
        
        _fromAddressLabel.text = _packDetails.packFromText;
        _toAddressLabel.text = _packDetails.packToText;
        _etaView.hidden = NO;
        
        if ([_comingView isEqualToString:@"track"]) {
        
            [self initializeUpdateLocationPeriodically];
            
            _etaLabel.text = @"In location";
            _milesLabel.text = @"In location";
            
            _courierMarker.userId = 2;
            _courierMarker.imageName = @"Courier-marker-icon-1";
            _courierMarker.latitude = [_packDetails.cLastLat doubleValue];
            _courierMarker.longitude = [_packDetails.cLastLng doubleValue];
            
            CLLocation *user = [[CLLocation alloc] initWithLatitude:[_packDetails.cLastLat doubleValue] longitude:[_packDetails.cLastLng doubleValue]];
            
            if ([_packDetails.packStatus isEqualToString:@"20"]) {
                
                CLLocation *source = [[CLLocation alloc] initWithLatitude:[_packDetails.packFromLat doubleValue] longitude:[_packDetails.packFromLng doubleValue]];
                
                [self drawRouteWithBlackLine:user SourceLocation:source];
                [self calculateDistanceAndTimeBetweenLocations:user destination:source];

                _courierMarker.marker = [_googleMapView addMapPinForUser:_courierMarker];
                [_markerArray addObject:_courierMarker];
            }
            else if ([_packDetails.packStatus isEqualToString:@"30"]) {
                
                CLLocation *source = [[CLLocation alloc] initWithLatitude:[_packDetails.packToLat doubleValue] longitude:[_packDetails.packToLng doubleValue]];
                
                [self drawRouteWithBlackLine:user SourceLocation:source];
                [self calculateDistanceAndTimeBetweenLocations:user destination:source];

                _courierMarker.marker = [_googleMapView addMapPinForUser:_courierMarker];
                [_markerArray addObject:_courierMarker];
            } else {
                _etaLabel.text = [NSString stringWithFormat:@"Time: %d mins", [_packDetails.distanceTime intValue]/60];
                _milesLabel.text = [NSString stringWithFormat:@"Miles: %@", _packDetails.distance];
            }
//            [_markerArray addObject:_courierMarker];
            
        } else {
            
            _etaLabel.text = [NSString stringWithFormat:@"Time: %d mins", [_packDetails.distanceTime intValue]/60];
            _milesLabel.text = [NSString stringWithFormat:@"Miles: %@", _packDetails.distance];
        }
        
        deliveryMarker.userId = 0;
        deliveryMarker.imageName = @"delivered-location-marker";
        deliveryMarker.latitude = [_packDetails.packToLat doubleValue];
        deliveryMarker.longitude = [_packDetails.packToLng doubleValue];
        deliveryMarker.marker = [_googleMapView addMapPinForUser:deliveryMarker];
        
        packMarker.userId = 1;
        packMarker.imageName = @"pick-location-marker";
        packMarker.latitude = [_packDetails.packFromLat doubleValue];
        packMarker.longitude = [_packDetails.packFromLng doubleValue];
        packMarker.marker = [_googleMapView addMapPinForUser:packMarker];
        
        [_markerArray addObject:packMarker];
        [_markerArray addObject:deliveryMarker];
        
        //GMSCoordinateBounds represents a rectangular bounding box on the Earth's surface.
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        
        for (CSUser *user in self.markerArray) {
            
            //Returns a GMSCoordinateBounds representing the current bounds extended to include the passed-in coordinate
            if((user.latitude != 0 && user.longitude != 0)) {
                bounds = [bounds includingCoordinate:user.marker.position];
            }
        }
        _googleMapView.padding = UIEdgeInsetsMake(140, 0, 135, 0);
        [self.googleMapView showAllMarkersInMapViewWithBound:bounds];
    }
}

- (void)drawRoute {
    
    CLLocation *userPack = [[CLLocation alloc] initWithLatitude:[_packDetails.packFromLat doubleValue] longitude:[_packDetails.packFromLng doubleValue]];
    
    CLLocation *deliveryPack = [[CLLocation alloc] initWithLatitude:[_packDetails.packToLat doubleValue] longitude:[_packDetails.packToLng doubleValue]];
    
    [self.googleMapView fetchPolylineWithOrigin:userPack color:ORANGE_LINE_COLOR destination:deliveryPack completionHandler:^(GMSPolyline *polyline) {
         if(polyline) {
             polyline.map = nil;
             polyline.map = self.googleMapView;
         }
     }];
}

- (void)drawRouteWithBlackLine:(CLLocation *)userLocation SourceLocation:(CLLocation *)sourceLocation {
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //back ground thread
        
        [self.googleMapView fetchPolylineWithOrigin:userLocation color:MARKER_BLACK_LINE destination:sourceLocation completionHandler:^(GMSPolyline *polyline) {
            if(polyline) {
                
                dispatch_async( dispatch_get_main_queue(), ^{
                    // main thread
                    self.polyline.map = nil;
                    self.polyline = polyline;
                    self.polyline.map = self.googleMapView;
                });
            }
        }];
    });
}

#pragma mark
#pragma mark -- Location update delegates

- (void)notifyForUpdateCourierMarker {
    
    NSInteger userInfo = [[UtilHelper getValueFromNSUserDefaultForKey:LOGON_USER] intValue];
    if (userInfo == 1) {
        //[self GetCourierGeoLocation];
    } else
        [self updateCourierLocation];
}

- (void)updateCourierLocation {
    
    if ([AppDelegate sharedAppDelegate].coureirLocation != nil) {
        
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([AppDelegate sharedAppDelegate].coureirLocation.coordinate.latitude, [AppDelegate sharedAppDelegate].coureirLocation.coordinate.longitude);
        
        if ([_packDetails.packStatus isEqualToString:@"20"]) {
            // courier selected
            CLLocation *source = [[CLLocation alloc] initWithLatitude:[_packDetails.packFromLat doubleValue] longitude:[_packDetails.packFromLng doubleValue]];
            
            [self drawRouteWithBlackLine:[AppDelegate sharedAppDelegate].coureirLocation SourceLocation:source];
            
            [self calculateDistanceAndTimeBetweenLocations:[AppDelegate sharedAppDelegate].coureirLocation destination:source];
        }
        else if ([_packDetails.packStatus isEqualToString:@"30"]) {
            //pack taken
            CLLocation *source = [[CLLocation alloc] initWithLatitude:[_packDetails.packToLat doubleValue] longitude:[_packDetails.packToLng doubleValue]];
            [self drawRouteWithBlackLine:[AppDelegate sharedAppDelegate].coureirLocation SourceLocation:source];
            [self calculateDistanceAndTimeBetweenLocations:[AppDelegate sharedAppDelegate].coureirLocation destination:source];
        }
        _courierMarker.marker.position = coordinate;
    }
}

- (void)GetCourierGeoLocation {
    
    [WebServicesClient GetCourierGeoLocation:_packDetails.acceptedCourierId
                               logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                 LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                LogUserAgent:[[UIDevice currentDevice] systemVersion]
                               LogDeviceType:LOG_DEVICE_TYPE
                           completionHandler:^(NSDictionary* locationDict, NSError *error) {
                               
                               if (!error) {
                                   if (locationDict != nil) {
                                       CLLocation *user = [[CLLocation alloc] initWithLatitude:[locationDict[@"last_lat"] doubleValue] longitude:[locationDict[@"last_lng"] doubleValue]];
                                       
                                       if ([self.packDetails.packStatus isEqualToString:@"20"]) {
                                           //courier selected
                                           CLLocation *source = [[CLLocation alloc] initWithLatitude:[self.packDetails.packFromLat doubleValue] longitude:[_packDetails.packFromLng doubleValue]];
                                           [self drawRouteWithBlackLine:user SourceLocation:source];
                                           
                                           [self calculateDistanceAndTimeBetweenLocations:user destination:source];
                                           
                                       } else if ([_packDetails.packStatus isEqualToString:@"30"]) {
                                           //pack taken
                                           CLLocation *source = [[CLLocation alloc] initWithLatitude:[_packDetails.packToLat doubleValue] longitude:[_packDetails.packToLng doubleValue]];
                                           
                                           [self drawRouteWithBlackLine:user SourceLocation:source];
                                           [self calculateDistanceAndTimeBetweenLocations:user destination:source];
                                       }
                                       
                                       CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(user.coordinate.latitude, user.coordinate.longitude);
                                       _courierMarker.marker.position = coordinate;
                                   }
                               } else if (error.code == 3)
                                   [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                           }];
}

- (void)calculateDistanceAndTimeBetweenLocations:(CLLocation *)source destination:(CLLocation *)destination {
    
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        //back ground thread
        
        [self->_googleMapView getDistanceAndTimeTaken:source.coordinate.latitude
                              UserFromLongitude:source.coordinate.longitude
                                 UserToLatitude:destination.coordinate.latitude
                                UserToLongitude:destination.coordinate.longitude
                                 withCompletion:^(NSDictionary *lastDict) {
                                     
                                     // main thread
                                     if (lastDict != nil) {
                                         
                                         SPDistanceTime *spDistanceTime = [self->_googleMapView parseDistaneTimeFromGoogleAPIResponse:lastDict];
                                         self->_distance = spDistanceTime.distance;
                                         self->_distanceTime = spDistanceTime.duration;
                                         
                                         dispatch_async(dispatch_get_main_queue(), ^{
                                             
                                             if(self->_distance == nil || _distanceTime == nil || [_distance intValue] == 0) {
                                             } else {
                                                 self->_etaView.hidden = NO;
                                                 _etaLabel.text = [NSString stringWithFormat:@"Time: %d mins", [_packDetails.distanceTime intValue]/60];
                                                 self->_milesLabel.text = [NSString stringWithFormat:@"Miles: %@", _distance];
                                             }
                                         });
                                     }
                                 }];
    });
}
@end
