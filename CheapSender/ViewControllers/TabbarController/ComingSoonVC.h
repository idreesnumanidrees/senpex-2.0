//
//  ComingSoonVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/22.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface ComingSoonVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *btnComingSoon;

@end
