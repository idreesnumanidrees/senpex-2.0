//
//  OrderDestinationVC.m
//  CheapSender
//
//  Created by admin on 4/23/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "OrderDestinationVC.h"
#import "PlacesViewController.h"
#import "SPDistanceTime.h"
#import "AddImageObject.h"
#import "SPPack.h"

@interface OrderDestinationVC () {
    
    CalendarHomeViewController *calendarHomeView;
    CalendarPopup *calendarPopup;
    UIView *hideView;
    UIDatePicker *datepicker;
    UIButton * doneBtn;
    NSString *takentime;
    NSString *takendate;
    NSString *TakenTime;
}

//Web related
@property (strong, nonatomic) NSString *takenType;
@property (strong, nonatomic) NSString *deliveryType;
@property (strong, nonatomic) NSString *takenAsap;
@property (strong, nonatomic) NSString *deliveryAsap;

@property (strong, nonatomic) NSString *tempDate;
@property (strong, nonatomic) NSString *tempTime;
@property (strong, nonatomic) NSString *tempTillOn;

@property (strong, nonatomic) NSString *takenDateTimeForWeb;
@property (strong, nonatomic) NSString *deliveryDateTimeForWeb;
@property (strong, nonatomic) NSString *stripeToken;

@property (assign, nonatomic) BOOL isFromAddress;
@property (assign, nonatomic) BOOL isToAddress;
@property (assign, nonatomic) BOOL isTakenTime;
@property (assign, nonatomic) BOOL isDeliveryTime;
@property (assign, nonatomic) BOOL isStripeTokenAdded;

@property (assign, nonatomic) BOOL isMainTapTakenTime;
@property (assign, nonatomic) BOOL isTimeSelected;
@property (strong, nonatomic) SPPack *pack;

@property (assign, nonatomic) BOOL isfirst;

@end

@implementation OrderDestinationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _selectedMediaArray = [NSMutableArray new];
    _pack = [SPPack new];
    
    NSDate *date = [NSDate date];
    TakenTime = [date getDateTimeInUTC];
    
    calendarPopup = [[[NSBundle mainBundle] loadNibNamed:@"CalendarPopUp" owner:nil options:nil] objectAtIndex:0];
    
    calendarPopup.calendar.delegate = self;
    calendarPopup.calendar.dataSource = self;
    calendarPopup.calendar.appearance.titleTodayColor = UIColor.blackColor;
    calendarPopup.calendar.appearance.todayColor = nil;
    
    datepicker = [[UIDatePicker alloc] init];
    datepicker.backgroundColor = UIColor.whiteColor;
    datepicker.frame = CGRectMake(calendarPopup.frame.origin.x, calendarPopup.frame.origin.y, calendarPopup.frame.size.width, calendarPopup.frame.size.height - 40);
    datepicker.datePickerMode = UIDatePickerModeTime;
    
    doneBtn = [[UIButton alloc] init];
    doneBtn.frame = CGRectMake(datepicker.frame.origin.x, datepicker.frame.origin.y + datepicker.frame.size.height, datepicker.frame.size.width, 40);
    doneBtn.backgroundColor = UIColor.whiteColor;
    [doneBtn setTitle:@"Done" forState:UIControlStateNormal];
    [doneBtn setTitleColor:UIColor.blueColor forState:UIControlStateNormal];
    [doneBtn addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
    if (cardInfo != nil) {
        if (![cardInfo.card isEqualToString:EMPTY_STRING]) {
            [_btnAddPymentMethod setTitle:[NSString stringWithFormat:@"%@ Edit", cardInfo.card] forState:UIControlStateNormal];
            NSString *decryptedToken = [FBEncryptorAES decryptBase64String:cardInfo.stripeToken keyString:@"token"];
            _stripeToken = decryptedToken;
            _isStripeTokenAdded = YES;
        }
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"] && self.isfirst && !_btnByReceiver.selected) {
        [_btnAddPymentMethod setTitle:@"+ Insert Credit Card" forState:UIControlStateNormal];
        self.isfirst = false;
        _isStripeTokenAdded = NO;
    }
    [self checkActiveButton:@"a"];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        if (screenRect.size.height > 667) {
            
            if (screenRect.size.height != 812) {
                
                CGRect frame = self.driverNotesTextView.placeHolderLabel.frame;
                frame.origin.y = -10;
                self.driverNotesTextView.placeHolderLabel.frame = frame;
            }
            
            if (screenRect.size.height == 896) {
                
                CGRect frame = self.driverNotesTextView.placeHolderLabel.frame;
                frame.origin.y = -10;
                self.driverNotesTextView.placeHolderLabel.frame = frame;
            }
        }
    });
}

#pragma mark -
#pragma mark - Helpers

- (void)prepareUI {
    
    _descriptionSpacing.constant = _driverNotesTextView.frame.size.height + 90;
    _driverNotesTextView.minHeight = 80;

    Boolean makeFastOrder = [[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"];
    self.isfirst = true;
    
    _purchaseItemSpacing.constant = 0;
    if ([_send_cat_id isEqualToString:@"1"]) {
        [_iconView setImage:[UIImage imageNamed:@"ups-status"]];
    } else if ([_send_cat_id isEqualToString:@"3"]) {
        [_iconView setImage:[UIImage imageNamed:@"deliverIcon"]];
        _purchaseItemSpacing.constant = 44;
        _insuranceView.hidden = false;
    } else if ([_send_cat_id isEqualToString:@"4"]) {
        [_iconView setImage:[UIImage imageNamed:@"returnIcon"]];
    } else {
        _purchaseItemSpacing.constant = 0;
    }
    
    if (makeFastOrder){
        _collectionView.hidden = true;
        _collectionView.frame = CGRectMake(_collectionView.frame.origin.x, _collectionView.frame.origin.y, _collectionView.frame.size.width, 0);
        [self.view layoutIfNeeded];
        _descriptionSpacing.constant -= 90;
        _userInfoSpacing.constant = 100;
        //        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"MakeFastOrder"];
    } else {
        _userInfoSpacing.constant = 0;
        _tfYourName.hidden = true;
        _tfYourEmail.hidden = true;
        _tfYourSurName.hidden = true;
        _tfCellNumber.hidden = true;
    }
    //    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"PurchaseAndDeliver"]) {
    //        _purchaseItemSpacing.constant = 44;
    //    } else {
    //        _purchaseItemSpacing.constant = 0;
    //        _insuranceView.hidden = true;
    //    }
    _driverNotesTextView.editable = true;
    
    _btnPayNow = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnPayNow withRadius:_btnPayNow.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    [_btnItemPrice setImage:[UIImage imageNamed:@"CheckWhiteUnsel"] forState:UIControlStateNormal];
    [_btnItemPrice setImage:[UIImage imageNamed:@"CheckWhiteSel"] forState:UIControlStateSelected];
    
    _lblPrice.text = [NSString stringWithFormat:@"$%@", [self getshowString:_price]];
    _lblItemPrice.text = [NSString stringWithFormat:@"$%@", _item_value];
    
    //Bottom view settings
//    _btnPayNow.selected = YES;
    _btnPayNow.backgroundColor = OFFLINE_BUTTON_SELECTION;
    
    //    _lblIndicator.hidden = true;
    _tfEmail.enabled = false;
    
    // set First charactor to not unpercase
    _tfEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
    _tfYourEmail.autocapitalizationType = UITextAutocapitalizationTypeNone;
    
    
    _emailHeightConstraint.constant = 0;
    
    if (makeFastOrder) {
        _validationArray = @[_tfSending, _tfReceiverName, _tfReceiverPhone, _tfYourName, _tfYourSurName, _tfYourEmail, _tfCellNumber];
    } else {
        _validationArray = @[_tfSending, _tfReceiverName, _tfReceiverPhone];
    }
    
    //Calender View
    calendarHomeView = [[AppDelegate sharedAppDelegate].appController.mainStoryBoard instantiateViewControllerWithIdentifier:@"CalendarHomeViewController"];
    calendarHomeView.calendarDelegate = self;
    
    [_btnByReceiver setImage:[UIImage imageNamed:@"CheckUnsel"] forState:UIControlStateNormal];
    [_btnByReceiver setImage:[UIImage imageNamed:@"CheckSel"] forState:UIControlStateSelected];
    [_btnByReceiver setImage:[UIImage imageNamed:@"CheckSel"] forState:UIControlStateDisabled];
    
    _btnByReceiver.selected = false;
    
    [self checkActiveButton:@""];
    [self populateDraftData];
    
}

- (void)populateDraftData {
    if (_draftdata != nil) {
        _packId = _draftdata.dataIdentifier;
        _tfSending.text = _draftdata.sendName;
        _tfReceiverName.text = _draftdata.receiverName;
        _tfReceiverPhone.text = _draftdata.receiverPhoneNumber;
        _driverNotesTextView.text = _draftdata.descText;
        [self byReceiverAction:nil];
        NSString *email = [[NSUserDefaults standardUserDefaults] stringForKey:@"LoginEmail"];
        _tfEmail.text = email;
        _tfSending.enabled = false;
        _tfReceiverName.enabled = false;
        _tfReceiverPhone.enabled = false;
        _driverNotesTextView.editable = false;
        _tfEmail.enabled = false;
        _btnByReceiver.enabled = false;
        _btnByReceiver.selected = YES;
        _clickbtnByReceiver.enabled = false;
        if (_draftdata.packFromText.length > 0 && _draftdata.packToText.length > 0) {
            _isToAddress = YES;
            _isFromAddress = YES;
            
            NSDate *takenDate;
            if(_draftdata.takenTime != nil) {
                takenDate = [NSDate dateFromDateString:_draftdata.takenTime];
                _takenDateTimeForWeb = [takenDate getDateTimeInUTC];
                [_btnTakeTime setTitle:_draftdata.takenTime forState:UIControlStateNormal];
            }
            
            NSDate *deliveryDate;
            if(_draftdata.deliveryTime != nil) {
                deliveryDate = [NSDate dateFromDateString:_draftdata.deliveryTime];
                _deliveryDateTimeForWeb = [deliveryDate getDateTimeInUTC];
            }
            
            if(_draftdata.takenAsap != nil) {
                if ([_draftdata.takenAsap isEqualToString:@"1"]) {
                } else {
                }
                _takenAsap = _draftdata.takenAsap;
                _takenType = _draftdata.takenType;
                _isTakenTime = YES;
            }
            
            if(_draftdata.deliveryAsap != nil) {
                if ([_draftdata.deliveryAsap isEqualToString:@"1"]) {
                } else {
                }
                
                _deliveryAsap = _draftdata.deliveryAsap;
                _deliveryType = _draftdata.deliveryType;
                _isDeliveryTime = YES;
            }
            
            if(_draftdata.takenTime != nil && _draftdata.deliveryTime != nil && _draftdata.takenAsap != nil && _draftdata.deliveryAsap != nil) {
                _pack.packPriceWithIns = _draftdata.packPrice;
                _pack.packInsuranceMode = _draftdata.insuranceMode;
                _pack.packPriceWithoutIns = _draftdata.insurancePrice;
                _pack.packInsurancePrice = _draftdata.insurancePrice;
                
                //Will change later
//                _btnPayNow.selected = NO;
//                _btnPayNow.backgroundColor = ORANGE_LINE_COLOR;
                
                if ([_pack.packInsuranceMode isEqual:@2]) {
                    _insuranceView.hidden = NO;
                    _lblItemPrice.text = [NSString stringWithFormat:@"$%@", _item_value];
                    _lblPrice.text = [NSString stringWithFormat:@"$%@", _pack.packPriceWithIns];
                } else {
                    _insuranceView.hidden = YES;
                    _lblPrice.text = [NSString stringWithFormat:@"$%@", _pack.packPriceWithIns];
                }
            }
        }
    } else {
        _pack.packPriceWithIns = [NSString stringWithFormat:@"%.2f", ([_price floatValue] + [_item_value floatValue])];
        _pack.packInsuranceMode = @"2";
        _pack.packPriceWithoutIns = _price;
        _pack.packInsurancePrice = _item_value;
    }
    //    [UIView animateWithDuration:0.3 animations:^{
    //        [self.view layoutIfNeeded];
    //    }];
}

#pragma mark -
#pragma mark - IBActions
- (IBAction)insuranceAction:(UIButton*)sender {
    sender.selected = !sender.selected;
    
    if (sender.selected)
        _lblPrice.text = [NSString stringWithFormat:@"$%@", _pack.packPriceWithIns];
    else
        _lblPrice.text = [NSString stringWithFormat:@"$%@", _pack.packPriceWithoutIns];
}

- (IBAction)byReceiverAction:(id)sender {
    
    _btnByReceiver.selected = !_btnByReceiver.selected;
    
//    _tfEmail = (JVFloatLabeledTextField *)[UtilHelper makeBorderForTextField:_tfEmail color:PACK_RED_STATUS_COLOR width:0];
    _lblPaybyReceiverLine.backgroundColor = VALIDATION_LINE_COLOR;
    
    if (_btnByReceiver.selected) {
        _emailHeightConstraint.constant = 40;
        //        _lblIndicator.hidden = false;
        _tfEmail.enabled = true;
        if (sender != nil){
            [_tfEmail becomeFirstResponder];
            [_btnPayNow setTitle:@"Send to pay" forState:UIControlStateNormal];
            _addPaymentSpacing.constant = -25;
            _btnAddPymentMethod.hidden = YES;
        }
    } else {
        _emailHeightConstraint.constant = 0;
        //        _lblIndicator.hidden = true;
        _tfEmail.enabled = false;
        [_btnPayNow setTitle:@"Pay now" forState:UIControlStateNormal];
        _addPaymentSpacing.constant = 4;
        _btnAddPymentMethod.hidden = NO;
    }
    [self checkActiveButton:@" "];
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (IBAction)takenTimeAction:(id)sender {
    //    [self presentViewController:calendarHomeView animated:YES completion:nil];
    
    //    calendarPopup.calendarPopupDelegate = self;
    
    CGRect viewsize = self.view.frame;
    
    CGRect newFrame = calendarPopup.frame;
    newFrame.origin.x = (viewsize.size.width - calendarPopup.frame.size.width) / 2;
    newFrame.origin.y = (viewsize.size.height - calendarPopup.frame.size.height) / 2;
    
    calendarPopup.frame = newFrame;
    
    
    
    calendarPopup.btnUrgent = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)calendarPopup.btnUrgent withRadius:calendarPopup.btnUrgent.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    calendarPopup.btnMin = (UIButton *)[UtilHelper makeBorderCornerCurved:(UIView *)calendarPopup.btnMin withRadius:4.0 color:[UIColor colorWithRed:255.0f/255.0f green:204.0f/255.0f blue:0.0f/255.0f alpha:1.0f] width:2];
    calendarPopup.btnHour = (UIButton *)[UtilHelper makeBorderCornerCurved:(UIView *)calendarPopup.btnHour withRadius:4.0 color:[UIColor colorWithRed:255.0f/255.0f green:204.0f/255.0f blue:0.0f/255.0f alpha:1.0f] width:2];
    calendarPopup.btnAMPM = (UIButton *)[UtilHelper makeBorderCornerCurved:(UIView *)calendarPopup.btnAMPM withRadius:4.0 color:[UIColor colorWithRed:255.0f/255.0f green:204.0f/255.0f blue:0.0f/255.0f alpha:1.0f] width:2];
    [self.view endEditing:YES];
    
    hideView = [[UIView alloc] initWithFrame:self.view.frame];
    
    [self.view addSubview:hideView];
    
    [self.view addSubview:calendarPopup];
    [self.view layoutIfNeeded];
    
}

- (IBAction)CancelActionPopupCalendar:(UIButton *)sender {
    [calendarPopup removeFromSuperview];
    [hideView removeFromSuperview];
    [self.view layoutIfNeeded];
}
- (IBAction)OkActionPopupCalendar:(UIButton *)sender {
    
    //        [UtilHelper showApplicationAlertWithMessage:@"Invalid Time" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    [calendarPopup removeFromSuperview];
    [hideView removeFromSuperview];
    [self.view layoutIfNeeded];
    if (calendarPopup.btnUrgent.selected){
        //        NSDate *date = [calendarPopup.calendar today];
        //        NSString *selectedDate = [date getMMDDYYYYDate];
        [_btnTakeTime setTitle:@"Urgent" forState:UIControlStateNormal];
    } else {
        NSDate *date = [calendarPopup.calendar selectedDate];
        takendate = [date getYYYYMMDDDate];
        NSString *time = [takentime substringToIndex:[takentime length] - 3];
        NSString *datestr = [NSString stringWithFormat:@"%@ %@", takendate, time];
        if (date != NULL) {
            [_btnTakeTime setTitle:datestr forState:UIControlStateNormal];
        }
    }
}
- (IBAction)UrgentActionPopupCalendar:(UIButton *)sender {
    if (!calendarPopup.btnUrgent.selected) {
        //        NSDate *date = [calendarPopup.calendar today];
        //        takendate = [date getMMDDYYYYDate];
        //        NSDate * now = [NSDate date];
        //        NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
        //        [outputFormatter setDateFormat:@"HH:mm:ss"];
        //        NSString *newtimeString = [outputFormatter stringFromDate:now];
        //        TakenTime = [NSString stringWithFormat:@"%@ %@", takendate, newtimeString];
        NSDate *date = [NSDate date];
        TakenTime = [date getDateTimeInUTC];
        [calendarPopup.calendar deselectDate:calendarPopup.calendar.selectedDate];
        [calendarPopup.btnUrgent setBackgroundColor:[UIColor colorWithRed:12.0/255.0 green:106.0/255.0 blue:207.0/255.0 alpha:1.0]];
        [_btnTakeTime setTitle:@"Urgent" forState:UIControlStateNormal];
        _isTimeSelected = YES;
        [self checkActiveButton:@" "];
    } else {
        
        [calendarPopup.btnUrgent setBackgroundColor:[UIColor colorWithRed:249.0/255.0 green:80.0/255.0 blue:80.0/255.0 alpha:1.0]];
        [_btnTakeTime setTitle:@"Select the time" forState:UIControlStateNormal];
        _isTimeSelected = NO;
        [self checkActiveButton:@" "];
    }
    calendarPopup.btnUrgent.selected = !calendarPopup.btnUrgent.selected;
    [calendarPopup removeFromSuperview];
    [hideView removeFromSuperview];
    [self.view layoutIfNeeded];
}
- (IBAction)ClickHourPopupCalendar:(UIButton *)sender {
    NSString *str = sender.titleLabel.text;
    int hour = str.intValue;
    if (hour == 11) {
        NSString *setstr = @"0";
        [sender setTitle:setstr forState:UIControlStateNormal];
        [sender setSelected:false];
        [self.view layoutIfNeeded];
    } else {
        hour = hour + 1;
        NSString *setstr = [NSString stringWithFormat:@"%d", hour];
        [sender setTitle:setstr forState:UIControlStateNormal];
        [sender setSelected:false];
        [self.view layoutIfNeeded];
    }
}
- (IBAction)ClickMinPopupCalendar:(UIButton *)sender {
    NSString *str = sender.titleLabel.text;
    int min = str.intValue;
    if (min == 59) {
        NSString *setstr = @"0";
        [sender setTitle:setstr forState:UIControlStateNormal];
        [sender setSelected:false];
        [self.view layoutIfNeeded];
    } else {
        min = min + 1;
        NSString *setstr = [NSString stringWithFormat:@"%d", min];
        [sender setTitle:setstr forState:UIControlStateNormal];
        [sender setSelected:false];
        [self.view layoutIfNeeded];
    }
}
- (IBAction)ClickAMPMPopupCalendar:(UIButton *)sender {
    NSString *str = sender.titleLabel.text;
    if ([str isEqualToString:@"AM"]) {
        [sender setTitle:@"PM" forState:UIControlStateNormal];
        [sender setSelected:false];
        [self.view layoutIfNeeded];
    } else {
        [sender setTitle:@"AM" forState:UIControlStateNormal];
        [sender setSelected:false];
        [self.view layoutIfNeeded];
    }
}

- (IBAction)additionalDetailsAction:(id)sender {
    int direction = _descriptionSpacing.constant == 0 ? 1 : 0;
    if (_descriptionSpacing.constant == 0){
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]) {
            _descriptionSpacing.constant = _driverNotesTextView.frame.size.height;
        } else {
            _descriptionSpacing.constant = _driverNotesTextView.frame.size.height + 90;
        }
        _driverNotesTextView.hidden = false;
        _collectionView.hidden = false;
    }
    else{
        _descriptionSpacing.constant = 0;
        _driverNotesTextView.hidden = true;
        _collectionView.hidden = true;
    }
    
    _driverNotesTextView.editable = direction == 1;
    _downArrow.transform = CGAffineTransformMakeRotation(direction * M_PI);
    
    //    _userInfoSpacing.constant = 100;
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)updateTekenTime:(NSString *)time {
    _isTakenTime = YES;
    //
    //    //UTC dateTime
    //    NSDate *date = _datePicker.date;
    //    _tempTime = [date getTimeForWeb];
    //    NSString *dateString = [NSString stringWithFormat:@"%@ %@", _tempDate, _tempTime];
    //    NSDate *takenDate = [NSDate dateFromDateStringInLocalTimeZone:dateString];
    //    _lblTakenDate.text = [takenDate getTimeDayFromDate];
    //    _lblTakenTime.text = [NSString stringWithFormat:@"%@ %@", time, [date getTimeFromDateWithoutDay]];
    //
    //
    //    _lblTakenTime.textColor = [UIColor blackColor];
    //    _takenTimeTopConstraint.constant = 0;
    //
    //    //Now taken date is converted to UTC
    //    takenDate = [NSDate dateFromDateStringInLocalTimeZone:dateString];
    //
    //    _takenDateTimeForWeb = [takenDate getDateTimeInUTC];
    //
    //    //[_lblTakenDate setTextColor:GRAY_OUT_COLOR];
    //
}

- (IBAction)payNowAction:(id)sender {
    
    NSDate *date = [NSDate date];
    TakenTime = [date getDateTimeInUTC];
    //    if (!_btnPayNow.selected) {
    if (![UtilHelper isNetworkAvailable]) {
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    if ([self validation]) {
        
        [self showProgressHud];
        if (_btnByReceiver.selected){
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]) {
                [self requestToAskFriendToPayInstant];
            }
            else {
                if (_draftdata != nil) {
                    [self requestAskpaypackemailedbyme];
                } else {
                    [self requestAskFriendtoPay];
                }
                
            }
        }
        else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]) {
            [self requestToMakeInstantPayment];
        } else {
            [self requestToPayNow];
        }
    }
    //    }
}

- (BOOL)validation {
    
    BOOL isValidated = YES;
    
    if ([_tfSending.text isEqualToString:EMPTY_STRING]) {
        
//        _tfSending = (JVFloatLabeledTextField *)[UtilHelper makeBorderForTextField:_tfSending color:PACK_RED_STATUS_COLOR width:1];
        _lblSendingLine.backgroundColor = PACK_RED_STATUS_COLOR;
        isValidated = NO;
    }
    
    if ([_tfReceiverName.text isEqualToString:EMPTY_STRING]) {
        
//        _tfReceiverName = (JVFloatLabeledTextField *)[UtilHelper makeBorderForTextField:_tfReceiverName color:PACK_RED_STATUS_COLOR width:1];
        _lblReceiverNameLine.backgroundColor = PACK_RED_STATUS_COLOR;

        isValidated = NO;
    }
    
    if ([_tfReceiverPhone.text isEqualToString:EMPTY_STRING]) {
        
//        _tfReceiverPhone = (JVFloatLabeledTextField *)[UtilHelper makeBorderForTextField:_tfReceiverPhone color:PACK_RED_STATUS_COLOR width:1];
        _lblReceiverPhoneLine.backgroundColor = PACK_RED_STATUS_COLOR;

        isValidated = NO;
    }
    
    Boolean makeFastOrder = [[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"];
    if (makeFastOrder) {
        
        if ([_tfYourName.text isEqualToString:EMPTY_STRING]) {
            
//            _tfYourName = (JVFloatLabeledTextField *)[UtilHelper makeBorderForTextField:_tfYourName color:PACK_RED_STATUS_COLOR width:1];
            _lblYourFirstNameLine.backgroundColor = PACK_RED_STATUS_COLOR;
            isValidated = NO;
        }
        
        if ([_tfYourSurName.text isEqualToString:EMPTY_STRING]) {
            
//            _tfYourSurName = (JVFloatLabeledTextField *)[UtilHelper makeBorderForTextField:_tfYourSurName color:PACK_RED_STATUS_COLOR width:1];
            _lblYourLastNameLine.backgroundColor = PACK_RED_STATUS_COLOR;
            isValidated = NO;
        }
        
        if ([_tfYourEmail.text isEqualToString:EMPTY_STRING]) {
            
//            _tfYourEmail = (JVFloatLabeledTextField *)[UtilHelper makeBorderForTextField:_tfYourEmail color:PACK_RED_STATUS_COLOR width:1];
            _lblYourEmailLine.backgroundColor = PACK_RED_STATUS_COLOR;
            isValidated = NO;
        }
        
        if ([_tfCellNumber.text isEqualToString:EMPTY_STRING]) {
            
//            _tfCellNumber = (JVFloatLabeledTextField *)[UtilHelper makeBorderForTextField:_tfCellNumber color:PACK_RED_STATUS_COLOR width:1];
            _lblYourCellPhoneLine.backgroundColor = PACK_RED_STATUS_COLOR;

            isValidated = NO;
        }
    }
    
    if (_btnByReceiver.selected) {
        
        if ([_tfEmail.text isEqualToString:EMPTY_STRING]) {
            
//            _tfEmail = (JVFloatLabeledTextField *)[UtilHelper makeBorderForTextField:_tfEmail color:PACK_RED_STATUS_COLOR width:1];
            _lblPaybyReceiverLine.backgroundColor = PACK_RED_STATUS_COLOR;
            isValidated = NO;
        }
    }
    
    if (isValidated) {
        
        if (!_isStripeTokenAdded && !_btnByReceiver.selected) {
            
            [UtilHelper showApplicationAlertWithMessage:@"Please add card details." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            isValidated = NO;
        }
    }
    return isValidated;
}

- (IBAction)addPaymentMethodAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showAddPaymentView];
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestToPayNow {
    
    if (!_isStripeTokenAdded) {
        [UtilHelper showApplicationAlertWithMessage:@"Please add card detail" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        [self hideProgressHud];
        return;
    }
    
    NSString *packPrice = _pack.packPriceWithoutIns;
    NSString *isInsurnacePaid = @"0";
    
    if (_btnItemPrice.selected) {
        packPrice = _pack.packPriceWithIns;
        isInsurnacePaid = @"1";
    }
    
    CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
    NSString *last4DigitsOfCard=[cardInfo.card substringFromIndex:MAX((int)[cardInfo.card length]-4, 0)]; //in case string is less than 4 characters long.
    
    [WebServicesClient payForPack:_packId
                        PackPrice:_price
                  IsInsurancePaid:@"1"
                       StripToken:_stripeToken
                     cardLastFour:last4DigitsOfCard
                       Taken_asap:@"1"
                       Taken_Time:TakenTime
                        Send_Name:_tfSending.text
                        Desc_Text:_driverNotesTextView.text
                    Receiver_Name:_tfReceiverName.text
            Receiver_Phone_Number:_tfReceiverPhone.text
                    LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                      LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                     LogUserAgent:[[UIDevice currentDevice] systemVersion]
                    LogDeviceType:LOG_DEVICE_TYPE
                completionHandler:^(NSString *orderId, NSError *error) {
                    
                    [self hideProgressHud];
                    
                    if (!error) {
                        CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
                        if (cardInfo != nil) {
                            [CardInfoDB insertAndupdateCardInfoWithId:cardInfo.cardId
                                                           CardNumber:cardInfo.card
                                                          StripeToken:EMPTY_STRING
                                                             IsUpdate:YES];
                        }
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ORDERPLACED" object:nil];
                        [[AppDelegate sharedAppDelegate].appController showConfirmPaymentView:_packId AskToFriend:NO FastOrder:NO];
                    } else if (error.code == 3)
                        [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                    else
                        [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                    //                    [[AppDelegate sharedAppDelegate].appController showConfirmPaymentView:_packId];
                }];
}

- (void)requestAskpaypackemailedbyme {
    if (!_isStripeTokenAdded) {
        [UtilHelper showApplicationAlertWithMessage:@"Please add card detail" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        [self hideProgressHud];
        return;
    }
    
    NSString *packPrice = _pack.packPriceWithoutIns;
    NSString *isInsurnacePaid = @"0";
    
    if (_btnItemPrice.selected) {
        packPrice = _pack.packPriceWithIns;
        isInsurnacePaid = @"1";
    }
    NSString *email = [[NSUserDefaults standardUserDefaults] stringForKey:@"LoginEmail"];
    
    CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
    NSString *last4DigitsOfCard=[cardInfo.card substringFromIndex:MAX((int)[cardInfo.card length]-4, 0)]; //in case string is less than 4 characters long.
    
    [WebServicesClient PayForAskedPackByEmail:_packId
                                  Payor_Email:email
                                 cardLastFour:last4DigitsOfCard
                                   StripToken:_stripeToken
                                LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                            completionHandler:^(NSString *orderId, NSError *error) {
                                
                                [self hideProgressHud];
                                
                                if (!error) {
                                    CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
                                    if (cardInfo != nil) {
                                        [CardInfoDB insertAndupdateCardInfoWithId:cardInfo.cardId
                                                                       CardNumber:cardInfo.card
                                                                      StripeToken:EMPTY_STRING
                                                                         IsUpdate:YES];
                                    }
                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ORDERPLACED" object:nil];
                                    [[AppDelegate sharedAppDelegate].appController showConfirmPaymentView:_packId AskToFriend:NO FastOrder:NO];
                                } else if (error.code == 3)
                                    [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                else
                                    [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                                //                    [[AppDelegate sharedAppDelegate].appController showConfirmPaymentView:_packId];
                            }];
}

- (void)requestAskFriendtoPay {
    
    [WebServicesClient AskFriendToPay:_packId
                            PackPrice:_price
                      IsInsurancePaid:@"1"
                           Taken_asap:@"1"
                           Taken_Time:TakenTime
                            Send_Name:_tfSending.text
                            Desc_Text:_driverNotesTextView.text
                        Receiver_Name:_tfReceiverName.text
                Receiver_Phone_Number:_tfReceiverPhone.text
                          Payor_Email:_tfEmail.text
                        LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                          LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                         LogUserAgent:[[UIDevice currentDevice] systemVersion]
                        LogDeviceType:LOG_DEVICE_TYPE
                    completionHandler:^(NSString *orderId, NSError *error) {
                        
                        [self hideProgressHud];
                        
                        if (!error) {
                            CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
                            if (cardInfo != nil) {
                                [CardInfoDB insertAndupdateCardInfoWithId:cardInfo.cardId
                                                               CardNumber:cardInfo.card
                                                              StripeToken:EMPTY_STRING
                                                                 IsUpdate:YES];
                            }
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"ORDERPLACED" object:nil];
                            [[AppDelegate sharedAppDelegate].appController showConfirmPaymentView:_packId AskToFriend:YES FastOrder:NO];
                        } else if (error.code == 3)
                            [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                        else
                            [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                        //                    [[AppDelegate sharedAppDelegate].appController showConfirmPaymentView:_packId];
                    }];
}

- (void)requestToMakeInstantPayment{
    
    NSCalendar * cal = [NSCalendar currentCalendar];
    NSDateComponents * comp = [cal components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSInteger day = [comp weekday];
    NSString *daystr = [NSString stringWithFormat:@"%ld", (long)day];
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    
    //    CSUser *from = [_packVC.markerArray objectAtIndex:0];
    //    CSUser *to = [_packVC.markerArray objectAtIndex:1];
    
    if (!_isStripeTokenAdded) {
        [UtilHelper showApplicationAlertWithMessage:@"Please add card detail" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        [self hideProgressHud];
        return;
    }
    
    NSString *packPrice = _pack.packPriceWithoutIns;
    NSString *isInsurnacePaid = @"0";
    
    if (_btnItemPrice.selected) {
        packPrice = _pack.packPriceWithIns;
        isInsurnacePaid = @"1";
    }
    
    CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
    NSString *last4DigitsOfCard=[cardInfo.card substringFromIndex:MAX((int)[cardInfo.card length]-4, 0)]; //in case string is less than 4 characters long.
    [WebServicesClient MakeInstantPayment:_tfYourEmail.text
                             Phone_Number:_tfCellNumber.text
                                Send_Name:_tfSending.text
                                Desc_Text:_driverNotesTextView.text
                            Receiver_Name:_tfReceiverName.text
                    Receiver_Phone_Number:_tfReceiverPhone.text
                                PackPrice:_price
                               StripToken:_stripeToken
                               Item_Value:_item_value
                             Pack_Size_ID:_pack_size_id
                      Default_Tariff_Plan:_default_tariff_plan
                                 Distance:_distance
                            Distance_Time:_distanceTime
                              Send_Cat_ID:_send_cat_id
                           Pack_From_Text:_from_user.address
                            Pack_From_Lng:[[NSNumber numberWithDouble:_from_user.longitude] stringValue]
                            Pack_From_Lat:[[NSNumber numberWithDouble:_from_user.latitude] stringValue]
                             Pack_To_Text:_to_user.address
                              Pack_To_Lng:[[NSNumber numberWithDouble:_to_user.longitude] stringValue]
                              Pack_To_Lat:[[NSNumber numberWithDouble:_to_user.latitude] stringValue]
                                     Name:_tfYourName.text
                                  SurName:_tfYourSurName.text
                             cardLastFour:last4DigitsOfCard
                                      Day:daystr
                                     Time:newDateString
                             Waiting_Mins:_waiting_time
                               Promo_Code:_promo_code
     //                            LogSessionKey:<#(NSString *)#>
                              LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                             LogUserAgent:[[UIDevice currentDevice] systemVersion]
                            LogDeviceType:LOG_DEVICE_TYPE
                        completionHandler:^(NSString *orderId, NSError *error) {
                            
                            [self hideProgressHud];
                            
                            if (!error) {
                                CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
                                if (cardInfo != nil) {
                                    [CardInfoDB insertAndupdateCardInfoWithId:cardInfo.cardId
                                                                   CardNumber:cardInfo.card
                                                                  StripeToken:EMPTY_STRING
                                                                     IsUpdate:YES];
                                }
                                _packId = orderId;
                                //                        [[NSNotificationCenter defaultCenter] postNotificationName:@"ORDERPLACED" object:nil];
                                [[AppDelegate sharedAppDelegate].appController showConfirmPaymentView:orderId AskToFriend:NO FastOrder:YES];
                            } else if (error.code == 3)
                                [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                            else
                                [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                            //                    [[AppDelegate sharedAppDelegate].appController showConfirmPaymentView:_packId];
                        }];
}

- (void)requestToAskFriendToPayInstant{
    
    NSCalendar * cal = [NSCalendar currentCalendar];
    NSDateComponents * comp = [cal components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSInteger day = [comp weekday];
    NSString *daystr = [NSString stringWithFormat:@"%ld", (long)day];
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    
    //    CSUser *from = [_packVC.markerArray objectAtIndex:0];
    //    CSUser *to = [_packVC.markerArray objectAtIndex:1];
    
    
    NSString *packPrice = _pack.packPriceWithoutIns;
    NSString *isInsurnacePaid = @"0";
    
    if (_btnItemPrice.selected) {
        packPrice = _pack.packPriceWithIns;
        isInsurnacePaid = @"1";
    }
    
    CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
    NSString *last4DigitsOfCard=[cardInfo.card substringFromIndex:MAX((int)[cardInfo.card length]-4, 0)]; //in case string is less than 4 characters long.
    
    [WebServicesClient AskFriendToPayInstant:_tfYourEmail.text
                                phone_number:_tfCellNumber.text
                                   Send_Name:_tfSending.text
                                   Desc_Text:_driverNotesTextView.text
                               Receiver_Name:_tfReceiverName.text
                       Receiver_Phone_Number:_tfReceiverPhone.text
                                   PackPrice:_price
                                  Item_Value:_item_value
                                Pack_Size_Id:_pack_size_id
                                    Distance:_distance
                               Distance_Time:_distanceTime
                                 Send_Cat_Id:_send_cat_id
                              Tariff_Plan_Id:_default_tariff_plan
                              Pack_From_Text:_from_user.address
                               Pack_From_Lng:[[NSNumber numberWithDouble:_from_user.longitude] stringValue]
                               Pack_From_Lat:[[NSNumber numberWithDouble:_from_user.latitude] stringValue]
                                Pack_To_Text:_to_user.address
                                 Pack_To_Lng:[[NSNumber numberWithDouble:_to_user.longitude] stringValue]
                                 Pack_To_Lat:[[NSNumber numberWithDouble:_to_user.latitude] stringValue]
                                        Name:_tfYourName.text
                                     SurName:_tfYourSurName.text
                                cardLastFour:last4DigitsOfCard
                                         Day:daystr
                                        Time:newDateString
                                Waiting_Mins:_waiting_time
                                  Promo_Code:_promo_code
                                 Payor_Email:_tfEmail.text
                               LogSessionKey:@""
                                 LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                LogUserAgent:[[UIDevice currentDevice] systemVersion]
                               LogDeviceType:LOG_DEVICE_TYPE
                           completionHandler:^(NSString *orderId, NSError *error) {
                               
                               [self hideProgressHud];
                               
                               if (!error) {
                                   CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
                                   if (cardInfo != nil) {
                                       [CardInfoDB insertAndupdateCardInfoWithId:cardInfo.cardId
                                                                      CardNumber:cardInfo.card
                                                                     StripeToken:EMPTY_STRING
                                                                        IsUpdate:YES];
                                   }
                                   _packId = orderId;
                                   //                                [[NSNotificationCenter defaultCenter] postNotificationName:@"ORDERPLACED" object:nil];
                                   [[AppDelegate sharedAppDelegate].appController showConfirmPaymentView:orderId AskToFriend:YES FastOrder:YES];
                               } else if (error.code == 3)
                                   [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                               else
                                   [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                               //                    [[AppDelegate sharedAppDelegate].appController showConfirmPaymentView:_packId];
                           }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 3005) {
    }
}

#pragma
#pragma  mark action sheet delegate method

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(buttonIndex == 1)
        [self pickMediaFromSource:UIImagePickerControllerSourceTypeCamera];
    else if(buttonIndex == 0)
        [self pickMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
}

#pragma
#pragma  mark camera delegate method

- (void)pickMediaFromSource:(UIImagePickerControllerSourceType)sourceType{
    @autoreleasepool {
        if([UIImagePickerController isSourceTypeAvailable:sourceType]) {
            //            NSArray *mediaTypes=[UIImagePickerController availableMediaTypesForSourceType:sourceType];
            UIImagePickerController *picker=[[UIImagePickerController alloc]init];
            //            picker.mediaTypes = mediaTypes;
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = sourceType;
            [self presentViewController:picker animated:YES completion:NULL];
        } else
            [UtilHelper showApplicationAlertWithMessage:@"Camera is not found" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
    }
}

#pragma mark -
#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)]) {
        @autoreleasepool {
            UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
            
            AddImageObject *addImageObject = [AddImageObject new];
            addImageObject.image = chosenImage;
            addImageObject.isUploaded = NO;
            
            [_selectedMediaArray addObject:addImageObject];
            [_collectionView reloadData];
            [self requestForAddImage:_selectedMediaArray.count-1];
        }
    } else
        [UtilHelper showApplicationAlertWithMessage:@"Video does not support." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *textFullyEntered = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self checkActiveButton:textFullyEntered];
    [self changeValidationLinesColor];
    
    return YES;
}

- (void)changeValidationLinesColor {
    _lblSendingLine.backgroundColor = VALIDATION_LINE_COLOR;
    _lblReceiverNameLine.backgroundColor = VALIDATION_LINE_COLOR;
    _lblReceiverPhoneLine.backgroundColor = VALIDATION_LINE_COLOR;
    _lblYourFirstNameLine.backgroundColor = VALIDATION_LINE_COLOR;
    _lblYourLastNameLine.backgroundColor = VALIDATION_LINE_COLOR;
    _lblYourEmailLine.backgroundColor = VALIDATION_LINE_COLOR;
    _lblYourCellPhoneLine.backgroundColor = VALIDATION_LINE_COLOR;
    _lblPaybyReceiverLine.backgroundColor = VALIDATION_LINE_COLOR;
}

- (void)checkActiveButton:(NSString *)numberOFCharacter {
    
    BOOL isActive = NO;
    for (JVFloatLabeledTextField *tfText in _validationArray) {
        if (tfText.text.length > 0) {
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
        } else {
            isActive = NO;
            break;
        }
    }
    
    if (_btnByReceiver.selected && _tfEmail.text.length == 0)
        isActive = NO;
    if (!_isStripeTokenAdded && !_btnByReceiver.selected)
        isActive = NO;
    
    if (isActive) {
//        _btnPayNow.backgroundColor = OFFLINE_BUTTON_SELECTION;
//        _btnPayNow.selected = NO;
    } else {
//        _btnPayNow.backgroundColor = LOGIN_DISABLE_COLOR;
//        _btnPayNow.selected = YES;
    }
}


#pragma mark UICollectionView Methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSInteger count = _selectedMediaArray.count;
    return count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MediaUploadingViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MediaUploadingViewCell" forIndexPath:indexPath];
    
    bool first = indexPath.row == 0;
    cell.containerView = [UtilHelper makeCornerCurved:cell.containerView withRadius:10 color:first ? [UIColor lightGrayColor] : [UIColor clearColor] width:first ? 1 : 0];
    
    cell.mediaCellDelegate = self;
    
    if (first) {
        cell.imageView.hidden = true;
        cell.btnCancel.hidden = true;
        return cell;
    }
    
    AddImageObject *addImageObject = _selectedMediaArray[indexPath.row-1];
    [cell prepareUIForRowWithImage:addImageObject index:indexPath.row-1];
    
    cell.imageView.hidden = false;
    
    if (addImageObject.image != nil) {
        [cell.activityIndicatorView stopAnimating];
        [cell.activityIndicatorView startAnimating];
    } else {
        //Shows indicator on imageView
        [cell.imageView setShowActivityIndicatorView:YES];
        [cell.imageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, addImageObject.imageName];
        
        //Loads image
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
    }
    
    if (addImageObject.isUploaded == NO) {
        [cell.activityIndicatorView stopAnimating];
        [cell.activityIndicatorView startAnimating];
        cell.btnCancel.hidden = YES;
    } else {
        [cell.activityIndicatorView startAnimating];
        [cell.activityIndicatorView stopAnimating];
        cell.btnCancel.hidden = NO;
    }
    
    return cell;
}

- (void)cancelForCellRow:(NSInteger)row {
    [self requestForDeleteImage:row];
    
}

- (void)actionForCellRow:(NSInteger)row {
    [self.view endEditing:YES];
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]) {
        if (_selectedMediaArray.count < 4) {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Media" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library", @"Take Photo",nil];
            actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
            [actionSheet showInView:self.view];
        } else {
            [UtilHelper showApplicationAlertWithMessage:@"Sorry! You can not upload more than 4 images" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
        }
    } else {
        [UtilHelper showApplicationAlertWithMessage:@"Sorry, complete the order without adding picture." withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
    }
    
}

#pragma mark
#pragma mark - Text View Delegate Methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    UIToolbar *ViewForDoneButtonOnKeyboard = [[UIToolbar alloc] init];
    [ViewForDoneButtonOnKeyboard sizeToFit];
    UIBarButtonItem *btnDoneOnKeyboard = [[UIBarButtonItem alloc] initWithTitle:@"DONE" style:UIBarButtonItemStylePlain target:self action:@selector(doneBtnFromKeyboardClicked:)];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [ViewForDoneButtonOnKeyboard setItems:[NSArray arrayWithObjects:flex, btnDoneOnKeyboard, nil]];
    textView.inputAccessoryView = ViewForDoneButtonOnKeyboard;
    return YES;
}

- (void)doneBtnFromKeyboardClicked:(id)sender {
    [self.view endEditing:YES];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text isEqualToString:@"\n"]) {
    }
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]) {
        _descriptionSpacing.constant = _driverNotesTextView.frame.size.height;
    } else {
        _descriptionSpacing.constant = _driverNotesTextView.frame.size.height + 90;
    }

    NSString *description = [textView.text stringByReplacingCharactersInRange:range withString:text];

    if (description.length < 1) {
        CGRect frame = _driverNotesTextView.placeHolderLabel.frame;
        frame.origin.y = -10;
        _driverNotesTextView.placeHolderLabel.frame = frame;
    }
    
    [self.view layoutIfNeeded];

    //Store description text below
    [self checkActiveButton:description];
    return YES;
}

- (void)requestForAddImage:(NSInteger)index {
    AddImageObject *addImageObject = _selectedMediaArray[index];
    
    NSData* imageData = UIImageJPEGRepresentation(addImageObject.image, 0.5);
    NSString *strBase64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString *stringBase = @"data:image/png;base64,";
    NSString *finalBase64 = [stringBase stringByAppendingString:strBase64];
    
    [WebServicesClient AddPackImage:_packId
                    PackImageBase64:finalBase64
                      LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey//@"5BD81926-3EBD-4181-BE1A-ED293C10E6BA2017060307400"
                        LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                       LogUserAgent:[[UIDevice currentDevice] systemVersion]
                      LogDeviceType:LOG_DEVICE_TYPE
                  completionHandler:^(AddImageObject *addImage, NSError *error) {
                      
                      if (!error) {
                          addImageObject.isUploaded = YES;
                          addImageObject.imageName = addImage.imageName;
                          addImageObject.insertedId = addImage.insertedId;
                          [self->_collectionView reloadData];
                      } else if (error.code == 3) {
                          [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                      } else {
                      }
                  }];
}

- (void)requestForDeleteImage:(NSInteger)index {
    
    AddImageObject *addImageObject = _selectedMediaArray[index];
    [WebServicesClient DeletePackImage:addImageObject.insertedId
                         PackImageName:addImageObject.imageName
                         LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey//@"5BD81926-3EBD-4181-BE1A-ED293C10E6BA2017060307400"
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                     completionHandler:^(BOOL isDeleted, NSError *error) {
                         
                         if (!error) {
                             if (self->_selectedMediaArray.count > 0) {
                                 [self->_selectedMediaArray removeObjectAtIndex:index];
                                 [self->_collectionView reloadData];
                                 
                                 //                                 if (_selectedMediaArray.count == 0)
                                 //                                     _collectionView.hidden = YES;
                             }
                         } else if (error.code == 3) {
                             [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                         } else {
                         }
                     }];
}

- (void)requestForAllImages {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [WebServicesClient GetAllPackImages:self->_draftdata.dataIdentifier
                                LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                               LogUserAgent:[[UIDevice currentDevice] systemVersion]
                              LogDeviceType:LOG_DEVICE_TYPE
                                 SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                          completionHandler:^(NSMutableArray *deliveryImages, NSError *error) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  if (!error) {
                                      if (deliveryImages.count > 0) {
                                          [_selectedMediaArray addObjectsFromArray:deliveryImages];
                                          [self->_collectionView reloadData];
                                      } else {}
                                  } else {}
                              });
                          }];
    });
}

-(void)okAction{
    [calendarPopup removeFromSuperview];
    
}

-(void)cancelAction{
    
}

- (NSString *)getshowString:(NSString *)str {
    
    int i = 0;
    for (i; i<str.length; i ++) {
        if([str characterAtIndex:i] == '.')
            break;
    }
    if (i < (str.length - 2)){
        NSString *newstr = [str substringToIndex:i+3];
        return newstr;
    }
    return str;
}

#pragma mark - FSCalendar Delegate

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition {
    
    [calendarPopup addSubview:datepicker];
    [calendarPopup addSubview:doneBtn];
    calendarPopup.btnUrgent.selected = false;
    if (calendarPopup.btnUrgent.selected) {
        [calendarPopup.btnUrgent setBackgroundColor:[UIColor colorWithRed:12.0/255.0 green:106.0/255.0 blue:207.0/255.0 alpha:1.0]];
    } else {
        
        [calendarPopup.btnUrgent setBackgroundColor:[UIColor colorWithRed:249.0/255.0 green:80.0/255.0 blue:80.0/255.0 alpha:1.0]];
    }
    
    // calendar.today border
    [calendarPopup layoutIfNeeded];
    _isTimeSelected = YES;
    [self checkActiveButton:@" "];
}

- (UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance borderDefaultColorForDate:(NSDate *)date{
    return date == calendar.today? UIColor.redColor : nil;
}

- (void)doneAction:(id)sender
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    takentime = [dateFormatter stringFromDate:datepicker.date];
    NSDate *todaydate = [NSDate date];
    NSString *todayStr = [todaydate getDateTimeInLocalTimeZone];
    NSDate *today = [NSDate dateFromDateString:todayStr];
    NSDate *date = [calendarPopup.calendar selectedDate];
    takendate = [date getYYYYMMDDDate];
    NSString *selectedString = [NSString stringWithFormat:@"%@ %@", takendate, takentime];
    NSDate *selectdate = [NSDate dateFromDateString:selectedString];
    if ([today compare:selectdate] == NSOrderedAscending) {
        NSDate *selectedUTCDate = [NSDate dateFromDateStringInLocalTimeZone:selectedString];
        TakenTime = [selectedUTCDate getDateTimeInUTC];
    } else {
        [UtilHelper showApplicationAlertWithMessage:@"Taken time cannot be less than current date if \"Urgent\" selected" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
    }
    NSDate *selectedUTCDate = [NSDate dateFromDateStringInLocalTimeZone:selectedString];
    TakenTime = [selectedUTCDate getDateTimeInUTC];
    [datepicker removeFromSuperview];
    [doneBtn removeFromSuperview];
}

@end
