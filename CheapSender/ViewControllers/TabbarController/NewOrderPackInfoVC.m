//
//  NewOrderPackInfoVC.m
//  CheapSender
//
//  Created by admin on 4/22/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "NewOrderPackInfoVC.h"
#import "PlacesViewController.h"
#import "AddressViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface NewOrderPackInfoVC () <PlacesViewControllerDelegate> {
    
    bool isSizeSelected;
    bool fromScedual;
}

@property (strong, nonatomic) NSString *distance;
@property (strong, nonatomic) NSString *distanceTime;
@property (strong, nonatomic) NSString *takenType;
@property (strong, nonatomic) NSString *deliveryType;
@property (strong, nonatomic) NSString *takenAsap;
@property (strong, nonatomic) NSString *deliveryAsap;

@property (strong, nonatomic) NSString *tempDate;
@property (strong, nonatomic) NSString *tempTime;
@property (strong, nonatomic) NSString *tempTillOn;

@property (strong, nonatomic) NSString *takenDateTimeForWeb;
@property (strong, nonatomic) NSString *deliveryDateTimeForWeb;
@property (strong, nonatomic) NSString *stripeToken;

@property (assign, nonatomic) BOOL isFromAddress;
@property (assign, nonatomic) BOOL isToAddress;
@property (assign, nonatomic) BOOL isTakenTime;
@property (assign, nonatomic) BOOL isDeliveryTime;
@property (assign, nonatomic) BOOL isStripeTokenAdded;

@property (assign, nonatomic) BOOL isMainTapTakenTime;
@property (strong, nonatomic) SPPack *pack;

@end

@implementation NewOrderPackInfoVC

NSString *price_urgent;
NSString *price_within12h;
NSString *description_urgent;
NSString *description_within12h;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _packId = EMPTY_STRING;
    
    fromScedual = false;
    isSizeSelected = false;
    [self initializeLocationManagerService];
    
    //    [UIView setAnimationsEnabled:YES];
    
    [_tableView registerNib:[UINib nibWithNibName:@"AddressViewCell" bundle:nil] forCellReuseIdentifier:@"addressCell"];
    [self prepareUI];
    [self placeTextFieldOptionals];
    
    _imgBackWhiteIcon.hidden = YES;
    _backButton.hidden = YES;
    //Version1.7
    if (_isBackHidden)
    {
        _imgBackWhiteIcon.hidden = NO;
        _backButton.hidden = NO;
    }
}

- (void)createPackRequest {
    [self showProgressHud];
    [WebServicesClient CreatePack:_send_Cat_Id
                      LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                     LogUserAgent:[[UIDevice currentDevice] systemVersion]
                    LogDeviceType:LOG_DEVICE_TYPE
                       SessionKey:[AppDelegate sharedAppDelegate].sessionKey//@"5BD81926-3EBD-4181-BE1A-ED293C10E6BA2017060307400"
                completionHandler:^(NSString *packId, NSError *error) {
                    [self hideProgressHud];
                    
                    if (!error)
                        self.packId = packId;
                    else {
                        if (error.code == 3)
                            [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                        else
                            [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                    }
                }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    //    if([_packId isEqual:EMPTY_STRING ] || _packId == nil)
    //        [self createPackRequest];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark -
#pragma mark - Helpers

- (void)prepareUI {
    
    _lblUrgentPrice.text = EMPTY_STRING;
    _lbl12Price.text = EMPTY_STRING;
    
    [_googleMapView clear];
    //    [self placeTextFieldOptionals];
    _mapViewHeight.constant = (self.view.frame.size.height - 20 - 120);
    _scrollView.scrollEnabled = false;
    if ([_send_Cat_Id isEqualToString:@"1"]) {
        [_iconView setImage:[UIImage imageNamed:@"ups-status"]];
        //        _sizeviewSpace.constant = 50;
        _promocodeHeight.constant += 50;
        _tfWaitingtime.hidden = false;
    } else if ([_send_Cat_Id isEqualToString:@"3"]) {
        [_iconView setImage:[UIImage imageNamed:@"deliverIcon"]];
        _tfItemValue.placeholder = @"Item to be purchased";
    } else if ([_send_Cat_Id isEqualToString:@"4"]) {
        [_iconView setImage:[UIImage imageNamed:@"returnIcon"]];
        //        _sizeviewSpace.constant = 50;
    }
    
    _send_Cat_Id = @"2";
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]){
        _promocodeHeight.constant -= 50;
        _promoLabelHeight.constant = 0;
        _promoImage.hidden = true;
        _tfPromoCode.hidden = true;
    }
    
    self.selectedGroupIndex = 0;
    _btnNext = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnNext withRadius:_btnNext.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    _locationView = (UIButton *)[UtilHelper makeCornerCurved:_locationView withRadius:15 color:COMMON_ICONS_COLOR width:0];
    [_btnUrgent setImage:[UIImage imageNamed:@"RadioUnsel"] forState:UIControlStateNormal];
    [_btn12 setImage:[UIImage imageNamed:@"RadioUnsel"] forState:UIControlStateNormal];
    [_btnUrgent setImage:[UIImage imageNamed:@"RadioSel"] forState:UIControlStateSelected];
    [_btn12 setImage:[UIImage imageNamed:@"RadioSel"] forState:UIControlStateSelected];
    
    [_timeView setHidden:true];
    _buttonViewSpacing.constant = 0;
    
    //Initialize the mapview
    [self initializeMapView];
    
    //    [_tableView reloadData];
    
    //Initialize the markers array
    _markerArray = [NSMutableArray new];
    
    if(_packSizesList == nil)
        _packSizesList = [NSArray new];
    
    _validationArray = @[];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(orderPlacedSuccessfully:)
                                                 name:@"ORDERPLACED"
                                               object:nil];
    
    if ([AppDelegate sharedAppDelegate].allDictData.count > 0) {
        NSPredicate *packSizesPred = [NSPredicate predicateWithFormat:@"dicShort contains[c] %@", @"pack_sizes"];
        NSMutableArray *packSizes = [[[AppDelegate sharedAppDelegate].allDictData filteredArrayUsingPredicate:packSizesPred] mutableCopy];
        
        _packSizesList = packSizes;
        
        if (_packSizesList.count > 0 )
            [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSKeyedArchiver archivedDataWithRootObject:_packSizesList] forKey:@"PackList"];
    }
    if(_packSizesList.count == 0) {
        //        NSArray *cacheArry = [NSKeyedUnarchiver unarchiveObjectWithData:[UtilHelper getValueFromNSUserDefaultForKey:@"PackList"]];
        NSString *cache = [UtilHelper getValueFromNSUserDefaultForKey:@"PackList"];
        id result = [UtilHelper getValueFromNSUserDefaultForKey:@"PackList"];
        if ([result isKindOfClass:[NSMutableData class]])
            _packSizesList =  [NSKeyedUnarchiver unarchiveObjectWithData:[UtilHelper getValueFromNSUserDefaultForKey:@"PackList"]];
        else if (![cache isEqualToString:@"-1"])
            _packSizesList =  [NSKeyedUnarchiver unarchiveObjectWithData:[UtilHelper getValueFromNSUserDefaultForKey:@"PackList"]];
    }
    
#pragma mark -- Web related
    _packSizeId = 0;
    
    if (_draftdata != nil) {
        _packId = _draftdata.dataIdentifier;
        _tfPromoCode.text = [_draftdata.packWeight isEqualToString:@"0"]? @"" : _draftdata.packWeight;
        _tfItemValue.text = _draftdata.itemValue;
        _packSizeId = _draftdata.packSizeId;
        //        _packSizeId = @"0";
        _selectedOption = [_draftdata.packOptionsId intValue];
        
        
        if([_draftdata.packWeight integerValue] <= 25 && [_draftdata.packWeight integerValue] != 0)
            [self sizeUISetup:1];
        else if ([_draftdata.packWeight integerValue] <= 50 && [_draftdata.packWeight integerValue] != 0)
            [self sizeUISetup:2];
        else if ([_draftdata.packWeight integerValue] <= 70 && [_draftdata.packWeight integerValue] != 0)
            [self sizeUISetup:3];
        
        for (GetDictByName *dictByName in _packSizesList) {
            if ([dictByName.listId isEqualToString:_draftdata.packSizeId]) {
                _packSizeId = _draftdata.packSizeId;
                if ([dictByName.listName isEqualToString:@"Small"])
                    [self sizeUISetup:1];
                else if ([dictByName.listName isEqualToString:@"Medium"])
                    [self sizeUISetup:2];
                else if ([dictByName.listName isEqualToString:@"Big"])
                    [self sizeUISetup:3];
                break;
            }
        }
        if (_packSizesList.count == 0) {
            
        }
        _send_Cat_Id = _draftdata.sendCatId;
        _distance = _draftdata.distance;
        _distanceTime = _draftdata.distanceTime;
        if(_distance != nil){
            _scrollView.scrollEnabled = true;
            _mapViewHeight.constant = 275;
            UIButton * tempBtn = [[UIButton alloc] init];
            [tempBtn setTag:[_draftdata.packSizeId intValue]];
            [self sizeSelectionAction:tempBtn];
        }
    }
    
    CSUser *userPack, *userDeli;
    
    userPack = [CSUser new];
    userDeli = [CSUser new];
    
    userPack.userId = 0;
    userPack.imageName = @"pick-location-marker";
    userPack.ovalImage = @"OvalPick";
    userPack.latitude = _draftdata != nil ? [_draftdata.packFromLat doubleValue] : 0.0;
    userPack.longitude = _draftdata != nil ? [_draftdata.packFromLng doubleValue] : 0.0;
    userPack.address = _draftdata != nil ? _draftdata.packFromText : @"Select Pick-up Address";
    
    userDeli.userId = 1;
    userDeli.imageName = @"delivered-location-marker";
    userDeli.ovalImage = @"OvalDest";
    userDeli.latitude = _draftdata != nil ? [_draftdata.packToLat doubleValue] : 0.0;
    userDeli.longitude = _draftdata != nil ? [_draftdata.packToLng doubleValue] : 0.0;
    userDeli.address = _draftdata != nil ? _draftdata.packToText : @"Select Delivery Address";
    
    userPack.marker = [_googleMapView addMapPinForUser:userPack];
    userDeli.marker = [_googleMapView addMapPinForUser:userDeli];
    
    _distance = _draftdata.distance;
    _distanceTime = _draftdata.distanceTime;
    
    //Removing and adding new users in userInfoArray
    [_markerArray addObject:userPack];
    [_markerArray addObject:userDeli];
    
    if (userDeli.marker != nil && userPack.marker != nil) {
        [self drawRoute];
        [self calculateDistanceAndTimeBetweenLocations];
    }
    NSString *addrPack = ((CSUser*)[_markerArray objectAtIndex:0]).address;
    NSString *addrDeli = ((CSUser*)[_markerArray objectAtIndex:1]).address;
    
    if(_draftdata != nil)
    {
        _addressTextField1.text = addrPack;
        _addressTextField2.text = addrDeli;
    }
    
    //    [UIView animateWithDuration:0.3 animations:^{
    //        [self.view layoutIfNeeded];
    //    }];
    
    //GMSCoordinateBounds represents a rectangular bounding box on the Earth's surface.
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    for (CSUser *user in _markerArray) {
        //Returns a GMSCoordinateBounds representing the current bounds extended to include the passed-in coordinate
        if((user.latitude != 0 && user.longitude != 0))
            bounds = [bounds includingCoordinate:CLLocationCoordinate2DMake(user.marker.position.latitude, user.marker.position.longitude)];
    }
    
    _googleMapView.padding = UIEdgeInsetsMake(20, 0, 0, 0);
    [_googleMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    //    [_googleMapView showAllMarkersInMapViewWithBound:bounds];
    [self drawRoute];
    
    [self checkActiveButton:@""];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:36.778259 longitude:-119.417931 zoom: 6];
    [_googleMapView animateToCameraPosition:camera];
}

//Initializing and allocating periodicWebCallManager class
- (void)initializeMapView
{
    //Assigning delegates
    self.googleMapView.delegate = _googleMapView;
    [self.googleMapView initWithCustomStyle];
}

//Initializing and allocating current location manager class
- (void)initializeLocationManagerService
{
    //Allocating and Initializing LocationManagerService class
    _locationManagerService = [LocationManagerService new];
    
    //Assigning delegates of LocationManagerService
    _locationManagerService.locationManagerDelagete = self;
    
    //Initialize setup method for CLLocation
    [_locationManagerService initialize];
}

#pragma mark
#pragma mark -- LocationManagerService Delegates

- (void)notifyToMapViewWithLocation:(CLLocation *)currentLocation {
    if (!_isMapLoaded) {
        [_googleMapView showUserLocation:currentLocation];
        [_locationManagerService stopUpdatingLocation];
    }
}

- (void)notifyOnGPSDisable {
    
}

- (void)showPlacesView {
    //    PlacesViewController *placesView = [[AppDelegate sharedAppDelegate].appController.mainStoryBoard instantiateViewControllerWithIdentifier:@"PlacesViewController"];
    //    placesView.placesDelegate = self;
    //    placesView.headingName = [[NSString alloc]initWithFormat:@"Select %@ Address", _addressIndex == 0 ? @"Pick-up" : @"Delivery"];
    //    placesView.lblHeading.text = [[NSString alloc]initWithFormat:@"Select %@ Address", _addressIndex == 0 ? @"Pick-up" : @"Delivery"];
    //
    //    placesView.view.backgroundColor = [UIColor clearColor];
    //    placesView.modalPresentationStyle = UIModalPresentationCurrentContext;
    //    placesView.modalPresentationStyle = UIModalPresentationFormSheet;
    ////    self.navigationController.modalPresentationStyle = UIModalPresentationCurrentContext;
    ////    [AppDelegate sharedAppDelegate].appController.navController.modalPresentationStyle = UIModalPresentationCurrentContext;
    ////    [AppDelegate sharedAppDelegate].appController.navController.modalPresentationStyle = UIModalPresentationFormSheet;
    ////    [AppDelegate sharedAppDelegate].appController.navController.view.backgroundColor = [UIColor clearColor];
    ////    [[AppDelegate sharedAppDelegate].appController.navController pushViewController:placesView animated:NO];
    //
    //    [self.view addSubview:placesView.view];
    //    [self addChildViewController:placesView];
    //
    ////    [self presentViewController:placesView animated:NO completion:nil];
    
    //    CGRect tframe = _tableView.frame;
    //    CGRect lframe = _locationView.frame;
    //    CGRect cframe = [_tableView rectForRowAtIndexPath:0];
    //    CGRect csframe = [_tableView convertRect:cframe toView:_tableView.superview];
    ////    _addressTextField.hidden = true;
    //    [_addressTextField setText:@""];
    //    _addressTextField.backgroundColor = [UIColor whiteColor];
    //
    //    _addressTextField.frame = CGRectMake(45, lframe.origin.y + _addressIndex*44, 240, 43);
    //    _addressTextField.hidden = false;
    
    //    [self.view layoutIfNeeded];
    //    [UIView animateWithDuration:0.5 animations:^{
    //        [self.view layoutIfNeeded];
    //    }];
    //
    //    [self placeTextFieldOptionals];
    
}

- (void)placeTextFieldOptionals
{
    //Optional Properties of MVPlaceSearchTextField
    _addressTextField1.autoCompleteRegularFontName = @"SFUIText-Bold";
    _addressTextField1.autoCompleteBoldFontName = @"SFUIText-Regular";
    _addressTextField1.autoCompleteTableCornerRadius=0.0;
    _addressTextField1.autoCompleteRowHeight=35;
    _addressTextField1.autoCompleteTableCellTextColor=[UIColor colorWithWhite:0.131 alpha:1.000];
    _addressTextField1.autoCompleteFontSize=12;
    _addressTextField1.autoCompleteTableBorderWidth=1.0;
    _addressTextField1.showTextFieldDropShadowWhenAutoCompleteTableIsOpen=NO;
    _addressTextField1.autoCompleteShouldHideOnSelection=YES;
    _addressTextField1.autoCompleteShouldHideClosingKeyboard=YES;
    _addressTextField1.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
    _addressTextField1.clearButtonMode = UITextFieldViewModeNever;
    //    self.placeSearchTextField = (MVPlaceSearchTextField *)[UtilHelper shadowView:(UIView *)self.placeSearchTextField];
    
    //Placeholder Pedding in MVPlaceSearchTextField
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    _addressTextField1.leftView = paddingView;
    _addressTextField1.leftViewMode = UITextFieldViewModeAlways;
    
    //    [_addressTextField1 becomeFirstResponder];
    
    _addressTextField2.autoCompleteRegularFontName = @"SFUIText-Bold";
    _addressTextField2.autoCompleteBoldFontName = @"SFUIText-Regular";
    _addressTextField2.autoCompleteTableCornerRadius=0.0;
    _addressTextField2.autoCompleteRowHeight=35;
    _addressTextField2.autoCompleteTableCellTextColor=[UIColor colorWithWhite:0.131 alpha:1.000];
    _addressTextField2.autoCompleteFontSize=12;
    _addressTextField2.autoCompleteTableBorderWidth=1.0;
    _addressTextField2.showTextFieldDropShadowWhenAutoCompleteTableIsOpen=NO;
    _addressTextField2.autoCompleteShouldHideOnSelection=YES;
    _addressTextField2.autoCompleteShouldHideClosingKeyboard=YES;
    _addressTextField2.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
    _addressTextField2.clearButtonMode = UITextFieldViewModeNever;
    //    self.placeSearchTextField = (MVPlaceSearchTextField *)[UtilHelper shadowView:(UIView *)self.placeSearchTextField];
    
    //Placeholder Pedding in MVPlaceSearchTextField
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    _addressTextField2.leftView = paddingView2;
    _addressTextField2.leftViewMode = UITextFieldViewModeAlways;
    
    //    [_addressTextField2 becomeFirstResponder];
}

#pragma mark -
#pragma mark - PlacesViewControllerDelegate

- (void)reloadMarkerOfMap {
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    for (CSUser *user in _markerArray) {
        
        //Returns a GMSCoordinateBounds representing the current bounds extended to include the passed-in coordinate
        if((user.latitude != 0 && user.longitude != 0)) {
            bounds = [bounds includingCoordinate:user.marker.position];
        }
    }
    _googleMapView.padding = UIEdgeInsetsMake(20, 0, 0, 0);
    [_googleMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
}

- (void)placeMarker:(GMSPlace *)responseDict {
    [self sizeUISetup:0];
    [_googleMapView clear];
    
    if (_addressIndex == _markerArray.count)
        [_markerArray addObject:[CSUser new]];
    
    CSUser* user = [_markerArray objectAtIndex:_addressIndex];
    user.userId = _addressIndex;
    user.imageName = _addressIndex == 0 ? @"pick-location-marker" : @"delivered-location-marker";
    user.ovalImage = _addressIndex == 0 ? @"OvalPick" : @"OvalDest";
    user.latitude = responseDict.coordinate.latitude;
    user.longitude = responseDict.coordinate.longitude;
    user.address = responseDict.formattedAddress;
    
    if (user.marker != nil)
        user.marker.map = nil;
    user.marker = [_googleMapView addMapPinForUser:user];
    
    NSArray *tmpArray = [[NSArray alloc]initWithArray:_markerArray];
    [_markerArray removeAllObjects];
    for (int i = 0; i < tmpArray.count; i ++) {
        if (i == _addressIndex)
            [_markerArray addObject:user];
        else {
            CSUser *tmpUser = [tmpArray objectAtIndex:i];
            tmpUser.marker = [_googleMapView addMapPinForUser:tmpUser];
            [_markerArray addObject:tmpUser];
        }
    }
    
    NSString *addrPack = ((CSUser*)[_markerArray objectAtIndex:0]).address;
    NSString *addrDeli = ((CSUser*)[_markerArray objectAtIndex:1]).address;
    _pick_upAddressLabel.text = addrPack;
    _deliveryAddresslabel.text = addrDeli;
    if (![addrPack containsString:@"Select"] && ![addrDeli containsString:@"Select"]) {
        _mapViewHeight.constant = 275;
        
        _scrollView.scrollEnabled = true;
        [_scrollView scrollRectToVisible:CGRectMake(0, 0, 10, 10) animated:false];
        [_scrollView layoutIfNeeded];
        
        [self drawRoute];
        [self calculateDistanceAndTimeBetweenLocations];
    }
    
    //GMSCoordinateBounds represents a rectangular bounding box on the Earth's surface.
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    for (CSUser *user in self.markerArray) {
        //Returns a GMSCoordinateBounds representing the current bounds extended to include the passed-in coordinate
        if((user.latitude != 0 && user.longitude != 0))
            bounds = [bounds includingCoordinate:CLLocationCoordinate2DMake(user.marker.position.latitude, user.marker.position.longitude)];
    }
    
    _googleMapView.padding = UIEdgeInsetsMake(20, 0, 0, 0);
    [_googleMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
    //    [self.googleMapView showAllMarkersInMapViewWithBound:bounds];
    [self drawRoute];
    
    
    if (_addressIndex == 0) {
        //        NSIndexPath *index = [NSIndexPath indexPathForRow:1 inSection:0];
        //        [_tableView selectRowAtIndexPath:index animated:YES scrollPosition:UITableViewScrollPositionNone];
        //        [self tableView:_tableView didSelectRowAtIndexPath:index];
    }
    [_addressTextField resignFirstResponder];
    //    [_tableView reloadData];
}

- (void)calculateDistanceAndTimeBetweenLocations {
    
    CSUser* userPack = [_markerArray objectAtIndex:0];
    for (int i = 1; i < _markerArray.count; i ++) {
        CSUser *userDeli = [_markerArray objectAtIndex:i];
        [_googleMapView getDistanceAndTimeTaken:userPack.latitude
                              UserFromLongitude:userPack.longitude
                                 UserToLatitude:userDeli.latitude
                                UserToLongitude:userDeli.longitude
                                 withCompletion:^(NSDictionary *lastDict) {
                                     if (lastDict != nil) {
                                         SPDistanceTime *spDistanceTime = [self.googleMapView parseDistaneTimeFromGoogleAPIResponse:lastDict];
                                         
                                         if (spDistanceTime != nil) {
                                             
                                             self.distance = spDistanceTime.distance;
                                             self.distanceTime = spDistanceTime.duration;
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 if(self.distance == nil || _distanceTime == nil || [_distance intValue] > 300 || [_distance floatValue] == 0) {
                                                     NSString *alertMessage = @"The package can't be delivered to the selected destination. Please try with a different destination.";
                                                     
                                                     if([self.distance intValue] > 300)
                                                         alertMessage = @"The order cannot be delivered to the selected destination. Max delivery 300 miles.";
                                                     else if([self.distance floatValue] == 0)
                                                         alertMessage = @"The package can't be delivered to the selected destination. Please make sure Pick-up and Receiver address are different.";
                                                     
                                                     [UtilHelper showApplicationAlertWithMessage:alertMessage
                                                                                    withDelegate:self
                                                                                         withTag:3005
                                                                               otherButtonTitles:@[@"OK"]];
                                                 } else
                                                     [self validation:YES];
                                             });
                                         } else {
                                             
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 
                                                 self.distance = EMPTY_STRING;
                                                 self.distanceTime = EMPTY_STRING;
                                                 [UtilHelper showApplicationAlertWithMessage:@"Sorry! Dear customer please re-enter the pickup and dropoff locations." withDelegate:self withTag:3006
                                                                 otherButtonTitles:@[@"OK"]];
                                             });
                                             
                                         }
                                     }
                                 }];
    }
}

- (void)validation:(BOOL)willCallWebSevice {
    if (!_isTakenTime)
        return;
    else if (!_isDeliveryTime)
        return;
    else if (!_isFromAddress || !_isToAddress)
        return;
    else if (willCallWebSevice) {
        if (![UtilHelper isNetworkAvailable]) {
            [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
        
        //        [self showProgressHud];
        //        [self requestToUpdatePackDestination];
    }
}

- (void)requestToUpdatePackDestination {
    CSUser* userPack = [_markerArray objectAtIndex:0];
    //    for (int i = 1; i < _markerArray.count; i ++) {
    CSUser *userDeli = [_markerArray objectAtIndex:1];
    [WebServicesClient UpdatePack_Destination:_packId
                                  PackFromLat:[NSString stringWithFormat:@"%f", userPack.latitude]
                                  PackFromLng:[NSString stringWithFormat:@"%f", userPack.longitude]
                                 PackFromText:userPack.address
                                    PackToLat:[NSString stringWithFormat:@"%f", userDeli.latitude]
                                    PackToLng:[NSString stringWithFormat:@"%f", userDeli.longitude]
                                   PackToText:userDeli.address
                                     Distance:_distance
                                 DistanceTime:_distanceTime
                                    TakenTime:_takenDateTimeForWeb
                                    TakenType:_takenType
                                    TakenAsap:_takenAsap
                                 DeliveryTime:_deliveryDateTimeForWeb
                                 DeliveryType:_deliveryType
                                 DeliveryAsap:_deliveryAsap
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                                   SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                            completionHandler:^(SPPack *pack, NSError *error) {
                                
                                [self hideProgressHud];
                                
                                if (!error) {
                                    self.pack = pack;
                                    if (error.code == 3)
                                        [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                } else
                                    [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                            }];
    //    }
}

- (void)drawRoute {
    CSUser* userPack = [_markerArray objectAtIndex:0];
    CLLocation *locPack = [[CLLocation alloc] initWithLatitude:userPack.latitude longitude:userPack.longitude];
    
    for (int i = 1; i < _markerArray.count; i ++) {
        CSUser *userDeli = [_markerArray objectAtIndex:i];
        CLLocation *locDeli = [[CLLocation alloc] initWithLatitude:userDeli.latitude longitude:userDeli.longitude];
        
        [_googleMapView fetchPolylineWithOrigin:locPack color:ORANGE_LINE_COLOR destination:locDeli completionHandler:^(GMSPolyline *polyline) {
            if(polyline) {
                polyline.map = nil;
                polyline.map = self.googleMapView;
            }
        }];
    }
}

- (void)orderPlacedSuccessfully:(NSNotification *)notification {
    NSLog(@"order is placed.. ");
    
    _packId = EMPTY_STRING;
    _tfPromoCode.text = EMPTY_STRING;
    _tfItemValue.text = EMPTY_STRING;
    
    _packSizeId = 0;
    _selectedOption = 0;
    _btnNext.selected = NO;
    
    [self sizeUISetup:0];
}

#pragma mark -
#pragma mark - IBActions

- (IBAction)nextAction:(id)sender {
    
    if (!_btnNext.selected) {
        
        if (![UtilHelper isNetworkAvailable]) {
            [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND
                                           withDelegate:nil
                                                withTag:0
                                      otherButtonTitles:@[@"Ok"]];
            return;
        }
        
        //        int itemValue = [_tfItemValue.text intValue];
        //
        //        if (itemValue == 0) {
        //
        //            [UtilHelper showApplicationAlertWithMessage:@"Please enter the estimated cost of sending items. We are asking for insurance purpose." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        //
        //            return;
        //        }
        //        fromScedual = true;
        //        [self instantPaymentShowPrice:[_packSizeId integerValue]];
        //        [self updateAnExistingPack];
        //        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]) {
        //            [[AppDelegate sharedAppDelegate].appController showNewOrderDestinationView_fastorder:_distance DistanceTime:_distanceTime From:[_markerArray objectAtIndex:0] To:[_markerArray objectAtIndex:1] Pack_Size_ID:_packSizeId Promo_Code:_tfPromoCode.text Item_Value:_tfItemValue.text PackPrice:_packPrice Send_Cat_ID:_send_Cat_Id Waiting_Mins:_tfWaitingtime.text SenderData:nil];
        //        } else {
        //            [self createPack_PriceIfo:_packPrice tariff_Id:_tariff_id];
        //        }
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]) {
            [[AppDelegate sharedAppDelegate].appController showNewOrderDestinationView_fastorder:self->_distance DistanceTime:self->_distanceTime From:[self->_markerArray objectAtIndex:0] To:[_markerArray objectAtIndex:1] Pack_Size_ID:_packSizeId Promo_Code:_tfPromoCode.text Item_Value:_tfItemValue.text PackPrice:_packPrice Send_Cat_ID:_send_Cat_Id Waiting_Mins:_tfWaitingtime.text Default_Tariff_Plan:_tariff_id SenderData:nil];
        } else {
            [self createPack_PriceIfo:self->_packPrice tariff_Id:_tariff_id];
        }
    }
}

- (IBAction)sizeSelectionAction:(UIButton *)sender {
    
    _packSizeId = [NSString stringWithFormat:@"%d", sender.tag];
    [self instantPaymentShowPrice:(int)sender.tag];
    [self sizeUISetup:(int)sender.tag];
    [_btnUrgent setSelected:true];
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height+40);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
    //    [_btnUrgent setSelected:true];
}

- (IBAction)timeSelectionAction:(UIButton *)sender {
    [self timeUISetup:(int)sender.tag];
}

- (void)sizeUISetup:(NSInteger)tag {
    
    if ([self getshowString:price_urgent] != NULL || [self getshowString:price_urgent] != nil) {
        _lblUrgentPrice.text = [NSString stringWithFormat:@"$%@", [self getshowString:price_urgent]];
    }
    if ([self getshowString:price_within12h] != NULL || [self getshowString:price_within12h] != nil) {
        _lbl12Price.text = [NSString stringWithFormat:@"$%@", [self getshowString:price_within12h]];
    }
    _lblUrgentDescription.text = description_urgent;
    _lbl12Description.text = description_within12h;
    _lblSmallSize.textColor = [UIColor blackColor];
    _lblMediumSize.textColor = [UIColor blackColor];
    _lblLargeSize.textColor = [UIColor blackColor];
    _lblSmallSize.alpha = 0.4;
    _lblMediumSize.alpha = 0.4;
    _lblLargeSize.alpha = 0.4;
    _imgSmallSize.tintColor = self.view.tintColor;
    _imgMediumSize.tintColor = self.view.tintColor;
    _imgLargeSize.tintColor = self.view.tintColor;
    
    if (tag == 0) {
        isSizeSelected = false;
        
        [_timeView setHidden:true];
        _buttonViewSpacing.constant = 0;
        _lblsizeDescription.text = @"";
        _lblsizeDescription.textColor = LIGHT_ORANGE_COLOR;
        [UIView animateWithDuration:0.3 animations:^{
            [self.view layoutIfNeeded];
        }];
        [self checkActiveButton:@"1"];
        return;
    }
    if (!isSizeSelected) {
        [_timeView setHidden:false];
        
        if (_packPrice != NULL || _packPrice != nil) {
            _lblUrgentPrice.text = [NSString stringWithFormat:@"$%@", _packPrice];
        }
        _buttonViewSpacing.constant = 120;
        
        [UIView animateWithDuration:0.1 animations:^{
            [self.view layoutIfNeeded];
        }];
        CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
        [self.scrollView setContentOffset:bottomOffset animated:YES];
    }
    
    
    if (tag == 1) {
        //        GetDictByName *dictByName = _packSizesList[0];
        //        _packSizeId = [dictByName.listId intValue];
        _imgSmallSize.tintColor = LIGHT_ORANGE_COLOR;
        _lblSmallSize.textColor = LIGHT_ORANGE_COLOR;
        _lblSmallSize.alpha = 1;
        _lblsizeDescription.text = @"Fits into front seat of the car. Be certain pack is 25 lbs or less";
    } else if (tag == 2) {
        //        GetDictByName *dictByName = _packSizesList[1];
        //        _packSizeId = [dictByName.listId intValue];
        _imgMediumSize.tintColor = LIGHT_ORANGE_COLOR;
        _lblMediumSize.textColor = LIGHT_ORANGE_COLOR;
        _lblMediumSize.alpha = 1;
        _lblsizeDescription.text = @"Fit in the back seat of the car. Be certain pack is between 26-50 lbs";
    } else if (tag == 3) {
        //        GetDictByName *dictByName = _packSizesList[2];
        //        _packSizeId = [dictByName.listId intValue];
        _imgLargeSize.tintColor = LIGHT_ORANGE_COLOR;
        _lblLargeSize.textColor = LIGHT_ORANGE_COLOR;
        _lblLargeSize.alpha = 1;
        _lblsizeDescription.text = @"Fit in a truck (car, hatchback or SUV). Be certain pack is between 51-70 lbs";
    }
    isSizeSelected = true;
    
    //    [_btn12 setSelected:true];
    [self checkActiveButton:@"1"];
}

- (void)timeUISetup:(NSInteger)tag {
    _lblUrgentPrice.textColor = _lbl12Price.textColor = [UIColor blackColor];
    _lblUrgentDescription.textColor = _lbl12Description.textColor = [UIColor blackColor];
    [_btnUrgent setSelected:false];
    [_btn12 setSelected:false];
    
    if (tag == 11) {
        [_btnUrgent setSelected:true];
        _packPrice = price_urgent;
        _lblUrgentPrice.textColor = LIGHT_ORANGE_COLOR;
        _lblUrgentDescription.textColor = LIGHT_ORANGE_COLOR;
        _tariff_id = @"1";
    } else {
        [_btn12 setSelected:true];
        _packPrice = price_within12h;
        _lbl12Price.textColor = LIGHT_ORANGE_COLOR;
        _lbl12Description.textColor = LIGHT_ORANGE_COLOR;
        _tariff_id = @"2";
    }
    [self checkActiveButton:@"1"];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1001)
        [self senderLogout];
    else if(alertView.tag == 3005){
        _scrollView.scrollEnabled = false;
    } else {
        [self checkActiveButton:@"1"];
    }
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *textFullyEntered = [textField.text stringByReplacingCharactersInRange:range withString:string];
    //    _packSizeId = 0;
    [self checkActiveButton:textFullyEntered];
    [self sizeUISetup:0];
    //    [self timeUISetup:0];
    return YES;
}


#pragma mark--
#pragma mark-- Server Side handling

- (void)checkActiveButton:(NSString *)numberOFCharacter {
    BOOL isActive = YES;
    for (JVFloatLabeledTextField *tfText in self.validationArray) {
        if (tfText.text.length > 0) {
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
        } else {
            isActive = NO;
            break;
        }
    }
    if (!isSizeSelected){
        isActive = NO;
    }
    if (_packSizeId == 0)
        isActive = NO;
    
    if (isActive) {
        _btnNext.backgroundColor = LOGIN_ACTIVE_COLOR;
        _btnNext.selected = NO;
    } else {
        _btnNext.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnNext.selected = YES;
    }
}

//MARK: Table View Delegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    _tableHeightConstraint.constant = 44 * _markerArray.count;
    [self.view layoutIfNeeded];
    return _markerArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addressCell" forIndexPath:indexPath];
    CSUser *user = [_markerArray objectAtIndex:indexPath.row];
    
    [cell.btnEdit setTag:indexPath.row];
    [cell.btnRemove setTag:indexPath.row];
    [cell.btnEdit addTarget:self action:@selector(editTapped:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnRemove addTarget:self action:@selector(removeTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.topDotsImageView.hidden = indexPath.row == 0;
    cell.botDotsImageView.hidden = indexPath.row == _markerArray.count - 1;
    cell.lblSeparator.hidden = indexPath.row == _markerArray.count - 1;
    
    cell.lblAddress.text = user.address;
    
    [cell.ovalImageView setImage:[UIImage imageNamed:indexPath.row == 0 ? @"OvalPick" : @"OvalDest"]];
    [cell.btnEdit setImage:[UIImage imageNamed:indexPath.row == 0 ? @"EditPick" : @"EditDest"] forState:UIControlStateNormal];
    //    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)editTapped:(UIButton*)sender {
    _addressIndex = (int) sender.tag;
    [self showPlacesView];
}
- (IBAction)editBtTapped:(UIButton *)sender {
    _addressIndex = (int)sender.tag;
    if (_addressIndex == 0) {
        [_addressTextField1 becomeFirstResponder];
        [_addressTextField1 setText:@""];
    } else {
        [_addressTextField2 becomeFirstResponder];
        [_addressTextField2 setText:@""];
    }
}
- (IBAction)removeBtTapped:(UIButton *)sender {
    if (sender.tag < 2) {
        CSUser* user = [_markerArray objectAtIndex:sender.tag];
        user.address = [[NSString alloc]initWithFormat:@"Select %@ Address", sender.tag == 0 ? @"Pick-up" : @"Delivery"];
        user.latitude = user.longitude = 0;
        if (sender.tag == 0) {
            [_addressTextField1 setText:@""];
        } else {
            [_addressTextField2 setText:@""];
        }
        [self.view layoutIfNeeded];
        [_markerArray replaceObjectAtIndex:sender.tag withObject:user];
        [self reloadMarkerOfMap];
        
    } else
        [_markerArray removeObjectAtIndex:sender.tag];
}
- (IBAction)addressTapped:(UIButton *)sender {
    _addressIndex = (int)sender.tag;
    if (_addressIndex == 0) {
        [_addressTextField1 becomeFirstResponder];
    } else {
        [_addressTextField2 becomeFirstResponder];
        
    }
    [self showPlacesView];
}


-(void)removeTapped:(UIButton*)sender {
    if (sender.tag < 2) {
        CSUser* user = [_markerArray objectAtIndex:sender.tag];
        user.address = [[NSString alloc]initWithFormat:@"Select %@ Address", sender.tag == 0 ? @"Pick-up" : @"Delivery"];
        user.latitude = user.longitude = 0;
        [_markerArray replaceObjectAtIndex:sender.tag withObject:user];
    } else
        [_markerArray removeObjectAtIndex:sender.tag];
    //    [_tableView reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _addressIndex = (int) indexPath.row;
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    [self showPlacesView];
}

- (IBAction)addLocationAction:(UIButton *)sender {
    NSString *addrPack = ((CSUser*)[_markerArray objectAtIndex:0]).address;
    NSString *addrDeli = ((CSUser*)[_markerArray objectAtIndex:1]).address;
    if ([addrPack containsString:@"Select"] || [addrDeli containsString:@"Select"]) {
        [UtilHelper showApplicationAlertWithMessage:@"Please select addresses above" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    _addressIndex = (int) _markerArray.count;
    [self showPlacesView];
}

- (void)updateAnExistingPack {
    [self showProgressHud];
    [WebServicesClient UpdatePack_GenInfo:_packId
                                  PetType:@""
                                 PetCount:@""
                                 PetISVac:@""
                                 SendName:@""
                                   isPack:YES
                               PackSizeId:_packSizeId
                            PackOptionsId:[NSString stringWithFormat:@"%li",(long)_selectedOption]
                               PackWeight:_tfPromoCode.text
                                ItemValue:_tfItemValue.text
                              Description:@""
                             ReceiverName:@""
                      ReceiverPhoneNumber:@""
                              LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                             LogUserAgent:[[UIDevice currentDevice] systemVersion]
                            LogDeviceType:LOG_DEVICE_TYPE
                               SessionKey:[AppDelegate sharedAppDelegate].sessionKey//@"5BD81926-3EBD-4181-BE1A-ED293C10E6BA2017060307400"
                        completionHandler:^(int packId, NSError *error) {
                            [self hideProgressHud];
                            
                            if (!error) {
                                if (self.draftdata != nil) {
                                    [[NSNotificationCenter defaultCenter] postNotificationName:SENDER_ACTIVE_LIST_UPDATED object:@{}];
                                    [[AppDelegate sharedAppDelegate].appController showNewOrderDestinationView:EMPTY_STRING PackPrice:@"0" Send_Cat_ID:self->_send_Cat_Id ItemPrice:self.tfItemValue.text SenderData:self->_draftdata];
                                } else
                                    [[AppDelegate sharedAppDelegate].appController showNewOrderDestinationView:self.packId PackPrice:@"0" Send_Cat_ID:self->_send_Cat_Id ItemPrice:self->_tfItemValue.text SenderData:nil];
                            } else if (error.code == 3)
                                [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                            else
                                [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                        }];
}

- (void)createPack_PriceIfo:(NSString*)price tariff_Id:(NSString*)tariff_id {
    
    [self showProgressHud];
    CSUser *from = [_markerArray objectAtIndex:0];
    CSUser *to = [_markerArray objectAtIndex:1];
    NSCalendar * cal = [NSCalendar currentCalendar];
    NSDateComponents * comp = [cal components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSInteger day = [comp weekday];
    NSString *daystr = [NSString stringWithFormat:@"%ld", (long)day];
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    
    if ([_tfItemValue.text isEqualToString:EMPTY_STRING]) {
        
        _tfItemValue.text = @"0";
    }
    
    [WebServicesClient Createpack_PriceInfo:_tfItemValue.text
                               Pack_Size_ID:_packSizeId
                                   Distance:_distance
                              Distance_Time:_distanceTime
                             Pack_From_Text:from.address
                              Pack_From_Lng:[[NSNumber numberWithDouble:from.longitude] stringValue]
                              Pack_From_Lat:[[NSNumber numberWithDouble:from.latitude] stringValue]
                               Pack_To_Text:to.address
                                Pack_To_Lng:[[NSNumber numberWithDouble:to.longitude] stringValue]
                                Pack_To_Lat:[[NSNumber numberWithDouble:to.latitude] stringValue]
                         Is_Insurance_Payed:@"1"
                                 Pack_Price:price
                                        Day:daystr
                                       Time:_tempTime
                             Tariff_Plan_ID:tariff_id
                                Send_Cat_ID:_send_Cat_Id
                               Waiting_Mins:_tfWaitingtime.text
                                 Promo_Code:_tfPromoCode.text
                                LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                               LogUserAgent:[[UIDevice currentDevice] systemVersion]
                              LogDeviceType:LOG_DEVICE_TYPE
                                 SessionKey:[AppDelegate sharedAppDelegate].sessionKey//@"5BD81926-3EBD-4181-BE1A-ED293C10E6BA2017060307400"
                          completionHandler:^(int inserted_id, NSError *error) {
                              [self hideProgressHud];
                              if (!error) {
                                  [[AppDelegate sharedAppDelegate].appController showNewOrderDestinationView:[NSString stringWithFormat:@"%d", inserted_id] PackPrice:price Send_Cat_ID:self.send_Cat_Id ItemPrice:_tfItemValue.text SenderData:nil];
                              } else {
                                  
                              }
                          }];
}

- (void)instantPaymentShowPrice:(NSInteger)tag {
    
    [self showProgressHud];
    NSCalendar * cal = [NSCalendar currentCalendar];
    NSDateComponents * comp = [cal components:NSCalendarUnitWeekday fromDate:[NSDate date]];
    NSInteger day = [comp weekday];
    NSString *daystr = [NSString stringWithFormat:@"%ld", (long)day];
    NSDate * now = [NSDate date];
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setDateFormat:@"HH:mm"];
    NSString *newDateString = [outputFormatter stringFromDate:now];
    _tempTime = newDateString;
    
    if ([_tfItemValue.text isEqualToString:EMPTY_STRING]) {
        
        _tfItemValue.text = @"0";
    }
    
    [WebServicesClient InstantPaymentShowPrice:_distance
                                 Distance_time:_distanceTime
                                    Item_Value:_tfItemValue.text
                                  Pack_Size_ID:_packSizeId
                                        Cat_Id:_send_Cat_Id
                                           Day:daystr
                                          Time:newDateString
                                  Waiting_Mins:_tfWaitingtime.text
                                    Promo_Code:_tfPromoCode.text
                                   LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                  LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                 LogDeviceType:LOG_DEVICE_TYPE
                             completionHandler:^(id result, NSError *error) {
                                 [self hideProgressHud];
                                 if (!error) {
                                     NSDictionary *resultDict = (NSDictionary *)result;
                                     
                                     if ([resultDict[@"code"] intValue] == 0) {
                                         
                                         NSMutableArray *resultArray = resultDict[@"details"];
                                         
                                         
                                         NSDictionary *result = resultArray[0];
                                         
                                         price_urgent = [result[@"pack_price_with_ins"] stringValue];
                                         description_urgent = result[@"tariff_desc"];
                                         result = resultArray[1];
                                         price_within12h = [result[@"pack_price_with_ins"] stringValue];
                                         self.packPrice = price_urgent;
                                         description_within12h = result[@"tariff_desc"];
                                         
                                         if (self->fromScedual) {
                                             if (self->_btnUrgent.selected) {
                                                 self.packPrice = price_urgent;
                                             } else if (self->_btn12.selected){
                                                 self.packPrice = price_within12h;
                                             }
                                             if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]) {
                                                 [[AppDelegate sharedAppDelegate].appController showNewOrderDestinationView_fastorder:self->_distance DistanceTime:self->_distanceTime From:[self->_markerArray objectAtIndex:0] To:[_markerArray objectAtIndex:1] Pack_Size_ID:_packSizeId Promo_Code:_tfPromoCode.text Item_Value:_tfItemValue.text PackPrice:_packPrice Send_Cat_ID:_send_Cat_Id Waiting_Mins:_tfWaitingtime.text Default_Tariff_Plan:_tariff_id SenderData:nil];
                                             } else {
                                                 [self createPack_PriceIfo:self->_packPrice tariff_Id:_tariff_id];
                                             }
                                             fromScedual = false;
                                         } else {
                                             [self sizeUISetup:tag];
                                             [self timeUISetup:11];
                                         }
                                         
                                         //                                         UserModel *user = [UserModel modelObjectWithDictionary:resultArray[0]];
                                         //                                         completionHandler(user, nil);
                                     }
                                 }else {
                                     
                                 }
                             }];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = scrollView.contentOffset;
    float limit = _googleMapView.frame.origin.y + _googleMapView.frame.size.height - _locationView.frame.size.height;
    if (offset.y < -limit) {
        offset.y = -limit;
        [scrollView setContentOffset:offset];
    }
    _googleMapView.alpha = 0.75 - fabs(offset.y) / limit;
}

- (NSString *)getshowString:(NSString *)str{
    int i = 0;
    for (i; i<str.length; i ++) {
        if([str characterAtIndex:i] == '.')
            break;
    }
    if (i < (str.length - 2)){
        NSString *newstr = [str substringToIndex:i+3];
        return newstr;
    }
    return str;
}

#pragma mark - Place search Textfield Delegates

- (void)placeSearch:(MVPlaceSearchTextField *)textField ResponseForSelectedPlace:(GMSPlace *)responseDict{
    
    
    
    [self.view endEditing:YES];
    //Only allow user to select a place from list if device location is turned on
    //    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    //    {
    //        textField.text = EMPTY_STRING;
    //        return;
    //    }
    //    else
    //    {
    [textField resignFirstResponder];
    if (textField.tag == 0) {
        _addressIndex = 0;
    } else {
        _addressIndex = 1;
    }
    [self placeMarker:responseDict];
    _addressTextField.hidden = true;
    //        [self.navigationController popViewControllerAnimated:NO];
    //    [self.view removeFromSuperview];
    //    [self removeFromParentViewController];
    //    }
}

- (void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField
{
    
}

- (void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField
{
    //    [self hideETAandRelatedContent];
}

- (void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
    cell.contentView.backgroundColor = [UIColor whiteColor];
}


@end
