//
//  AddPaymentMethodVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/23.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "AddPaymentMethodVC.h"
#import <Stripe/Stripe.h>

@interface AddPaymentMethodVC () <UIPickerViewDelegate, UIPickerViewDataSource, STPPaymentCardTextFieldDelegate> {
    int numberOfRows;
    int currentMonth;
    int currentYear;
    NSArray *months;
    NSMutableArray *years;
}

@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfDateTime;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfCardNumber;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfCardHolderName;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfBillingAddress;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfZipCode;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfCVV;

@property (weak, nonatomic) STPPaymentCardTextField *paymentTextField;

@property (strong, nonatomic) NSArray *textFieldArray;
@property (strong, nonatomic) UIPickerView *picker;

@end

@implementation AddPaymentMethodVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.paymentTextField becomeFirstResponder];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    CGFloat padding = 120;
    CGFloat width = CGRectGetWidth(self.view.frame) - 32;
    self.paymentTextField.frame = CGRectMake(16, padding, width, 44);
}

#pragma mark
#pragma mark -- Helpers

- (void)prepareUI
{
    self.btnSave = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnSave withRadius:_btnSave.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    numberOfRows = 4;
    
    self.btnSave.selected = YES;
    self.textFieldArray = @[_tfCardNumber, _tfDateTime,_tfCVV];
    
    STPPaymentCardTextField *paymentTextField = [[STPPaymentCardTextField alloc] init];
    paymentTextField.delegate = self;
    paymentTextField.cursorColor = [UIColor purpleColor];
    self.paymentTextField = paymentTextField;
    [self.view addSubview:paymentTextField];
}

- (void)paymentCardTextFieldDidChange:(nonnull STPPaymentCardTextField *)textField {
    
    if (textField.isValid) {
        
        _btnSave.backgroundColor = LOGIN_ACTIVE_COLOR;
        _btnSave.selected = NO;
    }
}

#pragma mark
#pragma mark --IBActions

- (IBAction)saveAction:(id)sender
{
    if (!self.btnSave.selected)
    {
        [self.paymentTextField resignFirstResponder];
        [self showProgressHud];
        [self requestToAddPayment];
    }
}

- (IBAction)datePickerAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select Expiry Date" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Ok", nil];
    alert.delegate = self;
    alert.tag = 100;
    _picker = [[UIPickerView alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
    [self initPickerData ];
    _picker.delegate = self;
    _picker.dataSource = self;
    
    [alert addSubview:_picker];
    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
    [alert setValue:_picker forKey:@"accessoryView"];
    [alert show];
}

- (void)initPickerData
{
    months = [[NSArray alloc] initWithObjects:@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"June", @"July", @"Aug", @"Sept", @"Oct", @"Nov", @"Dec", nil];
    
    NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
    formatter.dateFormat        = @"MM-yyyy";
    NSString *string            = [formatter stringFromDate:[NSDate date]];
    
    NSArray *comps = [string componentsSeparatedByString:@"-"];
    currentMonth    = [[comps objectAtIndex:0] intValue] - 1;
    currentYear     = [[comps objectAtIndex:1] intValue];
    
    years   = [[NSMutableArray alloc] init];
    for (int i = 0; i < 12; i++)
    {
        [years addObject:[NSString stringWithFormat:@"%i", currentYear + i]];
    }
}

#pragma mark - Picker View Delegates -

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return 12;
    }
    return [years count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [months objectAtIndex:row];
    }
    return [years objectAtIndex:row];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if (buttonIndex == 1) {
//        
//        NSString *year = [[self pickerView:_picker titleForRow:[_picker selectedRowInComponent:1] forComponent:1] substringWithRange:NSMakeRange(2,2)];
//        
//        NSString *dateText = [NSString stringWithFormat:@"   %@/%@",
//                              [NSString stringWithFormat: @"%ld", ([_picker selectedRowInComponent:0] + 1)],year];
//        
//        self.tfDateTime.text = dateText;
//        [self checkActiveButton:dateText];
//    }
    if(alertView.tag == 999)
    {
        [self backAction:nil];
    }
    
    else if (alertView.tag == 2001)
    {
        [self senderLogout];
    }
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self checkActiveButton:numberOFCharacter];
    
    return YES;
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)checkActiveButton:(NSString *)numberOFCharacter
{
    BOOL isActive = NO;
    
    for (JVFloatLabeledTextField *tfText in self.textFieldArray) {
        
        if (tfText.text.length > 0) {
            
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
        }
        else {
            
            isActive = NO;
            break;
        }
    }
    
    if (isActive) {
        
        _btnSave.backgroundColor = LOGIN_ACTIVE_COLOR;
        _btnSave.selected = NO;
        
    } else {
        
        _btnSave.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnSave.selected = YES;
    }
}

- (void)requestToAddPayment
{
    
    if (![self.paymentTextField isValid]) {
        [self hideProgressHud];

        return;
    }
    if (![Stripe defaultPublishableKey]) {
        [self hideProgressHud];

        [UtilHelper showApplicationAlertWithMessage:@"The payment account of the admin has some issues. Please try again. If the problem persists, please contact senpex administrator."
                                       withDelegate:nil
                                            withTag:2001
                                  otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    [[STPAPIClient sharedClient] createTokenWithCard:self.paymentTextField.cardParams
                                          completion:^(STPToken *token, NSError *error) {
                                              
                                              [self hideProgressHud];
                                              
                                              if (error) {
                                                  
                                                  [UtilHelper showApplicationAlertWithMessage:[error localizedDescription] withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                                              }
                                              else {
                                                  
                                                  [UtilHelper showApplicationAlertWithMessage:@"Credit card details successfully added" withDelegate:self withTag:999 otherButtonTitles:@[@"Ok"]];
                                                  
                                                  //Add object in database
                                                  
                                                  CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
                                                  
                                                  BOOL isUpdate = NO;
                                                  if (cardInfo != nil) {
                                                      
                                                      isUpdate = YES;
                                                  }
                                                  
                                                  NSString *cardNumber = [_paymentTextField.cardParams.number substringFromIndex: [_paymentTextField.cardParams.number length] - 4];

                                                  NSString *appendString = [NSString stringWithFormat:@"****%@",cardNumber];
                                                  
                                                  NSString *encryptedToken = [FBEncryptorAES encryptBase64String:token.tokenId                        keyString:@"token" separateLines:YES];
                                                  
                                                  [CardInfoDB insertAndupdateCardInfoWithId:1
                                                                                CardNumber:appendString
                                                                               StripeToken:encryptedToken
                                                                                  IsUpdate:isUpdate];
                                              }
                                          }];
}



@end
