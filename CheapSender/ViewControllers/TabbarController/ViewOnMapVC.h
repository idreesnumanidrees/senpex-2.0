//
//  ViewOnMapVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/03.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "GoogleMapHelper.h"
#import "CSUser.h"

@interface ViewOnMapVC : BaseViewController <GMSMapViewDelegate>

@property (nonatomic, strong) NSString *comingView;

@property (nonatomic, strong) PackDetailsModel *packDetails;
@property (nonatomic, strong) CourierListData *courierListData;

@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UILabel *lblHeadingTitle;
@property (weak, nonatomic) IBOutlet UILabel *fromAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *toAddressLabel;

@property (assign, nonatomic) BOOL isComingFromCourierApp;

//Miles and Times
@property (weak, nonatomic) IBOutlet UIView *etaView;
@property (weak, nonatomic) IBOutlet UILabel *milesLabel;
@property (weak, nonatomic) IBOutlet UILabel *etaLabel;

//GoogleMapHelper instance variable
@property (strong, nonatomic) IBOutlet GoogleMapHelper *googleMapView;
@property (nonatomic, strong) NSMutableArray *markerArray;

@end
