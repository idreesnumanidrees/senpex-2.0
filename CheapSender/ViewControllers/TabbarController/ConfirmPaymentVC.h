//
//  ConfirmPaymentVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/23.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface ConfirmPaymentVC : BaseViewController

@property (nonatomic, strong) NSString *packId;
@property (nonatomic, assign) Boolean asktofriend;
@property (nonatomic, assign) Boolean fastOrder;

@end
