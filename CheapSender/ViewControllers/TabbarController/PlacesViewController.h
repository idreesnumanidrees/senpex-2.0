//
//  PlacesViewController.h
//  CheapSender
//
//  Created by admin on 4/29/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "MVPlaceSearchTextField.h"

@protocol PlacesViewControllerDelegate <NSObject>

- (void)placeMarker:(GMSPlace *)responseDict;

@end

@interface PlacesViewController : BaseViewController<PlaceSearchTextFieldDelegate>

@property (nonatomic, assign) id <PlacesViewControllerDelegate> placesDelegate;

//MVPlaceSearchTextField instance variable
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *placeSearchTextField;
@property (strong, nonatomic) IBOutlet UILabel *lblHeading;

@property (strong, nonatomic) NSString *headingName;

@end
