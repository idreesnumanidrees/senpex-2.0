//
//  NewOrderPetInfoVC.m
//  CheapSender
//
//  Created by admin on 4/22/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "NewOrderPetInfoVC.h"
#import "MediaUploadingViewCell.h"
#import "PlacesViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIKit/UIKit.h"

@interface NewOrderPetInfoVC () <PlacesViewControllerDelegate>

@end

@implementation NewOrderPetInfoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    if (!_isFirstTime) {
        
        [self updateCameraBox:YES];
        _isFirstTime = YES;
    }
}

#pragma mark -
#pragma mark - Helpers

- (void)prepareUI
{
    self.btnNext = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnNext withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    
    self.btnTermCondition = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnTermCondition withRadius:3.0 color:BORDER_COLOR width:1];
    
    _selectedMediaArray = [NSMutableArray new];
    _petTypeList = [NSArray new];
    _packSizesList = [NSArray new];
    _petVacinated = [NSArray new];
    
    _pickerOptionsList = @[@"Fit to car (baggage + front)", @"Fitted to a car (front & Back)", @"Fitted in a shoe-box", @"Fit in A4 pack", @"Fit to airplane baggage",@"Fit to van",@"Fit to Lorry",@"Fit to motorcycle"];
    
    //    _petTypeList = @[@"House dog", @"House cat", @"Aquarium animals (fishes and others)", @"Birds", @"Small Pets: Mammals and rodents (Rabbits, Mice, etc)"];
    
    if ([AppDelegate sharedAppDelegate].allDictData.count > 0) {        
        NSPredicate *petTypesPred = [NSPredicate predicateWithFormat:@"dicShort contains[c] %@", @"pack_options"];
        NSMutableArray *petTypes = [[[AppDelegate sharedAppDelegate].allDictData filteredArrayUsingPredicate:petTypesPred] mutableCopy];
        _petTypeList = petTypes;
        
        NSPredicate *packSizesPred = [NSPredicate predicateWithFormat:@"dicShort contains[c] %@", @"pack_sizes"];
        NSMutableArray *packSizes = [[[AppDelegate sharedAppDelegate].allDictData filteredArrayUsingPredicate:packSizesPred] mutableCopy];
        _packSizesList = packSizes;
        
        NSPredicate *petVacinatedPred = [NSPredicate predicateWithFormat:@"dicShort contains[c] %@", @"yes_no"];
        NSMutableArray *petVacinated = [[[AppDelegate sharedAppDelegate].allDictData filteredArrayUsingPredicate:petVacinatedPred] mutableCopy];
        _petVacinated = petVacinated;
    }
    
#pragma mark -- Web related
    _btnNext.selected = YES;
    _packSizeId = 0;
    _selectedOption = 0;
    _petTypeIndex = 0;
    _petIsVacIndex = -1;
    
    _validationArray = @[_tfSendingName, _tfNumberOfPets, _tfLbs, _tfPetValue, _tfReceiverName,_tfReceiverPhone, _descriptionTextView];
    
    [self petTypeSetup];
    [self populateData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    [_descriptionTextView setTextContainerInset:UIEdgeInsetsMake(10, 5, 10, 16)];
    
}

- (void)populateData
{
    if (_draftdata != nil) {
        
        _packId = _draftdata.dataIdentifier;
        _tfSendingName.text = _draftdata.sendName;
        _tfLbs.text = _draftdata.packWeight;
        _tfPetValue.text = _draftdata.itemValue;
        _tfReceiverName.text = _draftdata.receiverName;
        _tfReceiverPhone.text = _draftdata.receiverPhoneNumber;
        _descriptionTextView.text = _draftdata.descText;
        _tfNumberOfPets.text = _draftdata.petCount;
        
        
        if ([_descriptionTextView.text length] > 0) {
            
            self.lblUptoInsure.text = [NSString stringWithFormat:@"$%@", _draftdata.itemValue];
            _placeholderLabel.text = @"";
            _btnTermCondition.selected = YES;
        }
        
        for (GetDictByName *dictByName in _petVacinated) {
            
            if ([dictByName.listId isEqualToString:_draftdata.petIsVac]) {
                
                _petIsVacIndex = [_draftdata.petIsVac intValue];
                
                if ([dictByName.listName isEqualToString:@"Yes"]) {
                    [self vaccinationSetup:500];
                }
                else if ([dictByName.listName isEqualToString:@"No"]) {
                    [self vaccinationSetup:501];
                }
                break;
            }
        }
        
        for (GetDictByName *dictByName in _packSizesList) {
            
            if ([dictByName.listId isEqualToString:_draftdata.packSizeId]) {
                
                _packSizeId = [_draftdata.packSizeId intValue];
                
                if ([dictByName.listName isEqualToString:@"Small"]) {
                    [self sizeUISetup:100];
                }
                else if ([dictByName.listName isEqualToString:@"Medium"]) {
                    [self sizeUISetup:101];
                }
                else if ([dictByName.listName isEqualToString:@"Big"]) {
                    [self sizeUISetup:102];
                }
                break;
            }
        }
        
        int petIndex = 0;
        for (GetDictByName *dictByName in _petTypeList) {
            
            if ([dictByName.listId isEqualToString:_draftdata.petType]) {
                
                _petTypeIndex = petIndex;
                break;
            }
            petIndex ++;
        }
        
        if (_petTypeIndex > 0) {
            
            for (UIButton *petName in self.PetTypeScrollView.subviews) {
                
                if ([petName isKindOfClass:[UIButton class]]) {
                    
                    if (petName.tag == _petTypeIndex) {
                        
                        [self petTypeSelection:petName];
                        break;
                    }
                }
            }
        }
        
        [self requestForAllImages];
        [self checkActiveButton:@"1"];
        
    } else {
        
        [_tfSendingName becomeFirstResponder];
        [_tfSendingName setValue:PACK_GREEN_STATUS_COLOR
                      forKeyPath:@"_placeholderLabel.textColor"];
    }
    
}

- (void)updateCameraBox:(BOOL)isZeroImage
{
    if (isZeroImage) {
        
        _cameraWidthConstraint.constant = 160;
        _cameraTrailingConstraint.constant = (self.view.frame.size.width-160)/2;
        self.lblAddPic.text = @"Add a Photo(s)";
    } else {
        
        _cameraWidthConstraint.constant = 100;
        _cameraTrailingConstraint.constant = 0;
        self.lblAddPic.text = @"Add a pic";
    }
}

- (void)textChanged:(NSNotification *)notification
{
    if([[_descriptionTextView text] length] == 0) {
        
        //Your feedback
        _placeholderLabel.text = @"Description (specific needs)";
    }
    else
    {
        _placeholderLabel.text = @"";
    }
}

- (void)petTypeSetup
{
    CGRect buttonFrame = CGRectMake(0.0f, 7.0f, 60, 40.0f);
    
    for (int index = 0; index < _petTypeList.count; index++) {
        
        UIButton *petName = [[UIButton alloc]initWithFrame:buttonFrame];
        [petName addTarget:self action:@selector(petTypeSelection:) forControlEvents:UIControlEventTouchUpInside];
        [petName setTag:index];
        
        GetDictByName *getDictByName = _petTypeList[index];
        
        [petName setTitle:getDictByName.listName forState:UIControlStateNormal];
        [petName setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        petName.titleLabel.font = [UIFont fontWithName:@"SFUIText-Regular" size:14];
        petName.titleLabel.textAlignment = NSTextAlignmentLeft;
        petName.alpha = 0.41;
        
        [self.PetTypeScrollView addSubview:petName];
        buttonFrame.origin.x+= buttonFrame.size.width+5.0f;
    }
    
    CGSize contentSize = self.PetTypeScrollView.frame.size;
    contentSize.width = buttonFrame.origin.x;
    [self.PetTypeScrollView setContentSize:contentSize];
}

#pragma mark -
#pragma mark - IBActions
- (IBAction)senderRulesAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showTermsView:@"pet_rule"];
}

- (IBAction)petTypeSelection:(UIButton *)sender
{
    for (UIButton *petName in self.PetTypeScrollView.subviews) {
        
        if ([petName isKindOfClass:[UIButton class]]) {
            
            [petName setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            petName.alpha = 0.41;
        }
    }
    
    [sender setTitleColor:LIGHT_ORANGE_COLOR forState:UIControlStateNormal];
    sender.alpha = 1;
    
    GetDictByName *getDictByName = _petTypeList[sender.tag];
    
    _petTypeIndex = [getDictByName.listId intValue];
    [self checkActiveButton:@"1"];
}

- (IBAction) nextAction:(id)sender
{
    if (!_btnNext.selected) {
        
        if (![UtilHelper isNetworkAvailable]) {
            
            [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
        
        [self requestToUpdateGeneralInfo];
    }
}

- (IBAction)termConditionAction:(UIButton *)sender {
    
    sender.selected = !sender.selected;
    
    [self checkActiveButton:@"1"];
}

- (IBAction)sizeSelectionAction:(UIButton *)sender {
    
    [self sizeUISetup:sender.tag];
}

- (void)sizeUISetup:(NSInteger)tag
{
    
    _lblSmallSize.textColor = [UIColor blackColor];
    _lblMediumSize.textColor = [UIColor blackColor];
    _lblLargeSize.textColor = [UIColor blackColor];
    _lblSmallSize.alpha = 0.4;
    _lblMediumSize.alpha = 0.4;
    _lblLargeSize.alpha = 0.4;
    
    if (tag == 100) {
        
        GetDictByName *dictByName = _packSizesList[0];
        _packSizeId = [dictByName.listId intValue];
        _lblSmallSize.textColor = LIGHT_ORANGE_COLOR;
        _lblSmallSize.alpha = 1;
        _lblSizeDescription.text = @"Never exceeds a small box or bag, fitted front seat of the car";
        
    } else if (tag == 101) {
        
        GetDictByName *dictByName = _packSizesList[1];
        _packSizeId = [dictByName.listId intValue];
        _lblMediumSize.textColor = LIGHT_ORANGE_COLOR;
        _lblMediumSize.alpha = 1;
        _lblSizeDescription.text = @"Fit in the backseat of the car";
        
    }else if (tag == 102) {
        
        GetDictByName *dictByName = _packSizesList[2];
        _packSizeId = [dictByName.listId intValue];
        _lblLargeSize.textColor = LIGHT_ORANGE_COLOR;
        _lblLargeSize.alpha = 1;
        _lblSizeDescription.text = @"fits in a trunk (car, hatchback or SUV)";
    }
    
    [self checkActiveButton:@"1"];
    _lblSizeDescription.hidden = NO;
    _sizeTypeViewHeightConstraint.constant = 135;
}

- (IBAction)vaccinationAction:(UIButton *)sender {
    
    [self vaccinationSetup:sender.tag];
}

- (void)vaccinationSetup:(NSInteger)tag
{
    [_btnYes setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_btnNo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    _btnYes.alpha = 0.4;
    _btnNo.alpha = 0.4;
    
    if (tag == 500) {
        
        [_btnYes setTitleColor:LIGHT_ORANGE_COLOR forState:UIControlStateNormal];
        _btnYes.alpha = 1;
        
        GetDictByName *dictByName = _petVacinated[0];
        _petIsVacIndex = [dictByName.listId intValue];
        
    } else if (tag == 501) {
        
        [_btnNo setTitleColor:LIGHT_ORANGE_COLOR forState:UIControlStateNormal];
        _btnNo.alpha = 1;
        GetDictByName *dictByName = _petVacinated[1];
        _petIsVacIndex = [dictByName.listId intValue];
    }
    [self checkActiveButton:@"1"];
}

//Camera Btutton
- (IBAction)cameraAction:(id)sender {
    
    [self.view endEditing:YES];
    
    if (_selectedMediaArray.count < 4) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Media" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library",@"Take Photo",nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [actionSheet showInView:self.view];
    } else
        [UtilHelper showApplicationAlertWithMessage:@"Sorry! You can not select more than 4 images" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
}


#pragma
#pragma  mark action sheet delegate method

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex==1)
        [self pickMediaFromSource:UIImagePickerControllerSourceTypeCamera];
    else if(buttonIndex==0)
        [self pickMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
}

#pragma
#pragma  mark camera delegate method
- (void)pickMediaFromSource:(UIImagePickerControllerSourceType)sourceType{
    @autoreleasepool {
        if([UIImagePickerController isSourceTypeAvailable:sourceType]) {
            NSArray *mediaTypes=[UIImagePickerController availableMediaTypesForSourceType:sourceType];
            UIImagePickerController *picker=[[UIImagePickerController alloc]init];
            picker.mediaTypes=mediaTypes;
            picker.delegate=self;
            picker.allowsEditing=YES;
            //            picker.allowsEditing=NO;
            picker.sourceType=sourceType;
            [self presentViewController:picker animated:YES completion:NULL];
        } else
            [UtilHelper showApplicationAlertWithMessage:@"Camera is not found" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
    }
}

#pragma mark -
#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)]) {
        @autoreleasepool {
            UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
            AddImageObject *addImageObject = [AddImageObject new];
            addImageObject.image = chosenImage;
            addImageObject.isUploaded = NO;
            
            [_selectedMediaArray addObject:addImageObject];
            [_collectionView reloadData];
            _collectionView.hidden = NO;
            [self updateCameraBox:NO];
            
            [self requestForAddImage:_selectedMediaArray.count-1];
        }
    } else
        [UtilHelper showApplicationAlertWithMessage:@"Video does not support." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)optionsAction:(id)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select Option" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.delegate = self;
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
    picker.delegate = self;
    [picker selectRow:self.selectedGroupIndex inComponent:0 animated:YES];
    [alert addSubview:picker];
    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
    [alert setValue:picker forKey:@"accessoryView"];
    [alert show];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return self.pickerOptionsList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    return [self.pickerOptionsList objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.selectedGroupIndex = row;
    _lblOption.text = _pickerOptionsList[row];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }else {
        
        _lblOption.text = _pickerOptionsList[self.selectedGroupIndex];
        _selectedOption = _selectedGroupIndex +1;
        [self checkActiveButton:@"1"];
    }
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (_tfPetValue == textField) {
        
        NSString *itemValue = [textField.text stringByReplacingCharactersInRange:range withString:string];
        self.lblUptoInsure.text = [NSString stringWithFormat:@"$%@", itemValue];
    }
    
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    [self checkActiveButton:numberOFCharacter];
    
    return YES;
}

#pragma mark UICollectionView Methods
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSInteger count = _selectedMediaArray.count;
    
    if(_selectedMediaArray.count > 0)
    {
        _collectionView.hidden = NO;
    }
    else
    {
        _collectionView.hidden = YES;
    }
    return count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MediaUploadingViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MediaUploadingViewCell" forIndexPath:indexPath];
    
    cell.mediaCellDelegate = self;
    AddImageObject *addImageObject = _selectedMediaArray[indexPath.row];
    [cell prepareUIForRowWithImage:addImageObject index:indexPath.row];
    
    if (addImageObject.image != nil) {
        [cell.activityIndicatorView stopAnimating];
        [cell.activityIndicatorView startAnimating];
    } else {
        //Shows indicatore on imageView
        [cell.imageView setShowActivityIndicatorView:YES];
        [cell.imageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, addImageObject.imageName];
        
        //Loads image
        [cell.imageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
    }
    
    if(addImageObject.isUploaded == NO)
    {
        [cell.activityIndicatorView stopAnimating];
        [cell.activityIndicatorView startAnimating];
        cell.btnCancel.hidden = YES;
    }
    else{
        
        [cell.activityIndicatorView startAnimating];
        [cell.activityIndicatorView stopAnimating];
        cell.btnCancel.hidden = NO;
    }
    return cell;
}

- (void)cancelForCellRow:(NSInteger)row
{
    [self requestForDeleteImage:row];
}

- (void)actionForCellRow:(NSInteger)row
{
    [self requestForDeleteImage:row];
}

#pragma mark
#pragma mark - Text View Delegate Methods

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    UIToolbar *ViewForDoneButtonOnKeyboard = [[UIToolbar alloc] init];
    [ViewForDoneButtonOnKeyboard sizeToFit];
    UIBarButtonItem *btnDoneOnKeyboard = [[UIBarButtonItem alloc] initWithTitle:@"DONE"
                                                                          style:UIBarButtonItemStylePlain target:self
                                                                         action:@selector(doneBtnFromKeyboardClicked:)];
    
    UIBarButtonItem *flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    [ViewForDoneButtonOnKeyboard setItems:[NSArray arrayWithObjects:flex, btnDoneOnKeyboard, nil]];
    textView.inputAccessoryView = ViewForDoneButtonOnKeyboard;
    
    return YES;
}

- (IBAction)doneBtnFromKeyboardClicked:(id)sender
{
    //Hide Keyboard by endEditing or Anything you want.
    [self.view endEditing:YES];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        //[textView resignFirstResponder];
    }
    
    NSString *description = [textView.text stringByReplacingCharactersInRange:range withString:text];
    
    //Store description text below
    [self checkActiveButton:description];
    
    return YES;
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)checkActiveButton:(NSString *)numberOFCharacter
{
    BOOL isActive = NO;
    
    for (JVFloatLabeledTextField *tfText in self.validationArray) {
        
        if (tfText.text.length > 0) {
            
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
            
        } else {
            
            isActive = NO;
            break;
        }
    }
    
    if (_packSizeId == 0 || !_btnTermCondition.selected || _petTypeIndex == 0 || _petIsVacIndex == -1)
    {
        isActive = NO;
    }
    
    if (isActive) {
        
        _btnNext.backgroundColor = LOGIN_ACTIVE_COLOR;
        _btnNext.selected = NO;
        
    }else {
        
        _btnNext.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnNext.selected = YES;
    }
}

- (void)requestToUpdateGeneralInfo
{
    if (_packSizeId == 1) {
        
        if ([_tfLbs.text intValue] > 25 || [_tfLbs.text intValue] < 0) {
            
            [UtilHelper showApplicationAlertWithMessage:@"The lbs value that you have set should be within the size type range you have selected" withDelegate:self withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
    }
    else if (_packSizeId == 2) {
        
        if ([_tfLbs.text intValue] > 50 || [_tfLbs.text intValue] < 26) {
            
            [UtilHelper showApplicationAlertWithMessage:@"The lbs value that you have set should be within the size type range you have selected" withDelegate:self withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
    }
    else if (_packSizeId == 3) {
        
        if ([_tfLbs.text intValue] < 51 || [_tfLbs.text intValue] > 70) {
            
            [UtilHelper showApplicationAlertWithMessage:@"The lbs value that you have set should be within the size type range you have selected" withDelegate:self withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
    }

    [self showProgressHud];
    [WebServicesClient UpdatePack_GenInfo:_packId
                                  PetType:[NSString stringWithFormat:@"%li",(long)_petTypeIndex]
                                 PetCount:_tfNumberOfPets.text
                                 PetISVac:[NSString stringWithFormat:@"%li",(long)_petIsVacIndex]
                                 SendName:_tfSendingName.text
                                   isPack:NO
                               PackSizeId:[NSString stringWithFormat:@"%li",(long)_packSizeId]
                            PackOptionsId:[NSString stringWithFormat:@"%li",(long)_selectedOption]
                               PackWeight:_tfLbs.text
                                ItemValue:_tfPetValue.text
                              Description:_descriptionTextView.text
                             ReceiverName:_tfReceiverName.text
                      ReceiverPhoneNumber:_tfReceiverPhone.text
                              LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                             LogUserAgent:[[UIDevice currentDevice] systemVersion]
                            LogDeviceType:LOG_DEVICE_TYPE
                               SessionKey:[AppDelegate sharedAppDelegate].sessionKey//@"5BD81926-3EBD-4181-BE1A-ED293C10E6BA2017060307400"
                        completionHandler:^(int packId, NSError *error) {
                            
                            [self hideProgressHud];
                            
                            if (!error) {
                                
                                if (self->_draftdata != nil) {
                                    
                                    [[NSNotificationCenter defaultCenter] postNotificationName:SENDER_ACTIVE_LIST_UPDATED object:@{}];
                                    
                                    [[AppDelegate sharedAppDelegate].appController showNewOrderDestinationView:EMPTY_STRING PackPrice:@"0" Send_Cat_ID:@"3" ItemPrice:@"0" SenderData:self->_draftdata];
                                } else {
                                    
                                    [[AppDelegate sharedAppDelegate].appController showNewOrderDestinationView:self->_packId PackPrice:@"0" Send_Cat_ID:@"1" ItemPrice:@"0" SenderData:nil];
                                }
                                
                            } else if (error.code == 3) {
                                
                                [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                
                            } else {
                                
                                [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                            }
                        }];
}

- (void)requestForAddImage:(NSInteger)index
{
    AddImageObject *addImageObject = _selectedMediaArray[index];
    
    NSData* imageData = UIImageJPEGRepresentation(addImageObject.image, 0.5);
    NSString *strBase64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString *stringBase = @"data:image/png;base64,";
    
    NSString *finalBase64 = [stringBase stringByAppendingString:strBase64];
    
    [WebServicesClient AddPackImage:_packId
                    PackImageBase64:finalBase64
                      LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey//@"5BD81926-3EBD-4181-BE1A-ED293C10E6BA2017060307400"
                        LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                       LogUserAgent:[[UIDevice currentDevice] systemVersion]
                      LogDeviceType:LOG_DEVICE_TYPE
                  completionHandler:^(AddImageObject *addImage, NSError *error) {
                      
                      if (!error) {
                          
//                          NSLog(@"Image uploaded successfully!");
                          
                          addImageObject.isUploaded = YES;
                          addImageObject.imageName = addImage.imageName;
                          [_collectionView reloadData];
                          
                      } else if (error.code == 3) {
                          
                          [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                          
                      } else {
                          
//                          NSLog(@"Image upload error");
                          
                      }
                  }];
}

- (void)requestForDeleteImage:(NSInteger)index
{
    AddImageObject *addImageObject = _selectedMediaArray[index];
    
    [WebServicesClient DeletePackImage:_packId
                         PackImageName:addImageObject.imageName
                         LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey//@"5BD81926-3EBD-4181-BE1A-ED293C10E6BA2017060307400"
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                     completionHandler:^(BOOL isDeleted, NSError *error) {
                         
                         if (!error) {
                             
//                             NSLog(@"Image Deleted successfully!");
                             
                             if (_selectedMediaArray.count > 0) {
                                 
                                 [_selectedMediaArray removeObjectAtIndex:index];
                                 [_collectionView reloadData];
                                 
                                 if (_selectedMediaArray.count == 0) {
                                     
                                     _collectionView.hidden = YES;
                                     [self updateCameraBox:YES];
                                 }
                             }
                             
                         } else if (error.code == 3) {
                             
                             [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                             
                         } else {
                             
//                             NSLog(@"Image delete error");
                             
                         }
                     }];
}

- (void)requestForAllImages
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetAllPackImages:_draftdata.dataIdentifier
                                LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                               LogUserAgent:[[UIDevice currentDevice] systemVersion]
                              LogDeviceType:LOG_DEVICE_TYPE
                                 SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                          completionHandler:^(NSMutableArray *deliveryImages, NSError *error) {
                              
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  
                                  if (!error) {
                                      
                                      if (deliveryImages.count > 0) {
                                          
                                          [_selectedMediaArray addObjectsFromArray:deliveryImages];
                                          [self updateCameraBox:NO];
                                          [_collectionView reloadData];
                                          
                                      } else {}
                                  } else {}
                              });
                          }];
    });
}

@end
