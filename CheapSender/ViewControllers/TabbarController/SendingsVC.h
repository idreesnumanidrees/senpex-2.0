//
//  SendingsVC.h
//  CheapSender
//
//  Created by admin on 4/20/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import "GoogleMapHelper.h"

@interface SendingsVC : BaseViewController<CLLocationManagerDelegate, MKMapViewDelegate, GMSMapViewDelegate>
@property (weak, nonatomic) IBOutlet GoogleMapHelper *googlemapView;

@end
