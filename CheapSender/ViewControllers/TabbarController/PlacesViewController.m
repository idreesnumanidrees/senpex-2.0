//
//  PlacesViewController.m
//  CheapSender
//
//  Created by admin on 4/29/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "PlacesViewController.h"

@interface PlacesViewController ()

@end

@implementation PlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self preapreUI];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self placeTextFieldOptionals];
}

#pragma mark -
#pragma mark - Helpers

- (void)preapreUI {
    self.lblHeading.text = self.headingName;
    self.view.backgroundColor = [UIColor clearColor];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
}

//Setup of Optional Properties of MVPlaceSearchTextField
- (void)placeTextFieldOptionals
{
    //Optional Properties of MVPlaceSearchTextField
    _placeSearchTextField.autoCompleteRegularFontName = @"SFUIText-Bold";
    _placeSearchTextField.autoCompleteBoldFontName = @"SFUIText-Regular";
    _placeSearchTextField.autoCompleteTableCornerRadius=0.0;
    _placeSearchTextField.autoCompleteRowHeight=35;
    _placeSearchTextField.autoCompleteTableCellTextColor=[UIColor colorWithWhite:0.131 alpha:1.000];
    _placeSearchTextField.autoCompleteFontSize=14;
    _placeSearchTextField.autoCompleteTableBorderWidth=1.0;
    _placeSearchTextField.showTextFieldDropShadowWhenAutoCompleteTableIsOpen=YES;
    _placeSearchTextField.autoCompleteShouldHideOnSelection=YES;
    _placeSearchTextField.autoCompleteShouldHideClosingKeyboard=YES;
    _placeSearchTextField.autoCompleteShouldSelectOnExactMatchAutomatically = YES;
    _placeSearchTextField.clearButtonMode = UITextFieldViewModeAlways;
    self.placeSearchTextField = (MVPlaceSearchTextField *)[UtilHelper shadowView:(UIView *)self.placeSearchTextField];
    
    //Placeholder Pedding in MVPlaceSearchTextField
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    _placeSearchTextField.leftView = paddingView;
    _placeSearchTextField.leftViewMode = UITextFieldViewModeAlways;
    
    [_placeSearchTextField becomeFirstResponder];
}

#pragma mark -
#pragma mark - IBActions

- (IBAction)closeAction:(id)sender
{
//    [self.navigationController popViewControllerAnimated:NO];
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark
#pragma mark - Place search Textfield Delegates

- (void)placeSearch:(MVPlaceSearchTextField *)textField ResponseForSelectedPlace:(GMSPlace *)responseDict{
    [self.view endEditing:YES];
        //Only allow user to select a place from list if device location is turned on
//    if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
//    {
//        textField.text = EMPTY_STRING;
//        return;
//    }
//    else
//    {
        [textField resignFirstResponder];
        [self.placesDelegate placeMarker:responseDict];
//        [self.navigationController popViewControllerAnimated:NO];
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
//    }
}

- (void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField
{
    
}

- (void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField
{
    //    [self hideETAandRelatedContent];
}

- (void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
    cell.contentView.backgroundColor = [UIColor whiteColor];
}

@end
