//
//  ChatDetailVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/03.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "BHInputToolbar.h"

@interface ChatDetailVC : BaseViewController


@property (weak, nonatomic) IBOutlet BHInputToolbar *inputToolbar;

//Heading outlets
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastSeen;

//Main View outlets
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *tfSendMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnSend;

@property (nonatomic, strong) NSMutableArray *chatMessagesArray;

@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property (nonatomic, strong) PackDetailsModel *packDetails;
@property (nonatomic, strong) CourierConversations *conversation;

@property (nonatomic, strong) NSString *lastMessagesCount;
@property (nonatomic, strong) NSString *currentMessagesCount;

@property (assign, nonatomic) BOOL moreLoadOn;
@property (assign, nonatomic) int startCount;

@property (nonatomic, strong) NSString *recieverID;
@property (nonatomic, strong) NSString *tableHeader;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong)IBOutlet UIView *navigationColor;

//Constraints
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *messageViewBottomConstraint;


@end
