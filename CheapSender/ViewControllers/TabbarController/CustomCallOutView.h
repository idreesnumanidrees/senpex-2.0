//
//  CustomCallOutView.h
//  CheapSender
//
//  Created by Idrees on 2017/06/14.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCallOutView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *ovalImageView;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;

- (void)populateFields:(NSString *)address ovalImageName:(NSString *)ovalImageName;

@end
