//
//  NewOrderVC.m
//  CheapSender
//
//  Created by admin on 4/20/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "NewOrderVC.h"
#import "PackSelection.h"

@interface NewOrderVC ()

//Outlets
@property (strong, nonatomic) IBOutlet UIButton *btnContinue;
@property (weak, nonatomic) IBOutlet UIImageView *closeImage;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UIButton *btnTerms;
@property (strong, nonatomic) IBOutlet UIButton *btnService1;
@property (strong, nonatomic) IBOutlet UILabel *lblService1;
@property (strong, nonatomic) IBOutlet UIButton *btnService2;
@property (strong, nonatomic) IBOutlet UILabel *lblService2;
@property (strong, nonatomic) IBOutlet UIButton *btnService3;
@property (strong, nonatomic) IBOutlet UILabel *lblService3;
@property (strong, nonatomic) IBOutlet UIButton *btnService4;
@property (strong, nonatomic) IBOutlet UILabel *lblService4;
@property (nonatomic, assign) PackSelected packSelected;
@property (weak, nonatomic) IBOutlet UIImageView *imageService1;
@property (weak, nonatomic) IBOutlet UIImageView *imageService2;
@property (weak, nonatomic) IBOutlet UIImageView *imageService3;
@property (weak, nonatomic) IBOutlet UIImageView *imageService4;

@end

@implementation NewOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"PurchaseAndDeliver"];
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self hideProgressHud];
    NSLog(@"view will appear");
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI {
    _btnContinue = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnContinue withRadius:_btnContinue.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    _packSelected = SERVICE1;
    [self buttonSelection:_btnService1 label:_lblService1 tagID:2];
    
    [_btnTerms setImage:[UIImage imageNamed:@"CheckUnsel"] forState:UIControlStateNormal];
    [_btnTerms setImage:[UIImage imageNamed:@"CheckSel"] forState:UIControlStateSelected];
    
    _btnContinue.selected = false;
    _btnTerms.selected = true;
    
    if (_isBackHidden)
    {
        _closeImage.hidden = NO;
        _btnClose.hidden = NO;
        _btnClose.enabled = YES;
    }
}

#pragma mark
#pragma mark --Actions

- (IBAction)continueAction:(id)sender {
    if (![UtilHelper isNetworkAvailable]) {
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    } else if (_btnContinue.selected){
        [UtilHelper showApplicationAlertWithMessage:@"Please accept the rules to continue" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    [self showProgressHud];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MakeFastOrder"]) {
        NSString *send_cat_id = [[NSString alloc]initWithFormat:@"%d", (int)_packSelected];
        [[AppDelegate sharedAppDelegate].appController showNewOrderPackInfoView:@"" Send_Cat_ID:send_cat_id SenderData:nil isBackHidden:YES];
    } else {
//        [self createPackRequest];
        NSString *send_cat_id = [[NSString alloc]initWithFormat:@"%d", (int)_packSelected];
        [[AppDelegate sharedAppDelegate].appController showNewOrderPackInfoView:@"" Send_Cat_ID:send_cat_id SenderData:nil isBackHidden:YES];
    }
//    [self createPackRequest];
    
}

- (IBAction)termsAction:(id)sender {
    _btnTerms.selected = !_btnTerms.selected;
    _btnContinue.selected = !_btnTerms.selected;
    [_btnContinue setBackgroundColor: _btnContinue.selected ? LOGIN_DISABLE_COLOR : LOGIN_ACTIVE_COLOR];
}

- (IBAction)rulesAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showTermsView:@"pack_rule"];
}

- (IBAction)serviceAction:(UIButton *)sender {
    if (sender.tag == 3) {
        [[NSUserDefaults standardUserDefaults] setBool:true forKey:@"PurchaseAndDeliver"];
    } else {
        [[NSUserDefaults standardUserDefaults] setBool:false forKey:@"PurchaseAndDeliver"];
    }
    _packSelected = sender.tag;
    UILabel *label = sender.tag == 2 ? _lblService1 : (sender.tag == 1 ? _lblService2 : (sender.tag == 3 ? _lblService3 : _lblService4));
    UIImageView *image = sender.tag == 2 ? _imageService1 : (sender.tag == 1 ? _imageService2 : (sender.tag == 3 ? _imageService3 : _imageService4));
    [self buttonSelection:sender label:label tagID:sender.tag];
}

- (void)buttonSelection:(UIButton *)selectedButton label:(UILabel *)selectedLabel tagID:(NSInteger) tagid {
    _lblService1.textColor = UIColor.lightGrayColor;
    _lblService2.textColor = UIColor.lightGrayColor;
    _lblService3.textColor = UIColor.lightGrayColor;
    _lblService4.textColor = UIColor.lightGrayColor;
    selectedLabel.textColor = [[UIColor alloc] initWithRed:1 green:104.0/255 blue:0 alpha:1];
    
    [_imageService1 setImage:[UIImage imageNamed:@"Service1_1"]];
    [_imageService2 setImage:[UIImage imageNamed:@"Service2_1"]];
    [_imageService3 setImage:[UIImage imageNamed:@"Service3_1"]];
    [_imageService4 setImage:[UIImage imageNamed:@"Service4_1"]];
    if (tagid == 2) {
        [_imageService1 setImage:[UIImage imageNamed:@"Service1"]];
    } else if (tagid == 1){
        [_imageService2 setImage:[UIImage imageNamed:@"Service2"]];
    } else if (tagid == 3){
        [_imageService3 setImage:[UIImage imageNamed:@"Service3"]];
    } else {
        [_imageService4 setImage:[UIImage imageNamed:@"Service4"]];
    }
    
    
    [self.view layoutIfNeeded];
    
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)createPackRequest {
    NSString *send_cat_id = [[NSString alloc]initWithFormat:@"%d", (int)_packSelected];
    [WebServicesClient CreatePack:send_cat_id
                      LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                     LogUserAgent:[[UIDevice currentDevice] systemVersion]
                    LogDeviceType:LOG_DEVICE_TYPE
                       SessionKey:[AppDelegate sharedAppDelegate].sessionKey//@"5BD81926-3EBD-4181-BE1A-ED293C10E6BA2017060307400"
                completionHandler:^(NSString *packId, NSError *error) {
                    
//                    [self hideProgressHud];
                    
                    if (!error)
                        [[AppDelegate sharedAppDelegate].appController showNewOrderPackInfoView:packId Send_Cat_ID:send_cat_id SenderData:nil isBackHidden:YES];
                    else {
                        if (error.code == 3)
                            [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                        else
                            [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                    }
                }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1001)
        [self senderLogout];
}

@end
