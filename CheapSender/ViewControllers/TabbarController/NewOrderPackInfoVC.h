//
//  NewOrderPackInfoVC.h
//  CheapSender
//
//  Created by admin on 4/22/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "AUIAutoGrowingTextView.h"
#import "GoogleMapHelper.h"
#import "LocationManagerService.h"
#import "MVPlaceSearchTextField.h"


@interface NewOrderPackInfoVC : BaseViewController <GMSMapViewDelegate, LocationManagerServiceDelegate, UINavigationControllerDelegate, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, PlaceSearchTextFieldDelegate>

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBackWhiteIcon;

@property (nonatomic, assign) BOOL isBackHidden;


//GoogleMapHelper instance variable
@property (strong, nonatomic) IBOutlet GoogleMapHelper *googleMapView;
@property (assign, nonatomic) BOOL isMapLoaded;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *mapViewHeight;

//LocationManagerService instance variable
@property (strong, nonatomic) LocationManagerService *locationManagerService;

//NSMutableArray contains all user info
@property (nonatomic, strong) NSMutableArray *markerArray;

@property (strong, nonatomic) IBOutlet UIView *locationView;
@property (strong, nonatomic) IBOutlet UIView *timeView;


@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

//JVFloatLabeledTextField outlets
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfPromoCode;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfItemValue;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField *tfWaitingtime;

@property (weak, nonatomic) IBOutlet UIView *waitingView;
//Size Type outlets
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sizeviewSpace;
@property (weak, nonatomic) IBOutlet UILabel *lblSmallSize;
@property (strong, nonatomic) IBOutlet UIImageView *imgSmallSize;
@property (weak, nonatomic) IBOutlet UILabel *lblMediumSize;
@property (strong, nonatomic) IBOutlet UIImageView *imgMediumSize;
@property (weak, nonatomic) IBOutlet UILabel *lblLargeSize;
@property (strong, nonatomic) IBOutlet UIImageView *imgLargeSize;
@property (weak, nonatomic) IBOutlet UILabel *lblsizeDescription;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *promocodeHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *promoLabelHeight;
@property (weak, nonatomic) IBOutlet UIImageView *promoImage;


//Time Type outlets
@property (strong, nonatomic) IBOutlet UIButton *btnUrgent;
@property (strong, nonatomic) IBOutlet UILabel *lblUrgentPrice;
@property (strong, nonatomic) IBOutlet UILabel *lblUrgentDescription;
@property (strong, nonatomic) IBOutlet UIButton *btn12;
@property (strong, nonatomic) IBOutlet UILabel *lbl12Price;
@property (strong, nonatomic) IBOutlet UILabel *lbl12Description;

//Location outlets
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;
@property (strong, nonatomic) IBOutlet UILabel *pick_upAddressLabel;
@property (strong, nonatomic) IBOutlet UILabel *deliveryAddresslabel;


//Constraint outlets
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *buttonViewSpacing;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *addressTextField;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *addressTextField1;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *addressTextField2;


//Server related properties
@property (strong, nonatomic) NSString *packId;
@property (strong, nonatomic) NSString *packPrice;
@property (nonatomic, assign) NSInteger selectedGroupIndex;
@property (nonatomic, assign) NSInteger selectedOption;
@property (nonatomic, assign) NSString *packSizeId;
@property (nonatomic, strong) NSArray *validationArray;
@property (nonatomic, strong) NSArray *packSizesList;
@property int addressIndex;
@property (strong, nonatomic) SenderData *draftdata;
@property (strong, nonatomic) NSString *send_Cat_Id;
@property (strong, nonatomic) NSString *tariff_id;
@property (strong, nonatomic) NSString *waiting_time;


@end
