//
//  OrderDestinationVC.h
//  CheapSender
//
//  Created by admin on 4/23/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "MediaUploadingViewCell.h"
#import "CSUser.h"
#import "LocationManagerService.h"
#import "AUIAutoGrowingTextView.h"
#import "CalendarHomeViewController.h"
#import <FSCalendar/FSCalendar.h>
#import "CalendarPopup.h"

@interface OrderDestinationVC : BaseViewController <MediaUploadCellDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, CalendarHomeDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UINavigationControllerDelegate, FSCalendarDelegate, FSCalendarDataSource>

//Outlets
@property (strong, nonatomic) IBOutlet UIView *scrollContentView;
@property (weak, nonatomic) IBOutlet UIImageView *iconView;

@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfSending;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfReceiverName;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfReceiverPhone;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfYourName;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfYourSurName;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfYourEmail;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfCellNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnByReceiver;
@property (weak, nonatomic) IBOutlet UIButton *clickbtnByReceiver;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnTakeTime;
@property (strong, nonatomic) IBOutlet AUIAutoGrowingTextView *driverNotesTextView;
@property (strong, nonatomic) IBOutlet UIImageView *downArrow;

@property (strong, nonatomic) IBOutlet UIView *checkView;
@property (strong, nonatomic) IBOutlet UILabel *lblIndicator;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *emailHeightConstraint;

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *descriptionSpacing;
@property (strong, nonatomic) IBOutlet UIView *descriptionView;
@property (weak, nonatomic) IBOutlet UIButton *btnAddPymentMethod;
@property (strong, nonatomic) IBOutlet UIButton *btnPayNow;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *userInfoSpacing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *purchaseItemSpacing;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addPaymentSpacing;

//Insurance
@property (strong, nonatomic) IBOutlet UILabel *lblPrice;
@property (strong, nonatomic) IBOutlet UIView *insuranceView;

@property (weak, nonatomic) IBOutlet UIButton *btnItemPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblItemPrice;

@property (strong, nonatomic) NSString *packageName;
@property (assign, nonatomic) NSString *price;
@property (assign, nonatomic) NSString *packId;
@property (nonatomic, strong) NSArray *validationArray;
@property (strong, nonatomic) SenderData *draftdata;
@property (nonatomic, assign) BOOL isFirstTime;
@property (strong, nonatomic) MediaUploadingViewCell *mediaCell;
@property (strong, nonatomic) NSMutableArray *selectedMediaArray;

@property (strong, nonatomic) NSString *distance;
@property (strong, nonatomic) NSString *distanceTime;
@property (strong, nonatomic) CSUser * from_user;
@property (strong, nonatomic) CSUser * to_user;
@property (assign, nonatomic) NSString *pack_size_id;
@property (assign, nonatomic) NSString *promo_code;
@property (assign, nonatomic) NSString *item_value;
@property (assign, nonatomic) NSString *send_cat_id;
@property (assign, nonatomic) NSString *waiting_time;
@property (assign, nonatomic) NSString *default_tariff_plan;


//New work (28-07-2019)
@property (weak, nonatomic) IBOutlet UILabel *lblSendingLine;
@property (weak, nonatomic) IBOutlet UILabel *lblReceiverNameLine;
@property (weak, nonatomic) IBOutlet UILabel *lblReceiverPhoneLine;
@property (weak, nonatomic) IBOutlet UILabel *lblYourFirstNameLine;
@property (weak, nonatomic) IBOutlet UILabel *lblYourLastNameLine;
@property (weak, nonatomic) IBOutlet UILabel *lblYourEmailLine;
@property (weak, nonatomic) IBOutlet UILabel *lblYourCellPhoneLine;
@property (weak, nonatomic) IBOutlet UILabel *lblPaybyReceiverLine;

@end
