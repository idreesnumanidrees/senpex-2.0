//
//  CalendarPopup.h
//  CheapSender
//
//  Created by Admin on 12/04/2018.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FSCalendar/FSCalendar.h>

//@protocol CalendarPopupDelegate < NSObject >
//
//-(void)okAction;
//-(void)cancelAction;
//
//@end

@interface CalendarPopup : UIView

//@property (weak, nonatomic) IBOutlet JTAppleCalendarView *calendarView;

//@property (nonatomic, assign) id <CalendarPopupDelegate> calendarPopupDelegate;

@property (weak, nonatomic) IBOutlet UIButton *btnUrgent;
@property (weak, nonatomic) IBOutlet FSCalendar *calendar;
@property (weak, nonatomic) IBOutlet UIButton *btnAMPM;
@property (weak, nonatomic) IBOutlet UIButton *btnHour;
@property (weak, nonatomic) IBOutlet UIButton *btnMin;


@end
