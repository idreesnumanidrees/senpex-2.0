//
//  ChatDetailVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/03.
//  Copyright © 2017 CheapSender. All rights reserved.
//
#import "ChatDetailVC.h"
#import "ChatMessage.h"
#import "MyChatTableCell.h"
#import "OtherChatCell.h"

#define VERTICAL_SPACE_BETWEEN_CHAT_CELL 20
#define CHAT_MESSAGE_FONT_SIZE 13.0
#define CHAT_MESSAGE_FONT_TYPE @"SFUIText-Regular"
#define LABEL_WIDTH 180.0f

@interface ChatDetailVC () <BHInputToolbarDelegate>
{
    CGFloat keyBoardHeight;
    CGFloat variation;
}
@end

@implementation ChatDetailVC
@synthesize inputToolbar;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareUI];
    inputToolbar.inputDelegate = self;
    inputToolbar.textView.placeholder = @"Send Message";
    [self.inputToolbar.textView setMaximumNumberOfLines:4];

    [[IQKeyboardManager sharedManager] setEnable:NO];
    
    
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 60; //Set this to any value that works for you.
    
    [[[AppDelegate sharedAppDelegate].appController.senderTabbar.tabBar.items objectAtIndex:1] setBadgeValue:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)myNotificationMethod:(NSNotification*)notification
{
}

- (void)keyboardWillShow:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];

    if (keyBoardHeight == 0) {
        keyBoardHeight = keyboardFrameBeginRect.size.height;
        variation = keyboardFrameBeginRect.size.height;

    } else {
        variation = (keyboardFrameBeginRect.size.height - keyBoardHeight);
        keyBoardHeight = keyboardFrameBeginRect.size.height;

    }

    if (IS_IPHONE_X) {
        variation = variation - 33;
    } else if (IS_IPHONE_6P){
    }
    _messageViewBottomConstraint.constant +=  variation;
    
    [UIView animateWithDuration:1.0 animations:^{
        
        //        NSLog(@"keyboard height %f",variation);
        [self.view layoutIfNeeded];
        //        NSLog(@"messageViewBottomConstraint constant %f",_messageViewBottomConstraint.constant);
        [self scrollToBottomOfTable];
        
    } completion:^(BOOL completed){}];
}
- (void)keyboardWillHide:(NSNotification*)notification
{
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;

    keyBoardHeight = 0;
    [UIView animateWithDuration:1.0 animations:^{
        
        _messageViewBottomConstraint.constant =  0;;//300.0;
        
    } completion:^(BOOL completed){
        [self scrollToBottomOfTable];
    }];
}


- (void)viewWillAppear:(BOOL)animated
{

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myNotificationMethod:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    

    //Add observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(viewChatMessage:)
                                                 name:VIEW_CHAT_MESSAGE object:nil];
    [AppDelegate sharedAppDelegate].isNeedToChatDetails = 1;
}



- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [AppDelegate sharedAppDelegate].isNeedToChatDetails = 0;
}

- (void)viewChatMessage:(NSNotification *) notification
{
    [[[AppDelegate sharedAppDelegate].appController.senderTabbar.tabBar.items objectAtIndex:1] setBadgeValue:nil];
    
    [self getMessageCount];
}

#pragma mark --
#pragma mark -- Helpers
- (void)prepareUI
{
    
    NSInteger userInfo = [[UtilHelper getValueFromNSUserDefaultForKey:LOGON_USER] intValue];

    if (userInfo == 1) {
        
        //_navigationColor.backgroundColor = LIGHT_ORANGE_COLOR;
    }
    else
    {
        _navigationColor.backgroundColor = LOGIN_ACTIVE_COLOR;
    }
    
    UIView *leftPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    _tfSendMessage.leftView = leftPadding;
    _tfSendMessage.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *rightPadding = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    _tfSendMessage.rightView = rightPadding;
    _tfSendMessage.rightViewMode = UITextFieldViewModeAlways;
    
    self.chatMessagesArray = [NSMutableArray new];
    
    [self.btnSend setAttributedTitle:[self setFontWithSize:25 withWebFontText:SEND_TEXT_IMAGE_HEX]
                            forState:UIControlStateNormal];
    self.btnSend.titleLabel.textColor = SEND_MESSAGE_COLOR;
    
    self.profileImageView = (UIImageView *)[UtilHelper makeCornerCurved:self.profileImageView withRadius:18.0 color:nil width:0];
    
    self.tfSendMessage = (UITextField *)[UtilHelper makeCornerCurved:self.tfSendMessage withRadius:15.0 color:LOGIN_DISABLE_COLOR width:1];
    
    //Check for PackDetails
    if (_packDetails != nil) {
        
        _lblUserName.text = _packDetails.acceptedCourierName;
        _recieverID = _packDetails.acceptedCourierId;
        [self loadSenderProfileImage:_packDetails.acceptedCourierSelfImg];
        
    } else {
        
        _lblUserName.text = _conversation.viewName;
        _recieverID = _conversation.viewId;
        [self loadSenderProfileImage:_conversation.viewImg];
    }
    
//    _currentMessagesCount = @"0";
    [self requestToLoginUserInfo];
    
    _startCount = 0;
    [self showProgressHud];
    [self fetchMessageList:_startCount count:@"15" isPush:NO isScrollToBottom:YES];
}

- (void)loadSenderProfileImage:(NSString *)selfImage
{
    //Shows indicatore on imageView
    [self.profileImageView setShowActivityIndicatorView:YES];
    [self.profileImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, selfImage];
    
    //Loads image
    [self.profileImageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
}

- (void)scrollToBottomOfTable
{
    if(self.chatMessagesArray.count>0)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatMessagesArray.count-1 inSection:0];
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

#pragma mark --
#pragma mark -- IBActions

-(void)inputButtonPressed:(NSString *)inputText;
{
    /* Called when toolbar button is pressed */
    NSLog(@"Pressed button with text: '%@'", inputText);
    
    if ([inputText length] > 0) {
        
        NSString *senderId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
        ChatListModel *chatMessage = [ChatListModel new];
        chatMessage.chatText = inputText;
        chatMessage.senderId = senderId;
        NSDate *date = [NSDate date];
        chatMessage.insertedDate = [date getDateTimeInUTC];
        
        
        [self requestToSendMessage:chatMessage.chatText];
        
        [self.chatMessagesArray addObject:chatMessage];
        
        self.tfSendMessage.text = @"";
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatMessagesArray.count-1 inSection:0];
        
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                              withRowAnimation:UITableViewRowAnimationFade];
        
        [self.tableView reloadData];
        [self scrollToBottomOfTable];

    }
}

- (IBAction)sendAction:(id)sender {
    
    if ([self.tfSendMessage.text length] > 0) {
        
        NSString *senderId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
        ChatListModel *chatMessage = [ChatListModel new];
        chatMessage.chatText = self.tfSendMessage.text;
        chatMessage.senderId = senderId;
        NSDate *date = [NSDate date];
        chatMessage.insertedDate = [date getDateTimeInUTC];

        
        [self requestToSendMessage:chatMessage.chatText];
        
        [self.chatMessagesArray addObject:chatMessage];
        
        self.tfSendMessage.text = @"";
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatMessagesArray.count-1 inSection:0];
        
        [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                              withRowAnimation:UITableViewRowAnimationFade];
        
        [self.tableView reloadData];
        [self scrollToBottomOfTable];
        
    }
}

- (IBAction)showSendingDetailAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showSendingDetailView:nil PackId:@"" FromReceiver:false];
}

- (IBAction)showCourierProfileAction:(id)sender
{
    [[AppDelegate sharedAppDelegate].appController showCourierProfileView:_packDetails CourierId:_conversation isComingFrom:YES];
}

#pragma mark --
#pragma mark -- UITextField Delegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
   
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
  
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.view endEditing:YES];
    [self scrollToBottomOfTable];

    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

#pragma mark -
#pragma mark Table view delegate methods
#pragma mark ---------------------------

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *sectionName;
    switch (section)
    {
        case 0:
            sectionName = _tableHeader;
            break;
        default:
            sectionName = @"";
            break;
    }
    return sectionName;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 16, tableView.frame.size.width, 18)];
    [label setFont:[UIFont fontWithName:@"SFUIText-Regular" size:14]];
    
    label.textAlignment = NSTextAlignmentCenter;
    
    NSString *string = _tableHeader;
    
    /* Section header is in 0th index... */
    [label setText:string];
    
    label.textColor = [UIColor colorWithRed:137/255.0 green:137/255.0 blue:137/255.0 alpha:1.0];
    
    [view addSubview:label];
    [view setBackgroundColor:[UIColor clearColor]]; //your background color...
    
    _headerView = view;
    
    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    _startCount = (int)self.chatMessagesArray.count;
    return self.chatMessagesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ChatListModel *chatMsg = self.chatMessagesArray[indexPath.row];
    NSString *senderId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
    
    NSDate *headerDate = [NSDate dateFromDateString:chatMsg.insertedDate];
   //commented by Riyas
    // _tableHeader = [UtilHelper dateTime:headerDate];
    
    UITableViewCell *tableViewCell = nil;
    static NSString *cellId = @"MyChatTableCell";
    
    
    if ([senderId isEqualToString:chatMsg.senderId]) {
        
        MyChatTableCell *myChatCell = (MyChatTableCell*)[tableView dequeueReusableCellWithIdentifier:cellId];
        
        NSArray* arrayOfNibs = [[NSBundle mainBundle] loadNibNamed:@"MyChatTableCell" owner:self options:nil];
        myChatCell = [arrayOfNibs objectAtIndex:0];
        tableViewCell = myChatCell;
        
        [myChatCell prepareChatUI:chatMsg];
        
        [myChatCell.contentView setNeedsLayout];
        [myChatCell.contentView layoutIfNeeded];
        
    } else {
        
        cellId = @"OtherChatCell";
        
        OtherChatCell *otherChatCell = (OtherChatCell*)[tableView dequeueReusableCellWithIdentifier:cellId];
        
        NSArray* arrayOfNibs = [[NSBundle mainBundle] loadNibNamed:@"OtherChatCell" owner:self options:nil];
        otherChatCell = [arrayOfNibs objectAtIndex:0];
        tableViewCell = otherChatCell;
        
        [otherChatCell prepareChatUI:chatMsg];
        
        [otherChatCell.contentView setNeedsLayout];
        [otherChatCell.contentView layoutIfNeeded];
    }
    
    tableViewCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return tableViewCell;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    _headerView.hidden = NO;
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY < 0) {
        
        if (_moreLoadOn) {
            
            _moreLoadOn = NO;
            [self fetchMessageList:_startCount count:@"15" isPush:NO isScrollToBottom:NO];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _headerView.hidden = YES;
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
    }
}

- (NSMutableArray *)getLatestMessages:(NSArray *)currentMessageArray
{
    NSMutableArray *reversedArray = [NSMutableArray new];
    
    for (ChatListModel *listModel in currentMessageArray) {
        
        BOOL isFound = YES;
        
        for (ChatListModel *subListModel in _chatMessagesArray) {
            
            isFound = NO;
            if ([listModel.messageId isEqualToString:subListModel.messageId]) {
                
                isFound = YES;
                break;
            }
        }
        
        if (!isFound) {
            
            [reversedArray addObject:listModel];
        }
    }
    return reversedArray;
}


#pragma mark--
#pragma mark-- Server Side handling

- (void)requestToSendMessage:(NSString *)message
{
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient SendChatMessage:_recieverID
                                  ChatText:message
                                   PushNot:@"1"
                               LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                              LogUserAgent:[[UIDevice currentDevice] systemVersion]
                             LogDeviceType:LOG_DEVICE_TYPE
                                SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                         completionHandler:^(BOOL isSent, NSError *error) {
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 
                                 if (!error) {
                                     
                                     [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_CONVERSATION_LIST object:@{}];
                                     
                                     //DB operations for last message status
                                     LastChatMessageDB *lastMessageDB = [LastChatMessageDB getLastMessage:_recieverID];
                                     
                                     if (lastMessageDB != nil) {
                                         
                                         [LastChatMessageDB insertAndupdateLastMessageWithId:_recieverID message:message IsUpdate:YES];
                                         
                                     } else {
                                         
                                         [LastChatMessageDB insertAndupdateLastMessageWithId:_recieverID message:message IsUpdate:NO];
                                     }
                                     
                                 } else {
                                     
                                     if (error.code == 3) {
                                         
                                         [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                     }
                                 }
                             });
                             
                         }];
    });
}

- (void)fetchMessageList:(int)start count:(NSString *)count isPush:(BOOL)isPush isScrollToBottom:(BOOL)isScrollToBottom
{
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetMessageList:_recieverID
                                    Count:count
                                    Start:[NSString stringWithFormat:@"%d", start]
                              LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                             LogUserAgent:[[UIDevice currentDevice] systemVersion]
                            LogDeviceType:LOG_DEVICE_TYPE
                               SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                        completionHandler:^(NSMutableArray *messagesList, NSString *lastCount, NSError *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                [self hideProgressHud];
                                if (!error) {
                                    
                                    if (messagesList.count > 0) {
                                        
                                        _currentMessagesCount = lastCount;
                                        _moreLoadOn = YES;
                                        NSArray* reversedArray = [[messagesList reverseObjectEnumerator] allObjects];
                                        NSMutableArray *serverRatings = [NSMutableArray new];
                                        
                                        if (isPush) {
                                            
                                            [serverRatings addObjectsFromArray:_chatMessagesArray];
                                            [serverRatings addObjectsFromArray:[self getLatestMessages:reversedArray]];
                                            _chatMessagesArray = [serverRatings mutableCopy];
                                            [_tableView reloadData];
                                            [self scrollToBottomOfTable];
                                            
                                        } else {
                                            
                                            [serverRatings addObjectsFromArray:reversedArray];
                                            [serverRatings addObjectsFromArray:_chatMessagesArray];
                                            _chatMessagesArray = [serverRatings mutableCopy];
                                            [_tableView reloadData];
                                            
                                            if (isScrollToBottom) {
                                                
                                                [self scrollToBottomOfTable];
                                            }
                                        }
                                        
                                        //DB operations for last message status
                                        ChatListModel *chatMessage = [_chatMessagesArray lastObject];
                                        LastChatMessageDB *lastMessageDB = [LastChatMessageDB getLastMessage:_recieverID];
                                        
                                        if (lastMessageDB != nil) {
                                            
                                            [LastChatMessageDB insertAndupdateLastMessageWithId:_recieverID message:chatMessage.chatText IsUpdate:YES];
                                            
                                        } else {
                                            
                                            [LastChatMessageDB insertAndupdateLastMessageWithId:_recieverID message:chatMessage.chatText IsUpdate:NO];
                                        }
                                    }
                                    
                                } else {
                                    
                                    if (error.code == 3) {
                                        
                                        [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                    }
                                }
                            });
                        }];
    });
}

- (void)getMessageCount
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetMessageCount:_recieverID
                               LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                              LogUserAgent:[[UIDevice currentDevice] systemVersion]
                             LogDeviceType:LOG_DEVICE_TYPE
                                SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                         completionHandler:^(NSString *lastCount, NSError *error) {
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 
                                 if (!error) {
                                     
                                     _lastMessagesCount = lastCount;
                                     
                                     int countForMessageList = [_lastMessagesCount intValue] -  [_currentMessagesCount intValue];
                                     
                                     [self fetchMessageList:0 count:[NSString stringWithFormat:@"%d", countForMessageList] isPush:YES isScrollToBottom:YES];
                                     
                                 } else {
                                     
                                     if (error.code == 3) {
                                         
                                         [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                     }
                                 }
                             });
                         }];
    });
}


- (void)requestToLoginUserInfo
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetUserInfoById:_recieverID
                             LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                               LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                              LogUserAgent:[[UIDevice currentDevice] systemVersion]
                             LogDeviceType:LOG_DEVICE_TYPE
                         completionHandler:^(UserModel *user, NSError *error) {
                             
                             dispatch_async(dispatch_get_main_queue(), ^{
                                 
                                 if (!error) {
                                     
                                     _lblUserName.text = [NSString stringWithFormat:@"%@", user.name];
                                     [self loadSenderProfileImage:user.selfImg];
                                 }
                                 else if (error.code == 3) {
                                     
                                     [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                     
                                 } else {
                                     
                                     [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                                 }
                             });
                             
                         }];
    });
}

@end
