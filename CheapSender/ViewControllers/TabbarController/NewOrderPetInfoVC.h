//
//  NewOrderPetInfoVC.h
//  CheapSender
//
//  Created by admin on 4/22/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "AUIAutoGrowingTextView.h"
#import "MediaUploadingViewCell.h"

@interface NewOrderPetInfoVC : BaseViewController<MediaUploadCellDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIButton *btnCheckBox;
@property (strong, nonatomic) IBOutlet UILabel *lblUptoInsure;

//Size Type
@property (weak, nonatomic) IBOutlet UILabel *lblSmallSize;
@property (weak, nonatomic) IBOutlet UIButton *btnSmallSize;
@property (weak, nonatomic) IBOutlet UILabel *lblMediumSize;
@property (weak, nonatomic) IBOutlet UIButton *btnMediumSize;
@property (weak, nonatomic) IBOutlet UILabel *lblLargeSize;
@property (weak, nonatomic) IBOutlet UIButton *btnLargeSize;
@property (weak, nonatomic) IBOutlet UILabel *lblSizeDescription;

@property (weak, nonatomic) IBOutlet UIButton *btnTermCondition;

//JVFloatLabeledTextField outlets
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfSendingName;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfLbs;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfPetValue;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfReceiverName;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfReceiverPhone;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfDescription;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *tfNumberOfPets;

@property (weak, nonatomic) IBOutlet AUIAutoGrowingTextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLabel;

//Camera
@property (weak, nonatomic) IBOutlet UIButton *btnCamera;
@property (weak, nonatomic) IBOutlet UILabel *lblCameraBottom;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) MediaUploadingViewCell *mediaCell;
@property (strong, nonatomic) NSMutableArray *selectedMediaArray;
@property (weak, nonatomic) IBOutlet UIView *cameraBoxView;
@property (weak, nonatomic) IBOutlet UILabel *lblAddPic;
@property (nonatomic, assign) BOOL isFirstTime;

//Pet Type outlets
@property (weak, nonatomic) IBOutlet UIScrollView *PetTypeScrollView;
@property (nonatomic, strong) NSArray *petTypeList;
@property (nonatomic, strong) NSArray *packSizesList;
@property (nonatomic, strong) NSArray *petVacinated;


//Options selection
@property (weak, nonatomic) IBOutlet UILabel *lblOption;
@property (nonatomic, assign) NSInteger selectedGroupIndex;
@property (nonatomic, strong) NSArray *pickerOptionsList;

//Description TextView outlet
@property (strong, nonatomic) IBOutlet AUIAutoGrowingTextView *tvDescription;

//Vacinations outlets
@property (strong, nonatomic) IBOutlet UIButton *btnYes;
@property (strong, nonatomic) IBOutlet UIButton *btnNo;

//Constraint outlets
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cameraTrailingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cameraWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sizeTypeViewHeightConstraint;

//Server related properties
@property (strong, nonatomic) NSString *packId;
@property (nonatomic, assign) NSInteger petTypeIndex;
@property (nonatomic, assign) NSInteger petIsVacIndex;
@property (nonatomic, assign) NSInteger selectedOption;
@property (nonatomic, assign) NSInteger packSizeId;

//Form Validation
@property (nonatomic, assign) BOOL isSendingNameFilled;
@property (nonatomic, assign) BOOL isPetTypeFilled;
@property (nonatomic, assign) BOOL isPetCountFilled;
@property (nonatomic, assign) BOOL isPetIsVacFilled;
@property (nonatomic, assign) BOOL isPackSizeIdFilled;
@property (nonatomic, assign) BOOL isPackOptionIdFilled;
@property (nonatomic, assign) BOOL isPackWeightFilled;
@property (nonatomic, assign) BOOL isPackItemValueFilled;
@property (nonatomic, assign) BOOL isPackDescriptionFilled;
@property (nonatomic, assign) BOOL isPackReceiverNameFilled;
@property (nonatomic, assign) BOOL isPackReceiverNumberFilled;

@property (nonatomic, strong) NSArray *validationArray;

@property (strong, nonatomic) SenderData *draftdata;



@end
