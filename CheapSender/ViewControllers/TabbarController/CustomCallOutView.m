//
//  CustomCallOutView.m
//  CheapSender
//
//  Created by Idrees on 2017/06/14.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CustomCallOutView.h"

@implementation CustomCallOutView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    
    self.bgImageView.layer.cornerRadius = 10;
    self.bgImageView.layer.masksToBounds = YES;
}

- (void)populateFields:(NSString *)address ovalImageName:(UIImage *)ovalImageName
{
   
    self.addressLabel.text = address;
    self.ovalImageView.image = [UIImage imageNamed:ovalImageName];
}



@end
