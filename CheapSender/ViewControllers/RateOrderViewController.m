//
//  RateOrderViewController.m
//  CheapSender
//
//  Created by Ambika Vijay on 4/28/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "RateOrderViewController.h"
#import "HCSStarRatingView.h"
#import "AUIAutoGrowingTextView.h"

@interface RateOrderViewController ()

@property (strong, nonatomic) IBOutlet HCSStarRatingView *MarkRateView;
@property (strong, nonatomic) IBOutlet HCSStarRatingView *showRateView;
@property (strong, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UIButton *givetipBtn;
@property (weak, nonatomic) IBOutlet UIImageView *courierProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *courierNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *rateCountLabel;
@property (weak, nonatomic) IBOutlet AUIAutoGrowingTextView *rateTextView;
@property (weak, nonatomic) IBOutlet UIButton *btnTip1;
@property (weak, nonatomic) IBOutlet UIButton *btnTip2;
@property (weak, nonatomic) IBOutlet UIButton *btnTip3;
@property (weak, nonatomic) IBOutlet UIButton *btnTip4;

@property (strong, nonatomic) NSString * tipPrice;

@end

@implementation RateOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareUI
{
    //_rateTextView.hidden = NO;
    
    _courierNameLabel.text = _packDetails.acceptedCourierName;
    _tipPrice = @"0";
    _rateCountLabel.text = [NSString stringWithFormat:@"(%@)", _packDetails.acceptedCourierRate];
    [_givetipBtn setBackgroundColor:LOGIN_DISABLE_COLOR];
    _showRateView.value = [_packDetails.acceptedCourierRate floatValue];
    [self loadSenderProfileImage:_packDetails.acceptedCourierSelfImg];
    
    NSMutableArray *AllDict = [AppDelegate sharedAppDelegate].allDictData;
    int tipID = 0;
    for (int i = 0; i < AllDict.count; i ++) {
        GetDictByName *val = [AllDict objectAtIndex:i];
        if ([val.dicId  isEqual: @"28"]) {
            tipID = i;
            break;
        }
    }
    GetDictByName *tipValue = [AllDict objectAtIndex:tipID];
    [_btnTip1 setTitle:[NSString stringWithFormat:@"%s%@","$", tipValue.listId] forState:UIControlStateNormal];
    tipValue = [AllDict objectAtIndex:tipID+1];
    [_btnTip2 setTitle:[NSString stringWithFormat:@"%s%@","$", tipValue.listId] forState:UIControlStateNormal];
    tipValue = [AllDict objectAtIndex:tipID+2];
    [_btnTip3 setTitle:[NSString stringWithFormat:@"%s%@","$", tipValue.listId] forState:UIControlStateNormal];
    tipValue = [AllDict objectAtIndex:tipID+3];
    [_btnTip4 setTitle:[NSString stringWithFormat:@"%s%@","$", tipValue.listId] forState:UIControlStateNormal];
}

- (void)loadSenderProfileImage:(NSString *)selfImage
{
    //Shows indicatore on imageView
    [_courierProfileImageView setShowActivityIndicatorView:YES];
    [_courierProfileImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, selfImage];
    
    //Loads image
    [_courierProfileImageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
}

#pragma mark--
#pragma mark-- IBActions

- (IBAction)starRateAction:(id)sender {
    
    [self enableSendButton];
    
}
- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true ];
}

- (IBAction)rateUserAction:(id)sender {
    
    [self requestToRateUser];
}
- (IBAction)addtipAction:(UIButton *)sender {
    [self requestGiveTip];
}

- (IBAction)tipAction:(UIButton *)sender {
    _btnTip1.selected = false;
    _btnTip2.selected = false;
    _btnTip3.selected = false;
    _btnTip4.selected = false;
    [self enableAddtipButton];
    
    if (sender.tag == 1) {
        _btnTip1.selected = true;
        _tipPrice = [_btnTip1.titleLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    } else if (sender.tag == 2){
        _btnTip2.selected = true;
        _tipPrice = [_btnTip2.titleLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    } else if (sender.tag == 3){
        _btnTip3.selected = true;
        _tipPrice = [_btnTip3.titleLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    } else {
        _btnTip4.selected = true;
        _tipPrice = [_btnTip4.titleLabel.text stringByReplacingOccurrencesOfString:@"$" withString:@""];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView{

}

- (void)enableSendButton {
    
    UIColor *sendBtnEnabledColor = [[UIColor alloc]initWithRed:53/255 green:66/255 blue:88/255 alpha:1];
    
    [_sendBtn setBackgroundColor:sendBtnEnabledColor];
    _sendBtn.enabled = true;
}

- (void)enableAddtipButton {
    
    UIColor *sendBtnEnabledColor = [[UIColor alloc]initWithRed:53/255 green:66/255 blue:88/255 alpha:1];
    
    [_givetipBtn setBackgroundColor:OFFLINE_BUTTON_SELECTION];
    _givetipBtn.enabled = true;
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestToRateUser
{
    [self showProgressHud];
    [WebServicesClient RateUser:_packDetails.acceptedCourierId
                        RateVal:[NSString stringWithFormat:@"%f", _MarkRateView.value]
                  logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                    LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                   LogUserAgent:[[UIDevice currentDevice] systemVersion]
                  LogDeviceType:LOG_DEVICE_TYPE
              completionHandler:^(BOOL isRated, NSError *error) {
                  [self hideProgressHud];
                  
                  if (!error) {
                      
                      [self requestToAddCourierReview];
//                      [self requestGiveTip];
                      //[UtilHelper showApplicationAlertWithMessage:@"Rated successfully!" withDelegate:self withTag:2 otherButtonTitles:@[@"Ok"]];
                      
                  } else if (error.code == 3) {
                      [self hideProgressHud];
                      [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                      
                  } else {
//                      [self requestToAddCourierReview];
//                      [self requestGiveTip];
                      [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:2 otherButtonTitles:@[@"Ok"]];
                  }
              }];
}

- (void)requestGiveTip
{
    [self showProgressHud];
    [WebServicesClient GiveTip:_tipPrice
                       Pack_Id:_packDetails.internalBaseClassIdentifier
                    Courier_Id:_packDetails.acceptedCourierId
                 logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                   LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                  LogUserAgent:[[UIDevice currentDevice] systemVersion]
                 LogDeviceType:LOG_DEVICE_TYPE
             completionHandler:^(BOOL isSent, NSError *error) {
                  
                  if (!error) {
                      
                      [UtilHelper showApplicationAlertWithMessage:@"Tip successfully added!" withDelegate:self withTag:5 otherButtonTitles:@[@"Ok"]];
                      
                  } else if (error.code == 3) {
                      [self hideProgressHud];
                      [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                  } else {
                      [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                  }
                 [self hideProgressHud];
              }];
}

- (void)requestToAddCourierReview
{
    //[self showProgressHud];
    [WebServicesClient AddCourierReview:_packDetails.acceptedCourierId
                             reviewText:_rateTextView.text
                          logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                            LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                           LogUserAgent:[[UIDevice currentDevice] systemVersion]
                          LogDeviceType:LOG_DEVICE_TYPE
                      completionHandler:^(BOOL isRated, NSError *error) {
                  
                  [self hideProgressHud];
                  
                  if (!error) {
                      
                      [UtilHelper showApplicationAlertWithMessage:@"Thanks for your feedback" withDelegate:self withTag:2 otherButtonTitles:@[@"Ok"]];
                      
                  } else if (error.code == 3) {
                      
                      [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                      
                  } else {
                      
                      [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                  }
              }];
}


#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 3)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else if(alertView.tag == 5){
        [self.navigationController popViewControllerAnimated:YES ];
    }
    else if(alertView.tag == 1001)
    {
        [self senderLogout];
        
    }
}



@end
