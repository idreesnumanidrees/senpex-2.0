//
//  TermsViewController.m
//  CheapSender
//
//  Created by Riyas Abdul Rahman on 5/1/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "TermsViewController.h"
#import "TermsRulesTableCell.h"

@interface TermsViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *rules;
@property (strong, nonatomic) NSMutableDictionary *heightAtIndexPath;

@property (weak, nonatomic) IBOutlet UIView *topBarNavigation;

@end

@implementation TermsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _rules.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TermsRulesTableCell";
    TermsRulesTableCell *cell = (TermsRulesTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSDictionary *ruleDict = _rules[indexPath.row];
    
    cell.headingLabel.text = ruleDict[@"header_text"];
    cell.bodyLabel.text = ruleDict[@"body_text"];
    
//    if (indexPath.row == 0) {
//        
//        cell.headingLabel.text = @"djfbds fdsjf jdshf dsjhf dshf dshf jdshf dfh";
//        cell.bodyLabel.text = @"djfbds fdsjf jdshf dsjhf dshf dshf jdshf dfh sdjaskf sajfh asjhf ashf ashf sahgf ahsgf ahsgf ashgf ashgf asghf asghf ahsgf ahsgf ashgf ashgf ahsgf aghsfagfg ssaf ";
//
//    } else {
//        
//        cell.headingLabel.text = @"djfbds fdsjf jdshf dsjhf ";
//        cell.bodyLabel.text = @"djfbds fdsjf jdshf dsjhf dshf dshf jdshf dfh sdjaskf sajfh asjhf ashf ashf sahgf ahsgf ahsgf ashgf ashgf asghf asghf ahsgf ahsgf ashgf ashgf ahsgf aghsfagfg ssaf dsfmnafas fjas fh asfga sgf saghf ahsgf ashgf sagfahsgfhsagkf kasgfahksgfhsakfg ashfg sahfgsa fgashfg sahf gashfg ashkfg askhgf sakgf askgf sagfkasghf asghf asgfks ahksgf sagkf asfasgaskfhg";
//    }
    
    
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *height = [self.heightAtIndexPath objectForKey:indexPath];
    
    if(height) {
        return height.floatValue;
    } else {
        return UITableViewAutomaticDimension;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSNumber *height = @(cell.frame.size.height);
    [self.heightAtIndexPath setObject:height forKey:indexPath];
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

- (void)prepareUI
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;

    if([_ruleType isEqualToString:@"courier_rule"])
    {
        _topBarNavigation.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
    }
    self.heightAtIndexPath = [NSMutableDictionary new];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    if ([AppDelegate sharedAppDelegate].sessionKey == nil)
    {
        [AppDelegate sharedAppDelegate].sessionKey = EMPTY_STRING;
    }
    
    [self showProgressHud];
    [WebServicesClient GetAcceptRule:_ruleType
                         LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                        LogUserAgent:[[UIDevice currentDevice] systemVersion]
                       LogDeviceType:LOG_DEVICE_TYPE
                          SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                   completionHandler:^(NSMutableArray *rulesArray, NSError *error) {
                       
                       [self hideProgressHud];

                       if (!error) {
                           
                           if (rulesArray.count > 0) {
                               
                               _rules = rulesArray;
                               [self.tableView reloadData];
                           }
                           
                       } else if (error.code == 3) {
                           
                           [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                           
                       } else {
                           
                           [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                       }
                   }];
}

@end
