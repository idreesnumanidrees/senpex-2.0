//
//  SendingDetailViewController.h
//  CheapSender
//
//  Created by Riyas Abdul Rahman on 4/30/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import "GoogleMapHelper.h"
#import "HCSStarRatingView.h"
#import "AUIAutoGrowingTextView.h"
#import "MLPAutoCompleteTextField.h"


@interface SendingDetailViewController : BaseViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, assign) BOOL isOrderWaiting;
@property (nonatomic, assign) BOOL fromReceiver;
@property (assign, nonatomic) NSString *packId;
@property (strong, nonatomic) PackDetailsModel *packDetails;
@property (strong, nonatomic) SenderData *senderData;

@property (nonatomic, assign) CGFloat heightForDesc;

//Outlets
@property (weak, nonatomic) IBOutlet UIImageView *icon;

@property (strong, nonatomic) IBOutlet UIButton *btnOverView;
@property (strong, nonatomic) IBOutlet UIButton *btnStatus;

@property (strong, nonatomic) IBOutlet GoogleMapHelper *googleMapView;

@property (weak, nonatomic) IBOutlet UILabel *sendingLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *milesLabel;
@property (weak, nonatomic) IBOutlet UILabel *packageStutusLabel;
@property (strong, nonatomic) IBOutlet UILabel *smallSizeLabel;
@property (strong, nonatomic) IBOutlet UILabel *mediumSizeLabel;
@property (strong, nonatomic) IBOutlet UILabel *largeSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *takenDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *takenTime;
@property (weak, nonatomic) IBOutlet UILabel *deliveryDate;
@property (weak, nonatomic) IBOutlet UILabel *deliveryTime;
@property (weak, nonatomic) IBOutlet UILabel *date;


@property (strong, nonatomic) IBOutlet UIButton *btnMore;
@property (weak, nonatomic) IBOutlet UIButton *btnRateUser;
@property (strong, nonatomic) IBOutlet UIButton *btnCancelOrder;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

//Successfully Delivered view outlets
@property (strong, nonatomic) IBOutlet UIView *receiverProfileView;
@property (weak, nonatomic) IBOutlet UILabel *receiverNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *receiverEmailLabel;
@property (weak, nonatomic) IBOutlet UILabel *receiverPhoneLabel;
@property (strong, nonatomic) IBOutlet UIButton *callRecBtn;

@property (strong, nonatomic) IBOutlet UIView *courierProfileView;
@property (weak, nonatomic) IBOutlet UIView *ImagesView;

@property (weak, nonatomic) IBOutlet UIImageView *courierProfilePic;
@property (strong, nonatomic) IBOutlet UILabel *courierNameLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet UIButton *chatBtn;
@property (weak, nonatomic) IBOutlet UIButton *callBtn;
@property (weak, nonatomic) IBOutlet UIButton *btnTrack;

@property (weak, nonatomic) IBOutlet UIView *successfullyDeliveredView;
@property (weak, nonatomic) IBOutlet UILabel *successfullyDeliveredLabel;
@property (strong, nonatomic) IBOutlet UIView *cancelOrderView;
@property (weak, nonatomic) IBOutlet UIView *transparentView;
@property (weak, nonatomic) IBOutlet UIView *containerView;

//Report View
@property (strong, nonatomic) IBOutlet UIView *reportView;
@property (strong, nonatomic) IBOutlet AUIAutoGrowingTextView *commentTextView;


@property (weak, nonatomic) IBOutlet UIView *navigationView;

@property (weak, nonatomic) IBOutlet UIImageView *pic1;
@property (weak, nonatomic) IBOutlet UIImageView *pic2;
@property (weak, nonatomic) IBOutlet UIImageView *pic3;
@property (weak, nonatomic) IBOutlet UIImageView *pic4;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *PicArray;

//Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *descriptionHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *moreButtonHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *addressTableHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *receiverHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *courierHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *detailHeightConstraint;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *reportViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *successViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cancelButtonTopConstraint;

@property (weak, nonatomic) IBOutlet UILabel *lblReason;
@property (nonatomic, assign) NSInteger selectedReasonIndex;
@property (nonatomic, strong) NSArray *pickerOptionsList;

@property (weak, nonatomic) IBOutlet UITextView *SendingTextfield;
@property (weak, nonatomic) IBOutlet UITextView *DescriptionTextfield;

@property (weak, nonatomic) IBOutlet UIButton *ChangeBtn1;
@property (weak, nonatomic) IBOutlet UIButton *ChangeBtn2;



@end
