//
//  TutorialViewController.m
//  CheapSender
//
//  Created by admin on 4/18/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "TutorialViewController.h"
#import "TAPageControl.h"


@interface TutorialViewController () <UIScrollViewDelegate, TAPageControlDelegate>

//Outlets
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutletCollection(UIScrollView) NSArray *scrollViews;

@property (weak, nonatomic) IBOutlet TAPageControl *customStoryboardPageControl;

@property (strong, nonatomic) IBOutlet UILabel *currentPageTitle;

@property (strong, nonatomic) IBOutlet UIButton *btnGetStaerted;

@property (strong, nonatomic) MPMoviePlayerViewController *moviePlayer;

//Arrays
@property (strong, nonatomic) NSArray *imagesData;
@property (strong, nonatomic) NSArray *titles;

@end

@implementation TutorialViewController

#pragma mark
#pragma mark -- UIView LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
//    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:NO];
}

- (void)viewDidAppear:(BOOL)animated
{
    
    self.imagesData = @[@"Bg.png",@"Bg.png",@"Bg.png"];
    
    self.titles = @[@"On Demand Delivery Service",@"Fast, Secure, Low-Cost, and On-Time App",@"Earn Extra Income"];
    
    [self setupScrollViewImages];
    
    for (UIScrollView *scrollView in self.scrollViews) {
        scrollView.delegate = self;
    }
    
    // TAPageControl from storyboard
    self.customStoryboardPageControl.currentPage = 0;
    self.customStoryboardPageControl.numberOfPages = self.imagesData.count;
    self.customStoryboardPageControl.dotSize = CGSizeMake(11, 11);
    self.customStoryboardPageControl.dotImage = [UIImage imageNamed:@"Dot-Icon.png"];
    self.customStoryboardPageControl.currentDotImage = [UIImage imageNamed:@"Dot-Selected-Icon.png"];
    
    for (UIScrollView *scrollView in self.scrollViews) {
        scrollView.contentSize = CGSizeMake(CGRectGetWidth(scrollView.frame) * self.imagesData.count, CGRectGetHeight(scrollView.frame));
    }
    self.currentPageTitle.text = self.titles[0];
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    
   // self.btnGetStaerted = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnGetStaerted withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
}

- (void)setupScrollViewImages
{
    for (UIScrollView *scrollView in self.scrollViews) {
        
        CGRect frame = scrollView.frame;
        frame.size.width = self.view.frame.size.width;
        frame.size.height = self.scrollView.frame.size.height;
        scrollView.frame = frame;
        
        [self.imagesData enumerateObjectsUsingBlock:^(NSString *imageName, NSUInteger idx, BOOL *stop) {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(scrollView.frame) * idx, 0, CGRectGetWidth(scrollView.frame), CGRectGetHeight(scrollView.frame))];
            imageView.contentMode = UIViewContentModeScaleAspectFill;
            imageView.image = [UIImage imageNamed:imageName];
            [scrollView addSubview:imageView];
        }];
    }
}

#pragma mark
#pragma mark --UIScrollView Delegates

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    NSInteger pageIndex = scrollView.contentOffset.x / CGRectGetWidth(scrollView.frame);
    self.customStoryboardPageControl.currentPage = pageIndex;
    
    self.currentPageTitle.text = self.titles[pageIndex];
}

#pragma mark
#pragma mark --IBActions

- (IBAction)getStartedAction:(id)sender {
    
//    [[AppDelegate sharedAppDelegate].appController showStartupView];
//    [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:0] forKey:SHOW_TUTORIAL_SCREEN];
}

- (IBAction)playVideoAction:(id)sender {
    
//    NSString *filepath   =   [[NSBundle mainBundle] pathForResource:@"PromoVideo.mp4"
//                                                             ofType:nil];
//    NSURL    *fileURL    =   [NSURL fileURLWithPath:filepath];
//    
//    [AppDelegate sharedAppDelegate].shouldRotate = YES;
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(moviePlaybackDidFinish)
//                                                 name:MPMoviePlayerPlaybackDidFinishNotification
//                                               object:_moviePlayer.moviePlayer];
//    
//    _moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:fileURL];
//    [self.navigationController presentMoviePlayerViewControllerAnimated:_moviePlayer];
}

- (void)moviePlaybackDidFinish
{
    [AppDelegate sharedAppDelegate].shouldRotate = NO;
}




@end
