//
//  SignUpTakePhotoVC.m
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SignUpTakePhotoVC.h"

@interface SignUpTakePhotoVC ()

//Outlets
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
@property (strong, nonatomic) IBOutlet UIButton *takePhotoBtn;
@property (strong, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (strong, nonatomic) IBOutlet UILabel *stepLabel;

@property (nonatomic, assign) NSInteger profileImageSelection;
@property (weak, nonatomic) IBOutlet UIButton *laterButton;

@end

@implementation SignUpTakePhotoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
    self.btnRegister = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnRegister withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    _userProfileImage = (UIImageView *)[UtilHelper makeImageRounded:_userProfileImage];
    
    self.btnRegister.selected = YES;
    _profileImageSelection = 0;
    
    if (_isComingFromProfile) {
        
        _laterButton.hidden = YES;
        _stepLabel.hidden = YES;
        [self.btnRegister setTitle:@"Update" forState:UIControlStateNormal];
        self.btnRegister.selected = NO;
        _profileImageSelection = 1;
        [_takePhotoBtn setTitle:@"Change picture" forState:UIControlStateNormal];
        _headingTopConstaint.constant = 36;

        [self checkActiveButton];

        NSString *senderId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
        //Update database
        CourierInfoDB *courierInfo =  [CourierInfoDB getCourierInfoDB:[senderId intValue]];
        [self loadCourierProfileImage:courierInfo.imageName];
        [self requestToUserinfo];
    }
    else
    {
//        [self updateCountOfStepsCompletedByCourierInRegistration:@"2"];
    }
}

- (void)loadCourierProfileImage:(NSString *)profileImage
{
    //Shows indicatore on imageView
    [_userProfileImage setShowActivityIndicatorView:YES];
    [_userProfileImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, profileImage];
    
    //Loads image
    [_userProfileImage sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
}


#pragma mark
#pragma mark --Actions

- (IBAction)laterBtnAction:(id)sender {
//    [[AppDelegate sharedAppDelegate].appController showTransportTypeVC:NO isComingFromVehicleView:YES];
}

- (IBAction)signUpAction:(id)sender {
    
    if (!_btnRegister.selected) {
        
        [self showProgressHud];
        [self requestToUploadSelfImage];
    }
}

//Camera Btutton
- (IBAction)cameraImageViewAction:(id)sender {
    
    [self showMediaActionSheet];
}

//Camera Btutton
- (IBAction)cameraAction:(id)sender {
        
    [self showMediaActionSheet];
}

- (void)showMediaActionSheet
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Media" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library",@"Take Photo",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:self.view];
}

#pragma
#pragma  mark action sheet delegate method

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex==1) {
        
//        NSLog(@"Take Photo Button Pressed");
        [self pickMediaFromSource:UIImagePickerControllerSourceTypeCamera];
    }
    else if(buttonIndex==0)
    {
        [self pickMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

#pragma
#pragma  mark camera delegate method
- (void)pickMediaFromSource:(UIImagePickerControllerSourceType)sourceType{
    @autoreleasepool {
        if([UIImagePickerController isSourceTypeAvailable:sourceType])
        {
            
            NSArray *mediaTypes=[UIImagePickerController availableMediaTypesForSourceType:sourceType];
            UIImagePickerController *picker=[[UIImagePickerController alloc]init];
            picker.mediaTypes=mediaTypes;
            picker.delegate=self;
            picker.allowsEditing=YES;
            //            picker.allowsEditing=NO;
            picker.sourceType=sourceType;
            [self presentViewController:picker animated:YES completion:NULL];
            
        }
        else
        {
            [UtilHelper showApplicationAlertWithMessage:@"Camera is not found" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
        }
    }
}

#pragma mark -
#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)])
    {
//        NSLog(@"Image");
        //image
        @autoreleasepool {
            UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
            
            _userProfileImage.image = chosenImage;
            [_takePhotoBtn setTitle:@"Change picture" forState:UIControlStateNormal];
            
            [_takePhotoBtn setTitleColor: [UIColor colorWithRed:17/255.0 green:167/255.0 blue:170/255.0 alpha:1.0] forState:UIControlStateNormal];
            [_takePhotoBtn setImage:nil forState:UIControlStateNormal];
            
            _profileImageSelection = 1;
            [self checkActiveButton];
        }
    }
    else
    {
//        NSLog(@"Video");
        //video
        [UtilHelper showApplicationAlertWithMessage:@"Video does not support." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark--
#pragma mark-- Server Side handling

- (void)checkActiveButton
{
    BOOL isActive = YES;
    
    if (_profileImageSelection == 0) {
        
        isActive = NO;
    }
    
    if (isActive) {
        
        _btnRegister.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
        _btnRegister.selected = NO;
        
    }else {
        
        _btnRegister.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnRegister.selected = YES;
    }
}

- (void)requestToUploadSelfImage
{
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    NSData* imageData = UIImageJPEGRepresentation(_userProfileImage.image, 0.5);
    NSString *image1Base64 = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    NSString *stringBase = @"data:image/png;base64,";
    NSString *finalBase64 = [stringBase stringByAppendingString:image1Base64];
    
    
    [WebServicesClient SelfImage:finalBase64
                   LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                     LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                    LogUserAgent:[[UIDevice currentDevice] systemVersion]
                   LogDeviceType:LOG_DEVICE_TYPE
               completionHandler:^(NSString *imageName, NSError *error) {
                   
                   [self hideProgressHud];
                   
                   if (!error) {
                       
                       if (!_isComingFromProfile) {
                           
                           [[AppDelegate sharedAppDelegate].appController showTransportTypeVC:NO isComingFromVehicleView:NO];
                           [self updateCountOfStepsCompletedByCourierInRegistration:@"3"];

                           
                       } else {
                           
                           NSString *senderId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
                           [CourierInfoDB insertAndupdateCourierWithId:senderId
                                                                  Name:EMPTY_STRING
                                                               SurName:EMPTY_STRING
                                                                 Email:EMPTY_STRING
                                                               Address:EMPTY_STRING
                                                             ImageName:imageName
                                                                  Rate:EMPTY_STRING
                                                               Balance:@"100"
                                                              IsUpdate:YES];
                       }
                       
                   } else if (error.code == 3) {
                       
                       [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                       
                   } else {
                       
                       [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                   }
               }];
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestToUserinfo
{
    NSString *senderId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
    [WebServicesClient GetUserInfoById:senderId
                         LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                     completionHandler:^(UserModel *user, NSError *error) {
                         
                         if (!error) {
                             
                             [self loadCourierProfileImage:user.selfImg];
                         }
                     }];
}

#pragma mark -
#pragma mark - AlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
    }
}


@end
