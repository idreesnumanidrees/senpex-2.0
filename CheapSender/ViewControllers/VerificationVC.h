//
//  VerificationVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface VerificationVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIView *emptyNotificationView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
