//
//  TermsViewController.h
//  CheapSender
//
//  Created by Riyas Abdul Rahman on 5/1/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface TermsViewController : BaseViewController

@property (strong, nonatomic) NSString *ruleType;

@end
