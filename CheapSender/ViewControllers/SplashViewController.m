//
//  SplashViewController.m
//  CheapSender
//
//  Created by Tarik on 3/26/18.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import "SplashViewController.h"
#import "AppDelegate.h"

@interface SplashViewController ()

@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewImage;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollViewTexts;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@property (nonatomic, assign) BOOL isNextButtonPressed;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
//# warning This will remove in future
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please wait..."
//                                                    message:@""
//                                                   delegate:nil
//                                          cancelButtonTitle:nil
//                                          otherButtonTitles:nil, nil];
//    [alert show];
//    [alert dismissWithClickedButtonIndex:0 animated:NO];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)skipAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showStartupView];
}

- (IBAction)nextAction:(id)sender {

    if (_pageControl.currentPage == 2) {

        [self skipAction:nil];
    } else {
       
        _isNextButtonPressed = YES;
        _pageControl.currentPage += 1;
        [self setPage];
    }
}

- (IBAction)pageClicked:(id)sender {
    [self setPage];
}

- (void)setPage {
    
    if((_scrollViewImage.contentOffset.x + _scrollViewImage.frame.size.width) <= _scrollViewImage.contentSize.width) {
        CGPoint tempContentOffset = _scrollViewImage.contentOffset;
        tempContentOffset.x += _scrollViewImage.frame.size.width;  //to go right
        [_scrollViewImage setContentOffset: tempContentOffset animated:YES];
    }
    
    if((_scrollViewTexts.contentOffset.x + _scrollViewTexts.frame.size.width) <= _scrollViewTexts.contentSize.width) {
        CGPoint tempContentOffset = _scrollViewTexts.contentOffset;
        tempContentOffset.x += _scrollViewTexts.frame.size.width;  //to go right
        [_scrollViewTexts setContentOffset: tempContentOffset animated:YES];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    _isNextButtonPressed = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (_isNextButtonPressed == NO) {
        CGPoint offset = scrollView.contentOffset;
        if (scrollView == _scrollViewImage) {
            
            offset.x = offset.x * _scrollViewTexts.frame.size.width / _scrollViewImage.frame.size.width;
            [_scrollViewTexts setContentOffset:offset];
            
        } else {
            
            offset.x = offset.x * _scrollViewImage.frame.size.width / _scrollViewTexts.frame.size.width;
            [_scrollViewImage setContentOffset:offset];
        }
        
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        self.pageControl.currentPage = page;
        if(page == 2){
            [_btnNext setTitle:@"Finish" forState:UIControlStateNormal];
        } else {
            [_btnNext setTitle:@"Next" forState:UIControlStateNormal];
        }
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
