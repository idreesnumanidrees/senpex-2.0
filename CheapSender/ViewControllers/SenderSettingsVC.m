//
//  SenderSettingsVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/23.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SenderSettingsVC.h"

@interface SenderSettingsVC ()

@end

@implementation SenderSettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark --IBActions

- (IBAction)changePasswordAction:(id)sender {    
    [[AppDelegate sharedAppDelegate].appController showSenderChangePasswordVC];
}

@end
