//
//  SendingStatusViewController.h
//  CheapSender
//
//  Created by Admin on 10/04/2018.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UtilHelper.h"
#import "BaseViewController.h"

@interface SendingStatusViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *icon;

@property (weak, nonatomic) IBOutlet UILabel *label_1;
@property (weak, nonatomic) IBOutlet UILabel *label_2;
@property (weak, nonatomic) IBOutlet UILabel *label_3;
@property (weak, nonatomic) IBOutlet UILabel *label_4;
@property (weak, nonatomic) IBOutlet UILabel *label_5;

@property (weak, nonatomic) IBOutlet UILabel *title_1;
@property (weak, nonatomic) IBOutlet UILabel *title_2;
@property (weak, nonatomic) IBOutlet UILabel *title_3;
@property (weak, nonatomic) IBOutlet UILabel *title_4;
@property (weak, nonatomic) IBOutlet UILabel *title_5;

@property (weak, nonatomic) IBOutlet UILabel *courierName_2;
@property (weak, nonatomic) IBOutlet UILabel *courierName_3;
@property (weak, nonatomic) IBOutlet UILabel *courierName_5;
@property (weak, nonatomic) IBOutlet UILabel *courerTitle_2;
@property (weak, nonatomic) IBOutlet UILabel *courierTitle_3;
@property (weak, nonatomic) IBOutlet UILabel *courierTitle_5;

@property (weak, nonatomic) IBOutlet UIImageView *image_2;
@property (weak, nonatomic) IBOutlet UIImageView *image_3;
@property (weak, nonatomic) IBOutlet UIImageView *image_5;

@property (weak, nonatomic) IBOutlet UIButton *btnOverview;
@property (weak, nonatomic) IBOutlet UIButton *btnStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblDay1;
@property (strong, nonatomic) IBOutlet UILabel *lblTime1;
@property (strong, nonatomic) IBOutlet UILabel *lblDay2;
@property (strong, nonatomic) IBOutlet UILabel *lblTime2;
@property (strong, nonatomic) IBOutlet UILabel *lblDay3;
@property (strong, nonatomic) IBOutlet UILabel *lblTime3;
@property (strong, nonatomic) IBOutlet UILabel *lblDay4;
@property (strong, nonatomic) IBOutlet UILabel *lblTime4;
@property (strong, nonatomic) IBOutlet UILabel *lblDay5;
@property (strong, nonatomic) IBOutlet UILabel *lblTime5;

@property (strong, nonatomic) IBOutlet UILabel *lblPickAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblDeliveryAddress;


// View Height
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight3;
@property (weak, nonatomic) IBOutlet UIImageView *dotLine2;
@property (weak, nonatomic) IBOutlet UIImageView *dotLine3;


@property (strong, nonatomic) PackDetailsModel *packDetails;

@end
