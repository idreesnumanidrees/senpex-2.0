//
//  SenderFeedBackVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/23.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SenderFeedBackVC.h"
#import "AUIAutoGrowingTextView.h"

@interface SenderFeedBackVC ()

@property (weak, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UILabel *placeholderLbl;
@property (weak, nonatomic) IBOutlet AUIAutoGrowingTextView *textView;

//Subject of feedback
@property (weak, nonatomic) IBOutlet UILabel *lblReason;
@property (nonatomic, assign) NSInteger selectedReasonIndex;
@property (nonatomic, strong) NSArray *pickerOptionsList;

@end

@implementation SenderFeedBackVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark--
#pragma mark --Helpers

- (void)prepareUI
{
    self.btnSend = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnSend withRadius:_btnSend.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextViewTextDidChangeNotification object:nil];
    [_textView setTextContainerInset:UIEdgeInsetsMake(10, 16, 10, 16)];
        
    _pickerOptionsList = @[@"Subject1",@"Subject2",@"Subject3",@"Subject4"];
    self.selectedReasonIndex = 0;
}

- (void)textChanged:(NSNotification *)notification
{
    if([[_textView text] length] == 0) {
        
        _btnSend.enabled = NO;
        _btnSend.backgroundColor = [UIColor colorWithRed:185.0f/255.0f green:192.0f/255.0f blue:200.0f/255.0f alpha:1];
        //Your feedback
        _placeholderLbl.text = @"Leave a comment";
        
    }
    else
    {
        _btnSend.enabled = YES;
        _btnSend.backgroundColor = [UIColor colorWithRed:53.0f/255.0f green:66.0f/255.0f blue:88.0f/255.0f alpha:1];
        _placeholderLbl.text = @"";
    }
}

- (IBAction)sendFeedbackAction:(id)sender {
    [self.view endEditing:YES];
    [_textView resignFirstResponder];
    _btnSend.enabled = NO;

    [self performSelector:@selector(requestToSendFeedback) withObject:nil afterDelay:1];
}

- (IBAction)callUsAction:(id)sender {
    
    NSString *cleanedString = [[@"6697775733" componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
}

- (IBAction)cancelOrderOptionsAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Subject" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.delegate = self;
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
    alert.tag = 0;
    picker.delegate = self;
    [picker selectRow:self.selectedReasonIndex inComponent:0 animated:YES];
    [alert addSubview:picker];
    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
    [alert setValue:picker forKey:@"accessoryView"];
    [alert show];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.pickerOptionsList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.pickerOptionsList objectAtIndex:row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.selectedReasonIndex = row;
    _lblReason.text = _pickerOptionsList[row];
}

#pragma mark UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 2)
        [self.navigationController popViewControllerAnimated:YES];
    else if(alertView.tag == 1001)
        [self senderLogout];
    else
        _lblReason.text = _pickerOptionsList[self.selectedReasonIndex];
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestToSendFeedback
{
    [self showProgressHud];
    
    [WebServicesClient SendFeedback:_textView.text
                           OptionId:[NSString stringWithFormat:@"%li", (long)self.selectedReasonIndex]
                      logSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                        LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                       LogUserAgent:[[UIDevice currentDevice] systemVersion]
                      LogDeviceType:LOG_DEVICE_TYPE
                  completionHandler:^(BOOL isSent, NSError *error) {
                      _btnSend.enabled = YES;

                      [self hideProgressHud];
                      
                      if (!error) {
                          
                          [UtilHelper showApplicationAlertWithMessage:@"Your feedbacks are valuable for us. Shortly, Senpex Team will respond to you." withDelegate:self withTag:2 otherButtonTitles:@[@"Ok"]];
                          
                      } else if (error.code == 3) {
                          
                          [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                          
                      } else {
                          
                          [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                      }
                  }];
}

@end
