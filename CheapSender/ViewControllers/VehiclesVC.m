//
//  VehiclesVC.m
//  CheapSender
//
//  Created by Idrees on 2017/07/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "VehiclesVC.h"
#import "TransportTableCell.h"

@interface VehiclesVC () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *transports;

@end

@implementation VehiclesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [self getUserTransports];
}

#pragma mark
#pragma mark - IBActions

- (IBAction)addNewVehicle:(id)sender
{
    [[AppDelegate sharedAppDelegate].appController showTransportTypeVC:YES isComingFromVehicleView:NO];
}

#pragma mark
#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _transports.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"TransportTableCell";
    
    TransportTableCell *cell = (TransportTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    GetTransports *transport = _transports[indexPath.row];
    cell.modelLabel.text = [NSString stringWithFormat:@"%@, %@", transport.transportMake, transport.transportModel];
    
    if ([transport.transportType isEqualToString:@"1"]) {
        
        cell.vehicleImgView.image = [UIImage imageNamed:@"car"];
    }else if ([transport.transportType isEqualToString:@"2"]) {
        
        cell.vehicleImgView.image = [UIImage imageNamed:@"van"];
    }else if ([transport.transportType isEqualToString:@"3"]) {
        
        cell.vehicleImgView.image = [UIImage imageNamed:@"truck"];
    }else if ([transport.transportType isEqualToString:@"4"]) {
        
        cell.vehicleImgView.image = [UIImage imageNamed:@"scooter"];
    }else if ([transport.transportType isEqualToString:@"6"]) {
        
        cell.modelLabel.text = @"Bike";
        cell.vehicleImgView.image = [UIImage imageNamed:@"motor-Cycle"];
    }else if ([transport.transportType isEqualToString:@"7"]) {
        
        cell.modelLabel.text = @"Walking";
        cell.vehicleImgView.image = [UIImage imageNamed:@"walk"];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    GetTransports *transport = _transports[indexPath.row];
    if ([transport.transportType isEqualToString:@"1"]) {
        
        [AppDelegate sharedAppDelegate].vehicleId = @"1";
        [AppDelegate sharedAppDelegate].vehicleName = @"Car";
        [AppDelegate sharedAppDelegate].vehicleImageName = @"car";
        
    }else if ([transport.transportType isEqualToString:@"2"]) {
        
        [AppDelegate sharedAppDelegate].vehicleId = @"2";
        [AppDelegate sharedAppDelegate].vehicleName = @"Van";
        [AppDelegate sharedAppDelegate].vehicleImageName = @"van";
        
    }else if ([transport.transportType isEqualToString:@"3"]) {
        
        [AppDelegate sharedAppDelegate].vehicleId = @"3";
        [AppDelegate sharedAppDelegate].vehicleName = @"Truck";
        [AppDelegate sharedAppDelegate].vehicleImageName = @"truck";

    }else if ([transport.transportType isEqualToString:@"4"]) {
        
        [AppDelegate sharedAppDelegate].vehicleId = @"4";
        [AppDelegate sharedAppDelegate].vehicleName = @"Scooter";
        [AppDelegate sharedAppDelegate].vehicleImageName = @"scooter";
    }

    if (![transport.transportType isEqualToString:@"6"] && ![transport.transportType isEqualToString:@"7"]) {
    [[AppDelegate sharedAppDelegate].appController showAddTransportView:YES Transport:_transports[indexPath.row]];
    }
}

//************************************ Get and update courier transports ********************


- (void)getUserTransports
{
    [self showProgressHud];
    
    [WebServicesClient GetUserTransports:[AppDelegate sharedAppDelegate].sessionKey
                             LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                            LogUserAgent:[[UIDevice currentDevice] systemVersion]
                           LogDeviceType:LOG_DEVICE_TYPE
                       completionHandler:^(NSMutableArray *usersTransports, NSError *error) {
                           
                           [self hideProgressHud];
                           
                           if (!error) {
                               
                               _transports = usersTransports;
                               [_tableView reloadData];
                           }
                           else if (error.code == 3) {
                               
                               [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                               
                           } else {
                               
                               [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                           }
                       }];
}



@end
