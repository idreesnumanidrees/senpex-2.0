//
//  AddTransportVC.m
//  CheapSender
//
//  Created by admin on 4/22/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "AddTransportVC.h"
#import "AddTransportTableCell.h"
#import "AddImageObject.h"

@interface AddTransportVC ()<AddNewTransportDelegate>{
    
    AddTransportTableCell *currCell;
}

@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UIButton *addNewVehicle;

@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UILabel *stepLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *selectedMediaArray;

@property (strong, nonatomic) NSString *insertedId;
@property (assign, nonatomic) BOOL isUpdatedTransport;

@property (nonatomic, strong) NSArray *validationArray;

@property (assign, nonatomic) int indexOfImageUpload;
@property (assign, nonatomic) BOOL isNextPressed;
@property (assign, nonatomic) BOOL isNeedsToUpdate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headingTopConstraint;

@end

@implementation AddTransportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    
    [_tableView setContentInset:UIEdgeInsetsMake(-10, 0, 0, 0)];

    _btnNext = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnNext withRadius:_btnNext.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    _selectedMediaArray = [NSMutableArray new];
    
    _btnNext.selected = YES;
    _indexOfImageUpload = 0;
    
    if (_isComingFromProfile) {

        _stepLabel.hidden = YES;
        _headingLabel.text = @"Vehicle";
        _headingTopConstraint.constant = 36;
        
        _addNewVehicle.hidden = YES;
        
        [self.btnNext setTitle:@"Update" forState:UIControlStateNormal];
        
        if (_transport == nil) {
            
            [self.btnNext setTitle:@"Add a vehicle" forState:UIControlStateNormal];
        }
        else
        {
            [self getTransportImages];
            _isUpdatedTransport = YES;
        }
        _insertedId = _transport.internalBaseClassIdentifier;
    }
    else
    {
//        [self updateCountOfStepsCompletedByCourierInRegistration:@"3"];
    }
}

#pragma mark
#pragma mark --Actions

- (IBAction)addMoreVehicalAction:(id)sender {
    
    _isNextPressed = NO;
    [self validation];
}

- (IBAction)laterBtnAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showInsuranceCardView:NO];
}

- (IBAction)nextAction:(id)sender {
    
    if (!_btnNext.selected) {
        
        _isNextPressed = YES;
        [self validation];
    }
}

//- (IBAction)deleteImageBtnClicked:(UIButton *)sender {
//
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
//    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
//    AddTransportTableCell *deleteCell = [self.tableView cellForRowAtIndexPath:indexPath];
//    [self deleteTransportImageById:deleteCell Sender:sender];
//}

- (IBAction)imagePickerBtnClicked:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    currCell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    [self.view endEditing:YES];
    
    if (_selectedMediaArray.count < 4) {
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Media" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library",@"Take Photo",nil];
        actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
        [actionSheet showInView:self.view];
    }
    else
    {
        [UtilHelper showApplicationAlertWithMessage:@"Sorry! You can not select more than 4 images" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
    }
}

#pragma
#pragma  mark action sheet delegate method

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex==1) {
        
        //        NSLog(@"Take Photo Button Pressed");
        [self pickMediaFromSource:UIImagePickerControllerSourceTypeCamera];
    }
    else if(buttonIndex==0)
    {
        [self pickMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

#pragma
#pragma  mark camera delegate method
- (void)pickMediaFromSource:(UIImagePickerControllerSourceType)sourceType{
    @autoreleasepool {
        if([UIImagePickerController isSourceTypeAvailable:sourceType])
        {
            
            NSArray *mediaTypes=[UIImagePickerController availableMediaTypesForSourceType:sourceType];
            UIImagePickerController *picker=[[UIImagePickerController alloc]init];
            picker.mediaTypes=mediaTypes;
            picker.delegate=self;
            picker.allowsEditing=YES;
            picker.sourceType=sourceType;
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            [UtilHelper showApplicationAlertWithMessage:@"Camera is not found" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
        }
    }
}

#pragma mark -
#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)])
    {
        //        NSLog(@"Image");
        //image
        @autoreleasepool {
            UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
            
            AddImageObject *addImageObject = [AddImageObject new];
            addImageObject.image = chosenImage;
            [_selectedMediaArray addObject:addImageObject];
            
            [self.tableView reloadData];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
            });
            
            [self checkActiveButton:@"123"];
        }
    }
    else
    {
        //        NSLog(@"Video");
        //video
        [UtilHelper showApplicationAlertWithMessage:@"Video does not support." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark
#pragma mark - TableView Data source and Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1; //count of section
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"AddTransportCell";
    AddTransportTableCell *cell = (AddTransportTableCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.Delegate = self;
    _validationArray = @[cell.tfMake,cell.tfModel,cell.tfYear,cell.tfPlateNumber];
    currCell = cell;
    currCell.selectedVehicleLabel.text = [AppDelegate sharedAppDelegate].vehicleName;
    currCell.selectedVehicleImageView.image = [UIImage imageNamed:[AppDelegate sharedAppDelegate].vehicleImageName];
    
    [cell.deleteImageButton addTarget:self action:@selector(deleteImageTapped) forControlEvents:UIControlEventTouchUpInside];

    [cell setupForImages:_selectedMediaArray];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_selectedMediaArray.count > 0) {
        return 676;
    } else
    {
        return 462;
    }
}

- (void)deleteImageTapped
{
    if (_selectedMediaArray.count > 0) {
        
        [self deleteTransportImageById:currCell.pageControl.currentPage];
    }
}

#pragma mark -
#pragma mark - Text Field Delegate Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    [self checkValidation:numberOFCharacter];
    
    return YES;
}

#pragma
#pragma  mark AddNewTransportCell Delegate

- (void)checkValidation:(NSString *)text
{
    [self checkActiveButton:text];
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)checkActiveButton:(NSString *)numberOFCharacter
{
    BOOL isActive = NO;
    
    for (JVFloatLabeledTextField *tfText in self.validationArray) {
        
        if (tfText.text.length > 0) {
            
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
            
        } else {
            
            isActive = NO;
            break;
        }
    }
    
    if (_selectedMediaArray.count==0) {
        
        isActive = NO;
    }
    
    if (isActive) {
        
        _btnNext.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
        _btnNext.selected = NO;
        
    }else {
        
        _btnNext.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnNext.selected = YES;
    }
}

- (void)validation
{
    if ([UtilHelper isEmptyString:currCell.tfMake.text]) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Please enter vehicle make" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        
    } else if ([UtilHelper isEmptyString:currCell.tfModel.text]) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Please enter vehicle model" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        
    } else if ([UtilHelper isEmptyString:currCell.tfYear.text]) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Please enter vehicle year" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        
    } else if ([UtilHelper isEmptyString:currCell.tfPlateNumber.text]) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Please enter vehicle plate number" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    else if (_selectedMediaArray.count < 1)
    {
        [UtilHelper showApplicationAlertWithMessage:@"Please select atleast one vehicle" withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    else {
        
        if (![UtilHelper isNetworkAvailable]) {
            
            [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
            return;
        }
        
        [self showProgressHud];
        [self requestForAddNewTransport];
    }
}

- (void)requestForAddNewTransport
{
    [WebServicesClient AddNewTransport:[AppDelegate sharedAppDelegate].vehicleId
                         TransportYear:currCell.tfYear.text
                        TransportModel:currCell.tfModel.text
                         TransportMake:currCell.tfMake.text
                           PlateNumber:currCell.tfPlateNumber.text
                       IsUpdateVehicel:_isUpdatedTransport
                           transportId:_insertedId
                         LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                     completionHandler:^(NSString *insertedId, NSError *error) {
                         
                         if (!error) {
                             
                             if (self->_selectedMediaArray.count > 0) {
                                 
                                 self->_insertedId = insertedId;
                                 
                                 if (self->_isComingFromProfile) {
                                     
                                     if (self->_transport != nil) {
                                         self->_insertedId = self->_transport.internalBaseClassIdentifier;
                                     }
                                 }
                                 [self requestForAddNewTransportImage:0];
                                 
                             } else {
                                 
                                 if (!self->_isComingFromProfile) {
                                     
//                                     [[AppDelegate sharedAppDelegate].appController showAddTransportView:NO Transport:nil];
                                     [[AppDelegate sharedAppDelegate].appController showTransportTypeVC:NO isComingFromVehicleView:YES];
                                 }
                             }
                         } else {
                             
                             [self hideProgressHud];
                             [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                         }
                     }];
}

- (void)requestForAddNewTransportImage:(int)index
{
    AddImageObject *addImageObject = _selectedMediaArray[index];
    
    if (addImageObject.image == nil) {
        
        [self hideProgressHud];
        addImageObject.isUploaded = YES;
        _indexOfImageUpload++;
        
        BOOL isAllUploaded = YES;
        for (AddImageObject *imageObject in _selectedMediaArray) {
            
            if (!imageObject.isUploaded) {
                
                isAllUploaded = NO;
                [self requestForAddNewTransportImage:_indexOfImageUpload];
                break;
                return;
            }
        }
        
        if (isAllUploaded) {
            
            _indexOfImageUpload = 0;
            if (_isNextPressed) {
                
                if (!_isComingFromProfile) {
                    [[AppDelegate sharedAppDelegate].appController showInsuranceCardView:NO];
                }
                
            } else {
                
                if (!_isComingFromProfile) {
                    
                    [[AppDelegate sharedAppDelegate].appController showTransportTypeVC:NO isComingFromVehicleView:YES];
//                    [[AppDelegate sharedAppDelegate].appController showAddTransportView:NO Transport:nil];
                }
            }
        }
        
        return;
    }
    
    if(addImageObject.image != nil)
    {
        NSData* imageData1 = UIImageJPEGRepresentation(addImageObject.image, 0.5);
        NSString *image1Base64 = [imageData1 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        
        NSString *stringBase = @"data:image/png;base64,";
        NSString *finalBase64 = [stringBase stringByAppendingString:image1Base64];
        
        [WebServicesClient AddNewTransportImage:_insertedId
                                 TransportImage:finalBase64
                                  LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                    LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                   LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                  LogDeviceType:LOG_DEVICE_TYPE
                              completionHandler:^(NSString *transportImage, NSError *error) {
                                  
                                  if (!error) {
                                      
                                      _isNeedsToUpdate = YES;
                                      _isUpdatedTransport = YES;
                                      
                                      addImageObject.isUploaded = YES;
                                      addImageObject.insertedId = transportImage;
                                      _indexOfImageUpload++;
                                      
                                      BOOL isAllUploaded = YES;
                                      
                                      for (AddImageObject *imageObject in _selectedMediaArray) {
                                          
                                          if (!imageObject.isUploaded) {
                                              
                                              isAllUploaded = NO;
                                              [self requestForAddNewTransportImage:_indexOfImageUpload];
                                              break;
                                              return;
                                          }
                                      }
                                      
                                      if (isAllUploaded) {
                                          
                                          [self hideProgressHud];
                                          _indexOfImageUpload = 0;
                                          
                                          if (_isNextPressed) {
                                              
                                              if (!_isComingFromProfile) {
                                                  [[AppDelegate sharedAppDelegate].appController showInsuranceCardView:NO];
                                                  [self updateCountOfStepsCompletedByCourierInRegistration:@"4"];
                                              }
                                              
                                          } else {
                                              
                                              if (!_isComingFromProfile) {
                                                  
                                                  [[AppDelegate sharedAppDelegate].appController showTransportTypeVC:NO isComingFromVehicleView:YES];

//                                                  [[AppDelegate sharedAppDelegate].appController showAddTransportView:NO Transport:nil];
                                              }
                                          }
                                      }
                                  }
                                  else {
                                      
                                      [self hideProgressHud];
                                      [UtilHelper showApplicationAlertWithMessage:error.domain
                                                                     withDelegate:nil
                                                                          withTag:0
                                                                otherButtonTitles:@[@"Ok"]];
                                  }
                              }];
    }
    else
    {
        [self hideProgressHud];
        
        [UtilHelper showApplicationAlertWithMessage:@"One or more images have not been downloaded. Please contact administrator or try visitng the page after sometime."
                                       withDelegate:nil
                                            withTag:0
                                  otherButtonTitles:@[@"Ok"]];
        
    }
}

- (void)deleteTransportImageById:(NSUInteger)index
{
  
    AddImageObject *addImage = _selectedMediaArray[index];
    
    if (addImage.insertedId != nil) {
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient DeleteTransportImageById:addImage.insertedId
                                      LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                        LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                       LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                      LogDeviceType:LOG_DEVICE_TYPE
                                  completionHandler:^(BOOL result, NSError *error) {
                                      
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          
                                          if (!error) {
                                              
                                              [_selectedMediaArray removeObjectAtIndex:index];

                                              [self checkActiveButton:@"123"];
                                              [self.tableView reloadData];
                                          }
                                      });
                                  }];
    });
    } else {
       
        [_selectedMediaArray removeObjectAtIndex:index];
        [self checkActiveButton:@"123"];
        [self.tableView reloadData];
    }

}

//*********************************** Get transport images *****************

- (void)getTransportImages
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetTransportImagesByTransportId:_transport.internalBaseClassIdentifier
                                             LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                               LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                              LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                             LogDeviceType:LOG_DEVICE_TYPE
                                         completionHandler:^(NSMutableArray *imagesArray, NSError *error) {
                                             
                                             dispatch_async(dispatch_get_main_queue(), ^{
                                                 
                                                 if (!error) {
                                                     
                                                     currCell.transportTypeIndex = [_transport.transportType intValue];
                                                     currCell.tfModel.text = _transport.transportModel;
                                                     currCell.tfMake.text = _transport.transportMake;
                                                     currCell.tfYear.text = _transport.transportYear;
                                                     currCell.tfPlateNumber.text = _transport.plateNumber;
                                                     _selectedMediaArray = imagesArray;
                                                     
                                                     [self checkActiveButton:@"123"];
                                                     [self.tableView reloadData];
                                                 }
                                             });
                                         }];
    });
}





@end
