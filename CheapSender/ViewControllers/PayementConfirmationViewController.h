//
//  PayementConfirmationViewController.h
//  TestProject
//
//  Created by Ambika on 02/05/17.
//  Copyright © 2017 ZCO. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PaymentConfirmationScreenType.h"
#import "PaymentModel.h"

@interface PayementConfirmationViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic) PaymentConfirmationScreenType screenType;
@property (nonatomic) PaymentModel *model;

@end
