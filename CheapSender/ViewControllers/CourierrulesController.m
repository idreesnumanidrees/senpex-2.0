//
//  CourierrulesController.m
//  CheapSender
//
//  Created by Riyas Abdul Rahman on 5/1/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierrulesController.h"

@interface CourierrulesController ()

@end

@implementation CourierrulesController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)prepareUI
{
    [WebServicesClient GetAcceptRule:@"courier_rule"
                         LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                        LogUserAgent:[[UIDevice currentDevice] systemVersion]
                       LogDeviceType:LOG_DEVICE_TYPE
                          SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                   completionHandler:^(NSMutableArray *rulesArray, NSError *error) {
                       
                       if (!error) {
                           
                       } else {
                           
                       }
                   }];
}

@end
