//
//  PaymentVC.m
//  Demo
//
//  Created by Rajesh on 04/05/17.
//  Copyright © 2017 Rajesh. All rights reserved.
//

#import "PaymentVC.h"
#import "PaymentHistoryCell.h"

@interface PaymentVC ()
@property (weak, nonatomic) IBOutlet UITableView *paymentTableView;
@property (strong, nonatomic) NSMutableArray *paymentsHistory;
@property (weak, nonatomic) IBOutlet UIView *emptypaymentsView;
@property (weak, nonatomic) IBOutlet UIView *transactionView;

@end

@implementation PaymentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.emptypaymentsView.hidden = YES;
    
    [self requestForPaymentHistory];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backBtnClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_paymentsHistory.count > 0) {
        self.paymentTableView.hidden = NO;
        self.emptypaymentsView.hidden = YES;
        self.transactionView.hidden = NO;
    } else {
        self.paymentTableView.hidden = YES;
        self.emptypaymentsView.hidden = NO;
        self.transactionView.hidden = YES;
    }
    return _paymentsHistory.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PaymentHistoryCell *cell = (PaymentHistoryCell *)[tableView dequeueReusableCellWithIdentifier:@"PaymentHistoryCell" forIndexPath:indexPath];

    PaymentHistory *paymentHistory = _paymentsHistory[indexPath.row];
    
    cell.sendName.text = paymentHistory.sendName;
    cell.price.text = [NSString stringWithFormat:@"$%@",paymentHistory.mcGross];
//    cell.insertedDate.text = paymentHistory.insertedDate;
    
    NSDate *notiDate = [NSDate dateFromDateString:paymentHistory.insertedDate];
    NSString *stringDate = [UtilHelper time:notiDate];
    cell.insertedDate.text = stringDate;
    
    cell.number.hidden = YES;
    
//    //Pack Status color
//    if ([paymentHistory.packStatus isEqualToString:@"10"]) {
//        cell.status.text = @"Paid pack";
//        cell.status.textColor = ORANGE_COLOR;
//    } else if ([paymentHistory.packStatus isEqualToString:@"20"]) {
//        cell.status.text = @"Selected";
//        cell.status.textColor = PACK_GREEN_STATUS_COLOR;
//    } else if ([paymentHistory.packStatus isEqualToString:@"30"]) {
//        cell.status.text = @"Given";
//        cell.status.textColor = PACK_GREEN_STATUS_COLOR;
//    } else if ([paymentHistory.packStatus isEqualToString:@"40"]) {
//        cell.status.text = @"Delivered";
//        cell.status.textColor = PACK_GREEN_STATUS_COLOR;
//    } else if ([paymentHistory.packStatus isEqualToString:@"50"]) {
//        cell.status.text = @"Not Given";
//        cell.status.textColor = ORANGE_COLOR;
//    } else if ([paymentHistory.packStatus isEqualToString:@"90"]) {
//        cell.status.text = @"Reported";
//        cell.status.textColor = PACK_RED_STATUS_COLOR;
//    } else if ([paymentHistory.packStatus isEqualToString:@"100"]) {
//        cell.status.text = @"Cancelled";
//        cell.status.textColor = PACK_RED_STATUS_COLOR;
//    }
//
//    if([paymentHistory.paymentModule isEqualToString:@"2"])
//        cell.status.text = [cell.status.text stringByAppendingString:@" - Refund"];
    
    if ([paymentHistory.paymentModule isEqualToString:@"1"]) {
        cell.status.textColor = ORANGE_COLOR;
    } else if ([paymentHistory.paymentModule isEqualToString:@"3"]){
        cell.status.textColor = PACK_GREEN_STATUS_COLOR;
    } else {
        cell.status.textColor = PACK_RED_STATUS_COLOR;
    }
    cell.status.text = paymentHistory.paymentModuleName;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestForPaymentHistory {
    [self showProgressHud];
    [WebServicesClient GetMyPayments:[AppDelegate sharedAppDelegate].iosDeviceToken
                        LogUserAgent:[[UIDevice currentDevice] systemVersion]
                       LogDeviceType:LOG_DEVICE_TYPE
                          SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                   completionHandler:^(NSMutableArray *paymentHistory, NSError *error) {
                       
                       [self hideProgressHud];
                       
                       if (!error) {
                           _paymentsHistory = paymentHistory;
                           [_paymentTableView reloadData];
                       } else if (error.code == 3)
                           [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                       else
                           [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                   }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1001)
        [self senderLogout];
}

@end
