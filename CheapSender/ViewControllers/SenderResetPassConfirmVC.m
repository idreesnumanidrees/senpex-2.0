//
//  SenderResetPassConfirmVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/23.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SenderResetPassConfirmVC.h"
#import "LoginViewController.h"
#import "SenderChangePasswordVC.h"

@interface SenderResetPassConfirmVC ()
@property (weak, nonatomic) IBOutlet UIButton *btnGoToLogin;

@end

@implementation SenderResetPassConfirmVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    self.btnGoToLogin = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnGoToLogin withRadius:_btnGoToLogin.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    if (_isComingFromChangePassword) {
        
        [self.btnGoToLogin setTitle:@"Go back" forState:UIControlStateNormal];
    }
}

#pragma mark
#pragma mark --IBActions

- (IBAction)goToLoginAction:(id)sender {
    
    if (_isComingFromChangePassword) {
        
        [self backToSettingScreen];
        
    } else {
      
        [self backToLogin];
    }
}

- (IBAction)closeAction:(id)sender {
    
    if (_isComingFromChangePassword) {
        
        [self backToSettingScreen];
        
    } else {
        
        [self backToLogin];
    }
}

- (void)backToLogin
{
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[LoginViewController class]]) {
            [self.navigationController popToViewController:aViewController animated:YES];
        }
    }
}

- (void)backToSettingScreen
{
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[SenderChangePasswordVC class]]) {
            [self.navigationController popToViewController:aViewController animated:YES];
        }
    }
}

@end
