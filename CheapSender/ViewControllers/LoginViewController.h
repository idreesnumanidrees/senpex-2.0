//
//  LoginViewController.h
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@import GoogleSignIn;

@interface LoginViewController : BaseViewController <GIDSignInDelegate, GIDSignInUIDelegate>

@end
