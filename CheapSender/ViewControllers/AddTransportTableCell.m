//
//  AddTransportTableCell.m
//  CheapSender
//
//  Created by Idrees on 2017/05/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "AddTransportTableCell.h"
#import "UtilHelper.h"
#import "AddImageObject.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CSWebConstants.h"
#import "AppDelegate.h"

@implementation AddTransportTableCell

@synthesize  firstCellIsHidden;
@synthesize seconCellIsHidden;

- (void)awakeFromNib {
    
    [self superclass];
    
    _transportTypeIndex = 0;
    self.innerScrollView.delegate = self;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)transportTypesSetup
{
    
//    //#define CarListArry @[@"Car", @"Van", @"Scooter", @"Bike"]
//
//    if ([AppDelegate sharedAppDelegate].allDictData.count > 0) {
//
//        NSPredicate *senderRepResPred = [NSPredicate predicateWithFormat:@"dicShort contains[c] %@", @"transport_types"];
//        NSMutableArray *senderRepResArr = [[[AppDelegate sharedAppDelegate].allDictData filteredArrayUsingPredicate:senderRepResPred] mutableCopy];
//        _CarListArry = senderRepResArr;
//    }
//
//    for (UIView *subView in _PetTypeScrollView.subviews) {
//        [subView removeFromSuperview];
//    }
//
//    CGRect scrollFrame = _PetTypeScrollView.frame;
//
//    float scrollViewWidth = (scrollFrame.size.width/4)-15;
//
//    CGRect buttonFrame = CGRectMake(0.0f, 7.0f, scrollViewWidth + 30, 40.0f);
//
//    for (int index = 0; index < _CarListArry.count; index++) {
//
//        UIButton *petName = [[UIButton alloc]initWithFrame:buttonFrame];
//        [petName addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
//        [petName setTag:index+1];
//
//        GetDictByName *dictName = _CarListArry[index];
//        NSString *imageName = dictName.listName;
//
//        if ([imageName isEqualToString:@"Truck"]) {
//
//            imageName = @"Van";
//        }
//        else if ([imageName isEqualToString:@"Motorcycle"]) {
//
//            imageName = @"Scooter";
//        }
//
//        [petName setImage:[UIImage imageNamed:[imageName lowercaseString]] forState:UIControlStateNormal];
//
//        petName.alpha = 0.41;
//        [self.PetTypeScrollView addSubview:petName];
//
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(buttonFrame), CGRectGetMaxY(buttonFrame)+20, CGRectGetWidth(buttonFrame), 18)];
//        [label setFont:[UIFont fontWithName:@"SFUIText-Regular" size:16]];
//        label.textAlignment = NSTextAlignmentCenter;
//
//        [label setText:dictName.listName];
//        label.textColor = [UIColor colorWithRed:37/255.0 green:37/255.0 blue:37/255.0 alpha:.40];
//        [self.PetTypeScrollView addSubview:label];
//        buttonFrame.origin.x+= buttonFrame.size.width+20.0f;
//
//        if (_transportTypeIndex == petName.tag) {
//
//            petName.alpha = 1.0;
//            [petName setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@-selected",[imageName lowercaseString]]] forState:UIControlStateNormal];
//        }
//    }
//
//    CGSize contentSize = self.PetTypeScrollView.frame.size;
//    contentSize.width = buttonFrame.origin.x;
//    [self.PetTypeScrollView setContentSize:contentSize];
}

- (IBAction)buttonPressed:(UIButton *)sender
{
//    int index = 0;
//    for (UIButton *petName in self.PetTypeScrollView.subviews) {
//
//        if ([petName isKindOfClass:[UIButton class]]) {
//            petName.alpha = 0.41;
//
//            GetDictByName *dictName = _CarListArry[index];
//
//            NSString *imageName = dictName.listName;
//
//            if ([imageName isEqualToString:@"Truck"]) {
//
//                imageName = @"Van";
//            }
//            else if ([imageName isEqualToString:@"Motorcycle"]) {
//
//                imageName = @"Scooter";
//            }
//
//            [petName setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@",[imageName lowercaseString]]] forState:UIControlStateNormal];
//            index++;
//        }
//    }
//
//    sender.alpha = 1.0;
//
//    GetDictByName *dictName = _CarListArry[[sender tag]-1];
//
//    NSString *imageName = dictName.listName;
//
//    if ([imageName isEqualToString:@"Truck"]) {
//
//        imageName = @"Van";
//    }
//    else if ([imageName isEqualToString:@"Motorcycle"]) {
//
//        imageName = @"Scooter";
//    }
//
//    [sender setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@-selected",[imageName lowercaseString]]] forState:UIControlStateNormal];
//    _transportTypeIndex = [dictName.listId intValue];
//
//    [_Delegate checkValidation:@"23324"];
}

- (void)setupForImages:(NSMutableArray *)mediaArray
{
    [self.pageControl setNumberOfPages:mediaArray.count];
    
    int imagesOffSet = 0;
    for (int i = 0; i < [mediaArray count]; i++) {
        
        AddImageObject *addImageObject = mediaArray[i];
        //We'll create an imageView object in every 'page' of our scrollView.
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(imagesOffSet,0,241, 155)];
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 7.0f;
        img.contentMode = UIViewContentModeScaleAspectFill;
        [self.innerScrollView addSubview:img];
        
        if (addImageObject.image == nil) {
            
            [self loadImage:addImageObject.imageName imageView:img];
            
        } else {
            
            img.image = addImageObject.image;
        }
        
        imagesOffSet+=241;
    }
    //Set the content size of our scrollview according to the total width of our imageView objects.
    _innerScrollView.contentSize = CGSizeMake(imagesOffSet, 155);
    [_innerScrollView setContentOffset:CGPointMake(_innerScrollView.contentSize.width - _innerScrollView.bounds.size.width, 0) animated:YES];

    if (mediaArray.count == 0) {
        _innerScrollView.hidden = YES;
        _pageControl.hidden = YES;
        _deleteImageButton.hidden = YES;
    }else {
        _innerScrollView.hidden = NO;
        _pageControl.hidden = NO;
        _deleteImageButton.hidden = NO;
    }
    
//    self.firstSetPhotoView.hidden  = YES; //!cell.firstCellIsHidden;
//    self.secondSetPhotoView.hidden = YES; //!cell.seconCellIsHidden;
//
//    self.btnCross1.hidden = YES;
//    self.btnCross2.hidden = YES;
//    self.btnCross3.hidden = YES;
//    self.btnCross4.hidden = YES;
//
//    _carImage1View.image = nil;
//    _carImage2View.image = nil;
//    _carImage3View.image = nil;
//    _carImage4View.image = nil;
//
//    for (int i = 0; i < mediaArray.count; i++) {
//
//        AddImageObject *addImageObject = mediaArray[i];
//
//        if (i == 0) {
//
//            if (addImageObject.image == nil) {
//
//                [self loadImage:addImageObject.imageName imageView:self.carImage1View];
//
//            } else {
//
//                self.carImage1View.image = addImageObject.image;
//            }
//            self.firstSetPhotoView.hidden  = NO;
//            self.btnCross1.hidden = NO;
//
//        } else if (i == 1) {
//
//            if (addImageObject.image == nil) {
//
//                [self loadImage:addImageObject.imageName imageView:self.carImage2View];
//
//            } else {
//
//                self.carImage2View.image = addImageObject.image;
//            }
//            self.firstSetPhotoView.hidden  = NO;
//            self.btnCross2.hidden = NO;
//
//        } else if (i == 2) {
//
//            if (addImageObject.image == nil) {
//
//                [self loadImage:addImageObject.imageName imageView:self.carImage3View];
//
//            } else {
//
//                self.carImage3View.image = addImageObject.image;
//            }
//            self.firstSetPhotoView.hidden  = NO;
//            self.secondSetPhotoView.hidden  = NO;
//            self.btnCross3.hidden = NO;
//
//        } else if (i == 3) {
//
//            if (addImageObject.image == nil) {
//
//                [self loadImage:addImageObject.imageName imageView:self.carImage4View];
//
//            } else {
//
//                self.carImage4View.image = addImageObject.image;
//            }
//            self.firstSetPhotoView.hidden  = NO;
//            self.secondSetPhotoView.hidden  = NO;
//            self.btnCross4.hidden = NO;
//        }
//    }
}

#pragma mark
#pragma mark -- LocationManagerService Delegates
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.innerScrollView.frame.size.width;
    int page = floor((self.innerScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
}

- (void)loadImage:(NSString *)profileImage imageView:(UIImageView *)imageView
{
    //Shows indicatore on imageView
    [imageView setShowActivityIndicatorView:YES];
    [imageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, profileImage];
    
    //Loads image
    [imageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
}

- (void)refreshTableView
{
    _transportTypeIndex = 0;
    
//    _carImage1View.image = nil;
//    _carImage2View.image = nil;
//    _carImage3View.image = nil;
//    _carImage4View.image = nil;
//
//    self.firstSetPhotoView.hidden  = YES;
//    self.secondSetPhotoView.hidden = YES;
//    self.btnCross1.hidden = YES;
//    self.btnCross2.hidden = YES;
//    self.btnCross3.hidden = YES;
//    self.btnCross4.hidden = YES;
    
    _tfMake.text = @"";
    _tfModel.text = @"";
    _tfYear.text = @"";
    _tfPlateNumber.text = @"";
}

@end
