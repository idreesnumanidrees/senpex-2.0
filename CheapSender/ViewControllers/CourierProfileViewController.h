//
//  CourierProfileViewController.h
//  CheapSender
//
//  Created by Ambika on 28/04/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface CourierProfileViewController : BaseViewController

@property (nonatomic, strong) PackDetailsModel *packDetails;
@property (nonatomic, strong) CourierConversations *conversation;
@property (nonatomic, assign) BOOL isComingFromChatDetails;

@end
