//
//  SendingDetailViewController.m
//  CheapSender
//
//  Created by Riyas Abdul Rahman on 4/30/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SendingDetailViewController.h"
#import "SendingsVC.h"
#import "UIImageView+AFNetworking.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "TabbarController/AddressViewCell.h"
#import "NewOrderVC.h"
#import "LPPhotoViewer.h"

@interface SendingDetailViewController () {
    NSArray *addressArray;
    BOOL changeTitlestatus;
    BOOL changeDescriptionstatus;
}

@end

@implementation SendingDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_tableView registerNib:[UINib nibWithNibName:@"AddressViewCell" bundle:nil] forCellReuseIdentifier:@"addressCell"];
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void) viewWillAppear:(BOOL)animated {
//    [super viewWillAppear: animated];
//    [self prepareUI];
//}

- (void)viewDidAppear:(BOOL)animated {
    
    IQKeyboardManager.sharedManager.enable = YES;
    
    //Add observer
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshOrderDetails:)
                                                 name:REFRESH_ORDER_DETAILS object:nil];
    
    [AppDelegate sharedAppDelegate].isNeedToChatDetails = 1;
    
    if([AppDelegate sharedAppDelegate].isNeedToRefreshPackDetails == 1 && ![self.packDetails.packStatus isEqualToString:@"90"]) {
        [self requestForGetPackDetails];
        [AppDelegate sharedAppDelegate].isNeedToRefreshPackDetails = 0;
    }
}

- (void)refreshOrderDetails:(NSNotification *)notification {
    
    NSNumber* packageId = 0;
    if ([notification.name isEqualToString:REFRESH_ORDER_DETAILS]) {
        NSDictionary* userInfo = notification.userInfo;
        packageId = (NSNumber*)userInfo[@"package_id"];
        NSLog (@"Successfully received test notification! %i", packageId.intValue);
    }
    
    //Refresh ui only when push message received for this particular package
    if(self.packDetails != nil &&
       packageId != 0 &&
       packageId.intValue == [self.packDetails.internalBaseClassIdentifier intValue]) {
        if([self.packDetails.packStatus isEqualToString:@"100"]) {
            //if package is cancelled by sender, thenwe dont want to refresh the pack details again.
            // this is because the sender cancelled the app and alert is shown on successful cancelling
            //on clikcing alert we navigate to a different page. so if pack details are refreshed
            //there is a possibility that activity indicator will be shown and we navigate from that screen
            //in this case activity indicator will remain on screen without hiding
            return;
        }
        [self requestForGetPackDetails];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [AppDelegate sharedAppDelegate].isNeedToChatDetails = 0;
}

- (void)viewDidLayoutSubviews{
    
}
- (void)loadSenderProfileImage:(NSString *)selfImage {
    //Shows indicatore on imageView
    [_courierProfilePic setShowActivityIndicatorView:YES];
    [_courierProfilePic setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, selfImage];
    
    //Loads image
    
//    [_courierProfilePic sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEncodingWithAllowedCharacters:NSCharacterSet.URLPathAllowedCharacterSet]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    [_courierProfilePic sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    [_courierProfilePic layoutIfNeeded];
}

#pragma mark
#pragma mark --Helpers
- (void)prepareUI {
    _btnOverView = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnOverView withRadius:_btnOverView.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    _btnStatus = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnStatus withRadius:_btnStatus.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    _courierProfilePic = (UIImageView *)[UtilHelper makeImageRounded:_courierProfilePic];
    _btnRateUser = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnRateUser withRadius:_btnRateUser.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    _btnCancelOrder = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnCancelOrder withRadius:_btnCancelOrder.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    _selectedReasonIndex = 0;
    _moreButtonHeightConstraint.constant = 0;
    _btnMore.hidden = true;
    
    [_btnMore setTitle:@"More" forState:UIControlStateNormal];
    [_btnMore setTitle:@"Less" forState:UIControlStateSelected];
    [_btnMore setTitleColor:[UIColor colorWithRed:255.0f/255.0f green:141.0f/255.0f blue:61.0f/255.0f alpha:1.0f] forState:UIControlStateSelected];
    
    _starRatingView.minimumValue = 0;
    _starRatingView.maximumValue = 5;
    
    self.successfullyDeliveredView.hidden = YES;
    [self requestForGetPackDetails];
    [self initializeMapView];
    
    if ([AppDelegate sharedAppDelegate].allDictData.count > 0) {
        
        NSPredicate *senderRepResPred = [NSPredicate predicateWithFormat:@"dicShort contains[c] %@", @"s_report_reasons"];
        NSMutableArray *petTypes = [[[AppDelegate sharedAppDelegate].allDictData filteredArrayUsingPredicate:senderRepResPred] mutableCopy];
        _pickerOptionsList = petTypes;
    }
    
    CSUser *userPack, *userDeli;
    NSMutableArray *markerArray = [NSMutableArray new];
    
    userPack = [CSUser new];
    userDeli = [CSUser new];
    
    userPack.userId = 0;
    userPack.imageName = @"pick-location-marker";
    userPack.ovalImage = @"OvalPick";
    userPack.latitude = [_packDetails.packFromLat doubleValue];
    userPack.longitude = [_packDetails.packFromLng doubleValue];
    userPack.address = _packDetails.packFromText;
    
    userDeli.userId = 1;
    userDeli.imageName = @"delivered-location-marker";
    userDeli.ovalImage = @"OvalDest";
    userDeli.latitude =  [_packDetails.packToLat doubleValue];
    userDeli.longitude = [_packDetails.packToLng doubleValue];
    userDeli.address = _packDetails.packToText;
    
    userPack.marker = [_googleMapView addMapPinForUser:userPack];
    userDeli.marker = [_googleMapView addMapPinForUser:userDeli];
    
//    _distance = _draftdata.distance;
//    _distanceTime = _draftdata.distanceTime;
    
//    Removing and adding new users in userInfoArray
    [markerArray addObject:userPack];
    [markerArray addObject:userDeli];
    
    if (userDeli.marker != nil && userPack.marker != nil) {
//        [self drawRoute];
//        [self calculateDistanceAndTimeBetweenLocations];
    }
    
    //GMSCoordinateBounds represents a rectangular bounding box on the Earth's surface.
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    for (CSUser *user in markerArray) {
        //Returns a GMSCoordinateBounds representing the current bounds extended to include the passed-in coordinate
        if((user.latitude != 0 && user.longitude != 0))
            bounds = [bounds includingCoordinate:user.marker.position];
    }
    
//    bounds = [bounds includingCoordinate:CLLocationCoordinate2DMake(userPack.marker.position.latitude, userPack.marker.position.longitude)];
//    bounds = [bounds includingCoordinate:CLLocationCoordinate2DMake(userDeli.marker.position.latitude, userDeli.marker.position.longitude)];
    
    _googleMapView.padding = UIEdgeInsetsMake(20, 0, 0, 0);
    [_googleMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
//    [_googleMapView showAllMarkersInMapViewWithBound:bounds];
    [self drawRoute];
    
}

- (void)initializeMapView
{
    //Assigning delegates
    self.googleMapView.delegate = self;
    [self.googleMapView initWithCustomStyle];
}

#pragma mark
#pragma mark --IBActions

- (IBAction)backAction:(id)sender {
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[UITabBarController class]]) {
            
            UITabBarController *tabbar = (UITabBarController *)aViewController;
            NSString *restorIdentity = tabbar.restorationIdentifier;
            
            if ([restorIdentity isEqualToString:@"sender"]) {
                [self.navigationController popToViewController:aViewController animated:YES];
                tabbar.selectedIndex = 0;
            }
        }
    }
}

- (IBAction)statusAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showStatusView:_packDetails];
}

- (IBAction)showBidsAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showBidsView];
}

- (IBAction)chatAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController ShowChatDetailView:_packDetails.acceptedCourierId PackDetails:_packDetails];
}

- (IBAction)callCourierAction:(UIButton *)sender {
    NSString *cleanedString;
    if (sender.tag == 0) {
        cleanedString = [[_packDetails.receiverPhoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    } else {
        cleanedString = [[_packDetails.acceptedCourierCell componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    }
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [WebServicesClient AddCallHistory:self->_packDetails.packOwnerId
                                    ReciverId:self->_packDetails.acceptedCourierId
                                     CallDate:[NSString stringWithFormat:@"%@", [NSDate date]]
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                                   SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                            completionHandler:^(BOOL isCallAdded, NSError *error) {
                            }];
        });
    }
}

- (IBAction)callReceiverAction:(UIButton *)sender {
    NSString *cleanedString;
    cleanedString = [[_packDetails.receiverPhoneNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [WebServicesClient AddCallHistory:self->_packDetails.packOwnerId
                                    ReciverId:self->_packDetails.receiverPhoneNumber
                                     CallDate:[NSString stringWithFormat:@"%@", [NSDate date]]
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                                   SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                            completionHandler:^(BOOL isCallAdded, NSError *error) {
                            }];
        });
    }
}


- (IBAction)trackCourierAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showViewOnMap:@"track" PackDetails:_packDetails isComingFromCourierApp:NO];
}

- (IBAction)cancelButtonBeforeCourierAction:(id)sender {
    if (_packDetails.acceptedCourierId != nil){
        if ([_packDetails.packStatus isEqualToString:@"30"]) {
            self.transparentView.hidden = NO;
            [self.containerView bringSubviewToFront:self.transparentView];
            [self.containerView bringSubviewToFront:self.reportView];
            _reportViewBottomConstraint.constant = 0;
            [self animateOnViewUpdated];
        } else {
            [UtilHelper showApplicationAlertWithMessage:@"A courier has been selected for your order. if canceled, you will be charges $7. Continue?"
                                               withDelegate:self
                                                    withTag:1
                                          otherButtonTitles:@[@"No", @"Yes"]];
        }
    }
    else {
        if ([_packDetails.packStatus isEqualToString:@"5"]) {
            [UtilHelper showApplicationAlertWithMessage:@"Are you sure you want to cancel the order?"
                                           withDelegate:self
                                                withTag:1
                                      otherButtonTitles:@[@"No", @"Yes"]];
        } else {
            [UtilHelper showApplicationAlertWithMessage:@"Are you sure you want to cancel the order? Cancelation fee is $7."
                                           withDelegate:self
                                                withTag:1
                                      otherButtonTitles:@[@"No", @"Yes"]];
        }
        
    }
    
}

- (IBAction)closeReportView:(UIButton *)sender {
    self.reportViewBottomConstraint.constant = -380;
    [self animateOnViewUpdated];
    self.transparentView.hidden = YES;
}

- (IBAction)reportDeliveryAction:(UIButton *)sender {
    [UtilHelper showApplicationAlertWithMessage:@"Are you sure you would like to report this delivery and cancel it?"
                                   withDelegate:self
                                        withTag:88
                              otherButtonTitles:@[@"No", @"Yes"]];
    
}

//Report Problem
- (void)requestForCancelOrder
{
    [self showProgressHud];
    
    GetDictByName *dictName = _pickerOptionsList[self.selectedReasonIndex];
    
    [WebServicesClient SenderReportProblem:_packDetails.internalBaseClassIdentifier
                                 ReasonText:_commentTextView.text
                                   ReasonId:dictName.listId
                                LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                               LogUserAgent:[[UIDevice currentDevice] systemVersion]
                              LogDeviceType:LOG_DEVICE_TYPE
                                 SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                          completionHandler:^(BOOL isPackCancelled, NSError *error) {
                              
                              [self hideProgressHud];
                              
                              if (!error) {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:@"Cancelled successfully." withDelegate:self withTag:100 otherButtonTitles:@[@"Ok"]];
                                  [[NSNotificationCenter defaultCenter] postNotificationName:COURIER_ACTIVE_LIST_UPDATED object:@{}];
                              }
                              else if (error.code == 3) {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                  
                              } else {
                                  
                                  [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                              }
                          }];
}



- (IBAction)viewOnMapAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showViewOnMap:@"track" PackDetails:_packDetails isComingFromCourierApp:NO];
}

- (IBAction)courierTappedAction:(id)sender {
    if (_packDetails.acceptedCourierId != nil)
        [[AppDelegate sharedAppDelegate].appController showCourierProfileView:_packDetails CourierId:nil isComingFrom:NO];
}

- (IBAction)rateUserTap:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showRateOrderView:_packDetails];
}
- (IBAction)reasonAction:(UIButton *)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select reason" message:@"" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.delegate = self;
    UIPickerView *picker = [[UIPickerView alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
    alert.tag = 1;
    picker.delegate = self;
    [picker selectRow:_selectedReasonIndex inComponent:0 animated:YES];
    [alert addSubview:picker];
    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
    [alert setValue:picker forKey:@"accessoryView"];
    [alert show];
}

- (IBAction)changeTitle:(UIButton *)sender {
    changeTitlestatus = !changeTitlestatus;
    if (changeTitlestatus) {
        _SendingTextfield.text = _sendingLabel.text;
        _SendingTextfield.hidden = false;
        [_SendingTextfield becomeFirstResponder];
        [_ChangeBtn1 setImage:[UIImage imageNamed:@"accept_small"] forState:UIControlStateNormal];
    }
    else {
        _sendingLabel.text = _SendingTextfield.text;
        _SendingTextfield.hidden = true;
        [_SendingTextfield resignFirstResponder];
        [self requestChangeTitle];
        [_ChangeBtn1 setImage:[UIImage imageNamed:@"EditPick"] forState:UIControlStateNormal];
    }
    
}
- (IBAction)changeDescription:(UIButton *)sender {
    changeDescriptionstatus = !changeDescriptionstatus;
    if (changeDescriptionstatus) {
        _DescriptionTextfield.text = _descriptionLabel.text;
        _DescriptionTextfield.hidden = false;
        [_DescriptionTextfield becomeFirstResponder];
        [_ChangeBtn2 setImage:[UIImage imageNamed:@"accept_small"] forState:UIControlStateNormal];
    }
    else {
        _descriptionLabel.text = _DescriptionTextfield.text;
        _DescriptionTextfield.hidden = true;
        [_DescriptionTextfield resignFirstResponder];
        [self requestChangeDescription];
        [_ChangeBtn2 setImage:[UIImage imageNamed:@"EditPick"] forState:UIControlStateNormal];
    }
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return self.pickerOptionsList.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    GetDictByName *dictName = _pickerOptionsList[row];
    return dictName.listName;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    self.selectedReasonIndex = row;
    GetDictByName *dictName = _pickerOptionsList[row];
    _lblReason.text = dictName.listName;
    _lblReason.textColor = [UIColor blackColor];
}

#pragma mark - TableView Delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    _addressTableHeightConstraint.constant = 44 * addressArray.count;
    return addressArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addressCell" forIndexPath:indexPath];
    
    cell.btnEdit.hidden = true;
    cell.btnRemove.hidden = true;
    cell.btnEdit.frame = CGRectMake(cell.btnEdit.frame.origin.x, cell.btnEdit.frame.origin.y, 0, cell.btnEdit.frame.size.height);
    cell.btnRemove.frame = CGRectMake(cell.btnRemove.frame.origin.x, cell.btnRemove.frame.origin.y, 0, cell.btnRemove.frame.size.height);
    [cell layoutIfNeeded];
    
    cell.topDotsImageView.hidden = indexPath.row == 0;
    cell.botDotsImageView.hidden = indexPath.row == addressArray.count - 1;
    cell.lblSeparator.hidden = indexPath.row == addressArray.count - 1;
    cell.lblAddress.text = addressArray[indexPath.row];
    [cell.ovalImageView setImage:[UIImage imageNamed:indexPath.row == 0 ? @"OvalPick" : @"OvalDest"]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 1) {
        switch (buttonIndex) {
            case 0:
                break;
            case 1:
                [self requestForCancelPack];
                break;
            default:
                break;
        }
    } else if(alertView.tag == 2){
        NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
        
        BOOL isControllerIsThere = NO;
        for (UIViewController *aViewController in allViewControllers) {
            if ([aViewController isKindOfClass:[UITabBarController class]]) {
                
                UITabBarController *tabbar = (UITabBarController *)aViewController;
                NSString *restorIdentity = tabbar.restorationIdentifier;
                if ([restorIdentity isEqualToString:@"sender"]) {
                    isControllerIsThere = YES;
                    [self.navigationController popToViewController:aViewController animated:YES];
                    tabbar.selectedIndex = 0;
                }
            }
        }
        if (!isControllerIsThere)
            [self.navigationController popViewControllerAnimated:YES];
        else {
        }

        
 //commented  by Riyas sep 10
//        BOOL isControllerIsThere = NO;
//        for (UIViewController *vc in self.navigationController.viewControllers) {
//            if ([vc isKindOfClass:[UITabBarController class]]) {
//                
//                isControllerIsThere = YES;
//                [self.navigationController popToViewController:vc animated:NO];
//            }
//        }        if (!isControllerIsThere) {
//            [self.navigationController popViewControllerAnimated:YES];
//        } else {
//        
//        }
    }
    else if( alertView.tag == 11)
        [self.navigationController popViewControllerAnimated:YES];
    else if(alertView.tag == 1001)
        [self senderLogout];
    else if(alertView.tag == 88)
    {
        switch (buttonIndex) {
            case 0:
                break;
            case 1:
                [self requestForCancelOrder];
                break;
            default:
                break;
        }
    }
    else if(alertView.tag == 100){
        self.reportViewBottomConstraint.constant = -380;
        [self animateOnViewUpdated];
        self.transparentView.hidden = YES;
        [self prepareUI];
    }
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestForGetPackDetails {
    
    if (![UtilHelper isNetworkAvailable]) {
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    [self showProgressHud];    
    [WebServicesClient GetPackDetails:_packId
                        IncludeImages:@"1"
                       IncludeBidders:@"1"
                                Start:@"0"
                                Count:@"0"
                        LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                          LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                         LogUserAgent:[[UIDevice currentDevice] systemVersion]
                        LogDeviceType:LOG_DEVICE_TYPE
                    completionHandler:^(PackDetailsModel *packDetails, NSError *error) {
                        
                        [self hideProgressHud];
                        if (!error)
                            [self populateFields:packDetails];
                        else if (error.code == 3)
                            [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                        else
                            [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                    }];
}
- (void)requestChangeTitle {
    if (![UtilHelper isNetworkAvailable]) {
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    [self showProgressHud];
    [WebServicesClient UpdatePack_SendingName:_packId
                                    Send_Name:_sendingLabel.text
                                LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                            completionHandler:^(BOOL result, NSError *error)
 {
                        
                        [self hideProgressHud];
                        if (!error){
                            if (result) {
                                [UtilHelper showApplicationAlertWithMessage:@"Your information has been successfully updated" withDelegate:self withTag:1234 otherButtonTitles:@[@"OK"]];
                            }
                        }
                        else if (error.code == 3)
                            [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                        else
                            [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                    }];
}

- (void)requestChangeDescription {
    if (![UtilHelper isNetworkAvailable]) {
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    [self showProgressHud];
    [WebServicesClient UpdatePack_Description:_packId
                                    Desc_Text:_descriptionLabel.text
                                LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                            completionHandler:^(BOOL result, NSError *error)
     {
         
         [self hideProgressHud];
         if (!error){
             if (result) {
                 [UtilHelper showApplicationAlertWithMessage:@"Your information updated successfully" withDelegate:self withTag:1234 otherButtonTitles:@[@"OK"]];
             }
         }
         else if (error.code == 3)
             [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
         else
             [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
     }];
}

- (void)requestForCancelPack {
    if (![UtilHelper isNetworkAvailable]) {
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    [self showProgressHud];
    
    [WebServicesClient CancelPack:_packId
                      LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                     LogUserAgent:[[UIDevice currentDevice] systemVersion]
                    LogDeviceType:LOG_DEVICE_TYPE
                       SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                completionHandler:^(BOOL isPackCancelled, NSError *error) {
                    
                    [self hideProgressHud];
                    
                    if (!error) {
                        [UtilHelper showApplicationAlertWithMessage:@"Order has been canceled" withDelegate:self withTag:2 otherButtonTitles:@[@"Ok"]];
                        
                        [[NSNotificationCenter defaultCenter] postNotificationName:SENDER_ACTIVE_LIST_UPDATED object:@{}];
                        
                    } else if (error.code == 3)
                        [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                   else
                        [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                }];
}

- (void)populateFields:(PackDetailsModel *)packDetails {
    
    _detailHeightConstraint.constant = 710;
    
    _packDetails = packDetails;
    _sendingLabel.text = packDetails.sendName;
    _priceLabel.text = [NSString stringWithFormat:@"$%@", packDetails.packPrice];
    _descriptionLabel.text = packDetails.descText;
    _orderIdLabel.text = [NSString stringWithFormat:@"%@%@", @"Order id: ", packDetails.insertedBy];
    
    // Set Icon
    if ([_packDetails.sendCatId isEqualToString:@"2"] || [_packDetails.sendCatId isEqual:@2]) {
        _icon.image = [UIImage imageNamed:@"pack-icon"];
        
    } else if ([_packDetails.sendCatId isEqualToString:@"1"] || [_packDetails.sendCatId isEqual:@1]) {
        _icon.image = [UIImage imageNamed:@"ups-status"];
        
    } else if ([_packDetails.sendCatId isEqualToString:@"3"] || [_packDetails.sendCatId isEqual:@3]) {
        
        _icon.image = [UIImage imageNamed:@"Store-status"];
        
    }else if ([_packDetails.sendCatId isEqualToString:@"4"] || [_packDetails.sendCatId isEqual:@4]) {
        
        _icon.image = [UIImage imageNamed:@"return-services"];
    }
    
    addressArray = @[packDetails.packFromText, packDetails.packToText];
    
    [_tableView reloadData];
    
    int size = [packDetails.packSizeId intValue];
    if (size == 1) {
        _smallSizeLabel.textColor = ORANGE_LINE_COLOR;
        _smallSizeLabel.alpha = 1;
    } else if (size == 2) {
        _mediumSizeLabel.textColor = ORANGE_LINE_COLOR;
        _mediumSizeLabel.alpha = 1;
    } else if (size == 3){
        _largeSizeLabel.textColor = ORANGE_LINE_COLOR;
        _largeSizeLabel.alpha = 1;
    }
    
    _packageStutusLabel.text = packDetails.statusSender;
    _receiverNameLabel.text = packDetails.receiverName;
    _receiverEmailLabel.text = packDetails.packOwnerEmail;
    _receiverPhoneLabel.text = packDetails.receiverPhoneNumber;
    _courierNameLabel.text = [NSString stringWithFormat:@"%@ %@", packDetails.acceptedCourierName, packDetails.acceptedCourierSurname];
    
    
    _receiverProfileView.hidden = _receiverNameLabel.text == nil;
    _courierProfileView.hidden = _courierNameLabel.text == nil;
    
    _receiverHeightConstraint.constant = _receiverProfileView.hidden ? 0 : 80;
    _courierHeightConstraint.constant = _courierProfileView.hidden ? 0 : 100;
    
    _orderIdLabel.text = [NSString stringWithFormat:@"Order is: %@", packDetails.internalBaseClassIdentifier];
    _milesLabel.text = [NSString stringWithFormat:@"%@ miles", packDetails.distance];
    _itemValueLabel.text = [NSString stringWithFormat:@"$%@", packDetails.itemValue];
    
    _starRatingView.value = [packDetails.acceptedCourierRate floatValue];
    [self loadSenderProfileImage:_packDetails.acceptedCourierSelfImg];
    
    NSDate *takenDate = [NSDate dateFromDateString:packDetails.takenTime];
    NSDate *deliveryDate = [NSDate dateFromDateString:packDetails.deliveryTime];
    NSDate *taken = [NSDate dateFromDateString:packDetails.takenTime];
    NSString *localtime = [taken getDateTimeInLocalTimeZone];
    NSDate *localdate = [NSDate dateFromDateString:localtime];
//    _takenTime.text = [localdate getTimeFromDateWithoutDay];
    
    
    if ([packDetails.takenAsap isEqualToString:@"1"]) {
//        _takenDateLabel.text = @"Today";
//        _takenTime.text = @"Urgent";
//        _takenTime.textColor = YELLOW_COLOR;
    } else {
        _takenTime.textColor = [UIColor blackColor];
        NSString *timeString = [localdate getTimeFromDateWithoutDay];
        _takenDateLabel.text = [localdate getTimeDayFromDate];
        
        //Commented on Oct 7, 2017
//        _takenTime.text = [NSString stringWithFormat:@"On %@", timeString];
        _takenTime.text = timeString;
        //Commented on oct 7,2017
//        if ([packDetails.takenType isEqualToString:@"1"]) {
//            
//            _takenTime.text = [NSString stringWithFormat:@"Till %@", timeString];
//        }
    }
//    NSDate *date = [NSDate dateFromDateString:_packDetails.takenTime];
//    NSString *localtime = [date getDateTimeInLocalTimeZone];
//    NSDate *localdate = [NSDate dateFromDateString:localtime];
//    _lblDay1.text = [date getMMDDYYYYDate];
//    _lblTime1.text = [date getTimeFromDateWithoutDay];
//    _date.text = packDetails.takenTime;
    _date.text = localtime;
    
    if ([packDetails.deliveryAsap isEqualToString:@"1"]) {
        _deliveryDate.text = @"Today";
        _deliveryTime.text = @"Urgent";
        _deliveryTime.textColor = YELLOW_COLOR;
    } else {
        _deliveryTime.textColor = [UIColor blackColor];
        NSString *timeString = [deliveryDate getTimeFromDateWithoutDay];
        _deliveryDate.text = [deliveryDate getTimeDayFromDate];
        _deliveryTime.text = [NSString stringWithFormat:@"%@", timeString];
    }
    
    [self initDescriptionLabel];
    
    if ([packDetails.packStatus isEqual:@20])
        _courierProfileView.hidden = NO;
    
    //Show PackImages
    if (packDetails.packImages.count == 0) {
        self.ImagesView.hidden = YES;
        _detailHeightConstraint.constant -= 80;
    } else {
        self.ImagesView.hidden = NO;
        for (int i = 0; i < packDetails.packImages.count; i ++) {
            ((UIImageView *)self.PicArray[i]).hidden = NO;
            NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, ((PackImages *)packDetails.packImages[i]).packImg];
            
            //Loads image
            [((UIImageView *)self.PicArray[i]) sd_setImageWithURL:[NSURL URLWithString:finalUrl] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
        }
    }
    
    // Paid pack
    //Pack Status color
    if ([packDetails.packStatus isEqualToString:@"5"]){
        _detailHeightConstraint.constant -= 120;
        _courierHeightConstraint.constant = 0;
        self.courierProfileView.hidden = YES;
        [self.view layoutIfNeeded];
    }
    else if ([packDetails.packStatus isEqualToString:@"10"]){
        self.courierProfileView.hidden = YES;
        _courierHeightConstraint.constant = 0;
//        _containerHeightConstraint.constant -= 100;
        _detailHeightConstraint.constant -= 120;
        _packageStutusLabel.textColor = ORANGE_LINE_COLOR;
        _ChangeBtn1.hidden = false;
        _ChangeBtn2.hidden = false;
        [self.view layoutIfNeeded];
    }
    //Courier selected for the package
    else if ([packDetails.packStatus isEqualToString:@"20"]) {
        self.courierProfileView.hidden = NO;
        _packageStutusLabel.textColor = PACK_GREEN_STATUS_COLOR;
        
//        _ChangeBtn1.hidden = false;
//        _ChangeBtn2.hidden = false;
    }
    //Courier pickedup the package from sender. Pack given
    else if ([packDetails.packStatus isEqualToString:@"30"]) {
        self.courierProfileView.hidden = NO;
        _packageStutusLabel.textColor = PACK_GREEN_STATUS_COLOR;
        [_btnCancelOrder setTitle:@"Report Courier" forState:UIControlStateNormal];
    }
    //Package is delivered
    else if ([packDetails.packStatus isEqualToString:@"40"]) {
        self.courierProfileView.hidden = NO;
        self.successfullyDeliveredView.hidden = NO;
        
        NSDate *dateInUTC = [NSDate dateFromDateString:_packDetails.lastOperationTime];
        NSString *dateInLocalTimeZone = [dateInUTC getDateTimeInLocalTimeZone];
        
        NSString *deliveredStatusWithTime = [[_packDetails.statusSender stringByAppendingString:@" at "]
                                             stringByAppendingString:dateInLocalTimeZone];
        
        self.successfullyDeliveredLabel.text = deliveredStatusWithTime;
        _packageStutusLabel.textColor = PACK_GREEN_STATUS_COLOR;
        _btnTrack.hidden = YES;
        _cancelOrderView.hidden = YES;
        if (![[NSUserDefaults standardUserDefaults] boolForKey:packDetails.internalBaseClassIdentifier]) {
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:packDetails.internalBaseClassIdentifier];
            if (!_fromReceiver) {
                [[AppDelegate sharedAppDelegate].appController showRateOrderView:_packDetails];
            }
        }
    }
    //Pack not given by sender
    else if ([packDetails.packStatus isEqualToString:@"50"]) {
        self.courierProfileView.hidden = NO;
        self.successfullyDeliveredView.hidden = NO;
        self.successfullyDeliveredLabel.text = _packDetails.statusSender;
        self.successfullyDeliveredLabel.textColor = PACK_RED_STATUS_COLOR;
        _packageStutusLabel.textColor = ORANGE_LINE_COLOR;
        _cancelOrderView.hidden = YES;
        _btnRateUser.hidden = YES;
        _successViewHeightConstraint.constant = 35;
    }
    else if ([packDetails.packStatus isEqualToString:@"30"]) {
        self.courierProfileView.hidden = NO;
        self.successfullyDeliveredView.hidden = NO;
        self.successfullyDeliveredLabel.text = _packDetails.statusSender;
        self.successfullyDeliveredLabel.textColor = PACK_RED_STATUS_COLOR;
        _packageStutusLabel.textColor = PACK_RED_STATUS_COLOR;
    }
    //90 is reported pack
    //100 is cancelled pack
    else if ([packDetails.packStatus isEqualToString:@"100"] || [packDetails.packStatus isEqualToString:@"90"]) {
        
        _detailHeightConstraint.constant -= 100;
        _courierHeightConstraint.constant = 0;
        self.courierProfileView.hidden = YES;
        _successViewHeightConstraint.constant = 35;
        _btnRateUser.hidden = true;
        self.successfullyDeliveredView.hidden = NO;
        [self.view layoutIfNeeded];
        self.successfullyDeliveredLabel.text = _packDetails.statusSender;
        self.successfullyDeliveredLabel.textColor = PACK_RED_STATUS_COLOR;
        _packageStutusLabel.textColor = PACK_RED_STATUS_COLOR;
        _btnTrack.hidden = YES;
        _cancelOrderView.hidden = YES;
        
        //If no courier name is obtained, which means order has been cancelled before a courier accepted the order
        //then hide chat and call icons along with star rating
        if([_courierNameLabel.text isEqualToString:EMPTY_STRING] || _courierNameLabel.text == nil) {
            _courierProfileView.hidden = YES;
//            _courierNameLabel.text = @"No courier assigned";
//            _starRatingView.hidden = YES;
//            _chatBtn.hidden = YES;
//            _callBtn.hidden = YES;
        }
    }
    
    if (_descriptionLabel.text.length < 1) {
        _descriptionHeightConstraint.constant = 0;
    }
    
    if (_fromReceiver) {
//        self.successfullyDeliveredView.hidden = NO;
        _cancelOrderView.hidden = YES;
        _btnRateUser.hidden = true;
        _successViewHeightConstraint.constant = 35;
        [self.view layoutIfNeeded];
    }
    _detailHeightConstraint.constant += (_sendingLabel.frame.size.height - 19.5);
    
    CSUser *userPack, *userDeli;
    NSMutableArray *markerArray = [NSMutableArray new];
    
    userPack = [CSUser new];
    userDeli = [CSUser new];
    
    userPack.userId = 0;
    userPack.imageName = @"pick-location-marker";
    userPack.ovalImage = @"OvalPick";
    userPack.latitude = [_packDetails.packFromLat doubleValue];
    userPack.longitude = [_packDetails.packFromLng doubleValue];
    userPack.address = _packDetails.packFromText;
    
    userDeli.userId = 1;
    userDeli.imageName = @"delivered-location-marker";
    userDeli.ovalImage = @"OvalDest";
    userDeli.latitude =  [_packDetails.packToLat doubleValue];
    userDeli.longitude = [_packDetails.packToLng doubleValue];
    userDeli.address = _packDetails.packToText;
    
    userPack.marker.map = nil;
    userDeli.marker.map = nil;
    
    userPack.marker = [_googleMapView addMapPinForUser:userPack];
    userDeli.marker = [_googleMapView addMapPinForUser:userDeli];
    
    //    Removing and adding new users in userInfoArray
        [markerArray addObject:userPack];
        [markerArray addObject:userDeli];

    if (userDeli.marker != nil && userPack.marker != nil) {
                [self drawRoute];
//                [self calculateDistanceAndTimeBetweenLocations];
    }
    
    //GMSCoordinateBounds represents a rectangular bounding box on the Earth's surface.
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
        for (CSUser *user in markerArray) {
            //Returns a GMSCoordinateBounds representing the current bounds extended to include the passed-in coordinate
            if((user.latitude != 0 && user.longitude != 0))
                bounds = [bounds includingCoordinate:user.marker.position];
        }
    
    _googleMapView.padding = UIEdgeInsetsMake(20, 0, 2, 0);
    [_googleMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds]];
//    [_googleMapView showAllMarkersInMapViewWithBound:bounds];
//    [_googleMapView animateToZoom:9];
    [self drawRoute];
}

- (void)drawRoute {
    CLLocation *userPack = [[CLLocation alloc] initWithLatitude:[_packDetails.packFromLat doubleValue] longitude:[_packDetails.packFromLng doubleValue]];
    
    CLLocation *deliveryPack = [[CLLocation alloc] initWithLatitude:[_packDetails.packToLat doubleValue] longitude:[_packDetails.packToLng doubleValue]];
    
    
    [self.googleMapView fetchPolylineWithOrigin:userPack color:ORANGE_LINE_COLOR destination:deliveryPack completionHandler:^(GMSPolyline *polyline)
    {
        if(polyline)
        {
            polyline.map = nil;
            polyline.map = self.googleMapView;
        }
    }];
}

-(void)initDescriptionLabel {
    if (_descriptionLabel.text == nil) {
        _descriptionLabel.text = @"";
    }
    NSInteger lengthForVisibleString = [UtilHelper fitString:_descriptionLabel.text intoLabel:_descriptionLabel];
    NSInteger lengthForString = _descriptionLabel.text.length;
    
    if (lengthForString > lengthForVisibleString) {
        NSMutableString *mutableString = [[NSMutableString alloc] initWithString:_descriptionLabel.text];
        NSString *trimmedString = [mutableString stringByReplacingCharactersInRange:NSMakeRange(lengthForVisibleString, (_descriptionLabel.text.length - lengthForVisibleString)) withString:@""];
        NSMutableAttributedString *answerAttributed = [[NSMutableAttributedString alloc] initWithString:trimmedString attributes:@{ NSFontAttributeName : _descriptionLabel.font}];
        _descriptionLabel.attributedText = answerAttributed;
        _btnMore.hidden = false;
        _moreButtonHeightConstraint.constant = 25;
        _detailHeightConstraint.constant += 25;
    } else {
        _btnMore.hidden = true;
        _moreButtonHeightConstraint.constant = 0;
    }
}

- (IBAction)moreAction:(id)sender {
    _btnMore.selected = !_btnMore.selected;
    if (_btnMore.selected) {
        _descriptionLabel.text = _packDetails.descText;
        [self getHeightDescLabel];
    } else {
        _descriptionLabel.text = EMPTY_STRING;
        _descriptionHeightConstraint.constant = 47;
        _detailHeightConstraint.constant -= _heightForDesc;
        
        [self animateOnViewUpdated];
        [self performSelector:@selector(showMoreText) withObject:nil afterDelay:0.3];
    }
    _btnMore.hidden = false;
    _moreButtonHeightConstraint.constant = 25;
}

- (void)getHeightDescLabel {
    CGSize maximumLabelSize = CGSizeMake(296, FLT_MAX);
    CGSize expectedLabelSize = [_packDetails.descText sizeWithFont:_descriptionLabel.font constrainedToSize:maximumLabelSize lineBreakMode:_descriptionLabel.lineBreakMode];
    
    _heightForDesc = expectedLabelSize.height-35;
    CGRect labelfram = _descriptionLabel.frame;
    _descriptionLabel.frame = CGRectMake(labelfram.origin.x, labelfram.origin.y, labelfram.size.width, expectedLabelSize.height);
    _descriptionHeightConstraint.constant += _heightForDesc;
    _detailHeightConstraint.constant += _heightForDesc;
    [self animateOnViewUpdated];
}

- (void)showMoreText {
    _descriptionLabel.text = _packDetails.descText;
    [self initDescriptionLabel];
}

- (void)animateOnViewUpdated {
    [UIView animateWithDuration:1.0 animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished){}];
}
- (IBAction)imageTapped:(UITapGestureRecognizer *)sender {
    NSMutableArray *imgArr = [NSMutableArray new];
    
    for (int i = 0; i < _packDetails.packImages.count; i ++) {
        NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, ((PackImages *)_packDetails.packImages[i]).packImg];
        [imgArr addObject:finalUrl];
    }
    
    LPPhotoViewer *pvc = [[LPPhotoViewer alloc] init];
    pvc.imgArr = imgArr;
    pvc.currentIndex = 0;
    [self presentViewController:pvc animated:YES completion:nil];
}

@end
