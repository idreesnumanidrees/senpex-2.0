//
//  SignUpIDCardVC.m
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SignUpIDCardVC.h"
#import "PlacesViewController.h"
#import "LocationManagerService.h"

@interface SignUpIDCardVC ()<LocationManagerServiceDelegate, UIScrollViewDelegate> {
    
    NSUInteger currentTagValue;
}
//Outlets

@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
@property (strong, nonatomic) IBOutlet UILabel *stepLabel;

@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *nameTextField;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *addressTextField;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *currAddressTextField;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *cityTextField;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *idCardTextField;
@property (strong, nonatomic) IBOutlet JVFloatLabeledTextField *expPeriodTextField;

@property (strong, nonatomic) UserModel *user;

@property (strong, nonatomic) NSString *deliveryDateTimeForWeb;

//LocationManagerService instance variable
@property (strong, nonatomic) LocationManagerService *locationManagerService;

//Validation related
@property (nonatomic, strong) NSArray *validationArray;
@property (nonatomic, assign) NSInteger genderSelection;

//verion1.5
@property (weak, nonatomic) IBOutlet UIScrollView *outerScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *innerScrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIImageView *cameraIcon;
@property (weak, nonatomic) IBOutlet UIButton *addPicButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteImageButton;

@property (strong, nonatomic) NSMutableArray *imagesArray;
@property (weak, nonatomic) IBOutlet UIButton *laterButton;

//Constraint
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nextButtonTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addPicTopConstraint;

@end

@implementation SignUpIDCardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_locationManagerService stopUpdatingLocation];
}

#pragma mark
#pragma mark --Helpers

- (void)prepareUI
{
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;

    self.btnRegister = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnRegister withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    
    self.btnMale = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnMale withRadius:13.0 color:nil width:0];
    self.btnFemale = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnFemale withRadius:13.0 color:nil width:0];

    //Web Related
    self.btnRegister.selected = YES;
    _genderSelection = 0;
    
    self.validationArray = @[_nameTextField,_addressTextField,_cityTextField,_idCardTextField,_expPeriodTextField];
    
    if (_isComingFromProfile) {
        
        _laterButton.hidden = YES;
        _stepLabel.hidden = YES;
        [self.btnRegister setTitle:@"Update" forState:UIControlStateNormal];
        _headingTopConstraint.constant = 36;
        [self requestToLoginUserInfo];
    }
    else
    {
//        _innerViewTopConstraint.constant  = 0;
//        [self updateCountOfStepsCompletedByCourierInRegistration:@"1"];
    }
    
    [self initializeLocationManagerService];
    
    //verion1.5
    
    self.innerScrollView.delegate = self;
    _innerScrollView.pagingEnabled = YES;
    _innerScrollView.scrollEnabled = YES;
    self.innerScrollView.hidden = YES;
    self.pageControl.hidden = YES;
    _deleteImageButton.hidden = YES;
    self.nextButtonTopConstraint.constant = -30;
    _imagesArray = [NSMutableArray new];
}

#pragma mark
#pragma mark --Actions

- (IBAction)addPicButtonTapped:(id)sender {
    
    if (_imagesArray.count < 2) {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Choose Image Media" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library",@"Take Photo",nil];
    actionSheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    [actionSheet showInView:self.view];
    } else {
        [UtilHelper showApplicationAlertWithMessage:@"Sorry! You can not select more than 2 images" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
    }
}

- (IBAction)deleteImageButtonTapped:(id)sender {
    
    [_imagesArray removeObjectAtIndex:_pageControl.currentPage];
    [self.pageControl setNumberOfPages:_imagesArray.count];
    
    [self checkActiveButton:@"123"];

    for (UIView *v in _innerScrollView.subviews) {
        if ([v isKindOfClass:[UIImageView class]]) {
            [v removeFromSuperview];
        }
    }
    
    if (_imagesArray.count > 0)
    {
//        _innerViewTopConstraint.constant  = 20;
        int imagesOffSet = 0;
        for (int i = 0; i < [_imagesArray count]; i++) {
            //We'll create an imageView object in every 'page' of our scrollView.
            UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(imagesOffSet,0,241, 155)];
            img.layer.masksToBounds = YES;
            img.layer.cornerRadius = 7.0f;
            [self.innerScrollView addSubview:img];
            
            AddImageObject *imageObject = _imagesArray[i];
            if (imageObject.isUploaded == NO) {
                
                img.image = imageObject.image;
                
            } else {
                
                NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, imageObject.imageName];
                [img sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
                imageObject.image = img.image;
            }
            
            imagesOffSet+=241;
        }
        //Set the content size of our scrollview according to the total width of our imageView objects.
        _innerScrollView.contentSize = CGSizeMake(imagesOffSet, 155);
        
    } else {
        
        [_outerScrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
        self.innerScrollView.delegate = self;
        _innerScrollView.pagingEnabled = YES;
        _innerScrollView.scrollEnabled = YES;
        self.innerScrollView.hidden = YES;
        self.pageControl.hidden = YES;
        _deleteImageButton.hidden = YES;
        _cameraIcon.hidden = NO;
        _addPicTopConstraint.constant = 8;
        self.nextButtonTopConstraint.constant = -30;
//        _innerViewTopConstraint.constant  = 0;
    }
}

- (IBAction)useofficialAction:(UIButton *)sender {
    
    _currAddressTextField.text = _addressTextField.text;
    [self checkActiveButton:@"123"];
}

- (IBAction)genderAction:(UIButton *)sender {
    
    sender.selected = YES;
    sender.alpha = 1;
    sender.backgroundColor = [UIColor colorWithRed:17.0/255.0 green:167.0/255.0 blue:170.0/255.0 alpha:1.0f];
    
    if (sender.tag == 100) {
        
        _btnFemale.selected = NO;
        _btnFemale.backgroundColor = [UIColor clearColor];
        _genderSelection = 1;
        _btnFemale.alpha = 0.40;
        
    } else {
        
        _btnMale.selected = NO;
        _btnMale.backgroundColor = [UIColor clearColor];
        _btnMale.alpha = 0.40;
        _genderSelection = 2;
    }
    
    [self checkActiveButton:@"123"];
}

- (IBAction)laterBtnAction:(id)sender {
    [[AppDelegate sharedAppDelegate].appController showTakePhotoView:NO];
}

- (IBAction)loginAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)signUpAction:(id)sender {
    
    if (!_btnRegister.selected) {
        
        [self showProgressHud];
        [self requestToRegisterIDCardUser];
    }
}

#pragma
#pragma  mark action sheet delegate method

- (void)actionSheet:(UIActionSheet *)actionSheet willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 1) {
        [self pickMediaFromSource:UIImagePickerControllerSourceTypeCamera];
    }
    else if(buttonIndex==0)
    {
        [self pickMediaFromSource:UIImagePickerControllerSourceTypePhotoLibrary];
    }
}

#pragma
#pragma  mark camera delegate method
- (void)pickMediaFromSource:(UIImagePickerControllerSourceType)sourceType{
    @autoreleasepool {
        if([UIImagePickerController isSourceTypeAvailable:sourceType])
        {
            NSArray *mediaTypes=[UIImagePickerController availableMediaTypesForSourceType:sourceType];
            UIImagePickerController *picker=[[UIImagePickerController alloc]init];
            picker.mediaTypes=mediaTypes;
            picker.delegate=self;
            picker.allowsEditing=YES;
            //            picker.allowsEditing=NO;
            picker.sourceType=sourceType;
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else
        {
            [UtilHelper showApplicationAlertWithMessage:@"Camera is not found" withDelegate:self withTag:0 otherButtonTitles:@[@"OK"]];
        }
    }
}

#pragma mark -
#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    if([info[UIImagePickerControllerMediaType] isEqualToString:(__bridge NSString *)(kUTTypeImage)])
    {
        //        NSLog(@"Image");
        //image
        @autoreleasepool {
            UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
            
            AddImageObject *addImage = [AddImageObject new];
            addImage.image = chosenImage;
            addImage.isUploaded = NO;
            [_imagesArray addObject:addImage];
            [self addImages];
        }
    }
    else
    {
        //        NSLog(@"Video");
        //video
        [UtilHelper showApplicationAlertWithMessage:@"Video does not support." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
    }
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)addImages
{
    [self.pageControl setNumberOfPages:_imagesArray.count];
    [self checkActiveButton:@"123"];
    
    int imagesOffSet = 0;
    for (int i = 0; i < [_imagesArray count]; i++) {
        //We'll create an imageView object in every 'page' of our scrollView.
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(imagesOffSet,0,241, 155)];
        img.contentMode = UIViewContentModeScaleAspectFill;
        img.layer.masksToBounds = YES;
        img.layer.cornerRadius = 7.0f;
        [self.innerScrollView addSubview:img];
        
        AddImageObject *imageObject = _imagesArray[i];
        if (imageObject.isUploaded == NO) {
            
            img.image = imageObject.image;
            
        } else {
            
            [img setShowActivityIndicatorView:YES];
            [img setIndicatorStyle:UIActivityIndicatorViewStyleGray];
            NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, imageObject.imageName];
            [img sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRefreshCached];
        }
        imagesOffSet+=241;
    }
    
//    if (IS_IPHONE_X) {
//        _innerViewTopConstraint.constant  = 0;
//    }else{
//        _innerViewTopConstraint.constant  = 20;
//    }
    
    //Set the content size of our scrollview according to the total width of our imageView objects.
    _innerScrollView.contentSize = CGSizeMake(imagesOffSet, 155);
    _innerScrollView.hidden = NO;
    _cameraIcon.hidden = YES;
    _pageControl.hidden = NO;
    _deleteImageButton.hidden = NO;
    _addPicTopConstraint.constant = 107;
    _nextButtonTopConstraint.constant = 70;
    [_addPicButton setTitle:@"Add DL pictures" forState:UIControlStateNormal];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_innerScrollView setContentOffset:CGPointMake(_innerScrollView.contentSize.width - _innerScrollView.bounds.size.width, 0) animated:YES];
    });
}

#pragma
#pragma mark Places

- (IBAction)googlePlacesAction:(id)sender {
    
//    PlacesViewController *placesView = [[AppDelegate sharedAppDelegate].appController.mainStoryBoard instantiateViewControllerWithIdentifier:@"PlacesViewController"];
//    placesView.placesDelegate = self;
//    placesView.headingName = @"Add Address";
//    [[AppDelegate sharedAppDelegate].appController.navController pushViewController:placesView
//                                                                           animated:NO];
}

#pragma mark -
#pragma mark - PlacesViewControllerDelegate

- (void)placeMarker:(GMSPlace *)responseDict
{
    self.addressTextField.text = responseDict.formattedAddress;
    [self checkActiveButton:self.addressTextField.text];
}

#pragma mark -
#pragma mark - Text Field Delegate Methods
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == _expPeriodTextField) {
        [self.view endEditing:YES];
        [self takenTimeAction:0];
        return NO;
    }
    else if (textField == _currAddressTextField) {
        
    } else if (textField == _idCardTextField) {
        
        textField.secureTextEntry = NO;
    }
    return YES;
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *numberOFCharacter = [textField.text stringByReplacingCharactersInRange:range withString:string];
    [self checkActiveButton:numberOFCharacter];
    return YES;
}

- (IBAction)takenTimeAction:(id)sender {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select Expire Period Date" message:@"" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Select", nil];
    alert.delegate = self;
    alert.tag = 100;
    _picker = [[UIDatePicker alloc] initWithFrame:CGRectMake(10, alert.bounds.size.height, 320, 216)];
    _picker.backgroundColor = [UIColor colorWithRed:232.0/255 green:232.0/255 blue:232.0/255 alpha:1.0];
    [_picker setDatePickerMode:UIDatePickerModeDate];
    [_picker setMinimumDate:[NSDate date]];
    [_picker addTarget:self action:@selector(updateTakenDateTime:)
      forControlEvents:UIControlEventValueChanged];
    [alert addSubview:_picker];
    alert.bounds = CGRectMake(0, 0, 320 + 20, alert.bounds.size.height + 216 + 20);
    [alert setValue:_picker forKey:@"accessoryView"];
    [alert show];
    
    _isTimeTaken = YES;
}

- (IBAction)updateTakenDateTime:(id)sender
{
    //    [self updateTekenTime];
}

- (void)updateTekenTime
{
    //getMMDDYYYYDate
    NSDate *date = self.picker.date;
    NSString *timeString = [date getMMDDYYYYDate];
    _expPeriodTextField.text = timeString;
    
    _deliveryDateTimeForWeb = [date getDateTimeInUTC];
    
    [self checkActiveButton:_expPeriodTextField.text];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        [self updateTekenTime];
    }
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)checkActiveButton:(NSString *)numberOFCharacter
{
    BOOL isActive = NO;
    
    for (JVFloatLabeledTextField *tfText in self.validationArray) {
        
        if (tfText.text.length > 0) {
            
            isActive = YES;
            if (numberOFCharacter.length == 0) {
                isActive = NO;
                break;
            }
            
        } else {
            
            isActive = NO;
            break;
        }
    }
    
    if (_genderSelection == 0 || _imagesArray.count == 0) {
        
        isActive = NO;
    }
    
    if (isActive) {
        
        _btnRegister.backgroundColor = COURIER_REG_ACTIVE_BUTTON;
        _btnRegister.selected = NO;
        
    } else {
        
        _btnRegister.backgroundColor = LOGIN_DISABLE_COLOR;
        _btnRegister.selected = YES;
    }
}

- (void)requestToRegisterIDCardUser
{
    
    if (_idCardTextField.text.length > 9 || _idCardTextField.text.length < 9) {
        [UtilHelper showApplicationAlertWithMessage:@"SSN must be 9 digits." withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        [self hideProgressHud];

        return;
    }
    
    if (![UtilHelper isNetworkAvailable]) {
        
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        [self hideProgressHud];

        return;
    }
    
    if(_imagesArray.count > 0)
    {
        int tempIndex = 0;
        for (UIImageView *v in _innerScrollView.subviews) {
            if ([v isKindOfClass:[UIImageView class]]) {
                
                if (tempIndex < 2) {
                    
                    AddImageObject *image1 = _imagesArray[tempIndex];
                    if (image1.image == nil) {
                        image1.image = v.image;
                    }
                }
                tempIndex++;
            }
        }
        
        NSString *finalBase641 = EMPTY_STRING;
        NSString *finalBase642 = EMPTY_STRING;
        
        if (_imagesArray.count >= 1) {
            
            AddImageObject *imageObject = _imagesArray[0];
            NSData* imageData1 = UIImageJPEGRepresentation(imageObject.image, 0.5);
            NSString *image1Base64 = [imageData1 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            NSString *stringBase1 = @"data:image/png;base64,";
            finalBase641 = [stringBase1 stringByAppendingString:image1Base64];
        }
        
        if (_imagesArray.count > 1) {
            
            AddImageObject *imageObject = _imagesArray[1];
            NSData* imageData2 = UIImageJPEGRepresentation(imageObject.image, 0.5);
            NSString *image2Base64 = [imageData2 base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            NSString *stringBase2 = @"data:image/png;base64,";
            finalBase642 = [stringBase2 stringByAppendingString:image2Base64];
        }
        
        [WebServicesClient UpdateCourierIDCard:finalBase641
                                  IdCardImage1:finalBase642
                                  IdCardNumber:_nameTextField.text
                              IdCardExpiryDate:_deliveryDateTimeForWeb
                                 AddressActual:_addressTextField.text
                               AddressOfficial:_addressTextField.text
                                   IDCardState:_cityTextField.text
                                    SSNNumaber:_idCardTextField.text
                                        Gender:[NSString stringWithFormat:@"%li", (long)_genderSelection]
                                   UseOfficial:@"1"
                                 LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                   LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                  LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                 LogDeviceType:LOG_DEVICE_TYPE
                             completionHandler:^(BOOL result, NSError *error) {
                                 
                                 [self hideProgressHud];
                                 
                                 if (!error) {
                                     
                                     if (!_isComingFromProfile) {
                                         [[AppDelegate sharedAppDelegate].appController showTakePhotoView:NO];
                                         
                                          [self updateCountOfStepsCompletedByCourierInRegistration:@"2"];
                                     }
                                     
                                 } else {
                                     
                                     [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                                 }
                             }];
        
    }
    else
    {
        [self hideProgressHud];
        
        [UtilHelper showApplicationAlertWithMessage:@"One or more ID card images have not been downloaded. Please contact administrator or try visitng the page after sometime"
                                       withDelegate:nil
                                            withTag:0
                                  otherButtonTitles:@[@"Ok"]];
        
    }
}

- (void)requestToLoginUserInfo
{
    [self showProgressHud];
    [WebServicesClient GetUserInfo:[AppDelegate sharedAppDelegate].sessionKey
                       LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                      LogUserAgent:[[UIDevice currentDevice] systemVersion]
                     LogDeviceType:LOG_DEVICE_TYPE
                 completionHandler:^(UserModel *user, NSError *error) {
                     
                     [self hideProgressHud];
                     
                     if (!error) {
                         
                         self.user = user;
                         [self populateUserdata:user];
                         
                     }
                     else if (error.code == 3) {
                         
                         [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                         
                     } else {
                         
                         [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                     }
                 }];
}

- (void)populateUserdata:(UserModel *)user
{
    _nameTextField.text = user.idcardNumber;
    _currAddressTextField.text = user.addressOffical;
    _addressTextField.text = user.addressOffical;
    _idCardTextField.text = user.ssnNumber;
    _cityTextField.text = user.idcatdState;
    
    NSDate *date = [NSDate justDateFromDateString:user.idcardExpDate];
    _deliveryDateTimeForWeb = [date getDateTimeInUTC];
    
    if (user.addressActual != nil) {
        
        _addressTextField.text = user.addressActual;
    }
    
    if (user.idcardExpDate != nil) {
        
        _isTimeTaken = YES;
//        _expPeriodTextField.text = user.idcardExpDate;
        _expPeriodTextField.text = [date getMMDDYYYYDate];

    }
    
    if (user.idcardImg1 != nil) {
        
        AddImageObject *addImage = [AddImageObject new];
        addImage.imageName = user.idcardImg1;
        addImage.isUploaded = YES;
        [_imagesArray addObject:addImage];
    }
    
    if (user.idcardImg2 != nil) {
        
        AddImageObject *addImage = [AddImageObject new];
        addImage.imageName = user.idcardImg2;
        addImage.isUploaded = YES;
        [_imagesArray addObject:addImage];
    }
    [self addImages];
    
    _genderSelection = [user.gender integerValue];
    
    if ([user.gender isEqualToString:@"1"]) {
        [self genderAction:_btnMale];
    } else if ([user.gender isEqualToString:@"2"]) {
        [self genderAction:_btnFemale];
    }
}

//- (void)loadCourierProfileImage:(NSString *)profileImage imageView:(UIImageView *)imageView
//{
//    //Shows indicatore on imageView
//    [imageView setShowActivityIndicatorView:YES];
//    [imageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
//
//    NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, profileImage];
//
//    //Loads image
//    [imageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
//                 placeholderImage:[UIImage imageNamed:@""]
//                          options:SDWebImageRefreshCached];
//}

//Initializing and allocating current location manager class
- (void)initializeLocationManagerService
{
    //Allocating and Initializing LocationManagerService class
    _locationManagerService = [LocationManagerService new];
    
    //Assigning delegates of LocationManagerService
    _locationManagerService.locationManagerDelagete = self;
    
    //Initialize setup method for CLLocation
    [_locationManagerService initialize];
}

#pragma mark
#pragma mark -- LocationManagerService Delegates

- (void)notifyToMapViewWithLocation:(CLLocation *)currentLocation;
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // @"37.787544"
        //@"-122.444927"
        [WebServicesClient UpdateUserGeoLocation:[NSString stringWithFormat:@"%f",currentLocation.coordinate.latitude]
                                         UserLng:[NSString stringWithFormat:@"%f",currentLocation.coordinate.longitude]
                                   LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                                     LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                    LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                   LogDeviceType:LOG_DEVICE_TYPE
                               completionHandler:^(BOOL result, NSError *error) {
                                   
                                   dispatch_async(dispatch_get_main_queue(), ^{
                                      
                                       [_locationManagerService stopUpdatingLocation];
                                   });
                               }];
    });
}

#pragma mark
#pragma mark -- LocationManagerService Delegates
- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Update the page when more than 50% of the previous/next page is visible
    CGFloat pageWidth = self.innerScrollView.frame.size.width;
    int page = floor((self.innerScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    self.pageControl.currentPage = page;
}


@end
