//
//  ProfileViewController.m
//  CheapSender
//
//  Created by M Imran on 01/05/2017.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "ProfileViewController.h"
#import "ProfileTableViewCell.h"
#import "LoginViewController.h"

@interface ProfileViewController () <UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *imagesArray;
@property (nonatomic, strong) NSArray *menuNamesArray;

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
    
}

#pragma mark -
#pragma mark - Helpers

- (void)prepareUI
{
//    self.menuNamesArray = @[@"Personal Info",@"Payment",@"Settings",@"FAQ",@"Feedback",@"Log out",@"Create a courier profile"];

//    self.imagesArray = @[@"personalInfo",@"payments",@"settings",@"faq",@"faq",@"logout",@"+"];
    
    self.menuNamesArray = @[@"Personal Information",@"Payments",@"Settings",@"FAQ",@"Feedback",@"Log out"];
    self.imagesArray = @[@"personalInfo",@"payments",@"settings",@"faq",@"feedback",@"logout"];

    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.profileImageView = (UIImageView *)[UtilHelper makeCornerCurved:self.profileImageView withRadius:self.profileImageView.frame.size.width/2 color:nil width:0];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSString *senderId = [UtilHelper getValueFromNSUserDefaultForKey:SENDER_ID];
    
    //Update database
    SenderInfoDB *senderInfo =  [SenderInfoDB getSenderInfoDB:[senderId intValue]];
    
    if (senderId != nil) {
        
        self.nameLabel.text = senderInfo.name;
        self.emailLabel.text = senderInfo.email;
        self.typeLabel.text = @"Sender";
        [self loadSenderProfileImage:senderInfo.imageName];
    }
    
    [self requestToLoginUserInfo];
}

- (void)loadSenderProfileImage:(NSString *)profileImage
{
    //Shows indicatore on imageView
    [_profileImageView setShowActivityIndicatorView:YES];
    [_profileImageView setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, profileImage];
    
    //Loads image
//    [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    
    [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
}

#pragma mark -
#pragma mark - IBActions

- (IBAction)profileAction:(id)sender {
    
    [[AppDelegate sharedAppDelegate].appController showPersonalEditView];
}

#pragma mark -
#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuNamesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ProfileTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.imageIcon.image = [UIImage imageNamed:self.imagesArray[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
//    if (indexPath.row == self.menuNamesArray.count-1) {
//        //last row
//        cell.menuItemNameLabel.textColor = [UIColor colorWithRed:255.0/255.0 green:104/255.0 blue:0/255.0 alpha:1];
//        cell.menuItemNameLabel.font = [UIFont fontWithName:@"SFUIText-Medium" size:14];
//    }else {
//        cell.menuItemNameLabel.textColor = [UIColor blackColor];
//        cell.menuItemNameLabel.font = [UIFont fontWithName:@"SFUIText-Regular" size:14];
//    }
    
    
   
    cell.menuItemNameLabel.textColor = [UIColor blackColor];
    cell.menuItemNameLabel.font = [UIFont fontWithName:@"SFUIText-Regular" size:14];
    cell.menuItemNameLabel.text = self.menuNamesArray[indexPath.row];
    
    if (indexPath.row == self.menuNamesArray.count-1){
        cell.seperator.hidden = true;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 0) {
        
        [[AppDelegate sharedAppDelegate].appController showPersonalEditView];
    }
    else if (indexPath.row == 1) {
        
        [[AppDelegate sharedAppDelegate].appController showPaymentsMadeView];
    }
    else if (indexPath.row == 2) {
        
        [[AppDelegate sharedAppDelegate].appController showSenderSettingsView];
    }
    else if (indexPath.row == 3) {
        
        [[AppDelegate sharedAppDelegate].appController showCourierFAQView:@"2"];
    }
    else if (indexPath.row == 4) {
        
        [[AppDelegate sharedAppDelegate].appController showSenderFeedbackView];
    }
    else if (indexPath.row == 5) {
        
        [UtilHelper showApplicationAlertWithMessage:@"Are you sure you want to logout?"
                                       withDelegate:self
                                            withTag:1001
                                  otherButtonTitles:@[@"No", @"Yes"]];
    }
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        if (buttonIndex == 1) {
            
            [self showProgressHud];
            [WebServicesClient LogoutUser:[AppDelegate sharedAppDelegate].iosDeviceToken
                             LogUserAgent:[[UIDevice currentDevice] systemVersion]
                            LogDeviceType:LOG_DEVICE_TYPE
                               SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                        completionHandler:^(BOOL isLogout, NSError *error) {
                            
                            [self hideProgressHud];
                            
                            if (!error) {
                                
                                [self senderLogout];
                                
                            }
                            else if (error.code == 3) {
                                
                                [self senderLogout];
                            }
                        }];
        }
    }
}

- (void)requestToLoginUserInfo
{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [WebServicesClient GetUserInfo:[AppDelegate sharedAppDelegate].sessionKey
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                     completionHandler:^(UserModel *user, NSError *error) {
                         
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                             if (!error) {
                                 
                                 [self populateUserdata:user];
                                 
                             }
                             else if (error.code == 3) {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                 
                             } else {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                             }
                         });
                         
                     }];
    });
}

- (void)populateUserdata:(UserModel *)user
{
    
    self.nameLabel.text = user.name;
    self.emailLabel.text = user.email;
    self.typeLabel.text = @"Sender";
    [self loadSenderProfileImage:user.selfImg];
    
    //Update database
    [SenderInfoDB insertAndupdateSenderWithId:user.internalBaseClassIdentifier
                                         Name:user.name
                                      SurName:user.surname
                                        Email:user.email
                                      Address:user.addressActual
                                    ImageName:user.selfImg
                                     IsUpdate:YES];
}

@end
