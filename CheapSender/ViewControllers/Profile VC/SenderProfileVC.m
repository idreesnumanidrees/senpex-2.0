//
//  SenderProfileVC.m
//  CheapSender
//
//  Created by Idrees on 2017/05/29.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SenderProfileVC.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface SenderProfileVC ()
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIView *statusDot;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;

@property (nonatomic, strong) NSString *courierId;
@property (nonatomic, strong) NSString *senderId;
@property (nonatomic, strong) NSString *cellNumber;

@property (weak, nonatomic) IBOutlet UIButton *chatButton;
@property (weak, nonatomic) IBOutlet UILabel *chatLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *callButtonXConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *callLabelXConstraint;

@end

@implementation SenderProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Helpers

- (void)prepareUI
{
    
    if (_isComingFromChatDetails) {
        
        _chatButton.hidden = YES;
        _chatLabel.hidden = YES;
        _callButtonXConstraint.constant = 0;
        _callLabelXConstraint.constant = 0;
    }
    
    if (_courierListData != nil) {
        
        _lblName.text = _courierListData.packOwnerName;
        [self loadSenderProfileImage:_courierListData.packOwnerSelfImage];
        
        _senderId = _courierListData.packOwnerId;
        _courierId = _courierListData.acceptedCourierId;
        _cellNumber = _courierListData.packOwnerCell;
        
    } else {
        
        _courierId = _conversation.senderId;
         _senderId = _conversation.viewId;
        
        if(_courierId == nil || _courierId == 0)
        {
            _courierId = _conversation.receiverId;
        }
    }
    
    [self requestToUserinfo];
    
}

- (void)loadSenderProfileImage:(NSString *)profileImage
{
    //Shows indicatore on imageView
    [_profileImage setShowActivityIndicatorView:YES];
    [_profileImage setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@",DOWNLOAD_BASE_IMAGE_URL, profileImage];
    
    //Loads image
//    [_profileImage sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    
    [_profileImage sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
}

#pragma mark - User Actions
- (IBAction)chatAction:(id)sender {
    
    CourierConversations *conversationSender = [CourierConversations new];
    
    if (_courierListData != nil) {
        
        conversationSender.receiverId = _courierListData.packOwnerId;
        conversationSender.viewId = _courierListData.packOwnerId;
        conversationSender.viewName = _courierListData.packOwnerName;
        conversationSender.viewImg = _courierListData.packOwnerSelfImage;
        conversationSender.senderId = _courierListData.acceptedCourierId;
        
    } else {
        
        conversationSender.receiverId = _conversation.receiverId;
        conversationSender.viewId = _conversation.viewId;
        conversationSender.viewName = _conversation.viewName;
        conversationSender.viewImg = _conversation.viewImg;
        conversationSender.senderId = _conversation.senderId;

    }
    
    [[AppDelegate sharedAppDelegate].appController showChatCourierDetails:conversationSender
                                                              PackDetails:nil];

}

- (IBAction)callAction:(id)sender {
    
    NSString *cleanedString = [[_cellNumber componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
    NSString *escapedPhoneNumber = [cleanedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *phoneURLString = [NSString stringWithFormat:@"telprompt:%@", escapedPhoneNumber];
    NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
    
    if ([[UIApplication sharedApplication] canOpenURL:phoneURL]) {
        [[UIApplication sharedApplication] openURL:phoneURL];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            [WebServicesClient AddCallHistory:_senderId
                                    ReciverId:_courierId
                                     CallDate:[NSString stringWithFormat:@"%@", [NSDate date]]
                                  LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                                 LogUserAgent:[[UIDevice currentDevice] systemVersion]
                                LogDeviceType:LOG_DEVICE_TYPE
                                   SessionKey:[AppDelegate sharedAppDelegate].sessionKey
                            completionHandler:^(BOOL isCallAdded, NSError *error) {
                                
                            }];
        });
    }
}

#pragma mark--
#pragma mark-- Server Side handling

- (void)requestToUserinfo
{
    [self showProgressHud];
    
    [WebServicesClient GetUserInfoById:_senderId
                         LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                           LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                          LogUserAgent:[[UIDevice currentDevice] systemVersion]
                         LogDeviceType:LOG_DEVICE_TYPE
                     completionHandler:^(UserModel *user, NSError *error) {
                         
                         [self hideProgressHud];
                         
                         if (!error) {
                             
                             self->_lblName.text = [NSString stringWithFormat:@"%@ %@", user.name, user.surname];
                             self->_cellNumber = user.cell;
                             [self loadSenderProfileImage:user.selfImg];
                             
                             if ([user.availStatus isEqualToString:@"1"])
                             {
                                 self->_lblStatus.text = @"Online";
                                 self->_statusDot.backgroundColor = [UIColor greenColor];
                                 
                             }
                             else if ([user.availStatus isEqualToString:@"2"])
                             {
                                 self->_lblStatus.text = @"Offline";
                                 self->_statusDot.backgroundColor = [UIColor grayColor];
                                 
                             }
                             else if ([user.availStatus isEqualToString:@"3"])
                             {
                                 self->_lblStatus.text = @"Busy";
                                 self->_statusDot.backgroundColor = [UIColor redColor];
                             }
                             
                         } else {
                             
                             if (error.code == 3) {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                                 
                             } else {
                                 
                                 [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
                             }
                         }
                     }];
}

#pragma mark UIAlertView Delegate Methods
- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag == 1001)
    {
        [self senderLogout];
    }
}


@end
