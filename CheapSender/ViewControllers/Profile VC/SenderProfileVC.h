//
//  SenderProfileVC.h
//  CheapSender
//
//  Created by Idrees on 2017/05/29.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface SenderProfileVC : BaseViewController
@property (nonatomic, strong) PackDetailsModel *courierListData;
@property (nonatomic, strong) CourierConversations *conversation;
@property (nonatomic, assign) BOOL isComingFromChatDetails;

@end
