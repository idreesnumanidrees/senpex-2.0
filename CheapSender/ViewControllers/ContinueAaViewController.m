//
//  ContinueAaViewController.m
//  CheapSender
//
//  Created by admin on 4/19/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "ContinueAaViewController.h"

@interface ContinueAaViewController ()

//Outlets
@property (strong, nonatomic) IBOutlet UIButton *btnSender;
@property (strong, nonatomic) IBOutlet UIButton *btnCourier;

@end

@implementation ContinueAaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Helpers

- (void)prepareUI
{
    self.btnSender = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnSender withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
    
    self.btnCourier = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)self.btnCourier withRadius:25.0 color:COMMON_ICONS_COLOR width:0];
}

#pragma mark -
#pragma mark - IBActions

- (IBAction)senderAction:(id)sender {
    
    [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:1] forKey:LOGON_USER];
    [[AppDelegate sharedAppDelegate].appController loadTabbarController];
    
    //Update card number
//    CardInfoDB *cardInfo = [CardInfoDB getCardInfoDB:@"1"];
//    
//    if (cardInfo != nil) {
//        [CardInfoDB insertAndupdateCardInfoWithId:cardInfo.cardId
//                                       CardNumber:EMPTY_STRING
//                                      StripeToken:EMPTY_STRING
//                                         IsUpdate:YES];
//    }
}

- (IBAction)courierAction:(id)sender {
    
    [UtilHelper saveAndUpdateNSUserDefaultWithObject:[NSNumber numberWithInt:2] forKey:LOGON_USER];
    [[AppDelegate sharedAppDelegate].appController loadCourierTabbarController];
}


@end
