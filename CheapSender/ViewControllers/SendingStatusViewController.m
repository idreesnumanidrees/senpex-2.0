//
//  SendingStatusViewController.m
//  CheapSender
//
//  Created by Admin on 10/04/2018.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import "SendingStatusViewController.h"
#import "OverviewImageDetailViewController.h"

@interface SendingStatusViewController ()

@end

@implementation SendingStatusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareUI];
}

- (void)prepareUI{
    
    _label_1 = (UILabel *)[UtilHelper makeBorderCornerCurved:(UIView *)self.label_1 withRadius:_label_1.frame.size.height/2 color:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1] width:1];
    _label_2 = (UILabel *)[UtilHelper makeBorderCornerCurved:(UIView *)self.label_2 withRadius:_label_2.frame.size.height/2 color:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1] width:1];
    _label_3 = (UILabel *)[UtilHelper makeBorderCornerCurved:(UIView *)self.label_3 withRadius:_label_3.frame.size.height/2 color:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1] width:1];
    _label_4 = (UILabel *)[UtilHelper makeBorderCornerCurved:(UIView *)self.label_4 withRadius:_label_4.frame.size.height/2 color:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1] width:1];
    _label_5 = (UILabel *)[UtilHelper makeBorderCornerCurved:(UIView *)self.label_5 withRadius:_label_5.frame.size.height/2 color:[UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:1] width:1];
    _btnOverview = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnOverview withRadius:_btnOverview.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    _btnStatus = (UIButton *)[UtilHelper makeCornerCurved:(UIView *)_btnStatus withRadius:_btnStatus.frame.size.height/2 color:COMMON_ICONS_COLOR width:0];
    
    _image_2 = (UIImageView *)[UtilHelper makeImageRounded:_image_2];
    _image_3 = (UIImageView *)[UtilHelper makeImageRounded:_image_3];
    _image_5 = (UIImageView *)[UtilHelper makeImageRounded:_image_5];
    
    // Set Icon
    if ([_packDetails.sendCatId isEqualToString:@"2"] || [_packDetails.sendCatId isEqual:@2]) {
        _icon.image = [UIImage imageNamed:@"pack-icon"];
        
    } else if ([_packDetails.sendCatId isEqualToString:@"1"] || [_packDetails.sendCatId isEqual:@1]) {
        _icon.image = [UIImage imageNamed:@"ups-status"];
        
    } else if ([_packDetails.sendCatId isEqualToString:@"3"] || [_packDetails.sendCatId isEqual:@3]) {
        
        _icon.image = [UIImage imageNamed:@"Store-status"];
        
    }else if ([_packDetails.sendCatId isEqualToString:@"4"] || [_packDetails.sendCatId isEqual:@4]) {
        
        _icon.image = [UIImage imageNamed:@"return-services"];
    }
    
    //View Hieght
    _viewHeight2.constant = 55;
    _viewHeight3.constant = 55;
    [_dotLine2 setImage:[UIImage imageNamed:@"dotline_1"]];
    [_dotLine3 setImage:[UIImage imageNamed:@"dotline_1"]];
    
    _lblPickAddress.text = _packDetails.packFromText;
    _lblDeliveryAddress.text = _packDetails.packToText;
    // Paid pack
    //Pack Status color
    if ([_packDetails.packStatus isEqualToString:@"10"]){
        [self showStatus_1:YES];
    }
    //Courier selected for the package
    else if ([_packDetails.packStatus isEqualToString:@"20"]){
        [self showStatus_1:NO];
        [self showStatus_2:YES];
    }
    //Courier pickedup the package from sender. Pack given
    else if ([_packDetails.packStatus isEqualToString:@"30"] || [_packDetails.packStatus isEqualToString:@"50"]){
        [self showStatus_1:NO];
        [self showStatus_2:NO];
        [self showStatus_3:YES];
    }
    //Package is delivered
    else if ([_packDetails.packStatus isEqualToString:@"40"] || [_packDetails.packStatus isEqualToString:@"90"]){
        [self showStatus_1:NO];
        [self showStatus_2:NO];
        [self showStatus_3:NO];
        [self showStatus_4:YES];
    }
    //Pack not given by sender
    else if ([_packDetails.packStatus isEqualToString:@"80"]){
        [self showStatus_1:NO];
        [self showStatus_2:NO];
        [self showStatus_3:NO];
        [self showStatus_4:NO];
        [self showStatus_5:YES];
    }
}

- (void) showStatus_1:(Boolean)status{
    _lblTime1.hidden = false;
    _lblDay1.hidden = false;
    NSDate *date = [NSDate dateFromDateString:_packDetails.insertedDate];
    NSString *localtime = [date getDateTimeInLocalTimeZone];
    NSDate *localdate = [NSDate dateFromDateString:localtime];
    _lblDay1.text = [date getMMDDYYYYDate];
    _lblTime1.text = [date getTimeFromDateWithoutDay];
    if (status) {
        _label_1 = (UILabel *)[UtilHelper makeBorderCornerCurved:(UIView *)self.label_1 withRadius:_label_1.frame.size.height/2 color:LIGHT_ORANGE_COLOR width:1];
        _label_1.textColor = LIGHT_ORANGE_COLOR;
        _title_1.textColor = LIGHT_ORANGE_COLOR;
        _lblTime1.textColor = LIGHT_ORANGE_COLOR;
        _lblDay1.textColor = LIGHT_ORANGE_COLOR;
    }
}
- (void) showStatus_2:(Boolean)status{
    _image_2.hidden = false;
    _lblTime2.hidden = false;
    _lblDay2.hidden = false;
    _courierName_2.hidden = false;
    _courerTitle_2.hidden = false;
    NSDate *date;
    if ([_packDetails.packStatus isEqualToString:@"90"]) {
        date = [NSDate dateFromDateString:_packDetails.lastOperationTime];
    } else {
        date = [NSDate dateFromDateString:[_packDetails.packBidders[0] objectForKey:@"accepted_date"]];
    }
    NSString *localtime = [date getDateTimeInLocalTimeZone];
    NSDate *localdate = [NSDate dateFromDateString:localtime];
    _lblDay2.text = [date getMMDDYYYYDate];
    _lblTime2.text = [date getTimeFromDateWithoutDay];
    _viewHeight2.constant = 110;
    [_dotLine2 setImage:[UIImage imageNamed:@"dotline_2"]];
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, _packDetails.acceptedCourierSelfImg];
    
    //Loads image
    [_image_2 sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    _courierName_2.text = [NSString stringWithFormat:@"%@ %@", _packDetails.acceptedCourierName, _packDetails.acceptedCourierSurname];
    if (status) {
        
        _label_2 = (UILabel *)[UtilHelper makeBorderCornerCurved:(UIView *)self.label_2 withRadius:_label_2.frame.size.height/2 color:LIGHT_ORANGE_COLOR width:1];
        _label_2.textColor = LIGHT_ORANGE_COLOR;
        _title_2.textColor = LIGHT_ORANGE_COLOR;
        _lblTime2.textColor = LIGHT_ORANGE_COLOR;
        _lblDay2.textColor = LIGHT_ORANGE_COLOR;
        
    }
}
- (void) showStatus_3:(Boolean)status{
    NSDate *date = [NSDate dateFromDateString:_packDetails.lastOperationTime];
    NSString *localtime = [date getDateTimeInLocalTimeZone];
    NSDate *localdate = [NSDate dateFromDateString:localtime];
    _lblDay3.text = [date getMMDDYYYYDate];
    _lblTime3.text = [date getTimeFromDateWithoutDay];
    _image_3.hidden = false;
    _lblTime3.hidden = false;
    _lblDay3.hidden = false;
    _courierName_3.hidden = false;
    _courierTitle_3.hidden = false;
    _viewHeight3.constant = 55;
    [_dotLine3 setImage:[UIImage imageNamed:@"dotline_1"]];
    _courierName_3.text = [NSString stringWithFormat:@"%@ %@", _packDetails.acceptedCourierName, _packDetails.acceptedCourierSurname];
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, _packDetails.acceptedCourierSelfImg];
    
    //Loads image
    [_image_3 sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    if (status) {
        _label_3 = (UILabel *)[UtilHelper makeBorderCornerCurved:(UIView *)self.label_3 withRadius:_label_3.frame.size.height/2 color:LIGHT_ORANGE_COLOR width:1];
        _label_3.textColor = LIGHT_ORANGE_COLOR;
        _title_3.textColor = LIGHT_ORANGE_COLOR;
        _lblTime3.textColor = LIGHT_ORANGE_COLOR;
        _lblDay3.textColor = LIGHT_ORANGE_COLOR;
        if ([_packDetails.packStatus isEqualToString:@"50"]) {
            _title_3.text = @"Pack not given";
        }
    }
}
- (void) showStatus_4:(Boolean)status{
    NSDate *date = [NSDate dateFromDateString:_packDetails.lastOperationTime];
    NSString *localtime = [date getDateTimeInLocalTimeZone];
    NSDate *localdate = [NSDate dateFromDateString:localtime];
    _lblDay4.text = [date getMMDDYYYYDate];
    _lblTime4.text = [date getTimeFromDateWithoutDay];
    _lblTime4.hidden = false;
    _lblDay4.hidden = false;
    if (status) {
        _label_4 = (UILabel *)[UtilHelper makeBorderCornerCurved:(UIView *)self.label_4 withRadius:_label_4.frame.size.height/2 color:LIGHT_ORANGE_COLOR width:1];
        _label_4.textColor = LIGHT_ORANGE_COLOR;
        _title_4.textColor = LIGHT_ORANGE_COLOR;
        _lblTime4.textColor = LIGHT_ORANGE_COLOR;
        _lblDay4.textColor = LIGHT_ORANGE_COLOR;
        if ([_packDetails.packStatus isEqualToString:@"90"]) {
            _title_4.text = @"Order was reported";
        }
    }
}
- (void) showStatus_5:(Boolean)status{
    _image_5.hidden = false;
    _lblTime5.hidden = false;
    _lblDay5.hidden = false;
    _courierName_5.hidden = false;
    _courierTitle_5.hidden = false;
    _courierName_5.text = [NSString stringWithFormat:@"%@ %@", _packDetails.acceptedCourierName, _packDetails.acceptedCourierSurname];
    NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, _packDetails.packOwnerSelfImage];
    
    //Loads image
    [_image_5 sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    if (status) {
        _label_5 = (UILabel *)[UtilHelper makeBorderCornerCurved:(UIView *)self.label_5 withRadius:_label_5.frame.size.height/2 color:LIGHT_ORANGE_COLOR width:1];
        _label_5.textColor = LIGHT_ORANGE_COLOR;
        _title_5.textColor = LIGHT_ORANGE_COLOR;
        _lblTime5.textColor = LIGHT_ORANGE_COLOR;
        _lblDay5.textColor = LIGHT_ORANGE_COLOR;
        _courierName_5.text = _packDetails.acceptedCourierName;
        NSString *finalUrl = [NSString stringWithFormat:@"%@%@", DOWNLOAD_BASE_IMAGE_URL, _packDetails.packOwnerSelfImage];
        
        //Loads image
        [_image_5 sd_setImageWithURL:[NSURL URLWithString:[finalUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] placeholderImage:[UIImage imageNamed:@"profilePhoto"] options:SDWebImageRefreshCached];
    }
}

- (IBAction)clickImage:(id)sender {
//    UIStoryboard *mainstoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    OverviewImageDetailViewController *imageView = [mainstoryboard instantiateViewControllerWithIdentifier:@"OverviewIDVC"];
//    [self presentViewController:imageView animated:YES completion:nil];
    
}
- (IBAction)clickOverview:(id)sender {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(UIButton *)sender {
    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
    
    for (UIViewController *aViewController in allViewControllers) {
        if ([aViewController isKindOfClass:[UITabBarController class]]) {
            
            UITabBarController *tabbar = (UITabBarController *)aViewController;
            NSString *restorIdentity = tabbar.restorationIdentifier;
            
            if ([restorIdentity isEqualToString:@"sender"]) {
                [self.navigationController popToViewController:aViewController animated:YES];
                tabbar.selectedIndex = 0;
            }
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
