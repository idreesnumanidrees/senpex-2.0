//
//  ResetPasswordViewController.h
//  CheapSender
//
//  Created by admin on 4/18/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseViewController.h"

@interface ResetPasswordViewController : BaseViewController

@property (nonatomic, assign) BOOL isComingFromChangePassword;


@end
