//
//  CSWebConstants.m
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CSWebConstants.h"

@implementation CSWebConstants

//Base URL
//NSString *const BASE_URL = @"http://phpexamscript.net/cheap_sender/webservices/?service=";

//Test server
NSString *const BASE_URL = @"https://app3.senpex.com/senpex/webservices/?service=";
NSString *const DOWNLOAD_BASE_IMAGE_URL = @"https://app3.senpex.com/senpex/images/";
NSString *const API_VERSION = @"&api_version=2";

//Production
//NSString *const BASE_URL = @"https://www.senpex.com/senpex/webservices/?service=";
//NSString *const DOWNLOAD_BASE_IMAGE_URL = @"https://www.senpex.com/senpex/images/";
//NSString *const API_VERSION = @"&api_version=2";

//NSString *const DOWNLOAD_BASE_IMAGE_URL = @"http://phpexamscript.net/cheap_sender/images/";

//**************************** Operations ********************

//**************************** Sender Reg ********************

NSString *const LOGIN_USER = @"LoginUser";
NSString *const FB_LOGIN = @"LoginWithFb";
NSString *const GOOGLE_LOGIN = @"LoginWithGoogle";
NSString *const REGISTER_SENDER = @"RegisterSender";

//**************************** End Sender Reg ********************

NSString *const CREATE_PACK = @"CreatePack";
NSString *const INSTANTPAYMENTSHOWPRICE = @"InstantPaymentShowPrice";
NSString *const CREATEPACK_PRICEINFO = @"CreatePack_PriceInfo";
NSString *const UPDATEPACK_PRICEINFO = @"UpdatePack_PriceInfo";
NSString *const DELETE_MY_DRAFT_PACK = @"DeleteMyDraftPack";

NSString *const UPDATE_PACK_GENINFO = @"UpdatePack_GenInfo";
NSString *const CREATE_PACK_GENINFO = @"CreatePack_GenInfo";

NSString *const ADD_PACK_IMAGE = @"AddPackImage";
NSString *const DELETE_PACK_IMAGE = @"DeletePackImage";

NSString *const GET_USER_INFO = @"GetUserInfo";
NSString *const GET_USER_INFO_BY_ID = @"GetUserInfoByID";

NSString *const UPDATE_SENDER = @"UpdateSender";

NSString *const SEND_APPROVE_EMAIL_TO_SENDER = @"SendApproveEmailToSender";

NSString *const UPDATE_PACK_DESTINATION = @"UpdatePack_Destination";
NSString *const PAY_FOR_PACK = @"PayForPack";
NSString *const PAY_FOR_ASKEDPACK_BYEMAIL = @"PayForAskedPackByEmail";
NSString *const ASK_FRIEND_TO_PAY = @"AskFriendToPay";
NSString *const ASK_FRIEND_TO_PAY_INSTANT = @"AskFrientToPayInstant";
NSString *const MAKE_INSTANT_PAYMENT = @"MakeInstantPayment";
NSString *const GET_PACK_DETAILS = @"GetPackDetails";
NSString *const CANCEL_PACK = @"CancelPack";
NSString *const SENDER_PEPORT_PROBLEM = @"SenderReportProblem";

NSString *const UPDATEPACK_SENDINGNAME = @"UpdatePack_SendingName";
NSString *const UPDATEPACK_DESCRIPTION = @"UpdatePack_Description";

NSString *const SENDER_GET_ACTIVE_PACKS = @"SenderGetActivePacks";
NSString *const SENDER_GET_DRAFT_PACKS = @"SenderGetDraftPacks";
NSString *const SENDER_GET_COMPLETED_PACKS = @"SenderGetCompletedPacks";
NSString *const SENDER_GET_RECEIVED_PACKS = @"SenderGetPacksPaidByMe";

#pragma mark -- Operations Courier Params
NSString *const REGISTER_COURIER_GEN_INFO = @"RegisterCourier_GenInfo";
NSString *const UPDATE_COURIER_GEN_INFO = @"UpdateCourier_GenInfo";
NSString *const UPDATE_COURIER_GEN_ID_CARD = @"UpdateCourier_IDCard";
NSString *const UPDATE_USER_SELF_IMAGE = @"UpdateUser_SelfImg";
NSString *const GET_USERS_TRANSPORT_TYPES = @"GetUsersTransportTypes";
NSString *const ADD_NEW_TRANSPORT = @"AddNewTransport";
NSString *const UPDATE_USER_TRANSPORT = @"UpdateUserTransport";

NSString *const ADD_NEW_TRANSPORT_IMAGE = @"AddNewTransportImage";
NSString *const GET_USER_TRANSPORTS = @"GetUserTransports";
NSString *const UPDATE_USER_INS_GEN = @"UpdateUserInsGen";
NSString *const GET_USER_INS_GEN = @"GetUserInsGen";

NSString *const SEND_APPROVE_EMAIL_TO_COURIER = @"SendApproveEmailToCourier";

NSString *const UPDATE_USER_AVAIL_STATUS = @"UpdateUserAvailStatus";
NSString *const UPDATE_USER_GEO_LOCATION = @"UpdateUserGeoLocation";
NSString *const COURIER_GET_PENDING_PACKS = @"CourierGetPendingPacks";

NSString *const COURIER_GET_ACTIVE_PACKS = @"CourierGetActivePacks";
NSString *const COURIER_GET_NEW_PACKS = @"CourierGetNewPacks";
NSString *const COURIER_GET_COMPLETED_PACKS = @"CourierGetCompletedPacks";
NSString *const CANCEL_PACKAGE_REQUEST = @"CancelPackageRequest";
NSString *const GET_ACCEPT_RULE = @"GetAcceptRule";
NSString *const ADD_CALL_HISTORY = @"AddCallHistory";

NSString *const COURIER_TAKE_PACK = @"TakePack";
NSString *const DELIVER_PACK = @"DeliverPack";
NSString *const ADD_DELIVERY_IMG = @"AddDeliveryImg";
NSString *const DELETE_DELIVERY_IMG = @"DeleteDeliveryImg";
NSString *const GET_ALL_DELIVERY_IMAGES = @"GetAllDeliveryImgs";
NSString *const PACK_DID_NOT_GIVEN = @"PackDidnotGiven";
NSString *const COURIER_REPORT_PROBLEM = @"CourierReportProblem";
NSString *const BE_COURIER_OF_PACK = @"BeCourierOfPack";

NSString *const GET_PACK_ALL_IMAGES = @"GetPackAllImages";

NSString *const MODIFY_USER_TRANSPORT_TYPES = @"ModifyUserTransportTypes";
NSString *const DELETE_TRANSPORT_IMAGE_BY_ID = @"DeleteTransportImageById";
NSString *const DELETE_USER_TRANSPORT_BY_ID = @"DeleteUserTransportById";
NSString *const GET_TRANSPORT_IMAGES_BY_TRANSPORT_ID = @"GetTransportImagesByTransportId";
NSString *const ADD_USER_INSURANCE = @"AddUserIns";
NSString *const GET_USER_INSURANCE = @"GetUserIns";
NSString *const DELETE_USER_INSURANCE_BY_ID = @"DeleteUserInsById";
NSString *const UPDATE_USER_INSURANCE = @"UpdateUserIns";
NSString *const DELETE_USER_INSURANCE_BY_TRANSPORT_ID = @"DeleteUserInsByTransportId";
NSString *const UPDATE_QOURIER_QUESTION = @"UpdateCourier_Questions";
NSString *const APPROVE_USER = @"ApproveUser";
NSString *const CHANGE_PASSWORD_BY_GU_ID = @"ChangePasswordByGuid";
NSString *const CHANGE_PASSWORD = @"ChangePassword";
NSString *const CHANGE_PASSWORD_2 = @"ChangePassword2";

NSString *const CREATE_SENDER_PROFILE = @"CreateSenderProfile";
NSString *const MAKE_DEFAULT_PROFILE = @"MakeDefaultProfile";
NSString *const RESEND_PROFILE_FOR_ADMIN_APPROVE = @"ResendProfileForAdminApprove";
NSString *const SEND_FORGOT_PASSWORD_EMAIL = @"SendForgotPasswordEmail";
NSString *const CREATE_COURIER_PROFILE = @"CreateCourierProfile";
NSString *const UPDATE_USERS_DRIVING_LISENCE = @"UpdateUsersDrivingLis";
NSString *const GET_USERS_DRIVING_LISENCE = @"GetUsersDrivingLis";

NSString *const GET_FAQ_BY_ID = @"GetFaqById";
NSString *const GET_FAQ_HEADERS = @"GetFaqHeaders";
NSString *const GET_USERS_RATES = @"GetUsersRates";
NSString *const GET_USERS_RATES_BY_USER_ID = @"GetUserRatesByUserID";
NSString *const RATE_USER = @"RateUser";
NSString *const REVIEW_COURIER = @"AddUserReview";
NSString *const SEND_FEEDBACK = @"SendFeedback";
NSString *const GIVE_TIP = @"GiveTip";
NSString *const GET_DIC_BY_NAME = @"GetDicByName";
NSString *const GET_ALL_DICS = @"GetAllDics";
NSString *const UPDATE_BANK_ACCOUNT = @"UpdateBankAccount";
NSString *const GETESTIMATEDNEXTPAYMENT = @"GetEstimatedNextPayment";
NSString *const GET_BANK_ACCOUNT = @"GetBankAccount";

//********************************** Payments ******************
NSString *const GET_MY_PAYMENTS = @"GetMyPayments";
NSString *const GET_MY_PAYOUTS = @"GetMyPayouts";
NSString *const GET_PAYOUT_Details = @"GetPayoutDetails";

//********************************** Chat ******************
NSString *const SEND_CHAT_MESSAGE = @"SendChatMessage";
NSString *const SEND_CHAT_CONVERSATION = @"GetChatConversations";
NSString *const GET_MESSAGE_LIST = @"GetMessageList";

//********************************** Notifications ******************
NSString *const GET_USER_NOTS = @"GetUserNots";
NSString *const MARK_AS_NOT = @"MarkNotAs";


#pragma mark -- Request Params
//**************************** REQUEST PARAMS ********************
//************************************************

//****************************LOGIN********************
NSString *const REST_PARAM_REQUEST_PASSWORD = @"password";
NSString *const REST_PARAM_REQUEST_EMAIL = @"email";
NSString *const REST_PARAM_REQUEST_FB_TOKEN = @"fb_token";
NSString *const REST_PARAM_REQUEST_GOOGLE_TOKEN = @"google_token";

//**************************** REGISTER SENDER ********************
NSString *const REST_PARAM_REQUEST_NAME = @"name";
NSString *const REST_PARAM_REQUEST_SURNAME = @"surname";

//**************************** UPDATE SENDER ********************
NSString *const REST_PARAM_REQUEST_LOCATION_USE = @"use_location";
NSString *const REST_PARAM_REQUEST_GENDER = @"gender";
NSString *const REST_PARAM_REQUEST_PAYPAL_EMAIL = @"paypal_email";

//**************************** REGISTER OF COURIER ********************
NSString *const REST_PARAM_REQUEST_ADDRESS_OFFICIAL = @"address_offical";
NSString *const REST_PARAM_REQUEST_REFERAL_NAME = @"referal_name";
NSString *const REST_PARAM_REQUEST_REFERAL_CELL = @"referal_cell";

//**************************** UPDATE COURIER ID CARD ********************
NSString *const REST_PARAM_REQUEST_COURIER_ID = @"courier_id";
NSString *const REST_PARAM_REQUEST_ID_CARD_IMAGE_1 = @"idcard_img1";
NSString *const REST_PARAM_REQUEST_ID_CARD_IMAGE_2 = @"idcard_img2";
NSString *const REST_PARAM_REQUEST_ID_CARD_NUMBER = @"idcard_number";
NSString *const REST_PARAM_REQUEST_ACTUAL_ADDRESS = @"address_actual";
NSString *const REST_PARAM_REQUEST_OFFICIAL_ADDRESS = @"address_offical";
NSString *const REST_PARAM_REQUEST_ID_CARD_STATE = @"idcatd_state";
NSString *const REST_PARAM_REQUEST_SSN_NUMBER = @"ssn_number";
NSString *const REST_PARAM_REQUEST_USE_OFFICIAL = @"use_offical";

//**************************** SELF IMAGE ********************
NSString *const REST_PARAM_REQUEST_SELF_IMAGE = @"self_img";

//**************************** MODITY USER TRANSPORT TYPES ********************
NSString *const REST_PARAM_REQUEST_TRANSPORT_IDS = @"transport_ids";

//**************************** ADD NEW TRANSPORT ********************
NSString *const REST_PARAM_REQUEST_TRANSPORT_TYPE = @"transport_type";
NSString *const REST_PARAM_REQUEST_TRANSPORT_YEAR = @"transport_year";
NSString *const REST_PARAM_REQUEST_TRANSPORT_MODEL = @"transport_model";
NSString *const REST_PARAM_REQUEST_TRANSPORT_COLOR = @"transport_color";
NSString *const REST_PARAM_REQUEST_TRANSPORT_PLATE_NUMBER = @"plate_number";
NSString *const REST_PARAM_REQUEST_TRANSPORT_MAKE = @"make";

//**************************** ADD NEW TRANSPORT IMAGE ********************
NSString *const REST_PARAM_REQUEST_TRANSPORT_IMAGE = @"transport_img";

//**************************** ADD USER INSURANCE ********************
NSString *const REST_PARAM_REQUEST_INSURANCE_NUMBER = @"ins_number";
NSString *const REST_PARAM_REQUEST_INSURANCE_ISSUE_DATE = @"ins_issue_date";
NSString *const REST_PARAM_REQUEST_INSURANCE_EXP_DATE = @"ins_exp_date";
NSString *const REST_PARAM_REQUEST_INSURANCE_FRANSHISE_AMOUNT = @"franshise_amount";
NSString *const REST_PARAM_REQUEST_INSURANCE_NAME_IN_POLICY = @"name_in_policy";
NSString *const REST_PARAM_REQUEST_INSURANCE_PROVIDER = @"ins_provider";
NSString *const REST_PARAM_REQUEST_INSURANCE_POLICY_NUMBER = @"policy_number";
NSString *const REST_PARAM_REQUEST_INSURANCE_PHONE_NUMBER = @"ins_phone_number";
NSString *const REST_PARAM_REQUEST_INSURANCE_IMAGE_1 = @"ins_img1";
NSString *const REST_PARAM_REQUEST_INSURANCE_IMAGE_2 = @"ins_img2";

//**************************** UPDATE COURIER QUESTION ********************
NSString *const REST_PARAM_REQUEST_QUESTION_HEALTH = @"qst_health";
NSString *const REST_PARAM_REQUEST_QUESTION_DISABILITY = @"qst_criminal";
NSString *const REST_PARAM_REQUEST_QUESTION_CRIMINAL = @"qst_criminal";
NSString *const REST_PARAM_REQUEST_QUESTION_SEXUAL = @"qst_sexsual";

//**************************** APPROVE USER ********************
NSString *const REST_PARAM_REQUEST_APPROVE_GU_ID = @"approve_guid";

//**************************** MAKE DEFAULT PROFILE ********************
NSString *const REST_PARAM_REQUEST_USER_TYPE = @"user_type";
NSString *const REST_PARAM_FAQ_ID = @"faq_id";

//**************************** UPDATE USERS DRIVING LISENCE ********************
NSString *const REST_PARAM_REQUEST_LIS_NUMBER = @"lis_number";
NSString *const REST_PARAM_REQUEST_LIS_EXP_DATE = @"exp_date";
NSString *const REST_PARAM_REQUEST_LIS_LAST_NAME = @"last_name";
NSString *const REST_PARAM_REQUEST_LIS_FIRST_NAME = @"first_name";
NSString *const REST_PARAM_REQUEST_LIS_IMAGE_1 = @"lis_img1";
NSString *const REST_PARAM_REQUEST_LIS_IMAGE_2 = @"lis_img2";

//**************************** CREATE PACKAGE ********************
NSString *const REST_PARAM_CP_SEND_CAT_ID = @"send_cat_id";
NSString *const REST_PARAM_CP_PACK_ID = @"pack_id";
NSString *const REST_PARAM_CP_SEND_NAME = @"send_name";
NSString *const REST_PARAM_CP_PACK_SIZE_ID = @"pack_size_id";
NSString *const REST_PARAM_CP_PACK_OPTION_ID = @"pack_options_id";
NSString *const REST_PARAM_CP_PACK_WEIGHT = @"pack_weight";
NSString *const REST_PARAM_CP_ITEM_VALUE = @"item_value";
NSString *const REST_PARAM_CP_DESC_TEXT = @"desc_text";
NSString *const REST_PARAM_CP_RECEIVER_NAME = @"receiver_name";
NSString *const REST_PARAM_CP_RECEIVER_PHONE_NUMBER = @"receiver_phone_number";
NSString *const REST_PARAM_CP_PET_TYPE = @"pet_type";
NSString *const REST_PARAM_CP_PET_COUNT = @"pet_count";
NSString *const REST_PARAM_CP_PET_IS_VAC = @"pet_is_vac";
NSString *const REST_PARAM_CP_BASE_64_IMAGE = @"pack_img";
NSString *const REST_PARAM_CP_DELETE_IMAGE_ID = @"id";
NSString *const REST_PARAM_CP_RULE_MODULE = @"rule_module";

//**************************** Make Instant Payment **************

NSString *const REST_PARAM_CP_PHONE_NUMBER = @"phone_number";

//**************************** InstantPaymentShowPrice *************
NSString *const REST_PARAM_CP_CAT_ID = @"cat_id";
NSString *const REST_PARAM_CP_DAY = @"day";
NSString *const REST_PARAM_CP_TIME = @"time";
NSString *const REST_PARAM_CP_TARIFF_PLAN_ID = @"tariff_plan_id";
NSString *const REST_PARAM_CP_DEFAULT_TARIFF_PLAN = @"default_tariff_plan";
NSString *const REST_PARAM_CP_WAITING_MINS = @"waiting_mins";
NSString *const REST_PARAM_CP_PROMO_CODE = @"promo_code";

//**************************** Add Call History ********************
NSString *const REST_PARAM_CALLER_ID = @"caller_id";
NSString *const REST_PARAM_RECIVER_ID = @"reciver_id";
NSString *const REST_PARAM_CALL_DATE = @"call_date";

//Destination
NSString *const REST_PARAM_CP_PACK_FROM_LAT = @"pack_from_lat";
NSString *const REST_PARAM_CP_PACK_FROM_LNG = @"pack_from_lng";
NSString *const REST_PARAM_CP_PACK_FROM_TEXT = @"pack_from_text";
NSString *const REST_PARAM_CP_PACK_TO_LAT = @"pack_to_lat";
NSString *const REST_PARAM_CP_PACK_TO_LNG = @"pack_to_lng";
NSString *const REST_PARAM_CP_PACK_TO_TEXT = @"pack_to_text";
NSString *const REST_PARAM_CP_DISTANCE = @"distance";
NSString *const REST_PARAM_CP_DISTANCE_TIME = @"distance_time";
NSString *const REST_PARAM_CP_TAKEN_TIME = @"taken_time";
NSString *const REST_PARAM_CP_TAKEN_TYPE = @"taken_type";
NSString *const REST_PARAM_CP_TAKEN_ASAP = @"taken_asap";
NSString *const REST_PARAM_CP_DELIVERY_TIME = @"delivery_time";
NSString *const REST_PARAM_CP_DELIVERY_TYPE = @"delivery_type";
NSString *const REST_PARAM_CP_DELIVERY_ASAP = @"delivery_asap";

//PayForPack
NSString *const REST_PARAM_CP_PACK_PRICE = @"pack_price";
NSString *const REST_PARAM_CP_IS_INSURANCE_PAID = @"is_insurance_payed";
NSString *const REST_PARAM_CP_STRIP_TOKEN = @"stripeToken";
NSString *const REST_PARAM_CP_CARD_LAST_FOUR = @"card_last_4";

//AskFriendToPay
NSString *const REST_PARAM_CP_PAYOR_EMAIL = @"payor_email";

//Pack Details
NSString *const REST_PARAM_GET_PACK_INCLUDE_IMAGES = @"include_images";
NSString *const REST_PARAM_GET_PACK_INCLUDE_BIDDERS = @"include_bidders";
NSString *const REST_PARAM_GET_PACK_START = @"start";
NSString *const REST_PARAM_GET_PACK_COUNT = @"count";

//Pack report reason
NSString *const REST_PARAM_PR_REASON_TEXT = @"reason_text";
NSString *const REST_PARAM_PR_REASON_ID = @"reason_id";

//Pack List
NSString *const REST_PARAM_PL_COUNT = @"count";
NSString *const REST_PARAM_PL_START = @"start";

//Change Password
NSString *const REST_PARAM_CP_OLD_PASSWORD = @"old_password";
NSString *const REST_PARAM_CP_NEW_PASSWORD = @"new_password";
NSString *const REST_PARAM_CP_RE_TYPE_PASSWORD = @"re_new_password";


//***************************** COMMON PARAMS *******************
NSString *const REST_PARAM_COMMON_ID = @"id";
NSString *const REST_PARAM_COMMON_TRANSPORT_ID = @"transport_id";
NSString *const REST_PARAM_COMMON_NAME = @"name";
NSString *const REST_PARAM_COMMON_SURNAME = @"surname";
NSString *const REST_PARAM_COMMON_CELL = @"cell";
NSString *const REST_PARAM_COMMON_ADDRESS_ACTUAL = @"address_actual";
NSString *const REST_PARAM_COMMON_COMPANY_NAME = @"company_name";
NSString *const REST_PARAM_COMMON_COUNTRY = @"country";
NSString *const REST_PARAM_COMMON_CITY = @"city";
NSString *const REST_PARAM_COMMON_SEND_APPROVE_EMAIL = @"send_approve_mail";
NSString *const REST_PARAM_COMMON_ID_CARD_EXP_DATE = @"idcard_exp_date";

NSString *const REST_PARAM_COMMON_USER_ID = @"user_id";

NSString *const REST_PARAM_COMMON_LOG_DEVICE_ID = @"log_device_id";
NSString *const REST_PARAM_COMMON_LOG_USER_AGENT = @"log_user_argent";
NSString *const REST_PARAM_COMMON_LOG_DEVICE_TYPE = @"log_device_type";
NSString *const REST_PARAM_COMMON_LOG_SESSON_KEY = @"log_session_key";

//**************************** RESPONSE PARAMS ********************
//************************************************

//***************************** Courier My Delivery PARAMS *******************
NSString *const REST_PARAM_DL_AVAIL_STATUS = @"avail_status";
NSString *const REST_PARAM_DL_LAST_LAT = @"last_lat";
NSString *const REST_PARAM_DL_LAST_LNG = @"last_lng";

//********************************** Chat Params ******************
NSString *const REST_PARAM_RECIEVER_ID = @"receiver_id";
NSString *const REST_PARAM_CHAT_TEXT = @"chat_text";
NSString *const REST_PARAM_PUSH_NOT = @"push_not";

//********************************** Notifications Params ******************
NSString *const REST_PARAM_MARK_AS_PUSHED = @"mark_as_pushed";
NSString *const REST_PARAM_STATUS = @"status";
NSString *const REST_PARAM_NOT_ID = @"not_id";
NSString *const REST_PARAM_IS_READ = @"is_read";

//********************************** Rate Params ******************
NSString *const REST_PARAM_RATE_VAL = @"rate_val";
NSString *const REST_PARAM_FEEDBACK_TEXT = @"feedback_text";
NSString *const REST_PARAM_REVIEW_COURIER_TEXT = @"comment_text";

NSString *const REST_PARMA_PAY_AMOUNT = @"pay_amount";

//********************************** Courier Bank Details ******************
NSString *const REST_PARAM_AC_LINE1 = @"line1";
NSString *const REST_PARAM_AC_LINE2 = @"line2";
NSString *const REST_PARAM_AC_STATE = @"state";
NSString *const REST_PARAM_AC_POSTAL_CODE = @"postal_code";
NSString *const REST_PARAM_AC_DATE = @"date";
NSString *const REST_PARAM_AC_ROUTE_NUMBER = @"route_number";
NSString *const REST_PARAM_AC_ACCOUNT_NUMBER = @"account_number";

NSString *const REST_PARAM_PAYOUT_ID = @"payout_id";


//MARK: -Version 1.5
NSString *const GET_USER_ROUTES = @"GetUserRoutes";




@end
