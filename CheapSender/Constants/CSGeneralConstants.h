//
//  CSGeneralConstants.h
//  CheapSender
//
//  Created by admin on 4/5/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#ifndef CSGeneralConstants_h
#define CSGeneralConstants_h

//Comparison current device vesion
#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)


#define GRAY_OUT_COLOR [UIColor colorWithRed:1 green:1 blue:1 alpha:0.50f]

#define COMMON_ICONS_COLOR [UIColor colorWithRed:46.0f/255.0f green:58.0f/255.0f blue:78.0f/255.0f alpha:1.0f]

#define FONT_COLOR [UIColor colorWithRed:46.0f/255.0f green:58.0f/255.0f blue:78.0f/255.0f alpha:1.0f]

#define FONT_COLOR_FADE [UIColor colorWithRed:46.0f/255.0f green:58.0f/255.0f blue:78.0f/255.0f alpha:0.5f]

#define PACK_YELLOW_STATUS_COLOR [UIColor colorWithRed:249.0f/255.0f green:177.0f/255.0f blue:10.0f/255.0f alpha:1.0f]

#define PACK_RED_STATUS_COLOR [UIColor colorWithRed:239.0f/255.0f green:68.0f/255.0f blue:0.0f/255.0f alpha:1.0f]

#define PACK_GREEN_STATUS_COLOR [UIColor colorWithRed:21.0f/255.0f green:198.0f/255.0f blue:83.0f/255.0f alpha:1.0f]

#define YELLOW_COLOR [UIColor colorWithRed:255.0f/255.0f green:104.0f/255.0f blue:0.0f/255.0f alpha:1.0f]

#define LIGHT_ORANGE_COLOR [UIColor colorWithRed:255.0f/255.0f green:104.0f/255.0f blue:1.0f/255.0f alpha:1.0f]

#define LOGIN_DISABLE_COLOR [UIColor colorWithRed:185.0f/255.0f green:192.0f/255.0f blue:200.0f/255.0f alpha:1.0f]

#define LOGIN_ACTIVE_COLOR LIGHT_ORANGE_COLOR
//[UIColor colorWithRed:53.0f/255.0f green:66.0f/255.0f blue:88.0f/255.0f alpha:1.0f]

#define SEND_MESSAGE_COLOR [UIColor colorWithRed:53.0f/255.0f green:66.0f/255.0f blue:88.0f/255.0f alpha:1.0f]

#define VIEW_MAP_NAV_COLOR [UIColor colorWithRed:255.0f/255.0f green:186.0f/255.0f blue:32.0f/255.0f alpha:1.0f]

#define OFFLINE_BUTTON_SELECTION [UIColor colorWithRed:68.0f/255.0f green:176.0f/255.0f blue:73.0f/255.0f alpha:1.0f]
#define ONLINE_BUTTON_SELECTION [UIColor colorWithRed:208.0f/255.0f green:2.0f/255.0f blue:27.0f/255.0f alpha:1.0f]

#define BORDER_COLOR [UIColor colorWithRed:227.0f/255.0f green:227.0f/255.0f blue:227.0f/255.0f alpha:1.0f]

#define ORANGE_LINE_COLOR [UIColor colorWithRed:255.0f/255.0f green:143.0f/255.0f blue:65.0f/255.0f alpha:1.0f]

#define MARKER_BLACK_LINE [UIColor colorWithRed:53.0f/255.0f green:66.0f/255.0f blue:89.0f/255.0f alpha:1.0f]

#define COURIER_REG_ACTIVE_BUTTON [UIColor colorWithRed:53.0f/255.0f green:66.0f/255.0f blue:88.0f/255.0f alpha:1.0f]

//RIYAS
#define ORANGE_COLOR [UIColor colorWithRed:255.0f/255.0f green:174.0f/255.0f blue:28.0f/255.0f alpha:0.5f]
//Riyas code Ends

#define IS_IPHONE_4_OR_LESS [[UIScreen mainScreen ] bounds].size.height < 568.0f
#define IS_IPHONE_5 [[UIScreen mainScreen ] bounds].size.height == 568.0f
#define IS_IPHONE_6 [[UIScreen mainScreen ] bounds].size.height == 667.0f
#define IS_IPHONE_6P [[UIScreen mainScreen ] bounds].size.height == 736.0f
#define IS_IPHONE_X [[UIScreen mainScreen ] bounds].size.height == 812.0f

#define SEND_TEXT_IMAGE_HEX "\uf579"

#define TIME_INTERVAL 600

//Line Color
#define VALIDATION_LINE_COLOR [UIColor colorWithRed:154.0f/255.0f green:154.0f/255.0f blue:154.0f/255.0f alpha:0.5f]



#endif /* CSGeneralConstants_h */
