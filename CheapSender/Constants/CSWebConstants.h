//
//  CSWebConstants.h
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSWebConstants : NSObject

//Base URL
extern NSString *const BASE_URL;
extern NSString *const API_VERSION;

extern NSString *const DOWNLOAD_BASE_IMAGE_URL;

//Operations
extern NSString *const LOGIN_USER;
extern NSString *const FB_LOGIN;
extern NSString *const GOOGLE_LOGIN;
extern NSString *const REGISTER_SENDER;
extern NSString *const CREATE_PACK;
extern NSString *const INSTANTPAYMENTSHOWPRICE;
extern NSString *const CREATEPACK_PRICEINFO;
extern NSString *const UPDATEPACK_PRICEINFO;
extern NSString *const DELETE_MY_DRAFT_PACK;
extern NSString *const UPDATE_PACK_GENINFO;
extern NSString *const CREATE_PACK_GENINFO;
extern NSString *const ADD_PACK_IMAGE;
extern NSString *const DELETE_PACK_IMAGE;

extern NSString *const UPDATE_PACK_DESTINATION;
extern NSString *const PAY_FOR_PACK;
extern NSString *const PAY_FOR_ASKEDPACK_BYEMAIL;
extern NSString *const ASK_FRIEND_TO_PAY;
extern NSString *const ASK_FRIEND_TO_PAY_INSTANT;
extern NSString *const MAKE_INSTANT_PAYMENT;
extern NSString *const GET_PACK_DETAILS;
extern NSString *const CANCEL_PACK;
extern NSString *const SENDER_PEPORT_PROBLEM;

extern NSString *const UPDATEPACK_SENDINGNAME;
extern NSString *const UPDATEPACK_DESCRIPTION;

extern NSString *const SEND_APPROVE_EMAIL_TO_SENDER;

extern NSString *const SENDER_GET_ACTIVE_PACKS;
extern NSString *const SENDER_GET_DRAFT_PACKS;
extern NSString *const SENDER_GET_COMPLETED_PACKS;
extern NSString *const SENDER_GET_RECEIVED_PACKS;

extern NSString *const GET_USER_INFO;
extern NSString *const GET_USER_INFO_BY_ID;

extern NSString *const UPDATE_SENDER;
extern NSString *const REGISTER_COURIER_GEN_INFO;
extern NSString *const UPDATE_COURIER_GEN_INFO;
extern NSString *const UPDATE_COURIER_GEN_ID_CARD;
extern NSString *const UPDATE_USER_SELF_IMAGE;
extern NSString *const MODIFY_USER_TRANSPORT_TYPES;
extern NSString *const GET_USERS_TRANSPORT_TYPES;
extern NSString *const ADD_NEW_TRANSPORT;
extern NSString *const UPDATE_USER_TRANSPORT;

extern NSString *const UPDATE_USER_INS_GEN;
extern NSString *const GET_USER_INS_GEN;

extern NSString *const DELETE_TRANSPORT_IMAGE_BY_ID;
extern NSString *const DELETE_USER_TRANSPORT_BY_ID;
extern NSString *const ADD_NEW_TRANSPORT_IMAGE;
extern NSString *const GET_TRANSPORT_IMAGES_BY_TRANSPORT_ID;
extern NSString *const GET_USER_TRANSPORTS;
extern NSString *const ADD_USER_INSURANCE;
extern NSString *const GET_USER_INSURANCE;
extern NSString *const DELETE_USER_INSURANCE_BY_ID;
extern NSString *const UPDATE_USER_INSURANCE;

extern NSString *const DELETE_USER_INSURANCE_BY_TRANSPORT_ID;
extern NSString *const UPDATE_QOURIER_QUESTION;
extern NSString *const SEND_APPROVE_EMAIL_TO_COURIER;
extern NSString *const UPDATE_USER_AVAIL_STATUS;
extern NSString *const UPDATE_USER_GEO_LOCATION;
extern NSString *const COURIER_GET_PENDING_PACKS;

extern NSString *const COURIER_GET_ACTIVE_PACKS;
extern NSString *const COURIER_GET_NEW_PACKS;
extern NSString *const COURIER_GET_COMPLETED_PACKS;
extern NSString *const CANCEL_PACKAGE_REQUEST;
extern NSString *const GET_ACCEPT_RULE;
extern NSString *const ADD_CALL_HISTORY;

extern NSString *const COURIER_TAKE_PACK;
extern NSString *const DELIVER_PACK;
extern NSString *const ADD_DELIVERY_IMG;
extern NSString *const DELETE_DELIVERY_IMG;
extern NSString *const GET_ALL_DELIVERY_IMAGES;
extern NSString *const PACK_DID_NOT_GIVEN;
extern NSString *const COURIER_REPORT_PROBLEM;
extern NSString *const BE_COURIER_OF_PACK;

extern NSString *const GET_PACK_ALL_IMAGES;
extern NSString *const UPDATE_BANK_ACCOUNT;
extern NSString *const GETESTIMATEDNEXTPAYMENT;
extern NSString *const GET_BANK_ACCOUNT;

extern NSString *const APPROVE_USER;
extern NSString *const CHANGE_PASSWORD_BY_GU_ID;
extern NSString *const CHANGE_PASSWORD;
extern NSString *const CHANGE_PASSWORD_2;

extern NSString *const CREATE_SENDER_PROFILE;
extern NSString *const MAKE_DEFAULT_PROFILE;
extern NSString *const RESEND_PROFILE_FOR_ADMIN_APPROVE;
extern NSString *const SEND_FORGOT_PASSWORD_EMAIL;
extern NSString *const CREATE_COURIER_PROFILE;
extern NSString *const UPDATE_USERS_DRIVING_LISENCE;
extern NSString *const GET_USERS_DRIVING_LISENCE;

extern NSString *const GET_FAQ_BY_ID;
extern NSString *const GET_FAQ_HEADERS;
extern NSString *const GET_USERS_RATES;
extern NSString *const GET_USERS_RATES_BY_USER_ID;
extern NSString *const RATE_USER;
extern NSString *const REVIEW_COURIER;
extern NSString *const SEND_FEEDBACK;
extern NSString *const GIVE_TIP;
extern NSString *const GET_DIC_BY_NAME;
extern NSString *const GET_ALL_DICS;

//********************************** Payments ******************
extern NSString *const GET_MY_PAYMENTS;
extern NSString *const GET_MY_PAYOUTS;
extern NSString *const GET_PAYOUT_Details;

//********************************** Chat ******************
extern NSString *const SEND_CHAT_MESSAGE;
extern NSString *const SEND_CHAT_CONVERSATION;
extern NSString *const GET_MESSAGE_LIST;

//********************************** Chat Params ******************
extern NSString *const REST_PARAM_RECIEVER_ID;
extern NSString *const REST_PARAM_CHAT_TEXT;
extern NSString *const REST_PARAM_PUSH_NOT;

//********************************** Notifications ******************
extern NSString *const GET_USER_NOTS;
extern NSString *const MARK_AS_NOT;

//**************************** REQUEST PARAMS ********************
//************************************************

//**************************** LOGIN ********************
extern NSString *const REST_PARAM_REQUEST_PASSWORD;
extern NSString *const REST_PARAM_REQUEST_EMAIL;
extern NSString *const REST_PARAM_REQUEST_FB_TOKEN;
extern NSString *const REST_PARAM_REQUEST_GOOGLE_TOKEN;

//**************************** REGISTER SENDER ********************
extern NSString *const REST_PARAM_REQUEST_NAME;
extern NSString *const REST_PARAM_REQUEST_SURNAME;

//**************************** UPDATE SENDER ********************
extern NSString *const REST_PARAM_REQUEST_LOCATION_USE;
extern NSString *const REST_PARAM_REQUEST_GENDER;
extern NSString *const REST_PARAM_REQUEST_PAYPAL_EMAIL;

//**************************** REGISTER OF COURIER ********************
extern NSString *const REST_PARAM_REQUEST_ADDRESS_OFFICIAL;
extern NSString *const REST_PARAM_REQUEST_REFERAL_NAME;
extern NSString *const REST_PARAM_REQUEST_REFERAL_CELL;

//**************************** UPDATE COURIER ID CARD ********************
extern NSString *const REST_PARAM_REQUEST_COURIER_ID;
extern NSString *const REST_PARAM_REQUEST_ID_CARD_IMAGE_1;
extern NSString *const REST_PARAM_REQUEST_ID_CARD_IMAGE_2;
extern NSString *const REST_PARAM_REQUEST_ID_CARD_NUMBER;
extern NSString *const REST_PARAM_REQUEST_ACTUAL_ADDRESS;
extern NSString *const REST_PARAM_REQUEST_OFFICIAL_ADDRESS;
extern NSString *const REST_PARAM_REQUEST_ID_CARD_STATE;
extern NSString *const REST_PARAM_REQUEST_SSN_NUMBER;
extern NSString *const REST_PARAM_REQUEST_USE_OFFICIAL;

//**************************** SELF IMAGE ********************
extern NSString *const REST_PARAM_REQUEST_SELF_IMAGE;

//**************************** MODITY USER TRANSPORT TYPES ********************
extern NSString *const REST_PARAM_REQUEST_TRANSPORT_IDS;

//**************************** ADD NEW TRANSPORT ********************
extern NSString *const REST_PARAM_REQUEST_TRANSPORT_TYPE;
extern NSString *const REST_PARAM_REQUEST_TRANSPORT_YEAR;
extern NSString *const REST_PARAM_REQUEST_TRANSPORT_MODEL;
extern NSString *const REST_PARAM_REQUEST_TRANSPORT_COLOR;
extern NSString *const REST_PARAM_REQUEST_TRANSPORT_PLATE_NUMBER;
extern NSString *const REST_PARAM_REQUEST_TRANSPORT_MAKE;

//**************************** ADD NEW TRANSPORT IMAGE ********************
extern NSString *const REST_PARAM_REQUEST_TRANSPORT_IMAGE;

//**************************** ADD USER INSURANCE ********************
extern NSString *const REST_PARAM_REQUEST_INSURANCE_NUMBER;
extern NSString *const REST_PARAM_REQUEST_INSURANCE_ISSUE_DATE;
extern NSString *const REST_PARAM_REQUEST_INSURANCE_EXP_DATE;
extern NSString *const REST_PARAM_REQUEST_INSURANCE_FRANSHISE_AMOUNT;
extern NSString *const REST_PARAM_REQUEST_INSURANCE_NAME_IN_POLICY;
extern NSString *const REST_PARAM_REQUEST_INSURANCE_PROVIDER;
extern NSString *const REST_PARAM_REQUEST_INSURANCE_POLICY_NUMBER;
extern NSString *const REST_PARAM_REQUEST_INSURANCE_PHONE_NUMBER;
extern NSString *const REST_PARAM_REQUEST_INSURANCE_IMAGE_1;
extern NSString *const REST_PARAM_REQUEST_INSURANCE_IMAGE_2;

//**************************** UPDATE COURIER QUESTION ********************
extern NSString *const REST_PARAM_REQUEST_QUESTION_HEALTH;
extern NSString *const REST_PARAM_REQUEST_QUESTION_DISABILITY;
extern NSString *const REST_PARAM_REQUEST_QUESTION_CRIMINAL;
extern NSString *const REST_PARAM_REQUEST_QUESTION_SEXUAL;

//**************************** APPROVE USER ********************
extern NSString *const REST_PARAM_REQUEST_APPROVE_GU_ID;

//**************************** MAKE DEFAULT PROFILE ********************
extern NSString *const REST_PARAM_REQUEST_USER_TYPE;
extern NSString *const REST_PARAM_FAQ_ID;

//**************************** UPDATE USERS DRIVING LISENCE ********************
extern NSString *const REST_PARAM_REQUEST_LIS_NUMBER;
extern NSString *const REST_PARAM_REQUEST_LIS_EXP_DATE;
extern NSString *const REST_PARAM_REQUEST_LIS_LAST_NAME;
extern NSString *const REST_PARAM_REQUEST_LIS_FIRST_NAME;
extern NSString *const REST_PARAM_REQUEST_LIS_IMAGE_1;
extern NSString *const REST_PARAM_REQUEST_LIS_IMAGE_2;

//**************************** CREATE PACKAGE ********************
extern NSString *const REST_PARAM_CP_SEND_CAT_ID;
extern NSString *const REST_PARAM_CP_PACK_ID;
extern NSString *const REST_PARAM_CP_SEND_NAME;
extern NSString *const REST_PARAM_CP_PACK_SIZE_ID;
extern NSString *const REST_PARAM_CP_PACK_OPTION_ID;
extern NSString *const REST_PARAM_CP_PACK_WEIGHT;
extern NSString *const REST_PARAM_CP_ITEM_VALUE;
extern NSString *const REST_PARAM_CP_DESC_TEXT;
extern NSString *const REST_PARAM_CP_RECEIVER_NAME;
extern NSString *const REST_PARAM_CP_RECEIVER_PHONE_NUMBER;
extern NSString *const REST_PARAM_CP_PET_TYPE;
extern NSString *const REST_PARAM_CP_PET_COUNT;
extern NSString *const REST_PARAM_CP_PET_IS_VAC;
extern NSString *const REST_PARAM_CP_BASE_64_IMAGE;
extern NSString *const REST_PARAM_CP_DELETE_IMAGE_ID;
extern NSString *const REST_PARAM_CP_RULE_MODULE;

//**************************** Ask Friend to Pay **********
extern NSString *const REST_PARAM_CP_PAYOR_EMAIL;

//**************************** Make Instant Payment ****************

extern NSString *const REST_PARAM_CP_PHONE_NUMBER;

//**************************** InstantPaymentShowPrice *************
extern NSString *const REST_PARAM_CP_CAT_ID;
extern NSString *const REST_PARAM_CP_DAY;
extern NSString *const REST_PARAM_CP_TIME;
extern NSString *const REST_PARAM_CP_TARIFF_PLAN_ID;
extern NSString *const REST_PARAM_CP_DEFAULT_TARIFF_PLAN;
extern NSString *const REST_PARAM_CP_WAITING_MINS;
extern NSString *const REST_PARAM_CP_PROMO_CODE;

//**************************** Add Call History ********************
extern NSString *const REST_PARAM_CALLER_ID;
extern NSString *const REST_PARAM_RECIVER_ID;
extern NSString *const REST_PARAM_CALL_DATE;

//Destination params
extern NSString *const REST_PARAM_CP_PACK_FROM_LAT;
extern NSString *const REST_PARAM_CP_PACK_FROM_LNG;
extern NSString *const REST_PARAM_CP_PACK_FROM_TEXT;
extern NSString *const REST_PARAM_CP_PACK_TO_LAT;
extern NSString *const REST_PARAM_CP_PACK_TO_LNG;
extern NSString *const REST_PARAM_CP_PACK_TO_TEXT;
extern NSString *const REST_PARAM_CP_DISTANCE;
extern NSString *const REST_PARAM_CP_DISTANCE_TIME;
extern NSString *const REST_PARAM_CP_TAKEN_TIME;
extern NSString *const REST_PARAM_CP_TAKEN_TYPE;
extern NSString *const REST_PARAM_CP_TAKEN_ASAP;
extern NSString *const REST_PARAM_CP_DELIVERY_TIME;
extern NSString *const REST_PARAM_CP_DELIVERY_TYPE;
extern NSString *const REST_PARAM_CP_DELIVERY_ASAP;

//PayForPack
extern NSString *const REST_PARAM_CP_PACK_PRICE;
extern NSString *const REST_PARAM_CP_IS_INSURANCE_PAID;
extern NSString *const REST_PARAM_CP_STRIP_TOKEN;
extern NSString *const REST_PARAM_CP_CARD_LAST_FOUR;

//Pack Details
extern NSString *const REST_PARAM_GET_PACK_INCLUDE_IMAGES;
extern NSString *const REST_PARAM_GET_PACK_INCLUDE_BIDDERS;
extern NSString *const REST_PARAM_GET_PACK_START;
extern NSString *const REST_PARAM_GET_PACK_COUNT;

//Pack report reason
extern NSString *const REST_PARAM_PR_REASON_TEXT;
extern NSString *const REST_PARAM_PR_REASON_ID;

//Pack List
extern NSString *const REST_PARAM_PL_COUNT;
extern NSString *const REST_PARAM_PL_START;

//Change Password
extern NSString *const REST_PARAM_CP_OLD_PASSWORD;
extern NSString *const REST_PARAM_CP_NEW_PASSWORD;
extern NSString *const REST_PARAM_CP_RE_TYPE_PASSWORD;

//***************************** COMMON PARAMS *******************
extern NSString *const REST_PARAM_COMMON_ID;
extern NSString *const REST_PARAM_COMMON_TRANSPORT_ID;
extern NSString *const REST_PARAM_COMMON_NAME;
extern NSString *const REST_PARAM_COMMON_SURNAME;
extern NSString *const REST_PARAM_COMMON_CELL;
extern NSString *const REST_PARAM_COMMON_ADDRESS_ACTUAL;
extern NSString *const REST_PARAM_COMMON_COMPANY_NAME;
extern NSString *const REST_PARAM_COMMON_COUNTRY;
extern NSString *const REST_PARAM_COMMON_CITY;
extern NSString *const REST_PARAM_COMMON_ID_CARD_EXP_DATE;
extern NSString *const REST_PARAM_COMMON_SEND_APPROVE_EMAIL;

extern NSString *const REST_PARAM_COMMON_USER_ID;


extern NSString *const REST_PARAM_COMMON_LOG_DEVICE_ID;
extern NSString *const REST_PARAM_COMMON_LOG_USER_AGENT;
extern NSString *const REST_PARAM_COMMON_LOG_DEVICE_TYPE;
extern NSString *const REST_PARAM_COMMON_LOG_SESSON_KEY;

//**************************** RESPONSE PARAMS ********************
//************************************************

//***************************** Courier My Delivery PARAMS *******************
extern NSString *const REST_PARAM_DL_AVAIL_STATUS;
extern NSString *const REST_PARAM_DL_LAST_LAT;
extern NSString *const REST_PARAM_DL_LAST_LNG;

//********************************** Rate Params ******************
extern NSString *const REST_PARAM_RATE_VAL;
extern NSString *const REST_PARAM_FEEDBACK_TEXT;
extern NSString *const REST_PARAM_REVIEW_COURIER_TEXT;

extern NSString *const REST_PARMA_PAY_AMOUNT;
//********************************** Courier Bank Details ******************
extern NSString *const REST_PARAM_AC_LINE1;
extern NSString *const REST_PARAM_AC_LINE2;
extern NSString *const REST_PARAM_AC_STATE;
extern NSString *const REST_PARAM_AC_POSTAL_CODE;
extern NSString *const REST_PARAM_AC_DATE;
extern NSString *const REST_PARAM_AC_ROUTE_NUMBER;
extern NSString *const REST_PARAM_AC_ACCOUNT_NUMBER;


//********************************** Notifications Params ******************
extern NSString *const REST_PARAM_MARK_AS_PUSHED;
extern NSString *const REST_PARAM_STATUS;
extern NSString *const REST_PARAM_NOT_ID;
extern NSString *const REST_PARAM_IS_READ;

extern NSString *const REST_PARAM_PAYOUT_ID;

//MARK: -Version 1.5
extern NSString *const GET_USER_ROUTES;


@end
