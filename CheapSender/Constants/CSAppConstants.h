//
//  CSAppConstants.h
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSAppConstants : NSObject

//Empty String
extern NSString *const EMPTY_STRING;

//Application name
extern NSString *const APPLICATION_NAME;

//Show tutorial screen
extern NSString *const SHOW_TUTORIAL_SCREEN;

//GOOGLE MAPS API KEY
extern NSString *const GOOGLE_MAPS_API_KEY;

//Stripe Publish key
extern NSString *const STRIPE_PUBLISHABLE_KEY;

//Keyboard observer keys
extern NSString *const KEYBOARD_SHOW;
extern NSString *const KEYBOARD_HIDE;

//Log device type
extern NSString *const LOG_DEVICE_TYPE;

//Internet alert message
extern NSString *const NO_INTERNET_FOUND;

//User Defaults
extern NSString *const LOGON_USER;
extern NSString *const LOGON_USER_INFO;
extern NSString *const SESSION_KEY;
extern NSString *const SENDER_ID;
extern NSString *const COURIER_AVAILABLE_STATUS;

//Observer Keys
extern NSString *const SENDER_ACTIVE_LIST_UPDATED;
extern NSString *const STOP_TIMER_AND_LOCATION_UPDATOR;
extern NSString *const COURIER_ACTIVE_LIST_UPDATED;
extern NSString *const REFRESH_ORDER_DETAILS;
extern NSString *const VIEW_CHAT_MESSAGE;
extern NSString *const UPDATE_CONVERSATION_LIST;
extern NSString *const ORDER_CANCEL_FROM_NEW_PACKS;

//Dictionaries
extern NSString *const PET_TYPE;
extern NSString *const TRANSPORT_TYPE;
extern NSString *const PACK_OPTIONS;
extern NSString *const USER_TYPES;
extern NSString *const PACK_STATE_SENDER;
extern NSString *const PACK_STATE_COURIER;
extern NSString *const PAYMENT_METHODS;
extern NSString *const PAYMENT_TYPES;
extern NSString *const PACK_SIZES;
extern NSString *const SENDER_REPORT_REASON;
extern NSString *const COURIER_REPORT_REASON;
extern NSString *const NOTIFICATION_STATUSES;

@end
