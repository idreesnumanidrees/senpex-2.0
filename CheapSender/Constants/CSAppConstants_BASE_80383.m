//
//  CSAppConstants.m
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CSAppConstants.h"

@implementation CSAppConstants

//Empty String
NSString *const EMPTY_STRING =  @"";

//Application name
NSString *const APPLICATION_NAME = @"CheapSender";

//Show tutorial screen
NSString *const SHOW_TUTORIAL_SCREEN = @"If app runs first time then tutorial screen shows";

//GOOGLE MAPS API KEY
NSString *const GOOGLE_MAPS_API_KEY = @"AIzaSyAka-cpT7DCe2ldb4NrxER3fV9x9460q4A";


//Keyboard observer keys
NSString *const KEYBOARD_SHOW = @"UIKeyboardWillShowNotification";
NSString *const KEYBOARD_HIDE = @"UIKeyboardWillHideNotification";

@end
