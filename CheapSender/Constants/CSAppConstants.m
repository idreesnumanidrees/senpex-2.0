//
//  CSAppConstants.m
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CSAppConstants.h"

@implementation CSAppConstants

//Empty String
NSString *const EMPTY_STRING =  @"";

//Application name
NSString *const APPLICATION_NAME = @"Senpex";

//Show tutorial screen
NSString *const SHOW_TUTORIAL_SCREEN = @"If app runs first time then tutorial screen shows";

//GOOGLE MAPS API KEY
//NSString *const GOOGLE_MAPS_API_KEY = @"AIzaSyD7_ODakrGl27RsA7PHwUzIs4K3EQzj6ZU";
//NSString *const GOOGLE_MAPS_API_KEY = @"AIzaSyDzv6HL63OWMnStpWpMZX2yy7jVRxOAh5w";
NSString *const GOOGLE_MAPS_API_KEY = @"AIzaSyB-lk3fD8ODgDFy8julvVcfrutekgRIkzw";
//NSString *const GOOGLE_MAPS_API_KEY = @"AIzaSyA50XWgVLv-Ngl_8aHhc2ZYknY516xEqTg";

//Stripe Publish key
//NSString *const STRIPE_PUBLISHABLE_KEY = @"pk_test_kRXZ9hTibz298AU42dUMnDH2";
NSString *const STRIPE_PUBLISHABLE_KEY =@"pk_live_lsPZvBNaoiNFIyEMjCiSILdB";

//Keyboard observer keys
NSString *const KEYBOARD_SHOW = @"UIKeyboardWillShowNotification";
NSString *const KEYBOARD_HIDE = @"UIKeyboardWillHideNotification";

//Log device type
NSString *const LOG_DEVICE_TYPE = @"APPLE_PHONE";

//Internet alert message
NSString *const NO_INTERNET_FOUND = @"Please connect your device to the internet";

//User Defaults
NSString *const LOGON_USER = @"REGISTERED_USER";
NSString *const LOGON_USER_INFO = @"LOGON_STATUS";
NSString *const SESSION_KEY = @"SESSION_KEY";
NSString *const SENDER_ID = @"SENDER_ID";
NSString *const COURIER_AVAILABLE_STATUS = @"IS COURIER ONLINE OR NOT";

//Observer Keys
NSString *const SENDER_ACTIVE_LIST_UPDATED = @"Sender active list will be updated";
NSString *const STOP_TIMER_AND_LOCATION_UPDATOR= @"Stops update location and timer";
NSString *const COURIER_ACTIVE_LIST_UPDATED = @"Courier active list will be updated";
NSString *const REFRESH_ORDER_DETAILS = @"Refreshes order details";
NSString *const VIEW_CHAT_MESSAGE = @"ViewChatMessage";
NSString *const UPDATE_CONVERSATION_LIST = @"Update conversation list";
NSString *const ORDER_CANCEL_FROM_NEW_PACKS = @"Order cancel from new packs";



//Dictionaries
NSString *const PET_TYPE = @"pet_types";
NSString *const TRANSPORT_TYPE = @"transport_types";
NSString *const PACK_OPTIONS = @"pack_options";
NSString *const USER_TYPES = @"user_types";
NSString *const PACK_STATE_SENDER = @"pack_stat_sender";
NSString *const PACK_STATE_COURIER = @"pack_stat_courier";
NSString *const PAYMENT_METHODS = @"payment_methods";
NSString *const PAYMENT_TYPES = @"payment_types";
NSString *const PACK_SIZES = @"pack_sizes";
NSString *const SENDER_REPORT_REASON = @"s_report_reasons";
NSString *const COURIER_REPORT_REASON = @"c_report_reasons";
NSString *const NOTIFICATION_STATUSES = @"not_statuses";





@end
