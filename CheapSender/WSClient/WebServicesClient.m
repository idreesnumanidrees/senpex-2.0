//
//  WebServicesClient.m
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "WebServicesClient.h"
#import "CSWebConstants.h"


@implementation WebServicesClient

//Singleton object of AFHTTPSessionManager
+ (AFHTTPSessionManager *)sharedManager {
    
    static AFHTTPSessionManager *sessionManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sessionManager = [AFHTTPSessionManager manager];
    });
    return sessionManager;
}

#pragma mark
#pragma mark - Post REQUEST
+ (void)postRequestWithURL:(NSString *)url
                parameters:(NSMutableArray *)parameters
         CompletionHandler:(void (^)(id result, NSError *error))completionHandler
{
    NSLog(@"url : %@", url);

    if (parameters) {
        
        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
        NSString * jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"request : %@", jsonString);
    }
    
    [self sharedManager].requestSerializer = [AFJSONRequestSerializer serializer];
    [self sharedManager].responseSerializer.acceptableContentTypes = [[self sharedManager].responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [[self sharedManager].requestSerializer setTimeoutInterval:30];  //Time out after 60 seconds
    
    [[self sharedManager] POST:url parameters:parameters progress:^(NSProgress * _Nonnull uploadProgress)
     {} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
         [task cancel];
         
        NSLog(@"response : %@", responseObject);
//         NSLog(@"response received for url: %@", url);
       
         completionHandler (responseObject ,nil);
         
     } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
         
         completionHandler (nil ,error);
     }];
}

+ (void)LoginUser:(NSString *)email
         password:(NSString *)password
      LogDeviceId:(NSString *)logDeviceId
     LogUserAgent:(NSString *)logUserAgent
    LogDeviceType:(NSString *)logDeviceType
       SessionKey:(NSString *)logSessionKey
completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_PASSWORD:password,
                             REST_PARAM_REQUEST_EMAIL:email,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, LOGIN_USER, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                UserModel *user = [UserModel modelObjectWithDictionary:resultArray[0]];
                completionHandler(user, nil);
            }
            else if ([resultDict[@"code"] intValue] == 1) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:1 userInfo:nil]);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"We have issue with server. Please try again." code:401 userInfo:nil]);
        }
    }];
}

+ (void)FBLogin:(NSString *)fb_token
    LogDeviceId:(NSString *)logDeviceId
   LogUserAgent:(NSString *)logUserAgent
  LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_FB_TOKEN:fb_token,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType
                             };
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, FB_LOGIN, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                UserModel *user = [UserModel modelObjectWithDictionary:resultArray[0]];
                completionHandler(user, nil);
            }
            else if ([resultDict[@"code"] intValue] == 1) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:1 userInfo:nil]);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"We have issue with server. Please try again." code:401 userInfo:nil]);
        }
    }];
}
+ (void)GoogleLogin:(NSString *)google_token
    LogDeviceId:(NSString *)logDeviceId
   LogUserAgent:(NSString *)logUserAgent
  LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_GOOGLE_TOKEN:google_token,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType
                             };
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GOOGLE_LOGIN, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                UserModel *user = [UserModel modelObjectWithDictionary:resultArray[0]];
                completionHandler(user, nil);
            }
            else if ([resultDict[@"code"] intValue] == 1) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:1 userInfo:nil]);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"We have issue with server. Please try again." code:401 userInfo:nil]);
        }
    }];
}
//*****************************************************Sender*****************************

+ (void)RegisterSender:(NSString *)name
               Surname:(NSString *)surname
                  Cell:(NSString *)cell
                 Email:(NSString *)email
              Password:(NSString *)password
               Address:(NSString *)address
           CompanyName:(NSString *)company
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
      SendApproveEmail:(NSString *)sendApproveEmail
     completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_REQUEST_NAME:name,
                             REST_PARAM_REQUEST_SURNAME:surname,
                             REST_PARAM_COMMON_CELL:cell,
                             REST_PARAM_COMMON_SEND_APPROVE_EMAIL:sendApproveEmail,
                             REST_PARAM_REQUEST_EMAIL:email,
                             REST_PARAM_REQUEST_PASSWORD:password,
                             REST_PARAM_COMMON_ADDRESS_ACTUAL:address,
                             REST_PARAM_COMMON_COMPANY_NAME:company,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, REGISTER_SENDER, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                
                UserModel *user = [UserModel modelObjectWithDictionary:resultArray[0]];
                completionHandler(user, nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetUserInfo:(NSString *)logSessionKey
        LogDeviceId:(NSString *)logDeviceId
       LogUserAgent:(NSString *)logUserAgent
      LogDeviceType:(NSString *)logDeviceType
  completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler

{
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_USER_INFO, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                
                UserModel *user = [UserModel modelObjectWithDictionary:resultArray[0]];
                completionHandler(user, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetUserInfoById:(NSString *)userId
          LogSessionKey:(NSString *)logSessionKey
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
      completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_COMMON_USER_ID:userId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_USER_INFO_BY_ID, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                
                UserModel *user = [UserModel modelObjectWithDictionary:resultArray[0]];
                completionHandler(user, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)SendForgotPasswordEmail:(NSString *)email
                  LogSessionKey:(NSString *)logSessionKey
                    LogDeviceId:(NSString *)logDeviceId
                   LogUserAgent:(NSString *)logUserAgent
                  LogDeviceType:(NSString *)logDeviceType
              completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_EMAIL:email,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, SEND_FORGOT_PASSWORD_EMAIL, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"inserted_id"], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)CreatePack:(NSString *)sendCatId
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
        SessionKey:(NSString *)logSessionKey
 completionHandler:(void (^)(NSString *packId, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_CP_SEND_CAT_ID:sendCatId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, CREATE_PACK, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"inserted_id"], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)Createpack_PriceInfo:(NSString *)item_value
                Pack_Size_ID:(NSString *)pack_size_id
                    Distance:(NSString *)distance
               Distance_Time:(NSString *)distance_time
              Pack_From_Text:(NSString *)pack_from_text
               Pack_From_Lng:(NSString *)pack_from_lng
               Pack_From_Lat:(NSString *)pack_from_lat
                Pack_To_Text:(NSString *)pack_to_text
                 Pack_To_Lng:(NSString *)pack_to_lng
                 Pack_To_Lat:(NSString *)pack_to_lat
          Is_Insurance_Payed:(NSString *)is_insurance_payed
                  Pack_Price:(NSString *)pack_price
                         Day:(NSString *)day
                        Time:(NSString *)time
              Tariff_Plan_ID:(NSString *)tariff_plan_id
                 Send_Cat_ID:(NSString *)send_cat_id
                Waiting_Mins:(NSString *)waiting_mins
                  Promo_Code:(NSString *)promo_code
                 LogDeviceId:(NSString *)logDeviceID
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(int inserted_id, NSError *error))completionHandler
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setDictionary:@{REST_PARAM_CP_ITEM_VALUE:item_value,
                            REST_PARAM_CP_PACK_SIZE_ID:pack_size_id,
                            REST_PARAM_CP_DISTANCE:distance,
                            REST_PARAM_CP_DISTANCE_TIME:distance_time,
                            REST_PARAM_CP_PACK_FROM_TEXT:pack_from_text,
                            REST_PARAM_CP_PACK_FROM_LNG:pack_from_lng,
                            REST_PARAM_CP_PACK_FROM_LAT:pack_from_lat,
                            REST_PARAM_CP_PACK_TO_TEXT:pack_to_text,
                            REST_PARAM_CP_PACK_TO_LNG:pack_to_lng,
                            REST_PARAM_CP_PACK_TO_LAT:pack_to_lat,
                            REST_PARAM_CP_IS_INSURANCE_PAID:is_insurance_payed,
                            REST_PARAM_CP_PACK_PRICE:pack_price,
                            REST_PARAM_CP_DAY:day,
                            REST_PARAM_CP_TIME:time,
                            REST_PARAM_CP_TARIFF_PLAN_ID:tariff_plan_id,
                            REST_PARAM_CP_SEND_CAT_ID:send_cat_id,
                            REST_PARAM_CP_WAITING_MINS:waiting_mins,
                            REST_PARAM_CP_PROMO_CODE:promo_code,
                            REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceID,
                            REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                            REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                            REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey}];
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, CREATEPACK_PRICEINFO, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler([resultDict[@"inserted_id"] intValue], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
            
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
    
}

+ (void)Updatepack_PriceInfo:(NSString *)item_value
                Pack_Size_ID:(NSString *)pack_size_id
                    Distance:(NSString *)distance
               Distance_Time:(NSString *)distance_time
              Pack_From_Text:(NSString *)pack_from_text
               Pack_From_Lng:(NSString *)pack_from_lng
               Pack_From_Lat:(NSString *)pack_from_lat
                Pack_To_Text:(NSString *)pack_to_text
                 Pack_To_Lng:(NSString *)pack_to_lng
                 Pack_To_Lat:(NSString *)pack_to_lat
          Is_Insurance_Payed:(NSString *)is_insurance_payed
                  Pack_Price:(NSString *)pack_price
                     Pack_Id:(NSString *)pack_id
                         Day:(NSString *)day
                        Time:(NSString *)time
              Tariff_Plan_ID:(NSString *)tariff_plan_id
                 Send_Cat_ID:(NSString *)send_cat_id
                   Waiting_Mins:(NSString *)waiting_mins
                  Promo_Code:(NSString *)promo_code
                 LogDeviceId:(NSString *)logDeviceID
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(int inserted_id, NSError *error))completionHandler
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setDictionary:@{REST_PARAM_CP_ITEM_VALUE:item_value,
                            REST_PARAM_CP_PACK_SIZE_ID:pack_size_id,
                            REST_PARAM_CP_DISTANCE:distance,
                            REST_PARAM_CP_DISTANCE_TIME:distance_time,
                            REST_PARAM_CP_PACK_FROM_TEXT:pack_from_text,
                            REST_PARAM_CP_PACK_FROM_LNG:pack_from_lng,
                            REST_PARAM_CP_PACK_FROM_LAT:pack_from_lat,
                            REST_PARAM_CP_PACK_TO_TEXT:pack_to_text,
                            REST_PARAM_CP_PACK_TO_LNG:pack_to_lng,
                            REST_PARAM_CP_PACK_TO_LAT:pack_to_lat,
                            REST_PARAM_CP_IS_INSURANCE_PAID:is_insurance_payed,
                            REST_PARAM_CP_PACK_PRICE:pack_price,
                            REST_PARAM_CP_PACK_ID:pack_id,
                            REST_PARAM_CP_DAY:day,
                            REST_PARAM_CP_TIME:time,
                            REST_PARAM_CP_TARIFF_PLAN_ID:tariff_plan_id,
                            REST_PARAM_CP_SEND_CAT_ID:send_cat_id,
                            REST_PARAM_CP_PROMO_CODE:promo_code,
                            REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceID,
                            REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                            REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                            REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey}];
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL,UPDATEPACK_PRICEINFO , API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler([resultDict[@"inserted_id"] intValue], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
            
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
    
}

+ (void)InstantPaymentShowPrice:(NSString *)distance
                  Distance_time:(NSString *)distance_time
                     Item_Value:(NSString *)item_value
                   Pack_Size_ID:(NSString *)pack_size_id
                         Cat_Id:(NSString *)cat_id
                            Day:(NSString *)day
                           Time:(NSString *)time
//                 Tariff_Plan_ID:(NSString *)tariff_plan_id
                   Waiting_Mins:(NSString *)waiting_mins
                     Promo_Code:(NSString *)promo_code
                    LogDeviceId:(NSString *)logDeviceID
                   LogUserAgent:(NSString *)logUserAgent
                  LogDeviceType:(NSString *)logDeviceType
              completionHandler:(void (^)(id result, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_CP_DISTANCE:distance,
                             REST_PARAM_CP_DISTANCE_TIME:distance_time,
                             REST_PARAM_CP_ITEM_VALUE:item_value,
                             REST_PARAM_CP_PACK_SIZE_ID:pack_size_id,
                             REST_PARAM_CP_CAT_ID:cat_id,
                             REST_PARAM_CP_DAY:day,
                             REST_PARAM_CP_TIME:time,
//                             REST_PARAM_CP_TARIFF_PLAN_ID:tariff_plan_id,
                             REST_PARAM_CP_WAITING_MINS:waiting_mins,
                             REST_PARAM_CP_PROMO_CODE:promo_code,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceID,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType
                             };
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, INSTANTPAYMENTSHOWPRICE, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                
//                UserModel *user = [UserModel modelObjectWithDictionary:resultArray[0]];
                completionHandler(result, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)DeleteMyDraftPack:(NSString *)packId
            logSessionKey:(NSString *)logSessionKey
              LogDeviceId:(NSString *)logDeviceId
             LogUserAgent:(NSString *)logUserAgent
            LogDeviceType:(NSString *)logDeviceType
        completionHandler:(void (^)(BOOL isDeleted, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, DELETE_MY_DRAFT_PACK, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)UpdatePack_GenInfo:(NSString *)packId
                   PetType:(NSString *)petType
                  PetCount:(NSString *)petCount
                  PetISVac:(NSString *)PetISVac
                  SendName:(NSString *)sendName
                    isPack:(BOOL)isPack
                PackSizeId:(NSString *)packSizeId
             PackOptionsId:(NSString *)packOptionsId
                PackWeight:(NSString *)packWeight
                 ItemValue:(NSString *)itemValue
               Description:(NSString *)description
              ReceiverName:(NSString *)receiverName
       ReceiverPhoneNumber:(NSString *)receiverPhoneNumber
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
                SessionKey:(NSString *)logSessionKey
         completionHandler:(void (^)(int packId, NSError *error))completionHandler
{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setDictionary:@{REST_PARAM_CP_PACK_ID:packId,
                            REST_PARAM_CP_SEND_NAME:sendName,
                            REST_PARAM_CP_PACK_SIZE_ID:packSizeId,
                            REST_PARAM_CP_PACK_OPTION_ID:packOptionsId,
                            REST_PARAM_CP_PACK_WEIGHT:packWeight,
                            REST_PARAM_CP_ITEM_VALUE:itemValue,
                            REST_PARAM_CP_DESC_TEXT:description,
                            REST_PARAM_CP_RECEIVER_NAME:receiverName,
                            REST_PARAM_CP_RECEIVER_PHONE_NUMBER:receiverPhoneNumber,
                            REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                            REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                            REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                            REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey}];
    if (!isPack) {
        
        [params setObject:petType forKey:REST_PARAM_CP_PET_TYPE];
        [params setObject:petCount forKey:REST_PARAM_CP_PET_COUNT];
        [params setObject:PetISVac forKey:REST_PARAM_CP_PET_IS_VAC];
    }
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, UPDATE_PACK_GENINFO, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler([resultDict[@"inserted_id"] intValue], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
            
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)CreatePack_GenInfo:(NSString *)packId
                   PetType:(NSString *)petType
                  PetCount:(NSString *)petCount
                  PetISVac:(NSString *)PetISVac
                  SendName:(NSString *)sendName
                    isPack:(BOOL)isPack
                PackSizeId:(NSString *)packSizeId
             PackOptionsId:(NSString *)packOptionsId
                PackWeight:(NSString *)packWeight
                 ItemValue:(NSString *)itemValue
               Description:(NSString *)description
              ReceiverName:(NSString *)receiverName
       ReceiverPhoneNumber:(NSString *)receiverPhoneNumber
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
                SessionKey:(NSString *)logSessionKey
         completionHandler:(void (^)(int packId, NSError *error))completionHandler
{
    
    NSMutableDictionary *params = [NSMutableDictionary new];
    [params setDictionary:@{REST_PARAM_CP_PACK_ID:packId,
                            REST_PARAM_CP_SEND_NAME:sendName,
                            REST_PARAM_CP_PACK_SIZE_ID:packSizeId,
                            REST_PARAM_CP_PACK_OPTION_ID:packOptionsId,
                            REST_PARAM_CP_PACK_WEIGHT:packWeight,
                            REST_PARAM_CP_ITEM_VALUE:itemValue,
                            REST_PARAM_CP_DESC_TEXT:description,
                            REST_PARAM_CP_RECEIVER_NAME:receiverName,
                            REST_PARAM_CP_RECEIVER_PHONE_NUMBER:receiverPhoneNumber,
                            REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                            REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                            REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                            REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey}];
    if (!isPack) {
        
        [params setObject:petType forKey:REST_PARAM_CP_PET_TYPE];
        [params setObject:petCount forKey:REST_PARAM_CP_PET_COUNT];
        [params setObject:PetISVac forKey:REST_PARAM_CP_PET_IS_VAC];
    }
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, CREATE_PACK_GENINFO, API_VERSION];
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler([resultDict[@"inserted_id"] intValue], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
            
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}


+ (void)AddPackImage:(NSString *)insertedId
     PackImageBase64:(NSString *)base64Image
       LogSessionKey:(NSString *)logSessionKey
         LogDeviceId:(NSString *)logDeviceId
        LogUserAgent:(NSString *)logUserAgent
       LogDeviceType:(NSString *)logDeviceType
   completionHandler:(void (^)(AddImageObject *addImage, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:insertedId,
                             REST_PARAM_CP_BASE_64_IMAGE:base64Image,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey
                             };
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, ADD_PACK_IMAGE, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                AddImageObject *addImage = [AddImageObject new];
                addImage.insertedId = resultDict[@"inserted_id"];
                addImage.imageName = resultDict[@"details"][@"pack_img"];
                addImage.isUploaded = NO;
                completionHandler(addImage, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)DeletePackImage:(NSString *)insertedId
          PackImageName:(NSString *)packImageName
          LogSessionKey:(NSString *)logSessionKey
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
      completionHandler:(void (^)(BOOL isDeleted, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_DELETE_IMAGE_ID:insertedId,
                             REST_PARAM_CP_BASE_64_IMAGE:packImageName,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, DELETE_PACK_IMAGE];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(NO, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)SendApproveEmailToSender:(NSString *)logSessionKey
                     LogDeviceId:(NSString *)logDeviceId
                    LogUserAgent:(NSString *)logUserAgent
                   LogDeviceType:(NSString *)logDeviceType
               completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, SEND_APPROVE_EMAIL_TO_SENDER, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)ChangePassword2:(NSString *)currentPassword
            NewPassword:(NSString *)newPassword
         ReTypePassword:(NSString *)ReTypePassword
          logSessionKey:(NSString *)logSessionKey
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
      completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_OLD_PASSWORD:currentPassword,
                             REST_PARAM_CP_NEW_PASSWORD:newPassword,
                             REST_PARAM_CP_RE_TYPE_PASSWORD:ReTypePassword,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, CHANGE_PASSWORD_2, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)UpdatePack_Destination:(NSString *)packId
                   PackFromLat:(NSString *)packFromLat
                   PackFromLng:(NSString *)packFromLng
                  PackFromText:(NSString *)packFromText
                     PackToLat:(NSString *)packToLat
                     PackToLng:(NSString *)packToLng
                    PackToText:(NSString *)packToText
                      Distance:(NSString *)distance
                  DistanceTime:(NSString *)distanceTime
                     TakenTime:(NSString *)takenTime
                     TakenType:(NSString *)takenType
                     TakenAsap:(NSString *)takenAsap
                  DeliveryTime:(NSString *)deliveryTime
                  DeliveryType:(NSString *)deliveryType
                  DeliveryAsap:(NSString *)deliveryAsap
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
                    SessionKey:(NSString *)logSessionKey
             completionHandler:(void (^)(SPPack *pack, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_CP_PACK_FROM_LAT:packFromLat,
                             REST_PARAM_CP_PACK_FROM_LNG:packFromLng,
                             REST_PARAM_CP_PACK_FROM_TEXT:packFromText,
                             REST_PARAM_CP_PACK_TO_LAT:packToLat,
                             REST_PARAM_CP_PACK_TO_LNG:packToLng,
                             REST_PARAM_CP_PACK_TO_TEXT:packToText,
                             REST_PARAM_CP_DISTANCE:distance,
                             REST_PARAM_CP_DISTANCE_TIME:distanceTime,
                             REST_PARAM_CP_TAKEN_TIME:takenTime,
                             REST_PARAM_CP_TAKEN_TYPE:takenType,
                             REST_PARAM_CP_TAKEN_ASAP:takenAsap,
                             REST_PARAM_CP_DELIVERY_TIME:deliveryTime,
                             REST_PARAM_CP_DELIVERY_TYPE:deliveryType,
                             REST_PARAM_CP_DELIVERY_ASAP:deliveryAsap,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, UPDATE_PACK_DESTINATION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                SPPack *pack = [SPPack new];
                pack.packPriceWithIns = resultDict[@"details"][@"pack_price_with_ins"];
                pack.packInsuranceMode = resultDict[@"details"][@"pack_insurance_mode"];
                pack.packPriceWithoutIns = resultDict[@"details"][@"pack_price_without_ins"];
                pack.packInsurancePrice = resultDict[@"details"][@"pack_insurance_price"];

                completionHandler(pack, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)UpdatePack_Description:(NSString *)packId
                     Desc_Text:(NSString *)desc_text
                 LogSessionKey:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
             completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_CP_DESC_TEXT:desc_text,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, UPDATEPACK_DESCRIPTION, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)UpdatePack_SendingName:(NSString *)packId
                     Send_Name:(NSString *)send_name
                 LogSessionKey:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
             completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_CP_SEND_NAME:send_name,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, UPDATEPACK_SENDINGNAME, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}


+ (void)payForPack:(NSString *)packId
         PackPrice:(NSString *)packPrice
   IsInsurancePaid:(NSString *)isInsurancePaid
        StripToken:(NSString *)stripToken
      cardLastFour:(NSString *)lastFourDigits
        Taken_asap:(NSString *)taken_asap
        Taken_Time:(NSString *)taken_time
         Send_Name:(NSString *)send_name
         Desc_Text:(NSString *)desc_text
     Receiver_Name:(NSString *)receiver_name
Receiver_Phone_Number:(NSString *)receiver_phone_number
     LogSessionKey:(NSString *)logSessionKey
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(NSString *orderId, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_CP_PACK_PRICE:packPrice,
                             REST_PARAM_CP_IS_INSURANCE_PAID:isInsurancePaid,
                             REST_PARAM_CP_STRIP_TOKEN:stripToken,
                             REST_PARAM_CP_CARD_LAST_FOUR:lastFourDigits,
                             REST_PARAM_CP_TAKEN_ASAP:taken_asap,
//                             REST_PARAM_CP_TAKEN_TIME:taken_time,
                             REST_PARAM_CP_SEND_NAME:send_name,
                             REST_PARAM_CP_DESC_TEXT:desc_text,
                             REST_PARAM_CP_RECEIVER_NAME:receiver_name,
                             REST_PARAM_CP_RECEIVER_PHONE_NUMBER:receiver_phone_number,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    if ([stripToken isEqualToString:@""]) {
        
        params = @{REST_PARAM_CP_PACK_ID:packId,
                   REST_PARAM_CP_PACK_PRICE:packPrice,
                   REST_PARAM_CP_IS_INSURANCE_PAID:isInsurancePaid,
                   REST_PARAM_CP_CARD_LAST_FOUR:lastFourDigits,
                   REST_PARAM_CP_TAKEN_ASAP:taken_asap,
                   REST_PARAM_CP_TAKEN_TIME:taken_time,
                   REST_PARAM_CP_SEND_NAME:send_name,
                   REST_PARAM_CP_DESC_TEXT:desc_text,
                   REST_PARAM_CP_RECEIVER_NAME:receiver_name,
                   REST_PARAM_CP_RECEIVER_PHONE_NUMBER:receiver_phone_number,
                   REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                   REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                   REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                   REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    }
    
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, PAY_FOR_PACK, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(@"", nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)PayForAskedPackByEmail:(NSString *)packId
                   Payor_Email:(NSString *)payor_email
                  cardLastFour:(NSString *)lastFourDigits
                    StripToken:(NSString *)stripToken
                 LogSessionKey:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(NSString *orderId, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_CP_PAYOR_EMAIL:payor_email,
                             REST_PARAM_CP_CARD_LAST_FOUR:lastFourDigits,
                             REST_PARAM_CP_STRIP_TOKEN:stripToken,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, PAY_FOR_ASKEDPACK_BYEMAIL, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(@"", nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)AskFriendToPay:(NSString *)packId
             PackPrice:(NSString *)packPrice
       IsInsurancePaid:(NSString *)isInsurancePaid
            Taken_asap:(NSString *)taken_asap
            Taken_Time:(NSString *)taken_time
             Send_Name:(NSString *)send_name
             Desc_Text:(NSString *)desc_text
         Receiver_Name:(NSString *)receiver_name
 Receiver_Phone_Number:(NSString *)receiver_phone_number
           Payor_Email:(NSString *)payor_email
         LogSessionKey:(NSString *)logSessionKey
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
     completionHandler:(void (^)(NSString *orderId, NSError *error))completionHandler{
    
    NSDictionary *params = @{
                             REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_CP_PACK_PRICE:packPrice,
                             REST_PARAM_CP_IS_INSURANCE_PAID:isInsurancePaid,
                             REST_PARAM_CP_TAKEN_ASAP:taken_asap,
//                             REST_PARAM_CP_TAKEN_TIME:taken_time,
                             REST_PARAM_CP_SEND_NAME:send_name,
                             REST_PARAM_CP_DESC_TEXT:desc_text,
                             REST_PARAM_CP_RECEIVER_NAME:receiver_name,
                             REST_PARAM_CP_RECEIVER_PHONE_NUMBER:receiver_phone_number,
                             REST_PARAM_CP_PAYOR_EMAIL:payor_email,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, ASK_FRIEND_TO_PAY, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(@"", nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)AskFriendToPayInstant:(NSString *)email
                 phone_number:(NSString *)phone_number
                    Send_Name:(NSString *)send_name
                    Desc_Text:(NSString *)desc_text
                Receiver_Name:(NSString *)receiver_name
        Receiver_Phone_Number:(NSString *)receiver_phone_number
                    PackPrice:(NSString *)packPrice
                   Item_Value:(NSString *)item_value
                 Pack_Size_Id:(NSString *)pack_size_id
                     Distance:(NSString *)distance
                Distance_Time:(NSString *)distance_time
                  Send_Cat_Id:(NSString *)send_cat_id
               Tariff_Plan_Id:(NSString *)tariff_plan_id
               Pack_From_Text:(NSString *)pack_from_text
                Pack_From_Lng:(NSString *)pack_from_lng
                Pack_From_Lat:(NSString *)pack_from_lat
                 Pack_To_Text:(NSString *)pack_to_text
                  Pack_To_Lng:(NSString *)pack_to_lng
                  Pack_To_Lat:(NSString *)pack_to_lat
                         Name:(NSString *)name
                      SurName:(NSString *)surname
                 cardLastFour:(NSString *)lastFourDigits
                          Day:(NSString *)day
                         Time:(NSString *)time
                 Waiting_Mins:(NSString *)waiting_mins
                   Promo_Code:(NSString *)promo_code
                  Payor_Email:(NSString *)payor_email
                LogSessionKey:(NSString *)logSessionKey
                  LogDeviceId:(NSString *)logDeviceId
                 LogUserAgent:(NSString *)logUserAgent
                LogDeviceType:(NSString *)logDeviceType
            completionHandler:(void (^)(NSString *orderId, NSError *error))completionHandler{
    
    NSDictionary *params = @{
                             REST_PARAM_REQUEST_EMAIL:email,
                             REST_PARAM_CP_PHONE_NUMBER:phone_number,
                             REST_PARAM_CP_SEND_NAME:send_name,
                             REST_PARAM_CP_DESC_TEXT:desc_text,
                             REST_PARAM_CP_RECEIVER_NAME:receiver_name,
                             REST_PARAM_CP_RECEIVER_PHONE_NUMBER:receiver_phone_number,
                             REST_PARAM_CP_PACK_PRICE:packPrice,
//                             REST_PARAM_CP_STRIP_TOKEN:stripToken,
                             REST_PARAM_CP_ITEM_VALUE:item_value,
                             REST_PARAM_CP_PACK_SIZE_ID:pack_size_id,
                             REST_PARAM_CP_TARIFF_PLAN_ID:tariff_plan_id,
                             REST_PARAM_CP_DISTANCE:distance,
                             REST_PARAM_CP_DISTANCE_TIME:distance_time,
                             REST_PARAM_CP_SEND_CAT_ID:send_cat_id,
                             REST_PARAM_CP_PACK_FROM_TEXT:pack_from_text,
                             REST_PARAM_CP_PACK_FROM_LNG:pack_from_lng,
                             REST_PARAM_CP_PACK_FROM_LAT:pack_from_lat,
                             REST_PARAM_CP_PACK_TO_TEXT:pack_to_text,
                             REST_PARAM_CP_PACK_TO_LNG:pack_to_lng,
                             REST_PARAM_CP_PACK_TO_LAT:pack_to_lat,
                             REST_PARAM_COMMON_NAME:name,
                             REST_PARAM_COMMON_SURNAME:surname,
                             REST_PARAM_CP_CARD_LAST_FOUR:lastFourDigits,
                             REST_PARAM_CP_DAY:day,
                             REST_PARAM_CP_TIME:time,
                             REST_PARAM_CP_WAITING_MINS:waiting_mins,
                             REST_PARAM_CP_PROMO_CODE:promo_code,
                             REST_PARAM_CP_PAYOR_EMAIL:payor_email,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, ASK_FRIEND_TO_PAY_INSTANT, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"inserted_id"], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)MakeInstantPayment:(NSString *)email
              Phone_Number:(NSString *)phone_number
                 Send_Name:(NSString *)send_name
                 Desc_Text:(NSString *)desc_text
             Receiver_Name:(NSString *)receiver_name
     Receiver_Phone_Number:(NSString *)receiver_phone_number
                 PackPrice:(NSString *)packPrice
                StripToken:(NSString *)stripToken
                Item_Value:(NSString *)item_value
              Pack_Size_ID:(NSString *)pack_size_id
       Default_Tariff_Plan:(NSString *)default_tariff_plan
                  Distance:(NSString *)distance
             Distance_Time:(NSString *)distance_time
               Send_Cat_ID:(NSString *)send_cat_id
            Pack_From_Text:(NSString *)pack_from_text
             Pack_From_Lng:(NSString *)pack_from_lng
             Pack_From_Lat:(NSString *)pack_from_lat
              Pack_To_Text:(NSString *)pack_to_text
               Pack_To_Lng:(NSString *)pack_to_lng
               Pack_To_Lat:(NSString *)pack_to_lat
                      Name:(NSString *)name
                   SurName:(NSString *)surname
              cardLastFour:(NSString *)lastFourDigits
                       Day:(NSString *)day
                      Time:(NSString *)time
              Waiting_Mins:(NSString *)waiting_mins
                Promo_Code:(NSString *)promo_code
//             LogSessionKey:(NSString *)logSessionKey
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(NSString *orderId, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_REQUEST_EMAIL:email,
                             REST_PARAM_CP_PHONE_NUMBER:phone_number,
                             REST_PARAM_CP_SEND_NAME:send_name,
                             REST_PARAM_CP_DESC_TEXT:desc_text,
                             REST_PARAM_CP_RECEIVER_NAME:receiver_name,
                             REST_PARAM_CP_RECEIVER_PHONE_NUMBER:receiver_phone_number,
                             REST_PARAM_CP_PACK_PRICE:packPrice,
                             REST_PARAM_CP_STRIP_TOKEN:stripToken,
                             REST_PARAM_CP_ITEM_VALUE:item_value,
                             REST_PARAM_CP_PACK_SIZE_ID:pack_size_id,
                             REST_PARAM_CP_TARIFF_PLAN_ID:default_tariff_plan,
                             REST_PARAM_CP_DISTANCE:distance,
                             REST_PARAM_CP_DISTANCE_TIME:distance_time,
                             REST_PARAM_CP_SEND_CAT_ID:send_cat_id,
                             REST_PARAM_CP_PACK_FROM_TEXT:pack_from_text,
                             REST_PARAM_CP_PACK_FROM_LNG:pack_from_lng,
                             REST_PARAM_CP_PACK_FROM_LAT:pack_from_lat,
                             REST_PARAM_CP_PACK_TO_TEXT:pack_to_text,
                             REST_PARAM_CP_PACK_TO_LNG:pack_to_lng,
                             REST_PARAM_CP_PACK_TO_LAT:pack_to_lat,
                             REST_PARAM_COMMON_NAME:name,
                             REST_PARAM_COMMON_SURNAME:surname,
                             REST_PARAM_CP_CARD_LAST_FOUR:lastFourDigits,
                             REST_PARAM_CP_DAY:day,
                             REST_PARAM_CP_TIME:time,
                             REST_PARAM_CP_WAITING_MINS:waiting_mins,
                             REST_PARAM_CP_PROMO_CODE:promo_code,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
//                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey
                             };
    
    if ([stripToken isEqualToString:@""]) {
        
        params = @{REST_PARAM_REQUEST_EMAIL:email,
                   REST_PARAM_CP_PHONE_NUMBER:phone_number,
                   REST_PARAM_CP_SEND_NAME:send_name,
                   REST_PARAM_CP_DESC_TEXT:desc_text,
                   REST_PARAM_CP_RECEIVER_NAME:receiver_name,
                   REST_PARAM_CP_RECEIVER_PHONE_NUMBER:receiver_phone_number,
                   REST_PARAM_CP_PACK_PRICE:packPrice,
                   REST_PARAM_CP_ITEM_VALUE:item_value,
                   REST_PARAM_CP_PACK_SIZE_ID:pack_size_id,
                   REST_PARAM_CP_DISTANCE:distance,
                   REST_PARAM_CP_DISTANCE_TIME:distance_time,
                   REST_PARAM_CP_SEND_CAT_ID:send_cat_id,
                   REST_PARAM_CP_PACK_FROM_TEXT:pack_from_text,
                   REST_PARAM_CP_PACK_FROM_LNG:pack_from_lng,
                   REST_PARAM_CP_PACK_FROM_LAT:pack_from_lat,
                   REST_PARAM_CP_PACK_TO_TEXT:pack_to_text,
                   REST_PARAM_CP_PACK_TO_LNG:pack_to_lng,
                   REST_PARAM_CP_PACK_TO_LAT:pack_to_lat,
                   REST_PARAM_COMMON_NAME:name,
                   REST_PARAM_COMMON_SURNAME:surname,
                   REST_PARAM_CP_CARD_LAST_FOUR:lastFourDigits,
                   REST_PARAM_CP_DAY:day,
                   REST_PARAM_CP_TIME:time,
                   REST_PARAM_CP_WAITING_MINS:waiting_mins,
                   REST_PARAM_CP_PROMO_CODE:promo_code,
                   REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                   REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                   REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
//                   REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey
                   
                   };
    }
    
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, MAKE_INSTANT_PAYMENT, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"inserted_id"], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}



+ (void)GetPackDetails:(NSString *)packId
         IncludeImages:(NSString *)includeImages
        IncludeBidders:(NSString *)includeBidders
                 Start:(NSString *)start
                 Count:(NSString *)count
         LogSessionKey:(NSString *)logSessionKey
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
     completionHandler:(void (^)(PackDetailsModel *packDetails, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             @"include_tips":@"1",
                             REST_PARAM_GET_PACK_INCLUDE_IMAGES:includeImages,
                             REST_PARAM_GET_PACK_INCLUDE_BIDDERS:includeBidders,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_PACK_DETAILS, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                
                PackDetailsModel *packDetails = [PackDetailsModel modelObjectWithDictionary:resultArray[0]];
                completionHandler(packDetails, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)CancelPack:(NSString *)packId
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
        SessionKey:(NSString *)logSessionKey
 completionHandler:(void (^)(BOOL isPackCancelled, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, CANCEL_PACK, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)SenderReportProblem:(NSString *)packId
                 ReasonText:(NSString *)reasonText
                   ReasonId:(NSString *)reasonId
                LogDeviceId:(NSString *)logDeviceId
               LogUserAgent:(NSString *)logUserAgent
              LogDeviceType:(NSString *)logDeviceType
                 SessionKey:(NSString *)logSessionKey
          completionHandler:(void (^)(BOOL isPackCancelled, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_PR_REASON_TEXT:reasonText,
                             REST_PARAM_PR_REASON_ID:reasonId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, SENDER_PEPORT_PROBLEM];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)SenderGetPacksList:(NSString *)start
                     count:(NSString *)count
                      List:(SenderPackList)packList
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
                SessionKey:(NSString *)logSessionKey
         completionHandler:(void (^)(SenderPacksList *senderPackList, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_PL_COUNT:count,
                             REST_PARAM_PL_START:start,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = @"";
    
    if (packList == SENDER_ACTIVE) {
        
        baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, SENDER_GET_ACTIVE_PACKS, API_VERSION];
        
    }
    else if (packList == SENDER_COMPLETED) {
        
        baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, SENDER_GET_COMPLETED_PACKS, API_VERSION];
        
    }
    else if (packList == SENDER_DRAFT) {
        
        baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, SENDER_GET_DRAFT_PACKS, API_VERSION];
        
    }
    else if (packList == SENDER_RECEIVE) {
        
        baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, SENDER_GET_RECEIVED_PACKS, API_VERSION];
    }
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                SenderPacksList *senderPacks = [SenderPacksList modelObjectWithDictionary:resultDict];
                completionHandler(senderPacks, nil);
                
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)UpdateSender:(NSString *)name
             Surname:(NSString *)surname
                Cell:(NSString *)cell
             Address:(NSString *)address
         UseLocation:(NSString *)useLocation
              Gender:(NSString *)gender
       LogSessionKey:(NSString *)logSessionKey
         LogDeviceId:(NSString *)logDeviceId
        LogUserAgent:(NSString *)logUserAgent
       LogDeviceType:(NSString *)logDeviceType
   completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_NAME:name,
                             REST_PARAM_REQUEST_SURNAME:surname,
                             REST_PARAM_COMMON_CELL:cell,
                             REST_PARAM_COMMON_ADDRESS_ACTUAL:address,
                             REST_PARAM_REQUEST_LOCATION_USE:useLocation,
                             REST_PARAM_REQUEST_GENDER:gender,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, UPDATE_SENDER, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else {
                
                completionHandler(NO, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(NO, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}


//*****************************************************Sender End*****************************

//*****************************************************Courier*****************************

#pragma mark -- Courier services
+ (void)RegisterCourierGenInfo:(NSString *)name
                       Surname:(NSString *)surname
                          Cell:(NSString *)cell
                   ReferalName:(NSString *)referalName
                   ReferalCell:(NSString *)referalCell
                         Email:(NSString *)email
                      Password:(NSString *)password
               SendApproveMail:(NSString *)approveEmail
                 LogSessionKey:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
             completionHandler:(void (^)(NSString *sessionKey, NSString *userId, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_NAME:name,
                             REST_PARAM_REQUEST_SURNAME:surname,
                             REST_PARAM_COMMON_CELL:cell,
                             REST_PARAM_REQUEST_REFERAL_NAME:referalName,
                             REST_PARAM_REQUEST_REFERAL_CELL:referalCell,
                             REST_PARAM_REQUEST_EMAIL:email,
                             REST_PARAM_REQUEST_PASSWORD:password,
                             REST_PARAM_COMMON_SEND_APPROVE_EMAIL:approveEmail,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, REGISTER_COURIER_GEN_INFO];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSDictionary *resultDic = resultDict[@"data"][0];
                completionHandler(resultDic[@"log_session_key"], resultDic[@"id"], nil);
            } else if ([resultDict[@"code"] intValue] == 6) {
                
                completionHandler(nil,nil, [NSError errorWithDomain:resultDict[@"short"] code:101 userInfo:nil]);

            }
            else {
                
                completionHandler(nil,nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil,nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)UpdateCourierIDCard:(NSString *)idCardImage1
               IdCardImage1:(NSString *)idCardImage2
               IdCardNumber:(NSString *)idCardNumber
           IdCardExpiryDate:(NSString *)idCardExpiryDate
              AddressActual:(NSString *)addressActual
            AddressOfficial:(NSString *)addressOfficial
                IDCardState:(NSString *)idCard_State
                 SSNNumaber:(NSString *)SSNNumaber
                     Gender:(NSString *)gender
                UseOfficial:(NSString *)useOfficial
              LogSessionKey:(NSString *)logSessionKey
                LogDeviceId:(NSString *)logDeviceId
               LogUserAgent:(NSString *)logUserAgent
              LogDeviceType:(NSString *)logDeviceType
          completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    
    NSMutableDictionary *paramsDict = [NSMutableDictionary new];
    
    [paramsDict setDictionary:@{REST_PARAM_REQUEST_ID_CARD_IMAGE_1:idCardImage1,
                                REST_PARAM_REQUEST_ID_CARD_NUMBER:idCardNumber,
                                REST_PARAM_COMMON_ID_CARD_EXP_DATE:idCardExpiryDate,
                                REST_PARAM_REQUEST_ACTUAL_ADDRESS:addressActual,
                                REST_PARAM_REQUEST_OFFICIAL_ADDRESS:addressOfficial,
                                REST_PARAM_REQUEST_ID_CARD_STATE:idCard_State,
                                REST_PARAM_REQUEST_SSN_NUMBER:SSNNumaber,
                                REST_PARAM_REQUEST_GENDER:gender,
                                REST_PARAM_REQUEST_USE_OFFICIAL:useOfficial,
                                REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                                REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                                REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                                REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType}];
    
    if(![idCardImage2 isEqualToString:@""])
    {
        [paramsDict setObject:idCardImage2 forKey:REST_PARAM_REQUEST_ID_CARD_IMAGE_2];
    }
    
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:paramsDict];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, UPDATE_COURIER_GEN_ID_CARD,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"inserted_id"], nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)SelfImage:(NSString *)selfImage
    LogSessionKey:(NSString *)logSessionKey
      LogDeviceId:(NSString *)logDeviceId
     LogUserAgent:(NSString *)logUserAgent
    LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(NSString *imageName, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_SELF_IMAGE:selfImage,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, UPDATE_USER_SELF_IMAGE,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"details"][@"self_img"], nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetUsersTransportTypes:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
             completionHandler:(void (^)(NSDictionary *usersTransportTypes, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_USER_TRANSPORTS,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"inserted_id"], nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetTransportImagesByTransportId:(NSString *)transportId
                          LogSessionKey:(NSString *)logSessionKey
                            LogDeviceId:(NSString *)logDeviceId
                           LogUserAgent:(NSString *)logUserAgent
                          LogDeviceType:(NSString *)logDeviceType
                      completionHandler:(void (^)(NSMutableArray *imagesArray, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_COMMON_TRANSPORT_ID:transportId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_TRANSPORT_IMAGES_BY_TRANSPORT_ID, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *reponse = resultDict[@"data"];
                NSMutableArray *images = [NSMutableArray new];
                
                for (NSDictionary *dict in reponse) {
                    
                    AddImageObject *addImage = [AddImageObject new];
                    addImage.insertedId = dict[@"transport_id"];
                    addImage.imageName = dict[@"transport_img"];
                    addImage.isUploaded = YES;
                    [images addObject:addImage];
                }
                
                completionHandler(images, nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)AddNewTransport:(NSString *)transportType
          TransportYear:(NSString *)transportYear
         TransportModel:(NSString *)transportModel
          TransportMake:(NSString *)transportMake
            PlateNumber:(NSString *)plateNumber
        IsUpdateVehicel:(BOOL)isUpdateVehicle
            transportId:(NSString *)transportId
          LogSessionKey:(NSString *)logSessionKey
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType

      completionHandler:(void (^)(NSString *insertedId, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_TRANSPORT_TYPE:transportType,
                             REST_PARAM_REQUEST_TRANSPORT_YEAR:transportYear,
                             REST_PARAM_REQUEST_TRANSPORT_MODEL:transportModel,
                             REST_PARAM_REQUEST_TRANSPORT_PLATE_NUMBER:plateNumber,
                             REST_PARAM_REQUEST_TRANSPORT_COLOR:@"red",
                             REST_PARAM_REQUEST_TRANSPORT_MAKE:transportMake,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, ADD_NEW_TRANSPORT,API_VERSION];
    
    if (isUpdateVehicle) {
        
        params = @{REST_PARAM_COMMON_TRANSPORT_ID:transportId,
                   REST_PARAM_REQUEST_TRANSPORT_TYPE:transportType,
                   REST_PARAM_REQUEST_TRANSPORT_YEAR:transportYear,
                   REST_PARAM_REQUEST_TRANSPORT_MODEL:transportModel,
                   REST_PARAM_REQUEST_TRANSPORT_PLATE_NUMBER:plateNumber,
                   REST_PARAM_REQUEST_TRANSPORT_COLOR:@"red",
                   REST_PARAM_REQUEST_TRANSPORT_MAKE:transportMake,
                   REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                   REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                   REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                   REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
        
        baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, UPDATE_USER_TRANSPORT,API_VERSION];
        
    }
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"inserted_id"], nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)AddNewTransportImage:(NSString *)transportId
              TransportImage:(NSString *)TransportImage
               LogSessionKey:(NSString *)logSessionKey
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
           completionHandler:(void (^)(NSString *transportImage, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_COMMON_TRANSPORT_ID:transportId,
                             REST_PARAM_REQUEST_TRANSPORT_IMAGE:TransportImage,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, ADD_NEW_TRANSPORT_IMAGE,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"inserted_id"], nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)DeleteTransportImageById:(NSString *)transportId
                   LogSessionKey:(NSString *)logSessionKey
                     LogDeviceId:(NSString *)logDeviceId
                    LogUserAgent:(NSString *)logUserAgent
                   LogDeviceType:(NSString *)logDeviceType
               completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{@"id":transportId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, DELETE_TRANSPORT_IMAGE_BY_ID,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"inserted_id"], nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)AddUserIns:(NSString *)userId
InsurancePolicyNumber:(NSString *)insurancePolicyNumber
InsuranceExpiryDate:(NSString *)insuranceExpiryDate
   InsuranceImage1:(NSString *)InsuranceImage1
   InsuranceImage2:(NSString *)InsuranceImage2
     LogSessionKey:(NSString *)logSessionKey
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    
    NSMutableDictionary *paramsDict = [NSMutableDictionary new];
    
    [paramsDict setDictionary:@{REST_PARAM_COMMON_USER_ID:userId,
                            REST_PARAM_REQUEST_INSURANCE_POLICY_NUMBER:insurancePolicyNumber,
                            REST_PARAM_REQUEST_LIS_EXP_DATE:insuranceExpiryDate,
                            REST_PARAM_REQUEST_INSURANCE_IMAGE_1:InsuranceImage1,
                            REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                            REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                            REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                            REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey}];
    
    if(![InsuranceImage2 isEqualToString:@""])
    {
        [paramsDict setObject:InsuranceImage2 forKey:REST_PARAM_REQUEST_INSURANCE_IMAGE_2];
    }

    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:paramsDict];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, UPDATE_USER_INS_GEN];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetUserTransports:(NSString *)logSessionKey
              LogDeviceId:(NSString *)logDeviceId
             LogUserAgent:(NSString *)logUserAgent
            LogDeviceType:(NSString *)logDeviceType
        completionHandler:(void (^)(NSMutableArray *usersTransports, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_USER_TRANSPORTS,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"];
                NSMutableArray *transports = [NSMutableArray new];
                
                for (NSDictionary *reposneDict in response) {
                    
                    GetTransports *transport = [GetTransports modelObjectWithDictionary:reposneDict];
                    [transports addObject:transport];
                }
                completionHandler(transports, nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetUserIns:(NSString *)userId
     LogSessionKey:(NSString *)logSessionKey
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(GetCourierInsuranceGen *courierInsGen, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_COMMON_USER_ID:userId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GET_USER_INS_GEN];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                GetCourierInsuranceGen *courierGen = [GetCourierInsuranceGen modelObjectWithDictionary:resultDict[@"data"][0]];
                completionHandler(courierGen, nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)SendApproveEmailToCourier:(NSString *)logSessionKey
                      LogDeviceId:(NSString *)logDeviceId
                     LogUserAgent:(NSString *)logUserAgent
                    LogDeviceType:(NSString *)logDeviceType
                completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, SEND_APPROVE_EMAIL_TO_COURIER, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)UpdateUserAvailStatus:(NSString *)availStatus
                LogSessionKey:(NSString *)logSessionKey
                  LogDeviceId:(NSString *)logDeviceId
                 LogUserAgent:(NSString *)logUserAgent
                LogDeviceType:(NSString *)logDeviceType
            completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_DL_AVAIL_STATUS:availStatus,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, UPDATE_USER_AVAIL_STATUS];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)UpdateUserGeoLocation:(NSString *)userLat
                      UserLng:(NSString *)userLng
                LogSessionKey:(NSString *)logSessionKey
                  LogDeviceId:(NSString *)logDeviceId
                 LogUserAgent:(NSString *)logUserAgent
                LogDeviceType:(NSString *)logDeviceType
            completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_DL_LAST_LAT:userLat,
                             REST_PARAM_DL_LAST_LNG:userLng,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, UPDATE_USER_GEO_LOCATION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)CourierGetPendingPacks:(NSString *)start
                         count:(NSString *)count
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
                    SessionKey:(NSString *)logSessionKey
             completionHandler:(void (^)(CourierPacksList *courierPackList, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_PL_COUNT:count,
                             REST_PARAM_PL_START:start,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, COURIER_GET_PENDING_PACKS];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                CourierPacksList *courierPacks = [CourierPacksList modelObjectWithDictionary:resultDict];
                completionHandler(courierPacks, nil);
                
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)CourierGetPacksList:(NSString *)start
                      count:(NSString *)count
                       List:(MyDeliverySelection)packList
                LogDeviceId:(NSString *)logDeviceId
               LogUserAgent:(NSString *)logUserAgent
              LogDeviceType:(NSString *)logDeviceType
                 SessionKey:(NSString *)logSessionKey
          completionHandler:(void (^)(CourierPacksList *courierPackList, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_PL_COUNT:count,
                             REST_PARAM_PL_START:start,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = @"";
    
    if (packList == Active) {
        
        baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, COURIER_GET_ACTIVE_PACKS,API_VERSION];
        
    }
    else if (packList == Completed) {
        
        baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, COURIER_GET_COMPLETED_PACKS,API_VERSION];
        
    }
    else if (packList == New) {
        
        baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, COURIER_GET_NEW_PACKS,API_VERSION];
    }
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                CourierPacksList *courierPacks = [CourierPacksList modelObjectWithDictionary:resultDict];
                completionHandler(courierPacks, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)TakePack:(NSString *)packId
     LogDeviceId:(NSString *)logDeviceId
    LogUserAgent:(NSString *)logUserAgent
   LogDeviceType:(NSString *)logDeviceType
      SessionKey:(NSString *)logSessionKey
completionHandler:(void (^)(BOOL isTookPack, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, COURIER_TAKE_PACK, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)DeliverPack:(NSString *)packId
        LogDeviceId:(NSString *)logDeviceId
       LogUserAgent:(NSString *)logUserAgent
      LogDeviceType:(NSString *)logDeviceType
         SessionKey:(NSString *)logSessionKey
  completionHandler:(void (^)(BOOL isDelivered, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, DELIVER_PACK,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)AddDeliveryImg:(NSString *)packId
       PackImageBase64:(NSString *)base64Image
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
            SessionKey:(NSString *)logSessionKey
     completionHandler:(void (^)(AddImageObject *addImage, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_CP_BASE_64_IMAGE:base64Image,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, ADD_DELIVERY_IMG];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                AddImageObject *addImage = [AddImageObject new];
                addImage.insertedId = resultDict[@"inserted_id"];
                addImage.imageIdFromServer = resultDict[@"inserted_id"];
                addImage.imageName = resultDict[@"details"][@"pack_img"];
                addImage.isUploaded = YES;
                completionHandler(addImage, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)DeleteDeliveryImg:(NSString *)packId
            PackImageName:(NSString *)packImageName
              LogDeviceId:(NSString *)logDeviceId
             LogUserAgent:(NSString *)logUserAgent
            LogDeviceType:(NSString *)logDeviceType
               SessionKey:(NSString *)logSessionKey
        completionHandler:(void (^)(BOOL isImageDeleted, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_DELETE_IMAGE_ID:packId,
                             REST_PARAM_CP_BASE_64_IMAGE:packImageName,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, DELETE_DELIVERY_IMG];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(NO, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetAllDeliveryImgs:(NSString *)packId
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
                SessionKey:(NSString *)logSessionKey
         completionHandler:(void (^)(NSMutableArray *deliveryImages, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GET_ALL_DELIVERY_IMAGES];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                NSMutableArray *imagesArray = [NSMutableArray new];
                
                for (NSDictionary *dict in resultArray) {
                    
                    AddImageObject *addImage = [AddImageObject new];
                    addImage.insertedId = dict[@"inserted_date"];
                    addImage.imageName = dict[@"pack_img"];
                    addImage.imageIdFromServer = dict[@"id"];
                    addImage.isUploaded = YES;
                    [imagesArray addObject:addImage];
                }
                
                completionHandler(imagesArray, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)PackDidnotGiven:(NSString *)packId
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
             SessionKey:(NSString *)logSessionKey
      completionHandler:(void (^)(BOOL isImageDeleted, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, PACK_DID_NOT_GIVEN,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(NO, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)CourierReportProblem:(NSString *)packId
                  ReasonText:(NSString *)reasonText
                    ReasonId:(NSString *)reasonId
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(BOOL isPackCancelled, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_PR_REASON_TEXT:reasonText,
                             REST_PARAM_PR_REASON_ID:reasonId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, COURIER_REPORT_PROBLEM,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)BeCourierOfPack:(NSString *)packId
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
             SessionKey:(NSString *)logSessionKey
      completionHandler:(void (^)(BOOL isPackCancelled, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             @"bid_text":@"",
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, BE_COURIER_OF_PACK,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else if ([resultDict[@"code"] intValue] == 14) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:14 userInfo:nil]);
                
            }
            else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}


+ (void)GetAllPackImages:(NSString *)packId
             LogDeviceId:(NSString *)logDeviceId
            LogUserAgent:(NSString *)logUserAgent
           LogDeviceType:(NSString *)logDeviceType
              SessionKey:(NSString *)logSessionKey
       completionHandler:(void (^)(NSMutableArray *deliveryImages, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_PACK_ALL_IMAGES,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                NSMutableArray *imagesArray = [NSMutableArray new];
                
                for (NSDictionary *dict in resultArray) {
                    
                    AddImageObject *addImage = [AddImageObject new];
                    addImage.insertedId = dict[@"inserted_id"];
                    addImage.imageName = dict[@"pack_img"];
                    addImage.isUploaded = YES;
                    [imagesArray addObject:addImage];
                }
                
                completionHandler(imagesArray, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)CancelPackageRequest:(NSString *)packId
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(BOOL isPackCancelled, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_PACK_ID:packId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, CANCEL_PACKAGE_REQUEST,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            }else if ([resultDict[@"code"] intValue] == 14) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:14 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetAcceptRule:(NSString *)ruleModule
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
           SessionKey:(NSString *)logSessionKey
    completionHandler:(void (^)(NSMutableArray *rulesArray, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CP_RULE_MODULE:ruleModule,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GET_ACCEPT_RULE];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                
                NSMutableArray *rules = [NSMutableArray new];
                
                for (NSDictionary *dict in resultArray) {
                    
                    NSDictionary *rule = @{@"body_text":dict[@"body_text"], @"header_text":dict[@"header_text"]};
                    [rules addObject:rule];
                }
                completionHandler(rules, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)AddCallHistory:(NSString *)callerId
             ReciverId:(NSString *)ReciverId
              CallDate:(NSString *)CallDate
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
            SessionKey:(NSString *)logSessionKey
     completionHandler:(void (^)(BOOL isCallAdded, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_CALLER_ID:callerId,
                             REST_PARAM_RECIVER_ID:ReciverId,
                             REST_PARAM_CALL_DATE:CallDate,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, ADD_CALL_HISTORY];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)UpdateCourierGenInfo:(NSString *)name
                     Surname:(NSString *)surname
                        Cell:(NSString *)cell
                     Address:(NSString *)address
                      Gender:(NSString *)gender
               LogSessionKey:(NSString *)logSessionKey
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
           completionHandler:(void (^)(BOOL result, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_NAME:name,
                             REST_PARAM_REQUEST_SURNAME:surname,
                             REST_PARAM_COMMON_CELL:cell,
                             REST_PARAM_COMMON_ADDRESS_ACTUAL:address,
                             REST_PARAM_REQUEST_GENDER:gender,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, UPDATE_COURIER_GEN_INFO, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(NO, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

//********************************** Payments ******************

+ (void)GetMyPayments:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
           SessionKey:(NSString *)logSessionKey
    completionHandler:(void (^)(NSMutableArray *paymentHistory, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_MY_PAYMENTS,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"];
                NSMutableArray *paymentHistoryArray = [NSMutableArray new];
                
                for (NSDictionary *responseDict in response) {
                    
                    PaymentHistory *paymentHistory = [PaymentHistory modelObjectWithDictionary:responseDict];
                    [paymentHistoryArray addObject:paymentHistory];
                }
                
                completionHandler(paymentHistoryArray, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(NO, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetMyPayouts:(NSString *)logDeviceId
        LogUserAgent:(NSString *)logUserAgent
       LogDeviceType:(NSString *)logDeviceType
          SessionKey:(NSString *)logSessionKey
   completionHandler:(void (^)(NSMutableArray *payoutHistory,NSString *totalAmount,NSString* deliveryCount, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_MY_PAYOUTS,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"][@"payouts"];
                NSMutableArray *paymentHistoryArray = [NSMutableArray new];
                
                for (NSDictionary *responseDict in response) {
                    
                    PayoutHistory *paymentHistory = [PayoutHistory modelObjectWithDictionary:responseDict];
                    [paymentHistoryArray addObject:paymentHistory];
                }
                
                NSString *totalPayment = resultDict[@"data"][@"total_amount"];
                NSString *deliveryCount = resultDict[@"data"][@"total_delivery"];

                completionHandler(paymentHistoryArray,totalPayment,deliveryCount, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0,@"0",@"0", [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0,@"0",@"0", [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0,@"0",@"0", [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetPayoutDetails:(NSString *)payoutID
             LogDeviceId:(NSString *)logDeviceId
            LogUserAgent:(NSString *)logUserAgent
           LogDeviceType:(NSString *)logDeviceType
              SessionKey:(NSString *)logSessionKey
       completionHandler:(void (^)(NSMutableArray *payoutDetails, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_PAYOUT_ID:payoutID,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_PAYOUT_Details,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"];
                NSMutableArray *paymentHistoryArray = [NSMutableArray new];
                
                for (NSDictionary *responseDict in response) {
                    
                    PayoutDetails *payoutDetails = [PayoutDetails modelObjectWithDictionary:responseDict];
                    [paymentHistoryArray addObject:payoutDetails];
                }
                
                completionHandler(paymentHistoryArray, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}


//********************************** Chat ******************

+ (void)SendChatMessage:(NSString *)recieverId
               ChatText:(NSString *)chatText
                PushNot:(NSString *)pushNot
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
             SessionKey:(NSString *)logSessionKey
      completionHandler:(void (^)(BOOL isSent, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_RECIEVER_ID:recieverId,
                             REST_PARAM_CHAT_TEXT:chatText,
                             REST_PARAM_PUSH_NOT:pushNot,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, SEND_CHAT_MESSAGE];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetChatConversations:(NSString *)count
                       Start:(NSString *)start
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(NSMutableArray *chatConversationList, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_PL_COUNT:count,
                             REST_PARAM_PL_START:start,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, SEND_CHAT_CONVERSATION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"];
                NSMutableArray *conversations = [NSMutableArray new];
                
                for (NSDictionary *responseDict in response) {
                    
                    CourierConversations *courierConversation = [CourierConversations modelObjectWithDictionary:responseDict];
                    [conversations addObject:courierConversation];
                }
                
                completionHandler(conversations, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetMessageList:(NSString *)recieverId
                 Count:(NSString *)count
                 Start:(NSString *)start
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
            SessionKey:(NSString *)logSessionKey
     completionHandler:(void (^)(NSMutableArray *messagesList, NSString *lastCount, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_RECIEVER_ID:recieverId,
                             REST_PARAM_PL_COUNT:count,
                             REST_PARAM_PL_START:start,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GET_MESSAGE_LIST];
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"][@"data"];
                NSMutableArray *chatMessages = [NSMutableArray new];
                
                for (NSDictionary *responseDict in response) {
                    
                    ChatListModel *chatListModel = [ChatListModel modelObjectWithDictionary:responseDict];
                    [chatMessages addObject:chatListModel];
                }
                
                completionHandler(chatMessages,resultDict[@"data"][@"count"], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0,0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0,0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0,0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetMessageCount:(NSString *)recieverId
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
            SessionKey:(NSString *)logSessionKey
     completionHandler:(void (^)(NSString *lastCount, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_RECIEVER_ID:recieverId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, @"GetMessageCount"];
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"data"][0][@"cnt"], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

//********************************** Notifications ******************
+ (void)GetUserNotifications:(NSString *)markAsPushed
                      Status:(NSString *)status
                       Count:(NSString *)count
                       Start:(NSString *)start
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(NSMutableArray *notificationList, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_MARK_AS_PUSHED:markAsPushed,
                             REST_PARAM_STATUS:status,
                             REST_PARAM_PL_COUNT:count,
                             REST_PARAM_PL_START:start,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_USER_NOTS, API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"];
                NSMutableArray *notifications = [NSMutableArray new];
                
                for (NSDictionary *responseDict in response) {
                    
                    Notification *rateUser = [Notification modelObjectWithDictionary:responseDict];
                    [notifications addObject:rateUser];
                }
                
                completionHandler(notifications, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)MarkNotAs:(NSString *)notIficationId
           IsRead:(NSString *)isRead
    logSessionKey:(NSString *)logSessionKey
      LogDeviceId:(NSString *)logDeviceId
     LogUserAgent:(NSString *)logUserAgent
    LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(BOOL isRead, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_NOT_ID:notIficationId,
                             REST_PARAM_IS_READ:isRead,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, MARK_AS_NOT];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(nil, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}


//********************************** FAQ ******************

+ (void)GetFaqById:(NSString *)faqId
     logSessionKey:(NSString *)logSessionKey
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(NSMutableArray *faqFaq, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_FAQ_ID:faqId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GET_FAQ_BY_ID];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *resultArray = resultDict[@"data"];
                
                NSMutableArray *faqs = [NSMutableArray new];
                
                for (NSDictionary *dict in resultArray) {
                    
                    NSDictionary *faq = @{@"faq_name":dict[@"faq_name"], @"faq_text":dict[@"faq_text"]};
                    [faqs addObject:faq];
                }
                completionHandler(faqs, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetFaqHeaders:(NSString *)userType
        logSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(NSMutableArray *faqHeaders, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_USER_TYPE:userType,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GET_FAQ_HEADERS];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"];
                NSMutableArray *faqHeaders = [NSMutableArray new];
                
                for (NSDictionary *responseDict in response) {
                    
                    NSDictionary *faqHeader = @{@"faq_name":responseDict[@"faq_name"], @"faq_id":responseDict[@"id"]};
                    
                    [faqHeaders addObject:faqHeader];
                }
                
                completionHandler(faqHeaders, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

//********************************** Rate ******************
+ (void)GetUserRatesByUserID:(NSString *)courrierId
               logSessionKey:(NSString *)logSessionKey
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
           completionHandler:(void (^)(NSMutableArray *courierRateList, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_COURIER_ID:courrierId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GET_USERS_RATES_BY_USER_ID];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"];
                NSMutableArray *notifications = [NSMutableArray new];
                completionHandler(notifications, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetUsersRates:(NSString *)start
                Count:(NSString *)count
        logSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(NSMutableArray *courierRateList, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_GET_PACK_START:start,
                             REST_PARAM_GET_PACK_COUNT:count,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GET_USERS_RATES];
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"];
                NSMutableArray *courierRatings = [NSMutableArray new];
                
                for (NSDictionary *responseDict in response) {
                    
                    RateUser *rateUser = [RateUser modelObjectWithDictionary:responseDict];
                    [courierRatings addObject:rateUser];
                }
                
                completionHandler(courierRatings, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)RateUser:(NSString *)courrierId
         RateVal:(NSString *)rateVal
   logSessionKey:(NSString *)logSessionKey
     LogDeviceId:(NSString *)logDeviceId
    LogUserAgent:(NSString *)logUserAgent
   LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(BOOL isRated, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_COURIER_ID:courrierId,
                             REST_PARAM_RATE_VAL:rateVal,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, RATE_USER];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}


+ (void)AddCourierReview:(NSString *)courierId
         reviewText:(NSString *)reviewText
   logSessionKey:(NSString *)logSessionKey
     LogDeviceId:(NSString *)logDeviceId
    LogUserAgent:(NSString *)logUserAgent
   LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(BOOL isRated, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_COURIER_ID:courierId,
                             REST_PARAM_REVIEW_COURIER_TEXT:reviewText,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, REVIEW_COURIER];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}


//********************************** Feedback ******************
+ (void)SendFeedback:(NSString *)feedbackText
            OptionId:(NSString *)optionId
       logSessionKey:(NSString *)logSessionKey
         LogDeviceId:(NSString *)logDeviceId
        LogUserAgent:(NSString *)logUserAgent
       LogDeviceType:(NSString *)logDeviceType
   completionHandler:(void (^)(BOOL isSent, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_FEEDBACK_TEXT:feedbackText,
                             //                             REST_PARAM_RATE_VAL:optionId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, SEND_FEEDBACK,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GiveTip:(NSString *)pay_amount
        Pack_Id:(NSString *)pack_id
     Courier_Id:(NSString *)courier_id
  logSessionKey:(NSString *)logSessionKey
    LogDeviceId:(NSString *)logDeviceId
   LogUserAgent:(NSString *)logUserAgent
  LogDeviceType:(NSString *)logDeviceType
   completionHandler:(void (^)(BOOL isSent, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARMA_PAY_AMOUNT:pay_amount,
                             REST_PARAM_CP_PACK_ID:pack_id,
                             REST_PARAM_REQUEST_COURIER_ID:courier_id,
                             //                             REST_PARAM_RATE_VAL:optionId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GIVE_TIP,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

//********************************** Dictionaries ******************

+ (void)GetDicByName:(NSString *)name
       logSessionKey:(NSString *)logSessionKey
         LogDeviceId:(NSString *)logDeviceId
        LogUserAgent:(NSString *)logUserAgent
       LogDeviceType:(NSString *)logDeviceType
   completionHandler:(void (^)(NSMutableArray *dictionaries, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_NAME:name,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_DIC_BY_NAME,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"];
                NSMutableArray *dictNames = [NSMutableArray new];
                
                for (NSDictionary *responseDict in response) {
                    
                    GetDictByName *getDictByName = [GetDictByName modelObjectWithDictionary:responseDict];
                    [dictNames addObject:getDictByName];
                }
                
                completionHandler(dictNames, nil);
                
                completionHandler(0, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetAllDics:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
        SessionKey:(NSString *)logSessionKey
 completionHandler:(void (^)(NSMutableArray *dictionaries, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, GET_ALL_DICS,API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *response = resultDict[@"data"];
                NSMutableArray *allDict = [NSMutableArray new];
                
                for (NSDictionary *responseDict in response) {
                    
                    GetDictByName *getDictByName = [GetDictByName modelObjectWithDictionary:responseDict];
                    [allDict addObject:getDictByName];
                }
                
                completionHandler(allDict, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

//********************************** Courier Bank Info ******************

+ (void)UpdateBankAccount:(NSString *)city
                    Line1:(NSString *)line1
                    Line2:(NSString *)line2
                    State:(NSString *)state
               PostalCode:(NSString *)postalCode
                     Date:(NSString *)date
              RouteNumber:(NSString *)route_number
            AccountNumber:(NSString *)account_number
            logSessionKey:(NSString *)logSessionKey
              LogDeviceId:(NSString *)logDeviceId
             LogUserAgent:(NSString *)logUserAgent
            LogDeviceType:(NSString *)logDeviceType
        completionHandler:(void (^)(BOOL isInserted, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_COMMON_CITY:city,
                             REST_PARAM_AC_LINE1:line1,
                             REST_PARAM_AC_LINE2:line2,
                             REST_PARAM_AC_STATE:state,
                             REST_PARAM_AC_POSTAL_CODE:postalCode,
                             REST_PARAM_AC_DATE:date,
                             REST_PARAM_AC_ROUTE_NUMBER:route_number,
                             REST_PARAM_AC_ACCOUNT_NUMBER:account_number,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, UPDATE_BANK_ACCOUNT];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(0, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetEstimatedNextPayment:(NSString *)logDeviceId
                   LogUserAgent:(NSString *)logUserAgent
                  LogDeviceType:(NSString *)logDeviceType
                     SessionKey:(NSString *)logSessionKey
              completionHandler:(void (^)(NSDictionary *paymentDate, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GETESTIMATEDNEXTPAYMENT];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *dataArray = resultDict[@"data"];
                
                if (dataArray.count > 0) {
                    
                    completionHandler(resultDict[@"data"][0], nil);
                    
                } else {
                    
                    completionHandler(nil, nil);
                }
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetBankAccount:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
            SessionKey:(NSString *)logSessionKey
     completionHandler:(void (^)(BankAccountDetail *accountDetails, NSError *error))completionHandler
{
    
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GET_BANK_ACCOUNT];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *dataArray = resultDict[@"data"];
                
                if (dataArray.count > 0) {
                    
                    BankAccountDetail *bankAccontDetail = [BankAccountDetail modelObjectWithDictionary:resultDict[@"data"][0]];
                    completionHandler(bankAccontDetail, nil);
                    
                } else {
                    
                    completionHandler(nil, nil);
                }
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)LogoutUser:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
        SessionKey:(NSString *)logSessionKey
 completionHandler:(void (^)(BOOL isLogout, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, @"LogoutUser"];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

//Location services
+ (void)GetCourierGeoLocation:(NSString *)courrierId
                logSessionKey:(NSString *)logSessionKey
                  LogDeviceId:(NSString *)logDeviceId
                 LogUserAgent:(NSString *)logUserAgent
                LogDeviceType:(NSString *)logDeviceType
            completionHandler:(void (^)(NSDictionary* locationDict, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_REQUEST_COURIER_ID:courrierId,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, @"GetCourierGeoLocation"];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                NSMutableArray *dataArray = resultDict[@"data"];
                NSDictionary *dict = @{@"last_lat":dataArray[0][@"last_lat"], @"last_lng":dataArray[0][@"last_lng"]};
                
                completionHandler(dict, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

//MARK - Version1.5 (Courier)
+ (void)UpdateNearestDistance:(NSString *)milesDistance
                logSessionKey:(NSString *)logSessionKey
                  LogDeviceId:(NSString *)logDeviceId
                 LogUserAgent:(NSString *)logUserAgent
                LogDeviceType:(NSString *)logDeviceType
            completionHandler:(void (^)(BOOL isUpdated, NSError *error))completionHandler
{
    
    NSDictionary *params = @{@"nearest_distance":milesDistance,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@%@", BASE_URL, @"UpdateNearestDistance", API_VERSION];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)GetUserRoutes:(NSString *)userID
        logSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(NSMutableArray *routesList, NSError *error))completionHandler
{
    NSDictionary *params = @{REST_PARAM_COMMON_USER_ID:userID,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, GET_USER_ROUTES];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(resultDict[@"data"], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)AddUserRoutes:(NSString *)fromLat
              fromLng:(NSString *)fromLng
                toLat:(NSString *)toLat
                toLng:(NSString *)toLng
          fromAddress:(NSString *)fromAddress
            toAddress:(NSString *)toAddress
        recursiveType:(NSString *)recursiveType
            routeDate:(NSString *)routeDate
            dayOfWeek:(NSString *)dayOfWeek
            transportType:(NSString *)transportType
        logSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(BOOL isAddedRoute, NSError *error))completionHandler
{
    NSDictionary *params = @{@"from_lat":fromLat,
                             @"from_lng":fromLng,
                             @"to_lat":toLat,
                             @"to_lng":toLng,
                             @"from_text":fromAddress,
                             @"to_text":toAddress,
                             @"rec_type":recursiveType,
                             @"route_date":routeDate,
                             @"day_of_week":dayOfWeek,
                             @"transport_type":transportType,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, @"AddUserRoute"];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)UpdateUserRoutes:(NSString *)fromLat
              fromLng:(NSString *)fromLng
                toLat:(NSString *)toLat
                toLng:(NSString *)toLng
          fromAddress:(NSString *)fromAddress
            toAddress:(NSString *)toAddress
        recursiveType:(NSString *)recursiveType
            routeDate:(NSString *)routeDate
            dayOfWeek:(NSString *)dayOfWeek
        transportType:(NSString *)transportType
              routeId:(NSString *)routeId
        logSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(BOOL isAddedRoute, NSError *error))completionHandler
{
    NSDictionary *params = @{@"route_id":routeId,
                             @"from_lat":fromLat,
                             @"from_lng":fromLng,
                             @"to_lat":toLat,
                             @"to_lng":toLng,
                             @"from_text":fromAddress,
                             @"to_text":toAddress,
                             @"rec_type":recursiveType,
                             @"route_date":routeDate,
                             @"day_of_week":dayOfWeek,
                             @"transport_type":transportType,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, @"UpdateUserRoute"];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler(YES, nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(nil, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

+ (void)DeleteUserRoutes:(NSString *)routeID
           logSessionKey:(NSString *)logSessionKey
             LogDeviceId:(NSString *)logDeviceId
            LogUserAgent:(NSString *)logUserAgent
           LogDeviceType:(NSString *)logDeviceType
       completionHandler:(void (^)(BOOL isDeletedRoute, NSError *error))completionHandler
{
    NSDictionary *params = @{@"route_id":routeID,
                             REST_PARAM_COMMON_LOG_DEVICE_ID:logDeviceId,
                             REST_PARAM_COMMON_LOG_SESSON_KEY:logSessionKey,
                             REST_PARAM_COMMON_LOG_USER_AGENT:logUserAgent,
                             REST_PARAM_COMMON_LOG_DEVICE_TYPE:logDeviceType};
    
    NSMutableArray *paramsArray = [NSMutableArray new];
    [paramsArray addObject:params];
    NSString *baseUrl = [NSString stringWithFormat:@"%@%@", BASE_URL, @"DeleteUserRoute"];
    
    [self postRequestWithURL:baseUrl parameters:paramsArray CompletionHandler:^(id result, NSError *error) {
        
        if (!error) {
            
            NSDictionary *resultDict = (NSDictionary *)result;
            if ([resultDict[@"code"] intValue] == 0) {
                
                completionHandler([resultDict[@"data"] boolValue], nil);
            }
            else if ([resultDict[@"code"] intValue] == 3) {
                
                completionHandler(NO, [NSError errorWithDomain:resultDict[@"msgtext"] code:3 userInfo:nil]);
                
            } else {
                
                completionHandler(NO, [NSError errorWithDomain:resultDict[@"msgtext"] code:101 userInfo:nil]);
            }
        }
        else
        {
            completionHandler(0, [NSError errorWithDomain:@"Something went wrong, please try again." code:101 userInfo:nil]);
        }
    }];
}

@end
