//
//  WebServicesClient.h
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

#import "DataModels.h"
#import "AddImageObject.h"
#import "SPPack.h"
#import "SenderPackList.h"
#import "MyDeliverySelection.h"

@interface WebServicesClient : NSObject

+ (void)postRequestWithURL:(NSString *)url
                parameters:(NSMutableArray *)parameters
         CompletionHandler:(void (^)(id result, NSError *error))completionHandler;

+ (void)LoginUser:(NSString *)email
         password:(NSString *)password
      LogDeviceId:(NSString *)logDeviceId
     LogUserAgent:(NSString *)logUserAgent
    LogDeviceType:(NSString *)logDeviceType
       SessionKey:(NSString *)logSessionKey
completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler;

+ (void)FBLogin:(NSString *)fb_token
    LogDeviceId:(NSString *)logDeviceId
   LogUserAgent:(NSString *)logUserAgent
  LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler;

+ (void)GoogleLogin:(NSString *)google_token
    LogDeviceId:(NSString *)logDeviceId
   LogUserAgent:(NSString *)logUserAgent
  LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler;

//*****************************************************Sender*****************************
+ (void)RegisterSender:(NSString *)name
               Surname:(NSString *)surname
                  Cell:(NSString *)cell
                 Email:(NSString *)email
              Password:(NSString *)password
               Address:(NSString *)address
           CompanyName:(NSString *)company
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
      SendApproveEmail:(NSString *)sendApproveEmail
     completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler;

+ (void)DeletePackImage:(NSString *)insertedId
          PackImageName:(NSString *)packImageName
          LogSessionKey:(NSString *)logSessionKey
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
      completionHandler:(void (^)(BOOL isDeleted, NSError *error))completionHandler;

+ (void)GetUserInfo:(NSString *)logSessionKey
        LogDeviceId:(NSString *)logDeviceId
       LogUserAgent:(NSString *)logUserAgent
      LogDeviceType:(NSString *)logDeviceType
  completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler;

+ (void)GetUserInfoById:(NSString *)userId
          LogSessionKey:(NSString *)logSessionKey
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
      completionHandler:(void (^)(UserModel *user, NSError *error))completionHandler;

+ (void)UpdateSender:(NSString *)name
             Surname:(NSString *)surname
                Cell:(NSString *)cell
             Address:(NSString *)address
         UseLocation:(NSString *)useLocation
              Gender:(NSString *)gender
       LogSessionKey:(NSString *)logSessionKey
         LogDeviceId:(NSString *)logDeviceId
        LogUserAgent:(NSString *)logUserAgent
       LogDeviceType:(NSString *)logDeviceType
   completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)SendApproveEmailToSender:(NSString *)logSessionKey
                     LogDeviceId:(NSString *)logDeviceId
                    LogUserAgent:(NSString *)logUserAgent
                   LogDeviceType:(NSString *)logDeviceType
               completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)CreatePack:(NSString *)sendCatId
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
        SessionKey:(NSString *)logSessionKey
 completionHandler:(void (^)(NSString *packId, NSError *error))completionHandler;

+ (void)Createpack_PriceInfo:(NSString *)item_value
                Pack_Size_ID:(NSString *)pack_size_id
                    Distance:(NSString *)distance
               Distance_Time:(NSString *)distance_time
              Pack_From_Text:(NSString *)pack_from_text
               Pack_From_Lng:(NSString *)pack_from_lng
               Pack_From_Lat:(NSString *)pack_from_lat
                Pack_To_Text:(NSString *)pack_to_text
                 Pack_To_Lng:(NSString *)pack_to_lng
                 Pack_To_Lat:(NSString *)pack_to_lat
          Is_Insurance_Payed:(NSString *)is_insurance_payed
                  Pack_Price:(NSString *)pack_price
                         Day:(NSString *)day
                        Time:(NSString *)time
              Tariff_Plan_ID:(NSString *)tariff_plan_id
                 Send_Cat_ID:(NSString *)send_cat_id
                Waiting_Mins:(NSString *)waiting_mins
                  Promo_Code:(NSString *)promo_code
                 LogDeviceId:(NSString *)logDeviceID
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(int inserted_id, NSError *error))completionHandler;

+ (void)Updatepack_PriceInfo:(NSString *)item_value
                Pack_Size_ID:(NSString *)pack_size_id
                    Distance:(NSString *)distance
               Distance_Time:(NSString *)distance_time
              Pack_From_Text:(NSString *)pack_from_text
               Pack_From_Lng:(NSString *)pack_from_lng
               Pack_From_Lat:(NSString *)pack_from_lat
                Pack_To_Text:(NSString *)pack_to_text
                 Pack_To_Lng:(NSString *)pack_to_lng
                 Pack_To_Lat:(NSString *)pack_to_lat
          Is_Insurance_Payed:(NSString *)is_insurance_payed
                  Pack_Price:(NSString *)pack_price
                     Pack_Id:(NSString *)pack_id
                         Day:(NSString *)day
                        Time:(NSString *)time
              Tariff_Plan_ID:(NSString *)tariff_plan_id
                 Send_Cat_ID:(NSString *)send_cat_id
                   Waiting_Mins:(NSString *)waiting_mins
                  Promo_Code:(NSString *)promo_code
                 LogDeviceId:(NSString *)logDeviceID
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(int inserted_id, NSError *error))completionHandler;


+ (void)InstantPaymentShowPrice:(NSString *)distance
                  Distance_time:(NSString *)distance_time
                     Item_Value:(NSString *)item_value
                   Pack_Size_ID:(NSString *)pack_size_id
                         Cat_Id:(NSString *)cat_id
                            Day:(NSString *)day
                           Time:(NSString *)time
//                 Tariff_Plan_ID:(NSString *)tariff_plan_id
                   Waiting_Mins:(NSString *)waiting_mins
                     Promo_Code:(NSString *)promo_code
                    LogDeviceId:(NSString *)logDeviceID
                   LogUserAgent:(NSString *)logUserAgent
                  LogDeviceType:(NSString *)logDeviceType
               completionHandler:(void (^)(id result, NSError *error))completionHandler;

+ (void)DeleteMyDraftPack:(NSString *)packId
            logSessionKey:(NSString *)logSessionKey
              LogDeviceId:(NSString *)logDeviceId
             LogUserAgent:(NSString *)logUserAgent
            LogDeviceType:(NSString *)logDeviceType
        completionHandler:(void (^)(BOOL isDeleted, NSError *error))completionHandler;

+ (void)UpdatePack_GenInfo:(NSString *)packId
                   PetType:(NSString *)petType
                  PetCount:(NSString *)petCount
                  PetISVac:(NSString *)PetISVac
                  SendName:(NSString *)sendName
                    isPack:(BOOL)isPack
                PackSizeId:(NSString *)packSizeId
             PackOptionsId:(NSString *)packOptionsId
                PackWeight:(NSString *)packWeight
                 ItemValue:(NSString *)itemValue
               Description:(NSString *)description
              ReceiverName:(NSString *)receiverName
       ReceiverPhoneNumber:(NSString *)receiverPhoneNumber
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
                SessionKey:(NSString *)logSessionKey
         completionHandler:(void (^)(int packId, NSError *error))completionHandler;


+ (void)CreatePack_GenInfo:(NSString *)packId
                   PetType:(NSString *)petType
                  PetCount:(NSString *)petCount
                  PetISVac:(NSString *)PetISVac
                  SendName:(NSString *)sendName
                    isPack:(BOOL)isPack
                PackSizeId:(NSString *)packSizeId
             PackOptionsId:(NSString *)packOptionsId
                PackWeight:(NSString *)packWeight
                 ItemValue:(NSString *)itemValue
               Description:(NSString *)description
              ReceiverName:(NSString *)receiverName
       ReceiverPhoneNumber:(NSString *)receiverPhoneNumber
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
                SessionKey:(NSString *)logSessionKey
         completionHandler:(void (^)(int packId, NSError *error))completionHandler;

+ (void)AddPackImage:(NSString *)packId
     PackImageBase64:(NSString *)base64Image
       LogSessionKey:(NSString *)logSessionKey
         LogDeviceId:(NSString *)logDeviceId
        LogUserAgent:(NSString *)logUserAgent
       LogDeviceType:(NSString *)logDeviceType
   completionHandler:(void (^)(AddImageObject *addImage, NSError *error))completionHandler;

+ (void)UpdatePack_Destination:(NSString *)packId
                   PackFromLat:(NSString *)packFromLat
                   PackFromLng:(NSString *)packFromLng
                  PackFromText:(NSString *)packFromText
                     PackToLat:(NSString *)packToLat
                     PackToLng:(NSString *)packToLng
                    PackToText:(NSString *)packToText
                      Distance:(NSString *)distance
                  DistanceTime:(NSString *)distanceTime
                     TakenTime:(NSString *)takenTime
                     TakenType:(NSString *)takenType
                     TakenAsap:(NSString *)takenAsap
                  DeliveryTime:(NSString *)deliveryTime
                  DeliveryType:(NSString *)deliveryType
                  DeliveryAsap:(NSString *)deliveryAsap
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
                    SessionKey:(NSString *)logSessionKey
             completionHandler:(void (^)(SPPack *pack, NSError *error))completionHandler;

+ (void)UpdatePack_Description:(NSString *)packId
                     Desc_Text:(NSString *)desc_text
                 LogSessionKey:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
             completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)UpdatePack_SendingName:(NSString *)packId
                     Send_Name:(NSString *)send_name
                 LogSessionKey:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
             completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)payForPack:(NSString *)packId
         PackPrice:(NSString *)packPrice
   IsInsurancePaid:(NSString *)isInsurancePaid
        StripToken:(NSString *)stripToken
      cardLastFour:(NSString *)lastFourDigits
        Taken_asap:(NSString *)taken_asap
        Taken_Time:(NSString *)taken_time
         Send_Name:(NSString *)send_name
         Desc_Text:(NSString *)desc_text
     Receiver_Name:(NSString *)receiver_name
Receiver_Phone_Number:(NSString *)receiver_phone_number
     LogSessionKey:(NSString *)logSessionKey
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(NSString *orderId, NSError *error))completionHandler;

+ (void)PayForAskedPackByEmail:(NSString *)packId
                   Payor_Email:(NSString *)payor_email
                  cardLastFour:(NSString *)lastFourDigits
                    StripToken:(NSString *)stripToken
                 LogSessionKey:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
             completionHandler:(void (^)(NSString *orderId, NSError *error))completionHandler;

+ (void)AskFriendToPay:(NSString *)packId
         PackPrice:(NSString *)packPrice
   IsInsurancePaid:(NSString *)isInsurancePaid
        Taken_asap:(NSString *)taken_asap
        Taken_Time:(NSString *)taken_time
         Send_Name:(NSString *)send_name
         Desc_Text:(NSString *)desc_text
     Receiver_Name:(NSString *)receiver_name
Receiver_Phone_Number:(NSString *)receiver_phone_number
        Payor_Email:(NSString *)payor_email
     LogSessionKey:(NSString *)logSessionKey
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(NSString *orderId, NSError *error))completionHandler;

+ (void)AskFriendToPayInstant:(NSString *)email
                 phone_number:(NSString *)phone_number
                    Send_Name:(NSString *)send_name
                    Desc_Text:(NSString *)desc_text
                Receiver_Name:(NSString *)receiver_name
        Receiver_Phone_Number:(NSString *)receiver_phone_number
                    PackPrice:(NSString *)packPrice
                   Item_Value:(NSString *)item_value
                 Pack_Size_Id:(NSString *)pack_size_id
                     Distance:(NSString *)distance
                Distance_Time:(NSString *)distance_time
                  Send_Cat_Id:(NSString *)send_cat_id
               Tariff_Plan_Id:(NSString *)tariff_plan_id
               Pack_From_Text:(NSString *)pack_from_text
                Pack_From_Lng:(NSString *)pack_from_lng
                Pack_From_Lat:(NSString *)pack_from_lat
                 Pack_To_Text:(NSString *)pack_to_text
                  Pack_To_Lng:(NSString *)pack_to_lng
                  Pack_To_Lat:(NSString *)pack_to_lat
                         Name:(NSString *)name
                      SurName:(NSString *)surname
                 cardLastFour:(NSString *)lastFourDigits
                          Day:(NSString *)day
                         Time:(NSString *)time
                 Waiting_Mins:(NSString *)waiting_mins
                   Promo_Code:(NSString *)promo_code
                  Payor_Email:(NSString *)payor_email
                LogSessionKey:(NSString *)logSessionKey
                  LogDeviceId:(NSString *)logDeviceId
                 LogUserAgent:(NSString *)logUserAgent
                LogDeviceType:(NSString *)logDeviceType
            completionHandler:(void (^)(NSString *orderId, NSError *error))completionHandler;

+ (void)MakeInstantPayment:(NSString *)email
              Phone_Number:(NSString *)phone_number
                 Send_Name:(NSString *)send_name
                 Desc_Text:(NSString *)desc_text
             Receiver_Name:(NSString *)receiver_name
     Receiver_Phone_Number:(NSString *)receiver_phone_number
                 PackPrice:(NSString *)packPrice
                StripToken:(NSString *)stripToken
                Item_Value:(NSString *)item_value
              Pack_Size_ID:(NSString *)pack_size_id
       Default_Tariff_Plan:(NSString *)default_tariff_plan
                  Distance:(NSString *)distance
             Distance_Time:(NSString *)distance_time
               Send_Cat_ID:(NSString *)send_cat_id
            Pack_From_Text:(NSString *)pack_from_text
             Pack_From_Lng:(NSString *)pack_from_lng
             Pack_From_Lat:(NSString *)pack_from_lat
              Pack_To_Text:(NSString *)pack_to_text
               Pack_To_Lng:(NSString *)pack_to_lng
               Pack_To_Lat:(NSString *)pack_to_lat
                      Name:(NSString *)name
                   SurName:(NSString *)surname
              cardLastFour:(NSString *)lastFourDigits
                       Day:(NSString *)day
                      Time:(NSString *)time
              Waiting_Mins:(NSString *)waiting_mins
                Promo_Code:(NSString *)promo_code
//             LogSessionKey:(NSString *)logSessionKey
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(NSString *orderId, NSError *error))completionHandler;

+ (void)GetPackDetails:(NSString *)packId
         IncludeImages:(NSString *)includeImages
        IncludeBidders:(NSString *)includeBidders
                 Start:(NSString *)start
                 Count:(NSString *)count
         LogSessionKey:(NSString *)logSessionKey
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
     completionHandler:(void (^)(PackDetailsModel *packDetails, NSError *error))completionHandler;

+ (void)CancelPack:(NSString *)packId
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
        SessionKey:(NSString *)logSessionKey
 completionHandler:(void (^)(BOOL isPackCancelled, NSError *error))completionHandler;

+ (void)SenderReportProblem:(NSString *)packId
                 ReasonText:(NSString *)reasonText
                   ReasonId:(NSString *)reasonId
                LogDeviceId:(NSString *)logDeviceId
               LogUserAgent:(NSString *)logUserAgent
              LogDeviceType:(NSString *)logDeviceType
                 SessionKey:(NSString *)logSessionKey
          completionHandler:(void (^)(BOOL isPackCancelled, NSError *error))completionHandler;

+ (void)SenderGetPacksList:(NSString *)start
                     count:(NSString *)count
                      List:(SenderPackList)packList
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
                SessionKey:(NSString *)logSessionKey
         completionHandler:(void (^)(SenderPacksList *senderPackList, NSError *error))completionHandler;

//*****************************************************Sender End*****************************


//*****************************************************Courier*****************************

//Courier Registration
+ (void)RegisterCourierGenInfo:(NSString *)name
                       Surname:(NSString *)surname
                          Cell:(NSString *)cell
                   ReferalName:(NSString *)referalName
                   ReferalCell:(NSString *)referalCell
                         Email:(NSString *)email
                      Password:(NSString *)password
               SendApproveMail:(NSString *)approveEmail
                 LogSessionKey:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
             completionHandler:(void (^)(NSString *sessionKey, NSString *userId, NSError *error))completionHandler;

+ (void)UpdateCourierIDCard:(NSString *)idCardImage1
               IdCardImage1:(NSString *)idCardImage2
               IdCardNumber:(NSString *)idCardNumber
           IdCardExpiryDate:(NSString *)idCardExpiryDate
              AddressActual:(NSString *)addressActual
            AddressOfficial:(NSString *)addressOfficial
                IDCardState:(NSString *)idCard_State
                 SSNNumaber:(NSString *)SSNNumaber
                     Gender:(NSString *)gender
                UseOfficial:(NSString *)useOfficial
              LogSessionKey:(NSString *)logSessionKey
                LogDeviceId:(NSString *)logDeviceId
               LogUserAgent:(NSString *)logUserAgent
              LogDeviceType:(NSString *)logDeviceType
          completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)SelfImage:(NSString *)selfImage
    LogSessionKey:(NSString *)logSessionKey
      LogDeviceId:(NSString *)logDeviceId
     LogUserAgent:(NSString *)logUserAgent
    LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(NSString *imageName, NSError *error))completionHandler;


+ (void)UpdateCourierGenInfo:(NSString *)name
                     Surname:(NSString *)surname
                        Cell:(NSString *)cell
                     Address:(NSString *)address
                      Gender:(NSString *)gender
               LogSessionKey:(NSString *)logSessionKey
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
           completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)GetUsersTransportTypes:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
             completionHandler:(void (^)(NSDictionary *usersTransportTypes, NSError *error))completionHandler;

+ (void)AddNewTransport:(NSString *)transportType
          TransportYear:(NSString *)transportYear
         TransportModel:(NSString *)transportModel
          TransportMake:(NSString *)transportMake
            PlateNumber:(NSString *)plateNumber
        IsUpdateVehicel:(BOOL)isUpdateVehicle
            transportId:(NSString *)transportId
          LogSessionKey:(NSString *)logSessionKey
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
      completionHandler:(void (^)(NSString *insertedId, NSError *error))completionHandler;

+ (void)AddNewTransportImage:(NSString *)transportId
              TransportImage:(NSString *)TransportImage
               LogSessionKey:(NSString *)logSessionKey
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
           completionHandler:(void (^)(NSString *transportImage, NSError *error))completionHandler;

+ (void)ModifyUserTransportTypes:(NSDictionary *)transportIds
                   LogSessionKey:(NSString *)logSessionKey
                     LogDeviceId:(NSString *)logDeviceId
                    LogUserAgent:(NSString *)logUserAgent
                   LogDeviceType:(NSString *)logDeviceType
               completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)DeleteTransportImageById:(NSString *)transportId
                   LogSessionKey:(NSString *)logSessionKey
                     LogDeviceId:(NSString *)logDeviceId
                    LogUserAgent:(NSString *)logUserAgent
                   LogDeviceType:(NSString *)logDeviceType
               completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)DeleteUserTransportById:(NSString *)transportId
                  LogSessionKey:(NSString *)logSessionKey
                    LogDeviceId:(NSString *)logDeviceId
                   LogUserAgent:(NSString *)logUserAgent
                  LogDeviceType:(NSString *)logDeviceType
              completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)GetTransportImagesByTransportId:(NSString *)transportId
                          LogSessionKey:(NSString *)logSessionKey
                            LogDeviceId:(NSString *)logDeviceId
                           LogUserAgent:(NSString *)logUserAgent
                          LogDeviceType:(NSString *)logDeviceType
                      completionHandler:(void (^)(NSMutableArray *imagesArray, NSError *error))completionHandler;

+ (void)GetUserTransports:(NSString *)logSessionKey
              LogDeviceId:(NSString *)logDeviceId
             LogUserAgent:(NSString *)logUserAgent
            LogDeviceType:(NSString *)logDeviceType
        completionHandler:(void (^)(NSMutableArray *usersTransports, NSError *error))completionHandler;

+ (void)AddUserIns:(NSString *)userId
InsurancePolicyNumber:(NSString *)insurancePolicyNumber
InsuranceExpiryDate:(NSString *)insuranceExpiryDate
   InsuranceImage1:(NSString *)InsuranceImage1
   InsuranceImage2:(NSString *)InsuranceImage2
     LogSessionKey:(NSString *)logSessionKey
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)GetUserIns:(NSString *)userId
     LogSessionKey:(NSString *)logSessionKey
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(GetCourierInsuranceGen *courierInsGen, NSError *error))completionHandler;

+ (void)SendApproveEmailToCourier:(NSString *)logSessionKey
                      LogDeviceId:(NSString *)logDeviceId
                     LogUserAgent:(NSString *)logUserAgent
                    LogDeviceType:(NSString *)logDeviceType
                completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)GetUserIns:(NSString *)logSessionKey
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(NSDictionary *usersTransportTypes, NSError *error))completionHandler;

+ (void)DeleteUserInsById:(NSString *)insuranceId
            LogSessionKey:(NSString *)logSessionKey
              LogDeviceId:(NSString *)logDeviceId
             LogUserAgent:(NSString *)logUserAgent
            LogDeviceType:(NSString *)logDeviceType
        completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)UpdateUserIns:(NSString *)transportId
      InsuranceNumber:(NSString *)insuranceNumber
   InsuranceIssueDate:(NSString *)insuranceIssueDate
  InsuranceExpiryDate:(NSString *)insuranceExpiryDate
      FranshiseAmount:(NSString *)franshiseAmount
         NameInPolicy:(NSString *)nameInPolicy
    InsuranceProvider:(NSString *)insuranceProvider
         PolicyNumber:(NSString *)policyNumber
 InsurancePhoneNumber:(NSString *)insurancePhoneNumber
      InsuranceImage1:(NSString *)InsuranceImage1
      InsuranceImage2:(NSString *)InsuranceImage2
        LogSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)DeleteUserInsByTransportId:(NSString *)transportId
                     LogSessionKey:(NSString *)logSessionKey
                       LogDeviceId:(NSString *)logDeviceId
                      LogUserAgent:(NSString *)logUserAgent
                     LogDeviceType:(NSString *)logDeviceType
                 completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)UpdateCourierQuestions:(NSString *)sendApproveEmail
                QuestionHealth:(NSString *)questionHealth
            QuestionDisability:(NSString *)questionDisability
              QuestionCriminal:(NSString *)questionCriminal
               QuestionSexsual:(NSString *)questionSexsual
                 LogSessionKey:(NSString *)logSessionKey
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
             completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)ApproveUser:(NSString *)approveGuid
      LogSessionKey:(NSString *)logSessionKey
        LogDeviceId:(NSString *)logDeviceId
       LogUserAgent:(NSString *)logUserAgent
      LogDeviceType:(NSString *)logDeviceType
  completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)ChangePasswordByGuid:(NSString *)approveGuid
               LogSessionKey:(NSString *)logSessionKey
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
           completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)ChangePassword:(NSString *)logSessionKey
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
     completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)ChangePassword2:(NSString *)currentPassword
            NewPassword:(NSString *)newPassword
         ReTypePassword:(NSString *)ReTypePassword
          logSessionKey:(NSString *)logSessionKey
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
      completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)CreateSenderProfile:(NSString *)logSessionKey
                LogDeviceId:(NSString *)logDeviceId
               LogUserAgent:(NSString *)logUserAgent
              LogDeviceType:(NSString *)logDeviceType
          completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)MakeDefaultProfile:(NSString *)userType
             LogSessionKey:(NSString *)logSessionKey
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
         completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)ResendProfileForAdminApprove:(NSString *)logSessionKey
                         LogDeviceId:(NSString *)logDeviceId
                        LogUserAgent:(NSString *)logUserAgent
                       LogDeviceType:(NSString *)logDeviceType
                   completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)SendForgotPasswordEmail:(NSString *)email
                  LogSessionKey:(NSString *)logSessionKey
                    LogDeviceId:(NSString *)logDeviceId
                   LogUserAgent:(NSString *)logUserAgent
                  LogDeviceType:(NSString *)logDeviceType
              completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)CreateCourierProfile:(NSString *)logSessionKey
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
           completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)UpdateUsersDrivingLisence:(NSString *)lisenceNumber
                LisenceExpiryDate:(NSString *)lisenceExpiryDate
                         LastName:(NSString *)lastName
                        FirstName:(NSString *)firstName
                    LisenceImage1:(NSString *)lisenceImage1
                    LisenceImage2:(NSString *)lisenceImage2
                      LogDeviceId:(NSString *)logDeviceId
                     LogUserAgent:(NSString *)logUserAgent
                    LogDeviceType:(NSString *)logDeviceType
                completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)GetUsersDrivingLis:(NSString *)logSessionKey
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
         completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)UpdateUserAvailStatus:(NSString *)availStatus
                LogSessionKey:(NSString *)logSessionKey
                  LogDeviceId:(NSString *)logDeviceId
                 LogUserAgent:(NSString *)logUserAgent
                LogDeviceType:(NSString *)logDeviceType
            completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)UpdateUserGeoLocation:(NSString *)userLat
                      UserLng:(NSString *)userLng
                LogSessionKey:(NSString *)logSessionKey
                  LogDeviceId:(NSString *)logDeviceId
                 LogUserAgent:(NSString *)logUserAgent
                LogDeviceType:(NSString *)logDeviceType
            completionHandler:(void (^)(BOOL result, NSError *error))completionHandler;

+ (void)CourierGetPendingPacks:(NSString *)start
                         count:(NSString *)count
                   LogDeviceId:(NSString *)logDeviceId
                  LogUserAgent:(NSString *)logUserAgent
                 LogDeviceType:(NSString *)logDeviceType
                    SessionKey:(NSString *)logSessionKey
             completionHandler:(void (^)(CourierPacksList *courierPackList, NSError *error))completionHandler;

+ (void)CourierGetPacksList:(NSString *)start
                      count:(NSString *)count
                       List:(MyDeliverySelection)packList
                LogDeviceId:(NSString *)logDeviceId
               LogUserAgent:(NSString *)logUserAgent
              LogDeviceType:(NSString *)logDeviceType
                 SessionKey:(NSString *)logSessionKey
          completionHandler:(void (^)(CourierPacksList *courierPackList, NSError *error))completionHandler;

+ (void)TakePack:(NSString *)packId
     LogDeviceId:(NSString *)logDeviceId
    LogUserAgent:(NSString *)logUserAgent
   LogDeviceType:(NSString *)logDeviceType
      SessionKey:(NSString *)logSessionKey
completionHandler:(void (^)(BOOL isTookPack, NSError *error))completionHandler;

+ (void)DeliverPack:(NSString *)packId
        LogDeviceId:(NSString *)logDeviceId
       LogUserAgent:(NSString *)logUserAgent
      LogDeviceType:(NSString *)logDeviceType
         SessionKey:(NSString *)logSessionKey
  completionHandler:(void (^)(BOOL isDelivered, NSError *error))completionHandler;

+ (void)AddDeliveryImg:(NSString *)packId
       PackImageBase64:(NSString *)base64Image
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
            SessionKey:(NSString *)logSessionKey
     completionHandler:(void (^)(AddImageObject *addImage, NSError *error))completionHandler;

+ (void)DeleteDeliveryImg:(NSString *)packId
            PackImageName:(NSString *)packImageName
              LogDeviceId:(NSString *)logDeviceId
             LogUserAgent:(NSString *)logUserAgent
            LogDeviceType:(NSString *)logDeviceType
               SessionKey:(NSString *)logSessionKey
        completionHandler:(void (^)(BOOL isImageDeleted, NSError *error))completionHandler;

+ (void)GetAllDeliveryImgs:(NSString *)packId
               LogDeviceId:(NSString *)logDeviceId
              LogUserAgent:(NSString *)logUserAgent
             LogDeviceType:(NSString *)logDeviceType
                SessionKey:(NSString *)logSessionKey
         completionHandler:(void (^)(NSMutableArray *deliveryImages, NSError *error))completionHandler;

+ (void)PackDidnotGiven:(NSString *)packId
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
             SessionKey:(NSString *)logSessionKey
      completionHandler:(void (^)(BOOL isImageDeleted, NSError *error))completionHandler;

+ (void)CourierReportProblem:(NSString *)packId
                  ReasonText:(NSString *)reasonText
                    ReasonId:(NSString *)reasonId
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(BOOL isPackCancelled, NSError *error))completionHandler;

+ (void)BeCourierOfPack:(NSString *)packId
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
             SessionKey:(NSString *)logSessionKey
      completionHandler:(void (^)(BOOL isPackCancelled, NSError *error))completionHandler;

+ (void)GetAllPackImages:(NSString *)packId
             LogDeviceId:(NSString *)logDeviceId
            LogUserAgent:(NSString *)logUserAgent
           LogDeviceType:(NSString *)logDeviceType
              SessionKey:(NSString *)logSessionKey
       completionHandler:(void (^)(NSMutableArray *deliveryImages, NSError *error))completionHandler;

+ (void)CancelPackageRequest:(NSString *)packId
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(BOOL isPackCancelled, NSError *error))completionHandler;

+ (void)GetAcceptRule:(NSString *)ruleModule
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
           SessionKey:(NSString *)logSessionKey
    completionHandler:(void (^)(NSMutableArray *rulesArray, NSError *error))completionHandler;

+ (void)AddCallHistory:(NSString *)callerId
             ReciverId:(NSString *)ReciverId
              CallDate:(NSString *)CallDate
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
            SessionKey:(NSString *)logSessionKey
     completionHandler:(void (^)(BOOL isCallAdded, NSError *error))completionHandler;


//********************************** Payments ******************

+ (void)GetMyPayments:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
           SessionKey:(NSString *)logSessionKey
    completionHandler:(void (^)(NSMutableArray *paymentHistory, NSError *error))completionHandler;

+ (void)GetMyPayouts:(NSString *)logDeviceId
        LogUserAgent:(NSString *)logUserAgent
       LogDeviceType:(NSString *)logDeviceType
          SessionKey:(NSString *)logSessionKey
   completionHandler:(void (^)(NSMutableArray *payoutHistory,NSString *totalAmount,NSString *deliveryCount, NSError *error))completionHandler;

+ (void)GetPayoutDetails:(NSString *)payoutID
             LogDeviceId:(NSString *)logDeviceId
            LogUserAgent:(NSString *)logUserAgent
           LogDeviceType:(NSString *)logDeviceType
              SessionKey:(NSString *)logSessionKey
       completionHandler:(void (^)(NSMutableArray *payoutDetails, NSError *error))completionHandler;


//********************************** Chat ******************

+ (void)SendChatMessage:(NSString *)recieverId
               ChatText:(NSString *)chatText
                PushNot:(NSString *)pushNot
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
             SessionKey:(NSString *)logSessionKey
      completionHandler:(void (^)(BOOL isSent, NSError *error))completionHandler;

+ (void)GetChatConversations:(NSString *)count
                       Start:(NSString *)start
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(NSMutableArray *chatConversationList, NSError *error))completionHandler;

+ (void)GetMessageList:(NSString *)recieverId
                 Count:(NSString *)count
                 Start:(NSString *)start
           LogDeviceId:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
            SessionKey:(NSString *)logSessionKey
     completionHandler:(void (^)(NSMutableArray *messagesList,NSString *lastCount, NSError *error))completionHandler;

+ (void)GetMessageCount:(NSString *)recieverId
            LogDeviceId:(NSString *)logDeviceId
           LogUserAgent:(NSString *)logUserAgent
          LogDeviceType:(NSString *)logDeviceType
             SessionKey:(NSString *)logSessionKey
      completionHandler:(void (^)(NSString *lastCount, NSError *error))completionHandler;

//********************************** Notifications ******************
+ (void)GetUserNotifications:(NSString *)markAsPushed
                      Status:(NSString *)status
                       Count:(NSString *)count
                       Start:(NSString *)start
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
                  SessionKey:(NSString *)logSessionKey
           completionHandler:(void (^)(NSMutableArray *notificationList, NSError *error))completionHandler;

+ (void)MarkNotAs:(NSString *)notIficationId
           IsRead:(NSString *)isRead
    logSessionKey:(NSString *)logSessionKey
      LogDeviceId:(NSString *)logDeviceId
     LogUserAgent:(NSString *)logUserAgent
    LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(BOOL isRead, NSError *error))completionHandler;

//********************************** FAQ ******************

+ (void)GetFaqById:(NSString *)faqId
     logSessionKey:(NSString *)logSessionKey
       LogDeviceId:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
 completionHandler:(void (^)(NSMutableArray *faqFaq, NSError *error))completionHandler;

+ (void)GetFaqHeaders:(NSString *)userType
        logSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(NSMutableArray *faqHeaders, NSError *error))completionHandler;

//********************************** Rate ******************
+ (void)GetUserRatesByUserID:(NSString *)courrierId
               logSessionKey:(NSString *)logSessionKey
                 LogDeviceId:(NSString *)logDeviceId
                LogUserAgent:(NSString *)logUserAgent
               LogDeviceType:(NSString *)logDeviceType
           completionHandler:(void (^)(NSMutableArray *courierRateList, NSError *error))completionHandler;

+ (void)GetUsersRates:(NSString *)start
                Count:(NSString *)count
        logSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(NSMutableArray *courierRateList, NSError *error))completionHandler;

+ (void)RateUser:(NSString *)courrierId
         RateVal:(NSString *)rateVal
   logSessionKey:(NSString *)logSessionKey
     LogDeviceId:(NSString *)logDeviceId
    LogUserAgent:(NSString *)logUserAgent
   LogDeviceType:(NSString *)logDeviceType
completionHandler:(void (^)(BOOL isRated, NSError *error))completionHandler;


+ (void)AddCourierReview:(NSString *)courierId
              reviewText:(NSString *)reviewText
           logSessionKey:(NSString *)logSessionKey
             LogDeviceId:(NSString *)logDeviceId
            LogUserAgent:(NSString *)logUserAgent
           LogDeviceType:(NSString *)logDeviceType
       completionHandler:(void (^)(BOOL isRated, NSError *error))completionHandler;

//********************************** Feedback ******************
+ (void)SendFeedback:(NSString *)feedbackText
            OptionId:(NSString *)optionId
       logSessionKey:(NSString *)logSessionKey
         LogDeviceId:(NSString *)logDeviceId
        LogUserAgent:(NSString *)logUserAgent
       LogDeviceType:(NSString *)logDeviceType
   completionHandler:(void (^)(BOOL isSent, NSError *error))completionHandler;

+ (void)GiveTip:(NSString *)pay_amount
        Pack_Id:(NSString *)pack_id
     Courier_Id:(NSString *)courier_id
  logSessionKey:(NSString *)logSessionKey
    LogDeviceId:(NSString *)logDeviceId
   LogUserAgent:(NSString *)logUserAgent
  LogDeviceType:(NSString *)logDeviceType
   completionHandler:(void (^)(BOOL isSent, NSError *error))completionHandler;


//********************************** Dictionaries ******************
+ (void)GetDicByName:(NSString *)name
       logSessionKey:(NSString *)logSessionKey
         LogDeviceId:(NSString *)logDeviceId
        LogUserAgent:(NSString *)logUserAgent
       LogDeviceType:(NSString *)logDeviceType
   completionHandler:(void (^)(NSMutableArray *dictionaries, NSError *error))completionHandler;

+ (void)GetAllDics:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
        SessionKey:(NSString *)logSessionKey
 completionHandler:(void (^)(NSMutableArray *dictionaries, NSError *error))completionHandler;

//********************************** Courier Bank Info ******************

+ (void)UpdateBankAccount:(NSString *)city
                    Line1:(NSString *)line1
                    Line2:(NSString *)line2
                    State:(NSString *)state
               PostalCode:(NSString *)postalCode
                     Date:(NSString *)date
              RouteNumber:(NSString *)route_number
            AccountNumber:(NSString *)account_number
            logSessionKey:(NSString *)logSessionKey
              LogDeviceId:(NSString *)logDeviceId
             LogUserAgent:(NSString *)logUserAgent
            LogDeviceType:(NSString *)logDeviceType
        completionHandler:(void (^)(BOOL isInserted, NSError *error))completionHandler;

+ (void)GetEstimatedNextPayment:(NSString *)logDeviceId
                   LogUserAgent:(NSString *)logUserAgent
                  LogDeviceType:(NSString *)logDeviceType
                     SessionKey:(NSString *)logSessionKey
              completionHandler:(void (^)(NSDictionary *paymentDate, NSError *error))completionHandler;

+ (void)GetBankAccount:(NSString *)logDeviceId
          LogUserAgent:(NSString *)logUserAgent
         LogDeviceType:(NSString *)logDeviceType
            SessionKey:(NSString *)logSessionKey
     completionHandler:(void (^)(BankAccountDetail *accountDetails, NSError *error))completionHandler;

+ (void)LogoutUser:(NSString *)logDeviceId
      LogUserAgent:(NSString *)logUserAgent
     LogDeviceType:(NSString *)logDeviceType
        SessionKey:(NSString *)logSessionKey
 completionHandler:(void (^)(BOOL isLogout, NSError *error))completionHandler;

//Location services
+ (void)GetCourierGeoLocation:(NSString *)courrierId
                logSessionKey:(NSString *)logSessionKey
                  LogDeviceId:(NSString *)logDeviceId
                 LogUserAgent:(NSString *)logUserAgent
                LogDeviceType:(NSString *)logDeviceType
            completionHandler:(void (^)(NSDictionary* locationDict, NSError *error))completionHandler;


//MARK - Version1.5 (Courier)
+ (void)UpdateNearestDistance:(NSString *)milesDistance
                logSessionKey:(NSString *)logSessionKey
                  LogDeviceId:(NSString *)logDeviceId
                 LogUserAgent:(NSString *)logUserAgent
                LogDeviceType:(NSString *)logDeviceType
            completionHandler:(void (^)(BOOL isUpdated, NSError *error))completionHandler;

+ (void)GetUserRoutes:(NSString *)userID
        logSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(NSMutableArray *routesList, NSError *error))completionHandler;

+ (void)AddUserRoutes:(NSString *)fromLat
              fromLng:(NSString *)fromLng
                toLat:(NSString *)toLat
                toLng:(NSString *)toLng
          fromAddress:(NSString *)fromAddress
            toAddress:(NSString *)toAddress
        recursiveType:(NSString *)recursiveType
            routeDate:(NSString *)routeDate
            dayOfWeek:(NSString *)dayOfWeek
        transportType:(NSString *)transportType
        logSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(BOOL isAddedRoute, NSError *error))completionHandler;

+ (void)UpdateUserRoutes:(NSString *)fromLat
                 fromLng:(NSString *)fromLng
                   toLat:(NSString *)toLat
                   toLng:(NSString *)toLng
             fromAddress:(NSString *)fromAddress
               toAddress:(NSString *)toAddress
           recursiveType:(NSString *)recursiveType
               routeDate:(NSString *)routeDate
               dayOfWeek:(NSString *)dayOfWeek
           transportType:(NSString *)transportType
                 routeId:(NSString *)routeId
           logSessionKey:(NSString *)logSessionKey
             LogDeviceId:(NSString *)logDeviceId
            LogUserAgent:(NSString *)logUserAgent
           LogDeviceType:(NSString *)logDeviceType
       completionHandler:(void (^)(BOOL isAddedRoute, NSError *error))completionHandler;

+ (void)DeleteUserRoutes:(NSString *)routeID
        logSessionKey:(NSString *)logSessionKey
          LogDeviceId:(NSString *)logDeviceId
         LogUserAgent:(NSString *)logUserAgent
        LogDeviceType:(NSString *)logDeviceType
    completionHandler:(void (^)(BOOL isDeletedRoute, NSError *error))completionHandler;



@end
