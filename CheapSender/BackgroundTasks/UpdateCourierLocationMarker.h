//
//  UpdateCourierLocationMarker.h
//  CheapSender
//
//  Created by Idrees on 2017/08/25.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseObject.h"

//Protocol setup
@protocol UpdateCourierLocationMarkerDelegate <NSObject>

- (void)notifyForUpdateCourierMarker;

@end

@interface UpdateCourierLocationMarker : BaseObject
@property (nonatomic, strong) NSTimer *updateLocationPeriodicallyTimer;

@property (nonatomic, assign) id <UpdateCourierLocationMarkerDelegate> updateCourierLocationMarkerDelegate;


- (void)startUpdateLocationThread;
- (void)stopUpdateLocationThread;


@end
