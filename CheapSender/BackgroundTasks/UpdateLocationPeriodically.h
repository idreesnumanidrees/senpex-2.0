//
//  UpdateLocationPeriodically.h
//  CheapSender
//
//  Created by Idrees on 2017/06/21.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseObject.h"

//Protocol setup
@protocol UpdateLocationPeriodicallyDelegate <NSObject>

- (void)notifyForLocationUpdate;
- (void)notifyForUpdatePendingPacksMap;

@end

@interface UpdateLocationPeriodically : BaseObject

//updateHistoricalLocationPeriodicallyTimer instance object of NSTimer
@property (nonatomic, strong) NSTimer *updateLocationPeriodicallyTimer;

@property (nonatomic, strong) NSTimer *updatePendingPacksTimer;

@property (nonatomic, assign) id <UpdateLocationPeriodicallyDelegate> updateLocationPeriodicallyDelegate;


//This method will start the thread for update location timer.
- (void)startUpdateLocationThread;

//This method will stop the thread for update location.
- (void)stopUpdateHistoricalLocation;

- (void)startUpdatePendingPacks;
- (void)stopUpdatePendingPacks;


@end
