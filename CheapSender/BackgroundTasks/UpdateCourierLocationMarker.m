//
//  UpdateCourierLocationMarker.m
//  CheapSender
//
//  Created by Idrees on 2017/08/25.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "UpdateCourierLocationMarker.h"

#define UPDATE_LOCATION_TIMER_INTERVAL 30.0


@implementation UpdateCourierLocationMarker

//This method will start the thread for update location timer.
- (void)startUpdateLocationThread
{
    //Timer invalidate check
    if(![_updateLocationPeriodicallyTimer isValid])
    {
        //Creates and returns a new NSTimer object and schedules it on the current run loop in the default mode.
        _updateLocationPeriodicallyTimer = [NSTimer scheduledTimerWithTimeInterval:UPDATE_LOCATION_TIMER_INTERVAL
                                                                            target:self
                                                                          selector:@selector(startLocationUpdatePeriodically)
                                                                          userInfo:nil
                                                                           repeats:YES];
    }
}

//This method will start the thread for update location.
- (void)startLocationUpdatePeriodically
{
    //This function is the fundamental mechanism for submitting blocks to a dispatch queue. Calls to this function always return immediately after the block has been submitted and never wait for the block to be invoked.
    
    [_updateCourierLocationMarkerDelegate notifyForUpdateCourierMarker];
}

- (void)stopUpdateLocationThread
{
    [_updateLocationPeriodicallyTimer invalidate];
    _updateLocationPeriodicallyTimer = nil;
}

@end
