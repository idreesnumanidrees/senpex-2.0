//
//  UpdateLocationPeriodically.m
//  CheapSender
//
//  Created by Idrees on 2017/06/21.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "UpdateLocationPeriodically.h"

#define UPDATE_LOCATION_TIMER_INTERVAL 60.0
#define UPDATE_PENDING_PACKS_TIMER_INTERVAL 60.0

@implementation UpdateLocationPeriodically

//This method will start the thread for update location timer.
- (void)startUpdateLocationThread
{
    //Timer invalidate check
    if(![_updateLocationPeriodicallyTimer isValid])
    {
        //Creates and returns a new NSTimer object and schedules it on the current run loop in the default mode.
        _updateLocationPeriodicallyTimer = [NSTimer scheduledTimerWithTimeInterval:UPDATE_LOCATION_TIMER_INTERVAL
                                                                                      target:self
                                                                                    selector:@selector(startLocationUpdatePeriodically)
                                                                                    userInfo:nil
                                                                                     repeats:YES];
    }
}

//This method will start the thread for update location.
- (void)startLocationUpdatePeriodically
{
    //This function is the fundamental mechanism for submitting blocks to a dispatch queue. Calls to this function always return immediately after the block has been submitted and never wait for the block to be invoked.
    
    [_updateLocationPeriodicallyDelegate notifyForLocationUpdate];
}

//This method will stop the thread for update location timer.
- (void)stopUpdateHistoricalLocation
{
    [_updateLocationPeriodicallyTimer invalidate];
    _updateLocationPeriodicallyTimer = nil;
}

- (void)startUpdatePendingPacks
{
    //Timer invalidate check
    if(![_updatePendingPacksTimer isValid])
    {
        //Creates and returns a new NSTimer object and schedules it on the current run loop in the default mode.
        _updatePendingPacksTimer = [NSTimer scheduledTimerWithTimeInterval:UPDATE_PENDING_PACKS_TIMER_INTERVAL
                                                                            target:self
                                                                          selector:@selector(startUpdatePendingPacksPeriodically)
                                                                          userInfo:nil
                                                                           repeats:YES];
    }
}

//This method will start the thread for update location.
- (void)startUpdatePendingPacksPeriodically
{
    //This function is the fundamental mechanism for submitting blocks to a dispatch queue. Calls to this function always return immediately after the block has been submitted and never wait for the block to be invoked.
    
    [_updateLocationPeriodicallyDelegate notifyForUpdatePendingPacksMap];
}

- (void)stopUpdatePendingPacks
{
    [_updatePendingPacksTimer invalidate];
    _updatePendingPacksTimer = nil;
}

@end
