//
//  CalendarHomeViewController.h
//  Calendar
//
//  Created by Allan on 14-6-23.
//  Copyright (c) 2014年 Allan. All rights reserved.
//
// 版权属于原作者
#import <UIKit/UIKit.h>
#import "CalendarViewController.h"

@protocol CalendarHomeDelegate<NSObject>
- (void)dateSelected:(NSString *)selectedDate;
@end



@interface CalendarHomeViewController : CalendarViewController
@property (nonatomic, assign) id <CalendarHomeDelegate> calendarDelegate;

@property (nonatomic, strong) NSString *calendartitle;//设置导航栏标题
@property (nonatomic, strong) NSString *selectedDateString;

- (void)setStartDateString:(NSString *) startDateString toDateString:(NSString *) endDateString andSelectedDateString:(NSString *) selectedDateString;
- (void)setHeighLitedCalenderDate:(NSDate *)selectDate;
@end
