//
//  CalendarViewController.m
//  Calendar
//
//  Created by Allan on 14-8-21.
//  Copyright (c) 2014年 Allan. All rights reserved.
//
// 版权属于原作者

#import "CalendarViewController.h"

#import "CalendarMonthCollectionViewLayout.h"
#import "CalendarMonthHeaderView.h"
#import "CalendarDayCell.h"

#import "CalendarDayModel.h"


@interface CalendarViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSTimer* timer;//定时器
}
@end

@implementation CalendarViewController

static NSString *MonthHeader = @"MonthHeaderView";

static NSString *DayCell = @"DayCell";


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        [self initData];
        [self initView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self prepareImageFromWebFonts];
    [self initData];
    [self initView];
    // Do any additional setup after loading the view.
}

- (void)prepareImageFromWebFonts
{

}

- (void)initView {
    CalendarMonthCollectionViewLayout *layout = [CalendarMonthCollectionViewLayout new];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height) collectionViewLayout:layout]; //初始化网格视图大小
    [self.collectionView registerClass:[CalendarDayCell class] forCellWithReuseIdentifier:DayCell];//cell重用设置ID
    [self.collectionView registerClass:[CalendarMonthHeaderView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MonthHeader];
    self.collectionView.delegate = self;//实现网格视图的delegate
    self.collectionView.dataSource = self;//实现网格视图的dataSource
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.collectionView];
  //  self.view.backgroundColor = [UIColor redColor];

}

-(void)initData{
    self.calendarMonth = [[NSMutableArray alloc]init];//每个月份的数组
}

#pragma mark - CollectionView代理方法
//定义展示的Section的个数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.calendarMonth.count;
}

//定义展示的UICollectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSMutableArray *monthArray = [self.calendarMonth objectAtIndex:section];
    return monthArray.count;
}

//每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CalendarDayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:DayCell forIndexPath:indexPath];
    NSMutableArray *monthArray = [self.calendarMonth objectAtIndex:indexPath.section];
   // NSLog(@"indexPath.section %ld",(long)indexPath.section);
    CalendarDayModel *model = [monthArray objectAtIndex:indexPath.row];
    cell.model = model;
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionReusableView *reusableview = nil;

    if (kind == UICollectionElementKindSectionHeader){
        NSMutableArray *month_Array = [self.calendarMonth objectAtIndex:indexPath.section];
        CalendarDayModel *model = [month_Array objectAtIndex:15];
        CalendarMonthHeaderView *monthHeader = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:MonthHeader forIndexPath:indexPath];
        NSDateFormatter *df = [[NSDateFormatter alloc] init] ;
        NSString *monthName = [[df monthSymbols] objectAtIndex:(model.month-1)];
        monthHeader.calendarHeaderLabel.text = [NSString stringWithFormat:@"%lu %@",(unsigned long)model.year,monthName];//@"日期";
        monthHeader.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:1.0f];
        reusableview = monthHeader;
    }

    return reusableview;
        
}

//UICollectionView被选中时调用的方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *month_Array = [self.calendarMonth objectAtIndex:indexPath.section];
    CalendarDayModel *model = [month_Array objectAtIndex:indexPath.row];
    
    if ([self checkIfSelectedDateExceededToday:model] == YES) {
        if (model.style != CellDayTypeEmpty) {
            [self.Logic selectLogic:model];
            timer = [NSTimer scheduledTimerWithTimeInterval:0.2
                                                     target:self
                                                   selector:@selector(onTimer)
                                                   userInfo:nil
                                                    repeats:NO];
            NSString *selectedDateString = [[NSString alloc]initWithFormat:@"%lu-%lu-%lu",(unsigned long)model.year,(unsigned long)model.month,(unsigned long)model.day] ;
            [self dateSelected:selectedDateString];
        }
        [self.collectionView reloadData];
    }
    else
    {
        
    }
    
}

//返回这个UICollectionView是否可以被选择
-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (NSDate *)dateToFormatedDate:(NSString *)dateStr {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    return date;
}

- (BOOL)checkIfSelectedDateExceededToday:(CalendarDayModel*)model {
    BOOL isExceeded = NO;
    NSDate *seletedDate = [self dateToFormatedDate:[NSString stringWithFormat:@"%lu-%lu-%lu",
                                                    (unsigned long)model.year,
                                                    (unsigned long)model.month,(unsigned long)model.day]];
    
    
    NSInteger dayDiff = (int)[[NSDate date] timeIntervalSinceNow] / (60*60*24);
   
    if([[NSDate date] compare:seletedDate] == NSOrderedAscending ) {
        isExceeded = YES;
    } else  if (dayDiff == 0) {
        isExceeded = YES;

    }
    return isExceeded;
}

- (void)onTimer {
    [timer invalidate];//定时器无效
    timer = nil;
}

- (void)dateSelected:(NSString *)selectedDateString
{
    
}

- (IBAction)backButtonTapped:(id)sender
{

}
@end
