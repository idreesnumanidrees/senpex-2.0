//
//  CalendarHomeViewController.m
//  Calendar
//
//  Created by Allan on 14-6-23.
//  Copyright (c) 2014年 Allan. All rights reserved.
//
// 版权属于原作者

#import "CalendarHomeViewController.h"
#import "CalendarDefines.h"
#import "NSDate+Utilities.h"


@interface CalendarHomeViewController ()
{
    NSDate *startDate, *endDate;

}
@end

@implementation CalendarHomeViewController

- (void)viewDidLoad {
    // Do any additional setup after loading the view.
    [super viewDidLoad];
    
    [self setStartDateString:[self getTimeDayFromDate]
                toDateString:[self previousYearDateFromCurrentDate]
       andSelectedDateString:_selectedDateString];
}

- (NSString *)previousYearDateFromCurrentDate {
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:[NSDate date]];
    
    NSDateComponents *newComponents = [[NSDateComponents alloc]init];
    newComponents.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    newComponents.timeZone = [NSTimeZone localTimeZone];
    [newComponents setDay:[dateComponents day]];
    [newComponents setMonth:[dateComponents month]];
    [newComponents setYear:[dateComponents year]+1];
    
    NSDate *combDate = [calendar dateFromComponents:newComponents];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormatter stringFromDate:combDate];
    return dateString;
}

#pragma mark - 设置方法
- (void)setStartDateString:(NSString *) startDateString toDateString:(NSString *) endDateString andSelectedDateString:(NSString *) selectedDateString {
    NSDate *date = [NSDate date];
    startDate = [date dateFromString:startDateString];
    endDate = [date dateFromString:endDateString];
    int days = [NSDate getDayNumbertoDay:startDate beforDay:endDate];
    NSDate *selectDate  = [NSDate date];
    if (selectedDateString) {
        selectDate = [selectDate dateFromString:selectedDateString];
    }
    super.Logic = [[CalendarLogic alloc]init];
    super.calendarMonth = [super.Logic reloadCalendarView:startDate selectDate:selectDate needDays:days];
    [super.collectionView reloadData];//刷新
    [self.view layoutIfNeeded];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:20
                                                 inSection:super.Logic.selectedMonthIndex];
    [super.collectionView scrollToItemAtIndexPath:indexPath
                                atScrollPosition:UICollectionViewScrollPositionTop
                                        animated:NO];
}

#pragma mark - 设置标题

- (void)setCalendartitle:(NSString *)calendartitle {
    [self.navigationItem setTitle:calendartitle];
}

- (void)setHeighLitedCalenderDate:(NSDate *)selectDate {
    int days = [NSDate getDayNumbertoDay:startDate beforDay:endDate];
    super.calendarMonth = [super.Logic reloadCalendarView:startDate selectDate:selectDate needDays:days];
    super.collectionView.scrollsToTop = YES;//刷新

    [super.collectionView reloadData];//刷新
    
    [self performSelector:@selector(loadDayCell:) withObject:selectDate afterDelay:.1];
    
    
}

- (void)loadDayCell:(NSDate *)selectDate {


    
    NSString *rowIndex = [selectDate getDateOnly];
    int calendarScrollDate = [rowIndex intValue]-1;
    int sectionIndex =  super.Logic.selectedMonthIndex;
    
    
    UICollectionViewScrollPosition position = UICollectionViewScrollPositionTop ;
    
    if (calendarScrollDate < 15) {
        position =  UICollectionViewScrollPositionCenteredVertically;
    }
    int itemCount = [[self.calendarMonth objectAtIndex:sectionIndex] count] - 30;
    calendarScrollDate = calendarScrollDate + itemCount;
    
    float yValueMargin = 0;
    int indexValue = 0;
    for (NSArray *arrObj in self.calendarMonth) {
        if (sectionIndex > indexValue) {
            yValueMargin += UIDaysCellHeight*([arrObj count]/7);
        } else {
            break;
        }
       
        indexValue++;
    }
    
    float yValue = yValueMargin + UIWeekTitleHeaderHeight*(calendarScrollDate/7) + UIWeekTitleHeaderHeight*(sectionIndex+1);
    NSLog(@"%f = yValue",yValue); //(UIWeekTitleHeaderHeight+WeekTitleLabelHeight)
    dispatch_async(dispatch_get_main_queue(), ^{

   // dispatch_async(dispatch_get_main_queue(), {
        [super.collectionView setContentOffset:CGPointMake(0, yValue-150) animated:YES];
        //setContentOffset(CGPointMake(0, 110), animated: true)
    });


    /*
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:calendarScrollDate
                                                     inSection:sectionIndex];
        [super.collectionView scrollToItemAtIndexPath:indexPath
                                     atScrollPosition:position
                                             animated:YES];*/

 
  
}

- (void)dateSelected:(NSString *)selectedDateString {
    if(_calendarDelegate != nil && [_calendarDelegate respondsToSelector:@selector(dateSelected:)]) {
        [_calendarDelegate dateSelected:selectedDateString];
    }
}

- (NSString *)getTimeDayFromDate {
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    [df_utc setDateFormat:@"yyyy-MM-dd"];
    NSString* ts_utc_string = [df_utc stringFromDate:[NSDate date]];
    return ts_utc_string;
}

@end
