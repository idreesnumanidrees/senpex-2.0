//
//  AppDelegate.h
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <UserNotifications/UserNotifications.h>
#import "AppController.h"
#import <CoreLocation/CoreLocation.h>
@import GoogleSignIn;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate, GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (strong, nonatomic) AppController *appController;

//NSString class instance variable holds the device tokenId
@property (strong, nonatomic) NSString *iosDeviceToken;
@property (strong, nonatomic) NSString *sessionKey;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) UserModel *user;

@property (strong, nonatomic) CLLocation* coureirLocation;


@property (assign, nonatomic) NSInteger isNavigateToOrderDetails;
@property (assign, nonatomic) NSInteger isNeedToOrderDetails;
@property (assign, nonatomic) NSInteger isNavigateToChatDetails;
@property (assign, nonatomic) NSInteger isNeedToChatDetails;
@property (assign, nonatomic) NSInteger isNeedToRefreshPackDetails;

@property (strong, nonatomic) NSMutableArray *allDictData;
@property (strong, nonatomic) NSMutableArray *courierRegistrationInfo;

@property (assign, nonatomic) BOOL shouldRotate;
- (void)saveContext;

#warning temp
@property (strong, nonatomic) NSString *vehicleId;
@property (strong, nonatomic) NSString *vehicleName;
@property (strong, nonatomic) NSString *vehicleImageName;



//Shared singleton object of appDelegate
+ (AppDelegate *)sharedAppDelegate;


@end

