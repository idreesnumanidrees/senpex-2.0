//
//  AUIAutoGrowingTextView.h
//
//  Created by Adam on 10/10/13.
//

#import <UIKit/UIKit.h>

@protocol AutoGrowingTextViewDelegate < NSObject >

- (void)textViewHeightChanged:(int)textViewHeight;

@end



@interface AUIAutoGrowingTextView : UITextView

@property (nonatomic, assign) id <AutoGrowingTextViewDelegate> autoGrowingTextViewDelegate;

@property (nonatomic) CGFloat maxHeight;
@property (nonatomic) CGFloat minHeight;

// TODO:
//@property(nonatomic) UIControlContentVerticalAlignment verticalAlignment;

@end


