//
//  ChatMessage.h
//  CheapSender
//
//  Created by Idrees on 2017/05/03.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseObject.h"

@interface ChatMessage : BaseObject

@property(nonatomic, strong)  NSString *message;
@property(nonatomic, assign)  int senderUserID;
@property(nonatomic, assign)  int sessionID;
@property(nonatomic, assign)  int chatID;
@property(nonatomic, assign)  int receiverUserID;

@property (nonatomic, assign) BOOL sentStatus;
@property (nonatomic) NSDate *updateStamp;
@property (nonatomic) NSDate *createStamp;

//Not obtained from server
@property (nonatomic, strong) NSString *senderUserName;
@property (nonatomic, assign) int localChatID;

@end
