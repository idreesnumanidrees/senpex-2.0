//
//  MyDeliverySelection.h
//  CheapSender
//
//  Created by Idrees on 2017/05/14.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#ifndef MyDeliverySelection_h
#define MyDeliverySelection_h

typedef NS_ENUM(NSInteger, MyDeliverySelection)
{
    New = 0,
    Active = 1,
    Completed = 2,
    GoOffline = 3,
    GoOnline = 4

};

#endif /* MyDeliverySelection_h */

