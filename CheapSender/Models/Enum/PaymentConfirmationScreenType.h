//
//  PaymentConfirmationScreenType.h
//  CheapSender
//
//  Created by Riyas Abdul Rahman on 5/8/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#ifndef PaymentConfirmationScreenType_h
#define PaymentConfirmationScreenType_h

typedef enum {
    paymentConfirmationRequest,
    paymentConfirmationDenied,
    paymentConfirmationAccepted
} PaymentConfirmationScreenType;

#define PaymentConfirmationRowHeight 63


#endif /* PaymentConfirmationScreenType_h */
