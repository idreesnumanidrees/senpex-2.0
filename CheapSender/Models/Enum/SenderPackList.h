//
//  SenderPackList.h
//  CheapSender
//
//  Created by Idrees on 2017/06/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#ifndef SenderPackList_h
#define SenderPackList_h


typedef NS_ENUM(NSInteger, SenderPackList)
{
    SENDER_ACTIVE = 0,
    SENDER_COMPLETED = 1,
    SENDER_DRAFT = 2,
    COURIER_ACTIVE = 3,
    COURIER_COMPLETED = 4,
    COURIER_NEW = 5,
    SENDER_RECEIVE = 6
};

#endif /* SenderPackList_h */
