//
//  CSUser.h
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseObject.h"
#import <GoogleMaps/GoogleMaps.h>

@interface CSUser : BaseObject

//Integer properties of existing objects
@property (assign, nonatomic) int userId;

//NSString property of NSString
@property (strong, nonatomic) NSString *name;

//Double properties of existing objects
@property (assign, nonatomic) double latitude;
@property (assign, nonatomic) double longitude;

//NSString property of NSString
@property (strong, nonatomic) NSString *address;

@property (strong, nonatomic) NSString *ovalImage;

@property (strong, nonatomic) NSString *imageName;

//GMSMarker properties of existing objects
@property (strong, nonatomic) GMSMarker *marker;

@end
