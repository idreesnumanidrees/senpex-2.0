//
//  AddImageObject.h
//  CheapSender
//
//  Created by Idrees on 2017/06/06.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseObject.h"

@interface AddImageObject : BaseObject


@property (strong, nonatomic) NSString *insertedId;
@property (strong, nonatomic) NSString *imageName;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *imageIdFromServer;
@property (assign, nonatomic) BOOL isUploaded;

@end
