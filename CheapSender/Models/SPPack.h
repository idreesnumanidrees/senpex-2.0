//
//  SPPack.h
//  CheapSender
//
//  Created by Idrees on 2017/06/14.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseObject.h"

@interface SPPack : BaseObject

@property (strong, nonatomic) NSString *packId;
@property (strong, nonatomic) NSString *packPriceWithoutIns;
@property (strong, nonatomic) NSString *packPriceWithIns;
@property (strong, nonatomic) NSString *packInsuranceMode;
@property (strong, nonatomic) NSString *packInsurancePrice;

@end
