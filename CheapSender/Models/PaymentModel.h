//
//  PaymentModel.h
//  TestProject
//
//  Created by Ambika on 04/05/17.
//  Copyright © 2017 ZCO. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PaymentModel : NSObject
@property NSString *cardNumber;
@property NSString *cardHolderName;
@property NSString *billingAddress;
@property NSString *cardCvv;
@property NSString *cardExpDate;

@end
