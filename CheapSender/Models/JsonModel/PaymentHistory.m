//
//  PaymentHistory.m
//
//  Created by   on 2017/07/13
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PaymentHistory.h"


NSString *const kPaymentHistoryInsertedDate = @"inserted_date";
NSString *const kPaymentHistoryOrderId = @"order_id";
NSString *const kPaymentHistoryStripeId = @"stripe_id";
NSString *const kPaymentHistorySendName = @"send_name";
NSString *const kPaymentHistoryPaymentModuleName = @"payment_module_name";
NSString *const kPaymentHistoryMcGross = @"mc_gross";
NSString *const kPaymentHistoryCurrency = @"currency";
NSString *const kPaymentHistoryPackStatus = @"pack_status";
NSString *const kPaymentHistoryPaymentModule = @"payment_module";



@interface PaymentHistory ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PaymentHistory

@synthesize insertedDate = _insertedDate;
@synthesize orderId = _orderId;
@synthesize stripeId = _stripeId;
@synthesize sendName = _sendName;
@synthesize paymentModuleName = _paymentModuleName;
@synthesize mcGross = _mcGross;
@synthesize currency = _currency;
@synthesize packStatus = _packStatus;
@synthesize paymentModule = _paymentModule;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.insertedDate = [self objectOrNilForKey:kPaymentHistoryInsertedDate fromDictionary:dict];
            self.orderId = [self objectOrNilForKey:kPaymentHistoryOrderId fromDictionary:dict];
            self.stripeId = [self objectOrNilForKey:kPaymentHistoryStripeId fromDictionary:dict];
            self.sendName = [self objectOrNilForKey:kPaymentHistorySendName fromDictionary:dict];
            self.paymentModuleName = [self objectOrNilForKey:kPaymentHistoryPaymentModuleName fromDictionary:dict];
            self.mcGross = [self objectOrNilForKey:kPaymentHistoryMcGross fromDictionary:dict];
            self.currency = [self objectOrNilForKey:kPaymentHistoryCurrency fromDictionary:dict];
           self.packStatus = [self objectOrNilForKey:kPaymentHistoryPackStatus fromDictionary:dict];
            self.paymentModule = [self objectOrNilForKey:kPaymentHistoryPaymentModule
                                          fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.insertedDate forKey:kPaymentHistoryInsertedDate];
    [mutableDict setValue:self.orderId forKey:kPaymentHistoryOrderId];
    [mutableDict setValue:self.stripeId forKey:kPaymentHistoryStripeId];
    [mutableDict setValue:self.sendName forKey:kPaymentHistorySendName];
    [mutableDict setValue:self.paymentModuleName forKey:kPaymentHistoryPaymentModuleName];
    [mutableDict setValue:self.mcGross forKey:kPaymentHistoryMcGross];
    [mutableDict setValue:self.currency forKey:kPaymentHistoryCurrency];
    [mutableDict setValue:self.packStatus forKey:kPaymentHistoryPackStatus];
    [mutableDict setValue:self.paymentModule forKey:kPaymentHistoryPaymentModule];


    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.insertedDate = [aDecoder decodeObjectForKey:kPaymentHistoryInsertedDate];
    self.orderId = [aDecoder decodeObjectForKey:kPaymentHistoryOrderId];
    self.stripeId = [aDecoder decodeObjectForKey:kPaymentHistoryStripeId];
    self.sendName = [aDecoder decodeObjectForKey:kPaymentHistorySendName];
    self.paymentModuleName = [aDecoder decodeObjectForKey:kPaymentHistoryPaymentModuleName];
    self.mcGross = [aDecoder decodeObjectForKey:kPaymentHistoryMcGross];
    self.currency = [aDecoder decodeObjectForKey:kPaymentHistoryCurrency];
    self.packStatus = [aDecoder decodeObjectForKey:kPaymentHistoryPackStatus];
    self.paymentModule = [aDecoder decodeObjectForKey:kPaymentHistoryPaymentModule];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_insertedDate forKey:kPaymentHistoryInsertedDate];
    [aCoder encodeObject:_orderId forKey:kPaymentHistoryOrderId];
    [aCoder encodeObject:_stripeId forKey:kPaymentHistoryStripeId];
    [aCoder encodeObject:_sendName forKey:kPaymentHistorySendName];
    [aCoder encodeObject:_paymentModuleName forKey:kPaymentHistoryPaymentModuleName];
    [aCoder encodeObject:_mcGross forKey:kPaymentHistoryMcGross];
    [aCoder encodeObject:_currency forKey:kPaymentHistoryCurrency];
    [aCoder encodeObject:_packStatus forKey:kPaymentHistoryPackStatus];
    [aCoder encodeObject:_paymentModule forKey:kPaymentHistoryPaymentModule];
}

- (id)copyWithZone:(NSZone *)zone {
    PaymentHistory *copy = [[PaymentHistory alloc] init];
    
    
    
    if (copy) {

        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.orderId = [self.orderId copyWithZone:zone];
        copy.stripeId = [self.stripeId copyWithZone:zone];
        copy.sendName = [self.sendName copyWithZone:zone];
        copy.paymentModuleName = [self.paymentModuleName copyWithZone:zone];
        copy.mcGross = [self.mcGross copyWithZone:zone];
        copy.currency = [self.currency copyWithZone:zone];
        copy.packStatus = [self.packStatus copyWithZone:zone];
        copy.paymentModule = [self.paymentModule copyWithZone:zone];
    }
    
    return copy;
}


@end
