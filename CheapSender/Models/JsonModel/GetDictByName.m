//
//  GetDictByName.m
//
//  Created by   on 2017/07/15
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "GetDictByName.h"


NSString *const kGetDictByNameDicId = @"dic_id";
NSString *const kGetDictByNameId = @"id";
NSString *const kGetDictByNameListId = @"list_id";
NSString *const kGetDictByNameOptionsText = @"options_text";
NSString *const kGetDictByNameListName = @"list_name";
NSString *const kGetDictByNameDicName = @"dic_name";
NSString *const kGetDictByNameRulesText = @"rules_text";
NSString *const kGetDictByNameDicShort = @"dic_short";


@interface GetDictByName ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GetDictByName

@synthesize dicId = _dicId;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize listId = _listId;
@synthesize optionsText = _optionsText;
@synthesize listName = _listName;
@synthesize dicName = _dicName;
@synthesize rulesText = _rulesText;
@synthesize dicShort = _dicShort;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.dicId = [self objectOrNilForKey:kGetDictByNameDicId fromDictionary:dict];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kGetDictByNameId fromDictionary:dict];
            self.listId = [self objectOrNilForKey:kGetDictByNameListId fromDictionary:dict];
            self.optionsText = [self objectOrNilForKey:kGetDictByNameOptionsText fromDictionary:dict];
            self.listName = [self objectOrNilForKey:kGetDictByNameListName fromDictionary:dict];
            self.dicName = [self objectOrNilForKey:kGetDictByNameDicName fromDictionary:dict];
            self.rulesText = [self objectOrNilForKey:kGetDictByNameRulesText fromDictionary:dict];
            self.dicShort = [self objectOrNilForKey:kGetDictByNameDicShort fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.dicId forKey:kGetDictByNameDicId];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kGetDictByNameId];
    [mutableDict setValue:self.listId forKey:kGetDictByNameListId];
    [mutableDict setValue:self.optionsText forKey:kGetDictByNameOptionsText];
    [mutableDict setValue:self.listName forKey:kGetDictByNameListName];
    [mutableDict setValue:self.dicName forKey:kGetDictByNameDicName];
    [mutableDict setValue:self.rulesText forKey:kGetDictByNameRulesText];
    [mutableDict setValue:self.dicShort forKey:kGetDictByNameDicShort];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.dicId = [aDecoder decodeObjectForKey:kGetDictByNameDicId];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kGetDictByNameId];
    self.listId = [aDecoder decodeObjectForKey:kGetDictByNameListId];
    self.optionsText = [aDecoder decodeObjectForKey:kGetDictByNameOptionsText];
    self.listName = [aDecoder decodeObjectForKey:kGetDictByNameListName];
    self.dicName = [aDecoder decodeObjectForKey:kGetDictByNameDicName];
    self.rulesText = [aDecoder decodeObjectForKey:kGetDictByNameRulesText];
    self.dicShort = [aDecoder decodeObjectForKey:kGetDictByNameDicShort];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_dicId forKey:kGetDictByNameDicId];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kGetDictByNameId];
    [aCoder encodeObject:_listId forKey:kGetDictByNameListId];
    [aCoder encodeObject:_optionsText forKey:kGetDictByNameOptionsText];
    [aCoder encodeObject:_listName forKey:kGetDictByNameListName];
    [aCoder encodeObject:_dicName forKey:kGetDictByNameDicName];
    [aCoder encodeObject:_rulesText forKey:kGetDictByNameRulesText];
    [aCoder encodeObject:_dicShort forKey:kGetDictByNameDicShort];
}

- (id)copyWithZone:(NSZone *)zone {
    GetDictByName *copy = [[GetDictByName alloc] init];
    
    
    
    if (copy) {

        copy.dicId = [self.dicId copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.listId = [self.listId copyWithZone:zone];
        copy.optionsText = [self.optionsText copyWithZone:zone];
        copy.listName = [self.listName copyWithZone:zone];
        copy.dicName = [self.dicName copyWithZone:zone];
        copy.rulesText = [self.rulesText copyWithZone:zone];
        copy.dicShort = [self.dicShort copyWithZone:zone];
    }
    
    return copy;
}


@end
