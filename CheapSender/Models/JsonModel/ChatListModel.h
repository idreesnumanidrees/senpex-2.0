//
//  ChatListModel.h
//
//  Created by   on 2017/07/24
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface ChatListModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *messageId;
@property (nonatomic, strong) NSString *senderName;
@property (nonatomic, strong) NSString *insertedDate;
@property (nonatomic, strong) NSString *chatStatus;
@property (nonatomic, strong) NSString *senderSurname;
@property (nonatomic, assign) id receiverSurname;
@property (nonatomic, strong) NSString *chatText;
@property (nonatomic, assign) id receiverName;
@property (nonatomic, strong) NSString *senderId;
@property (nonatomic, strong) NSString *receiverId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
