//
//  PayoutDetails.m
//
//  Created by   on 2017/08/26
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PayoutDetails.h"

NSString *const kPayoutDetailsPaymentModuleId = @"payment_module_id";
NSString *const kPayoutDetailsSendName = @"send_name";
NSString *const kPayoutDetailsPaymentAmount = @"payment_amount";
NSString *const kPayoutDetailsPackPrice = @"pack_price";
NSString *const kPayoutDetailsPackStatus = @"pack_status";
NSString *const kPayoutDetailsPaymentModule = @"payment_module";
NSString *const kPayoutDetailsPaymentModuleText = @"payment_module_text";

@interface PayoutDetails ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PayoutDetails

@synthesize paymentModuleId = _paymentModuleId;
@synthesize sendName = _sendName;
@synthesize paymentAmount = _paymentAmount;
@synthesize packPrice = _packPrice;
@synthesize packStatus = _packStatus;
@synthesize paymentModule = _paymentModule;
@synthesize paymentModuleText = _paymentModuleText;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.paymentModuleId = [self objectOrNilForKey:kPayoutDetailsPaymentModuleId fromDictionary:dict];
            self.sendName = [self objectOrNilForKey:kPayoutDetailsSendName fromDictionary:dict];
            self.paymentAmount = [self objectOrNilForKey:kPayoutDetailsPaymentAmount fromDictionary:dict];
            self.packPrice = [self objectOrNilForKey:kPayoutDetailsPackPrice fromDictionary:dict];
            self.packStatus = [self objectOrNilForKey:kPayoutDetailsPackStatus fromDictionary:dict];
        self.paymentModule = [self objectOrNilForKey:kPayoutDetailsPaymentModule fromDictionary:dict];
        self.paymentModuleText = [self objectOrNilForKey:kPayoutDetailsPaymentModuleText fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.paymentModuleId forKey:kPayoutDetailsPaymentModuleId];
    [mutableDict setValue:self.sendName forKey:kPayoutDetailsSendName];
    [mutableDict setValue:self.paymentAmount forKey:kPayoutDetailsPaymentAmount];
    [mutableDict setValue:self.packPrice forKey:kPayoutDetailsPackPrice];
    [mutableDict setValue:self.packStatus forKey:kPayoutDetailsPackStatus];
    
    [mutableDict setValue:self.paymentModule forKey:kPayoutDetailsPaymentModule];
    [mutableDict setValue:self.paymentModuleText forKey:kPayoutDetailsPaymentModuleText];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.paymentModuleId = [aDecoder decodeObjectForKey:kPayoutDetailsPaymentModuleId];
    self.sendName = [aDecoder decodeObjectForKey:kPayoutDetailsSendName];
    self.paymentAmount = [aDecoder decodeObjectForKey:kPayoutDetailsPaymentAmount];
    self.packPrice = [aDecoder decodeObjectForKey:kPayoutDetailsPackPrice];
    self.packStatus = [aDecoder decodeObjectForKey:kPayoutDetailsPackStatus];
    self.paymentModule = [aDecoder decodeObjectForKey:kPayoutDetailsPaymentModule];
    self.paymentModuleText = [aDecoder decodeObjectForKey:kPayoutDetailsPaymentModuleText];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_paymentModuleId forKey:kPayoutDetailsPaymentModuleId];
    [aCoder encodeObject:_sendName forKey:kPayoutDetailsSendName];
    [aCoder encodeObject:_paymentAmount forKey:kPayoutDetailsPaymentAmount];
    [aCoder encodeObject:_packPrice forKey:kPayoutDetailsPackPrice];
    [aCoder encodeObject:_packStatus forKey:kPayoutDetailsPackStatus];
    [aCoder encodeObject:_paymentModule forKey:kPayoutDetailsPaymentModule];
    [aCoder encodeObject:_paymentModuleText forKey:kPayoutDetailsPaymentModuleText];

}

- (id)copyWithZone:(NSZone *)zone {
    PayoutDetails *copy = [[PayoutDetails alloc] init];
    
    
    
    if (copy) {

        copy.paymentModuleId = [self.paymentModuleId copyWithZone:zone];
        copy.sendName = [self.sendName copyWithZone:zone];
        copy.paymentAmount = [self.paymentAmount copyWithZone:zone];
        copy.packPrice = [self.packPrice copyWithZone:zone];
        copy.packStatus = [self.packStatus copyWithZone:zone];
        copy.paymentModule = [self.paymentModule copyWithZone:zone];
        copy.paymentModuleText = [self.paymentModuleText copyWithZone:zone];
    }
    
    return copy;
}


@end
