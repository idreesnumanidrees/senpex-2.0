//
//  Data.h
//
//  Created by   on 2017/06/19
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface SenderData : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *packFromText;
@property (nonatomic, assign) id petTypeText;
@property (nonatomic, strong) NSString *courierEmail;
@property (nonatomic, strong) NSString *packOwnerEmail;
@property (nonatomic, assign) id petCount;
@property (nonatomic, strong) NSString *deliveryBringTypeText;
@property (nonatomic, strong) NSString *dataIdentifier;
@property (nonatomic, assign) id courierPaid;
@property (nonatomic, assign) id updatedBy;
@property (nonatomic, strong) NSString *receiverPhoneNumber;
@property (nonatomic, strong) NSString *sendName;
@property (nonatomic, strong) NSString *packOptionsText;
@property (nonatomic, strong) NSString *deliveryTakeType;
@property (nonatomic, strong) NSString *packToText;
@property (nonatomic, strong) NSString *statusSender;
@property (nonatomic, strong) NSString *acceptStatus;
@property (nonatomic, strong) NSString *acceptedCourierCell;
@property (nonatomic, assign) id distanceTime;
@property (nonatomic, strong) NSString *acceptedDate;
@property (nonatomic, strong) NSString *itemValue;
@property (nonatomic, strong) NSString *acceptedCourierName;
@property (nonatomic, strong) NSString *insertedBy;
@property (nonatomic, strong) NSString *imgTypeName;
@property (nonatomic, strong) NSString *takenAsapText;
@property (nonatomic, strong) NSString *packId;
@property (nonatomic, strong) NSString *sendCatText;
@property (nonatomic, strong) NSString *packStatus;
@property (nonatomic, strong) NSString *sendCatId;
@property (nonatomic, strong) NSString *takenBringTypeText;
@property (nonatomic, strong) NSString *packToLng;
@property (nonatomic, strong) NSString *bidText;
@property (nonatomic, assign) id takenTypeText;
@property (nonatomic, strong) NSString *packOwnerCell;
@property (nonatomic, strong) NSString *deliveryAsap;
@property (nonatomic, strong) NSString *packImg;
@property (nonatomic, strong) NSString *courierName;
@property (nonatomic, assign) id petIsVac;
@property (nonatomic, strong) NSString *packPrice;
@property (nonatomic, strong) NSString *packOwnerName;
@property (nonatomic, assign) id petIsVacText;
@property (nonatomic, strong) NSString *imgId;
@property (nonatomic, strong) NSString *statusCourier;
@property (nonatomic, strong) NSString *packSizeText;
@property (nonatomic, strong) NSString *leaveByDoor;
@property (nonatomic, strong) NSString *packOwnerSurname;
@property (nonatomic, strong) NSString *reasonId;
@property (nonatomic, strong) NSString *packFromLat;
@property (nonatomic, assign) id petType;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *deliveryAsapText;
@property (nonatomic, strong) NSString *reasonType;
@property (nonatomic, strong) NSString *takenType;
@property (nonatomic, strong) NSString *leaveByDoorText;
@property (nonatomic, strong) NSString *reasonTemplateText;
@property (nonatomic, strong) NSString *courierCell;
@property (nonatomic, strong) NSString *acceptedCourierSurname;
@property (nonatomic, strong) NSString *takenBringType;
@property (nonatomic, strong) NSString *deliveryTime;
@property (nonatomic, strong) NSString *insuranceMode;
@property (nonatomic, strong) NSString *bidderId;
@property (nonatomic, strong) NSString *bidDate;
@property (nonatomic, strong) NSString *reasonText;
@property (nonatomic, assign) id deliveryTypeText;
@property (nonatomic, strong) NSString *takenTime;
@property (nonatomic, strong) NSString *acceptedCourierId;
@property (nonatomic, strong) NSString *acceptStatusText;
@property (nonatomic, strong) NSString *isInsurancePayed;
@property (nonatomic, strong) NSString *receiverName;
@property (nonatomic, strong) NSString *packFromLng;
@property (nonatomic, strong) NSString *acceptedCourierEmail;
@property (nonatomic, strong) NSString *packOwnerId;
@property (nonatomic, strong) NSString *packOptionsId;
@property (nonatomic, strong) NSString *packToLat;
@property (nonatomic, assign) id updatedDate;
@property (nonatomic, strong) NSString *insertedDate;
@property (nonatomic, strong) NSString *courierSurname;
@property (nonatomic, strong) NSString *descText;
@property (nonatomic, strong) NSString *packWeight;
@property (nonatomic, strong) NSString *deliveryType;
@property (nonatomic, strong) NSString *insurancePrice;
@property (nonatomic, strong) NSString *takenAsap;
@property (nonatomic, strong) NSString *senderPaid;
@property (nonatomic, strong) NSString *packSizeId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
