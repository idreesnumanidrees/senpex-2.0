//
//  PayoutHistory.m
//
//  Created by   on 2017/07/13
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PayoutHistory.h"


NSString *const kPayoutHistoryId = @"id";
NSString *const kPayoutHistoryPaymentAmount = @"payment_amount";
NSString *const kPayoutHistoryUserId = @"user_id";
NSString *const kPayoutHistoryPayoutDate = @"payout_date";
NSString *const kPayoutHistoryIsPaid = @"is_paid";
NSString *const kPayoutHistoryDeliveryCount = @"delivery_count";

@interface PayoutHistory ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PayoutHistory

@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize paymentAmount = _paymentAmount;
@synthesize userId = _userId;
@synthesize payoutDate = _payoutDate;
@synthesize isPaid = _isPaid;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kPayoutHistoryId fromDictionary:dict];
            self.paymentAmount = [self objectOrNilForKey:kPayoutHistoryPaymentAmount fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kPayoutHistoryUserId fromDictionary:dict];
            self.payoutDate = [self objectOrNilForKey:kPayoutHistoryPayoutDate fromDictionary:dict];
            self.isPaid = [self objectOrNilForKey:kPayoutHistoryIsPaid fromDictionary:dict];
            self.deliveryCount = [self objectOrNilForKey:kPayoutHistoryDeliveryCount fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kPayoutHistoryId];
    [mutableDict setValue:self.paymentAmount forKey:kPayoutHistoryPaymentAmount];
    [mutableDict setValue:self.userId forKey:kPayoutHistoryUserId];
    [mutableDict setValue:self.payoutDate forKey:kPayoutHistoryPayoutDate];
    [mutableDict setValue:self.isPaid forKey:kPayoutHistoryIsPaid];
    [mutableDict setValue:self.deliveryCount forKey:kPayoutHistoryDeliveryCount];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kPayoutHistoryId];
    self.paymentAmount = [aDecoder decodeObjectForKey:kPayoutHistoryPaymentAmount];
    self.userId = [aDecoder decodeObjectForKey:kPayoutHistoryUserId];
    self.payoutDate = [aDecoder decodeObjectForKey:kPayoutHistoryPayoutDate];
    self.isPaid = [aDecoder decodeObjectForKey:kPayoutHistoryIsPaid];
    self.deliveryCount = [aDecoder decodeObjectForKey:kPayoutHistoryDeliveryCount];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kPayoutHistoryId];
    [aCoder encodeObject:_paymentAmount forKey:kPayoutHistoryPaymentAmount];
    [aCoder encodeObject:_userId forKey:kPayoutHistoryUserId];
    [aCoder encodeObject:_payoutDate forKey:kPayoutHistoryPayoutDate];
    [aCoder encodeObject:_isPaid forKey:kPayoutHistoryIsPaid];
    [aCoder encodeObject:_deliveryCount forKey:kPayoutHistoryDeliveryCount];

}

- (id)copyWithZone:(NSZone *)zone {
    PayoutHistory *copy = [[PayoutHistory alloc] init];
    
    
    
    if (copy) {

        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.paymentAmount = [self.paymentAmount copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.payoutDate = [self.payoutDate copyWithZone:zone];
        copy.isPaid = [self.isPaid copyWithZone:zone];
        copy.deliveryCount = [self.deliveryCount copyWithZone:zone];
    }
    
    return copy;
}


@end
