//
//  PaymentHistory.h
//
//  Created by   on 2017/07/13
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PaymentHistory : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *insertedDate;
@property (nonatomic, strong) NSString *orderId;
@property (nonatomic, strong) NSString *stripeId;
@property (nonatomic, strong) NSString *sendName;
@property (nonatomic, strong) NSString *paymentModuleName;
@property (nonatomic, strong) NSString *mcGross;
@property (nonatomic, strong) NSString *currency;
@property (nonatomic, strong) NSString *packStatus;
@property (nonatomic, strong) NSString *paymentModule;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
