//
//  GetCourierInsuranceGen.h
//
//  Created by   on 2017/07/08
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GetCourierInsuranceGen : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *insImg1;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *expDate;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *insImg2;
@property (nonatomic, strong) NSString *policyNumber;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
