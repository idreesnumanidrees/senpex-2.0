//
//  ChatListModel.m
//
//  Created by   on 2017/07/24
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "ChatListModel.h"

NSString *const kChatListModelMessageId = @"id";
NSString *const kChatListModelSenderName = @"sender_name";
NSString *const kChatListModelInsertedDate = @"inserted_date";
NSString *const kChatListModelChatStatus = @"chat_status";
NSString *const kChatListModelSenderSurname = @"sender_surname";
NSString *const kChatListModelReceiverSurname = @"receiver_surname";
NSString *const kChatListModelChatText = @"chat_text";
NSString *const kChatListModelReceiverName = @"receiver_name";
NSString *const kChatListModelSenderId = @"sender_id";
NSString *const kChatListModelReceiverId = @"receiver_id";


@interface ChatListModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation ChatListModel

@synthesize messageId = _messageId;
@synthesize senderName = _senderName;
@synthesize insertedDate = _insertedDate;
@synthesize chatStatus = _chatStatus;
@synthesize senderSurname = _senderSurname;
@synthesize receiverSurname = _receiverSurname;
@synthesize chatText = _chatText;
@synthesize receiverName = _receiverName;
@synthesize senderId = _senderId;
@synthesize receiverId = _receiverId;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        
           self.messageId = [self objectOrNilForKey:kChatListModelMessageId fromDictionary:dict];
            self.senderName = [self objectOrNilForKey:kChatListModelSenderName fromDictionary:dict];
            self.insertedDate = [self objectOrNilForKey:kChatListModelInsertedDate fromDictionary:dict];
            self.chatStatus = [self objectOrNilForKey:kChatListModelChatStatus fromDictionary:dict];
            self.senderSurname = [self objectOrNilForKey:kChatListModelSenderSurname fromDictionary:dict];
            self.receiverSurname = [self objectOrNilForKey:kChatListModelReceiverSurname fromDictionary:dict];
            self.chatText = [self objectOrNilForKey:kChatListModelChatText fromDictionary:dict];
            self.receiverName = [self objectOrNilForKey:kChatListModelReceiverName fromDictionary:dict];
            self.senderId = [self objectOrNilForKey:kChatListModelSenderId fromDictionary:dict];
            self.receiverId = [self objectOrNilForKey:kChatListModelReceiverId fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    
    [mutableDict setValue:self.messageId forKey:kChatListModelMessageId];
    [mutableDict setValue:self.senderName forKey:kChatListModelSenderName];
    [mutableDict setValue:self.insertedDate forKey:kChatListModelInsertedDate];
    [mutableDict setValue:self.chatStatus forKey:kChatListModelChatStatus];
    [mutableDict setValue:self.senderSurname forKey:kChatListModelSenderSurname];
    [mutableDict setValue:self.receiverSurname forKey:kChatListModelReceiverSurname];
    [mutableDict setValue:self.chatText forKey:kChatListModelChatText];
    [mutableDict setValue:self.receiverName forKey:kChatListModelReceiverName];
    [mutableDict setValue:self.senderId forKey:kChatListModelSenderId];
    [mutableDict setValue:self.receiverId forKey:kChatListModelReceiverId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.messageId = [aDecoder decodeObjectForKey:kChatListModelMessageId];
    self.senderName = [aDecoder decodeObjectForKey:kChatListModelSenderName];
    self.insertedDate = [aDecoder decodeObjectForKey:kChatListModelInsertedDate];
    self.chatStatus = [aDecoder decodeObjectForKey:kChatListModelChatStatus];
    self.senderSurname = [aDecoder decodeObjectForKey:kChatListModelSenderSurname];
    self.receiverSurname = [aDecoder decodeObjectForKey:kChatListModelReceiverSurname];
    self.chatText = [aDecoder decodeObjectForKey:kChatListModelChatText];
    self.receiverName = [aDecoder decodeObjectForKey:kChatListModelReceiverName];
    self.senderId = [aDecoder decodeObjectForKey:kChatListModelSenderId];
    self.receiverId = [aDecoder decodeObjectForKey:kChatListModelReceiverId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_messageId forKey:kChatListModelMessageId];
    [aCoder encodeObject:_senderName forKey:kChatListModelSenderName];
    [aCoder encodeObject:_insertedDate forKey:kChatListModelInsertedDate];
    [aCoder encodeObject:_chatStatus forKey:kChatListModelChatStatus];
    [aCoder encodeObject:_senderSurname forKey:kChatListModelSenderSurname];
    [aCoder encodeObject:_receiverSurname forKey:kChatListModelReceiverSurname];
    [aCoder encodeObject:_chatText forKey:kChatListModelChatText];
    [aCoder encodeObject:_receiverName forKey:kChatListModelReceiverName];
    [aCoder encodeObject:_senderId forKey:kChatListModelSenderId];
    [aCoder encodeObject:_receiverId forKey:kChatListModelReceiverId];
}

- (id)copyWithZone:(NSZone *)zone {
    ChatListModel *copy = [[ChatListModel alloc] init];
    
        if (copy) {

        copy.messageId = [self.messageId copyWithZone:zone];
        copy.senderName = [self.senderName copyWithZone:zone];
        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.chatStatus = [self.chatStatus copyWithZone:zone];
        copy.senderSurname = [self.senderSurname copyWithZone:zone];
        copy.receiverSurname = [self.receiverSurname copyWithZone:zone];
        copy.chatText = [self.chatText copyWithZone:zone];
        copy.receiverName = [self.receiverName copyWithZone:zone];
        copy.senderId = [self.senderId copyWithZone:zone];
        copy.receiverId = [self.receiverId copyWithZone:zone];
    }
    
    return copy;
}


@end
