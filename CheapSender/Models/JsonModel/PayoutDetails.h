//
//  PayoutDetails.h
//
//  Created by   on 2017/08/26
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PayoutDetails : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *paymentModuleId;
@property (nonatomic, strong) NSString *sendName;
@property (nonatomic, strong) NSString *paymentAmount;
@property (nonatomic, strong) NSString *packPrice;
@property (nonatomic, strong) NSString *packStatus;
@property (nonatomic, strong) NSString *paymentModule;
@property (nonatomic, strong) NSString *paymentModuleText;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
