//
//  MAXACTIVEPACKS.h
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface MAXACTIVEPACKS : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *mAXACTIVEPACKS;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
