//
//  MAXACTIVEPACKS.m
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "MAXACTIVEPACKS.h"


NSString *const kMAXACTIVEPACKSDesc = @"desc";
NSString *const kMAXACTIVEPACKSMAXACTIVEPACKS = @"MAX_ACTIVE_PACKS";


@interface MAXACTIVEPACKS ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation MAXACTIVEPACKS

@synthesize desc = _desc;
@synthesize mAXACTIVEPACKS = _mAXACTIVEPACKS;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.desc = [self objectOrNilForKey:kMAXACTIVEPACKSDesc fromDictionary:dict];
            self.mAXACTIVEPACKS = [self objectOrNilForKey:kMAXACTIVEPACKSMAXACTIVEPACKS fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.desc forKey:kMAXACTIVEPACKSDesc];
    [mutableDict setValue:self.mAXACTIVEPACKS forKey:kMAXACTIVEPACKSMAXACTIVEPACKS];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.desc = [aDecoder decodeObjectForKey:kMAXACTIVEPACKSDesc];
    self.mAXACTIVEPACKS = [aDecoder decodeObjectForKey:kMAXACTIVEPACKSMAXACTIVEPACKS];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_desc forKey:kMAXACTIVEPACKSDesc];
    [aCoder encodeObject:_mAXACTIVEPACKS forKey:kMAXACTIVEPACKSMAXACTIVEPACKS];
}

- (id)copyWithZone:(NSZone *)zone {
    MAXACTIVEPACKS *copy = [[MAXACTIVEPACKS alloc] init];
    
    
    
    if (copy) {

        copy.desc = [self.desc copyWithZone:zone];
        copy.mAXACTIVEPACKS = [self.mAXACTIVEPACKS copyWithZone:zone];
    }
    
    return copy;
}


@end
