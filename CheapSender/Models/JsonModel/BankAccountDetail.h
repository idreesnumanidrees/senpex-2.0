//
//  BankAccountDetail.h
//
//  Created by   on 2017/07/21
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface BankAccountDetail : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *payoutsEnabled;
@property (nonatomic, assign) id verifiedMsg;
@property (nonatomic, strong) NSString *isVerified;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *accdate;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *accountNumber;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *line1;
@property (nonatomic, strong) NSString *routeNumber;
@property (nonatomic, strong) NSString *city;
@property (nonatomic, strong) NSString *postalCode;
@property (nonatomic, strong) NSString *line2;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
