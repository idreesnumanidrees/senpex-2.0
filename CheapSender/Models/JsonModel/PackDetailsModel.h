
//
//  PackDetailsModel.h
//
//  Created by   on 2017/06/15
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PackDetailsModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *packFromText;
@property (nonatomic, assign) id petTypeText;
@property (nonatomic, strong) NSString *packOwnerEmail;
@property (nonatomic, assign) id deliveryBringTypeText;
@property (nonatomic, assign) id petCount;
@property (nonatomic, strong) NSArray *packImages;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *courierPaid;
@property (nonatomic, assign) id updatedBy;

@property (nonatomic, strong) NSString *courierProfitD;

@property (nonatomic, assign) id acceptedCourierAvailStatus;
@property (nonatomic, strong) NSString *acceptedCourierSelfImg;
@property (nonatomic, assign) id acceptedCourierRate;
@property (nonatomic, strong) NSString *receiverPhoneNumber;
@property (nonatomic, strong) NSString *sendName;
@property (nonatomic, strong) NSString *packOptionsText;
@property (nonatomic, assign) id deliveryTakeType;
@property (nonatomic, strong) NSString *packToText;
@property (nonatomic, strong) NSString *statusSender;
@property (nonatomic, strong) NSString *acceptedCourierCell;
@property (nonatomic, strong) NSString *distanceTime;
@property (nonatomic, strong) NSString *itemValue;
@property (nonatomic, strong) NSString * cLastLng;
@property (nonatomic, strong) NSString *acceptedCourierName;
@property (nonatomic, strong) NSString *insertedBy;
@property (nonatomic, strong) NSString *imgTypeName;
@property (nonatomic, strong) NSString *takenAsapText;
@property (nonatomic, strong) NSArray *packBidders;
@property (nonatomic, strong) NSString *sendCatText;
@property (nonatomic, strong) NSString *packStatus;
@property (nonatomic, strong) NSString *sendCatId;
@property (nonatomic, assign) id takenBringTypeText;
@property (nonatomic, strong) NSString *packToLng;
@property (nonatomic, strong) NSString *takenTypeText;
@property (nonatomic, strong) NSString *packOwnerCell;
@property (nonatomic, strong) NSString *cLastLat;
@property (nonatomic, strong) NSString *deliveryAsap;
@property (nonatomic, strong) NSString *packImg;
@property (nonatomic, assign) id petIsVac;
@property (nonatomic, strong) NSString *packOwnerName;
@property (nonatomic, strong) NSString *packOwnerSelfImage;

@property (nonatomic, assign) id packPrice;
@property (nonatomic, assign) id petIsVacText;
@property (nonatomic, strong) NSString *imgId;
@property (nonatomic, strong) NSString *statusCourier;
@property (nonatomic, strong) NSString *packSizeText;
@property (nonatomic, assign) id leaveByDoor;
@property (nonatomic, strong) NSString *packOwnerSurname;
@property (nonatomic, assign) id petType;
@property (nonatomic, strong) NSString *packFromLat;
@property (nonatomic, strong) NSString *distance;
@property (nonatomic, strong) NSString *deliveryAsapText;
@property (nonatomic, strong) NSString *takenType;
@property (nonatomic, assign) id leaveByDoorText;
@property (nonatomic, strong) NSString *acceptedCourierSurname;
@property (nonatomic, assign) id insuranceMode;
@property (nonatomic, strong) NSString *deliveryTime;
@property (nonatomic, assign) id takenBringType;
@property (nonatomic, strong) NSString *deliveryTypeText;
@property (nonatomic, strong) NSString *takenTime;
@property (nonatomic, strong) NSString *acceptedCourierId;
@property (nonatomic, assign) id isInsurancePayed;
@property (nonatomic, strong) NSString *receiverName;
@property (nonatomic, strong) NSString *packFromLng;
@property (nonatomic, assign) id acceptedCourierEmail;
@property (nonatomic, strong) NSString *packOwnerId;
@property (nonatomic, strong) NSString *packOptionsId;
@property (nonatomic, strong) NSString *packToLat;
@property (nonatomic, assign) id updatedDate;
@property (nonatomic, strong) NSString *insertedDate;
@property (nonatomic, strong) NSString *descText;
@property (nonatomic, strong) NSString *packWeight;
@property (nonatomic, strong) NSString *deliveryType;
@property (nonatomic, assign) id insurancePrice;
@property (nonatomic, strong) NSString *takenAsap;
@property (nonatomic, strong) NSString *senderPaid;
@property (nonatomic, strong) NSString *packSizeId;
@property (nonatomic, strong) NSString *lastOperationTime;
@property (nonatomic, strong) NSString *waitingTime;
@property (nonatomic, strong) NSString *tips;
@property (nonatomic, strong) NSString *tariffText;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
