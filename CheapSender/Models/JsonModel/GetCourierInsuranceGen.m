//
//  GetCourierInsuranceGen.m
//
//  Created by   on 2017/07/08
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "GetCourierInsuranceGen.h"


NSString *const kGetCourierInsuranceGenInsImg1 = @"ins_img1";
NSString *const kGetCourierInsuranceGenId = @"id";
NSString *const kGetCourierInsuranceGenExpDate = @"exp_date";
NSString *const kGetCourierInsuranceGenUserId = @"user_id";
NSString *const kGetCourierInsuranceGenInsImg2 = @"ins_img2";
NSString *const kGetCourierInsuranceGenPolicyNumber = @"policy_number";


@interface GetCourierInsuranceGen ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GetCourierInsuranceGen

@synthesize insImg1 = _insImg1;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize expDate = _expDate;
@synthesize userId = _userId;
@synthesize insImg2 = _insImg2;
@synthesize policyNumber = _policyNumber;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.insImg1 = [self objectOrNilForKey:kGetCourierInsuranceGenInsImg1 fromDictionary:dict];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kGetCourierInsuranceGenId fromDictionary:dict];
            self.expDate = [self objectOrNilForKey:kGetCourierInsuranceGenExpDate fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kGetCourierInsuranceGenUserId fromDictionary:dict];
            self.insImg2 = [self objectOrNilForKey:kGetCourierInsuranceGenInsImg2 fromDictionary:dict];
            self.policyNumber = [self objectOrNilForKey:kGetCourierInsuranceGenPolicyNumber fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.insImg1 forKey:kGetCourierInsuranceGenInsImg1];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kGetCourierInsuranceGenId];
    [mutableDict setValue:self.expDate forKey:kGetCourierInsuranceGenExpDate];
    [mutableDict setValue:self.userId forKey:kGetCourierInsuranceGenUserId];
    [mutableDict setValue:self.insImg2 forKey:kGetCourierInsuranceGenInsImg2];
    [mutableDict setValue:self.policyNumber forKey:kGetCourierInsuranceGenPolicyNumber];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.insImg1 = [aDecoder decodeObjectForKey:kGetCourierInsuranceGenInsImg1];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kGetCourierInsuranceGenId];
    self.expDate = [aDecoder decodeObjectForKey:kGetCourierInsuranceGenExpDate];
    self.userId = [aDecoder decodeObjectForKey:kGetCourierInsuranceGenUserId];
    self.insImg2 = [aDecoder decodeObjectForKey:kGetCourierInsuranceGenInsImg2];
    self.policyNumber = [aDecoder decodeObjectForKey:kGetCourierInsuranceGenPolicyNumber];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_insImg1 forKey:kGetCourierInsuranceGenInsImg1];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kGetCourierInsuranceGenId];
    [aCoder encodeObject:_expDate forKey:kGetCourierInsuranceGenExpDate];
    [aCoder encodeObject:_userId forKey:kGetCourierInsuranceGenUserId];
    [aCoder encodeObject:_insImg2 forKey:kGetCourierInsuranceGenInsImg2];
    [aCoder encodeObject:_policyNumber forKey:kGetCourierInsuranceGenPolicyNumber];
}

- (id)copyWithZone:(NSZone *)zone {
    GetCourierInsuranceGen *copy = [[GetCourierInsuranceGen alloc] init];
    
    
    
    if (copy) {

        copy.insImg1 = [self.insImg1 copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.expDate = [self.expDate copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.insImg2 = [self.insImg2 copyWithZone:zone];
        copy.policyNumber = [self.policyNumber copyWithZone:zone];
    }
    
    return copy;
}


@end
