//
//  GetDictByName.h
//
//  Created by   on 2017/07/15
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GetDictByName : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *dicId;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *listId;
@property (nonatomic, assign) id optionsText;
@property (nonatomic, strong) NSString *listName;
@property (nonatomic, strong) NSString *dicName;
@property (nonatomic, assign) id rulesText;
@property (nonatomic, strong) NSString *dicShort;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
