//
//  DataModels.h
//
//  Created by   on 2017/06/03
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "UserModel.h"
#import "UserProfiles.h"
#import "PackDetailsModel.h"
#import "PackImages.h"
#import "SenderPacksList.h"
#import "CourierPacksList.h"
#import "GetCourierInsuranceGen.h"
#import "GetTransports.h"
#import "PaymentHistory.h"
#import "PayoutHistory.h"
#import "GetDictByName.h"
#import "RateUser.h"
#import "Notification.h"
#import "BankAccountDetail.h"
#import "CourierConversations.h"
#import "ChatListModel.h"
#import "PayoutDetails.h"



#import "UserProfiles.h"
#import "INSVALUE.h"
#import "NEARESTDAYS.h"
#import "MAXACTIVEPACKS.h"
#import "GenConfig.h"
#import "NEARESTDISTANCE.h"
#import "IMAGEPATH.h"
