//
//  RateUser.h
//
//  Created by   on 2017/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface RateUser : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *userRate;
@property (nonatomic, strong) NSString *surname;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *selfImg;
@property (nonatomic, strong) NSString *cnt;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *total;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
