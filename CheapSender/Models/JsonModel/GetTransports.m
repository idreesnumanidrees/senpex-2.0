//
//  GetTransports.m
//
//  Created by   on 2017/07/10
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "GetTransports.h"


NSString *const kGetTransportsTransportModel = @"transport_model";
NSString *const kGetTransportsTransportMake = @"transport_make";
NSString *const kGetTransportsPlateNumber = @"plate_number";
NSString *const kGetTransportsId = @"id";
NSString *const kGetTransportsTransportColor = @"transport_color";
NSString *const kGetTransportsUserId = @"user_id";
NSString *const kGetTransportsTransportType = @"transport_type";
NSString *const kGetTransportsTransportYear = @"transport_year";


@interface GetTransports ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GetTransports

@synthesize transportModel = _transportModel;
@synthesize transportMake = _transportMake;
@synthesize plateNumber = _plateNumber;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize transportColor = _transportColor;
@synthesize userId = _userId;
@synthesize transportType = _transportType;
@synthesize transportYear = _transportYear;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.transportModel = [self objectOrNilForKey:kGetTransportsTransportModel fromDictionary:dict];
            self.transportMake = [self objectOrNilForKey:kGetTransportsTransportMake fromDictionary:dict];
            self.plateNumber = [self objectOrNilForKey:kGetTransportsPlateNumber fromDictionary:dict];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kGetTransportsId fromDictionary:dict];
            self.transportColor = [self objectOrNilForKey:kGetTransportsTransportColor fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kGetTransportsUserId fromDictionary:dict];
            self.transportType = [self objectOrNilForKey:kGetTransportsTransportType fromDictionary:dict];
            self.transportYear = [self objectOrNilForKey:kGetTransportsTransportYear fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.transportModel forKey:kGetTransportsTransportModel];
    [mutableDict setValue:self.transportMake forKey:kGetTransportsTransportMake];
    [mutableDict setValue:self.plateNumber forKey:kGetTransportsPlateNumber];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kGetTransportsId];
    [mutableDict setValue:self.transportColor forKey:kGetTransportsTransportColor];
    [mutableDict setValue:self.userId forKey:kGetTransportsUserId];
    [mutableDict setValue:self.transportType forKey:kGetTransportsTransportType];
    [mutableDict setValue:self.transportYear forKey:kGetTransportsTransportYear];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.transportModel = [aDecoder decodeObjectForKey:kGetTransportsTransportModel];
    self.transportMake = [aDecoder decodeObjectForKey:kGetTransportsTransportMake];
    self.plateNumber = [aDecoder decodeObjectForKey:kGetTransportsPlateNumber];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kGetTransportsId];
    self.transportColor = [aDecoder decodeObjectForKey:kGetTransportsTransportColor];
    self.userId = [aDecoder decodeObjectForKey:kGetTransportsUserId];
    self.transportType = [aDecoder decodeObjectForKey:kGetTransportsTransportType];
    self.transportYear = [aDecoder decodeObjectForKey:kGetTransportsTransportYear];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_transportModel forKey:kGetTransportsTransportModel];
    [aCoder encodeObject:_transportMake forKey:kGetTransportsTransportMake];
    [aCoder encodeObject:_plateNumber forKey:kGetTransportsPlateNumber];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kGetTransportsId];
    [aCoder encodeObject:_transportColor forKey:kGetTransportsTransportColor];
    [aCoder encodeObject:_userId forKey:kGetTransportsUserId];
    [aCoder encodeObject:_transportType forKey:kGetTransportsTransportType];
    [aCoder encodeObject:_transportYear forKey:kGetTransportsTransportYear];
}

- (id)copyWithZone:(NSZone *)zone {
    GetTransports *copy = [[GetTransports alloc] init];
    
    
    
    if (copy) {

        copy.transportModel = [self.transportModel copyWithZone:zone];
        copy.transportMake = [self.transportMake copyWithZone:zone];
        copy.plateNumber = [self.plateNumber copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.transportColor = [self.transportColor copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.transportType = [self.transportType copyWithZone:zone];
        copy.transportYear = [self.transportYear copyWithZone:zone];
    }
    
    return copy;
}


@end
