//
//  RateUser.m
//
//  Created by   on 2017/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "RateUser.h"


NSString *const kRateUserUserRate = @"user_rate";
NSString *const kRateUserSurname = @"surname";
NSString *const kRateUserUserId = @"user_id";
NSString *const kRateUserSelfImg = @"self_img";
NSString *const kRateUserCnt = @"cnt";
NSString *const kRateUserName = @"name";
NSString *const kRateUserTotal = @"total";


@interface RateUser ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation RateUser

@synthesize userRate = _userRate;
@synthesize surname = _surname;
@synthesize userId = _userId;
@synthesize selfImg = _selfImg;
@synthesize cnt = _cnt;
@synthesize name = _name;
@synthesize total = _total;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.userRate = [self objectOrNilForKey:kRateUserUserRate fromDictionary:dict];
            self.surname = [self objectOrNilForKey:kRateUserSurname fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kRateUserUserId fromDictionary:dict];
            self.selfImg = [self objectOrNilForKey:kRateUserSelfImg fromDictionary:dict];
            self.cnt = [self objectOrNilForKey:kRateUserCnt fromDictionary:dict];
            self.name = [self objectOrNilForKey:kRateUserName fromDictionary:dict];
            self.total = [self objectOrNilForKey:kRateUserTotal fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.userRate forKey:kRateUserUserRate];
    [mutableDict setValue:self.surname forKey:kRateUserSurname];
    [mutableDict setValue:self.userId forKey:kRateUserUserId];
    [mutableDict setValue:self.selfImg forKey:kRateUserSelfImg];
    [mutableDict setValue:self.cnt forKey:kRateUserCnt];
    [mutableDict setValue:self.name forKey:kRateUserName];
    [mutableDict setValue:self.total forKey:kRateUserTotal];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.userRate = [aDecoder decodeObjectForKey:kRateUserUserRate];
    self.surname = [aDecoder decodeObjectForKey:kRateUserSurname];
    self.userId = [aDecoder decodeObjectForKey:kRateUserUserId];
    self.selfImg = [aDecoder decodeObjectForKey:kRateUserSelfImg];
    self.cnt = [aDecoder decodeObjectForKey:kRateUserCnt];
    self.name = [aDecoder decodeObjectForKey:kRateUserName];
    self.total = [aDecoder decodeObjectForKey:kRateUserTotal];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_userRate forKey:kRateUserUserRate];
    [aCoder encodeObject:_surname forKey:kRateUserSurname];
    [aCoder encodeObject:_userId forKey:kRateUserUserId];
    [aCoder encodeObject:_selfImg forKey:kRateUserSelfImg];
    [aCoder encodeObject:_cnt forKey:kRateUserCnt];
    [aCoder encodeObject:_name forKey:kRateUserName];
    [aCoder encodeObject:_total forKey:kRateUserTotal];
}

- (id)copyWithZone:(NSZone *)zone {
    RateUser *copy = [[RateUser alloc] init];
    
    
    
    if (copy) {

        copy.userRate = [self.userRate copyWithZone:zone];
        copy.surname = [self.surname copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.selfImg = [self.selfImg copyWithZone:zone];
        copy.cnt = [self.cnt copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.total = [self.total copyWithZone:zone];
    }
    
    return copy;
}


@end
