//
//  Data.m
//
//  Created by   on 2017/06/22
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "CourierListData.h"


NSString *const kDataPackFromText = @"pack_from_text";
NSString *const kDataPetTypeText = @"pet_type_text";
NSString *const kDataCourierEmail = @"courier_email";
NSString *const kDataPackOwnerEmail = @"pack_owner_email";
NSString *const kDataPackCourierProfit = @"courier_profit";
NSString *const kDataPackOwnerSelfImage = @"pack_owner_self_img";
NSString *const kDataPetCount = @"pet_count";
NSString *const kDataDeliveryBringTypeText = @"delivery_bring_type_text";
NSString *const kDataId = @"id";
NSString *const kDataCourierPaid = @"courier_paid";
NSString *const kDataUpdatedBy = @"updated_by";
NSString *const kDataReceiverPhoneNumber = @"receiver_phone_number";
NSString *const kDataSendName = @"send_name";
NSString *const kDataPackOptionsText = @"pack_options_text";
NSString *const kDataDeliveryTakeType = @"delivery_take_type";
NSString *const kDataPackToText = @"pack_to_text";
NSString *const kDataStatusSender = @"status_sender";
NSString *const kDataCCLastLng = @"c_last_lng";
NSString *const kDataCCLastLat = @"c_last_lat";
NSString *const kDataAcceptStatus = @"accept_status";
NSString *const kDataAcceptedCourierCell = @"accepted_courier_cell";
NSString *const kDataDistanceTime = @"distance_time";
NSString *const kDataAcceptedDate = @"accepted_date";
NSString *const kDataItemValue = @"item_value";
NSString *const kDataAcceptedCourierName = @"accepted_courier_name";
NSString *const kDataInsertedBy = @"inserted_by";
NSString *const kDataImgTypeName = @"img_type_name";
NSString *const kDataTakenAsapText = @"taken_asap_text";
NSString *const kDataPackId = @"pack_id";
NSString *const kDataSendCatText = @"send_cat_text";
NSString *const kDataPackStatus = @"pack_status";
NSString *const kDataSendCatId = @"send_cat_id";
NSString *const kDataTakenBringTypeText = @"taken_bring_type_text";
NSString *const kDataPackToLng = @"pack_to_lng";
NSString *const kDataBidText = @"bid_text";
NSString *const kDataTakenTypeText = @"taken_type_text";
NSString *const kDataPackOwnerCell = @"pack_owner_cell";
NSString *const kDataDeliveryAsap = @"delivery_asap";
NSString *const kDataPackImg = @"pack_img";
NSString *const kDataCourierName = @"courier_name";
NSString *const kDataPetIsVac = @"pet_is_vac";
NSString *const kDataPackPrice = @"pack_price";
NSString *const kDataPackOwnerName = @"pack_owner_name";
NSString *const kDataPetIsVacText = @"pet_is_vac_text";
NSString *const kDataImgId = @"img_id";
NSString *const kDataStatusCourier = @"status_courier";
NSString *const kDataPackSizeText = @"pack_size_text";
NSString *const kDataLeaveByDoor = @"leave_by_door";
NSString *const kDataPackOwnerSurname = @"pack_owner_surname";
NSString *const kDataReasonId = @"reason_id";
NSString *const kDataPackFromLat = @"pack_from_lat";
NSString *const kDataPetType = @"pet_type";
NSString *const kDataDistance = @"distance";
NSString *const kDataDeliveryAsapText = @"delivery_asap_text";
NSString *const kDataReasonType = @"reason_type";
NSString *const kDataTakenType = @"taken_type";
NSString *const kDataLeaveByDoorText = @"leave_by_door_text";
NSString *const kDataReasonTemplateText = @"reason_template_text";
NSString *const kDataCourierCell = @"courier_cell";
NSString *const kDataAcceptedCourierSurname = @"accepted_courier_surname";
NSString *const kDataTakenBringType = @"taken_bring_type";
NSString *const kDataDeliveryTime = @"delivery_time";
NSString *const kDataInsuranceMode = @"insurance_mode";
NSString *const kDataBidderId = @"bidder_id";
NSString *const kDataBidDate = @"bid_date";
NSString *const kDataReasonText = @"reason_text";
NSString *const kDataDeliveryTypeText = @"delivery_type_text";
NSString *const kDataTakenTime = @"taken_time";
NSString *const kDataAcceptedCourierId = @"accepted_courier_id";
NSString *const kDataAcceptStatusText = @"accept_status_text";
NSString *const kDataIsInsurancePayed = @"is_insurance_payed";
NSString *const kDataReceiverName = @"receiver_name";
NSString *const kDataPackFromLng = @"pack_from_lng";
NSString *const kDataAcceptedCourierEmail = @"accepted_courier_email";
NSString *const kDataPackOwnerId = @"pack_owner_id";
NSString *const kDataPackOptionsId = @"pack_options_id";
NSString *const kDataPackToLat = @"pack_to_lat";
NSString *const kDataUpdatedDate = @"updated_date";
NSString *const kDataInsertedDate = @"inserted_date";
NSString *const kDataCourierSurname = @"courier_surname";
NSString *const kDataDescText = @"desc_text";
NSString *const kDataPackWeight = @"pack_weight";
NSString *const kDataDeliveryType = @"delivery_type";
NSString *const kDataInsurancePrice = @"insurance_price";
NSString *const kDataTakenAsap = @"taken_asap";
NSString *const kDataSenderPaid = @"sender_paid";
NSString *const kDataPackSizeId = @"pack_size_id";
NSString *const kPackDetailsModelLastOperationTimeC = @"last_operation_time";
NSString *const kDataTariffText = @"tariff_name";

@interface CourierListData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CourierListData

@synthesize packFromText = _packFromText;
@synthesize petTypeText = _petTypeText;
@synthesize courierEmail = _courierEmail;
@synthesize packOwnerEmail = _packOwnerEmail;
@synthesize packOwnerSelfImage = _packOwnerSelfImage;
@synthesize courierProfit = _courierProfit;
@synthesize petCount = _petCount;
@synthesize deliveryBringTypeText = _deliveryBringTypeText;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize courierPaid = _courierPaid;
@synthesize updatedBy = _updatedBy;
@synthesize receiverPhoneNumber = _receiverPhoneNumber;
@synthesize sendName = _sendName;
@synthesize packOptionsText = _packOptionsText;
@synthesize deliveryTakeType = _deliveryTakeType;
@synthesize packToText = _packToText;
@synthesize statusSender = _statusSender;
@synthesize acceptStatus = _acceptStatus;
@synthesize acceptedCourierCell = _acceptedCourierCell;
@synthesize distanceTime = _distanceTime;
@synthesize acceptedDate = _acceptedDate;
@synthesize itemValue = _itemValue;
@synthesize ccLastLng = _ccLastLng;
@synthesize ccLastLat = _ccLastLat;
@synthesize acceptedCourierName = _acceptedCourierName;
@synthesize insertedBy = _insertedBy;
@synthesize imgTypeName = _imgTypeName;
@synthesize takenAsapText = _takenAsapText;
@synthesize packId = _packId;
@synthesize sendCatText = _sendCatText;
@synthesize packStatus = _packStatus;
@synthesize sendCatId = _sendCatId;
@synthesize takenBringTypeText = _takenBringTypeText;
@synthesize packToLng = _packToLng;
@synthesize bidText = _bidText;
@synthesize takenTypeText = _takenTypeText;
@synthesize packOwnerCell = _packOwnerCell;
@synthesize deliveryAsap = _deliveryAsap;
@synthesize packImg = _packImg;
@synthesize courierName = _courierName;
@synthesize petIsVac = _petIsVac;
@synthesize packPrice = _packPrice;
@synthesize packOwnerName = _packOwnerName;
@synthesize petIsVacText = _petIsVacText;
@synthesize imgId = _imgId;
@synthesize statusCourier = _statusCourier;
@synthesize packSizeText = _packSizeText;
@synthesize leaveByDoor = _leaveByDoor;
@synthesize packOwnerSurname = _packOwnerSurname;
@synthesize reasonId = _reasonId;
@synthesize packFromLat = _packFromLat;
@synthesize petType = _petType;
@synthesize distance = _distance;
@synthesize deliveryAsapText = _deliveryAsapText;
@synthesize reasonType = _reasonType;
@synthesize takenType = _takenType;
@synthesize leaveByDoorText = _leaveByDoorText;
@synthesize reasonTemplateText = _reasonTemplateText;
@synthesize courierCell = _courierCell;
@synthesize acceptedCourierSurname = _acceptedCourierSurname;
@synthesize takenBringType = _takenBringType;
@synthesize deliveryTime = _deliveryTime;
@synthesize insuranceMode = _insuranceMode;
@synthesize bidderId = _bidderId;
@synthesize bidDate = _bidDate;
@synthesize reasonText = _reasonText;
@synthesize deliveryTypeText = _deliveryTypeText;
@synthesize takenTime = _takenTime;
@synthesize acceptedCourierId = _acceptedCourierId;
@synthesize acceptStatusText = _acceptStatusText;
@synthesize isInsurancePayed = _isInsurancePayed;
@synthesize receiverName = _receiverName;
@synthesize packFromLng = _packFromLng;
@synthesize acceptedCourierEmail = _acceptedCourierEmail;
@synthesize packOwnerId = _packOwnerId;
@synthesize packOptionsId = _packOptionsId;
@synthesize packToLat = _packToLat;
@synthesize updatedDate = _updatedDate;
@synthesize insertedDate = _insertedDate;
@synthesize courierSurname = _courierSurname;
@synthesize descText = _descText;
@synthesize packWeight = _packWeight;
@synthesize deliveryType = _deliveryType;
@synthesize insurancePrice = _insurancePrice;
@synthesize takenAsap = _takenAsap;
@synthesize senderPaid = _senderPaid;
@synthesize packSizeId = _packSizeId;
@synthesize lastOperationTimeC = _lastOperationTimeC;
@synthesize tariffText = _tariffText;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.packFromText = [self objectOrNilForKey:kDataPackFromText fromDictionary:dict];
            self.petTypeText = [self objectOrNilForKey:kDataPetTypeText fromDictionary:dict];
            self.courierEmail = [self objectOrNilForKey:kDataCourierEmail fromDictionary:dict];
            self.packOwnerEmail = [self objectOrNilForKey:kDataPackOwnerEmail fromDictionary:dict];
        self.courierProfit = [self objectOrNilForKey:kDataPackCourierProfit fromDictionary:dict];

        self.packOwnerSelfImage = [self objectOrNilForKey:kDataPackOwnerSelfImage fromDictionary:dict];
            self.petCount = [self objectOrNilForKey:kDataPetCount fromDictionary:dict];
            self.deliveryBringTypeText = [self objectOrNilForKey:kDataDeliveryBringTypeText fromDictionary:dict];
            self.dataIdentifier = [self objectOrNilForKey:kDataId fromDictionary:dict];
            self.courierPaid = [self objectOrNilForKey:kDataCourierPaid fromDictionary:dict];
            self.updatedBy = [self objectOrNilForKey:kDataUpdatedBy fromDictionary:dict];
            self.receiverPhoneNumber = [self objectOrNilForKey:kDataReceiverPhoneNumber fromDictionary:dict];
            self.sendName = [self objectOrNilForKey:kDataSendName fromDictionary:dict];
            self.packOptionsText = [self objectOrNilForKey:kDataPackOptionsText fromDictionary:dict];
            self.deliveryTakeType = [self objectOrNilForKey:kDataDeliveryTakeType fromDictionary:dict];
        
        self.ccLastLat = [self objectOrNilForKey:kDataCCLastLat fromDictionary:dict];
        self.ccLastLng = [self objectOrNilForKey:kDataCCLastLng fromDictionary:dict];

            self.packToText = [self objectOrNilForKey:kDataPackToText fromDictionary:dict];
            self.statusSender = [self objectOrNilForKey:kDataStatusSender fromDictionary:dict];
            self.acceptStatus = [self objectOrNilForKey:kDataAcceptStatus fromDictionary:dict];
            self.acceptedCourierCell = [self objectOrNilForKey:kDataAcceptedCourierCell fromDictionary:dict];
            self.distanceTime = [self objectOrNilForKey:kDataDistanceTime fromDictionary:dict];
            self.acceptedDate = [self objectOrNilForKey:kDataAcceptedDate fromDictionary:dict];
            self.itemValue = [self objectOrNilForKey:kDataItemValue fromDictionary:dict];
            self.acceptedCourierName = [self objectOrNilForKey:kDataAcceptedCourierName fromDictionary:dict];
            self.insertedBy = [self objectOrNilForKey:kDataInsertedBy fromDictionary:dict];
            self.imgTypeName = [self objectOrNilForKey:kDataImgTypeName fromDictionary:dict];
            self.takenAsapText = [self objectOrNilForKey:kDataTakenAsapText fromDictionary:dict];
            self.packId = [self objectOrNilForKey:kDataPackId fromDictionary:dict];
            self.sendCatText = [self objectOrNilForKey:kDataSendCatText fromDictionary:dict];
            self.packStatus = [self objectOrNilForKey:kDataPackStatus fromDictionary:dict];
            self.sendCatId = [self objectOrNilForKey:kDataSendCatId fromDictionary:dict];
            self.takenBringTypeText = [self objectOrNilForKey:kDataTakenBringTypeText fromDictionary:dict];
            self.packToLng = [self objectOrNilForKey:kDataPackToLng fromDictionary:dict];
            self.bidText = [self objectOrNilForKey:kDataBidText fromDictionary:dict];
            self.takenTypeText = [self objectOrNilForKey:kDataTakenTypeText fromDictionary:dict];
            self.packOwnerCell = [self objectOrNilForKey:kDataPackOwnerCell fromDictionary:dict];
            self.deliveryAsap = [self objectOrNilForKey:kDataDeliveryAsap fromDictionary:dict];
            self.packImg = [self objectOrNilForKey:kDataPackImg fromDictionary:dict];
            self.courierName = [self objectOrNilForKey:kDataCourierName fromDictionary:dict];
            self.petIsVac = [self objectOrNilForKey:kDataPetIsVac fromDictionary:dict];
            self.packPrice = [self objectOrNilForKey:kDataPackPrice fromDictionary:dict];
            self.packOwnerName = [self objectOrNilForKey:kDataPackOwnerName fromDictionary:dict];
            self.petIsVacText = [self objectOrNilForKey:kDataPetIsVacText fromDictionary:dict];
            self.imgId = [self objectOrNilForKey:kDataImgId fromDictionary:dict];
            self.statusCourier = [self objectOrNilForKey:kDataStatusCourier fromDictionary:dict];
            self.packSizeText = [self objectOrNilForKey:kDataPackSizeText fromDictionary:dict];
            self.leaveByDoor = [self objectOrNilForKey:kDataLeaveByDoor fromDictionary:dict];
            self.packOwnerSurname = [self objectOrNilForKey:kDataPackOwnerSurname fromDictionary:dict];
            self.reasonId = [self objectOrNilForKey:kDataReasonId fromDictionary:dict];
            self.packFromLat = [self objectOrNilForKey:kDataPackFromLat fromDictionary:dict];
            self.petType = [self objectOrNilForKey:kDataPetType fromDictionary:dict];
            self.distance = [self objectOrNilForKey:kDataDistance fromDictionary:dict];
            self.deliveryAsapText = [self objectOrNilForKey:kDataDeliveryAsapText fromDictionary:dict];
            self.reasonType = [self objectOrNilForKey:kDataReasonType fromDictionary:dict];
            self.takenType = [self objectOrNilForKey:kDataTakenType fromDictionary:dict];
            self.leaveByDoorText = [self objectOrNilForKey:kDataLeaveByDoorText fromDictionary:dict];
            self.reasonTemplateText = [self objectOrNilForKey:kDataReasonTemplateText fromDictionary:dict];
            self.courierCell = [self objectOrNilForKey:kDataCourierCell fromDictionary:dict];
            self.acceptedCourierSurname = [self objectOrNilForKey:kDataAcceptedCourierSurname fromDictionary:dict];
            self.takenBringType = [self objectOrNilForKey:kDataTakenBringType fromDictionary:dict];
            self.deliveryTime = [self objectOrNilForKey:kDataDeliveryTime fromDictionary:dict];
            self.insuranceMode = [self objectOrNilForKey:kDataInsuranceMode fromDictionary:dict];
            self.bidderId = [self objectOrNilForKey:kDataBidderId fromDictionary:dict];
            self.bidDate = [self objectOrNilForKey:kDataBidDate fromDictionary:dict];
            self.reasonText = [self objectOrNilForKey:kDataReasonText fromDictionary:dict];
            self.deliveryTypeText = [self objectOrNilForKey:kDataDeliveryTypeText fromDictionary:dict];
            self.takenTime = [self objectOrNilForKey:kDataTakenTime fromDictionary:dict];
            self.acceptedCourierId = [self objectOrNilForKey:kDataAcceptedCourierId fromDictionary:dict];
            self.acceptStatusText = [self objectOrNilForKey:kDataAcceptStatusText fromDictionary:dict];
            self.isInsurancePayed = [self objectOrNilForKey:kDataIsInsurancePayed fromDictionary:dict];
            self.receiverName = [self objectOrNilForKey:kDataReceiverName fromDictionary:dict];
            self.packFromLng = [self objectOrNilForKey:kDataPackFromLng fromDictionary:dict];
            self.acceptedCourierEmail = [self objectOrNilForKey:kDataAcceptedCourierEmail fromDictionary:dict];
            self.packOwnerId = [self objectOrNilForKey:kDataPackOwnerId fromDictionary:dict];
            self.packOptionsId = [self objectOrNilForKey:kDataPackOptionsId fromDictionary:dict];
            self.packToLat = [self objectOrNilForKey:kDataPackToLat fromDictionary:dict];
            self.updatedDate = [self objectOrNilForKey:kDataUpdatedDate fromDictionary:dict];
            self.insertedDate = [self objectOrNilForKey:kDataInsertedDate fromDictionary:dict];
            self.courierSurname = [self objectOrNilForKey:kDataCourierSurname fromDictionary:dict];
            self.descText = [self objectOrNilForKey:kDataDescText fromDictionary:dict];
            self.packWeight = [self objectOrNilForKey:kDataPackWeight fromDictionary:dict];
            self.deliveryType = [self objectOrNilForKey:kDataDeliveryType fromDictionary:dict];
            self.insurancePrice = [self objectOrNilForKey:kDataInsurancePrice fromDictionary:dict];
            self.takenAsap = [self objectOrNilForKey:kDataTakenAsap fromDictionary:dict];
            self.senderPaid = [self objectOrNilForKey:kDataSenderPaid fromDictionary:dict];
            self.packSizeId = [self objectOrNilForKey:kDataPackSizeId fromDictionary:dict];
           self.lastOperationTimeC = [self objectOrNilForKey:kPackDetailsModelLastOperationTimeC
                                          fromDictionary:dict];
        self.tariffText = [self objectOrNilForKey:kDataTariffText
                                           fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.packFromText forKey:kDataPackFromText];
    [mutableDict setValue:self.petTypeText forKey:kDataPetTypeText];
    [mutableDict setValue:self.courierEmail forKey:kDataCourierEmail];
    [mutableDict setValue:self.packOwnerEmail forKey:kDataPackOwnerEmail];
    [mutableDict setValue:self.courierProfit forKey:kDataPackCourierProfit];
    [mutableDict setValue:self.packOwnerSelfImage forKey:kDataPackOwnerSelfImage];
    [mutableDict setValue:self.petCount forKey:kDataPetCount];
    [mutableDict setValue:self.deliveryBringTypeText forKey:kDataDeliveryBringTypeText];
    [mutableDict setValue:self.dataIdentifier forKey:kDataId];
    [mutableDict setValue:self.courierPaid forKey:kDataCourierPaid];
    [mutableDict setValue:self.updatedBy forKey:kDataUpdatedBy];
    [mutableDict setValue:self.receiverPhoneNumber forKey:kDataReceiverPhoneNumber];
    [mutableDict setValue:self.sendName forKey:kDataSendName];
    
    [mutableDict setValue:self.ccLastLat forKey:kDataCCLastLat];
    [mutableDict setValue:self.ccLastLng forKey:kDataCCLastLng];

    [mutableDict setValue:self.packOptionsText forKey:kDataPackOptionsText];
    [mutableDict setValue:self.deliveryTakeType forKey:kDataDeliveryTakeType];
    [mutableDict setValue:self.packToText forKey:kDataPackToText];
    [mutableDict setValue:self.statusSender forKey:kDataStatusSender];
    [mutableDict setValue:self.acceptStatus forKey:kDataAcceptStatus];
    [mutableDict setValue:self.acceptedCourierCell forKey:kDataAcceptedCourierCell];
    [mutableDict setValue:self.distanceTime forKey:kDataDistanceTime];
    [mutableDict setValue:self.acceptedDate forKey:kDataAcceptedDate];
    [mutableDict setValue:self.itemValue forKey:kDataItemValue];
    [mutableDict setValue:self.acceptedCourierName forKey:kDataAcceptedCourierName];
    [mutableDict setValue:self.insertedBy forKey:kDataInsertedBy];
    [mutableDict setValue:self.imgTypeName forKey:kDataImgTypeName];
    [mutableDict setValue:self.takenAsapText forKey:kDataTakenAsapText];
    [mutableDict setValue:self.packId forKey:kDataPackId];
    [mutableDict setValue:self.sendCatText forKey:kDataSendCatText];
    [mutableDict setValue:self.packStatus forKey:kDataPackStatus];
    [mutableDict setValue:self.sendCatId forKey:kDataSendCatId];
    [mutableDict setValue:self.takenBringTypeText forKey:kDataTakenBringTypeText];
    [mutableDict setValue:self.packToLng forKey:kDataPackToLng];
    [mutableDict setValue:self.bidText forKey:kDataBidText];
    [mutableDict setValue:self.takenTypeText forKey:kDataTakenTypeText];
    [mutableDict setValue:self.packOwnerCell forKey:kDataPackOwnerCell];
    [mutableDict setValue:self.deliveryAsap forKey:kDataDeliveryAsap];
    [mutableDict setValue:self.packImg forKey:kDataPackImg];
    [mutableDict setValue:self.courierName forKey:kDataCourierName];
    [mutableDict setValue:self.petIsVac forKey:kDataPetIsVac];
    [mutableDict setValue:self.packPrice forKey:kDataPackPrice];
    [mutableDict setValue:self.packOwnerName forKey:kDataPackOwnerName];
    [mutableDict setValue:self.petIsVacText forKey:kDataPetIsVacText];
    [mutableDict setValue:self.imgId forKey:kDataImgId];
    [mutableDict setValue:self.statusCourier forKey:kDataStatusCourier];
    [mutableDict setValue:self.packSizeText forKey:kDataPackSizeText];
    [mutableDict setValue:self.leaveByDoor forKey:kDataLeaveByDoor];
    [mutableDict setValue:self.packOwnerSurname forKey:kDataPackOwnerSurname];
    [mutableDict setValue:self.reasonId forKey:kDataReasonId];
    [mutableDict setValue:self.packFromLat forKey:kDataPackFromLat];
    [mutableDict setValue:self.petType forKey:kDataPetType];
    [mutableDict setValue:self.distance forKey:kDataDistance];
    [mutableDict setValue:self.deliveryAsapText forKey:kDataDeliveryAsapText];
    [mutableDict setValue:self.reasonType forKey:kDataReasonType];
    [mutableDict setValue:self.takenType forKey:kDataTakenType];
    [mutableDict setValue:self.leaveByDoorText forKey:kDataLeaveByDoorText];
    [mutableDict setValue:self.reasonTemplateText forKey:kDataReasonTemplateText];
    [mutableDict setValue:self.courierCell forKey:kDataCourierCell];
    [mutableDict setValue:self.acceptedCourierSurname forKey:kDataAcceptedCourierSurname];
    [mutableDict setValue:self.takenBringType forKey:kDataTakenBringType];
    [mutableDict setValue:self.deliveryTime forKey:kDataDeliveryTime];
    [mutableDict setValue:self.insuranceMode forKey:kDataInsuranceMode];
    [mutableDict setValue:self.bidderId forKey:kDataBidderId];
    [mutableDict setValue:self.bidDate forKey:kDataBidDate];
    [mutableDict setValue:self.reasonText forKey:kDataReasonText];
    [mutableDict setValue:self.deliveryTypeText forKey:kDataDeliveryTypeText];
    [mutableDict setValue:self.takenTime forKey:kDataTakenTime];
    [mutableDict setValue:self.acceptedCourierId forKey:kDataAcceptedCourierId];
    [mutableDict setValue:self.acceptStatusText forKey:kDataAcceptStatusText];
    [mutableDict setValue:self.isInsurancePayed forKey:kDataIsInsurancePayed];
    [mutableDict setValue:self.receiverName forKey:kDataReceiverName];
    [mutableDict setValue:self.packFromLng forKey:kDataPackFromLng];
    [mutableDict setValue:self.acceptedCourierEmail forKey:kDataAcceptedCourierEmail];
    [mutableDict setValue:self.packOwnerId forKey:kDataPackOwnerId];
    [mutableDict setValue:self.packOptionsId forKey:kDataPackOptionsId];
    [mutableDict setValue:self.packToLat forKey:kDataPackToLat];
    [mutableDict setValue:self.updatedDate forKey:kDataUpdatedDate];
    [mutableDict setValue:self.insertedDate forKey:kDataInsertedDate];
    [mutableDict setValue:self.courierSurname forKey:kDataCourierSurname];
    [mutableDict setValue:self.descText forKey:kDataDescText];
    [mutableDict setValue:self.packWeight forKey:kDataPackWeight];
    [mutableDict setValue:self.deliveryType forKey:kDataDeliveryType];
    [mutableDict setValue:self.insurancePrice forKey:kDataInsurancePrice];
    [mutableDict setValue:self.takenAsap forKey:kDataTakenAsap];
    [mutableDict setValue:self.senderPaid forKey:kDataSenderPaid];
    [mutableDict setValue:self.packSizeId forKey:kDataPackSizeId];
    [mutableDict setValue:self.lastOperationTimeC forKey:kPackDetailsModelLastOperationTimeC];
    [mutableDict setValue:self.tariffText forKey:kDataTariffText];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.packFromText = [aDecoder decodeObjectForKey:kDataPackFromText];
    self.petTypeText = [aDecoder decodeObjectForKey:kDataPetTypeText];
    self.courierEmail = [aDecoder decodeObjectForKey:kDataCourierEmail];
    self.packOwnerEmail = [aDecoder decodeObjectForKey:kDataPackOwnerEmail];
    self.courierProfit = [aDecoder decodeObjectForKey:kDataPackCourierProfit];
    self.packOwnerSelfImage = [aDecoder decodeObjectForKey:kDataPackOwnerSelfImage];
    self.petCount = [aDecoder decodeObjectForKey:kDataPetCount];
    self.deliveryBringTypeText = [aDecoder decodeObjectForKey:kDataDeliveryBringTypeText];
    self.dataIdentifier = [aDecoder decodeObjectForKey:kDataId];
    self.courierPaid = [aDecoder decodeObjectForKey:kDataCourierPaid];
    self.updatedBy = [aDecoder decodeObjectForKey:kDataUpdatedBy];
    self.receiverPhoneNumber = [aDecoder decodeObjectForKey:kDataReceiverPhoneNumber];
    self.sendName = [aDecoder decodeObjectForKey:kDataSendName];
    self.packOptionsText = [aDecoder decodeObjectForKey:kDataPackOptionsText];
    self.deliveryTakeType = [aDecoder decodeObjectForKey:kDataDeliveryTakeType];
    self.packToText = [aDecoder decodeObjectForKey:kDataPackToText];
    
    self.ccLastLat = [aDecoder decodeObjectForKey:kDataCCLastLat];
    self.ccLastLng = [aDecoder decodeObjectForKey:kDataCCLastLng];

    self.statusSender = [aDecoder decodeObjectForKey:kDataStatusSender];
    self.acceptStatus = [aDecoder decodeObjectForKey:kDataAcceptStatus];
    self.acceptedCourierCell = [aDecoder decodeObjectForKey:kDataAcceptedCourierCell];
    self.distanceTime = [aDecoder decodeObjectForKey:kDataDistanceTime];
    self.acceptedDate = [aDecoder decodeObjectForKey:kDataAcceptedDate];
    self.itemValue = [aDecoder decodeObjectForKey:kDataItemValue];
    self.acceptedCourierName = [aDecoder decodeObjectForKey:kDataAcceptedCourierName];
    self.insertedBy = [aDecoder decodeObjectForKey:kDataInsertedBy];
    self.imgTypeName = [aDecoder decodeObjectForKey:kDataImgTypeName];
    self.takenAsapText = [aDecoder decodeObjectForKey:kDataTakenAsapText];
    self.packId = [aDecoder decodeObjectForKey:kDataPackId];
    self.sendCatText = [aDecoder decodeObjectForKey:kDataSendCatText];
    self.packStatus = [aDecoder decodeObjectForKey:kDataPackStatus];
    self.sendCatId = [aDecoder decodeObjectForKey:kDataSendCatId];
    self.takenBringTypeText = [aDecoder decodeObjectForKey:kDataTakenBringTypeText];
    self.packToLng = [aDecoder decodeObjectForKey:kDataPackToLng];
    self.bidText = [aDecoder decodeObjectForKey:kDataBidText];
    self.takenTypeText = [aDecoder decodeObjectForKey:kDataTakenTypeText];
    self.packOwnerCell = [aDecoder decodeObjectForKey:kDataPackOwnerCell];
    self.deliveryAsap = [aDecoder decodeObjectForKey:kDataDeliveryAsap];
    self.packImg = [aDecoder decodeObjectForKey:kDataPackImg];
    self.courierName = [aDecoder decodeObjectForKey:kDataCourierName];
    self.petIsVac = [aDecoder decodeObjectForKey:kDataPetIsVac];
    self.packPrice = [aDecoder decodeObjectForKey:kDataPackPrice];
    self.packOwnerName = [aDecoder decodeObjectForKey:kDataPackOwnerName];
    self.petIsVacText = [aDecoder decodeObjectForKey:kDataPetIsVacText];
    self.imgId = [aDecoder decodeObjectForKey:kDataImgId];
    self.statusCourier = [aDecoder decodeObjectForKey:kDataStatusCourier];
    self.packSizeText = [aDecoder decodeObjectForKey:kDataPackSizeText];
    self.leaveByDoor = [aDecoder decodeObjectForKey:kDataLeaveByDoor];
    self.packOwnerSurname = [aDecoder decodeObjectForKey:kDataPackOwnerSurname];
    self.reasonId = [aDecoder decodeObjectForKey:kDataReasonId];
    self.packFromLat = [aDecoder decodeObjectForKey:kDataPackFromLat];
    self.petType = [aDecoder decodeObjectForKey:kDataPetType];
    self.distance = [aDecoder decodeObjectForKey:kDataDistance];
    self.deliveryAsapText = [aDecoder decodeObjectForKey:kDataDeliveryAsapText];
    self.reasonType = [aDecoder decodeObjectForKey:kDataReasonType];
    self.takenType = [aDecoder decodeObjectForKey:kDataTakenType];
    self.leaveByDoorText = [aDecoder decodeObjectForKey:kDataLeaveByDoorText];
    self.reasonTemplateText = [aDecoder decodeObjectForKey:kDataReasonTemplateText];
    self.courierCell = [aDecoder decodeObjectForKey:kDataCourierCell];
    self.acceptedCourierSurname = [aDecoder decodeObjectForKey:kDataAcceptedCourierSurname];
    self.takenBringType = [aDecoder decodeObjectForKey:kDataTakenBringType];
    self.deliveryTime = [aDecoder decodeObjectForKey:kDataDeliveryTime];
    self.insuranceMode = [aDecoder decodeObjectForKey:kDataInsuranceMode];
    self.bidderId = [aDecoder decodeObjectForKey:kDataBidderId];
    self.bidDate = [aDecoder decodeObjectForKey:kDataBidDate];
    self.reasonText = [aDecoder decodeObjectForKey:kDataReasonText];
    self.deliveryTypeText = [aDecoder decodeObjectForKey:kDataDeliveryTypeText];
    self.takenTime = [aDecoder decodeObjectForKey:kDataTakenTime];
    self.acceptedCourierId = [aDecoder decodeObjectForKey:kDataAcceptedCourierId];
    self.acceptStatusText = [aDecoder decodeObjectForKey:kDataAcceptStatusText];
    self.isInsurancePayed = [aDecoder decodeObjectForKey:kDataIsInsurancePayed];
    self.receiverName = [aDecoder decodeObjectForKey:kDataReceiverName];
    self.packFromLng = [aDecoder decodeObjectForKey:kDataPackFromLng];
    self.acceptedCourierEmail = [aDecoder decodeObjectForKey:kDataAcceptedCourierEmail];
    self.packOwnerId = [aDecoder decodeObjectForKey:kDataPackOwnerId];
    self.packOptionsId = [aDecoder decodeObjectForKey:kDataPackOptionsId];
    self.packToLat = [aDecoder decodeObjectForKey:kDataPackToLat];
    self.updatedDate = [aDecoder decodeObjectForKey:kDataUpdatedDate];
    self.insertedDate = [aDecoder decodeObjectForKey:kDataInsertedDate];
    self.courierSurname = [aDecoder decodeObjectForKey:kDataCourierSurname];
    self.descText = [aDecoder decodeObjectForKey:kDataDescText];
    self.packWeight = [aDecoder decodeObjectForKey:kDataPackWeight];
    self.deliveryType = [aDecoder decodeObjectForKey:kDataDeliveryType];
    self.insurancePrice = [aDecoder decodeObjectForKey:kDataInsurancePrice];
    self.takenAsap = [aDecoder decodeObjectForKey:kDataTakenAsap];
    self.senderPaid = [aDecoder decodeObjectForKey:kDataSenderPaid];
    self.packSizeId = [aDecoder decodeObjectForKey:kDataPackSizeId];
    self.lastOperationTimeC = [aDecoder decodeObjectForKey:kPackDetailsModelLastOperationTimeC];
    self.tariffText = [aDecoder decodeObjectForKey:kDataTariffText];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_packFromText forKey:kDataPackFromText];
    [aCoder encodeObject:_petTypeText forKey:kDataPetTypeText];
    [aCoder encodeObject:_courierEmail forKey:kDataCourierEmail];
    [aCoder encodeObject:_packOwnerEmail forKey:kDataPackOwnerEmail];
    [aCoder encodeObject:_courierProfit forKey:kDataPackCourierProfit];
    [aCoder encodeObject:_packOwnerSelfImage forKey:kDataPackOwnerSelfImage];
    [aCoder encodeObject:_petCount forKey:kDataPetCount];
    [aCoder encodeObject:_deliveryBringTypeText forKey:kDataDeliveryBringTypeText];
    [aCoder encodeObject:_dataIdentifier forKey:kDataId];
    [aCoder encodeObject:_courierPaid forKey:kDataCourierPaid];
    [aCoder encodeObject:_updatedBy forKey:kDataUpdatedBy];
    [aCoder encodeObject:_receiverPhoneNumber forKey:kDataReceiverPhoneNumber];
    [aCoder encodeObject:_sendName forKey:kDataSendName];
    [aCoder encodeObject:_packOptionsText forKey:kDataPackOptionsText];
    [aCoder encodeObject:_deliveryTakeType forKey:kDataDeliveryTakeType];
    [aCoder encodeObject:_packToText forKey:kDataPackToText];
    [aCoder encodeObject:_statusSender forKey:kDataStatusSender];
    [aCoder encodeObject:_acceptStatus forKey:kDataAcceptStatus];
    [aCoder encodeObject:_acceptedCourierCell forKey:kDataAcceptedCourierCell];
    [aCoder encodeObject:_distanceTime forKey:kDataDistanceTime];
    [aCoder encodeObject:_acceptedDate forKey:kDataAcceptedDate];
    [aCoder encodeObject:_itemValue forKey:kDataItemValue];
    [aCoder encodeObject:_acceptedCourierName forKey:kDataAcceptedCourierName];
    [aCoder encodeObject:_insertedBy forKey:kDataInsertedBy];
    
    [aCoder encodeObject:_ccLastLat forKey:kDataCCLastLat];
    [aCoder encodeObject:_ccLastLng forKey:kDataCCLastLng];

    [aCoder encodeObject:_imgTypeName forKey:kDataImgTypeName];
    [aCoder encodeObject:_takenAsapText forKey:kDataTakenAsapText];
    [aCoder encodeObject:_packId forKey:kDataPackId];
    [aCoder encodeObject:_sendCatText forKey:kDataSendCatText];
    [aCoder encodeObject:_packStatus forKey:kDataPackStatus];
    [aCoder encodeObject:_sendCatId forKey:kDataSendCatId];
    [aCoder encodeObject:_takenBringTypeText forKey:kDataTakenBringTypeText];
    [aCoder encodeObject:_packToLng forKey:kDataPackToLng];
    [aCoder encodeObject:_bidText forKey:kDataBidText];
    [aCoder encodeObject:_takenTypeText forKey:kDataTakenTypeText];
    [aCoder encodeObject:_packOwnerCell forKey:kDataPackOwnerCell];
    [aCoder encodeObject:_deliveryAsap forKey:kDataDeliveryAsap];
    [aCoder encodeObject:_packImg forKey:kDataPackImg];
    [aCoder encodeObject:_courierName forKey:kDataCourierName];
    [aCoder encodeObject:_petIsVac forKey:kDataPetIsVac];
    [aCoder encodeObject:_packPrice forKey:kDataPackPrice];
    [aCoder encodeObject:_packOwnerName forKey:kDataPackOwnerName];
    [aCoder encodeObject:_petIsVacText forKey:kDataPetIsVacText];
    [aCoder encodeObject:_imgId forKey:kDataImgId];
    [aCoder encodeObject:_statusCourier forKey:kDataStatusCourier];
    [aCoder encodeObject:_packSizeText forKey:kDataPackSizeText];
    [aCoder encodeObject:_leaveByDoor forKey:kDataLeaveByDoor];
    [aCoder encodeObject:_packOwnerSurname forKey:kDataPackOwnerSurname];
    [aCoder encodeObject:_reasonId forKey:kDataReasonId];
    [aCoder encodeObject:_packFromLat forKey:kDataPackFromLat];
    [aCoder encodeObject:_petType forKey:kDataPetType];
    [aCoder encodeObject:_distance forKey:kDataDistance];
    [aCoder encodeObject:_deliveryAsapText forKey:kDataDeliveryAsapText];
    [aCoder encodeObject:_reasonType forKey:kDataReasonType];
    [aCoder encodeObject:_takenType forKey:kDataTakenType];
    [aCoder encodeObject:_leaveByDoorText forKey:kDataLeaveByDoorText];
    [aCoder encodeObject:_reasonTemplateText forKey:kDataReasonTemplateText];
    [aCoder encodeObject:_courierCell forKey:kDataCourierCell];
    [aCoder encodeObject:_acceptedCourierSurname forKey:kDataAcceptedCourierSurname];
    [aCoder encodeObject:_takenBringType forKey:kDataTakenBringType];
    [aCoder encodeObject:_deliveryTime forKey:kDataDeliveryTime];
    [aCoder encodeObject:_insuranceMode forKey:kDataInsuranceMode];
    [aCoder encodeObject:_bidderId forKey:kDataBidderId];
    [aCoder encodeObject:_bidDate forKey:kDataBidDate];
    [aCoder encodeObject:_reasonText forKey:kDataReasonText];
    [aCoder encodeObject:_deliveryTypeText forKey:kDataDeliveryTypeText];
    [aCoder encodeObject:_takenTime forKey:kDataTakenTime];
    [aCoder encodeObject:_acceptedCourierId forKey:kDataAcceptedCourierId];
    [aCoder encodeObject:_acceptStatusText forKey:kDataAcceptStatusText];
    [aCoder encodeObject:_isInsurancePayed forKey:kDataIsInsurancePayed];
    [aCoder encodeObject:_receiverName forKey:kDataReceiverName];
    [aCoder encodeObject:_packFromLng forKey:kDataPackFromLng];
    [aCoder encodeObject:_acceptedCourierEmail forKey:kDataAcceptedCourierEmail];
    [aCoder encodeObject:_packOwnerId forKey:kDataPackOwnerId];
    [aCoder encodeObject:_packOptionsId forKey:kDataPackOptionsId];
    [aCoder encodeObject:_packToLat forKey:kDataPackToLat];
    [aCoder encodeObject:_updatedDate forKey:kDataUpdatedDate];
    [aCoder encodeObject:_insertedDate forKey:kDataInsertedDate];
    [aCoder encodeObject:_courierSurname forKey:kDataCourierSurname];
    [aCoder encodeObject:_descText forKey:kDataDescText];
    [aCoder encodeObject:_packWeight forKey:kDataPackWeight];
    [aCoder encodeObject:_deliveryType forKey:kDataDeliveryType];
    [aCoder encodeObject:_insurancePrice forKey:kDataInsurancePrice];
    [aCoder encodeObject:_takenAsap forKey:kDataTakenAsap];
    [aCoder encodeObject:_senderPaid forKey:kDataSenderPaid];
    [aCoder encodeObject:_packSizeId forKey:kDataPackSizeId];
    [aCoder encodeObject:_lastOperationTimeC forKey:kPackDetailsModelLastOperationTimeC];
    [aCoder encodeObject:_tariffText forKey:kDataTariffText];
}

- (id)copyWithZone:(NSZone *)zone {
    CourierListData *copy = [[CourierListData alloc] init];
    
    
    
    if (copy) {

        copy.packFromText = [self.packFromText copyWithZone:zone];
        copy.petTypeText = [self.petTypeText copyWithZone:zone];
        copy.courierEmail = [self.courierEmail copyWithZone:zone];
        copy.packOwnerEmail = [self.packOwnerEmail copyWithZone:zone];
        copy.courierProfit = [self.courierProfit copyWithZone:zone];
        copy.packOwnerSelfImage = [self.packOwnerSelfImage copyWithZone:zone];
        copy.petCount = [self.petCount copyWithZone:zone];
        copy.deliveryBringTypeText = [self.deliveryBringTypeText copyWithZone:zone];
        copy.dataIdentifier = [self.dataIdentifier copyWithZone:zone];
        copy.courierPaid = [self.courierPaid copyWithZone:zone];
        copy.updatedBy = [self.updatedBy copyWithZone:zone];
        copy.receiverPhoneNumber = [self.receiverPhoneNumber copyWithZone:zone];
        copy.sendName = [self.sendName copyWithZone:zone];
        
        copy.ccLastLat = [self.ccLastLat copyWithZone:zone];
        copy.ccLastLng = [self.ccLastLng copyWithZone:zone];

        copy.packOptionsText = [self.packOptionsText copyWithZone:zone];
        copy.deliveryTakeType = [self.deliveryTakeType copyWithZone:zone];
        copy.packToText = [self.packToText copyWithZone:zone];
        copy.statusSender = [self.statusSender copyWithZone:zone];
        copy.acceptStatus = [self.acceptStatus copyWithZone:zone];
        copy.acceptedCourierCell = [self.acceptedCourierCell copyWithZone:zone];
        copy.distanceTime = [self.distanceTime copyWithZone:zone];
        copy.acceptedDate = [self.acceptedDate copyWithZone:zone];
        copy.itemValue = [self.itemValue copyWithZone:zone];
        copy.acceptedCourierName = [self.acceptedCourierName copyWithZone:zone];
        copy.insertedBy = [self.insertedBy copyWithZone:zone];
        copy.imgTypeName = [self.imgTypeName copyWithZone:zone];
        copy.takenAsapText = [self.takenAsapText copyWithZone:zone];
        copy.packId = [self.packId copyWithZone:zone];
        copy.sendCatText = [self.sendCatText copyWithZone:zone];
        copy.packStatus = [self.packStatus copyWithZone:zone];
        copy.sendCatId = [self.sendCatId copyWithZone:zone];
        copy.takenBringTypeText = [self.takenBringTypeText copyWithZone:zone];
        copy.packToLng = [self.packToLng copyWithZone:zone];
        copy.bidText = [self.bidText copyWithZone:zone];
        copy.takenTypeText = [self.takenTypeText copyWithZone:zone];
        copy.packOwnerCell = [self.packOwnerCell copyWithZone:zone];
        copy.deliveryAsap = [self.deliveryAsap copyWithZone:zone];
        copy.packImg = [self.packImg copyWithZone:zone];
        copy.courierName = [self.courierName copyWithZone:zone];
        copy.petIsVac = [self.petIsVac copyWithZone:zone];
        copy.packPrice = [self.packPrice copyWithZone:zone];
        copy.packOwnerName = [self.packOwnerName copyWithZone:zone];
        copy.petIsVacText = [self.petIsVacText copyWithZone:zone];
        copy.imgId = [self.imgId copyWithZone:zone];
        copy.statusCourier = [self.statusCourier copyWithZone:zone];
        copy.packSizeText = [self.packSizeText copyWithZone:zone];
        copy.leaveByDoor = [self.leaveByDoor copyWithZone:zone];
        copy.packOwnerSurname = [self.packOwnerSurname copyWithZone:zone];
        copy.reasonId = [self.reasonId copyWithZone:zone];
        copy.packFromLat = [self.packFromLat copyWithZone:zone];
        copy.petType = [self.petType copyWithZone:zone];
        copy.distance = [self.distance copyWithZone:zone];
        copy.deliveryAsapText = [self.deliveryAsapText copyWithZone:zone];
        copy.reasonType = [self.reasonType copyWithZone:zone];
        copy.takenType = [self.takenType copyWithZone:zone];
        copy.leaveByDoorText = [self.leaveByDoorText copyWithZone:zone];
        copy.reasonTemplateText = [self.reasonTemplateText copyWithZone:zone];
        copy.courierCell = [self.courierCell copyWithZone:zone];
        copy.acceptedCourierSurname = [self.acceptedCourierSurname copyWithZone:zone];
        copy.takenBringType = [self.takenBringType copyWithZone:zone];
        copy.deliveryTime = [self.deliveryTime copyWithZone:zone];
        copy.insuranceMode = [self.insuranceMode copyWithZone:zone];
        copy.bidderId = [self.bidderId copyWithZone:zone];
        copy.bidDate = [self.bidDate copyWithZone:zone];
        copy.reasonText = [self.reasonText copyWithZone:zone];
        copy.deliveryTypeText = [self.deliveryTypeText copyWithZone:zone];
        copy.takenTime = [self.takenTime copyWithZone:zone];
        copy.acceptedCourierId = [self.acceptedCourierId copyWithZone:zone];
        copy.acceptStatusText = [self.acceptStatusText copyWithZone:zone];
        copy.isInsurancePayed = [self.isInsurancePayed copyWithZone:zone];
        copy.receiverName = [self.receiverName copyWithZone:zone];
        copy.packFromLng = [self.packFromLng copyWithZone:zone];
        copy.acceptedCourierEmail = [self.acceptedCourierEmail copyWithZone:zone];
        copy.packOwnerId = [self.packOwnerId copyWithZone:zone];
        copy.packOptionsId = [self.packOptionsId copyWithZone:zone];
        copy.packToLat = [self.packToLat copyWithZone:zone];
        copy.updatedDate = [self.updatedDate copyWithZone:zone];
        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.courierSurname = [self.courierSurname copyWithZone:zone];
        copy.descText = [self.descText copyWithZone:zone];
        copy.packWeight = [self.packWeight copyWithZone:zone];
        copy.deliveryType = [self.deliveryType copyWithZone:zone];
        copy.insurancePrice = [self.insurancePrice copyWithZone:zone];
        copy.takenAsap = [self.takenAsap copyWithZone:zone];
        copy.senderPaid = [self.senderPaid copyWithZone:zone];
        copy.packSizeId = [self.packSizeId copyWithZone:zone];
        copy.lastOperationTimeC = [self.lastOperationTimeC copyWithZone:zone];
        copy.tariffText = [self.tariffText copyWithZone:zone];
    }
    
    return copy;
}


@end
