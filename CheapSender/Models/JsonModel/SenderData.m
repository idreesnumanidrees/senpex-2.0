//
//  Data.m
//
//  Created by   on 2017/06/19
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "SenderData.h"


NSString *const ksDataPackFromText = @"pack_from_text";
NSString *const ksDataPetTypeText = @"pet_type_text";
NSString *const ksDataCourierEmail = @"courier_email";
NSString *const ksDataPackOwnerEmail = @"pack_owner_email";
NSString *const ksDataPetCount = @"pet_count";
NSString *const ksDataDeliveryBringTypeText = @"delivery_bring_type_text";
NSString *const ksDataId = @"id";
NSString *const ksDataCourierPaid = @"courier_paid";
NSString *const ksDataUpdatedBy = @"updated_by";
NSString *const ksDataReceiverPhoneNumber = @"receiver_phone_number";
NSString *const ksDataSendName = @"send_name";
NSString *const ksDataPackOptionsText = @"pack_options_text";
NSString *const ksDataDeliveryTakeType = @"delivery_take_type";
NSString *const ksDataPackToText = @"pack_to_text";
NSString *const ksDataStatusSender = @"status_sender";
NSString *const ksDataAcceptStatus = @"accept_status";
NSString *const ksDataAcceptedCourierCell = @"accepted_courier_cell";
NSString *const ksDataDistanceTime = @"distance_time";
NSString *const ksDataAcceptedDate = @"accepted_date";
NSString *const ksDataItemValue = @"item_value";
NSString *const ksDataAcceptedCourierName = @"accepted_courier_name";
NSString *const ksDataInsertedBy = @"inserted_by";
NSString *const ksDataImgTypeName = @"img_type_name";
NSString *const ksDataTakenAsapText = @"taken_asap_text";
NSString *const ksDataPackId = @"pack_id";
NSString *const ksDataSendCatText = @"send_cat_text";
NSString *const ksDataPackStatus = @"pack_status";
NSString *const ksDataSendCatId = @"send_cat_id";
NSString *const ksDataTakenBringTypeText = @"taken_bring_type_text";
NSString *const ksDataPackToLng = @"pack_to_lng";
NSString *const ksDataBidText = @"bid_text";
NSString *const ksDataTakenTypeText = @"taken_type_text";
NSString *const ksDataPackOwnerCell = @"pack_owner_cell";
NSString *const ksDataDeliveryAsap = @"delivery_asap";
NSString *const ksDataPackImg = @"pack_img";
NSString *const ksDataCourierName = @"courier_name";
NSString *const ksDataPetIsVac = @"pet_is_vac";
NSString *const ksDataPackPrice = @"pack_price";
NSString *const ksDataPackOwnerName = @"pack_owner_name";
NSString *const ksDataPetIsVacText = @"pet_is_vac_text";
NSString *const ksDataImgId = @"img_id";
NSString *const ksDataStatusCourier = @"status_courier";
NSString *const ksDataPackSizeText = @"pack_size_text";
NSString *const ksDataLeaveByDoor = @"leave_by_door";
NSString *const ksDataPackOwnerSurname = @"pack_owner_surname";
NSString *const ksDataReasonId = @"reason_id";
NSString *const ksDataPackFromLat = @"pack_from_lat";
NSString *const ksDataPetType = @"pet_type";
NSString *const ksDataDistance = @"distance";
NSString *const ksDataDeliveryAsapText = @"delivery_asap_text";
NSString *const ksDataReasonType = @"reason_type";
NSString *const ksDataTakenType = @"taken_type";
NSString *const ksDataLeaveByDoorText = @"leave_by_door_text";
NSString *const ksDataReasonTemplateText = @"reason_template_text";
NSString *const ksDataCourierCell = @"courier_cell";
NSString *const ksDataAcceptedCourierSurname = @"accepted_courier_surname";
NSString *const ksDataTakenBringType = @"taken_bring_type";
NSString *const ksDataDeliveryTime = @"delivery_time";
NSString *const ksDataInsuranceMode = @"insurance_mode";
NSString *const ksDataBidderId = @"bidder_id";
NSString *const ksDataBidDate = @"bid_date";
NSString *const ksDataReasonText = @"reason_text";
NSString *const ksDataDeliveryTypeText = @"delivery_type_text";
NSString *const ksDataTakenTime = @"taken_time";
NSString *const ksDataAcceptedCourierId = @"accepted_courier_id";
NSString *const ksDataAcceptStatusText = @"accept_status_text";
NSString *const ksDataIsInsurancePayed = @"is_insurance_payed";
NSString *const ksDataReceiverName = @"receiver_name";
NSString *const ksDataPackFromLng = @"pack_from_lng";
NSString *const ksDataAcceptedCourierEmail = @"accepted_courier_email";
NSString *const ksDataPackOwnerId = @"pack_owner_id";
NSString *const ksDataPackOptionsId = @"pack_options_id";
NSString *const ksDataPackToLat = @"pack_to_lat";
NSString *const ksDataUpdatedDate = @"updated_date";
NSString *const ksDataInsertedDate = @"inserted_date";
NSString *const ksDataCourierSurname = @"courier_surname";
NSString *const ksDataDescText = @"desc_text";
NSString *const ksDataPackWeight = @"pack_weight";
NSString *const ksDataDeliveryType = @"delivery_type";
NSString *const ksDataInsurancePrice = @"insurance_price";
NSString *const ksDataTakenAsap = @"taken_asap";
NSString *const ksDataSenderPaid = @"sender_paid";
NSString *const ksDataPackSizeId = @"pack_size_id";


@interface SenderData ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation SenderData

@synthesize packFromText = _packFromText;
@synthesize petTypeText = _petTypeText;
@synthesize courierEmail = _courierEmail;
@synthesize packOwnerEmail = _packOwnerEmail;
@synthesize petCount = _petCount;
@synthesize deliveryBringTypeText = _deliveryBringTypeText;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize courierPaid = _courierPaid;
@synthesize updatedBy = _updatedBy;
@synthesize receiverPhoneNumber = _receiverPhoneNumber;
@synthesize sendName = _sendName;
@synthesize packOptionsText = _packOptionsText;
@synthesize deliveryTakeType = _deliveryTakeType;
@synthesize packToText = _packToText;
@synthesize statusSender = _statusSender;
@synthesize acceptStatus = _acceptStatus;
@synthesize acceptedCourierCell = _acceptedCourierCell;
@synthesize distanceTime = _distanceTime;
@synthesize acceptedDate = _acceptedDate;
@synthesize itemValue = _itemValue;
@synthesize acceptedCourierName = _acceptedCourierName;
@synthesize insertedBy = _insertedBy;
@synthesize imgTypeName = _imgTypeName;
@synthesize takenAsapText = _takenAsapText;
@synthesize packId = _packId;
@synthesize sendCatText = _sendCatText;
@synthesize packStatus = _packStatus;
@synthesize sendCatId = _sendCatId;
@synthesize takenBringTypeText = _takenBringTypeText;
@synthesize packToLng = _packToLng;
@synthesize bidText = _bidText;
@synthesize takenTypeText = _takenTypeText;
@synthesize packOwnerCell = _packOwnerCell;
@synthesize deliveryAsap = _deliveryAsap;
@synthesize packImg = _packImg;
@synthesize courierName = _courierName;
@synthesize petIsVac = _petIsVac;
@synthesize packPrice = _packPrice;
@synthesize packOwnerName = _packOwnerName;
@synthesize petIsVacText = _petIsVacText;
@synthesize imgId = _imgId;
@synthesize statusCourier = _statusCourier;
@synthesize packSizeText = _packSizeText;
@synthesize leaveByDoor = _leaveByDoor;
@synthesize packOwnerSurname = _packOwnerSurname;
@synthesize reasonId = _reasonId;
@synthesize packFromLat = _packFromLat;
@synthesize petType = _petType;
@synthesize distance = _distance;
@synthesize deliveryAsapText = _deliveryAsapText;
@synthesize reasonType = _reasonType;
@synthesize takenType = _takenType;
@synthesize leaveByDoorText = _leaveByDoorText;
@synthesize reasonTemplateText = _reasonTemplateText;
@synthesize courierCell = _courierCell;
@synthesize acceptedCourierSurname = _acceptedCourierSurname;
@synthesize takenBringType = _takenBringType;
@synthesize deliveryTime = _deliveryTime;
@synthesize insuranceMode = _insuranceMode;
@synthesize bidderId = _bidderId;
@synthesize bidDate = _bidDate;
@synthesize reasonText = _reasonText;
@synthesize deliveryTypeText = _deliveryTypeText;
@synthesize takenTime = _takenTime;
@synthesize acceptedCourierId = _acceptedCourierId;
@synthesize acceptStatusText = _acceptStatusText;
@synthesize isInsurancePayed = _isInsurancePayed;
@synthesize receiverName = _receiverName;
@synthesize packFromLng = _packFromLng;
@synthesize acceptedCourierEmail = _acceptedCourierEmail;
@synthesize packOwnerId = _packOwnerId;
@synthesize packOptionsId = _packOptionsId;
@synthesize packToLat = _packToLat;
@synthesize updatedDate = _updatedDate;
@synthesize insertedDate = _insertedDate;
@synthesize courierSurname = _courierSurname;
@synthesize descText = _descText;
@synthesize packWeight = _packWeight;
@synthesize deliveryType = _deliveryType;
@synthesize insurancePrice = _insurancePrice;
@synthesize takenAsap = _takenAsap;
@synthesize senderPaid = _senderPaid;
@synthesize packSizeId = _packSizeId;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.packFromText = [self objectOrNilForKey:ksDataPackFromText fromDictionary:dict];
            self.petTypeText = [self objectOrNilForKey:ksDataPetTypeText fromDictionary:dict];
            self.courierEmail = [self objectOrNilForKey:ksDataCourierEmail fromDictionary:dict];
            self.packOwnerEmail = [self objectOrNilForKey:ksDataPackOwnerEmail fromDictionary:dict];
            self.petCount = [self objectOrNilForKey:ksDataPetCount fromDictionary:dict];
            self.deliveryBringTypeText = [self objectOrNilForKey:ksDataDeliveryBringTypeText fromDictionary:dict];
            self.dataIdentifier = [self objectOrNilForKey:ksDataId fromDictionary:dict];
            self.courierPaid = [self objectOrNilForKey:ksDataCourierPaid fromDictionary:dict];
            self.updatedBy = [self objectOrNilForKey:ksDataUpdatedBy fromDictionary:dict];
            self.receiverPhoneNumber = [self objectOrNilForKey:ksDataReceiverPhoneNumber fromDictionary:dict];
            self.sendName = [self objectOrNilForKey:ksDataSendName fromDictionary:dict];
            self.packOptionsText = [self objectOrNilForKey:ksDataPackOptionsText fromDictionary:dict];
            self.deliveryTakeType = [self objectOrNilForKey:ksDataDeliveryTakeType fromDictionary:dict];
            self.packToText = [self objectOrNilForKey:ksDataPackToText fromDictionary:dict];
            self.statusSender = [self objectOrNilForKey:ksDataStatusSender fromDictionary:dict];
            self.acceptStatus = [self objectOrNilForKey:ksDataAcceptStatus fromDictionary:dict];
            self.acceptedCourierCell = [self objectOrNilForKey:ksDataAcceptedCourierCell fromDictionary:dict];
            self.distanceTime = [self objectOrNilForKey:ksDataDistanceTime fromDictionary:dict];
            self.acceptedDate = [self objectOrNilForKey:ksDataAcceptedDate fromDictionary:dict];
            self.itemValue = [self objectOrNilForKey:ksDataItemValue fromDictionary:dict];
            self.acceptedCourierName = [self objectOrNilForKey:ksDataAcceptedCourierName fromDictionary:dict];
            self.insertedBy = [self objectOrNilForKey:ksDataInsertedBy fromDictionary:dict];
            self.imgTypeName = [self objectOrNilForKey:ksDataImgTypeName fromDictionary:dict];
            self.takenAsapText = [self objectOrNilForKey:ksDataTakenAsapText fromDictionary:dict];
            self.packId = [self objectOrNilForKey:ksDataPackId fromDictionary:dict];
            self.sendCatText = [self objectOrNilForKey:ksDataSendCatText fromDictionary:dict];
            self.packStatus = [self objectOrNilForKey:ksDataPackStatus fromDictionary:dict];
            self.sendCatId = [self objectOrNilForKey:ksDataSendCatId fromDictionary:dict];
            self.takenBringTypeText = [self objectOrNilForKey:ksDataTakenBringTypeText fromDictionary:dict];
            self.packToLng = [self objectOrNilForKey:ksDataPackToLng fromDictionary:dict];
            self.bidText = [self objectOrNilForKey:ksDataBidText fromDictionary:dict];
            self.takenTypeText = [self objectOrNilForKey:ksDataTakenTypeText fromDictionary:dict];
            self.packOwnerCell = [self objectOrNilForKey:ksDataPackOwnerCell fromDictionary:dict];
            self.deliveryAsap = [self objectOrNilForKey:ksDataDeliveryAsap fromDictionary:dict];
            self.packImg = [self objectOrNilForKey:ksDataPackImg fromDictionary:dict];
            self.courierName = [self objectOrNilForKey:ksDataCourierName fromDictionary:dict];
            self.petIsVac = [self objectOrNilForKey:ksDataPetIsVac fromDictionary:dict];
            self.packPrice = [self objectOrNilForKey:ksDataPackPrice fromDictionary:dict];
            self.packOwnerName = [self objectOrNilForKey:ksDataPackOwnerName fromDictionary:dict];
            self.petIsVacText = [self objectOrNilForKey:ksDataPetIsVacText fromDictionary:dict];
            self.imgId = [self objectOrNilForKey:ksDataImgId fromDictionary:dict];
            self.statusCourier = [self objectOrNilForKey:ksDataStatusCourier fromDictionary:dict];
            self.packSizeText = [self objectOrNilForKey:ksDataPackSizeText fromDictionary:dict];
            self.leaveByDoor = [self objectOrNilForKey:ksDataLeaveByDoor fromDictionary:dict];
            self.packOwnerSurname = [self objectOrNilForKey:ksDataPackOwnerSurname fromDictionary:dict];
            self.reasonId = [self objectOrNilForKey:ksDataReasonId fromDictionary:dict];
            self.packFromLat = [self objectOrNilForKey:ksDataPackFromLat fromDictionary:dict];
            self.petType = [self objectOrNilForKey:ksDataPetType fromDictionary:dict];
            self.distance = [self objectOrNilForKey:ksDataDistance fromDictionary:dict];
            self.deliveryAsapText = [self objectOrNilForKey:ksDataDeliveryAsapText fromDictionary:dict];
            self.reasonType = [self objectOrNilForKey:ksDataReasonType fromDictionary:dict];
            self.takenType = [self objectOrNilForKey:ksDataTakenType fromDictionary:dict];
            self.leaveByDoorText = [self objectOrNilForKey:ksDataLeaveByDoorText fromDictionary:dict];
            self.reasonTemplateText = [self objectOrNilForKey:ksDataReasonTemplateText fromDictionary:dict];
            self.courierCell = [self objectOrNilForKey:ksDataCourierCell fromDictionary:dict];
            self.acceptedCourierSurname = [self objectOrNilForKey:ksDataAcceptedCourierSurname fromDictionary:dict];
            self.takenBringType = [self objectOrNilForKey:ksDataTakenBringType fromDictionary:dict];
            self.deliveryTime = [self objectOrNilForKey:ksDataDeliveryTime fromDictionary:dict];
            self.insuranceMode = [self objectOrNilForKey:ksDataInsuranceMode fromDictionary:dict];
            self.bidderId = [self objectOrNilForKey:ksDataBidderId fromDictionary:dict];
            self.bidDate = [self objectOrNilForKey:ksDataBidDate fromDictionary:dict];
            self.reasonText = [self objectOrNilForKey:ksDataReasonText fromDictionary:dict];
            self.deliveryTypeText = [self objectOrNilForKey:ksDataDeliveryTypeText fromDictionary:dict];
            self.takenTime = [self objectOrNilForKey:ksDataTakenTime fromDictionary:dict];
            self.acceptedCourierId = [self objectOrNilForKey:ksDataAcceptedCourierId fromDictionary:dict];
            self.acceptStatusText = [self objectOrNilForKey:ksDataAcceptStatusText fromDictionary:dict];
            self.isInsurancePayed = [self objectOrNilForKey:ksDataIsInsurancePayed fromDictionary:dict];
            self.receiverName = [self objectOrNilForKey:ksDataReceiverName fromDictionary:dict];
            self.packFromLng = [self objectOrNilForKey:ksDataPackFromLng fromDictionary:dict];
            self.acceptedCourierEmail = [self objectOrNilForKey:ksDataAcceptedCourierEmail fromDictionary:dict];
            self.packOwnerId = [self objectOrNilForKey:ksDataPackOwnerId fromDictionary:dict];
            self.packOptionsId = [self objectOrNilForKey:ksDataPackOptionsId fromDictionary:dict];
            self.packToLat = [self objectOrNilForKey:ksDataPackToLat fromDictionary:dict];
            self.updatedDate = [self objectOrNilForKey:ksDataUpdatedDate fromDictionary:dict];
            self.insertedDate = [self objectOrNilForKey:ksDataInsertedDate fromDictionary:dict];
            self.courierSurname = [self objectOrNilForKey:ksDataCourierSurname fromDictionary:dict];
            self.descText = [self objectOrNilForKey:ksDataDescText fromDictionary:dict];
            self.packWeight = [self objectOrNilForKey:ksDataPackWeight fromDictionary:dict];
            self.deliveryType = [self objectOrNilForKey:ksDataDeliveryType fromDictionary:dict];
            self.insurancePrice = [self objectOrNilForKey:ksDataInsurancePrice fromDictionary:dict];
            self.takenAsap = [self objectOrNilForKey:ksDataTakenAsap fromDictionary:dict];
            self.senderPaid = [self objectOrNilForKey:ksDataSenderPaid fromDictionary:dict];
            self.packSizeId = [self objectOrNilForKey:ksDataPackSizeId fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.packFromText forKey:ksDataPackFromText];
    [mutableDict setValue:self.petTypeText forKey:ksDataPetTypeText];
    [mutableDict setValue:self.courierEmail forKey:ksDataCourierEmail];
    [mutableDict setValue:self.packOwnerEmail forKey:ksDataPackOwnerEmail];
    [mutableDict setValue:self.petCount forKey:ksDataPetCount];
    [mutableDict setValue:self.deliveryBringTypeText forKey:ksDataDeliveryBringTypeText];
    [mutableDict setValue:self.dataIdentifier forKey:ksDataId];
    [mutableDict setValue:self.courierPaid forKey:ksDataCourierPaid];
    [mutableDict setValue:self.updatedBy forKey:ksDataUpdatedBy];
    [mutableDict setValue:self.receiverPhoneNumber forKey:ksDataReceiverPhoneNumber];
    [mutableDict setValue:self.sendName forKey:ksDataSendName];
    [mutableDict setValue:self.packOptionsText forKey:ksDataPackOptionsText];
    [mutableDict setValue:self.deliveryTakeType forKey:ksDataDeliveryTakeType];
    [mutableDict setValue:self.packToText forKey:ksDataPackToText];
    [mutableDict setValue:self.statusSender forKey:ksDataStatusSender];
    [mutableDict setValue:self.acceptStatus forKey:ksDataAcceptStatus];
    [mutableDict setValue:self.acceptedCourierCell forKey:ksDataAcceptedCourierCell];
    [mutableDict setValue:self.distanceTime forKey:ksDataDistanceTime];
    [mutableDict setValue:self.acceptedDate forKey:ksDataAcceptedDate];
    [mutableDict setValue:self.itemValue forKey:ksDataItemValue];
    [mutableDict setValue:self.acceptedCourierName forKey:ksDataAcceptedCourierName];
    [mutableDict setValue:self.insertedBy forKey:ksDataInsertedBy];
    [mutableDict setValue:self.imgTypeName forKey:ksDataImgTypeName];
    [mutableDict setValue:self.takenAsapText forKey:ksDataTakenAsapText];
    [mutableDict setValue:self.packId forKey:ksDataPackId];
    [mutableDict setValue:self.sendCatText forKey:ksDataSendCatText];
    [mutableDict setValue:self.packStatus forKey:ksDataPackStatus];
    [mutableDict setValue:self.sendCatId forKey:ksDataSendCatId];
    [mutableDict setValue:self.takenBringTypeText forKey:ksDataTakenBringTypeText];
    [mutableDict setValue:self.packToLng forKey:ksDataPackToLng];
    [mutableDict setValue:self.bidText forKey:ksDataBidText];
    [mutableDict setValue:self.takenTypeText forKey:ksDataTakenTypeText];
    [mutableDict setValue:self.packOwnerCell forKey:ksDataPackOwnerCell];
    [mutableDict setValue:self.deliveryAsap forKey:ksDataDeliveryAsap];
    [mutableDict setValue:self.packImg forKey:ksDataPackImg];
    [mutableDict setValue:self.courierName forKey:ksDataCourierName];
    [mutableDict setValue:self.petIsVac forKey:ksDataPetIsVac];
    [mutableDict setValue:self.packPrice forKey:ksDataPackPrice];
    [mutableDict setValue:self.packOwnerName forKey:ksDataPackOwnerName];
    [mutableDict setValue:self.petIsVacText forKey:ksDataPetIsVacText];
    [mutableDict setValue:self.imgId forKey:ksDataImgId];
    [mutableDict setValue:self.statusCourier forKey:ksDataStatusCourier];
    [mutableDict setValue:self.packSizeText forKey:ksDataPackSizeText];
    [mutableDict setValue:self.leaveByDoor forKey:ksDataLeaveByDoor];
    [mutableDict setValue:self.packOwnerSurname forKey:ksDataPackOwnerSurname];
    [mutableDict setValue:self.reasonId forKey:ksDataReasonId];
    [mutableDict setValue:self.packFromLat forKey:ksDataPackFromLat];
    [mutableDict setValue:self.petType forKey:ksDataPetType];
    [mutableDict setValue:self.distance forKey:ksDataDistance];
    [mutableDict setValue:self.deliveryAsapText forKey:ksDataDeliveryAsapText];
    [mutableDict setValue:self.reasonType forKey:ksDataReasonType];
    [mutableDict setValue:self.takenType forKey:ksDataTakenType];
    [mutableDict setValue:self.leaveByDoorText forKey:ksDataLeaveByDoorText];
    [mutableDict setValue:self.reasonTemplateText forKey:ksDataReasonTemplateText];
    [mutableDict setValue:self.courierCell forKey:ksDataCourierCell];
    [mutableDict setValue:self.acceptedCourierSurname forKey:ksDataAcceptedCourierSurname];
    [mutableDict setValue:self.takenBringType forKey:ksDataTakenBringType];
    [mutableDict setValue:self.deliveryTime forKey:ksDataDeliveryTime];
    [mutableDict setValue:self.insuranceMode forKey:ksDataInsuranceMode];
    [mutableDict setValue:self.bidderId forKey:ksDataBidderId];
    [mutableDict setValue:self.bidDate forKey:ksDataBidDate];
    [mutableDict setValue:self.reasonText forKey:ksDataReasonText];
    [mutableDict setValue:self.deliveryTypeText forKey:ksDataDeliveryTypeText];
    [mutableDict setValue:self.takenTime forKey:ksDataTakenTime];
    [mutableDict setValue:self.acceptedCourierId forKey:ksDataAcceptedCourierId];
    [mutableDict setValue:self.acceptStatusText forKey:ksDataAcceptStatusText];
    [mutableDict setValue:self.isInsurancePayed forKey:ksDataIsInsurancePayed];
    [mutableDict setValue:self.receiverName forKey:ksDataReceiverName];
    [mutableDict setValue:self.packFromLng forKey:ksDataPackFromLng];
    [mutableDict setValue:self.acceptedCourierEmail forKey:ksDataAcceptedCourierEmail];
    [mutableDict setValue:self.packOwnerId forKey:ksDataPackOwnerId];
    [mutableDict setValue:self.packOptionsId forKey:ksDataPackOptionsId];
    [mutableDict setValue:self.packToLat forKey:ksDataPackToLat];
    [mutableDict setValue:self.updatedDate forKey:ksDataUpdatedDate];
    [mutableDict setValue:self.insertedDate forKey:ksDataInsertedDate];
    [mutableDict setValue:self.courierSurname forKey:ksDataCourierSurname];
    [mutableDict setValue:self.descText forKey:ksDataDescText];
    [mutableDict setValue:self.packWeight forKey:ksDataPackWeight];
    [mutableDict setValue:self.deliveryType forKey:ksDataDeliveryType];
    [mutableDict setValue:self.insurancePrice forKey:ksDataInsurancePrice];
    [mutableDict setValue:self.takenAsap forKey:ksDataTakenAsap];
    [mutableDict setValue:self.senderPaid forKey:ksDataSenderPaid];
    [mutableDict setValue:self.packSizeId forKey:ksDataPackSizeId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.packFromText = [aDecoder decodeObjectForKey:ksDataPackFromText];
    self.petTypeText = [aDecoder decodeObjectForKey:ksDataPetTypeText];
    self.courierEmail = [aDecoder decodeObjectForKey:ksDataCourierEmail];
    self.packOwnerEmail = [aDecoder decodeObjectForKey:ksDataPackOwnerEmail];
    self.petCount = [aDecoder decodeObjectForKey:ksDataPetCount];
    self.deliveryBringTypeText = [aDecoder decodeObjectForKey:ksDataDeliveryBringTypeText];
    self.dataIdentifier = [aDecoder decodeObjectForKey:ksDataId];
    self.courierPaid = [aDecoder decodeObjectForKey:ksDataCourierPaid];
    self.updatedBy = [aDecoder decodeObjectForKey:ksDataUpdatedBy];
    self.receiverPhoneNumber = [aDecoder decodeObjectForKey:ksDataReceiverPhoneNumber];
    self.sendName = [aDecoder decodeObjectForKey:ksDataSendName];
    self.packOptionsText = [aDecoder decodeObjectForKey:ksDataPackOptionsText];
    self.deliveryTakeType = [aDecoder decodeObjectForKey:ksDataDeliveryTakeType];
    self.packToText = [aDecoder decodeObjectForKey:ksDataPackToText];
    self.statusSender = [aDecoder decodeObjectForKey:ksDataStatusSender];
    self.acceptStatus = [aDecoder decodeObjectForKey:ksDataAcceptStatus];
    self.acceptedCourierCell = [aDecoder decodeObjectForKey:ksDataAcceptedCourierCell];
    self.distanceTime = [aDecoder decodeObjectForKey:ksDataDistanceTime];
    self.acceptedDate = [aDecoder decodeObjectForKey:ksDataAcceptedDate];
    self.itemValue = [aDecoder decodeObjectForKey:ksDataItemValue];
    self.acceptedCourierName = [aDecoder decodeObjectForKey:ksDataAcceptedCourierName];
    self.insertedBy = [aDecoder decodeObjectForKey:ksDataInsertedBy];
    self.imgTypeName = [aDecoder decodeObjectForKey:ksDataImgTypeName];
    self.takenAsapText = [aDecoder decodeObjectForKey:ksDataTakenAsapText];
    self.packId = [aDecoder decodeObjectForKey:ksDataPackId];
    self.sendCatText = [aDecoder decodeObjectForKey:ksDataSendCatText];
    self.packStatus = [aDecoder decodeObjectForKey:ksDataPackStatus];
    self.sendCatId = [aDecoder decodeObjectForKey:ksDataSendCatId];
    self.takenBringTypeText = [aDecoder decodeObjectForKey:ksDataTakenBringTypeText];
    self.packToLng = [aDecoder decodeObjectForKey:ksDataPackToLng];
    self.bidText = [aDecoder decodeObjectForKey:ksDataBidText];
    self.takenTypeText = [aDecoder decodeObjectForKey:ksDataTakenTypeText];
    self.packOwnerCell = [aDecoder decodeObjectForKey:ksDataPackOwnerCell];
    self.deliveryAsap = [aDecoder decodeObjectForKey:ksDataDeliveryAsap];
    self.packImg = [aDecoder decodeObjectForKey:ksDataPackImg];
    self.courierName = [aDecoder decodeObjectForKey:ksDataCourierName];
    self.petIsVac = [aDecoder decodeObjectForKey:ksDataPetIsVac];
    self.packPrice = [aDecoder decodeObjectForKey:ksDataPackPrice];
    self.packOwnerName = [aDecoder decodeObjectForKey:ksDataPackOwnerName];
    self.petIsVacText = [aDecoder decodeObjectForKey:ksDataPetIsVacText];
    self.imgId = [aDecoder decodeObjectForKey:ksDataImgId];
    self.statusCourier = [aDecoder decodeObjectForKey:ksDataStatusCourier];
    self.packSizeText = [aDecoder decodeObjectForKey:ksDataPackSizeText];
    self.leaveByDoor = [aDecoder decodeObjectForKey:ksDataLeaveByDoor];
    self.packOwnerSurname = [aDecoder decodeObjectForKey:ksDataPackOwnerSurname];
    self.reasonId = [aDecoder decodeObjectForKey:ksDataReasonId];
    self.packFromLat = [aDecoder decodeObjectForKey:ksDataPackFromLat];
    self.petType = [aDecoder decodeObjectForKey:ksDataPetType];
    self.distance = [aDecoder decodeObjectForKey:ksDataDistance];
    self.deliveryAsapText = [aDecoder decodeObjectForKey:ksDataDeliveryAsapText];
    self.reasonType = [aDecoder decodeObjectForKey:ksDataReasonType];
    self.takenType = [aDecoder decodeObjectForKey:ksDataTakenType];
    self.leaveByDoorText = [aDecoder decodeObjectForKey:ksDataLeaveByDoorText];
    self.reasonTemplateText = [aDecoder decodeObjectForKey:ksDataReasonTemplateText];
    self.courierCell = [aDecoder decodeObjectForKey:ksDataCourierCell];
    self.acceptedCourierSurname = [aDecoder decodeObjectForKey:ksDataAcceptedCourierSurname];
    self.takenBringType = [aDecoder decodeObjectForKey:ksDataTakenBringType];
    self.deliveryTime = [aDecoder decodeObjectForKey:ksDataDeliveryTime];
    self.insuranceMode = [aDecoder decodeObjectForKey:ksDataInsuranceMode];
    self.bidderId = [aDecoder decodeObjectForKey:ksDataBidderId];
    self.bidDate = [aDecoder decodeObjectForKey:ksDataBidDate];
    self.reasonText = [aDecoder decodeObjectForKey:ksDataReasonText];
    self.deliveryTypeText = [aDecoder decodeObjectForKey:ksDataDeliveryTypeText];
    self.takenTime = [aDecoder decodeObjectForKey:ksDataTakenTime];
    self.acceptedCourierId = [aDecoder decodeObjectForKey:ksDataAcceptedCourierId];
    self.acceptStatusText = [aDecoder decodeObjectForKey:ksDataAcceptStatusText];
    self.isInsurancePayed = [aDecoder decodeObjectForKey:ksDataIsInsurancePayed];
    self.receiverName = [aDecoder decodeObjectForKey:ksDataReceiverName];
    self.packFromLng = [aDecoder decodeObjectForKey:ksDataPackFromLng];
    self.acceptedCourierEmail = [aDecoder decodeObjectForKey:ksDataAcceptedCourierEmail];
    self.packOwnerId = [aDecoder decodeObjectForKey:ksDataPackOwnerId];
    self.packOptionsId = [aDecoder decodeObjectForKey:ksDataPackOptionsId];
    self.packToLat = [aDecoder decodeObjectForKey:ksDataPackToLat];
    self.updatedDate = [aDecoder decodeObjectForKey:ksDataUpdatedDate];
    self.insertedDate = [aDecoder decodeObjectForKey:ksDataInsertedDate];
    self.courierSurname = [aDecoder decodeObjectForKey:ksDataCourierSurname];
    self.descText = [aDecoder decodeObjectForKey:ksDataDescText];
    self.packWeight = [aDecoder decodeObjectForKey:ksDataPackWeight];
    self.deliveryType = [aDecoder decodeObjectForKey:ksDataDeliveryType];
    self.insurancePrice = [aDecoder decodeObjectForKey:ksDataInsurancePrice];
    self.takenAsap = [aDecoder decodeObjectForKey:ksDataTakenAsap];
    self.senderPaid = [aDecoder decodeObjectForKey:ksDataSenderPaid];
    self.packSizeId = [aDecoder decodeObjectForKey:ksDataPackSizeId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_packFromText forKey:ksDataPackFromText];
    [aCoder encodeObject:_petTypeText forKey:ksDataPetTypeText];
    [aCoder encodeObject:_courierEmail forKey:ksDataCourierEmail];
    [aCoder encodeObject:_packOwnerEmail forKey:ksDataPackOwnerEmail];
    [aCoder encodeObject:_petCount forKey:ksDataPetCount];
    [aCoder encodeObject:_deliveryBringTypeText forKey:ksDataDeliveryBringTypeText];
    [aCoder encodeObject:_dataIdentifier forKey:ksDataId];
    [aCoder encodeObject:_courierPaid forKey:ksDataCourierPaid];
    [aCoder encodeObject:_updatedBy forKey:ksDataUpdatedBy];
    [aCoder encodeObject:_receiverPhoneNumber forKey:ksDataReceiverPhoneNumber];
    [aCoder encodeObject:_sendName forKey:ksDataSendName];
    [aCoder encodeObject:_packOptionsText forKey:ksDataPackOptionsText];
    [aCoder encodeObject:_deliveryTakeType forKey:ksDataDeliveryTakeType];
    [aCoder encodeObject:_packToText forKey:ksDataPackToText];
    [aCoder encodeObject:_statusSender forKey:ksDataStatusSender];
    [aCoder encodeObject:_acceptStatus forKey:ksDataAcceptStatus];
    [aCoder encodeObject:_acceptedCourierCell forKey:ksDataAcceptedCourierCell];
    [aCoder encodeObject:_distanceTime forKey:ksDataDistanceTime];
    [aCoder encodeObject:_acceptedDate forKey:ksDataAcceptedDate];
    [aCoder encodeObject:_itemValue forKey:ksDataItemValue];
    [aCoder encodeObject:_acceptedCourierName forKey:ksDataAcceptedCourierName];
    [aCoder encodeObject:_insertedBy forKey:ksDataInsertedBy];
    [aCoder encodeObject:_imgTypeName forKey:ksDataImgTypeName];
    [aCoder encodeObject:_takenAsapText forKey:ksDataTakenAsapText];
    [aCoder encodeObject:_packId forKey:ksDataPackId];
    [aCoder encodeObject:_sendCatText forKey:ksDataSendCatText];
    [aCoder encodeObject:_packStatus forKey:ksDataPackStatus];
    [aCoder encodeObject:_sendCatId forKey:ksDataSendCatId];
    [aCoder encodeObject:_takenBringTypeText forKey:ksDataTakenBringTypeText];
    [aCoder encodeObject:_packToLng forKey:ksDataPackToLng];
    [aCoder encodeObject:_bidText forKey:ksDataBidText];
    [aCoder encodeObject:_takenTypeText forKey:ksDataTakenTypeText];
    [aCoder encodeObject:_packOwnerCell forKey:ksDataPackOwnerCell];
    [aCoder encodeObject:_deliveryAsap forKey:ksDataDeliveryAsap];
    [aCoder encodeObject:_packImg forKey:ksDataPackImg];
    [aCoder encodeObject:_courierName forKey:ksDataCourierName];
    [aCoder encodeObject:_petIsVac forKey:ksDataPetIsVac];
    [aCoder encodeObject:_packPrice forKey:ksDataPackPrice];
    [aCoder encodeObject:_packOwnerName forKey:ksDataPackOwnerName];
    [aCoder encodeObject:_petIsVacText forKey:ksDataPetIsVacText];
    [aCoder encodeObject:_imgId forKey:ksDataImgId];
    [aCoder encodeObject:_statusCourier forKey:ksDataStatusCourier];
    [aCoder encodeObject:_packSizeText forKey:ksDataPackSizeText];
    [aCoder encodeObject:_leaveByDoor forKey:ksDataLeaveByDoor];
    [aCoder encodeObject:_packOwnerSurname forKey:ksDataPackOwnerSurname];
    [aCoder encodeObject:_reasonId forKey:ksDataReasonId];
    [aCoder encodeObject:_packFromLat forKey:ksDataPackFromLat];
    [aCoder encodeObject:_petType forKey:ksDataPetType];
    [aCoder encodeObject:_distance forKey:ksDataDistance];
    [aCoder encodeObject:_deliveryAsapText forKey:ksDataDeliveryAsapText];
    [aCoder encodeObject:_reasonType forKey:ksDataReasonType];
    [aCoder encodeObject:_takenType forKey:ksDataTakenType];
    [aCoder encodeObject:_leaveByDoorText forKey:ksDataLeaveByDoorText];
    [aCoder encodeObject:_reasonTemplateText forKey:ksDataReasonTemplateText];
    [aCoder encodeObject:_courierCell forKey:ksDataCourierCell];
    [aCoder encodeObject:_acceptedCourierSurname forKey:ksDataAcceptedCourierSurname];
    [aCoder encodeObject:_takenBringType forKey:ksDataTakenBringType];
    [aCoder encodeObject:_deliveryTime forKey:ksDataDeliveryTime];
    [aCoder encodeObject:_insuranceMode forKey:ksDataInsuranceMode];
    [aCoder encodeObject:_bidderId forKey:ksDataBidderId];
    [aCoder encodeObject:_bidDate forKey:ksDataBidDate];
    [aCoder encodeObject:_reasonText forKey:ksDataReasonText];
    [aCoder encodeObject:_deliveryTypeText forKey:ksDataDeliveryTypeText];
    [aCoder encodeObject:_takenTime forKey:ksDataTakenTime];
    [aCoder encodeObject:_acceptedCourierId forKey:ksDataAcceptedCourierId];
    [aCoder encodeObject:_acceptStatusText forKey:ksDataAcceptStatusText];
    [aCoder encodeObject:_isInsurancePayed forKey:ksDataIsInsurancePayed];
    [aCoder encodeObject:_receiverName forKey:ksDataReceiverName];
    [aCoder encodeObject:_packFromLng forKey:ksDataPackFromLng];
    [aCoder encodeObject:_acceptedCourierEmail forKey:ksDataAcceptedCourierEmail];
    [aCoder encodeObject:_packOwnerId forKey:ksDataPackOwnerId];
    [aCoder encodeObject:_packOptionsId forKey:ksDataPackOptionsId];
    [aCoder encodeObject:_packToLat forKey:ksDataPackToLat];
    [aCoder encodeObject:_updatedDate forKey:ksDataUpdatedDate];
    [aCoder encodeObject:_insertedDate forKey:ksDataInsertedDate];
    [aCoder encodeObject:_courierSurname forKey:ksDataCourierSurname];
    [aCoder encodeObject:_descText forKey:ksDataDescText];
    [aCoder encodeObject:_packWeight forKey:ksDataPackWeight];
    [aCoder encodeObject:_deliveryType forKey:ksDataDeliveryType];
    [aCoder encodeObject:_insurancePrice forKey:ksDataInsurancePrice];
    [aCoder encodeObject:_takenAsap forKey:ksDataTakenAsap];
    [aCoder encodeObject:_senderPaid forKey:ksDataSenderPaid];
    [aCoder encodeObject:_packSizeId forKey:ksDataPackSizeId];
}

- (id)copyWithZone:(NSZone *)zone {
    SenderData *copy = [[SenderData alloc] init];
    
    
    
    if (copy) {

        copy.packFromText = [self.packFromText copyWithZone:zone];
        copy.petTypeText = [self.petTypeText copyWithZone:zone];
        copy.courierEmail = [self.courierEmail copyWithZone:zone];
        copy.packOwnerEmail = [self.packOwnerEmail copyWithZone:zone];
        copy.petCount = [self.petCount copyWithZone:zone];
        copy.deliveryBringTypeText = [self.deliveryBringTypeText copyWithZone:zone];
        copy.dataIdentifier = [self.dataIdentifier copyWithZone:zone];
        copy.courierPaid = [self.courierPaid copyWithZone:zone];
        copy.updatedBy = [self.updatedBy copyWithZone:zone];
        copy.receiverPhoneNumber = [self.receiverPhoneNumber copyWithZone:zone];
        copy.sendName = [self.sendName copyWithZone:zone];
        copy.packOptionsText = [self.packOptionsText copyWithZone:zone];
        copy.deliveryTakeType = [self.deliveryTakeType copyWithZone:zone];
        copy.packToText = [self.packToText copyWithZone:zone];
        copy.statusSender = [self.statusSender copyWithZone:zone];
        copy.acceptStatus = [self.acceptStatus copyWithZone:zone];
        copy.acceptedCourierCell = [self.acceptedCourierCell copyWithZone:zone];
        copy.distanceTime = [self.distanceTime copyWithZone:zone];
        copy.acceptedDate = [self.acceptedDate copyWithZone:zone];
        copy.itemValue = [self.itemValue copyWithZone:zone];
        copy.acceptedCourierName = [self.acceptedCourierName copyWithZone:zone];
        copy.insertedBy = [self.insertedBy copyWithZone:zone];
        copy.imgTypeName = [self.imgTypeName copyWithZone:zone];
        copy.takenAsapText = [self.takenAsapText copyWithZone:zone];
        copy.packId = [self.packId copyWithZone:zone];
        copy.sendCatText = [self.sendCatText copyWithZone:zone];
        copy.packStatus = [self.packStatus copyWithZone:zone];
        copy.sendCatId = [self.sendCatId copyWithZone:zone];
        copy.takenBringTypeText = [self.takenBringTypeText copyWithZone:zone];
        copy.packToLng = [self.packToLng copyWithZone:zone];
        copy.bidText = [self.bidText copyWithZone:zone];
        copy.takenTypeText = [self.takenTypeText copyWithZone:zone];
        copy.packOwnerCell = [self.packOwnerCell copyWithZone:zone];
        copy.deliveryAsap = [self.deliveryAsap copyWithZone:zone];
        copy.packImg = [self.packImg copyWithZone:zone];
        copy.courierName = [self.courierName copyWithZone:zone];
        copy.petIsVac = [self.petIsVac copyWithZone:zone];
        copy.packPrice = [self.packPrice copyWithZone:zone];
        copy.packOwnerName = [self.packOwnerName copyWithZone:zone];
        copy.petIsVacText = [self.petIsVacText copyWithZone:zone];
        copy.imgId = [self.imgId copyWithZone:zone];
        copy.statusCourier = [self.statusCourier copyWithZone:zone];
        copy.packSizeText = [self.packSizeText copyWithZone:zone];
        copy.leaveByDoor = [self.leaveByDoor copyWithZone:zone];
        copy.packOwnerSurname = [self.packOwnerSurname copyWithZone:zone];
        copy.reasonId = [self.reasonId copyWithZone:zone];
        copy.packFromLat = [self.packFromLat copyWithZone:zone];
        copy.petType = [self.petType copyWithZone:zone];
        copy.distance = [self.distance copyWithZone:zone];
        copy.deliveryAsapText = [self.deliveryAsapText copyWithZone:zone];
        copy.reasonType = [self.reasonType copyWithZone:zone];
        copy.takenType = [self.takenType copyWithZone:zone];
        copy.leaveByDoorText = [self.leaveByDoorText copyWithZone:zone];
        copy.reasonTemplateText = [self.reasonTemplateText copyWithZone:zone];
        copy.courierCell = [self.courierCell copyWithZone:zone];
        copy.acceptedCourierSurname = [self.acceptedCourierSurname copyWithZone:zone];
        copy.takenBringType = [self.takenBringType copyWithZone:zone];
        copy.deliveryTime = [self.deliveryTime copyWithZone:zone];
        copy.insuranceMode = [self.insuranceMode copyWithZone:zone];
        copy.bidderId = [self.bidderId copyWithZone:zone];
        copy.bidDate = [self.bidDate copyWithZone:zone];
        copy.reasonText = [self.reasonText copyWithZone:zone];
        copy.deliveryTypeText = [self.deliveryTypeText copyWithZone:zone];
        copy.takenTime = [self.takenTime copyWithZone:zone];
        copy.acceptedCourierId = [self.acceptedCourierId copyWithZone:zone];
        copy.acceptStatusText = [self.acceptStatusText copyWithZone:zone];
        copy.isInsurancePayed = [self.isInsurancePayed copyWithZone:zone];
        copy.receiverName = [self.receiverName copyWithZone:zone];
        copy.packFromLng = [self.packFromLng copyWithZone:zone];
        copy.acceptedCourierEmail = [self.acceptedCourierEmail copyWithZone:zone];
        copy.packOwnerId = [self.packOwnerId copyWithZone:zone];
        copy.packOptionsId = [self.packOptionsId copyWithZone:zone];
        copy.packToLat = [self.packToLat copyWithZone:zone];
        copy.updatedDate = [self.updatedDate copyWithZone:zone];
        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.courierSurname = [self.courierSurname copyWithZone:zone];
        copy.descText = [self.descText copyWithZone:zone];
        copy.packWeight = [self.packWeight copyWithZone:zone];
        copy.deliveryType = [self.deliveryType copyWithZone:zone];
        copy.insurancePrice = [self.insurancePrice copyWithZone:zone];
        copy.takenAsap = [self.takenAsap copyWithZone:zone];
        copy.senderPaid = [self.senderPaid copyWithZone:zone];
        copy.packSizeId = [self.packSizeId copyWithZone:zone];
    }
    
    return copy;
}


@end
