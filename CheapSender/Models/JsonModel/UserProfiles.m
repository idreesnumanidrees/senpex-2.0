//
//  UserProfiles.m
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "UserProfiles.h"


NSString *const kUserProfilesVerifiedMsg = @"verified_msg";
NSString *const kUserProfilesId = @"id";
NSString *const kUserProfilesIsVerified = @"is_verified";
NSString *const kUserProfilesPayoutsEnabled = @"payouts_enabled";
NSString *const kUserProfilesUserType = @"user_type";
NSString *const kUserProfilesUserId = @"user_id";
NSString *const kUserProfilesCardLast4 = @"card_last_4";
NSString *const kUserProfilesStripeAccId = @"stripe_acc_id";
NSString *const kUserProfilesProcessedDate = @"processed_date";
NSString *const kUserProfilesIsDefault = @"is_default";
NSString *const kUserProfilesStripeCustId = @"stripe_cust_id";
NSString *const kUserProfilesIsProcessed = @"is_processed";
NSString *const kUserProfilesApprovedByAdmin = @"approved_by_admin";


@interface UserProfiles ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation UserProfiles

@synthesize verifiedMsg = _verifiedMsg;
@synthesize userProfilesIdentifier = _userProfilesIdentifier;
@synthesize isVerified = _isVerified;
@synthesize payoutsEnabled = _payoutsEnabled;
@synthesize userType = _userType;
@synthesize userId = _userId;
@synthesize cardLast4 = _cardLast4;
@synthesize stripeAccId = _stripeAccId;
@synthesize processedDate = _processedDate;
@synthesize isDefault = _isDefault;
@synthesize stripeCustId = _stripeCustId;
@synthesize isProcessed = _isProcessed;
@synthesize approvedByAdmin = _approvedByAdmin;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.verifiedMsg = [self objectOrNilForKey:kUserProfilesVerifiedMsg fromDictionary:dict];
            self.userProfilesIdentifier = [self objectOrNilForKey:kUserProfilesId fromDictionary:dict];
            self.isVerified = [self objectOrNilForKey:kUserProfilesIsVerified fromDictionary:dict];
            self.payoutsEnabled = [self objectOrNilForKey:kUserProfilesPayoutsEnabled fromDictionary:dict];
            self.userType = [self objectOrNilForKey:kUserProfilesUserType fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kUserProfilesUserId fromDictionary:dict];
            self.cardLast4 = [self objectOrNilForKey:kUserProfilesCardLast4 fromDictionary:dict];
            self.stripeAccId = [self objectOrNilForKey:kUserProfilesStripeAccId fromDictionary:dict];
            self.processedDate = [self objectOrNilForKey:kUserProfilesProcessedDate fromDictionary:dict];
            self.isDefault = [self objectOrNilForKey:kUserProfilesIsDefault fromDictionary:dict];
            self.stripeCustId = [self objectOrNilForKey:kUserProfilesStripeCustId fromDictionary:dict];
            self.isProcessed = [self objectOrNilForKey:kUserProfilesIsProcessed fromDictionary:dict];
            self.approvedByAdmin = [self objectOrNilForKey:kUserProfilesApprovedByAdmin fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.verifiedMsg forKey:kUserProfilesVerifiedMsg];
    [mutableDict setValue:self.userProfilesIdentifier forKey:kUserProfilesId];
    [mutableDict setValue:self.isVerified forKey:kUserProfilesIsVerified];
    [mutableDict setValue:self.payoutsEnabled forKey:kUserProfilesPayoutsEnabled];
    [mutableDict setValue:self.userType forKey:kUserProfilesUserType];
    [mutableDict setValue:self.userId forKey:kUserProfilesUserId];
    [mutableDict setValue:self.cardLast4 forKey:kUserProfilesCardLast4];
    [mutableDict setValue:self.stripeAccId forKey:kUserProfilesStripeAccId];
    [mutableDict setValue:self.processedDate forKey:kUserProfilesProcessedDate];
    [mutableDict setValue:self.isDefault forKey:kUserProfilesIsDefault];
    [mutableDict setValue:self.stripeCustId forKey:kUserProfilesStripeCustId];
    [mutableDict setValue:self.isProcessed forKey:kUserProfilesIsProcessed];
    [mutableDict setValue:self.approvedByAdmin forKey:kUserProfilesApprovedByAdmin];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.verifiedMsg = [aDecoder decodeObjectForKey:kUserProfilesVerifiedMsg];
    self.userProfilesIdentifier = [aDecoder decodeObjectForKey:kUserProfilesId];
    self.isVerified = [aDecoder decodeObjectForKey:kUserProfilesIsVerified];
    self.payoutsEnabled = [aDecoder decodeObjectForKey:kUserProfilesPayoutsEnabled];
    self.userType = [aDecoder decodeObjectForKey:kUserProfilesUserType];
    self.userId = [aDecoder decodeObjectForKey:kUserProfilesUserId];
    self.cardLast4 = [aDecoder decodeObjectForKey:kUserProfilesCardLast4];
    self.stripeAccId = [aDecoder decodeObjectForKey:kUserProfilesStripeAccId];
    self.processedDate = [aDecoder decodeObjectForKey:kUserProfilesProcessedDate];
    self.isDefault = [aDecoder decodeObjectForKey:kUserProfilesIsDefault];
    self.stripeCustId = [aDecoder decodeObjectForKey:kUserProfilesStripeCustId];
    self.isProcessed = [aDecoder decodeObjectForKey:kUserProfilesIsProcessed];
    self.approvedByAdmin = [aDecoder decodeObjectForKey:kUserProfilesApprovedByAdmin];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_verifiedMsg forKey:kUserProfilesVerifiedMsg];
    [aCoder encodeObject:_userProfilesIdentifier forKey:kUserProfilesId];
    [aCoder encodeObject:_isVerified forKey:kUserProfilesIsVerified];
    [aCoder encodeObject:_payoutsEnabled forKey:kUserProfilesPayoutsEnabled];
    [aCoder encodeObject:_userType forKey:kUserProfilesUserType];
    [aCoder encodeObject:_userId forKey:kUserProfilesUserId];
    [aCoder encodeObject:_cardLast4 forKey:kUserProfilesCardLast4];
    [aCoder encodeObject:_stripeAccId forKey:kUserProfilesStripeAccId];
    [aCoder encodeObject:_processedDate forKey:kUserProfilesProcessedDate];
    [aCoder encodeObject:_isDefault forKey:kUserProfilesIsDefault];
    [aCoder encodeObject:_stripeCustId forKey:kUserProfilesStripeCustId];
    [aCoder encodeObject:_isProcessed forKey:kUserProfilesIsProcessed];
    [aCoder encodeObject:_approvedByAdmin forKey:kUserProfilesApprovedByAdmin];
}

- (id)copyWithZone:(NSZone *)zone {
    UserProfiles *copy = [[UserProfiles alloc] init];
    
    
    
    if (copy) {

        copy.verifiedMsg = [self.verifiedMsg copyWithZone:zone];
        copy.userProfilesIdentifier = [self.userProfilesIdentifier copyWithZone:zone];
        copy.isVerified = [self.isVerified copyWithZone:zone];
        copy.payoutsEnabled = [self.payoutsEnabled copyWithZone:zone];
        copy.userType = [self.userType copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.cardLast4 = [self.cardLast4 copyWithZone:zone];
        copy.stripeAccId = [self.stripeAccId copyWithZone:zone];
        copy.processedDate = [self.processedDate copyWithZone:zone];
        copy.isDefault = [self.isDefault copyWithZone:zone];
        copy.stripeCustId = [self.stripeCustId copyWithZone:zone];
        copy.isProcessed = [self.isProcessed copyWithZone:zone];
        copy.approvedByAdmin = [self.approvedByAdmin copyWithZone:zone];
    }
    
    return copy;
}


@end
