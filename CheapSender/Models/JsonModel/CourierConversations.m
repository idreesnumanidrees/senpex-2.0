//
//  CourierConversations.m
//
//  Created by   on 2017/08/03
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "CourierConversations.h"


NSString *const kCourierConversationsId = @"id";
NSString *const kCourierConversationsReceiverName = @"receiver_name";
NSString *const kCourierConversationsIsBanned = @"is_banned";
NSString *const kCourierConversationsSenderId = @"sender_id";
NSString *const kCourierConversationsSenderImg = @"sender_img";
NSString *const kCourierConversationsViewId = @"view_id";
NSString *const kCourierConversationsReceiverImg = @"receiver_img";
NSString *const kCourierConversationsLastMessageStatus = @"last_message_status";
NSString *const kCourierConversationsViewName = @"view_name";
NSString *const kCourierConversationsReceiverSurname = @"receiver_surname";
NSString *const kCourierConversationsInsertedDate = @"inserted_date";
NSString *const kCourierConversationsViewImg = @"view_img";
NSString *const kCourierConversationsReceiverId = @"receiver_id";
NSString *const kCourierConversationsSenderSurname = @"sender_surname";
NSString *const kCourierConversationsChatText = @"chat_text";
NSString *const kCourierConversationsSenderName = @"sender_name";


@interface CourierConversations ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation CourierConversations

@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize receiverName = _receiverName;
@synthesize isBanned = _isBanned;
@synthesize senderId = _senderId;
@synthesize senderImg = _senderImg;
@synthesize viewId = _viewId;
@synthesize receiverImg = _receiverImg;
@synthesize lastMessageStatus = _lastMessageStatus;
@synthesize viewName = _viewName;
@synthesize receiverSurname = _receiverSurname;
@synthesize insertedDate = _insertedDate;
@synthesize viewImg = _viewImg;
@synthesize receiverId = _receiverId;
@synthesize senderSurname = _senderSurname;
@synthesize chatText = _chatText;
@synthesize senderName = _senderName;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kCourierConversationsId fromDictionary:dict];
            self.receiverName = [self objectOrNilForKey:kCourierConversationsReceiverName fromDictionary:dict];
            self.isBanned = [self objectOrNilForKey:kCourierConversationsIsBanned fromDictionary:dict];
            self.senderId = [self objectOrNilForKey:kCourierConversationsSenderId fromDictionary:dict];
            self.senderImg = [self objectOrNilForKey:kCourierConversationsSenderImg fromDictionary:dict];
            self.viewId = [self objectOrNilForKey:kCourierConversationsViewId fromDictionary:dict];
            self.receiverImg = [self objectOrNilForKey:kCourierConversationsReceiverImg fromDictionary:dict];
            self.lastMessageStatus = [self objectOrNilForKey:kCourierConversationsLastMessageStatus fromDictionary:dict];
            self.viewName = [self objectOrNilForKey:kCourierConversationsViewName fromDictionary:dict];
            self.receiverSurname = [self objectOrNilForKey:kCourierConversationsReceiverSurname fromDictionary:dict];
            self.insertedDate = [self objectOrNilForKey:kCourierConversationsInsertedDate fromDictionary:dict];
            self.viewImg = [self objectOrNilForKey:kCourierConversationsViewImg fromDictionary:dict];
            self.receiverId = [self objectOrNilForKey:kCourierConversationsReceiverId fromDictionary:dict];
            self.senderSurname = [self objectOrNilForKey:kCourierConversationsSenderSurname fromDictionary:dict];
            self.chatText = [self objectOrNilForKey:kCourierConversationsChatText fromDictionary:dict];
            self.senderName = [self objectOrNilForKey:kCourierConversationsSenderName fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kCourierConversationsId];
    [mutableDict setValue:self.receiverName forKey:kCourierConversationsReceiverName];
    [mutableDict setValue:self.isBanned forKey:kCourierConversationsIsBanned];
    [mutableDict setValue:self.senderId forKey:kCourierConversationsSenderId];
    [mutableDict setValue:self.senderImg forKey:kCourierConversationsSenderImg];
    [mutableDict setValue:self.viewId forKey:kCourierConversationsViewId];
    [mutableDict setValue:self.receiverImg forKey:kCourierConversationsReceiverImg];
    [mutableDict setValue:self.lastMessageStatus forKey:kCourierConversationsLastMessageStatus];
    [mutableDict setValue:self.viewName forKey:kCourierConversationsViewName];
    [mutableDict setValue:self.receiverSurname forKey:kCourierConversationsReceiverSurname];
    [mutableDict setValue:self.insertedDate forKey:kCourierConversationsInsertedDate];
    [mutableDict setValue:self.viewImg forKey:kCourierConversationsViewImg];
    [mutableDict setValue:self.receiverId forKey:kCourierConversationsReceiverId];
    [mutableDict setValue:self.senderSurname forKey:kCourierConversationsSenderSurname];
    [mutableDict setValue:self.chatText forKey:kCourierConversationsChatText];
    [mutableDict setValue:self.senderName forKey:kCourierConversationsSenderName];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kCourierConversationsId];
    self.receiverName = [aDecoder decodeObjectForKey:kCourierConversationsReceiverName];
    self.isBanned = [aDecoder decodeObjectForKey:kCourierConversationsIsBanned];
    self.senderId = [aDecoder decodeObjectForKey:kCourierConversationsSenderId];
    self.senderImg = [aDecoder decodeObjectForKey:kCourierConversationsSenderImg];
    self.viewId = [aDecoder decodeObjectForKey:kCourierConversationsViewId];
    self.receiverImg = [aDecoder decodeObjectForKey:kCourierConversationsReceiverImg];
    self.lastMessageStatus = [aDecoder decodeObjectForKey:kCourierConversationsLastMessageStatus];
    self.viewName = [aDecoder decodeObjectForKey:kCourierConversationsViewName];
    self.receiverSurname = [aDecoder decodeObjectForKey:kCourierConversationsReceiverSurname];
    self.insertedDate = [aDecoder decodeObjectForKey:kCourierConversationsInsertedDate];
    self.viewImg = [aDecoder decodeObjectForKey:kCourierConversationsViewImg];
    self.receiverId = [aDecoder decodeObjectForKey:kCourierConversationsReceiverId];
    self.senderSurname = [aDecoder decodeObjectForKey:kCourierConversationsSenderSurname];
    self.chatText = [aDecoder decodeObjectForKey:kCourierConversationsChatText];
    self.senderName = [aDecoder decodeObjectForKey:kCourierConversationsSenderName];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kCourierConversationsId];
    [aCoder encodeObject:_receiverName forKey:kCourierConversationsReceiverName];
    [aCoder encodeObject:_isBanned forKey:kCourierConversationsIsBanned];
    [aCoder encodeObject:_senderId forKey:kCourierConversationsSenderId];
    [aCoder encodeObject:_senderImg forKey:kCourierConversationsSenderImg];
    [aCoder encodeObject:_viewId forKey:kCourierConversationsViewId];
    [aCoder encodeObject:_receiverImg forKey:kCourierConversationsReceiverImg];
    [aCoder encodeObject:_lastMessageStatus forKey:kCourierConversationsLastMessageStatus];
    [aCoder encodeObject:_viewName forKey:kCourierConversationsViewName];
    [aCoder encodeObject:_receiverSurname forKey:kCourierConversationsReceiverSurname];
    [aCoder encodeObject:_insertedDate forKey:kCourierConversationsInsertedDate];
    [aCoder encodeObject:_viewImg forKey:kCourierConversationsViewImg];
    [aCoder encodeObject:_receiverId forKey:kCourierConversationsReceiverId];
    [aCoder encodeObject:_senderSurname forKey:kCourierConversationsSenderSurname];
    [aCoder encodeObject:_chatText forKey:kCourierConversationsChatText];
    [aCoder encodeObject:_senderName forKey:kCourierConversationsSenderName];
}

- (id)copyWithZone:(NSZone *)zone {
    CourierConversations *copy = [[CourierConversations alloc] init];
    
    
    
    if (copy) {

        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.receiverName = [self.receiverName copyWithZone:zone];
        copy.isBanned = [self.isBanned copyWithZone:zone];
        copy.senderId = [self.senderId copyWithZone:zone];
        copy.senderImg = [self.senderImg copyWithZone:zone];
        copy.viewId = [self.viewId copyWithZone:zone];
        copy.receiverImg = [self.receiverImg copyWithZone:zone];
        copy.lastMessageStatus = [self.lastMessageStatus copyWithZone:zone];
        copy.viewName = [self.viewName copyWithZone:zone];
        copy.receiverSurname = [self.receiverSurname copyWithZone:zone];
        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.viewImg = [self.viewImg copyWithZone:zone];
        copy.receiverId = [self.receiverId copyWithZone:zone];
        copy.senderSurname = [self.senderSurname copyWithZone:zone];
        copy.chatText = [self.chatText copyWithZone:zone];
        copy.senderName = [self.senderName copyWithZone:zone];
    }
    
    return copy;
}


@end
