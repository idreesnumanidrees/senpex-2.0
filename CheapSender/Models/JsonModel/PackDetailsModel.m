//
//  PackDetailsModel.m
//
//  Created by   on 2017/06/15
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PackDetailsModel.h"
#import "PackImages.h"

NSString *const kPackDetailsModelPackFromText = @"pack_from_text";
NSString *const kPackDetailsModelPetTypeText = @"pet_type_text";
NSString *const kPackDetailsModelPackOwnerEmail = @"pack_owner_email";
NSString *const kPackDetailsModelDeliveryBringTypeText = @"delivery_bring_type_text";
NSString *const kPackDetailsModelPetCount = @"pet_count";
NSString *const kPackDetailsModelPackImages = @"pack_images";
NSString *const kPackDetailsModelId = @"id";
NSString *const kPackDetailsCourierProfit = @"courier_profit";

NSString *const kPackDetailsModelCourierPaid = @"courier_paid";
NSString *const kPackDetailsModelUpdatedBy = @"updated_by";
NSString *const kPackDetailsModelReceiverPhoneNumber = @"receiver_phone_number";
NSString *const kPackDetailsModelSendName = @"send_name";
NSString *const kPackDetailsModelPackOptionsText = @"pack_options_text";
NSString *const kPackDetailsModelDeliveryTakeType = @"delivery_take_type";
NSString *const kDataAcceptedCourierAvailStatus = @"accepted_courier_avail_status";
NSString *const kDataAcceptedCourierSelfImg = @"accepted_courier_self_img";
NSString *const kDataAcceptedCourierRate = @"accepted_courier_rate";
NSString *const kPackDetailsModelPackToText = @"pack_to_text";
NSString *const kPackDetailsModelStatusSender = @"status_sender";
NSString *const kPackDetailsModelAcceptedCourierCell = @"accepted_courier_cell";
NSString *const kPackDetailsModelDistanceTime = @"distance_time";
NSString *const kPackDetailsModelItemValue = @"item_value";
NSString *const kDataCLastLng = @"c_last_lng";
NSString *const kPackDetailsModelAcceptedCourierName = @"accepted_courier_name";
NSString *const kPackDetailsModelInsertedBy = @"inserted_by";
NSString *const kPackDetailsModelImgTypeName = @"img_type_name";
NSString *const kPackDetailsModelTakenAsapText = @"taken_asap_text";
NSString *const kPackDetailsModelPackBidders = @"pack_bidders";
NSString *const kPackDetailsModelSendCatText = @"send_cat_text";
NSString *const kPackDetailsModelPackStatus = @"pack_status";
NSString *const kPackDetailsModelSendCatId = @"send_cat_id";
NSString *const kPackDetailsModelTakenBringTypeText = @"taken_bring_type_text";
NSString *const kPackDetailsModelPackToLng = @"pack_to_lng";
NSString *const kPackDetailsModelTakenTypeText = @"taken_type_text";
NSString *const kPackDetailsModelPackOwnerCell = @"pack_owner_cell";
NSString *const kDataCLastLat = @"c_last_lat";
NSString *const kPackDetailsModelDeliveryAsap = @"delivery_asap";
NSString *const kPackDetailsModelPackImg = @"pack_img";
NSString *const kPackDetailsModelPetIsVac = @"pet_is_vac";
NSString *const kPackDetailsModelPackOwnerName = @"pack_owner_name";
NSString *const kPackDetailsModelPackOwnerSelfImg = @"pack_owner_self_img";
NSString *const kPackDetailsModelPackPrice = @"pack_price";
NSString *const kPackDetailsModelPetIsVacText = @"pet_is_vac_text";
NSString *const kPackDetailsModelImgId = @"img_id";
NSString *const kPackDetailsModelStatusCourier = @"status_courier";
NSString *const kPackDetailsModelPackSizeText = @"pack_size_text";
NSString *const kPackDetailsModelLeaveByDoor = @"leave_by_door";
NSString *const kPackDetailsModelPackOwnerSurname = @"pack_owner_surname";
NSString *const kPackDetailsModelPetType = @"pet_type";
NSString *const kPackDetailsModelPackFromLat = @"pack_from_lat";
NSString *const kPackDetailsModelDistance = @"distance";
NSString *const kPackDetailsModelDeliveryAsapText = @"delivery_asap_text";
NSString *const kPackDetailsModelTakenType = @"taken_type";
NSString *const kPackDetailsModelLeaveByDoorText = @"leave_by_door_text";
NSString *const kPackDetailsModelAcceptedCourierSurname = @"accepted_courier_surname";
NSString *const kPackDetailsModelInsuranceMode = @"insurance_mode";
NSString *const kPackDetailsModelDeliveryTime = @"delivery_time";
NSString *const kPackDetailsModelTakenBringType = @"taken_bring_type";
NSString *const kPackDetailsModelDeliveryTypeText = @"delivery_type_text";
NSString *const kPackDetailsModelTakenTime = @"taken_time";
NSString *const kPackDetailsModelAcceptedCourierId = @"accepted_courier_id";
NSString *const kPackDetailsModelIsInsurancePayed = @"is_insurance_payed";
NSString *const kPackDetailsModelReceiverName = @"receiver_name";
NSString *const kPackDetailsModelPackFromLng = @"pack_from_lng";
NSString *const kPackDetailsModelAcceptedCourierEmail = @"accepted_courier_email";
NSString *const kPackDetailsModelPackOwnerId = @"pack_owner_id";
NSString *const kPackDetailsModelPackOptionsId = @"pack_options_id";
NSString *const kPackDetailsModelPackToLat = @"pack_to_lat";
NSString *const kPackDetailsModelUpdatedDate = @"updated_date";
NSString *const kPackDetailsModelInsertedDate = @"inserted_date";
NSString *const kPackDetailsModelDescText = @"desc_text";
NSString *const kPackDetailsModelPackWeight = @"pack_weight";
NSString *const kPackDetailsModelDeliveryType = @"delivery_type";
NSString *const kPackDetailsModelInsurancePrice = @"insurance_price";
NSString *const kPackDetailsModelTakenAsap = @"taken_asap";
NSString *const kPackDetailsModelSenderPaid = @"sender_paid";
NSString *const kPackDetailsModelPackSizeId = @"pack_size_id";
NSString *const kPackDetailsModelLastOperationTime = @"last_operation_time";
NSString *const kPackDetailsModelWaitingTime = @"waiting_mins";
NSString *const kPackDetailsModelTips = @"tips";
#define kDataTariffText @"tariff_name"


@interface PackDetailsModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PackDetailsModel

@synthesize packFromText = _packFromText;
@synthesize petTypeText = _petTypeText;
@synthesize packOwnerEmail = _packOwnerEmail;
@synthesize deliveryBringTypeText = _deliveryBringTypeText;
@synthesize petCount = _petCount;
@synthesize packImages = _packImages;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize acceptedCourierAvailStatus = _acceptedCourierAvailStatus;
@synthesize acceptedCourierRate = _acceptedCourierRate;
@synthesize courierPaid = _courierPaid;
@synthesize updatedBy = _updatedBy;
@synthesize courierProfitD = _courierProfitD;
@synthesize acceptedCourierSelfImg = _acceptedCourierSelfImg;
@synthesize receiverPhoneNumber = _receiverPhoneNumber;
@synthesize sendName = _sendName;
@synthesize packOptionsText = _packOptionsText;
@synthesize deliveryTakeType = _deliveryTakeType;
@synthesize packToText = _packToText;
@synthesize statusSender = _statusSender;
@synthesize acceptedCourierCell = _acceptedCourierCell;
@synthesize distanceTime = _distanceTime;
@synthesize itemValue = _itemValue;
@synthesize cLastLng = _cLastLng;
@synthesize acceptedCourierName = _acceptedCourierName;
@synthesize insertedBy = _insertedBy;
@synthesize imgTypeName = _imgTypeName;
@synthesize takenAsapText = _takenAsapText;
@synthesize packBidders = _packBidders;
@synthesize sendCatText = _sendCatText;
@synthesize packStatus = _packStatus;
@synthesize sendCatId = _sendCatId;
@synthesize takenBringTypeText = _takenBringTypeText;
@synthesize packToLng = _packToLng;
@synthesize takenTypeText = _takenTypeText;
@synthesize packOwnerCell = _packOwnerCell;
@synthesize cLastLat = _cLastLat;
@synthesize deliveryAsap = _deliveryAsap;
@synthesize packImg = _packImg;
@synthesize petIsVac = _petIsVac;
@synthesize packOwnerName = _packOwnerName;
@synthesize packOwnerSelfImage = _packOwnerSelfImage;
@synthesize packPrice = _packPrice;
@synthesize petIsVacText = _petIsVacText;
@synthesize imgId = _imgId;
@synthesize statusCourier = _statusCourier;
@synthesize packSizeText = _packSizeText;
@synthesize leaveByDoor = _leaveByDoor;
@synthesize packOwnerSurname = _packOwnerSurname;
@synthesize petType = _petType;
@synthesize packFromLat = _packFromLat;
@synthesize distance = _distance;
@synthesize deliveryAsapText = _deliveryAsapText;
@synthesize takenType = _takenType;
@synthesize leaveByDoorText = _leaveByDoorText;
@synthesize acceptedCourierSurname = _acceptedCourierSurname;
@synthesize insuranceMode = _insuranceMode;
@synthesize deliveryTime = _deliveryTime;
@synthesize takenBringType = _takenBringType;
@synthesize deliveryTypeText = _deliveryTypeText;
@synthesize takenTime = _takenTime;
@synthesize acceptedCourierId = _acceptedCourierId;
@synthesize isInsurancePayed = _isInsurancePayed;
@synthesize receiverName = _receiverName;
@synthesize packFromLng = _packFromLng;
@synthesize acceptedCourierEmail = _acceptedCourierEmail;
@synthesize packOwnerId = _packOwnerId;
@synthesize packOptionsId = _packOptionsId;
@synthesize packToLat = _packToLat;
@synthesize updatedDate = _updatedDate;
@synthesize insertedDate = _insertedDate;
@synthesize descText = _descText;
@synthesize packWeight = _packWeight;
@synthesize deliveryType = _deliveryType;
@synthesize insurancePrice = _insurancePrice;
@synthesize takenAsap = _takenAsap;
@synthesize senderPaid = _senderPaid;
@synthesize packSizeId = _packSizeId;
@synthesize lastOperationTime = _lastOperationTime;
@synthesize waitingTime = _waitingTime;
@synthesize tips = _tips;
@synthesize tariffText = _tariffText;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
        
            self.packFromText = [self objectOrNilForKey:kPackDetailsModelPackFromText fromDictionary:dict];
            self.petTypeText = [self objectOrNilForKey:kPackDetailsModelPetTypeText fromDictionary:dict];
            self.packOwnerEmail = [self objectOrNilForKey:kPackDetailsModelPackOwnerEmail fromDictionary:dict];
        self.courierProfitD = [self objectOrNilForKey:kPackDetailsCourierProfit fromDictionary:dict];

            self.deliveryBringTypeText = [self objectOrNilForKey:kPackDetailsModelDeliveryBringTypeText fromDictionary:dict];
            self.petCount = [self objectOrNilForKey:kPackDetailsModelPetCount fromDictionary:dict];
    NSObject *receivedPackImages = [dict objectForKey:kPackDetailsModelPackImages];
    NSMutableArray *parsedPackImages = [NSMutableArray array];
    
    if ([receivedPackImages isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedPackImages) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedPackImages addObject:[PackImages modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedPackImages isKindOfClass:[NSDictionary class]]) {
       [parsedPackImages addObject:[PackImages modelObjectWithDictionary:(NSDictionary *)receivedPackImages]];
    }

    self.packImages = [NSArray arrayWithArray:parsedPackImages];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kPackDetailsModelId fromDictionary:dict];
            self.courierPaid = [self objectOrNilForKey:kPackDetailsModelCourierPaid fromDictionary:dict];
            self.updatedBy = [self objectOrNilForKey:kPackDetailsModelUpdatedBy fromDictionary:dict];
            self.receiverPhoneNumber = [self objectOrNilForKey:kPackDetailsModelReceiverPhoneNumber fromDictionary:dict];
            self.sendName = [self objectOrNilForKey:kPackDetailsModelSendName fromDictionary:dict];
            self.packOptionsText = [self objectOrNilForKey:kPackDetailsModelPackOptionsText fromDictionary:dict];
            self.deliveryTakeType = [self objectOrNilForKey:kPackDetailsModelDeliveryTakeType fromDictionary:dict];
            self.packToText = [self objectOrNilForKey:kPackDetailsModelPackToText fromDictionary:dict];
        self.acceptedCourierAvailStatus = [self objectOrNilForKey:kDataAcceptedCourierAvailStatus fromDictionary:dict];
          self.acceptedCourierSelfImg = [self objectOrNilForKey:kDataAcceptedCourierSelfImg fromDictionary:dict];
        self.acceptedCourierRate = [self objectOrNilForKey:kDataAcceptedCourierRate fromDictionary:dict];

            self.statusSender = [self objectOrNilForKey:kPackDetailsModelStatusSender fromDictionary:dict];
            self.acceptedCourierCell = [self objectOrNilForKey:kPackDetailsModelAcceptedCourierCell fromDictionary:dict];
            self.distanceTime = [self objectOrNilForKey:kPackDetailsModelDistanceTime fromDictionary:dict];
            self.itemValue = [self objectOrNilForKey:kPackDetailsModelItemValue fromDictionary:dict];
            self.cLastLng = [self objectOrNilForKey:kDataCLastLng fromDictionary:dict];
            self.acceptedCourierName = [self objectOrNilForKey:kPackDetailsModelAcceptedCourierName fromDictionary:dict];
            self.insertedBy = [self objectOrNilForKey:kPackDetailsModelInsertedBy fromDictionary:dict];
            self.imgTypeName = [self objectOrNilForKey:kPackDetailsModelImgTypeName fromDictionary:dict];
            self.takenAsapText = [self objectOrNilForKey:kPackDetailsModelTakenAsapText fromDictionary:dict];
            self.packBidders = [self objectOrNilForKey:kPackDetailsModelPackBidders fromDictionary:dict];
            self.sendCatText = [self objectOrNilForKey:kPackDetailsModelSendCatText fromDictionary:dict];
            self.packStatus = [self objectOrNilForKey:kPackDetailsModelPackStatus fromDictionary:dict];
            self.sendCatId = [self objectOrNilForKey:kPackDetailsModelSendCatId fromDictionary:dict];
            self.takenBringTypeText = [self objectOrNilForKey:kPackDetailsModelTakenBringTypeText fromDictionary:dict];
            self.packToLng = [self objectOrNilForKey:kPackDetailsModelPackToLng fromDictionary:dict];
            self.takenTypeText = [self objectOrNilForKey:kPackDetailsModelTakenTypeText fromDictionary:dict];
            self.packOwnerCell = [self objectOrNilForKey:kPackDetailsModelPackOwnerCell fromDictionary:dict];
           self.cLastLat = [self objectOrNilForKey:kDataCLastLat fromDictionary:dict];
            self.deliveryAsap = [self objectOrNilForKey:kPackDetailsModelDeliveryAsap fromDictionary:dict];
            self.packImg = [self objectOrNilForKey:kPackDetailsModelPackImg fromDictionary:dict];
            self.petIsVac = [self objectOrNilForKey:kPackDetailsModelPetIsVac fromDictionary:dict];
            self.packOwnerName = [self objectOrNilForKey:kPackDetailsModelPackOwnerName fromDictionary:dict];
        self.packOwnerSelfImage = [self objectOrNilForKey:kPackDetailsModelPackOwnerSelfImg fromDictionary:dict];

            self.packPrice = [self objectOrNilForKey:kPackDetailsModelPackPrice fromDictionary:dict];
            self.petIsVacText = [self objectOrNilForKey:kPackDetailsModelPetIsVacText fromDictionary:dict];
            self.imgId = [self objectOrNilForKey:kPackDetailsModelImgId fromDictionary:dict];
            self.statusCourier = [self objectOrNilForKey:kPackDetailsModelStatusCourier fromDictionary:dict];
            self.packSizeText = [self objectOrNilForKey:kPackDetailsModelPackSizeText fromDictionary:dict];
            self.leaveByDoor = [self objectOrNilForKey:kPackDetailsModelLeaveByDoor fromDictionary:dict];
            self.packOwnerSurname = [self objectOrNilForKey:kPackDetailsModelPackOwnerSurname fromDictionary:dict];
            self.petType = [self objectOrNilForKey:kPackDetailsModelPetType fromDictionary:dict];
            self.packFromLat = [self objectOrNilForKey:kPackDetailsModelPackFromLat fromDictionary:dict];
            self.distance = [self objectOrNilForKey:kPackDetailsModelDistance fromDictionary:dict];
            self.deliveryAsapText = [self objectOrNilForKey:kPackDetailsModelDeliveryAsapText fromDictionary:dict];
            self.takenType = [self objectOrNilForKey:kPackDetailsModelTakenType fromDictionary:dict];
            self.leaveByDoorText = [self objectOrNilForKey:kPackDetailsModelLeaveByDoorText fromDictionary:dict];
            self.acceptedCourierSurname = [self objectOrNilForKey:kPackDetailsModelAcceptedCourierSurname fromDictionary:dict];
            self.insuranceMode = [self objectOrNilForKey:kPackDetailsModelInsuranceMode fromDictionary:dict];
            self.deliveryTime = [self objectOrNilForKey:kPackDetailsModelDeliveryTime fromDictionary:dict];
            self.takenBringType = [self objectOrNilForKey:kPackDetailsModelTakenBringType fromDictionary:dict];
            self.deliveryTypeText = [self objectOrNilForKey:kPackDetailsModelDeliveryTypeText fromDictionary:dict];
            self.takenTime = [self objectOrNilForKey:kPackDetailsModelTakenTime fromDictionary:dict];
            self.acceptedCourierId = [self objectOrNilForKey:kPackDetailsModelAcceptedCourierId fromDictionary:dict];
            self.isInsurancePayed = [self objectOrNilForKey:kPackDetailsModelIsInsurancePayed fromDictionary:dict];
            self.receiverName = [self objectOrNilForKey:kPackDetailsModelReceiverName fromDictionary:dict];
            self.packFromLng = [self objectOrNilForKey:kPackDetailsModelPackFromLng fromDictionary:dict];
            self.acceptedCourierEmail = [self objectOrNilForKey:kPackDetailsModelAcceptedCourierEmail fromDictionary:dict];
            self.packOwnerId = [self objectOrNilForKey:kPackDetailsModelPackOwnerId fromDictionary:dict];
            self.packOptionsId = [self objectOrNilForKey:kPackDetailsModelPackOptionsId fromDictionary:dict];
            self.packToLat = [self objectOrNilForKey:kPackDetailsModelPackToLat fromDictionary:dict];
            self.updatedDate = [self objectOrNilForKey:kPackDetailsModelUpdatedDate fromDictionary:dict];
            self.insertedDate = [self objectOrNilForKey:kPackDetailsModelInsertedDate fromDictionary:dict];
            self.descText = [self objectOrNilForKey:kPackDetailsModelDescText fromDictionary:dict];
            self.packWeight = [self objectOrNilForKey:kPackDetailsModelPackWeight fromDictionary:dict];
            self.deliveryType = [self objectOrNilForKey:kPackDetailsModelDeliveryType fromDictionary:dict];
            self.insurancePrice = [self objectOrNilForKey:kPackDetailsModelInsurancePrice fromDictionary:dict];
            self.takenAsap = [self objectOrNilForKey:kPackDetailsModelTakenAsap fromDictionary:dict];
            self.senderPaid = [self objectOrNilForKey:kPackDetailsModelSenderPaid fromDictionary:dict];
            self.packSizeId = [self objectOrNilForKey:kPackDetailsModelPackSizeId fromDictionary:dict];
            self.lastOperationTime = [self objectOrNilForKey:kPackDetailsModelLastOperationTime
                                              fromDictionary:dict];
        self.waitingTime = [self objectOrNilForKey:kPackDetailsModelWaitingTime fromDictionary:dict];
        self.tips = [self objectOrNilForKey:kPackDetailsModelTips fromDictionary:dict];
        self.tariffText = [self objectOrNilForKey:kDataTariffText
                                   fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    
    [mutableDict setValue:self.packFromText forKey:kPackDetailsModelPackFromText];
    [mutableDict setValue:self.petTypeText forKey:kPackDetailsModelPetTypeText];
    [mutableDict setValue:self.packOwnerEmail forKey:kPackDetailsModelPackOwnerEmail];
    [mutableDict setValue:self.deliveryBringTypeText forKey:kPackDetailsModelDeliveryBringTypeText];
    [mutableDict setValue:self.petCount forKey:kPackDetailsModelPetCount];
    [mutableDict setValue:self.courierProfitD forKey:kPackDetailsCourierProfit];

    NSMutableArray *tempArrayForPackImages = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.packImages) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPackImages addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPackImages addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPackImages] forKey:kPackDetailsModelPackImages];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kPackDetailsModelId];
    [mutableDict setValue:self.courierPaid forKey:kPackDetailsModelCourierPaid];
    [mutableDict setValue:self.acceptedCourierAvailStatus forKey:kDataAcceptedCourierAvailStatus];
    [mutableDict setValue:self.acceptedCourierSelfImg forKey:kDataAcceptedCourierSelfImg];
    [mutableDict setValue:self.acceptedCourierRate forKey:kDataAcceptedCourierRate];
    [mutableDict setValue:self.updatedBy forKey:kPackDetailsModelUpdatedBy];
    [mutableDict setValue:self.receiverPhoneNumber forKey:kPackDetailsModelReceiverPhoneNumber];
    [mutableDict setValue:self.sendName forKey:kPackDetailsModelSendName];
    [mutableDict setValue:self.packOptionsText forKey:kPackDetailsModelPackOptionsText];
    [mutableDict setValue:self.deliveryTakeType forKey:kPackDetailsModelDeliveryTakeType];
    [mutableDict setValue:self.packToText forKey:kPackDetailsModelPackToText];
    [mutableDict setValue:self.statusSender forKey:kPackDetailsModelStatusSender];
    [mutableDict setValue:self.acceptedCourierCell forKey:kPackDetailsModelAcceptedCourierCell];
    [mutableDict setValue:self.distanceTime forKey:kPackDetailsModelDistanceTime];
    [mutableDict setValue:self.itemValue forKey:kPackDetailsModelItemValue];
    [mutableDict setValue:self.cLastLng forKey:kDataCLastLng];
    [mutableDict setValue:self.acceptedCourierName forKey:kPackDetailsModelAcceptedCourierName];
    [mutableDict setValue:self.insertedBy forKey:kPackDetailsModelInsertedBy];
    [mutableDict setValue:self.imgTypeName forKey:kPackDetailsModelImgTypeName];
    [mutableDict setValue:self.takenAsapText forKey:kPackDetailsModelTakenAsapText];
    NSMutableArray *tempArrayForPackBidders = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.packBidders) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForPackBidders addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForPackBidders addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForPackBidders] forKey:kPackDetailsModelPackBidders];
    [mutableDict setValue:self.sendCatText forKey:kPackDetailsModelSendCatText];
    [mutableDict setValue:self.packStatus forKey:kPackDetailsModelPackStatus];
    [mutableDict setValue:self.sendCatId forKey:kPackDetailsModelSendCatId];
    [mutableDict setValue:self.takenBringTypeText forKey:kPackDetailsModelTakenBringTypeText];
    [mutableDict setValue:self.packToLng forKey:kPackDetailsModelPackToLng];
    [mutableDict setValue:self.takenTypeText forKey:kPackDetailsModelTakenTypeText];
    [mutableDict setValue:self.packOwnerCell forKey:kPackDetailsModelPackOwnerCell];
    [mutableDict setValue:self.cLastLat forKey:kDataCLastLat];
    [mutableDict setValue:self.deliveryAsap forKey:kPackDetailsModelDeliveryAsap];
    [mutableDict setValue:self.packImg forKey:kPackDetailsModelPackImg];
    [mutableDict setValue:self.petIsVac forKey:kPackDetailsModelPetIsVac];
    [mutableDict setValue:self.packOwnerName forKey:kPackDetailsModelPackOwnerName];
    [mutableDict setValue:self.packOwnerSelfImage forKey:kPackDetailsModelPackOwnerSelfImg];
    [mutableDict setValue:self.packPrice forKey:kPackDetailsModelPackPrice];
    [mutableDict setValue:self.petIsVacText forKey:kPackDetailsModelPetIsVacText];
    [mutableDict setValue:self.imgId forKey:kPackDetailsModelImgId];
    [mutableDict setValue:self.statusCourier forKey:kPackDetailsModelStatusCourier];
    [mutableDict setValue:self.packSizeText forKey:kPackDetailsModelPackSizeText];
    [mutableDict setValue:self.leaveByDoor forKey:kPackDetailsModelLeaveByDoor];
    [mutableDict setValue:self.packOwnerSurname forKey:kPackDetailsModelPackOwnerSurname];
    [mutableDict setValue:self.petType forKey:kPackDetailsModelPetType];
    [mutableDict setValue:self.packFromLat forKey:kPackDetailsModelPackFromLat];
    [mutableDict setValue:self.distance forKey:kPackDetailsModelDistance];
    [mutableDict setValue:self.deliveryAsapText forKey:kPackDetailsModelDeliveryAsapText];
    [mutableDict setValue:self.takenType forKey:kPackDetailsModelTakenType];
    [mutableDict setValue:self.leaveByDoorText forKey:kPackDetailsModelLeaveByDoorText];
    [mutableDict setValue:self.acceptedCourierSurname forKey:kPackDetailsModelAcceptedCourierSurname];
    [mutableDict setValue:self.insuranceMode forKey:kPackDetailsModelInsuranceMode];
    [mutableDict setValue:self.deliveryTime forKey:kPackDetailsModelDeliveryTime];
    [mutableDict setValue:self.takenBringType forKey:kPackDetailsModelTakenBringType];
    [mutableDict setValue:self.deliveryTypeText forKey:kPackDetailsModelDeliveryTypeText];
    [mutableDict setValue:self.takenTime forKey:kPackDetailsModelTakenTime];
    [mutableDict setValue:self.acceptedCourierId forKey:kPackDetailsModelAcceptedCourierId];
    [mutableDict setValue:self.isInsurancePayed forKey:kPackDetailsModelIsInsurancePayed];
    [mutableDict setValue:self.receiverName forKey:kPackDetailsModelReceiverName];
    [mutableDict setValue:self.packFromLng forKey:kPackDetailsModelPackFromLng];
    [mutableDict setValue:self.acceptedCourierEmail forKey:kPackDetailsModelAcceptedCourierEmail];
    [mutableDict setValue:self.packOwnerId forKey:kPackDetailsModelPackOwnerId];
    [mutableDict setValue:self.packOptionsId forKey:kPackDetailsModelPackOptionsId];
    [mutableDict setValue:self.packToLat forKey:kPackDetailsModelPackToLat];
    [mutableDict setValue:self.updatedDate forKey:kPackDetailsModelUpdatedDate];
    [mutableDict setValue:self.insertedDate forKey:kPackDetailsModelInsertedDate];
    [mutableDict setValue:self.descText forKey:kPackDetailsModelDescText];
    [mutableDict setValue:self.packWeight forKey:kPackDetailsModelPackWeight];
    [mutableDict setValue:self.deliveryType forKey:kPackDetailsModelDeliveryType];
    [mutableDict setValue:self.insurancePrice forKey:kPackDetailsModelInsurancePrice];
    [mutableDict setValue:self.takenAsap forKey:kPackDetailsModelTakenAsap];
    [mutableDict setValue:self.senderPaid forKey:kPackDetailsModelSenderPaid];
    [mutableDict setValue:self.packSizeId forKey:kPackDetailsModelPackSizeId];
    [mutableDict setValue:self.lastOperationTime forKey:kPackDetailsModelLastOperationTime];
    [mutableDict setValue:self.waitingTime forKey:kPackDetailsModelWaitingTime];
    [mutableDict setValue:self.tips forKey:kPackDetailsModelTips];
    [mutableDict setValue:self.tariffText forKey:kDataTariffText];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.packFromText = [aDecoder decodeObjectForKey:kPackDetailsModelPackFromText];
    self.petTypeText = [aDecoder decodeObjectForKey:kPackDetailsModelPetTypeText];
    self.packOwnerEmail = [aDecoder decodeObjectForKey:kPackDetailsModelPackOwnerEmail];
    self.deliveryBringTypeText = [aDecoder decodeObjectForKey:kPackDetailsModelDeliveryBringTypeText];
    self.petCount = [aDecoder decodeObjectForKey:kPackDetailsModelPetCount];
    self.courierProfitD = [aDecoder decodeObjectForKey:kPackDetailsCourierProfit];
    self.packImages = [aDecoder decodeObjectForKey:kPackDetailsModelPackImages];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kPackDetailsModelId];
    self.courierPaid = [aDecoder decodeObjectForKey:kPackDetailsModelCourierPaid];
    self.updatedBy = [aDecoder decodeObjectForKey:kPackDetailsModelUpdatedBy];
    self.acceptedCourierAvailStatus = [aDecoder decodeObjectForKey:kDataAcceptedCourierAvailStatus];
    self.acceptedCourierSelfImg = [aDecoder decodeObjectForKey:kDataAcceptedCourierSelfImg];
    self.acceptedCourierRate = [aDecoder decodeObjectForKey:kDataAcceptedCourierRate];

    self.receiverPhoneNumber = [aDecoder decodeObjectForKey:kPackDetailsModelReceiverPhoneNumber];
    self.sendName = [aDecoder decodeObjectForKey:kPackDetailsModelSendName];
    self.packOptionsText = [aDecoder decodeObjectForKey:kPackDetailsModelPackOptionsText];
    self.deliveryTakeType = [aDecoder decodeObjectForKey:kPackDetailsModelDeliveryTakeType];
    self.packToText = [aDecoder decodeObjectForKey:kPackDetailsModelPackToText];
    self.statusSender = [aDecoder decodeObjectForKey:kPackDetailsModelStatusSender];
    self.acceptedCourierCell = [aDecoder decodeObjectForKey:kPackDetailsModelAcceptedCourierCell];
    self.distanceTime = [aDecoder decodeObjectForKey:kPackDetailsModelDistanceTime];
    self.itemValue = [aDecoder decodeObjectForKey:kPackDetailsModelItemValue];
    self.cLastLng = [aDecoder decodeObjectForKey:kDataCLastLng];
    self.acceptedCourierName = [aDecoder decodeObjectForKey:kPackDetailsModelAcceptedCourierName];
    self.insertedBy = [aDecoder decodeObjectForKey:kPackDetailsModelInsertedBy];
    self.imgTypeName = [aDecoder decodeObjectForKey:kPackDetailsModelImgTypeName];
    self.takenAsapText = [aDecoder decodeObjectForKey:kPackDetailsModelTakenAsapText];
    self.packBidders = [aDecoder decodeObjectForKey:kPackDetailsModelPackBidders];
    self.sendCatText = [aDecoder decodeObjectForKey:kPackDetailsModelSendCatText];
    self.packStatus = [aDecoder decodeObjectForKey:kPackDetailsModelPackStatus];
    self.sendCatId = [aDecoder decodeObjectForKey:kPackDetailsModelSendCatId];
    self.takenBringTypeText = [aDecoder decodeObjectForKey:kPackDetailsModelTakenBringTypeText];
    self.packToLng = [aDecoder decodeObjectForKey:kPackDetailsModelPackToLng];
    self.takenTypeText = [aDecoder decodeObjectForKey:kPackDetailsModelTakenTypeText];
    self.packOwnerCell = [aDecoder decodeObjectForKey:kPackDetailsModelPackOwnerCell];
    self.cLastLat = [aDecoder decodeObjectForKey:kDataCLastLat];
    self.deliveryAsap = [aDecoder decodeObjectForKey:kPackDetailsModelDeliveryAsap];
    self.packImg = [aDecoder decodeObjectForKey:kPackDetailsModelPackImg];
    self.petIsVac = [aDecoder decodeObjectForKey:kPackDetailsModelPetIsVac];
    self.packOwnerName = [aDecoder decodeObjectForKey:kPackDetailsModelPackOwnerName];
    self.packOwnerSelfImage = [aDecoder decodeObjectForKey:kPackDetailsModelPackOwnerSelfImg];
    self.packPrice = [aDecoder decodeObjectForKey:kPackDetailsModelPackPrice];
    self.petIsVacText = [aDecoder decodeObjectForKey:kPackDetailsModelPetIsVacText];
    self.imgId = [aDecoder decodeObjectForKey:kPackDetailsModelImgId];
    self.statusCourier = [aDecoder decodeObjectForKey:kPackDetailsModelStatusCourier];
    self.packSizeText = [aDecoder decodeObjectForKey:kPackDetailsModelPackSizeText];
    self.leaveByDoor = [aDecoder decodeObjectForKey:kPackDetailsModelLeaveByDoor];
    self.packOwnerSurname = [aDecoder decodeObjectForKey:kPackDetailsModelPackOwnerSurname];
    self.petType = [aDecoder decodeObjectForKey:kPackDetailsModelPetType];
    self.packFromLat = [aDecoder decodeObjectForKey:kPackDetailsModelPackFromLat];
    self.distance = [aDecoder decodeObjectForKey:kPackDetailsModelDistance];
    self.deliveryAsapText = [aDecoder decodeObjectForKey:kPackDetailsModelDeliveryAsapText];
    self.takenType = [aDecoder decodeObjectForKey:kPackDetailsModelTakenType];
    self.leaveByDoorText = [aDecoder decodeObjectForKey:kPackDetailsModelLeaveByDoorText];
    self.acceptedCourierSurname = [aDecoder decodeObjectForKey:kPackDetailsModelAcceptedCourierSurname];
    self.insuranceMode = [aDecoder decodeObjectForKey:kPackDetailsModelInsuranceMode];
    self.deliveryTime = [aDecoder decodeObjectForKey:kPackDetailsModelDeliveryTime];
    self.takenBringType = [aDecoder decodeObjectForKey:kPackDetailsModelTakenBringType];
    self.deliveryTypeText = [aDecoder decodeObjectForKey:kPackDetailsModelDeliveryTypeText];
    self.takenTime = [aDecoder decodeObjectForKey:kPackDetailsModelTakenTime];
    self.acceptedCourierId = [aDecoder decodeObjectForKey:kPackDetailsModelAcceptedCourierId];
    self.isInsurancePayed = [aDecoder decodeObjectForKey:kPackDetailsModelIsInsurancePayed];
    self.receiverName = [aDecoder decodeObjectForKey:kPackDetailsModelReceiverName];
    self.packFromLng = [aDecoder decodeObjectForKey:kPackDetailsModelPackFromLng];
    self.acceptedCourierEmail = [aDecoder decodeObjectForKey:kPackDetailsModelAcceptedCourierEmail];
    self.packOwnerId = [aDecoder decodeObjectForKey:kPackDetailsModelPackOwnerId];
    self.packOptionsId = [aDecoder decodeObjectForKey:kPackDetailsModelPackOptionsId];
    self.packToLat = [aDecoder decodeObjectForKey:kPackDetailsModelPackToLat];
    self.updatedDate = [aDecoder decodeObjectForKey:kPackDetailsModelUpdatedDate];
    self.insertedDate = [aDecoder decodeObjectForKey:kPackDetailsModelInsertedDate];
    self.descText = [aDecoder decodeObjectForKey:kPackDetailsModelDescText];
    self.packWeight = [aDecoder decodeObjectForKey:kPackDetailsModelPackWeight];
    self.deliveryType = [aDecoder decodeObjectForKey:kPackDetailsModelDeliveryType];
    self.insurancePrice = [aDecoder decodeObjectForKey:kPackDetailsModelInsurancePrice];
    self.takenAsap = [aDecoder decodeObjectForKey:kPackDetailsModelTakenAsap];
    self.senderPaid = [aDecoder decodeObjectForKey:kPackDetailsModelSenderPaid];
    self.packSizeId = [aDecoder decodeObjectForKey:kPackDetailsModelPackSizeId];
    self.lastOperationTime = [aDecoder decodeObjectForKey:kPackDetailsModelLastOperationTime];
    self.waitingTime = [aDecoder decodeObjectForKey:kPackDetailsModelWaitingTime];
    self.tips = [aDecoder decodeObjectForKey:kPackDetailsModelTips];
    self.tariffText = [aDecoder decodeObjectForKey:kDataTariffText];

    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_packFromText forKey:kPackDetailsModelPackFromText];
    [aCoder encodeObject:_petTypeText forKey:kPackDetailsModelPetTypeText];
    [aCoder encodeObject:_packOwnerEmail forKey:kPackDetailsModelPackOwnerEmail];
    [aCoder encodeObject:_deliveryBringTypeText forKey:kPackDetailsModelDeliveryBringTypeText];
    [aCoder encodeObject:_petCount forKey:kPackDetailsModelPetCount];
    [aCoder encodeObject:_courierProfitD forKey:kPackDetailsCourierProfit];
    [aCoder encodeObject:_packImages forKey:kPackDetailsModelPackImages];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kPackDetailsModelId];
    [aCoder encodeObject:_courierPaid forKey:kPackDetailsModelCourierPaid];
    [aCoder encodeObject:_updatedBy forKey:kPackDetailsModelUpdatedBy];
    [aCoder encodeObject:_receiverPhoneNumber forKey:kPackDetailsModelReceiverPhoneNumber];
    [aCoder encodeObject:_sendName forKey:kPackDetailsModelSendName];
    [aCoder encodeObject:_packOptionsText forKey:kPackDetailsModelPackOptionsText];
    [aCoder encodeObject:_deliveryTakeType forKey:kPackDetailsModelDeliveryTakeType];
    [aCoder encodeObject:_acceptedCourierRate forKey:kDataAcceptedCourierRate];

    [aCoder encodeObject:_packToText forKey:kPackDetailsModelPackToText];
    [aCoder encodeObject:_statusSender forKey:kPackDetailsModelStatusSender];
    [aCoder encodeObject:_acceptedCourierCell forKey:kPackDetailsModelAcceptedCourierCell];
    [aCoder encodeObject:_distanceTime forKey:kPackDetailsModelDistanceTime];
    [aCoder encodeObject:_acceptedCourierAvailStatus forKey:kDataAcceptedCourierAvailStatus];
    [aCoder encodeObject:_acceptedCourierSelfImg forKey:kDataAcceptedCourierSelfImg];
    [aCoder encodeObject:_itemValue forKey:kPackDetailsModelItemValue];
    [aCoder encodeObject:_cLastLng forKey:kDataCLastLng];
    [aCoder encodeObject:_acceptedCourierName forKey:kPackDetailsModelAcceptedCourierName];
    [aCoder encodeObject:_insertedBy forKey:kPackDetailsModelInsertedBy];
    [aCoder encodeObject:_imgTypeName forKey:kPackDetailsModelImgTypeName];
    [aCoder encodeObject:_takenAsapText forKey:kPackDetailsModelTakenAsapText];
    [aCoder encodeObject:_packBidders forKey:kPackDetailsModelPackBidders];
    [aCoder encodeObject:_sendCatText forKey:kPackDetailsModelSendCatText];
    [aCoder encodeObject:_packStatus forKey:kPackDetailsModelPackStatus];
    [aCoder encodeObject:_sendCatId forKey:kPackDetailsModelSendCatId];
    [aCoder encodeObject:_takenBringTypeText forKey:kPackDetailsModelTakenBringTypeText];
    [aCoder encodeObject:_packToLng forKey:kPackDetailsModelPackToLng];
    [aCoder encodeObject:_takenTypeText forKey:kPackDetailsModelTakenTypeText];
    [aCoder encodeObject:_packOwnerCell forKey:kPackDetailsModelPackOwnerCell];
    [aCoder encodeObject:_cLastLat forKey:kDataCLastLat];
    [aCoder encodeObject:_deliveryAsap forKey:kPackDetailsModelDeliveryAsap];
    [aCoder encodeObject:_packImg forKey:kPackDetailsModelPackImg];
    [aCoder encodeObject:_petIsVac forKey:kPackDetailsModelPetIsVac];
    [aCoder encodeObject:_packOwnerName forKey:kPackDetailsModelPackOwnerName];
    [aCoder encodeObject:_packOwnerSelfImage forKey:kPackDetailsModelPackOwnerSelfImg];
    [aCoder encodeObject:_packPrice forKey:kPackDetailsModelPackPrice];
    [aCoder encodeObject:_petIsVacText forKey:kPackDetailsModelPetIsVacText];
    [aCoder encodeObject:_imgId forKey:kPackDetailsModelImgId];
    [aCoder encodeObject:_statusCourier forKey:kPackDetailsModelStatusCourier];
    [aCoder encodeObject:_packSizeText forKey:kPackDetailsModelPackSizeText];
    [aCoder encodeObject:_leaveByDoor forKey:kPackDetailsModelLeaveByDoor];
    [aCoder encodeObject:_packOwnerSurname forKey:kPackDetailsModelPackOwnerSurname];
    [aCoder encodeObject:_petType forKey:kPackDetailsModelPetType];
    [aCoder encodeObject:_packFromLat forKey:kPackDetailsModelPackFromLat];
    [aCoder encodeObject:_distance forKey:kPackDetailsModelDistance];
    [aCoder encodeObject:_deliveryAsapText forKey:kPackDetailsModelDeliveryAsapText];
    [aCoder encodeObject:_takenType forKey:kPackDetailsModelTakenType];
    [aCoder encodeObject:_leaveByDoorText forKey:kPackDetailsModelLeaveByDoorText];
    [aCoder encodeObject:_acceptedCourierSurname forKey:kPackDetailsModelAcceptedCourierSurname];
    [aCoder encodeObject:_insuranceMode forKey:kPackDetailsModelInsuranceMode];
    [aCoder encodeObject:_deliveryTime forKey:kPackDetailsModelDeliveryTime];
    [aCoder encodeObject:_takenBringType forKey:kPackDetailsModelTakenBringType];
    [aCoder encodeObject:_deliveryTypeText forKey:kPackDetailsModelDeliveryTypeText];
    [aCoder encodeObject:_takenTime forKey:kPackDetailsModelTakenTime];
    [aCoder encodeObject:_acceptedCourierId forKey:kPackDetailsModelAcceptedCourierId];
    [aCoder encodeObject:_isInsurancePayed forKey:kPackDetailsModelIsInsurancePayed];
    [aCoder encodeObject:_receiverName forKey:kPackDetailsModelReceiverName];
    [aCoder encodeObject:_packFromLng forKey:kPackDetailsModelPackFromLng];
    [aCoder encodeObject:_acceptedCourierEmail forKey:kPackDetailsModelAcceptedCourierEmail];
    [aCoder encodeObject:_packOwnerId forKey:kPackDetailsModelPackOwnerId];
    [aCoder encodeObject:_packOptionsId forKey:kPackDetailsModelPackOptionsId];
    [aCoder encodeObject:_packToLat forKey:kPackDetailsModelPackToLat];
    [aCoder encodeObject:_updatedDate forKey:kPackDetailsModelUpdatedDate];
    [aCoder encodeObject:_insertedDate forKey:kPackDetailsModelInsertedDate];
    [aCoder encodeObject:_descText forKey:kPackDetailsModelDescText];
    [aCoder encodeObject:_packWeight forKey:kPackDetailsModelPackWeight];
    [aCoder encodeObject:_deliveryType forKey:kPackDetailsModelDeliveryType];
    [aCoder encodeObject:_insurancePrice forKey:kPackDetailsModelInsurancePrice];
    [aCoder encodeObject:_takenAsap forKey:kPackDetailsModelTakenAsap];
    [aCoder encodeObject:_senderPaid forKey:kPackDetailsModelSenderPaid];
    [aCoder encodeObject:_packSizeId forKey:kPackDetailsModelPackSizeId];
    [aCoder encodeObject:_lastOperationTime forKey:kPackDetailsModelLastOperationTime];
    [aCoder encodeObject:_waitingTime forKey:kPackDetailsModelWaitingTime];
    [aCoder encodeObject:_tips forKey:kPackDetailsModelTips];
    [aCoder encodeObject:_tariffText forKey:kDataTariffText];

}

- (id)copyWithZone:(NSZone *)zone {
    
    PackDetailsModel *copy = [[PackDetailsModel alloc] init];

    if (copy) {
        copy.packFromText = [self.packFromText copyWithZone:zone];
        copy.petTypeText = [self.petTypeText copyWithZone:zone];
        copy.packOwnerEmail = [self.packOwnerEmail copyWithZone:zone];
        copy.deliveryBringTypeText = [self.deliveryBringTypeText copyWithZone:zone];
        copy.petCount = [self.petCount copyWithZone:zone];
        copy.packImages = [self.packImages copyWithZone:zone];
        copy.courierProfitD = [self.courierProfitD copyWithZone:zone];
        copy.acceptedCourierAvailStatus = [self.acceptedCourierAvailStatus copyWithZone:zone];
        copy.acceptedCourierSelfImg = [self.acceptedCourierSelfImg copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.courierPaid = [self.courierPaid copyWithZone:zone];
        copy.updatedBy = [self.updatedBy copyWithZone:zone];
        copy.acceptedCourierRate = [self.acceptedCourierRate copyWithZone:zone];
        copy.receiverPhoneNumber = [self.receiverPhoneNumber copyWithZone:zone];
        copy.sendName = [self.sendName copyWithZone:zone];
        copy.packOptionsText = [self.packOptionsText copyWithZone:zone];
        copy.deliveryTakeType = [self.deliveryTakeType copyWithZone:zone];
        copy.packToText = [self.packToText copyWithZone:zone];
        copy.statusSender = [self.statusSender copyWithZone:zone];
        copy.acceptedCourierCell = [self.acceptedCourierCell copyWithZone:zone];
        copy.distanceTime = [self.distanceTime copyWithZone:zone];
        copy.itemValue = [self.itemValue copyWithZone:zone];
        copy.cLastLng = [self.cLastLng copyWithZone:zone];
        copy.acceptedCourierName = [self.acceptedCourierName copyWithZone:zone];
        copy.insertedBy = [self.insertedBy copyWithZone:zone];
        copy.imgTypeName = [self.imgTypeName copyWithZone:zone];
        copy.takenAsapText = [self.takenAsapText copyWithZone:zone];
        copy.packBidders = [self.packBidders copyWithZone:zone];
        copy.sendCatText = [self.sendCatText copyWithZone:zone];
        copy.packStatus = [self.packStatus copyWithZone:zone];
        copy.sendCatId = [self.sendCatId copyWithZone:zone];
        copy.takenBringTypeText = [self.takenBringTypeText copyWithZone:zone];
        copy.packToLng = [self.packToLng copyWithZone:zone];
        copy.takenTypeText = [self.takenTypeText copyWithZone:zone];
        copy.packOwnerCell = [self.packOwnerCell copyWithZone:zone];
        copy.cLastLat = [self.cLastLat copyWithZone:zone];
        copy.deliveryAsap = [self.deliveryAsap copyWithZone:zone];
        copy.packImg = [self.packImg copyWithZone:zone];
        copy.petIsVac = [self.petIsVac copyWithZone:zone];
        copy.packOwnerName = [self.packOwnerName copyWithZone:zone];
        copy.packOwnerSelfImage = [self.packOwnerSelfImage copyWithZone:zone];
        copy.packPrice = [self.packPrice copyWithZone:zone];
        copy.petIsVacText = [self.petIsVacText copyWithZone:zone];
        copy.imgId = [self.imgId copyWithZone:zone];
        copy.statusCourier = [self.statusCourier copyWithZone:zone];
        copy.packSizeText = [self.packSizeText copyWithZone:zone];
        copy.leaveByDoor = [self.leaveByDoor copyWithZone:zone];
        copy.packOwnerSurname = [self.packOwnerSurname copyWithZone:zone];
        copy.petType = [self.petType copyWithZone:zone];
        copy.packFromLat = [self.packFromLat copyWithZone:zone];
        copy.distance = [self.distance copyWithZone:zone];
        copy.deliveryAsapText = [self.deliveryAsapText copyWithZone:zone];
        copy.takenType = [self.takenType copyWithZone:zone];
        copy.leaveByDoorText = [self.leaveByDoorText copyWithZone:zone];
        copy.acceptedCourierSurname = [self.acceptedCourierSurname copyWithZone:zone];
        copy.insuranceMode = [self.insuranceMode copyWithZone:zone];
        copy.deliveryTime = [self.deliveryTime copyWithZone:zone];
        copy.takenBringType = [self.takenBringType copyWithZone:zone];
        copy.deliveryTypeText = [self.deliveryTypeText copyWithZone:zone];
        copy.takenTime = [self.takenTime copyWithZone:zone];
        copy.acceptedCourierId = [self.acceptedCourierId copyWithZone:zone];
        copy.isInsurancePayed = [self.isInsurancePayed copyWithZone:zone];
        copy.receiverName = [self.receiverName copyWithZone:zone];
        copy.packFromLng = [self.packFromLng copyWithZone:zone];
        copy.acceptedCourierEmail = [self.acceptedCourierEmail copyWithZone:zone];
        copy.packOwnerId = [self.packOwnerId copyWithZone:zone];
        copy.packOptionsId = [self.packOptionsId copyWithZone:zone];
        copy.packToLat = [self.packToLat copyWithZone:zone];
        copy.updatedDate = [self.updatedDate copyWithZone:zone];
        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.descText = [self.descText copyWithZone:zone];
        copy.packWeight = [self.packWeight copyWithZone:zone];
        copy.deliveryType = [self.deliveryType copyWithZone:zone];
        copy.insurancePrice = [self.insurancePrice copyWithZone:zone];
        copy.takenAsap = [self.takenAsap copyWithZone:zone];
        copy.senderPaid = [self.senderPaid copyWithZone:zone];
        copy.packSizeId = [self.packSizeId copyWithZone:zone];
        copy.lastOperationTime = [self.lastOperationTime copyWithZone:zone];
        copy.waitingTime = [self.waitingTime copyWithZone:zone];
        copy.tips = [self.tips copyWithZone:zone];
        copy.tariffText = [self.tariffText copyWithZone:zone];
    }
    
    return copy;
}


@end
