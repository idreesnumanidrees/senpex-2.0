//
//  PackImages.h
//
//  Created by   on 2017/06/15
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PackImages : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *insertedDate;
@property (nonatomic, strong) NSString *isDefault;
@property (nonatomic, strong) NSString *packImg;
@property (nonatomic, strong) NSString *packImagesIdentifier;
@property (nonatomic, strong) NSString *imgType;
@property (nonatomic, strong) NSString *packId;
@property (nonatomic, strong) NSString *imgTypeName;
@property (nonatomic, strong) NSString *userId;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
