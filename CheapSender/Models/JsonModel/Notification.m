//
//  Notification.m
//
//  Created by   on 2017/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "Notification.h"


NSString *const kNotificationLinkEnabled = @"link_enabled";
NSString *const kNotificationInsertedDate = @"inserted_date";
NSString *const kNotificationIsRead = @"is_read";
NSString *const kNotificationNotText = @"not_text";
NSString *const kNotificationId = @"id";
NSString *const kNotificationUserId = @"user_id";
NSString *const kNotificationLinkModule = @"link_module";
NSString *const kNotificationLinkId = @"link_id";
NSString *const kNotificationNotStatus = @"not_status";


@interface Notification ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Notification

@synthesize linkEnabled = _linkEnabled;
@synthesize insertedDate = _insertedDate;
@synthesize isRead = _isRead;


@synthesize notText = _notText;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize userId = _userId;
@synthesize linkModule = _linkModule;
@synthesize linkId = _linkId;
@synthesize notStatus = _notStatus;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.linkEnabled = [self objectOrNilForKey:kNotificationLinkEnabled fromDictionary:dict];
            self.insertedDate = [self objectOrNilForKey:kNotificationInsertedDate fromDictionary:dict];
            self.isRead = [self objectOrNilForKey:kNotificationIsRead fromDictionary:dict];
            self.notText = [self objectOrNilForKey:kNotificationNotText fromDictionary:dict];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kNotificationId fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kNotificationUserId fromDictionary:dict];
            self.linkModule = [self objectOrNilForKey:kNotificationLinkModule fromDictionary:dict];
            self.linkId = [self objectOrNilForKey:kNotificationLinkId fromDictionary:dict];
            self.notStatus = [self objectOrNilForKey:kNotificationNotStatus fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.linkEnabled forKey:kNotificationLinkEnabled];
    [mutableDict setValue:self.insertedDate forKey:kNotificationInsertedDate];
    [mutableDict setValue:self.isRead forKey:kNotificationIsRead];
    [mutableDict setValue:self.notText forKey:kNotificationNotText];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kNotificationId];
    [mutableDict setValue:self.userId forKey:kNotificationUserId];
    [mutableDict setValue:self.linkModule forKey:kNotificationLinkModule];
    [mutableDict setValue:self.linkId forKey:kNotificationLinkId];
    [mutableDict setValue:self.notStatus forKey:kNotificationNotStatus];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.linkEnabled = [aDecoder decodeObjectForKey:kNotificationLinkEnabled];
    self.insertedDate = [aDecoder decodeObjectForKey:kNotificationInsertedDate];
    self.isRead = [aDecoder decodeObjectForKey:kNotificationIsRead];
    self.notText = [aDecoder decodeObjectForKey:kNotificationNotText];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kNotificationId];
    self.userId = [aDecoder decodeObjectForKey:kNotificationUserId];
    self.linkModule = [aDecoder decodeObjectForKey:kNotificationLinkModule];
    self.linkId = [aDecoder decodeObjectForKey:kNotificationLinkId];
    self.notStatus = [aDecoder decodeObjectForKey:kNotificationNotStatus];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_linkEnabled forKey:kNotificationLinkEnabled];
    [aCoder encodeObject:_insertedDate forKey:kNotificationInsertedDate];
    [aCoder encodeObject:_isRead forKey:kNotificationIsRead];
    [aCoder encodeObject:_notText forKey:kNotificationNotText];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kNotificationId];
    [aCoder encodeObject:_userId forKey:kNotificationUserId];
    [aCoder encodeObject:_linkModule forKey:kNotificationLinkModule];
    [aCoder encodeObject:_linkId forKey:kNotificationLinkId];
    [aCoder encodeObject:_notStatus forKey:kNotificationNotStatus];
}

- (id)copyWithZone:(NSZone *)zone {
    Notification *copy = [[Notification alloc] init];
    
    if (copy) {

        copy.linkEnabled = [self.linkEnabled copyWithZone:zone];
        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.isRead = [self.isRead copyWithZone:zone];
        copy.notText = [self.notText copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.linkModule = [self.linkModule copyWithZone:zone];
        copy.linkId = [self.linkId copyWithZone:zone];
        copy.notStatus = [self.notStatus copyWithZone:zone];
    }
    
    return copy;
}


@end
