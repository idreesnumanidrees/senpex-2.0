//
//  PackImages.m
//
//  Created by   on 2017/06/15
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "PackImages.h"


NSString *const kPackImagesInsertedDate = @"inserted_date";
NSString *const kPackImagesIsDefault = @"is_default";
NSString *const kPackImagesPackImg = @"pack_img";
NSString *const kPackImagesId = @"id";
NSString *const kPackImagesImgType = @"img_type";
NSString *const kPackImagesPackId = @"pack_id";
NSString *const kPackImagesImgTypeName = @"img_type_name";
NSString *const kPackImagesUserId = @"user_id";


@interface PackImages ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation PackImages

@synthesize insertedDate = _insertedDate;
@synthesize isDefault = _isDefault;
@synthesize packImg = _packImg;
@synthesize packImagesIdentifier = _packImagesIdentifier;
@synthesize imgType = _imgType;
@synthesize packId = _packId;
@synthesize imgTypeName = _imgTypeName;
@synthesize userId = _userId;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.insertedDate = [self objectOrNilForKey:kPackImagesInsertedDate fromDictionary:dict];
            self.isDefault = [self objectOrNilForKey:kPackImagesIsDefault fromDictionary:dict];
            self.packImg = [self objectOrNilForKey:kPackImagesPackImg fromDictionary:dict];
            self.packImagesIdentifier = [self objectOrNilForKey:kPackImagesId fromDictionary:dict];
            self.imgType = [self objectOrNilForKey:kPackImagesImgType fromDictionary:dict];
            self.packId = [self objectOrNilForKey:kPackImagesPackId fromDictionary:dict];
            self.imgTypeName = [self objectOrNilForKey:kPackImagesImgTypeName fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kPackImagesUserId fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.insertedDate forKey:kPackImagesInsertedDate];
    [mutableDict setValue:self.isDefault forKey:kPackImagesIsDefault];
    [mutableDict setValue:self.packImg forKey:kPackImagesPackImg];
    [mutableDict setValue:self.packImagesIdentifier forKey:kPackImagesId];
    [mutableDict setValue:self.imgType forKey:kPackImagesImgType];
    [mutableDict setValue:self.packId forKey:kPackImagesPackId];
    [mutableDict setValue:self.imgTypeName forKey:kPackImagesImgTypeName];
    [mutableDict setValue:self.userId forKey:kPackImagesUserId];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.insertedDate = [aDecoder decodeObjectForKey:kPackImagesInsertedDate];
    self.isDefault = [aDecoder decodeObjectForKey:kPackImagesIsDefault];
    self.packImg = [aDecoder decodeObjectForKey:kPackImagesPackImg];
    self.packImagesIdentifier = [aDecoder decodeObjectForKey:kPackImagesId];
    self.imgType = [aDecoder decodeObjectForKey:kPackImagesImgType];
    self.packId = [aDecoder decodeObjectForKey:kPackImagesPackId];
    self.imgTypeName = [aDecoder decodeObjectForKey:kPackImagesImgTypeName];
    self.userId = [aDecoder decodeObjectForKey:kPackImagesUserId];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_insertedDate forKey:kPackImagesInsertedDate];
    [aCoder encodeObject:_isDefault forKey:kPackImagesIsDefault];
    [aCoder encodeObject:_packImg forKey:kPackImagesPackImg];
    [aCoder encodeObject:_packImagesIdentifier forKey:kPackImagesId];
    [aCoder encodeObject:_imgType forKey:kPackImagesImgType];
    [aCoder encodeObject:_packId forKey:kPackImagesPackId];
    [aCoder encodeObject:_imgTypeName forKey:kPackImagesImgTypeName];
    [aCoder encodeObject:_userId forKey:kPackImagesUserId];
}

- (id)copyWithZone:(NSZone *)zone {
    PackImages *copy = [[PackImages alloc] init];
    
    
    
    if (copy) {

        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.isDefault = [self.isDefault copyWithZone:zone];
        copy.packImg = [self.packImg copyWithZone:zone];
        copy.packImagesIdentifier = [self.packImagesIdentifier copyWithZone:zone];
        copy.imgType = [self.imgType copyWithZone:zone];
        copy.packId = [self.packId copyWithZone:zone];
        copy.imgTypeName = [self.imgTypeName copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
    }
    
    return copy;
}


@end
