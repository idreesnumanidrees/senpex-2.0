//
//  Notification.h
//
//  Created by   on 2017/07/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface Notification : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *linkEnabled;
@property (nonatomic, strong) NSString *insertedDate;
@property (nonatomic, strong) NSString *isRead;
@property (nonatomic, strong) NSString *notText;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *linkModule;
@property (nonatomic, strong) NSString *linkId;
@property (nonatomic, strong) NSString *notStatus;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
