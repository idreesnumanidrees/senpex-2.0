//
//  BankAccountDetail.m
//
//  Created by   on 2017/07/21
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "BankAccountDetail.h"


NSString *const kBankAccountDetailPayoutsEnabled = @"payouts_enabled";
NSString *const kBankAccountDetailVerifiedMsg = @"verified_msg";
NSString *const kBankAccountDetailIsVerified = @"is_verified";
NSString *const kBankAccountDetailId = @"id";
NSString *const kBankAccountDetailAccdate = @"accdate";
NSString *const kBankAccountDetailState = @"state";
NSString *const kBankAccountDetailAccountNumber = @"account_number";
NSString *const kBankAccountDetailUserId = @"user_id";
NSString *const kBankAccountDetailLine1 = @"line1";
NSString *const kBankAccountDetailRouteNumber = @"route_number";
NSString *const kBankAccountDetailCity = @"city";
NSString *const kBankAccountDetailPostalCode = @"postal_code";
NSString *const kBankAccountDetailLine2 = @"line2";


@interface BankAccountDetail ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation BankAccountDetail

@synthesize payoutsEnabled = _payoutsEnabled;
@synthesize verifiedMsg = _verifiedMsg;
@synthesize isVerified = _isVerified;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize accdate = _accdate;
@synthesize state = _state;
@synthesize accountNumber = _accountNumber;
@synthesize userId = _userId;
@synthesize line1 = _line1;
@synthesize routeNumber = _routeNumber;
@synthesize city = _city;
@synthesize postalCode = _postalCode;
@synthesize line2 = _line2;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.payoutsEnabled = [self objectOrNilForKey:kBankAccountDetailPayoutsEnabled fromDictionary:dict];
            self.verifiedMsg = [self objectOrNilForKey:kBankAccountDetailVerifiedMsg fromDictionary:dict];
            self.isVerified = [self objectOrNilForKey:kBankAccountDetailIsVerified fromDictionary:dict];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kBankAccountDetailId fromDictionary:dict];
            self.accdate = [self objectOrNilForKey:kBankAccountDetailAccdate fromDictionary:dict];
            self.state = [self objectOrNilForKey:kBankAccountDetailState fromDictionary:dict];
            self.accountNumber = [self objectOrNilForKey:kBankAccountDetailAccountNumber fromDictionary:dict];
            self.userId = [self objectOrNilForKey:kBankAccountDetailUserId fromDictionary:dict];
            self.line1 = [self objectOrNilForKey:kBankAccountDetailLine1 fromDictionary:dict];
            self.routeNumber = [self objectOrNilForKey:kBankAccountDetailRouteNumber fromDictionary:dict];
            self.city = [self objectOrNilForKey:kBankAccountDetailCity fromDictionary:dict];
            self.postalCode = [self objectOrNilForKey:kBankAccountDetailPostalCode fromDictionary:dict];
            self.line2 = [self objectOrNilForKey:kBankAccountDetailLine2 fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.payoutsEnabled forKey:kBankAccountDetailPayoutsEnabled];
    [mutableDict setValue:self.verifiedMsg forKey:kBankAccountDetailVerifiedMsg];
    [mutableDict setValue:self.isVerified forKey:kBankAccountDetailIsVerified];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kBankAccountDetailId];
    [mutableDict setValue:self.accdate forKey:kBankAccountDetailAccdate];
    [mutableDict setValue:self.state forKey:kBankAccountDetailState];
    [mutableDict setValue:self.accountNumber forKey:kBankAccountDetailAccountNumber];
    [mutableDict setValue:self.userId forKey:kBankAccountDetailUserId];
    [mutableDict setValue:self.line1 forKey:kBankAccountDetailLine1];
    [mutableDict setValue:self.routeNumber forKey:kBankAccountDetailRouteNumber];
    [mutableDict setValue:self.city forKey:kBankAccountDetailCity];
    [mutableDict setValue:self.postalCode forKey:kBankAccountDetailPostalCode];
    [mutableDict setValue:self.line2 forKey:kBankAccountDetailLine2];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.payoutsEnabled = [aDecoder decodeObjectForKey:kBankAccountDetailPayoutsEnabled];
    self.verifiedMsg = [aDecoder decodeObjectForKey:kBankAccountDetailVerifiedMsg];
    self.isVerified = [aDecoder decodeObjectForKey:kBankAccountDetailIsVerified];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kBankAccountDetailId];
    self.accdate = [aDecoder decodeObjectForKey:kBankAccountDetailAccdate];
    self.state = [aDecoder decodeObjectForKey:kBankAccountDetailState];
    self.accountNumber = [aDecoder decodeObjectForKey:kBankAccountDetailAccountNumber];
    self.userId = [aDecoder decodeObjectForKey:kBankAccountDetailUserId];
    self.line1 = [aDecoder decodeObjectForKey:kBankAccountDetailLine1];
    self.routeNumber = [aDecoder decodeObjectForKey:kBankAccountDetailRouteNumber];
    self.city = [aDecoder decodeObjectForKey:kBankAccountDetailCity];
    self.postalCode = [aDecoder decodeObjectForKey:kBankAccountDetailPostalCode];
    self.line2 = [aDecoder decodeObjectForKey:kBankAccountDetailLine2];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_payoutsEnabled forKey:kBankAccountDetailPayoutsEnabled];
    [aCoder encodeObject:_verifiedMsg forKey:kBankAccountDetailVerifiedMsg];
    [aCoder encodeObject:_isVerified forKey:kBankAccountDetailIsVerified];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kBankAccountDetailId];
    [aCoder encodeObject:_accdate forKey:kBankAccountDetailAccdate];
    [aCoder encodeObject:_state forKey:kBankAccountDetailState];
    [aCoder encodeObject:_accountNumber forKey:kBankAccountDetailAccountNumber];
    [aCoder encodeObject:_userId forKey:kBankAccountDetailUserId];
    [aCoder encodeObject:_line1 forKey:kBankAccountDetailLine1];
    [aCoder encodeObject:_routeNumber forKey:kBankAccountDetailRouteNumber];
    [aCoder encodeObject:_city forKey:kBankAccountDetailCity];
    [aCoder encodeObject:_postalCode forKey:kBankAccountDetailPostalCode];
    [aCoder encodeObject:_line2 forKey:kBankAccountDetailLine2];
}

- (id)copyWithZone:(NSZone *)zone {
    BankAccountDetail *copy = [[BankAccountDetail alloc] init];
    
    
    
    if (copy) {

        copy.payoutsEnabled = [self.payoutsEnabled copyWithZone:zone];
        copy.verifiedMsg = [self.verifiedMsg copyWithZone:zone];
        copy.isVerified = [self.isVerified copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.accdate = [self.accdate copyWithZone:zone];
        copy.state = [self.state copyWithZone:zone];
        copy.accountNumber = [self.accountNumber copyWithZone:zone];
        copy.userId = [self.userId copyWithZone:zone];
        copy.line1 = [self.line1 copyWithZone:zone];
        copy.routeNumber = [self.routeNumber copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
        copy.postalCode = [self.postalCode copyWithZone:zone];
        copy.line2 = [self.line2 copyWithZone:zone];
    }
    
    return copy;
}


@end
