//
//  UserModel.m
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "UserModel.h"
#import "UserProfiles.h"
#import "GenConfig.h"


NSString *const kUserApproveGuid = @"approve_guid";
NSString *const kUserCell = @"cell";
NSString *const kUserSsnNumber = @"ssn_number";
NSString *const kUserUseOffical = @"use_offical";
NSString *const kUserLogDeviceId = @"log_device_id";
NSString *const kUserId = @"id";
NSString *const kUserLastLat = @"last_lat";
NSString *const kUserCountry = @"country";
NSString *const kUserQstDisability = @"qst_disability";
NSString *const kUserUserRate = @"user_rate";
NSString *const kUserUpdatedBy = @"updated_by";
NSString *const kUserIsDisabled = @"is_disabled";
NSString *const kUserUseLocation = @"use_location";
NSString *const kUserLogSessionKey = @"log_session_key";
NSString *const kUserApprovedByAdmin = @"approved_by_admin";
NSString *const kUserMeritalStatus = @"merital_status";
NSString *const kUserAvailStatus = @"avail_status";
NSString *const kUserQstCriminal = @"qst_criminal";
NSString *const kUserIdcatdState = @"idcatd_state";
NSString *const kUserUserProfiles = @"user_profiles";
NSString *const kUserEmail = @"email";
NSString *const kUserIdcardExpDate = @"idcard_exp_date";
NSString *const kUserLogUserArgent = @"log_user_argent";
NSString *const kUserInsertedDate = @"inserted_date";
NSString *const kUserSurname = @"surname";
NSString *const kUserIdcardNumber = @"idcard_number";
NSString *const kUserUpdatedDate = @"updated_date";
NSString *const kUserName = @"name";
NSString *const kUserLastLng = @"last_lng";
NSString *const kUserQstSexsual = @"qst_sexsual";
NSString *const kUserReferalName = @"referal_name";
NSString *const kUserCity = @"city";
NSString *const kUserSelfImg = @"self_img";
NSString *const kUserSystemRow = @"system_row";
NSString *const kUserLastSeenDate = @"last_seen_date";
NSString *const kUserAddressOffical = @"address_offical";
NSString *const kUserIdcardImg2 = @"idcard_img2";
NSString *const kUserGender = @"gender";
NSString *const kUserIsApproved = @"is_approved";
NSString *const kUserIdcardImg1 = @"idcard_img1";
NSString *const kUserAddressActual = @"address_actual";
NSString *const kUserPaypalEmail = @"paypal_email";
NSString *const kUserReferalCell = @"referal_cell";
NSString *const kUserLogDate = @"log_date";
NSString *const kUserLastLocationDate = @"last_location_date";
NSString *const kUserPassword = @"password";
NSString *const kUserInsertedBy = @"inserted_by";
NSString *const kUserQstHealth = @"qst_health";
NSString *const kUserGenConfig = @"gen_config";


@interface UserModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation UserModel

@synthesize approveGuid = _approveGuid;
@synthesize cell = _cell;
@synthesize ssnNumber = _ssnNumber;
@synthesize useOffical = _useOffical;
@synthesize logDeviceId = _logDeviceId;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize lastLat = _lastLat;
@synthesize country = _country;
@synthesize qstDisability = _qstDisability;
@synthesize userRate = _userRate;
@synthesize updatedBy = _updatedBy;
@synthesize isDisabled = _isDisabled;
@synthesize useLocation = _useLocation;
@synthesize logSessionKey = _logSessionKey;
@synthesize approvedByAdmin = _approvedByAdmin;
@synthesize meritalStatus = _meritalStatus;
@synthesize availStatus = _availStatus;
@synthesize qstCriminal = _qstCriminal;
@synthesize idcatdState = _idcatdState;
@synthesize userProfiles = _userProfiles;
@synthesize email = _email;
@synthesize idcardExpDate = _idcardExpDate;
@synthesize logUserArgent = _logUserArgent;
@synthesize insertedDate = _insertedDate;
@synthesize surname = _surname;
@synthesize idcardNumber = _idcardNumber;
@synthesize updatedDate = _updatedDate;
@synthesize name = _name;
@synthesize lastLng = _lastLng;
@synthesize qstSexsual = _qstSexsual;
@synthesize referalName = _referalName;
@synthesize city = _city;
@synthesize selfImg = _selfImg;
@synthesize systemRow = _systemRow;
@synthesize lastSeenDate = _lastSeenDate;
@synthesize addressOffical = _addressOffical;
@synthesize idcardImg2 = _idcardImg2;
@synthesize gender = _gender;
@synthesize isApproved = _isApproved;
@synthesize idcardImg1 = _idcardImg1;
@synthesize addressActual = _addressActual;
@synthesize paypalEmail = _paypalEmail;
@synthesize referalCell = _referalCell;
@synthesize logDate = _logDate;
@synthesize lastLocationDate = _lastLocationDate;
@synthesize password = _password;
@synthesize insertedBy = _insertedBy;
@synthesize qstHealth = _qstHealth;
@synthesize genConfig = _genConfig;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.approveGuid = [self objectOrNilForKey:kUserApproveGuid fromDictionary:dict];
            self.cell = [self objectOrNilForKey:kUserCell fromDictionary:dict];
            self.ssnNumber = [self objectOrNilForKey:kUserSsnNumber fromDictionary:dict];
        self.internalBaseClassIdentifier = [self objectOrNilForKey:kUserId fromDictionary:dict];
            self.useOffical = [self objectOrNilForKey:kUserUseOffical fromDictionary:dict];
            self.logDeviceId = [self objectOrNilForKey:kUserLogDeviceId fromDictionary:dict];
            self.dataIdentifier = [self objectOrNilForKey:kUserId fromDictionary:dict];
            self.lastLat = [self objectOrNilForKey:kUserLastLat fromDictionary:dict];
            self.country = [self objectOrNilForKey:kUserCountry fromDictionary:dict];
            self.qstDisability = [self objectOrNilForKey:kUserQstDisability fromDictionary:dict];
            self.userRate = [self objectOrNilForKey:kUserUserRate fromDictionary:dict];
            self.updatedBy = [self objectOrNilForKey:kUserUpdatedBy fromDictionary:dict];
            self.isDisabled = [self objectOrNilForKey:kUserIsDisabled fromDictionary:dict];
            self.useLocation = [self objectOrNilForKey:kUserUseLocation fromDictionary:dict];
            self.logSessionKey = [self objectOrNilForKey:kUserLogSessionKey fromDictionary:dict];
            self.approvedByAdmin = [self objectOrNilForKey:kUserApprovedByAdmin fromDictionary:dict];
            self.meritalStatus = [self objectOrNilForKey:kUserMeritalStatus fromDictionary:dict];
            self.availStatus = [self objectOrNilForKey:kUserAvailStatus fromDictionary:dict];
            self.qstCriminal = [self objectOrNilForKey:kUserQstCriminal fromDictionary:dict];
            self.idcatdState = [self objectOrNilForKey:kUserIdcatdState fromDictionary:dict];
    NSObject *receivedUserProfiles = [dict objectForKey:kUserUserProfiles];
    NSMutableArray *parsedUserProfiles = [NSMutableArray array];
    
    if ([receivedUserProfiles isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedUserProfiles) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedUserProfiles addObject:[UserProfiles modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedUserProfiles isKindOfClass:[NSDictionary class]]) {
       [parsedUserProfiles addObject:[UserProfiles modelObjectWithDictionary:(NSDictionary *)receivedUserProfiles]];
    }

    self.userProfiles = [NSArray arrayWithArray:parsedUserProfiles];
            self.email = [self objectOrNilForKey:kUserEmail fromDictionary:dict];
            self.idcardExpDate = [self objectOrNilForKey:kUserIdcardExpDate fromDictionary:dict];
            self.logUserArgent = [self objectOrNilForKey:kUserLogUserArgent fromDictionary:dict];
            self.insertedDate = [self objectOrNilForKey:kUserInsertedDate fromDictionary:dict];
            self.surname = [self objectOrNilForKey:kUserSurname fromDictionary:dict];
            self.idcardNumber = [self objectOrNilForKey:kUserIdcardNumber fromDictionary:dict];
            self.updatedDate = [self objectOrNilForKey:kUserUpdatedDate fromDictionary:dict];
            self.name = [self objectOrNilForKey:kUserName fromDictionary:dict];
            self.lastLng = [self objectOrNilForKey:kUserLastLng fromDictionary:dict];
            self.qstSexsual = [self objectOrNilForKey:kUserQstSexsual fromDictionary:dict];
            self.referalName = [self objectOrNilForKey:kUserReferalName fromDictionary:dict];
            self.city = [self objectOrNilForKey:kUserCity fromDictionary:dict];
            self.selfImg = [self objectOrNilForKey:kUserSelfImg fromDictionary:dict];
            self.systemRow = [self objectOrNilForKey:kUserSystemRow fromDictionary:dict];
            self.lastSeenDate = [self objectOrNilForKey:kUserLastSeenDate fromDictionary:dict];
            self.addressOffical = [self objectOrNilForKey:kUserAddressOffical fromDictionary:dict];
            self.idcardImg2 = [self objectOrNilForKey:kUserIdcardImg2 fromDictionary:dict];
            self.gender = [self objectOrNilForKey:kUserGender fromDictionary:dict];
            self.isApproved = [self objectOrNilForKey:kUserIsApproved fromDictionary:dict];
            self.idcardImg1 = [self objectOrNilForKey:kUserIdcardImg1 fromDictionary:dict];
            self.addressActual = [self objectOrNilForKey:kUserAddressActual fromDictionary:dict];
            self.paypalEmail = [self objectOrNilForKey:kUserPaypalEmail fromDictionary:dict];
            self.referalCell = [self objectOrNilForKey:kUserReferalCell fromDictionary:dict];
            self.logDate = [self objectOrNilForKey:kUserLogDate fromDictionary:dict];
            self.lastLocationDate = [self objectOrNilForKey:kUserLastLocationDate fromDictionary:dict];
            self.password = [self objectOrNilForKey:kUserPassword fromDictionary:dict];
            self.insertedBy = [self objectOrNilForKey:kUserInsertedBy fromDictionary:dict];
            self.qstHealth = [self objectOrNilForKey:kUserQstHealth fromDictionary:dict];
            self.genConfig = [GenConfig modelObjectWithDictionary:[dict objectForKey:kUserGenConfig]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.approveGuid forKey:kUserApproveGuid];
    [mutableDict setValue:self.cell forKey:kUserCell];
    [mutableDict setValue:self.ssnNumber forKey:kUserSsnNumber];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kUserId];
    
    [mutableDict setValue:self.useOffical forKey:kUserUseOffical];
    [mutableDict setValue:self.logDeviceId forKey:kUserLogDeviceId];
    [mutableDict setValue:self.dataIdentifier forKey:kUserId];
    [mutableDict setValue:self.lastLat forKey:kUserLastLat];
    [mutableDict setValue:self.country forKey:kUserCountry];
    [mutableDict setValue:self.qstDisability forKey:kUserQstDisability];
    [mutableDict setValue:self.userRate forKey:kUserUserRate];
    [mutableDict setValue:self.updatedBy forKey:kUserUpdatedBy];
    [mutableDict setValue:self.isDisabled forKey:kUserIsDisabled];
    [mutableDict setValue:self.useLocation forKey:kUserUseLocation];
    [mutableDict setValue:self.logSessionKey forKey:kUserLogSessionKey];
    [mutableDict setValue:self.approvedByAdmin forKey:kUserApprovedByAdmin];
    [mutableDict setValue:self.meritalStatus forKey:kUserMeritalStatus];
    [mutableDict setValue:self.availStatus forKey:kUserAvailStatus];
    [mutableDict setValue:self.qstCriminal forKey:kUserQstCriminal];
    [mutableDict setValue:self.idcatdState forKey:kUserIdcatdState];
    NSMutableArray *tempArrayForUserProfiles = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.userProfiles) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForUserProfiles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForUserProfiles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForUserProfiles] forKey:kUserUserProfiles];
    [mutableDict setValue:self.email forKey:kUserEmail];
    [mutableDict setValue:self.idcardExpDate forKey:kUserIdcardExpDate];
    [mutableDict setValue:self.logUserArgent forKey:kUserLogUserArgent];
    [mutableDict setValue:self.insertedDate forKey:kUserInsertedDate];
    [mutableDict setValue:self.surname forKey:kUserSurname];
    [mutableDict setValue:self.idcardNumber forKey:kUserIdcardNumber];
    [mutableDict setValue:self.updatedDate forKey:kUserUpdatedDate];
    [mutableDict setValue:self.name forKey:kUserName];
    [mutableDict setValue:self.lastLng forKey:kUserLastLng];
    [mutableDict setValue:self.qstSexsual forKey:kUserQstSexsual];
    [mutableDict setValue:self.referalName forKey:kUserReferalName];
    [mutableDict setValue:self.city forKey:kUserCity];
    [mutableDict setValue:self.selfImg forKey:kUserSelfImg];
    [mutableDict setValue:self.systemRow forKey:kUserSystemRow];
    [mutableDict setValue:self.lastSeenDate forKey:kUserLastSeenDate];
    [mutableDict setValue:self.addressOffical forKey:kUserAddressOffical];
    [mutableDict setValue:self.idcardImg2 forKey:kUserIdcardImg2];
    [mutableDict setValue:self.gender forKey:kUserGender];
    [mutableDict setValue:self.isApproved forKey:kUserIsApproved];
    [mutableDict setValue:self.idcardImg1 forKey:kUserIdcardImg1];
    [mutableDict setValue:self.addressActual forKey:kUserAddressActual];
    [mutableDict setValue:self.paypalEmail forKey:kUserPaypalEmail];
    [mutableDict setValue:self.referalCell forKey:kUserReferalCell];
    [mutableDict setValue:self.logDate forKey:kUserLogDate];
    [mutableDict setValue:self.lastLocationDate forKey:kUserLastLocationDate];
    [mutableDict setValue:self.password forKey:kUserPassword];
    [mutableDict setValue:self.insertedBy forKey:kUserInsertedBy];
    [mutableDict setValue:self.qstHealth forKey:kUserQstHealth];
    [mutableDict setValue:[self.genConfig dictionaryRepresentation] forKey:kUserGenConfig];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.approveGuid = [aDecoder decodeObjectForKey:kUserApproveGuid];
    self.cell = [aDecoder decodeObjectForKey:kUserCell];
    self.ssnNumber = [aDecoder decodeObjectForKey:kUserSsnNumber];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kUserId];

    self.useOffical = [aDecoder decodeObjectForKey:kUserUseOffical];
    self.logDeviceId = [aDecoder decodeObjectForKey:kUserLogDeviceId];
    self.dataIdentifier = [aDecoder decodeObjectForKey:kUserId];
    self.lastLat = [aDecoder decodeObjectForKey:kUserLastLat];
    self.country = [aDecoder decodeObjectForKey:kUserCountry];
    self.qstDisability = [aDecoder decodeObjectForKey:kUserQstDisability];
    self.userRate = [aDecoder decodeObjectForKey:kUserUserRate];
    self.updatedBy = [aDecoder decodeObjectForKey:kUserUpdatedBy];
    self.isDisabled = [aDecoder decodeObjectForKey:kUserIsDisabled];
    self.useLocation = [aDecoder decodeObjectForKey:kUserUseLocation];
    self.logSessionKey = [aDecoder decodeObjectForKey:kUserLogSessionKey];
    self.approvedByAdmin = [aDecoder decodeObjectForKey:kUserApprovedByAdmin];
    self.meritalStatus = [aDecoder decodeObjectForKey:kUserMeritalStatus];
    self.availStatus = [aDecoder decodeObjectForKey:kUserAvailStatus];
    self.qstCriminal = [aDecoder decodeObjectForKey:kUserQstCriminal];
    self.idcatdState = [aDecoder decodeObjectForKey:kUserIdcatdState];
    self.userProfiles = [aDecoder decodeObjectForKey:kUserUserProfiles];
    self.email = [aDecoder decodeObjectForKey:kUserEmail];
    self.idcardExpDate = [aDecoder decodeObjectForKey:kUserIdcardExpDate];
    self.logUserArgent = [aDecoder decodeObjectForKey:kUserLogUserArgent];
    self.insertedDate = [aDecoder decodeObjectForKey:kUserInsertedDate];
    self.surname = [aDecoder decodeObjectForKey:kUserSurname];
    self.idcardNumber = [aDecoder decodeObjectForKey:kUserIdcardNumber];
    self.updatedDate = [aDecoder decodeObjectForKey:kUserUpdatedDate];
    self.name = [aDecoder decodeObjectForKey:kUserName];
    self.lastLng = [aDecoder decodeObjectForKey:kUserLastLng];
    self.qstSexsual = [aDecoder decodeObjectForKey:kUserQstSexsual];
    self.referalName = [aDecoder decodeObjectForKey:kUserReferalName];
    self.city = [aDecoder decodeObjectForKey:kUserCity];
    self.selfImg = [aDecoder decodeObjectForKey:kUserSelfImg];
    self.systemRow = [aDecoder decodeObjectForKey:kUserSystemRow];
    self.lastSeenDate = [aDecoder decodeObjectForKey:kUserLastSeenDate];
    self.addressOffical = [aDecoder decodeObjectForKey:kUserAddressOffical];
    self.idcardImg2 = [aDecoder decodeObjectForKey:kUserIdcardImg2];
    self.gender = [aDecoder decodeObjectForKey:kUserGender];
    self.isApproved = [aDecoder decodeObjectForKey:kUserIsApproved];
    self.idcardImg1 = [aDecoder decodeObjectForKey:kUserIdcardImg1];
    self.addressActual = [aDecoder decodeObjectForKey:kUserAddressActual];
    self.paypalEmail = [aDecoder decodeObjectForKey:kUserPaypalEmail];
    self.referalCell = [aDecoder decodeObjectForKey:kUserReferalCell];
    self.logDate = [aDecoder decodeObjectForKey:kUserLogDate];
    self.lastLocationDate = [aDecoder decodeObjectForKey:kUserLastLocationDate];
    self.password = [aDecoder decodeObjectForKey:kUserPassword];
    self.insertedBy = [aDecoder decodeObjectForKey:kUserInsertedBy];
    self.qstHealth = [aDecoder decodeObjectForKey:kUserQstHealth];
    self.genConfig = [aDecoder decodeObjectForKey:kUserGenConfig];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_approveGuid forKey:kUserApproveGuid];
    [aCoder encodeObject:_cell forKey:kUserCell];
    [aCoder encodeObject:_ssnNumber forKey:kUserSsnNumber];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kUserId];

    [aCoder encodeObject:_useOffical forKey:kUserUseOffical];
    [aCoder encodeObject:_logDeviceId forKey:kUserLogDeviceId];
    [aCoder encodeObject:_dataIdentifier forKey:kUserId];
    [aCoder encodeObject:_lastLat forKey:kUserLastLat];
    [aCoder encodeObject:_country forKey:kUserCountry];
    [aCoder encodeObject:_qstDisability forKey:kUserQstDisability];
    [aCoder encodeObject:_userRate forKey:kUserUserRate];
    [aCoder encodeObject:_updatedBy forKey:kUserUpdatedBy];
    [aCoder encodeObject:_isDisabled forKey:kUserIsDisabled];
    [aCoder encodeObject:_useLocation forKey:kUserUseLocation];
    [aCoder encodeObject:_logSessionKey forKey:kUserLogSessionKey];
    [aCoder encodeObject:_approvedByAdmin forKey:kUserApprovedByAdmin];
    [aCoder encodeObject:_meritalStatus forKey:kUserMeritalStatus];
    [aCoder encodeObject:_availStatus forKey:kUserAvailStatus];
    [aCoder encodeObject:_qstCriminal forKey:kUserQstCriminal];
    [aCoder encodeObject:_idcatdState forKey:kUserIdcatdState];
    [aCoder encodeObject:_userProfiles forKey:kUserUserProfiles];
    [aCoder encodeObject:_email forKey:kUserEmail];
    [aCoder encodeObject:_idcardExpDate forKey:kUserIdcardExpDate];
    [aCoder encodeObject:_logUserArgent forKey:kUserLogUserArgent];
    [aCoder encodeObject:_insertedDate forKey:kUserInsertedDate];
    [aCoder encodeObject:_surname forKey:kUserSurname];
    [aCoder encodeObject:_idcardNumber forKey:kUserIdcardNumber];
    [aCoder encodeObject:_updatedDate forKey:kUserUpdatedDate];
    [aCoder encodeObject:_name forKey:kUserName];
    [aCoder encodeObject:_lastLng forKey:kUserLastLng];
    [aCoder encodeObject:_qstSexsual forKey:kUserQstSexsual];
    [aCoder encodeObject:_referalName forKey:kUserReferalName];
    [aCoder encodeObject:_city forKey:kUserCity];
    [aCoder encodeObject:_selfImg forKey:kUserSelfImg];
    [aCoder encodeObject:_systemRow forKey:kUserSystemRow];
    [aCoder encodeObject:_lastSeenDate forKey:kUserLastSeenDate];
    [aCoder encodeObject:_addressOffical forKey:kUserAddressOffical];
    [aCoder encodeObject:_idcardImg2 forKey:kUserIdcardImg2];
    [aCoder encodeObject:_gender forKey:kUserGender];
    [aCoder encodeObject:_isApproved forKey:kUserIsApproved];
    [aCoder encodeObject:_idcardImg1 forKey:kUserIdcardImg1];
    [aCoder encodeObject:_addressActual forKey:kUserAddressActual];
    [aCoder encodeObject:_paypalEmail forKey:kUserPaypalEmail];
    [aCoder encodeObject:_referalCell forKey:kUserReferalCell];
    [aCoder encodeObject:_logDate forKey:kUserLogDate];
    [aCoder encodeObject:_lastLocationDate forKey:kUserLastLocationDate];
    [aCoder encodeObject:_password forKey:kUserPassword];
    [aCoder encodeObject:_insertedBy forKey:kUserInsertedBy];
    [aCoder encodeObject:_qstHealth forKey:kUserQstHealth];
    [aCoder encodeObject:_genConfig forKey:kUserGenConfig];
}

- (id)copyWithZone:(NSZone *)zone {
    UserModel *copy = [[UserModel alloc] init];
    
    
    
    if (copy) {

        copy.approveGuid = [self.approveGuid copyWithZone:zone];
        copy.cell = [self.cell copyWithZone:zone];
        copy.ssnNumber = [self.ssnNumber copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];

        copy.useOffical = [self.useOffical copyWithZone:zone];
        copy.logDeviceId = [self.logDeviceId copyWithZone:zone];
        copy.dataIdentifier = [self.dataIdentifier copyWithZone:zone];
        copy.lastLat = [self.lastLat copyWithZone:zone];
        copy.country = [self.country copyWithZone:zone];
        copy.qstDisability = [self.qstDisability copyWithZone:zone];
        copy.userRate = [self.userRate copyWithZone:zone];
        copy.updatedBy = [self.updatedBy copyWithZone:zone];
        copy.isDisabled = [self.isDisabled copyWithZone:zone];
        copy.useLocation = [self.useLocation copyWithZone:zone];
        copy.logSessionKey = [self.logSessionKey copyWithZone:zone];
        copy.approvedByAdmin = [self.approvedByAdmin copyWithZone:zone];
        copy.meritalStatus = [self.meritalStatus copyWithZone:zone];
        copy.availStatus = [self.availStatus copyWithZone:zone];
        copy.qstCriminal = [self.qstCriminal copyWithZone:zone];
        copy.idcatdState = [self.idcatdState copyWithZone:zone];
        copy.userProfiles = [self.userProfiles copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.idcardExpDate = [self.idcardExpDate copyWithZone:zone];
        copy.logUserArgent = [self.logUserArgent copyWithZone:zone];
        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.surname = [self.surname copyWithZone:zone];
        copy.idcardNumber = [self.idcardNumber copyWithZone:zone];
        copy.updatedDate = [self.updatedDate copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.lastLng = [self.lastLng copyWithZone:zone];
        copy.qstSexsual = [self.qstSexsual copyWithZone:zone];
        copy.referalName = [self.referalName copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
        copy.selfImg = [self.selfImg copyWithZone:zone];
        copy.systemRow = [self.systemRow copyWithZone:zone];
        copy.lastSeenDate = [self.lastSeenDate copyWithZone:zone];
        copy.addressOffical = [self.addressOffical copyWithZone:zone];
        copy.idcardImg2 = [self.idcardImg2 copyWithZone:zone];
        copy.gender = [self.gender copyWithZone:zone];
        copy.isApproved = [self.isApproved copyWithZone:zone];
        copy.idcardImg1 = [self.idcardImg1 copyWithZone:zone];
        copy.addressActual = [self.addressActual copyWithZone:zone];
        copy.paypalEmail = [self.paypalEmail copyWithZone:zone];
        copy.referalCell = [self.referalCell copyWithZone:zone];
        copy.logDate = [self.logDate copyWithZone:zone];
        copy.lastLocationDate = [self.lastLocationDate copyWithZone:zone];
        copy.password = [self.password copyWithZone:zone];
        copy.insertedBy = [self.insertedBy copyWithZone:zone];
        copy.qstHealth = [self.qstHealth copyWithZone:zone];
        copy.genConfig = [self.genConfig copyWithZone:zone];
    }
    
    return copy;
}


@end
