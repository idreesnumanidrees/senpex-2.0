//
//  CourierConversations.h
//
//  Created by   on 2017/08/03
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface CourierConversations : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *receiverName;
@property (nonatomic, strong) NSString *isBanned;
@property (nonatomic, strong) NSString *senderId;
@property (nonatomic, strong) NSString *senderImg;
@property (nonatomic, strong) NSString *viewId;
@property (nonatomic, strong) NSString *receiverImg;
@property (nonatomic, strong) NSString *lastMessageStatus;
@property (nonatomic, strong) NSString *viewName;
@property (nonatomic, strong) NSString *receiverSurname;
@property (nonatomic, strong) NSString *insertedDate;
@property (nonatomic, strong) NSString *viewImg;
@property (nonatomic, strong) NSString *receiverId;
@property (nonatomic, strong) NSString *senderSurname;
@property (nonatomic, strong) NSString *chatText;
@property (nonatomic, strong) NSString *senderName;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
