//
//  GetTransports.h
//
//  Created by   on 2017/07/10
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface GetTransports : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *transportModel;
@property (nonatomic, strong) NSString *transportMake;
@property (nonatomic, strong) NSString *plateNumber;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *transportColor;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *transportType;
@property (nonatomic, strong) NSString *transportYear;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
