//
//  PayoutHistory.h
//
//  Created by   on 2017/07/13
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface PayoutHistory : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, strong) NSString *paymentAmount;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *payoutDate;
@property (nonatomic, strong) NSString *isPaid;
@property (nonatomic, strong) NSString *deliveryCount;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
