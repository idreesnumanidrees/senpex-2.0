//
//  SPDistanceTime.h
//  CheapSender
//
//  Created by Idrees on 2017/06/14.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseObject.h"

@interface SPDistanceTime : BaseObject

@property (strong, nonatomic) NSString *distance;
@property (strong, nonatomic) NSString *duration;


@end
