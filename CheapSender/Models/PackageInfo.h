//
//  PackageInfo.h
//  CheapSender
//
//  Created by Idrees on 2017/08/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseObject.h"

@interface PackageInfo : BaseObject

@property (assign, nonatomic) int packId;
@property (assign, nonatomic) int timerValue;
@property (strong, nonatomic) NSDate *dateTime;

@end
