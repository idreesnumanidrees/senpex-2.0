//
//  CancelPackReasonModel.h
//  CheapSender
//
//  Created by Riyas Abdul Rahman on 5/2/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CancelPackReasonModel : NSObject

//To check if this reason is the selected one
@property (assign, nonatomic) BOOL isSelected;

//reason title property of NSString
@property (strong, nonatomic) NSString *reasonTitle;

@end
