//
//  UtilHelper.m
//  OTW
//
//  Created by admin on 1/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

#import "UtilHelper.h"
#import "Reachability.h"
#import "CSGeneralConstants.h"
#import "NSDate+Utilities.h"

@implementation UtilHelper

//Sets the value in NSUserDefaults of specific key
+ (void)saveAndUpdateNSUserDefaultWithObject:(id)object forKey:(NSString *)key
{
    [[NSUserDefaults standardUserDefaults] setObject:object forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

//Gets the value from NSUserDefaults of specific key
+ (id)getValueFromNSUserDefaultForKey:(NSString *)key
{
    
    id value =  [[NSUserDefaults standardUserDefaults] valueForKey:key];
    if(value == nil)
        value = @"-1";
    
    return value;
}

//Gets the value from NSUserDefaults of specific key
+ (id)getUserFromNSUserDefaultForKey:(NSString *)key
{
    
    id value =  [[NSUserDefaults standardUserDefaults] valueForKey:key];
//    if(value == nil)
//        value = @"-1";
    
    return value;
}


//Returns UIView object with corner curved according to specific radious, color and width
+ (UIView *)makeCornerCurved:(UIView *)view withRadius:(CGFloat)radius color:(UIColor *)color width:(float)width
{
    UIView *curvyView = view;
    curvyView.layer.cornerRadius = radius;
    curvyView.layer.masksToBounds = YES;
    curvyView.layer.borderWidth = width-1;
    curvyView.layer.borderColor = color.CGColor;
    return curvyView;
}

+ (UITextField *)makeBorderForTextField:(UITextField *)view color:(UIColor *)color width:(float)width
{
    UITextField *curvyView = view;
    curvyView.layer.masksToBounds = YES;
    curvyView.layer.borderWidth = width;
    curvyView.layer.borderColor = color.CGColor;
    return curvyView;
}

+ (UIView *)makeBorderCornerCurved:(UIView *)view withRadius:(CGFloat)radius color:(UIColor *)color width:(float)width
{
    UIView *curvyView = view;
    curvyView.layer.cornerRadius = radius;
    curvyView.layer.masksToBounds = YES;
    
    curvyView.layer.borderWidth = width;
    curvyView.layer.borderColor = color.CGColor;
    return curvyView;
}

//Makes rounded imageView
+ (UIImageView *)makeImageRounded:(UIImageView *)imageView
{
    UIImageView *imgView = imageView;
    imgView.layer.cornerRadius = imageView.frame.size.width/2;
    imgView.layer.masksToBounds = YES;
    return imgView;
}

//Makes rounded imageView
+ (UIScrollView *)makeScrollViewRounded:(UIScrollView *)scroll
{
    UIScrollView *scrollView = scroll;
    scrollView.layer.cornerRadius = scroll.frame.size.width/2;
    scrollView.layer.masksToBounds = YES;
    return scrollView;
}


//Makes imageView corner rounded
+ (UIImageView *)makeImageCornerRounded:(UIImageView *)imageView
{
    UIImageView *imgView = imageView;
    imgView.layer.cornerRadius = 10;
    imgView.layer.masksToBounds = YES;
    return imgView;
}

//Returns UIView object with shadow effect
+ (UIView *)shadowView:(UIView *)view
{
    view.layer.shadowRadius  = 1.5f;
    view.layer.shadowColor   = [UIColor blackColor].CGColor;
    view.layer.shadowOffset  = CGSizeMake(0.0f, 0.0f);
    view.layer.shadowOpacity = 0.9f;
    view.layer.masksToBounds = NO;
    
    UIEdgeInsets shadowInsets     = UIEdgeInsetsMake(0, 0, 0, 0);
    UIBezierPath *shadowPath      = [UIBezierPath bezierPathWithRect:UIEdgeInsetsInsetRect(view.bounds, shadowInsets)];
    view.layer.shadowPath    = shadowPath.CGPath;
    return view;
}

//Internet availability check
+ (BOOL)isNetworkAvailable
{
    BOOL returnValue = NO;
    Reachability *internetReach = [Reachability reachabilityForInternetConnection] ;
    [internetReach startNotifier];
    
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    
    switch (netStatus)
    {
        case ReachableViaWWAN:
        {
            returnValue = YES;
            break;
        }
        case ReachableViaWiFi:
        {
            returnValue = YES;
            break;
        }
        case NotReachable:
        {
            returnValue = NO;
            break;
        }
    }
    [internetReach stopNotifier];
    return returnValue;
}

//Alert for validations
+ (void) showApplicationAlertWithMessage:(NSString *)alertMessage
                                 withDelegate:(id)delegate
                                      withTag:(NSInteger)tag
                            otherButtonTitles:(NSArray *)buttonTitles
{
    if(![alertMessage isEqualToString:EMPTY_STRING] && (alertMessage != nil))
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:APPLICATION_NAME message:alertMessage
                                                           delegate:delegate
                                                  cancelButtonTitle:nil otherButtonTitles:nil, nil];
        //alertView.delegate =  delegate;
        alertView.tag = tag;
        
        for (NSString *buttonTitle in buttonTitles)
        {
            [alertView addButtonWithTitle:buttonTitle];
        }
        [alertView show];
    }
}

//Convert date into UTC
+ (NSDate *)getUTCFormateDate:(NSDate *)date
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:date];
    NSDate *utc_date = [dateFormatter dateFromString:dateString];
    
    return utc_date;
}

//Gets server date time from specific date
+ (long)getServerTimeInMilies:(NSDate *)date
{
    long dateInMilies = [@(floor([date timeIntervalSince1970])) longLongValue];
    return dateInMilies;
}

//Convert seconds into H:M:S
+ (NSString *)getTimeStringFromSeconds:(double)seconds
{
    NSDateComponentsFormatter *dcFormatter = [[NSDateComponentsFormatter alloc] init];
    dcFormatter.allowedUnits = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    dcFormatter.unitsStyle = NSDateComponentsFormatterUnitsStyleShort;
    return [dcFormatter stringFromTimeInterval:seconds];
}

//Returns the boolean value
+ (BOOL)isEmptyString:(NSString *)text
{
    return (nil == text ||
            YES == [[self trimWhiteSpacesOfAText:text] isEqualToString:EMPTY_STRING]) ? YES : NO;
}

//trims the spaces from string
+ (NSString *)trimWhiteSpacesOfAText:(NSString *)text
{
    return [text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

//Returns rounded view
+ (UIView *)makeBorder:(UIView *)view
{
    UIView *curvyView = view;
    curvyView.layer.borderWidth = 1.0;
    curvyView.layer.borderColor = [UIColor blackColor].CGColor;
    return curvyView;
}

//Returns UIView object with corner curved according to specific radious, color and width
+ (UIView *)makeCornerCurved:(UIView *)view withRadius:(CGFloat)radius
{
    UIView *curvyView = view;
    curvyView.layer.cornerRadius = radius;
    curvyView.layer.masksToBounds = YES;
    return curvyView;
}

////Shows custom dialoge
//+ (void)showAlertViewWithButtonTitle:(NSArray *)buttonTitles title:(NSString *)title messsage:(NSString *)message alertLeftButton:(AlertButton)alertLeftButton alertRightButton:(AlertButton)alertRightButton delegate:(id)controller
//{
//    CustomIOSAlertView *alertView = [[CustomIOSAlertView alloc] init];
//    [[AppDelegate sharedAppDelegate].window addSubview:alertView];
//    alertView.delegate = controller;
//    alertView.alpha = 0.0f;
//    [self fadeIn:alertView];
//    alertView.alertLeftButton = alertLeftButton;
//    alertView.alertRightButton = alertRightButton;
//    [alertView setTitleOfButtons:buttonTitles];
//    [alertView setTitle:title message:message];
//}

//An ease-in curve causes the animation to begin slowly, and then speed up as it progresses.
+ (void)fadeIn:(UIView *)customView
{
    [UIView animateWithDuration:0.3f
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         customView.alpha = 1.0f;
                     }
                     completion:nil];
}

//Returns a bool vlaue whether key exists or not
+ (BOOL)containsKey:(NSString *)key InDictionary:(NSDictionary *)dictionary
{
    BOOL retVal = NO;
    NSArray *allKeys = [dictionary allKeys];
    
    retVal = [allKeys containsObject:key];
    
    // Check that the value is not null
    if (retVal && [dictionary objectForKey:key] == (id)[NSNull null]) {
        retVal = NO;
    }
    return retVal;
}

//Returns a Boolean value that indicates whether a given string is equal to the receiver using a literal Unicode-based comparison.
+ (BOOL)isMatchPassword:(NSString *)password confirmPassword:(NSString *)confirmPassword
{
    if ( [password isEqualToString: confirmPassword] )
    {
        return YES;
        
    } else {
        
        return NO;
    }
}

//The number of UTF-16 code units in the receiver.
+ (BOOL)isMatchPasswordLength:(NSString *)password
{
    if ( [password length] > 4)
    {
        return YES;
        
    } else {
        
        return NO;
    }
}

+ (BOOL)validatePhone:(NSString *)phone {
    BOOL isValidPhone = NO;
    NSString *expression = [NSString stringWithFormat:@"^[0-9]{10}$"];
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:nil];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:phone
                                                        options:0
                                                          range:NSMakeRange(0, [phone length])];
    if (numberOfMatches == 0) {
        expression = [NSString stringWithFormat:@"^[+][0-9]{11}$"];
        regex = [NSRegularExpression regularExpressionWithPattern:expression options:NSRegularExpressionCaseInsensitive   error:nil];
        numberOfMatches = [regex numberOfMatchesInString:phone
                                                 options:0
                                                   range:NSMakeRange(0, [phone length])];
        if (numberOfMatches == 0) {
            isValidPhone = NO;
        } else {
            isValidPhone =  YES;
        }
        
    } else {
        isValidPhone =  YES;

    }
    
    if (!isValidPhone) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:APPLICATION_NAME message:@"Please enter a valid phone number."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    
    return isValidPhone;
}

+ (BOOL)validateEmail:(NSString *)eMail
{
    BOOL isValidEmail = NO;
    
    if(eMail && ![eMail isEqualToString:EMPTY_STRING])
    {
        NSString *searchtext = @"\\";
        
        if ([eMail rangeOfString:searchtext].location != NSNotFound)
        {
            return NO;
        }
        
        searchtext = @"\"";
        
        if ([eMail rangeOfString:searchtext].location != NSNotFound)
        {
            return NO;
        }
        
        NSString *regExPattern = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
        NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
        NSUInteger regExMatches = [regEx numberOfMatchesInString:eMail options:0 range:NSMakeRange(0, [eMail length])];
        regEx =nil;
        isValidEmail = (regExMatches == 0) ? NO:YES;
        
    }
    
    return isValidEmail;
}

+ (NSUInteger)fitString:(NSString *)string intoLabel:(UILabel *)label
{
    UIFont *font           = label.font;
    NSLineBreakMode mode   = label.lineBreakMode;
    
    CGFloat labelWidth     = label.frame.size.width;
    CGFloat labelHeight    = label.frame.size.height;
    CGSize  sizeConstraint = CGSizeMake(labelWidth, CGFLOAT_MAX);
    
    if (SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"7.0"))
    {
        NSDictionary *attributes = @{ NSFontAttributeName : font };
        NSAttributedString *attributedText = [[NSAttributedString alloc] initWithString:string attributes:attributes];
        CGRect boundingRect = [attributedText boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin context:nil];
        {
            if (boundingRect.size.height > labelHeight)
            {
                NSUInteger index = 0;
                NSUInteger prev;
                NSCharacterSet *characterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
                
                do
                {
                    prev = index;
                    if (mode == NSLineBreakByCharWrapping)
                        index++;
                    else
                        index = [string rangeOfCharacterFromSet:characterSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
                }
                
                while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] boundingRectWithSize:sizeConstraint options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.height <= labelHeight);
                
                return prev;
            }
        }
    }
    else
    {
        if ([string sizeWithFont:font constrainedToSize:sizeConstraint lineBreakMode:mode].height > labelHeight)
        {
            NSUInteger index = 0;
            NSUInteger prev;
            NSCharacterSet *characterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            
            do
            {
                prev = index;
                if (mode == NSLineBreakByCharWrapping)
                    index++;
                else
                    index = [string rangeOfCharacterFromSet:characterSet options:0 range:NSMakeRange(index + 1, [string length] - index - 1)].location;
            }
            
            while (index != NSNotFound && index < [string length] && [[string substringToIndex:index] sizeWithFont:font constrainedToSize:sizeConstraint lineBreakMode:mode].height <= labelHeight);
            
            return prev;
        }
    }
    
    return [string length];
}

/*
+ (NSString *)dateTime:(NSDate *)serverdate
{
    
    NSDate *date = serverdate;
    NSInteger dayDiff = (int)[date timeIntervalSinceNow] / (60*60*24);
    
    //    NSDateComponents *componentsToday = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    //    NSDateComponents *componentsDate = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:serverdate];
    //    NSInteger day = [componentsToday day] - [componentsDate day];
    
    NSString *dateString = EMPTY_STRING;
    
    if (dayDiff == 0) {
        
        dateString = [NSString stringWithFormat:@"Today %@", [date getTimeFromDateWithoutDay]];
        
    } else if (dayDiff == -1) {
        
        dateString = [NSString stringWithFormat:@"Yesterday %@", [date getTimeFromDateWithoutDay]];
        
    } else {
        
        dateString = [NSString stringWithFormat:@"%@", [date getTimeDayFromDate]];
    }
    
    return dateString;
}
 */

+ (NSString*)dateTime:(NSDate*) serverdate
{
    NSDictionary *timeScale = @{@"sec"  :@1,
                                @"min"  :@60,
                                @"hr"   :@3600,
                                @"day"  :@86400,
                                @"week" :@605800,
                                @"month":@2629743,
                                @"year" :@31556926};
    NSString *scale;
    int timeAgo = 0-(int)[serverdate timeIntervalSinceNow];
    if (timeAgo < 60) {
        scale = @"sec";
    } else if (timeAgo < 3600) {
        scale = @"min";
    } else if (timeAgo < 86400) {
        scale = @"hr";
    } else if (timeAgo < 605800) {
        scale = @"day";
    } else if (timeAgo < 2629743) {
        scale = @"week";
    } else if (timeAgo < 31556926) {
        scale = @"month";
    } else {
        scale = @"year";
    }
    
    timeAgo = timeAgo/[[timeScale objectForKey:scale] integerValue];
    NSString *s = @"";
    if (timeAgo > 1) {
        s = @"s";
    }
    
    return [NSString stringWithFormat:@"%d %@%@", timeAgo, scale, s];
}

/*
+ (NSString *)dateTime:(NSDate *)date
{
    NSLog(@"date %@",date);
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitHour |
    NSCalendarUnitMinute;

    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components1 = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components1];
    
    components1 = [cal components:(NSCalendarUnitEra|NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay) fromDate:date];
    NSDate *thatdate = [cal dateFromComponents:components1];
    
    
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:thatdate
                                                                     toDate:today
                                                                    options:0];
    
    if (components.year > 0) {
        return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
    }
    else if (components.month > 0)
    {
        return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
    }
    else if (components.weekOfYear > 0)
    {
        return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
    }
    else if (components.day > 0)
    {
        if (components.day > 1)
        {
            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
        }
        else {
            
            return @"Yesterday";
        }
    }
    else if (components.hour > 0)
    {
        return [NSString stringWithFormat:@"%ld hours ago", (long)components.hour];

        //return @"Today";
    }
    else
    {
        return [NSString stringWithFormat:@"%ld minutes ago", (long)components.minute];
    }
}
*/


+ (NSString *)time:(NSDate *)serverdate
{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone localTimeZone];//[NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    
    [dateFormatter setDateFormat:@"EEE, MMM d, h:mm a"];
    NSString *dateString = [dateFormatter stringFromDate:serverdate];
    
    return dateString;
    
    /*
    NSDate *date = serverdate;
    NSInteger dayDiff = (int)[date timeIntervalSinceNow] / (60*60*24);
    
    //    NSDateComponents *componentsToday = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    //    NSDateComponents *componentsDate = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:serverdate];
    //    NSInteger day = [componentsToday day] - [componentsDate day];
    
    NSString *dateString = EMPTY_STRING;
    
    if (dayDiff == 0) {
        
        dateString = [NSString stringWithFormat:@"Today %@", [date getTimeFromDateWithoutDay]];
        
    } else if (dayDiff == -1) {
        
        dateString = [NSString stringWithFormat:@"Yesterday %@", [date getTimeFromDateWithoutDay]];
        
    } else {
        
        dateString = [NSString stringWithFormat:@"%@", [date getTimeDayFromDate]];
    }*/
    
   /// return  [date getTimeFromDateWithoutDay];*/
}

+ (NSString *)getImageFileType:(NSString *)imageName
{
    NSString *imageType = @"png";
    
    NSArray *arrayOfStringSeparatedByDot = [imageName componentsSeparatedByString:@"."];
    
    imageType = [arrayOfStringSeparatedByDot lastObject];
    
    return imageType;
}




@end
