

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@import QuartzCore;

@interface CALayer (Additions)

@property(nonatomic, assign) UIColor* borderIBColor;
//@property(nonatomic, assign) UIColor* shadowIBColor;

@end
