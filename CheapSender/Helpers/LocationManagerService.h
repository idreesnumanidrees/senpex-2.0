//
//  CurrentLocationManager.h
//  OTW
//
//  Created by admin on 1/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>

//Protocol setup
@protocol LocationManagerServiceDelegate <NSObject>

//Sends the current location to delegate method
- (void)notifyToMapViewWithLocation:(CLLocation *)currentLocation;

//Notify to MapView on disabled GPS
- (void)notifyOnGPSDisable;

@end

@interface LocationManagerService : CLLocation <CLLocationManagerDelegate>

//CLLocation manager instance object.
@property(nonatomic, strong) CLLocationManager *locationManager;

//CLGeocoder instance object.
@property(nonatomic, strong) CLGeocoder *geocoder;

//Setting Location manager delegate property, it will use for delegation between location manager class and other class.
@property (nonatomic, assign) id <LocationManagerServiceDelegate> locationManagerDelagete;

//Calls of basic methods of CLLocationManger
- (void)initialize;

//This method starts the location update manager
- (void)startUpdatingLocation;

//This method stops the location update manager
- (void)stopUpdatingLocation;

@end
