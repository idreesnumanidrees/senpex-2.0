//
//  GoogleMapHelper.h
//  OTW
//
//  Created by admin on 1/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import "CSUser.h"
#import "CustomCallOutView.h"
#import "CSUser.h"
#import "SPDistanceTime.h"

@interface GoogleMapHelper : GMSMapView <GMSMapViewDelegate, NSCopying>

@property (assign, nonatomic) BOOL isCurrentLocationLoadedOnce;

@property (nonatomic, strong) CustomCallOutView *callOutView;


// Set the map style by passing the URL for style.json.
- (void)initWithCustomStyle;

//  Clears all markup that has been added to the map, including markers, polylines and ground overlays.  This will not clear the visible location dot or reset the current mapType.
 - (void)resetMap;

//This map will add custom map pin for user against specific coordinates 
- (GMSMarker *)addMapPinForUser:(CSUser *)user;

- (void)showUserLocation:(CLLocation *)location;


//GMSCoordinateBounds represents a rectangular bounding box on the Earth's surface. GMSCoordinateBounds is immutable and can't be modified after construction.
- (void)showAllMarkersInMapViewWithBound:(GMSCoordinateBounds *)bounds;


- (void)getDistanceAndTimeTaken:(CLLocationDegrees )UserFromLatitude
              UserFromLongitude:(CLLocationDegrees )UserFromLongitude
                 UserToLatitude:(CLLocationDegrees )UserToLatitude
                UserToLongitude:(CLLocationDegrees )UserToLongitude
                 withCompletion:(void (^)(NSDictionary *lastDict))completionHandler;

- (SPDistanceTime *)parseDistaneTimeFromGoogleAPIResponse:(NSDictionary *)jsonDict;

- (void)fetchPolylineWithOrigin:(CLLocation *)origin color:(UIColor *)color destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler;


@end
