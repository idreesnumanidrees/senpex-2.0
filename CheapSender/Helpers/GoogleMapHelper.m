//
//  GoogleMapHelper.m
//  OTW
//
//  Created by admin on 1/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

#import "GoogleMapHelper.h"
#import "CSAppConstants.h"
#import "CSGeneralConstants.h"

@implementation GoogleMapHelper

//  Clears all markup that has been added to the map, including markers, polylines and ground overlays.  This will not clear the visible location dot or reset the current mapType.
- (void)resetMap
{
    [self clear];
}

// Set the map style by passing the URL for style.json.
- (void)initWithCustomStyle
{
    //An NSBundle object helps you access the code and resources in a bundle directory on disk
//    NSBundle *mainBundle = [NSBundle mainBundle];
//    NSURL *styleUrl = [mainBundle URLForResource:@"google_Map_Style" withExtension:@"json"];
//    NSError *error;
//    
//    // Set the map style by passing the URL for style.json.
//    GMSMapStyle *style = [GMSMapStyle styleWithContentsOfFileURL:styleUrl error:&error];
//    
//    if (!style) {
////        NSLog(@"The style definition could not be loaded: %@", error);
//    }
//    self.mapStyle = style;
    
    [self initializeCustomMapInfoWindow];
    
}

//GMSCoordinateBounds represents a rectangular bounding box on the Earth's surface. GMSCoordinateBounds is immutable and can't be modified after construction.
- (void)showAllMarkersInMapViewWithBound:(GMSCoordinateBounds *)bounds {
    [self animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:100]];
//    [self animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withEdgeInsets:UIEdgeInsetsMake(140, 100, 220, 30)]];
}

- (void)showUserLocation:(CLLocation *)location {
    if(_isCurrentLocationLoadedOnce == NO)
    {
        /**
         * GMSCameraPosition An immutable class that aggregates all camera position parameters.
         */
        GMSCameraPosition *camera = [[GMSCameraPosition alloc] initWithTarget:location.coordinate
                                                                         zoom:12
                                                                      bearing:0
                                                                 viewingAngle:0];
        self.camera = camera;
        _isCurrentLocationLoadedOnce = YES;
    }
}

//This map will add pin for user against specific coordinates
- (GMSMarker *)addMapPinForUser:(CSUser *)user;
{
    
    //nil marker
    GMSMarker *newMarker = nil;
    
    if((user.latitude != 0 && user.longitude != 0))
    {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(user.latitude, user.longitude);
        
        if(_isCurrentLocationLoadedOnce == NO)
        {
            /**
             * GMSCameraPosition An immutable class that aggregates all camera position parameters.
             */
            GMSCameraPosition *camera = [[GMSCameraPosition alloc] initWithTarget:coordinate
                                                                             zoom:12
                                                                          bearing:0
                                                                     viewingAngle:0];
            self.camera = camera;
            _isCurrentLocationLoadedOnce = YES;
        }
        
        //Prepare Marker
        newMarker = [[GMSMarker alloc] init];
        newMarker.position = coordinate;
        newMarker.map = self;
        newMarker.icon = [UIImage imageNamed:user.imageName];
        newMarker.title = user.address;
        newMarker.snippet = user.ovalImage;        
    }
    return newMarker;
}

- (void)initializeCustomMapInfoWindow
{
    NSArray *array = [[NSBundle mainBundle]loadNibNamed:@"CustomCallOutView" owner:self options:nil];
    self.callOutView = [[CustomCallOutView alloc]init];
    self.callOutView = [array firstObject];
}

#pragma mark - GMSMapView Delegate Methods
- (UIView *)mapView:(GMSMapView *)mapView markerInfoWindow:(GMSMarker *)marker
{
    
    [self.callOutView populateFields:marker.title ovalImageName:marker.snippet];
    return _callOutView;
}

- (void)getDistanceAndTimeTaken:(CLLocationDegrees )UserFromLatitude
              UserFromLongitude:(CLLocationDegrees )UserFromLongitude
                 UserToLatitude:(CLLocationDegrees )UserToLatitude
                UserToLongitude:(CLLocationDegrees )UserToLongitude
                 withCompletion:(void (^)(NSDictionary *lastDict))completionHandler
{
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/distancematrix/json?key=%@&origins=\%f,\%f&destinations=\%f,\%f&language=en-EN",GOOGLE_MAPS_API_KEY,UserFromLatitude,
                           UserFromLongitude,UserToLatitude, UserToLongitude];
    
//    NSLog(@"latitude, longitude %f %f", UserFromLatitude, UserFromLongitude);
    urlString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlString];

    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration];
    NSURLSessionDataTask *task = [session dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                  {
                                      if(data != nil)
                                      {
                                          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                                               options:NSJSONReadingMutableContainers
                                                                                                 error:nil];
                                          for(NSString *key in [json allKeys]) {
                                              NSLog(@"%@",[json objectForKey:key]);
                                          }
                                          completionHandler(json);
                                      }
                                      else {
                                          
                                          completionHandler(nil);
                                      }
                                  }];
    [task resume];
    
}

- (SPDistanceTime *)parseDistaneTimeFromGoogleAPIResponse:(NSDictionary *)jsonDict
{
    NSArray *valuesArray = jsonDict[@"rows"];
    SPDistanceTime *distanceTimeObj = nil;

    if (valuesArray.count > 0) {
        
        distanceTimeObj = [SPDistanceTime new];
        NSDictionary *dict = valuesArray[0];
        NSArray *closeArray = dict[@"elements"];
        NSDictionary *distanceTimeDict = closeArray[0];
        
        //Get distance and time value
        for(NSString *key in distanceTimeDict.allKeys)
        {
            if([key isEqualToString:@"distance"])
            {
                NSDictionary *distanceDict = [distanceTimeDict valueForKey:key];
                
                float distanceInKiloMeters = [[distanceDict valueForKey:@"value"] floatValue];
                
                //Distance in miles
                distanceTimeObj.distance = [NSString stringWithFormat:@"%.2f", (distanceInKiloMeters/1000)*0.621371];
            }
            else if([key isEqualToString:@"duration"])
            {
                NSDictionary *distanceDict = [distanceTimeDict valueForKey:key];
                
                int durationSeconds = [[distanceDict valueForKey:@"value"] intValue];
                distanceTimeObj.duration = [NSString stringWithFormat:@"%i", durationSeconds];
            }
        }
    }
    
    return distanceTimeObj;
}


- (void)fetchPolylineWithOrigin:(CLLocation *)origin color:(UIColor *)color destination:(CLLocation *)destination completionHandler:(void (^)(GMSPolyline *))completionHandler
{
    NSString *originString = [NSString stringWithFormat:@"%f,%f", origin.coordinate.latitude, origin.coordinate.longitude];
    NSString *destinationString = [NSString stringWithFormat:@"%f,%f", destination.coordinate.latitude, destination.coordinate.longitude];
    NSString *directionsAPI = @"https://maps.googleapis.com/maps/api/directions/json?";
    NSString *directionsUrlString = [NSString stringWithFormat:@"%@&origin=%@&destination=%@&mode=driving&key=%@", directionsAPI, originString, destinationString, GOOGLE_MAPS_API_KEY];
    NSURL *directionsUrl = [NSURL URLWithString:directionsUrlString];
    NSURLSessionDataTask *fetchDirectionsTask = [[NSURLSession sharedSession] dataTaskWithURL:directionsUrl completionHandler:
                                                 ^(NSData *data, NSURLResponse *response, NSError *error)
                                                 {
                                                     NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                                                     if(error)
                                                     {
                                                         if(completionHandler)
                                                             completionHandler(nil);
                                                         return;
                                                     }
                                                     
                                                     NSArray *routesArray = [json objectForKey:@"routes"];
                                                     
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         //Your main thread code goes in here
//                                                         NSLog(@"Im on the main thread");
                                                         
                                                         GMSPolyline *polyline = nil;
                                                         
                                                         if ([routesArray count] > 0)
                                                         {
                                                             NSDictionary *routeDict = [routesArray objectAtIndex:0];
                                                             NSDictionary *routeOverviewPolyline = [routeDict objectForKey:@"overview_polyline"];
                                                             NSString *points = [routeOverviewPolyline objectForKey:@"points"];
                                                             GMSPath *path = [GMSPath pathFromEncodedPath:points];
                                                             
                                                             polyline = [GMSPolyline polylineWithPath:path];
                                                             
                                                             polyline.strokeWidth = 3;
                                                             polyline.strokeColor = color;
                                                         }
                                                         if(completionHandler)
                                                             completionHandler(polyline);
                                                     });
                                                 }];
    [fetchDirectionsTask resume];
}

@end
