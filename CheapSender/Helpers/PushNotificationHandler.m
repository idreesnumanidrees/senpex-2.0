//
//  PushNotificationHandler.m
//  CheapSender
//
//  Created by Idrees on 2017/08/02.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "PushNotificationHandler.h"
#import "UtilHelper.h"
#import "AppDelegate.h"
#import "WebServicesClient.h"
#import "SendingDetailViewController.h"

@implementation PushNotificationHandler

- (void)requestForGetPackDetails:(NSString *)packID {
    if (![UtilHelper isNetworkAvailable]) {
        [UtilHelper showApplicationAlertWithMessage:NO_INTERNET_FOUND withDelegate:nil withTag:0 otherButtonTitles:@[@"Ok"]];
        return;
    }
    
    [WebServicesClient GetPackDetails:packID
                        IncludeImages:@"1"
                       IncludeBidders:@"1"
                                Start:@"0"
                                Count:@"0"
                        LogSessionKey:[AppDelegate sharedAppDelegate].sessionKey
                          LogDeviceId:[AppDelegate sharedAppDelegate].iosDeviceToken
                         LogUserAgent:[[UIDevice currentDevice] systemVersion]
                        LogDeviceType:LOG_DEVICE_TYPE
                    completionHandler:^(PackDetailsModel *packDetails, NSError *error) {
                        
                        if (!error)
                            [[AppDelegate sharedAppDelegate].appController showSendingDetailView:nil PackId:packID FromReceiver:false];
                        else if (error.code == 3)
                            [UtilHelper showApplicationAlertWithMessage:@"Your session has expired. Please login again." withDelegate:self withTag:1001 otherButtonTitles:@[@"Ok"]];
                        else
                            [UtilHelper showApplicationAlertWithMessage:error.domain withDelegate:self withTag:11 otherButtonTitles:@[@"Ok"]];
                    }];
}

+ (void)GetPushNotificationInfo:(NSDictionary *)userInfo
{
    NSDictionary *packInfo = userInfo[@"details"];
    
    if ([packInfo[@"link_enabled"] isEqualToString:@"1"]) {
        
        NSInteger userLogin = [[UtilHelper getValueFromNSUserDefaultForKey:LOGON_USER] intValue];
        
        if (userLogin == 1) {
            
            if ([packInfo[@"link_module"] isEqualToString:@"new_pack"] || [packInfo[@"link_module"] isEqualToString:@"view_pack"]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:SENDER_ACTIVE_LIST_UPDATED object:@{}];
                
                // [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_ORDER_DETAILS object:@{}];
                
                NSDictionary* userInfo = @{@"package_id": packInfo[@"link_id"]};
                [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_ORDER_DETAILS
                                                                    object:@{}
                                                                  userInfo:userInfo];
                
//                if ([AppDelegate sharedAppDelegate].isNavigateToOrderDetails == 1 && [AppDelegate sharedAppDelegate].isNeedToOrderDetails == 0) {
//                    if([[AppDelegate sharedAppDelegate].appController.navController.visibleViewController isKindOfClass:[SendingDetailViewController class]]){
//                        [[AppDelegate sharedAppDelegate].appController.navController popToRootViewControllerAnimated:true];
//                    }
                    [NSThread sleepForTimeInterval:1.0];
                    
                    [[AppDelegate sharedAppDelegate].appController showSendingDetailView:nil PackId:packInfo[@"link_id"] FromReceiver:false];
//                    [[AppDelegate sharedAppDelegate].appController showSendingDetailView:nil PackId:packInfo[@"link_id"] FromReceiver:false];
//                }
                
                if ([AppDelegate sharedAppDelegate].appController.senderTabbar.selectedIndex != 2) {
                    
                    [[[AppDelegate sharedAppDelegate].appController.senderTabbar.tabBar.items objectAtIndex:2] setBadgeValue:@""];
                    [[[AppDelegate sharedAppDelegate].appController.senderTabbar.tabBar.items objectAtIndex:2] setBadgeColor:[UIColor colorWithRed:255/255.0 green:104/255.0 blue:0/255.0 alpha:1.0]];
                }
            }
            else if ([packInfo[@"link_module"] isEqualToString:@"view_chat_message"]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:VIEW_CHAT_MESSAGE object:@{}];
                [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_CONVERSATION_LIST object:@{}];
                
                if ([AppDelegate sharedAppDelegate].appController.senderTabbar.selectedIndex != 1) {
                    
                    [[[AppDelegate sharedAppDelegate].appController.senderTabbar.tabBar.items objectAtIndex:1] setBadgeValue:@""];
                    [[[AppDelegate sharedAppDelegate].appController.senderTabbar.tabBar.items objectAtIndex:1] setBadgeColor:[UIColor colorWithRed:255/255.0 green:104/255.0 blue:0/255.0 alpha:1.0]];
                }
                
                if ([AppDelegate sharedAppDelegate].isNavigateToChatDetails == 1 && [AppDelegate sharedAppDelegate].isNeedToChatDetails == 0) {
                    
                    CourierConversations *sender = [CourierConversations new];
                    sender.receiverId = packInfo[@"user_id"];
                    sender.viewId = packInfo[@"link_id"];
                    sender.senderId = packInfo[@"sender_id"];
                    [[AppDelegate sharedAppDelegate].appController ShowChatDetailView:sender PackDetails:nil];
                    [AppDelegate sharedAppDelegate].isNavigateToChatDetails = 0;
                }
            }
        } else if (userLogin == 2) {
            
            BOOL isNotsTabNotSelected = NO;
            
            if ([packInfo[@"link_module"] isEqualToString:@"new_pack"] || [packInfo[@"link_module"] isEqualToString:@"view_pack"]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:COURIER_ACTIVE_LIST_UPDATED object:@{}];
                
                NSDictionary* userInfo = @{@"package_id": packInfo[@"link_id"]};
                [[NSNotificationCenter defaultCenter] postNotificationName:REFRESH_ORDER_DETAILS
                                                                    object:@{}
                                                                  userInfo:userInfo];
                
                if ([AppDelegate sharedAppDelegate].isNavigateToOrderDetails == 1 && [AppDelegate sharedAppDelegate].isNeedToOrderDetails == 0) {
                    
                    [[AppDelegate sharedAppDelegate].appController showCourierSendingsDetail:packInfo[@"link_id"]];
                }
                
                isNotsTabNotSelected = YES;
            }
            else if ([packInfo[@"link_module"] isEqualToString:@"view_bank_account"]) {
                
                [[AppDelegate sharedAppDelegate].appController showAccountInfoView];
                isNotsTabNotSelected = YES;
            }
            else if ([packInfo[@"link_module"] isEqualToString:@"view_payout_details"]) {
                
                [[AppDelegate sharedAppDelegate].appController showWithdrawHistoryView];
                isNotsTabNotSelected = YES;
            }
            else if ([packInfo[@"link_module"] isEqualToString:@"view_chat_message"]) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:VIEW_CHAT_MESSAGE object:@{}];
                [[NSNotificationCenter defaultCenter] postNotificationName:UPDATE_CONVERSATION_LIST object:@{}];
                
                if ([AppDelegate sharedAppDelegate].appController.courierTabbar.selectedIndex != 1) {
                    
                    [[[AppDelegate sharedAppDelegate].appController.courierTabbar.tabBar.items objectAtIndex:1] setBadgeValue:@""];
                    [[[AppDelegate sharedAppDelegate].appController.courierTabbar.tabBar.items objectAtIndex:1] setBadgeColor:[UIColor colorWithRed:17/255.0 green:167/255.0 blue:170/255.0 alpha:1.0]];
                }
                
                if ([AppDelegate sharedAppDelegate].isNavigateToChatDetails == 1 && [AppDelegate sharedAppDelegate].isNeedToChatDetails == 0) {
                    
                    CourierConversations *courier = [CourierConversations new];
                    courier.receiverId = packInfo[@"user_id"];
                    courier.viewId = packInfo[@"link_id"];
                    courier.senderId = packInfo[@"sender_id"];
                    [[AppDelegate sharedAppDelegate].appController showChatCourierDetails:courier PackDetails:nil];
                    [AppDelegate sharedAppDelegate].isNavigateToChatDetails = 0;
                }
            }
            
            if (isNotsTabNotSelected) {
                
                if ([AppDelegate sharedAppDelegate].appController.courierTabbar.selectedIndex != 3) {
                    
                    [[[AppDelegate sharedAppDelegate].appController.courierTabbar.tabBar.items objectAtIndex:3] setBadgeValue:@""];
                    [[[AppDelegate sharedAppDelegate].appController.courierTabbar.tabBar.items objectAtIndex:3] setBadgeColor:[UIColor colorWithRed:17/255.0 green:167/255.0 blue:170/255.0 alpha:1.0]];
                }
            }
        }
    }
}

@end
