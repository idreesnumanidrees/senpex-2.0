//
//  UtilHelper.h
//  OTW
//
//  Created by admin on 1/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CSAppConstants.h"

@interface UtilHelper : NSObject


//Sets the value in NSUserDefaults of specific key
+ (void)saveAndUpdateNSUserDefaultWithObject:(id)object forKey:(NSString *)key;

//Gets the value from NSUserDefaults of specific key
+ (id)getValueFromNSUserDefaultForKey:(NSString *)key;
+ (id)getUserFromNSUserDefaultForKey:(NSString *)key;

//Returns UIView object with corner curved according to specific radious, color and width
+ (UIView *)makeCornerCurved:(UIView *)view withRadius:(CGFloat)radius color:(UIColor *)color width:(float)width;

+ (UIView *)makeBorderCornerCurved:(UIView *)view withRadius:(CGFloat)radius color:(UIColor *)color width:(float)width;

+ (UITextField *)makeBorderForTextField:(UITextField *)view color:(UIColor *)color width:(float)width;

//Makes rounded imageView
+ (UIImageView *)makeImageRounded:(UIImageView *)imageView;

+ (UIScrollView *)makeScrollViewRounded:(UIScrollView *)scroll;

//Makes imageview corner rounded
+ (UIImageView *)makeImageCornerRounded:(UIImageView *)imageView;

//Returns UIView object with shadow effect
+ (UIView *)shadowView:(UIView *)view;

//Alert for internet validations
+ (BOOL)isNetworkAvailable;

+ (void)showApplicationAlertWithMessage:(NSString *)alertMessage
                                 withDelegate:(id)delegate
                                      withTag:(NSInteger)tag
                            otherButtonTitles:(NSArray *)buttonTitles;

//Convert date into UTC
+ (NSDate *)getUTCFormateDate:(NSDate *)date;

//Gets server date time from specific date
+ (long)getServerTimeInMilies:(NSDate *)date;

//Convert seconds into H:M:S
+ (NSString *)getTimeStringFromSeconds:(double)seconds;

//Returns the boolean value
+ (BOOL)isEmptyString:(NSString *)text;

//Returns rounded view
+ (UIView *)makeBorder:(UIView *)view;

//Returns UIView object with corner curved according to specific radious, color and width
+ (UIView *)makeCornerCurved:(UIView *)view withRadius:(CGFloat)radius;

//Shows custom dialoge
//+ (void)showAlertViewWithButtonTitle:(NSArray *)buttonTitles title:(NSString *)title messsage:(NSString *)message alertLeftButton:(AlertButton)alertLeftButton alertRightButton:(AlertButton)alertRightButton delegate:(id)controller;

//Returns a bool vlaue whether key exists or not
+ (BOOL)containsKey:(NSString *)key InDictionary:(NSDictionary *)dictionary;

//Returns a Boolean value that indicates whether a given string is equal to the receiver using a literal Unicode-based comparison.
+ (BOOL)isMatchPassword:(NSString *)password confirmPassword:(NSString *)confirmPassword;

//The number of UTF-16 code units in the receiver.
+ (BOOL)isMatchPasswordLength:(NSString *)password;

+ (BOOL)validateEmail:(NSString *)eMail;
+ (BOOL)validatePhone:(NSString *)phone;

+ (NSUInteger)fitString:(NSString *)string intoLabel:(UILabel *)label;

+ (NSString *)dateTime:(NSDate *)serverdate;
+ (NSString *)time:(NSDate *)serverdate;

+ (NSString *)getImageFileType:(NSString *)imageName;

@end
