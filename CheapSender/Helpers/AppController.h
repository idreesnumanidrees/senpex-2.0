//
//  AppController.h
//  CheapSender
//
//  Created by admin on 4/18/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DataModels.h"
#import "CSUser.h"
#import "CourierListData.h"(PackDetailsModel *)packListDetails

@interface AppController : NSObject

@property (nonatomic, strong) UIWindow *appWindow;
@property (nonatomic, strong) UIStoryboard *mainStoryBoard;
@property (nonatomic, strong) UIStoryboard *courierStoryboard;
@property (nonatomic, strong) UIStoryboard *courier1Storyboard;

@property (nonatomic, strong) UINavigationController *navController;
@property (nonatomic, strong) UITabBarController *senderTabbar;
@property (nonatomic, strong) UITabBarController *courierTabbar;

//StartUp window and navigaton storyboard setup
- (void)initWithWindow:(UIWindow *)window;
- (void)loadApplication;
- (void)navigationController;

//Tabbar Controller
- (void)loadTabbarController;

//Controllers methods
- (void)showAddTransportView:(BOOL)isComingfromProfile Transport:(GetTransports *)transport;
- (void) showVehiclesVC:(BOOL)isComingfromProfile;

- (void)showVerificationConfirmationnView;
- (void) showCourierRuleView;
- (void) showVerificationView;
- (void)showInsuranceCardView:(BOOL)isComingfromProfile;
- (void)showTakePhotoView:(BOOL)isComingfromProfile;
- (void) showSignUpIDCardVC:(BOOL)isComingfromProfile;
- (void)showTransportTypeVC:(BOOL)isComingfromProfile isComingFromVehicleView:(BOOL)isComingFromVehicleView;

- (void) showTutorialView;
- (void) showSplashView;
- (void) showStartupView;
- (void) showLoginView;
- (void) showRegisterView;
- (void) showResetPasswordView:(BOOL)isComingFromResetPassword;
- (void) showContinueAsView;
- (void) showSendingView;
- (void) showSendingDetailView:(SenderData *)senderData PackId:(NSString *)packId FromReceiver:(BOOL)fromreceiver;
- (void) showRateOrderView:(PackDetailsModel *)packDetails;
- (void)showCourierProfileView:(PackDetailsModel *)packDetails CourierId:(CourierConversations *)convesation isComingFrom:(BOOL)isComingFromChat;
- (void)showBidsView;
- (void)showStatusView:(PackDetailsModel *)packDetail;
- (void)showStatusImageDetail;
- (void)showAcceptedCourierView;
- (void)showTermsView:(NSString *)ruleType;
- (void)showOrderCancelReasonView;
- (void)showReportCourierView:(NSString *)packId;
- (void)showPaymentsMadeView;
- (void)showPaymentToDoViewController;

- (void)showSenderSignUpConfirmVC;
- (void)showSenderResetPassConfirmVC:(BOOL)isComingFromResetPassword;

//Tabbar Controllers
- (void) showNewOrderView:(BOOL)isBackHidden;
- (void) showNewOrderPetInfoView:(NSString *)packId SenderData:(SenderData *)senderData
;
- (void) showNewOrderPackInfoView:(NSString *)packId Send_Cat_ID:(NSString *)send_cat_id SenderData:(SenderData *)senderData isBackHidden :(BOOL)isBackHidden
;
- (void) showNewOrderDestinationView:(NSString *)packId PackPrice:(NSString *)packPrice Send_Cat_ID:(NSString *)send_cat_id ItemPrice:(NSString *)itemprice SenderData:(SenderData *)senderData;
- (void) showNewOrderDestinationView_fastorder:(NSString *)distance DistanceTime:(NSString *)distanceTime From:(CSUser *)from To:(CSUser *)to Pack_Size_ID:(NSString *)pack_size_id Promo_Code:(NSString *)promo_code Item_Value:(NSString *)item_value PackPrice:(NSString *)packPrice Send_Cat_ID:(NSString *)send_cat_id Waiting_Mins:(NSString *)waiting_mins Default_Tariff_Plan:(NSString *)default_tariff_plan SenderData:(SenderData *)senderData;

- (void)ShowChatDetailView:(CourierConversations *)conversation PackDetails:(PackDetailsModel *)packDetails;

- (void)showViewOnMap:(NSString *)comingView PackDetails:(PackDetailsModel *)packDetail isComingFromCourierApp:(BOOL)isComingFromCourierApp;

- (void)showPersonalEditView;
- (void)showComingSoonView;
- (void)showSenderFeedbackView;
- (void)showSenderSettingsView;
- (void)showAddPaymentView;
- (void)showConfirmPaymentView:(NSString *)packId AskToFriend:(Boolean)asktofriend FastOrder:(Boolean)fastorder;
- (void)showSenderChangePasswordVC;

//******************************************Couriers******************************************

- (void)showSignUpCourierView;
- (void)loadCourierTabbarController;
- (void)showChatCourierDetails:(CourierConversations *)conversation PackDetails:(PackDetailsModel *)packDetails;
- (void)showCourierSendingsDetail:(NSString *)packId;
- (void)showCourierPersonalInfoEdit;
- (void)showWithdrawHistoryView;
- (void)showAccountInfoView;
- (void)showBankAccountInfo1View:(BankAccountDetail *)bankAccountDetails;
- (void)showBankAccountInfo2View;
- (void)showCourierSettingsView;
- (void)showCourierSettingsNotifyView;
- (void)showSaveRoutView:(NSDictionary *)route;
- (void)showCourierFeedbackView;
- (void)showCourierRatingsView;
- (void)showCourierFAQView:(NSString *)appUserType;
- (void)showCourierPopularQuestionView:(NSString *)faqID From:(NSString *)from;
- (void)showcourierPersonalInfoVC;
- (void)showSenderProfileVC:(PackDetailsModel *)courierListData Conversation:(CourierConversations *)conversation  isComingFromChat:(BOOL)fromChatDetails;
- (void)showCourierChangePassVC;
- (void)showCourierPayoutDetails:(NSString *)payoutId;


@end
