//
//  AppController.m
//  CheapSender
//
//  Created by admin on 4/18/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "AppController.h"
#import "TutorialViewController.h"
#import "SplashViewController.h"
#import "StartUpViewController.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "ResetPasswordViewController.h"
#import "ContinueAaViewController.h"
#import "NewOrderVC.h"
#import "NewOrderPackInfoVC.h"
#import "NewOrderPetInfoVC.h"
#import "OrderDestinationVC.h"
#import "SendingDetailViewController.h"
#import "SendingsVC.h"
#import "ProfileViewController.h"
#import "RateOrderViewController.h"
#import "CourierProfileViewController.h"
#import "BidsViewController.h"
#import "AcceptedCourierViewController.h"
#import "TermsViewController.h"
#import "ChatDetailVC.h"
#import "ViewOnMapVC.h"
#import "PersonalInfoWithEditVC.h"
#import "OrderCancelReasonViewController.h"
#import "ReportVC.h"
#import "PaymentVC.h"
#import "PayementConfirmationViewController.h"
#import "SendingStatusViewController.h"
#import "OverviewImageDetailViewController.h"

#import "CourierrulesController.h"
#import "SignUpInsuranceCardVC.h"
#import "SignUpTakePhotoVC.h"
#import "VerificationVC.h"
#import "VerificationConfirmationVC.h"
#import "AddTransportVC.h"
#import "ComingSoonVC.h"
#import "SenderSignUpConfirmVC.h"
#import "SenderResetPassConfirmVC.h"
#import "SenderFeedBackVC.h"
#import "SenderSettingsVC.h"
#import "AddPaymentMethodVC.h"
#import "ConfirmPaymentVC.h"
#import "SenderChangePasswordVC.h"

//***********************************Courier****************
#import "SignUpCourierVC.h"
#import "SignUpIDCardVC.h"
#import "ChatCourierDetailVC.h"
#import "CourierSendingDetailVC.h"
#import "CouriererPesonalInfoWithEditVC.h"
#import "WithdrawHistoryVC.h"
#import "AccountInfoVC.h"
#import "BankAccountInfo1VC.h"
#import "BankAccountInfo2VC.h"
#import "CourierRatingsVC.h"
#import "CourierSettingsVC.h"
#import "CourierSettingsNotificationsVC.h"
#import "CourierFeedbackVC.h"
#import "CourierFAQVC.h"
#import "CourierPopularQuestionsVC.h"
#import "CourierPersonalInfoVC.h"
#import "SenderProfileVC.h"
#import "CourierChangePassVC.h"
#import "VehiclesVC.h"
#import "CourierPayoutDetailVC.h"
#import "TransportationTypesView.h"
#import "SaveMyRouteView.h"

@implementation AppController

//StartUp window and storyboard setup
- (void)initWithWindow:(UIWindow *)window
{
    self.appWindow = window;
    [self getStoryBoard];
    [self getCourierStoryboard];
    [self getCourier1Storyboard];
}

- (void)loadApplication
{
    [self getStoryBoard];
}

- (void)getStoryBoard
{
    _mainStoryBoard = [self getMainStoryboard];
}

- (UIStoryboard *)getMainStoryboard
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    return storyBoard;
}


- (void)getCourierStoryboard
{
    _courierStoryboard = [UIStoryboard storyboardWithName:@"Courier" bundle:nil];
}

- (void)getCourier1Storyboard
{
    _courier1Storyboard = [UIStoryboard storyboardWithName:@"Courier1" bundle:nil];
}

//Navigation Controller
- (void)navigationController
{
    _navController = [self getNavigationController];
    [self setRootViewControllerForWindow:_navController];
}

- (UINavigationController *)getNavigationController
{
    UINavigationController *navController = [[self getMainStoryboard] instantiateViewControllerWithIdentifier:@"MainNavigationController"];
    return navController;
}

- (void)setRootViewControllerForWindow: (UIViewController*)viewController
{
    self.appWindow.rootViewController = viewController;
    self.appWindow.hidden = false;
}

//Tabbar Controller
- (void)loadTabbarController
{
    _senderTabbar = (UITabBarController *)[[self getMainStoryboard] instantiateViewControllerWithIdentifier:@"MainTabbarController"];
    [_senderTabbar setSelectedIndex:0];
    [_senderTabbar setRestorationIdentifier:@"sender"];
    [_navController pushViewController:_senderTabbar animated:YES];
}

//Controllers methods

- (void)showSignUpIDCardVC:(BOOL)isComingfromProfile
{
    SignUpIDCardVC *idCardView = [_courier1Storyboard instantiateViewControllerWithIdentifier:@"SignUpIDCardVC"];
    idCardView.isComingFromProfile = isComingfromProfile;
    [_navController pushViewController:idCardView
                              animated:YES];
}

- (void)showTransportTypeVC:(BOOL)isComingfromProfile isComingFromVehicleView:(BOOL)isComingFromVehicleView
{
    TransportationTypesView *transportationTypesView = [_courier1Storyboard instantiateViewControllerWithIdentifier:@"TransportationTypesView"];
    transportationTypesView.isComingFromProfile = isComingfromProfile;
    transportationTypesView.isComingFromVehicleView = isComingFromVehicleView;
    [_navController pushViewController:transportationTypesView
                              animated:YES];
}

//AddTransportVC
- (void)showAddTransportView:(BOOL)isComingfromProfile Transport:(GetTransports *)transport
{
    AddTransportVC *addTransportView = [_courier1Storyboard instantiateViewControllerWithIdentifier:@"AddTransportVC"];
    addTransportView.transport = transport;
    addTransportView.isComingFromProfile = isComingfromProfile;
    [_navController pushViewController:addTransportView
                              animated:YES];
}

- (void) showVehiclesVC:(BOOL)isComingfromProfile
{
    VehiclesVC *vehicles = [_courier1Storyboard instantiateViewControllerWithIdentifier:@"VehiclesVC"];
    vehicles.isComingFromProfile = isComingfromProfile;
    [_navController pushViewController:vehicles
                              animated:YES];
}

- (void)showVerificationConfirmationnView
{
    VerificationConfirmationVC *termsView = [_courier1Storyboard instantiateViewControllerWithIdentifier:@"VerificationConfirmationVC"];
    [_navController pushViewController:termsView
                              animated:YES];
}


- (void)showVerificationView
{
    VerificationVC *termsView = [_courier1Storyboard instantiateViewControllerWithIdentifier:@"VerificationVC"];
    [_navController pushViewController:termsView
                              animated:YES];
}
- (void)showCourierRuleView
{
    CourierrulesController *termsView = [_courier1Storyboard instantiateViewControllerWithIdentifier:@"CourierrulesController"];
    [_navController pushViewController:termsView
                              animated:YES];
}


- (void)showInsuranceCardView:(BOOL)isComingfromProfile
{
    SignUpInsuranceCardVC *insuranceCardVC = [_courier1Storyboard instantiateViewControllerWithIdentifier:@"SignUpInsuranceCardVC"];
    insuranceCardVC.isComingFromProfile = isComingfromProfile;
    [_navController pushViewController:insuranceCardVC
                              animated:YES];
}

- (void)showTakePhotoView:(BOOL)isComingfromProfile
{
    SignUpTakePhotoVC *signUpTakePhoto = [_courier1Storyboard instantiateViewControllerWithIdentifier:@"SignUpTakePhotoVC"];
    signUpTakePhoto.isComingFromProfile = isComingfromProfile;
    [_navController pushViewController:signUpTakePhoto
                              animated:YES];
}

- (void) showTutorialView
{
    TutorialViewController *tutorialView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
    [_navController pushViewController:tutorialView
                              animated:YES];
}

- (void) showSplashView
{
    SplashViewController *splashView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"SplashViewController"];
    [_navController pushViewController:splashView
                              animated:YES];
}

- (void) showStartupView
{
    StartUpViewController *startupView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"StartUpViewController"];
    [_navController pushViewController:startupView
                              animated:YES];
}

- (void) showLoginView
{
    LoginViewController *loginView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [_navController pushViewController:loginView
                              animated:YES];
}

- (void) showRegisterView
{
    RegisterViewController *registerView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [_navController pushViewController:registerView
                              animated:YES];
}

- (void) showResetPasswordView:(BOOL)isComingFromResetPassword
{
    ResetPasswordViewController *resetPassword = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"ResetPasswordViewController"];
    resetPassword.isComingFromChangePassword = isComingFromResetPassword;
    [_navController pushViewController:resetPassword
                              animated:YES];
}

- (void) showContinueAsView
{
    ContinueAaViewController *continueAsView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"ContinueAaViewController"];
    [_navController pushViewController:continueAsView
                              animated:YES];
}

- (void) showSendingDetailView:(SenderData *)senderData PackId:(NSString *)packId FromReceiver:(BOOL) fromreceiver
{
    SendingDetailViewController *sendingDetailView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"SendingDetailVC"];
    sendingDetailView.senderData = senderData;
    sendingDetailView.packId = packId;
    sendingDetailView.fromReceiver = fromreceiver;
    [_navController pushViewController:sendingDetailView
                              animated:YES];
}

- (void) showSendingView
{
    SendingsVC *sendingsVC = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"SendingsVC"];
    [_navController pushViewController:sendingsVC
                              animated:YES];
}

- (void) showRateOrderView:(PackDetailsModel *)packDetails
{
    RateOrderViewController *rateOrderView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"RateOrderVC"];
    rateOrderView.packDetails = packDetails;
    [_navController pushViewController:rateOrderView
                              animated:YES];
}

- (void)showCourierProfileView:(PackDetailsModel *)packDetails CourierId:(CourierConversations *)convesation isComingFrom:(BOOL)isComingFromChat
{
    CourierProfileViewController *courierProfileView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"CourierProfileVC"];
    courierProfileView.packDetails = packDetails;
    courierProfileView.conversation = convesation;
    courierProfileView.isComingFromChatDetails = isComingFromChat;
    [_navController pushViewController:courierProfileView
                              animated:YES];
}

- (void)showBidsView
{
    BidsViewController *bidsView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"BidsVC"];
    [_navController pushViewController:bidsView
                              animated:YES];
}

- (void)showStatusView:(PackDetailsModel *)packDetail
{
//    BidsViewController *bidsView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"BidsVC"];
//    [_navController pushViewController:bidsView
//                              animated:YES];
    SendingStatusViewController *statusView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"SendingStatusVC"];
    statusView.packDetails = packDetail;
    [_navController pushViewController:statusView animated:NO];
}

- (void)showStatusImageDetail
{
}

- (void)showAcceptedCourierView
{
    AcceptedCourierViewController *acceptedCourierView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"AcceptedCourierVC"];
    [_navController pushViewController:acceptedCourierView
                              animated:YES];
}

- (void)showTermsView:(NSString *)ruleType
{
    TermsViewController *termsView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"TermsVC"];
    termsView.ruleType = ruleType;
    [_navController pushViewController:termsView
                              animated:YES];
}

- (void)showOrderCancelReasonView
{
    OrderCancelReasonViewController *orderCancelReasonView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"OrderCancelReasonView"];
    [_navController pushViewController:orderCancelReasonView animated:YES];
    
}

- (void)showReportCourierView:(NSString *)packId
{
    ReportVC *reportCourierView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"ReportVC"];
    reportCourierView.packId = packId;
    [_navController pushViewController:reportCourierView animated:YES];
    
}

- (void)showPaymentsMadeView
{
    PaymentVC *paymentView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"PaymentVC"];
    [_navController pushViewController:paymentView animated:YES];
}

- (void)showPaymentToDoViewController
{
    PayementConfirmationViewController *paymentConfirmationView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"PayementConfirmationViewController"];
    [_navController pushViewController:paymentConfirmationView animated:YES];
}

- (void)showSenderSignUpConfirmVC
{
    SenderSignUpConfirmVC *senderSignUpConfirmVC = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"SenderSignUpConfirmVC"];
    [_navController pushViewController:senderSignUpConfirmVC animated:YES];
}

- (void)showSenderResetPassConfirmVC:(BOOL)isComingFromResetPassword
{
    SenderResetPassConfirmVC *senderResetPassConfirmVC = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"SenderResetPassConfirmVC"];
    senderResetPassConfirmVC.isComingFromChangePassword = isComingFromResetPassword;
    [_navController pushViewController:senderResetPassConfirmVC
                              animated:YES];
}

//Tabbar Controllers
- (void) showNewOrderView:(BOOL)isBackHidden
{
    NewOrderVC *newOrder = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"NewOrderVC"];
    newOrder.isBackHidden = isBackHidden;
    [_navController pushViewController:newOrder
                              animated:YES];
}

- (void) showNewOrderPetInfoView:(NSString *)packId SenderData:(SenderData *)senderData
{
    NewOrderPetInfoVC *newOrderPet = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"NewOrderPetInfoVC"];
    newOrderPet.packId = packId;
    newOrderPet.draftdata = senderData;
    [_navController pushViewController:newOrderPet
                              animated:YES];
}

- (void) showNewOrderPackInfoView:(NSString *)packId Send_Cat_ID:(NSString *)send_cat_id SenderData:(SenderData *)senderData isBackHidden :(BOOL)isBackHidden
{
    NewOrderPackInfoVC *newOrderPack = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"NewOrderPackInfoVC"];
    newOrderPack.packId = packId;
    newOrderPack.draftdata = senderData;
    newOrderPack.isBackHidden = isBackHidden;
    newOrderPack.send_Cat_Id = send_cat_id;
    [_navController pushViewController:newOrderPack
                              animated:YES];
}

- (void) showNewOrderDestinationView:(NSString *)packId PackPrice:(NSString*)packPrice Send_Cat_ID:(NSString *)send_cat_id ItemPrice:(NSString *)itemprice SenderData:(SenderData *)senderData
{
    OrderDestinationVC *orderDestination = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"OrderDestinationVC"];
    orderDestination.packId = packId;
    orderDestination.draftdata = senderData;
    orderDestination.price = packPrice;
    orderDestination.send_cat_id = send_cat_id;
    orderDestination.item_value = itemprice;
    [_navController pushViewController:orderDestination
                              animated:YES];
}

- (void) showNewOrderDestinationView_fastorder:(NSString *)distance DistanceTime:(NSString *)distanceTime From:(CSUser *)from To:(CSUser *)to Pack_Size_ID:(NSString *)pack_size_id Promo_Code:(NSString *)promo_code Item_Value:(NSString *)item_value PackPrice:(NSString *)packPrice Send_Cat_ID:(NSString *)send_cat_id Waiting_Mins:(NSString *)waiting_mins Default_Tariff_Plan:(NSString *)default_tariff_plan SenderData:(SenderData *)senderData
{
    OrderDestinationVC *orderDestination = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"OrderDestinationVC"];
    orderDestination.draftdata = senderData;
    orderDestination.from_user = from;
    orderDestination.to_user = to;
    orderDestination.distance = distance;
    orderDestination.distanceTime = distanceTime;
    orderDestination.pack_size_id = pack_size_id;
    orderDestination.promo_code = promo_code;
    orderDestination.item_value = item_value;
    orderDestination.price = packPrice;
    orderDestination.send_cat_id = send_cat_id;
    orderDestination.waiting_time = waiting_mins;
    orderDestination.default_tariff_plan = default_tariff_plan;
    [_navController pushViewController:orderDestination animated:YES];
    
}

- (void)ShowChatDetailView:(CourierConversations *)conversation PackDetails:(PackDetailsModel *)packDetails {
    
    ChatDetailVC *chatView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"ChatDetailVC"];
    chatView.conversation = conversation;
    chatView.packDetails = packDetails;
    [_navController pushViewController:chatView
                              animated:YES];
}

- (void)showViewOnMap:(NSString *)comingView PackDetails:(PackDetailsModel *)packDetail isComingFromCourierApp:(BOOL)isComingFromCourierApp {
    
    ViewOnMapVC *viewOnMap = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"ViewOnMapVC"];
    viewOnMap.comingView = comingView;
    viewOnMap.packDetails = packDetail;
    viewOnMap.isComingFromCourierApp = isComingFromCourierApp;
    [_navController pushViewController:viewOnMap
                              animated:YES];
}

- (void)showPersonalEditView
{
    PersonalInfoWithEditVC *personalEditView = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"PersonalInfoWithEditVC"];
    [_navController pushViewController:personalEditView
                              animated:YES];
}

- (void)showComingSoonView
{
    ComingSoonVC *comingSoonVC = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"ComingSoonVC"];
    [_navController pushViewController:comingSoonVC
                              animated:YES];
}

- (void)showSenderFeedbackView
{
    SenderFeedBackVC *senderFeedBackVC = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"SenderFeedBackVC"];
    [_navController pushViewController:senderFeedBackVC
                              animated:YES];
}

- (void)showSenderSettingsView
{
    SenderSettingsVC *senderSettingsVC = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"SenderSettingsVC"];
    [_navController pushViewController:senderSettingsVC
                              animated:YES];
}

- (void)showAddPaymentView
{
    AddPaymentMethodVC *addPaymentMethodVC = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"AddPaymentMethodVC"];
    [_navController pushViewController:addPaymentMethodVC
                              animated:YES];
}

- (void)showConfirmPaymentView:(NSString *)packId AskToFriend:(Boolean)asktofriend FastOrder:(Boolean)fastorder
{
    ConfirmPaymentVC *confirmPaymentVC = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"ConfirmPaymentVC"];
    confirmPaymentVC.packId = packId;
    confirmPaymentVC.asktofriend = asktofriend;
    confirmPaymentVC.fastOrder = fastorder;
    [_navController pushViewController:confirmPaymentVC
                              animated:YES];
}

- (void)showSenderChangePasswordVC
{
    SenderChangePasswordVC *senderChangePasswordVC = [_mainStoryBoard instantiateViewControllerWithIdentifier:@"SenderChangePasswordVC"];
    [_navController pushViewController:senderChangePasswordVC
                              animated:YES];
}

//******************************************Couriers******************************************

- (void)showSignUpCourierView {
    
    SignUpCourierVC *signUpCourier = [self.courier1Storyboard instantiateViewControllerWithIdentifier:@"SignUpCourierVC"];
    [_navController pushViewController:signUpCourier
                              animated:YES];
}

//Courier Tabbar Controller
- (void)loadCourierTabbarController
{
    _courierTabbar = (UITabBarController *)[_courierStoryboard instantiateViewControllerWithIdentifier:@"CourierTabbarController"];
    [_courierTabbar setSelectedIndex:0];
    [_courierTabbar setRestorationIdentifier:@"courier"];
    [_navController pushViewController:_courierTabbar animated:YES];
}

- (void)showChatCourierDetails:(CourierConversations *)conversation PackDetails:(PackDetailsModel *)packDetails {
    
    ChatCourierDetailVC *chatCourierDetail = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"ChatCourierDetailVC"];
    chatCourierDetail.conversation = conversation;
    chatCourierDetail.packDetails = packDetails;
    [_navController pushViewController:chatCourierDetail
                              animated:YES];
}

- (void)showCourierSendingsDetail:(NSString *)packId {
    
    CourierSendingDetailVC *courierSendingDetailVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CourierSendingDetailVC"];
    courierSendingDetailVC.packId = packId;
    [_navController pushViewController:courierSendingDetailVC
                              animated:YES];
}

- (void)showWithdrawHistoryView {
    
    WithdrawHistoryVC *withdrawHistoryVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"WithdrawHistoryVC"];
    [_navController pushViewController:withdrawHistoryVC
                              animated:YES];
}

- (void)showCourierPersonalInfoEdit {
    
    CouriererPesonalInfoWithEditVC *couriererPesonalInfoWithEditVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CouriererPesonalInfoWithEditVC"];
    [_navController pushViewController:couriererPesonalInfoWithEditVC
                              animated:YES];
}

- (void)showAccountInfoView
{
    AccountInfoVC *accountInfoVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"AccountInfoVC"];
    [_navController pushViewController:accountInfoVC
                              animated:YES];
}

- (void)showBankAccountInfo1View:(BankAccountDetail *)bankAccountDetails
{
    BankAccountInfo1VC *bankAccountInfo1VC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"BankAccountInfo1VC"];
    bankAccountInfo1VC.bankAccountDetails = bankAccountDetails;
    [_navController pushViewController:bankAccountInfo1VC
                              animated:YES];
}

- (void)showBankAccountInfo2View
{
    BankAccountInfo2VC *bankAccountInfo2VC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"BankAccountInfo2VC"];
    [_navController pushViewController:bankAccountInfo2VC
                              animated:YES];
}

- (void)showCourierSettingsView
{
    CourierSettingsVC *courierSettingsVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CourierSettingsVC"];
    [_navController pushViewController:courierSettingsVC
                              animated:YES];
}

- (void)showCourierSettingsNotifyView
{
    CourierSettingsNotificationsVC *courierSettingsNotificationsVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CourierSettingsNotificationsVC"];
    [_navController pushViewController:courierSettingsNotificationsVC
                              animated:YES];
}

- (void)showSaveRoutView:(NSDictionary *)route
{
    SaveMyRouteView *saveMyRouteView = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"SaveMyRouteView"];
    saveMyRouteView.routeDict = route;
    [_navController pushViewController:saveMyRouteView
                              animated:YES];
}

- (void)showCourierFeedbackView
{
    CourierFeedbackVC *courierFeedbackVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CourierFeedbackVC"];
    [_navController pushViewController:courierFeedbackVC
                              animated:YES];
}

- (void)showCourierRatingsView
{
    CourierRatingsVC *courierRatingsVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CourierRatingsVC"];
    [_navController pushViewController:courierRatingsVC
                              animated:YES];
}

- (void)showCourierFAQView:(NSString *)appUserType
{
    CourierFAQVC *courierFAQVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CourierFAQVC"];
    courierFAQVC.currentAppUserType = appUserType;
    [_navController pushViewController:courierFAQVC
                              animated:YES];
}

- (void)showCourierPopularQuestionView:(NSString *)faqID From:(NSString *)from
{
    CourierPopularQuestionsVC *courierPopularQuestionsVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CourierPopularQuestionsVC"];
    courierPopularQuestionsVC.faqId = faqID;
    courierPopularQuestionsVC.from = from;
    [_navController pushViewController:courierPopularQuestionsVC
                              animated:YES];
}

- (void)showcourierPersonalInfoVC
{
    CourierPersonalInfoVC *courierPersonalInfoVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CourierPersonalInfoVC"];
    [_navController pushViewController:courierPersonalInfoVC
                              animated:YES];
}

- (void)showSenderProfileVC:(PackDetailsModel *)courierListData Conversation:(CourierConversations *)conversation isComingFromChat:(BOOL)fromChatDetails
{
    SenderProfileVC *senderProfileVC = [self.mainStoryBoard instantiateViewControllerWithIdentifier:@"SenderProfileVC"];
    senderProfileVC.courierListData = courierListData;
    senderProfileVC.conversation = conversation;
    senderProfileVC.isComingFromChatDetails = fromChatDetails;
    [_navController pushViewController:senderProfileVC
                              animated:YES];
}

- (void)showCourierChangePassVC
{
    CourierChangePassVC *courierChangePassVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CourierChangePassVC"];
    [_navController pushViewController:courierChangePassVC
                              animated:YES];
}

- (void)showCourierPayoutDetails:(NSString *)payoutId
{
    CourierPayoutDetailVC *courierPayoutDetailVC = [self.courierStoryboard instantiateViewControllerWithIdentifier:@"CourierPayoutDetailVC"];
    courierPayoutDetailVC.payoutId  = payoutId;
    [_navController pushViewController:courierPayoutDetailVC
                              animated:YES];
}


@end
