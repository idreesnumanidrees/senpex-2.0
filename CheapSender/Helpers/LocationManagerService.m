//
//  CurrentLocationManager.m
//  OTW
//
//  Created by admin on 1/11/17.
//  Copyright © 2017 admin. All rights reserved.
//

#import "LocationManagerService.h"
#import "AppDelegate.h"
#import "UtilHelper.h"

@implementation LocationManagerService

//Calls of basic methods of CLLocationManger
- (void)initialize
{
    //Allocation and Initialization of CLLocationManger class
    _locationManager = [CLLocationManager new];
    
    //Allocation and Initialization of CLGeocoder class
    _geocoder = [CLGeocoder new];
    
    //Assigning delegate methods
    self.locationManager.delegate = self;
    
    //Requests permission to use location service whenever the app is running
    [self.locationManager requestAlwaysAuthorization];
    
    //The accuracy of the location data.
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    //Starts the generation of updates that report the user’s current location.x
    [self.locationManager setAllowsBackgroundLocationUpdates:YES];
    [self.locationManager startUpdatingLocation];
}

- (void)startUpdatingLocation
{
    [self.locationManager startUpdatingLocation];
    
    //The check for ios version, if version is greater than 9.0.0 then it will true for allows background location updates. This is a new addition for ios9 and later.
    if ([NSProcessInfo.processInfo isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){9,0,0}]) {
        self.locationManager.allowsBackgroundLocationUpdates = YES;
    }
}

- (void)stopUpdatingLocation
{
    [self.locationManager stopUpdatingLocation];
    
    //The check for ios version, if version is greater than 9.0.0 then it will true for allows background location updates. This is a new addition for ios9 and later.
    if ([NSProcessInfo.processInfo isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){9,0,0}]) {
        self.locationManager.allowsBackgroundLocationUpdates = NO;
    }
}

#pragma mark -
#pragma mark - Location Manager delegates

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    //WE ARE ASSUMING THIS METHOD GETS CALLED ONLY WHEN LOCATION GETS UPDATED
    //DUE TO WHICH WE DONT CHECK FOR LOCATION CHANGE CONDITION TO PROCEED SHOWING PIN POINT IN MAP
    //ie WE UPDATE PIN WHENEVER THIS METHOD GETS CALLED REGARDLESS OF CHECKING IF THER IS ANY CHANGE IN COORDINATES NEWLY RECEIVED WITH THE PREVIOUS
    
    CLLocation *location = [[locations lastObject] copy];
    if (location.coordinate.latitude != 0 && location.coordinate.longitude != 0) {
        
//        NSLog(@"Current location received!");
        
        //The check for existance of current location delegate property
        if(_locationManagerDelagete != nil && [_locationManagerDelagete respondsToSelector:@selector(notifyToMapViewWithLocation:)])
        {
            //Sends the current location to delegate method
            [_locationManagerDelagete notifyToMapViewWithLocation:location];
            
            //Reverse Geocoding for country code
//            [self.geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
//             {
//                 if (placemarks == nil)
//                     return;
//                 CLPlacemark *placeMark = [placemarks objectAtIndex:0];
//                 [AppDelegate sharedAppDelegate].countryCode = [placeMark ISOcountryCode];
//             }];
        }
    }
    else {
        
//        NSLog(@"Location not found - Lat and Lng are zero!");
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    // NSLog(@"Location manager error: %@", error.localizedDescription);
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self.locationManager startUpdatingLocation];
        
        //The check for ios version, if version is greater than 9.0.0 then it will true for allows background location updates. This is a new addition for ios9 and later.
        if ([NSProcessInfo.processInfo isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){9,0,0}]) {
            self.locationManager.allowsBackgroundLocationUpdates = YES;
        }
    }
    else if (status == kCLAuthorizationStatusDenied)
    {
        if(_locationManagerDelagete != nil && [_locationManagerDelagete respondsToSelector:@selector(notifyOnGPSDisable)])
        {
            //Notify to MapView on disabled GPS
            [_locationManagerDelagete notifyOnGPSDisable];
        }
    }
    else if (status == kCLAuthorizationStatusAuthorizedAlways)
    {
        [self.locationManager startUpdatingLocation];
        
        //The check for ios version, if version is greater than 9.0.0 then it will true for allows background location updates. This is a new addition for ios9 and later.
        if ([NSProcessInfo.processInfo isOperatingSystemAtLeastVersion:(NSOperatingSystemVersion){9,0,0}]) {
            self.locationManager.allowsBackgroundLocationUpdates = YES;
        }
    }
    else if (status == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestAlwaysAuthorization];
    }
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    //Starts the generation of updates that report the user’s current location.
    [manager startUpdatingLocation];
}

@end
