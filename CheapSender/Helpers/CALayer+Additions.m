
#import "CALayer+Additions.h"

//@implementation CALayer (IBConfiguration)
//
//-(void)setBorderIBColor:(UIColor*)color
//{
//    self.borderColor = color.CGColor;
//}
//
//-(UIColor*)borderIBColor
//{
//    return [UIColor colorWithCGColor:self.borderColor];
//}
//
//-(void)setShadowIBColor:(UIColor*)color
//{
//    self.shadowColor = color.CGColor;
//}
//
//-(UIColor*)shadowIBColor
//{
//    return [UIColor colorWithCGColor:self.shadowColor];
//}
//
//@end

@implementation CALayer (Additions)

- (void)setBorderIBColor:(UIColor *)color
{
    self.borderColor = color.CGColor;
}

@end
