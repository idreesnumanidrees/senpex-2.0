//
//  PushNotificationHandler.h
//  CheapSender
//
//  Created by Idrees on 2017/08/02.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseObject.h"

@interface PushNotificationHandler : BaseObject

+ (void)GetPushNotificationInfo:(NSDictionary *)userInfo;

@end
