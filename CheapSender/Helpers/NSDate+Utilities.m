//
//  NSDate+Utilities.m
//  Track
//
//  Created by Asim Hafeez on 9/30/14.
//  Copyright (c) 2014 EnterMarkets. All rights reserved.
//

#import "NSDate+Utilities.h"

@implementation NSDate (Utilities)

+ (NSDate *) dateFromDotNet:(NSString *)stringDate
{
    NSDate *returnValue;
    if ([stringDate isMemberOfClass:[NSNull class]])
    {
        returnValue=nil;
    }
    else
    {
        NSInteger offset = [[NSTimeZone defaultTimeZone] secondsFromGMT];
        returnValue= [[NSDate dateWithTimeIntervalSince1970:
                       [[stringDate substringWithRange:NSMakeRange(6, 10)] intValue]]
                      dateByAddingTimeInterval:offset];
    }
    return returnValue;
}


+ (NSDate *) dateFromDotNetInUTC:(NSString*)stringDate
{
    NSDate *returnValue;
    if ([stringDate isMemberOfClass:[NSNull class]])
    {
        returnValue=nil;
    }
    else
    {
        NSInteger offset = [[NSTimeZone timeZoneWithName:@"UTC"] secondsFromGMT];
        returnValue= [[NSDate dateWithTimeIntervalSince1970:
                       [[stringDate substringWithRange:NSMakeRange(6, 10)] intValue]]
                      dateByAddingTimeInterval:offset];
    }
    return returnValue;
}

+ (NSDate *)dateFromDateString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];

    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
}

+ (NSDate *)dateFromDateStringInLocalTimeZone:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    
    [dateFormatter setDateFormat: @"yyyy-MM-dd HH:mm:ss"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
}

+ (NSDate *)justDateFromDateString:(NSString *)dateString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    [dateFormatter setDateFormat: @"yyyy-MM-dd"];
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    return destDate;
}

- (NSString *) dateToDotNet
{
    double timeSince1970=[self timeIntervalSince1970];
    NSInteger offset = [[NSTimeZone defaultTimeZone] secondsFromGMT];
    offset=offset/3600;
    double nowMillis = 1000.0 * (timeSince1970);
    NSString *dotNetDate=[NSString stringWithFormat:@"/Date(%.0f%+03ld00)/",nowMillis,(long)offset] ;
    return dotNetDate;
}

- (NSString*) convertUTCToLocalTimeZone
{
    NSDate* ts_utc = self;
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    [df_utc setDateFormat:@"EEEE, dd MMM yyy, hh:mm aa"];
    
//    if ([[[NSUserDefaults standardUserDefaults] valueForKey:APP_LANGUAGE_CODE] isEqualToString:ENGLISH]) {
    
        [df_utc setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
//    }
//    else
//    {
//        [df_utc setLocale:[NSLocale localeWithLocaleIdentifier:@"ar"]];
//    }
    
    NSString* ts_local_string = [df_utc stringFromDate:ts_utc];
    return ts_local_string;
}


-(NSString *)getMMDDYYYYDate
{
    //    NSString *timeComponentFromDateString;
    //    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    //    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    [df_utc setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [df_utc setDateFormat:@"MM-dd-yyyy"];
    
    //    [df_utc setDateFormat:@"E d MMM hh:mm a"];
    //Sun 23 Aug 10:05 AM
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    // NSLog(@"convertUTCToLocalTimeZone: ts_utc_string is %@", ts_utc_string);
    
    return ts_utc_string;
}


-(NSString *)getDateOnly
{
    //    NSString *timeComponentFromDateString;
    //    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    //    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    [df_utc setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    [df_utc setDateFormat:@"dd"];
    
    //    [df_utc setDateFormat:@"E d MMM hh:mm a"];
    //Sun 23 Aug 10:05 AM
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    // NSLog(@"convertUTCToLocalTimeZone: ts_utc_string is %@", ts_utc_string);
    
    return ts_utc_string;
}


-(NSString *)getYYYYMMDDDate
{
    //    NSString *timeComponentFromDateString;
    //    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    //    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    [df_utc setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    [df_utc setDateFormat:@"yyyy-MM-dd"];
    
    //    [df_utc setDateFormat:@"E d MMM hh:mm a"];
    //Sun 23 Aug 10:05 AM
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    // NSLog(@"convertUTCToLocalTimeZone: ts_utc_string is %@", ts_utc_string);
    
    return ts_utc_string;
}

//Get Date In UTC

- (NSString *)getYYYYMMDDDateInUTC
{
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [df_utc setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [df_utc setDateFormat:@"yyyy-MM-dd"];
    
    //    [df_utc setDateFormat:@"E d MMM hh:mm a"];
    //Sun 23 Aug 10:05 AM
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    // NSLog(@"convertUTCToLocalTimeZone: ts_utc_string is %@", ts_utc_string);
    
    return ts_utc_string;
    
}
-(NSString *)getTimeFromDate
{
    //    NSString *timeComponentFromDateString;
    //    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    //    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    [df_utc setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    [df_utc setDateFormat:@"E hh:mm a"];
    
    //    [df_utc setDateFormat:@"E d MMM hh:mm a"];
    //Sun 23 Aug 10:05 AM
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    // NSLog(@"convertUTCToLocalTimeZone: ts_utc_string is %@", ts_utc_string);
    
    return ts_utc_string;
}

- (NSString *)getTimeFromDateWithoutDay
{
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    [df_utc setDateFormat:@"hh:mm a"];
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    return ts_utc_string;
}

- (NSString *)getTimeForWeb
{
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    [df_utc setDateFormat:@"HH:mm:ss"];
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    return ts_utc_string;
}

- (NSString *)getDateTimeInLocalTimeZone
{
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    
    [df_utc setDateFormat:@"MM-dd-yyyy HH:mm:ss"];
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    
    return ts_utc_string;
}

- (NSString *)getDateTimeInUTC
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:self];
    return dateString;
    
    
//    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
//    [df_utc setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
//    
//    [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
//    NSString* ts_utc_string = [df_utc stringFromDate:self];
//    
//    return ts_utc_string;
}

//New Added
- (NSString *)getTimeInUTC
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"hh:mm a"];
    NSString *dateString = [dateFormatter stringFromDate:self];
    return dateString;
    
    
    //    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    //    [df_utc setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    //
    //    [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    //    NSString* ts_utc_string = [df_utc stringFromDate:self];
    //
    //    return ts_utc_string;
}

- (NSString *)getTimeFromDateWithoutDayAndUTC
{
    //    NSString *timeComponentFromDateString;
    //    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    //    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    //    [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    
    [df_utc setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    [df_utc setDateFormat:@"hh:mm a"];
    
    //    [df_utc setDateFormat:@"E d MMM hh:mm a"];
    //Sun 23 Aug 10:05 AM
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    // NSLog(@"convertUTCToLocalTimeZone: ts_utc_string is %@", ts_utc_string);
    
    return ts_utc_string;
}

- (NSString *)getTimeFromDateWithoutDayIn24HrFormat
{
    //    NSString *timeComponentFromDateString;
    //    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    //    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    
    
    [df_utc setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    
    [df_utc setDateFormat:@"HH:mm"];
    
    //    [df_utc setDateFormat:@"E d MMM hh:mm a"];
    //Sun 23 Aug 10:05 AM
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    // NSLog(@"convertUTCToLocalTimeZone: ts_utc_string is %@", ts_utc_string);
    
    return ts_utc_string;
}

- (NSString *)getTimeDayFromDate
{
    //    NSString *timeComponentFromDateString;
    //    NSDateFormatter* df_local = [[NSDateFormatter alloc] init];
    //    [df_local setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    
    //    [df_utc setDateFormat:@"dd/MM/yyy - E"];
    [df_utc setDateFormat:@"EE, d LLL"];
    
    //newly added Riyas 16 sep 2017
   // [df_utc setDateStyle:NSDateFormatterMediumStyle];

    
    //[df_utc setDateFormat:@"E hh:mm a"];
    
    //    [df_utc setDateFormat:@"E d MMM hh:mm a"];
    //Sun 23 Aug 10:05 AM
    NSString* ts_utc_string = [df_utc stringFromDate:self];
    // NSLog(@"convertUTCToLocalTimeZone: ts_utc_string is %@", ts_utc_string);
    
    return ts_utc_string;
}


- (NSDate *)convertToUTC
{
    NSDate* ts_utc = self;
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone systemTimeZone]];
    [df_utc setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSString* ts_utc_string = [df_utc stringFromDate:ts_utc];
    
    NSDate *utc_date = [df_utc dateFromString:ts_utc_string];
    
    return utc_date;
}


+ (NSDate *)convertToUTCWithMilliSecond:(NSString *)stringDate
{
    
    NSDateFormatter* df_utc = [[NSDateFormatter alloc] init];
    [df_utc setTimeZone:[NSTimeZone localTimeZone]];
    [df_utc setDateFormat:@"yyyy.MM.dd HH:mm:ss"];

    NSDate *utc_date = [df_utc dateFromString:stringDate];
    
    return utc_date;
}

+ (NSDate *)combineDate:(NSString *)date1 dateWithTime:(NSDate *)date2
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date = [dateFormat dateFromString:date1];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitDay|NSCalendarUnitMonth|NSCalendarUnitYear fromDate:date];
    NSDateComponents *timeComponents = [calendar components:NSCalendarUnitHour|NSCalendarUnitMinute fromDate:date2];
    
    NSDateComponents *newComponents = [[NSDateComponents alloc]init];
    newComponents.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    newComponents.timeZone = [NSTimeZone localTimeZone];
    [newComponents setDay:[dateComponents day]];
    [newComponents setMonth:[dateComponents month]];
    [newComponents setYear:[dateComponents year]];
    [newComponents setHour:[timeComponents hour]];
    [newComponents setMinute:[timeComponents minute]];
    
    NSDate *combDate = [calendar dateFromComponents:newComponents];
    
    return combDate;
}

+ (NSString *)relativeDateStringForDate:(NSDate *)date
{
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear;
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    
    if (components.year > 0) {
        return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
    } else if (components.month > 0) {
        return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
    } else if (components.weekOfYear > 0) {
        return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
    } else if (components.day > 0) {
        if (components.day > 1) {
            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
        } else {
            return @"Yesterday";
        }
    } else {
        return @"Today";
    }
}



@end
