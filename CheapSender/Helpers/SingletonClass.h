//
//  SingletonClass.h
//  CheapSender
//
//  Created by Idrees on 2017/06/05.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"

@interface SingletonClass : NSObject

+ (SingletonClass *)sharedInstance;

@property (nonatomic, strong) UserModel *user;


@end
