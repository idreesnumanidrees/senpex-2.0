//
//  NSDate+Utilities.h
//  Track
//
//  Created by Asim Hafeez on 9/30/14.
//  Copyright (c) 2014 EnterMarkets. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface NSDate (Utilities)

+(NSDate*) dateFromDotNet:(NSString*)stringDate;
+(NSDate*) dateFromDotNetInUTC:(NSString*)stringDate;
-(NSString*) dateToDotNet;
-(NSString*) convertUTCToLocalTimeZone;
-(NSString *)getTimeFromDate;
-(NSString *)getTimeFromDateWithoutDay;
-(NSString *)getTimeFromDateWithoutDayIn24HrFormat;

- (NSDate *)convertToUTC;
+ (NSDate *)convertToUTCWithMilliSecond:(NSString *)stringDate;
- (NSString *)getTimeDayFromDate;

+ (NSDate *)combineDate:(NSString *)date1 dateWithTime:(NSDate *)date2;
- (NSString *)getTimeFromDateWithoutDayAndUTC;

+ (NSDate *)dateFromDateString:(NSString *)dateString;
+ (NSDate *)dateFromDateStringInLocalTimeZone:(NSString *)dateString;
+ (NSDate *)justDateFromDateString:(NSString *)dateString;

+ (NSString *)relativeDateStringForDate:(NSDate *)date;
-(NSString *)getMMDDYYYYDate;
-(NSString *)getYYYYMMDDDate;
-(NSString *)getYYYYMMDDDateInUTC;//Added for UTC
-(NSString *)getDateOnly;

- (NSString *)getDateTimeInLocalTimeZone;
- (NSString *)getDateTimeInUTC;
- (NSString *)getTimeForWeb;

//New Added
- (NSString *)getTimeInUTC;


@end
