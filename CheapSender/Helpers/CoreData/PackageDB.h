//
//  PackageDB.h
//  CheapSender
//
//  Created by Idrees on 2017/08/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseDB.h"

@interface PackageDB : BaseDB

@property (assign, nonatomic) int packId;
@property (assign, nonatomic) int timerValue;
@property (assign, nonatomic) NSDate *dateTime;


+ (void)insertAndupdateLastPackage:(int)packId timerValue:(int)timerValue dateTime:(NSDate *)dateTime IsUpdate:(BOOL)isUpdate;

+ (NSMutableArray *)getPackageList;

+ (BOOL)deletePackage:(int)packId;

+ (NSMutableArray *)fetchPackage:(int)packId;

@end
