//
//  LastChatMessageDB.m
//  CheapSender
//
//  Created by Idrees on 2017/08/05.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "LastChatMessageDB.h"

@implementation LastChatMessageDB

+ (void)insertAndupdateLastMessageWithId:(NSString *)userId message:(NSString *)message IsUpdate:(BOOL)isUpdare
{
    if (isUpdare) {
        
        ///An instance of NSFetchRequest describes search criteria used to retrieve data from a Persistent store.
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"LastChatMessage"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId == %d", [userId intValue]];
        fetchRequest.predicate = predicate;
        
        NSError *error;
        NSArray *fetchResults = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        //Check for count of array
        if(fetchResults.count > 0)
        {
            LastChatMessage *lastMessage = fetchResults[0];
            lastMessage.userId = [userId intValue];
            lastMessage.message = message;
        }
        [[AppDelegate sharedAppDelegate] saveContext];
    }
    else
    {
        LastChatMessage *lastChatMessage = [NSEntityDescription insertNewObjectForEntityForName:@"LastChatMessage"
                                                           inManagedObjectContext:[AppDelegate sharedAppDelegate].managedObjectContext];
        lastChatMessage.userId = [userId intValue];
        lastChatMessage.message = message;
        [[AppDelegate sharedAppDelegate] saveContext];
    }

}

//This method will return the ShareETAState object from database
+ (LastChatMessageDB *)getLastMessage:(NSString *)userId
{
    //An instance of NSFetchRequest describes search criteria used to retrieve data from a Persistent store.
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"LastChatMessage"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId == %d", [userId intValue]];
    fetchRequest.predicate = predicate;
    
    NSError *error;
    NSArray *fetchResults = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    LastChatMessageDB *lastMessageDB = [LastChatMessageDB new];
    
    //Check for count of array
    if(fetchResults.count > 0)
    {
        LastChatMessage *lastMessage = fetchResults[0];
        
        lastMessageDB.userId = [NSString stringWithFormat:@"%lld",lastMessage.userId];
        lastMessageDB.message = lastMessage.message;

    } else {
        
        lastMessageDB = nil;
    }
    
    return lastMessageDB;

}

@end
