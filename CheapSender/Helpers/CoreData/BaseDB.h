//
//  BaseDB.h
//  CheapSender
//
//  Created by Idrees on 2017/06/06.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheapSender+CoreDataModel.h"
#import "DataModels.h"
#import "AppDelegate.h"


@interface BaseDB : NSObject

@end
