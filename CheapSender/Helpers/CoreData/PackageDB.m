//
//  PackageDB.m
//  CheapSender
//
//  Created by Idrees on 2017/08/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "PackageDB.h"
#import "PackageInfo.h"

@implementation PackageDB

+ (void)insertAndupdateLastPackage:(int)packId timerValue:(int)timerValue dateTime:(NSDate *)dateTime IsUpdate:(BOOL)isUpdate
{
    if (isUpdate) {
        
        //        ///An instance of NSFetchRequest describes search criteria used to retrieve data from a Persistent store.
        //        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"LastChatMessage"];
        //        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId == %d", [userId intValue]];
        //        fetchRequest.predicate = predicate;
        //
        //        NSError *error;
        //        NSArray *fetchResults = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
        //
        //        //Check for count of array
        //        if(fetchResults.count > 0)
        //        {
        //            LastChatMessage *lastMessage = fetchResults[0];
        //            lastMessage.userId = [userId intValue];
        //            lastMessage.message = message;
        //        }
        //        [[AppDelegate sharedAppDelegate] saveContext];
    }
    else
    {
        Package *package = [NSEntityDescription insertNewObjectForEntityForName:@"Package" inManagedObjectContext:[AppDelegate sharedAppDelegate].managedObjectContext];
        
        package.packID = packId;
        package.timerValue = timerValue;
        package.dateTime = dateTime;
        [[AppDelegate sharedAppDelegate] saveContext];
    }
}

+ (NSMutableArray *)getPackageList
{
    NSMutableArray *packList = [NSMutableArray new];
    
    //Fetch results from coredata
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Package"];
    NSError *error = nil;
    NSArray *packagesDB = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:request error:&error];
    
    if (packagesDB.count > 0) {
        
        for (Package *package in packagesDB) {
            
            PackageInfo *packageInfo = [PackageInfo new];
            packageInfo.packId = (int)package.packID;
            packageInfo.timerValue = package.timerValue;
            packageInfo.dateTime = package.dateTime;
            [packList addObject:packageInfo];
        }
    }
    return packList;
}

+ (BOOL)deletePackage:(int)packId
{
    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Package"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"packID == %d", packId];
    fetchRequest.predicate = predicate;
    
    NSError *error;
    NSArray *fetchResults = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    for (NSManagedObject *managedObject in fetchResults) {
        NSLog(@"package deleted %d",packId);
        [[AppDelegate sharedAppDelegate].managedObjectContext deleteObject:managedObject];
    }
    
    [[AppDelegate sharedAppDelegate] saveContext];
    
    return YES;
}


+ (NSMutableArray *)fetchPackage:(int)packId
{
    NSMutableArray *packageArray = [NSMutableArray new];

    
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Package"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"packID == %d", packId];
    fetchRequest.predicate = predicate;
    
    NSError *error;
    NSArray *packagesDB = [[AppDelegate sharedAppDelegate].managedObjectContext
                           executeFetchRequest:fetchRequest
                           error:&error];
    
    if(packagesDB.count>0)
    {
        Package *package = packagesDB[0];
        PackageInfo *packageInfo = [PackageInfo new];
        packageInfo.packId = (int)package.packID;
        packageInfo.timerValue = package.timerValue;
        packageInfo.dateTime = package.dateTime;
        [packageArray addObject:packageInfo];
    }
    
    return packageArray;
}

@end
