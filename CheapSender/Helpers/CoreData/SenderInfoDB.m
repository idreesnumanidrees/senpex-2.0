//
//  SenderInfoDB.m
//  CheapSender
//
//  Created by Idrees on 2017/07/07.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SenderInfoDB.h"

@implementation SenderInfoDB

+ (void)insertAndupdateSenderWithId:(NSString *)senderId
                               Name:(NSString *)name
                            SurName:(NSString *)surname
                              Email:(NSString *)email
                            Address:(NSString *)address
                          ImageName:(NSString *)imageName
                           IsUpdate:(BOOL)isUpdare
{
    
    if (isUpdare) {
        
        //An instance of NSFetchRequest describes search criteria used to retrieve data from a Persistent store.
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SenderInfo"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"senderId == %d", [senderId intValue]];
        fetchRequest.predicate = predicate;
        
        NSError *error;
        NSArray *fetchResults = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        
        //Check for count of array
        if(fetchResults.count > 0)
        {
            SenderInfo *senderInfo = fetchResults[0];
            
            senderInfo.name = name;
            senderInfo.surname = surname;
            senderInfo.email = email;
            senderInfo.address = address;
            
            if (![imageName  isEqual: @""]) {
                
                senderInfo.imageName = imageName;
            }
            
            [[AppDelegate sharedAppDelegate] saveContext];

        }
        
        
    } else {
        
        SenderInfo *senderInfo = [NSEntityDescription insertNewObjectForEntityForName:@"SenderInfo"
                                                               inManagedObjectContext:[AppDelegate sharedAppDelegate].managedObjectContext];
        
        senderInfo.senderId = [senderId integerValue];
        senderInfo.name = name;
        senderInfo.surname = surname;
        senderInfo.email = email;
        senderInfo.address = address;
        senderInfo.imageName = imageName;
        [[AppDelegate sharedAppDelegate] saveContext];
    }
   
}

//This method will return the ShareETAState object from database
+ (SenderInfoDB *)getSenderInfoDB:(int)senderId;
{
    //An instance of NSFetchRequest describes search criteria used to retrieve data from a Persistent store.
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"SenderInfo"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"senderId == %d", senderId];
    fetchRequest.predicate = predicate;
    
    NSError *error;
    NSArray *fetchResults = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    SenderInfoDB *senderInfoDB = nil;
    
    //Check for count of array
    if(fetchResults.count > 0)
    {
        SenderInfo *senderInfo = fetchResults[0];
        senderInfoDB.senderId = (int)senderInfo.senderId;
        senderInfoDB.name = senderInfo.name;
        senderInfoDB.surname = senderInfo.surname;
        senderInfoDB.email = senderInfo.email;
        senderInfoDB.address = senderInfo.address;
        senderInfoDB.imageName = senderInfo.imageName;
    }
    return senderInfoDB;
}

@end
