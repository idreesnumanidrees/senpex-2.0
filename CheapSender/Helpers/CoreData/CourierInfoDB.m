//
//  CourierInfoDB.m
//  CheapSender
//
//  Created by Idrees on 2017/07/07.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierInfoDB.h"

@implementation CourierInfoDB

+ (void)insertAndupdateCourierWithId:(NSString *)coureirId Name:(NSString *)name SurName:(NSString *)surname Email:(NSString *)email Address:(NSString *)address ImageName:(NSString *)imageName Rate:(NSString *)rate Balance:(NSString *)balance IsUpdate:(BOOL)isUpdare
{
    if (isUpdare) {
        
        //An instance of NSFetchRequest describes search criteria used to retrieve data from a Persistent store.
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"CourierInfo"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"courierId == %d", [coureirId intValue]];
        fetchRequest.predicate = predicate;
        
        NSError *error;
        NSArray *fetchResults = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        
        //Check for count of array
        if(fetchResults.count > 0)
        {
            CourierInfo *courierInfo = fetchResults[0];
            
            if (![name  isEqual: @""]) {
                
                courierInfo.name = name;
            }
            if (![surname  isEqual: @""]) {
                
                courierInfo.surname = surname;
            }
            if (![email  isEqual: @""]) {
                
                courierInfo.email = email;
            }
            if (![address  isEqual: @""]) {
                
                courierInfo.address = address;
            }
            if (![balance  isEqual: @""]) {
                
                courierInfo.balance = balance;
            }
            if (![rate  isEqual: @""]) {
                
                courierInfo.rate = rate;
            }
            
            if (![imageName  isEqual: @""]) {
                
                courierInfo.imageName = imageName;
            }
            
            [[AppDelegate sharedAppDelegate] saveContext];

        }
        
    } else {
        
        CourierInfo *courierInfo = [NSEntityDescription insertNewObjectForEntityForName:@"CourierInfo"
                                                                 inManagedObjectContext:[AppDelegate sharedAppDelegate].managedObjectContext];
        courierInfo.courierId = [coureirId integerValue];
        courierInfo.name = name;
        courierInfo.surname = surname;
        courierInfo.email = email;
        courierInfo.address = address;
        courierInfo.imageName = imageName;
        courierInfo.balance = balance;
        courierInfo.rate = rate;
        [[AppDelegate sharedAppDelegate] saveContext];
    }
}

//This method will return the ShareETAState object from database
//This method will return the ShareETAState object from database
+ (CourierInfoDB *)getCourierInfoDB:(int)courierId;
{
    //An instance of NSFetchRequest describes search criteria used to retrieve data from a Persistent store.
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"CourierInfo"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"courierId == %d", courierId];
    fetchRequest.predicate = predicate;
    
    NSError *error;
    NSArray *fetchResults = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    CourierInfoDB *courierInfoDB = [CourierInfoDB new];
    
    //Check for count of array
    if(fetchResults.count > 0)
    {
        CourierInfo *courierInfo = fetchResults[0];
        courierInfoDB.courierId = (int)courierInfo.courierId;
        courierInfoDB.name = courierInfo.name;
        courierInfoDB.surname = courierInfo.surname;
        courierInfoDB.email = courierInfo.email;
        courierInfoDB.address = courierInfo.address;
        courierInfoDB.imageName = courierInfo.imageName;
        courierInfoDB.balance = courierInfo.balance;
        courierInfoDB.rate = courierInfo.rate;
    }
    return courierInfoDB;
}


@end
