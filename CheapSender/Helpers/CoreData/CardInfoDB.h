//
//  CardInfoDB.h
//  CheapSender
//
//  Created by Idrees on 2017/07/19.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseDB.h"

@interface CardInfoDB : BaseDB

@property (assign, nonatomic) int cardId;
@property (strong, nonatomic) NSString *card;
@property (strong, nonatomic) NSString *stripeToken;

+ (void)insertAndupdateCardInfoWithId:(int)sameID CardNumber:(NSString *)cardNumber StripeToken:(NSString *)stripeToken IsUpdate:(BOOL)isUpdare;

//This method will return the ShareETAState object from database
+ (CardInfoDB *)getCardInfoDB:(NSString *)cardId;

@end
