//
//  SessionDB.h
//  CheapSender
//
//  Created by Idrees on 2017/06/06.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseDB.h"

@interface SessionDB : BaseDB

+ (void)insertSessionKey:(NSString *)sessionKey;

+ (void)updateSessionKey:(NSString *)sessionKey;

+ (SessionDB *)getSessionKey;

@end
