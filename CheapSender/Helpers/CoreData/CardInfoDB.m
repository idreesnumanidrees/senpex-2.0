//
//  CardInfoDB.m
//  CheapSender
//
//  Created by Idrees on 2017/07/19.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CardInfoDB.h"

@implementation CardInfoDB

+ (void)insertAndupdateCardInfoWithId:(int)sameID CardNumber:(NSString *)cardNumber StripeToken:(NSString *)stripeToken IsUpdate:(BOOL)isUpdare
{
    
    if (isUpdare) {
        
        ///An instance of NSFetchRequest describes search criteria used to retrieve data from a Persistent store.
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"CardInfo"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cardID == %d", sameID];
        fetchRequest.predicate = predicate;
        
        NSError *error;
        NSArray *fetchResults = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
        
        //Check for count of array
        if(fetchResults.count > 0)
        {
            CardInfo *cardInfo = fetchResults[0];
            cardInfo.cardNumber = cardNumber;
            cardInfo.stripToken = stripeToken;
        }
        
        [[AppDelegate sharedAppDelegate] saveContext];

    }
    else
    {
        CardInfo *cardInfo = [NSEntityDescription insertNewObjectForEntityForName:@"CardInfo"
                                                           inManagedObjectContext:[AppDelegate sharedAppDelegate].managedObjectContext];
        cardInfo.cardID = sameID;
        cardInfo.cardNumber = cardNumber;
        cardInfo.stripToken = stripeToken;
        [[AppDelegate sharedAppDelegate] saveContext];
    }
}

//This method will return the object from database
+ (CardInfoDB *)getCardInfoDB:(NSString *)cardId
{
    //An instance of NSFetchRequest describes search criteria used to retrieve data from a Persistent store.
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"CardInfo"];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"cardID == %d", [cardId intValue]];
    fetchRequest.predicate = predicate;
    
    NSError *error;
    NSArray *fetchResults = [[AppDelegate sharedAppDelegate].managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    CardInfoDB *cardInfoDB = [CardInfoDB new];
    
    //Check for count of array
    if(fetchResults.count > 0)
    {
        CardInfo *cardInfo = fetchResults[0];
        
        cardInfoDB.cardId = (int)cardInfo.cardID;
        cardInfoDB.card = cardInfo.cardNumber;
        cardInfoDB.stripeToken = cardInfo.stripToken;
    } else {
        
        cardInfoDB = nil;
    }
    
    return cardInfoDB;
}

@end
