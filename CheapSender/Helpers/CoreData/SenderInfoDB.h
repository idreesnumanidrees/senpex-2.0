//
//  SenderInfoDB.h
//  CheapSender
//
//  Created by Idrees on 2017/07/07.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseDB.h"

@interface SenderInfoDB : BaseDB

@property (assign, nonatomic) int senderId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *surname;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *imageName;

+ (void)insertAndupdateSenderWithId:(NSString *)senderId Name:(NSString *)name SurName:(NSString *)surname Email:(NSString *)email Address:(NSString *)address ImageName:(NSString *)imageName IsUpdate:(BOOL)isUpdare;

//This method will return the ShareETAState object from database
+ (SenderInfoDB *)getSenderInfoDB:(int)senderId;

@end
