//
//  UserDB.h
//  CheapSender
//
//  Created by Idrees on 2017/06/06.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseDB.h"


@interface UserDB : BaseDB

//This method will save the user object in database
+ (void)insertUser:(UserModel *)userObject;

@end
