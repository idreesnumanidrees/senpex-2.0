//
//  LastChatMessageDB.h
//  CheapSender
//
//  Created by Idrees on 2017/08/05.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseDB.h"

@interface LastChatMessageDB : BaseDB

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *message;

+ (void)insertAndupdateLastMessageWithId:(NSString *)userId message:(NSString *)message IsUpdate:(BOOL)isUpdare;

//This method will return the ShareETAState object from database
+ (LastChatMessageDB *)getLastMessage:(NSString *)userId;

@end
