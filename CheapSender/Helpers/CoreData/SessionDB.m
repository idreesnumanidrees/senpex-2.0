//
//  SessionDB.m
//  CheapSender
//
//  Created by Idrees on 2017/06/06.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SessionDB.h"

@implementation SessionDB

+ (void)insertSessionKey:(NSString *)sessionKey
{
    Session *session = [NSEntityDescription insertNewObjectForEntityForName:@"Session"
                                                     inManagedObjectContext:[AppDelegate sharedAppDelegate].managedObjectContext];
    session.sessionKey = sessionKey;
    [[AppDelegate sharedAppDelegate] saveContext];
}

+ (void)updateSessionKey:(NSString *)sessionKey
{
    
}

+ (SessionDB *)getSessionKey
{
    return [SessionDB new];
}

@end
