//
//  CourierInfoDB.h
//  CheapSender
//
//  Created by Idrees on 2017/07/07.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "BaseDB.h"

@interface CourierInfoDB : BaseDB

@property (assign, nonatomic) int courierId;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *surname;
@property (strong, nonatomic) NSString *email;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *imageName;
@property (strong, nonatomic) NSString *balance;
@property (strong, nonatomic) NSString *rate;

+ (void)insertAndupdateCourierWithId:(NSString *)coureirId Name:(NSString *)name SurName:(NSString *)surname Email:(NSString *)email Address:(NSString *)address ImageName:(NSString *)imageName Rate:(NSString *)rate Balance:(NSString *)balance IsUpdate:(BOOL)isUpdare;

//This method will return the ShareETAState object from database
+ (CourierInfoDB *)getCourierInfoDB:(int)courierId;

@end
