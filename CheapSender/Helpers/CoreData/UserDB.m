//
//  UserDB.m
//  CheapSender
//
//  Created by Idrees on 2017/06/06.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "UserDB.h"

@implementation UserDB

//This method will save the user object in database
+ (void)insertUser:(UserModel *)userObject
{
    User *user = [NSEntityDescription insertNewObjectForEntityForName:@"User"
                                               inManagedObjectContext:[AppDelegate sharedAppDelegate].managedObjectContext];
    
    user.userId = userObject.internalBaseClassIdentifier;
    user.username = userObject.name;
    
    UserProfiles *userProfile = userObject.userProfiles[0];
    user.userType = userProfile.userType;
    
    [[AppDelegate sharedAppDelegate] saveContext];

}

@end
