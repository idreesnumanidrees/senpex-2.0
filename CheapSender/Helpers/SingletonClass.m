//
//  SingletonClass.m
//  CheapSender
//
//  Created by Idrees on 2017/06/05.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SingletonClass.h"

@implementation SingletonClass

+ (SingletonClass *)sharedInstance
{
    static dispatch_once_t onceToken;
    static SingletonClass *instance = nil;
    
    dispatch_once(&onceToken, ^{
        instance = [[SingletonClass alloc] init];
    });
    return instance;
}


@end
