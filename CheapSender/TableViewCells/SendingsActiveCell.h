//
//  SendingsActiveCell.h
//  CheapSender
//
//  Created by admin on 4/20/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SendingsActiveCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgPackStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgPackImage;
@property (weak, nonatomic) IBOutlet UIView *packContainer;
@property (weak, nonatomic) IBOutlet UILabel *packStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderFromLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderToLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;

@end
