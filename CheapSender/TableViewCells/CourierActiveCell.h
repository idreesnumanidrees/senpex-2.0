//
//  CourierActiveCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/14.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourierActiveCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgPackImage;
@property (weak, nonatomic) IBOutlet UIView *packContainer;
@property (weak, nonatomic) IBOutlet UILabel *packStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderFromLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderToLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *milesLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
