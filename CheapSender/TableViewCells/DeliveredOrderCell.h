//
//  DeliveredOrderCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/31.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DeliveredOrderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *orderImageView;
@property (weak, nonatomic) IBOutlet UIButton *crossButton;

@end
