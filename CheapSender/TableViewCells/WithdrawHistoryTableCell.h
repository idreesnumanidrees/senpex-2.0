//
//  WithdrawHistoryTableCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WithdrawHistoryTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *progressStatus;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *desription;

@end
