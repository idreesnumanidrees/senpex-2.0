//
//  BidViewCell.h
//  CheapSender
//
//  Created by Ambika on 27/04/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface BidViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRating;
@property (weak, nonatomic) IBOutlet UILabel *lblRating;

@end
