//
//  PaymentUpdatedCell.h
//  TestProject
//
//  Created by Ambika Vijay on 5/3/17.
//  Copyright © 2017 ZCO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentUpdatedCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblValue;
@property (strong, nonatomic) IBOutlet UIImageView *iconView;
@property (weak, nonatomic) IBOutlet UIImageView *cardTypeView;

@end
