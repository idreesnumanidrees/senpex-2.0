//
//  CourierProfileCell.h
//  CheapSender
//
//  Created by Ambika on 28/04/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourierProfileCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblData;

@end
