//
//  ChatListCell.m
//  CheapSender
//
//  Created by Idrees on 2017/05/02.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "ChatListCell.h"
#import "UtilHelper.h"

@implementation ChatListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.countView = [UtilHelper makeCornerCurved:self.countView withRadius:5.0 color:nil width:0];
    self.profileImageView = (UIImageView *)[UtilHelper makeCornerCurved:self.profileImageView withRadius:25.0 color:nil width:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
