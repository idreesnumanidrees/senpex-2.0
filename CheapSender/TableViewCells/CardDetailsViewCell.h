//
//  CardDetailsViewCell.h
//  TestProject
//
//  Created by Ambika Vijay on 5/4/17.
//  Copyright © 2017 ZCO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardDetailsViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *textFieldCvv;
@property (weak, nonatomic) IBOutlet UILabel *lblExpDate;

@end
