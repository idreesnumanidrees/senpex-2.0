//
//  CancelPackReasonTableCell.h
//  CheapSender
//
//  Created by Riyas Abdul Rahman on 5/2/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CancelPackReasonModel.h"

@interface CancelPackReasonTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *reasonTitle;
@property (weak, nonatomic) IBOutlet UIImageView *checkmark;


- (void)prepareUI:(CancelPackReasonModel *)cancelPackReasonObj;
@end
