//
//  CardDetailsEditCell.h
//  TestProject
//
//  Created by Ambika on 04/05/17.
//  Copyright © 2017 ZCO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CardDetailsEditCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnExpDate;
@property (weak, nonatomic) IBOutlet UITextField *textFieldCvv;

@end
