//
//  CourierPersonalInfoCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/29.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourierPersonalInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *profilePicImageView;

@end
