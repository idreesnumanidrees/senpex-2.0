//
//  OtherChatCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatListModel.h"

@interface OtherChatCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UIView *cellResizeView;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

- (void)prepareChatUI:(ChatListModel *)chatMsgObj;

@end
