//
//  SendingsActiveCell.m
//  CheapSender
//
//  Created by admin on 4/20/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "SendingsActiveCell.h"
#import "UtilHelper.h"

@implementation SendingsActiveCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.packContainer = (UIButton *)[UtilHelper makeCornerCurved:self.packContainer withRadius:5.0 color:nil width:0];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
