//
//  CancelPackReasonTableCell.m
//  CheapSender
//
//  Created by Riyas Abdul Rahman on 5/2/17.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CancelPackReasonTableCell.h"

@implementation CancelPackReasonTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareUI:(CancelPackReasonModel *)cancelPackReasonObj
{
    self.reasonTitle.text = cancelPackReasonObj.reasonTitle;
    
    NSString *imageName = (cancelPackReasonObj.isSelected == YES) ? @"tick-icon" : @"";
    self.checkmark.image = [UIImage imageNamed:imageName];
}

@end
