//
//  NotificationListCell.m
//  CheapSender
//
//  Created by Idrees on 2017/05/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "NotificationListCell.h"
#import "UtilHelper.h"

@implementation NotificationListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.containerView = [UtilHelper makeCornerCurved:self.containerView
                                           withRadius:5.0
                                                color:nil
                                                width:0];
    
     self.dotView = [UtilHelper makeCornerCurved:self.dotView
                                      withRadius:4.0
                                           color:nil
                                           width:0];
    
//    self.profileImageView = (UIImageView *)[UtilHelper makeCornerCurved:self.profileImageView
//                                                             withRadius:20.0
//                                                                  color:nil
//                                                                  width:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
