//
//  CourierActiveCell.m
//  CheapSender
//
//  Created by Idrees on 2017/05/14.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "CourierActiveCell.h"
#import "UtilHelper.h"

@implementation CourierActiveCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
     self.packContainer = (UIButton *)[UtilHelper makeCornerCurved:self.packContainer withRadius:5.0 color:nil width:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
