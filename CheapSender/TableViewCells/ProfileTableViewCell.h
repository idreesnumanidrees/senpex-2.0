//
//  ProfileTableViewCell.h
//  CheapSender
//
//  Created by M Imran on 01/05/2017.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageIcon;
@property (weak, nonatomic) IBOutlet UILabel *menuItemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *seperator;

@end
