//
//  MyRouteTableCell.m
//  CheapSender
//
//  Created by Idrees on 2017/05/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "MyRouteTableCell.h"
#import "UtilHelper.h"

@implementation MyRouteTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.containerView = [UtilHelper makeCornerCurved:self.containerView withRadius:7.0 color:nil width:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
