//
//  FAQTableCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;

@end
