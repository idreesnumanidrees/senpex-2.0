//
//  CardDetailsEditCell.m
//  TestProject
//
//  Created by Ambika on 04/05/17.
//  Copyright © 2017 ZCO. All rights reserved.
//

#import "CardDetailsEditCell.h"

@implementation CardDetailsEditCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(BOOL)isEmpty{
    if([self isStringEmpty: self.textFieldCvv.text]){
        [self markInValid ];
        return false;
    }
    return true;
}
-(void)markInValid{
    self.textFieldCvv.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor redColor]);
    self.textFieldCvv.layer.borderWidth = 1.0;
    [self.textFieldCvv becomeFirstResponder];
}
- (BOOL)isStringEmpty:(NSString *)string {
    if([string length] == 0) { //string is empty or nil
        return YES;
    }
    
    if(![[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
        //string is all whitespace
        return YES;
    }
    
    return NO;
}

@end
