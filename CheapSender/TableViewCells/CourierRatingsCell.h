//
//  CourierRatingsCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface CourierRatingsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *lblRatingCount;

@end
