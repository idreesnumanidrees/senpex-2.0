//
//  MyRouteTableCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/18.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyRouteTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *fromLocationLabel;
@property (weak, nonatomic) IBOutlet UILabel *toLocationLabel;

//Week days 
@property (weak, nonatomic) IBOutlet UIView *weekView;
@property (weak, nonatomic) IBOutlet UIButton *btnMonday;
@property (weak, nonatomic) IBOutlet UIButton *btnTuesday;
@property (weak, nonatomic) IBOutlet UIButton *btnWednesday;
@property (weak, nonatomic) IBOutlet UIButton *btnThursday;
@property (weak, nonatomic) IBOutlet UIButton *btnFriday;
@property (weak, nonatomic) IBOutlet UIButton *btnSaturday;
@property (weak, nonatomic) IBOutlet UIButton *btnSunday;
@property (weak, nonatomic) IBOutlet UILabel *dateTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end
