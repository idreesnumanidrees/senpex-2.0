//
//  PaymentConfirmationCell.m
//  TestProject
//
//  Created by Ambika Vijay on 5/3/17.
//  Copyright © 2017 ZCO. All rights reserved.
//

#import "PaymentConfirmationCell.h"

@implementation PaymentConfirmationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(BOOL)isEmpty{
    if([self isStringEmpty: self.valueField.text]){
        [self markInValid ];
        return true;
    }
    return false;
}
-(void)markInValid{
    self.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor redColor]);
    self.layer.borderWidth = 1.0;
    [self.valueField becomeFirstResponder];
}
- (BOOL)isStringEmpty:(NSString *)string {
    if([string length] == 0) { //string is empty or nil
        return YES;
    }
    
    if(![[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length]) {
        //string is all whitespace
        return YES;
    }
    
    return NO;
}
@end
