//
//  PaymentConfirmationCell.h
//  TestProject
//
//  Created by Ambika Vijay on 5/3/17.
//  Copyright © 2017 ZCO. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentConfirmationCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *iconView;
@property (strong, nonatomic) IBOutlet UITextField *valueField;
-(BOOL)isEmpty;
@end
