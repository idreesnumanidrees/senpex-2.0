//
//  PaymentHistoryCell.h
//  CheapSender
//
//  Created by Idrees on 2017/07/13.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentHistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *sendName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UILabel *insertedDate;
@property (weak, nonatomic) IBOutlet UILabel *status;
@property (weak, nonatomic) IBOutlet UILabel *number;

@end
