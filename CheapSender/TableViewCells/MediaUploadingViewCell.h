//
//  MediaUploadingViewCell.h
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddImageObject.h"
#import "MRActivityIndicatorView.h"

@protocol MediaUploadCellDelegate < NSObject >

-(void)cancelForCellRow:(NSInteger)row;
-(void)actionForCellRow:(NSInteger)row;

@end

@interface MediaUploadingViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;
@property (strong, nonatomic) IBOutlet UIButton *btnAction;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet MRActivityIndicatorView *activityIndicatorView;

@property (nonatomic, assign) id <MediaUploadCellDelegate> mediaCellDelegate;

-(void)prepareUIForRowWithImage:(AddImageObject *)addImageObject index:(NSInteger)index;


@end
