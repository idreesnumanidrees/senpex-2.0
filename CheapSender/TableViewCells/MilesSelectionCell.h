//
//  MilesSelectionCell.h
//  CheapSender
//
//  Created by Idrees on 2018/06/24.
//  Copyright © 2018 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MilesSelectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIButton *milesButton;
@property (weak, nonatomic) IBOutlet UILabel *milesLabel;

@end
