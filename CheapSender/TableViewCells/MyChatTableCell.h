//
//  MyChatTableCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatListModel.h"

@interface MyChatTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UIView *cellResizeView;

- (void)prepareChatUI:(ChatListModel *)chatMsgObj;

@end
