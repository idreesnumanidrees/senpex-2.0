//
//  TermsRulesTableCell.h
//  CheapSender
//
//  Created by Idrees on 2017/07/05.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsRulesTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *headingLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;

@end
