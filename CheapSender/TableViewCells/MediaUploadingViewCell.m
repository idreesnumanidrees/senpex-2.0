//
//  MediaUploadingViewCell.m
//  CheapSender
//
//  Created by Idrees on 2017/04/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "MediaUploadingViewCell.h"
#import "UtilHelper.h"

@interface MediaUploadingViewCell()

@property (nonatomic,assign) int row;
@end

#define STOPUPLOAD_IMAGE_HEX "\uf24c"
#define UPLOADED_IMAGE_HEX "\uf22a"

@implementation MediaUploadingViewCell

- (void)prepareUIForRowWithImage:(AddImageObject *)addImageObject index:(NSInteger)index {
    
    _btnCancel.tag = index;
    
    if (addImageObject.image != nil)
        _imageView.image = addImageObject.image;
    _imageView = [UtilHelper makeImageCornerRounded:_imageView];
}

- (IBAction)cancelTapped:(UIButton *)sender {
    if (_mediaCellDelegate && [_mediaCellDelegate respondsToSelector:@selector(cancelForCellRow:)])
        [_mediaCellDelegate cancelForCellRow:sender.tag];
}

- (IBAction)actionTapped:(UIButton *)sender {
    if (_mediaCellDelegate && [_mediaCellDelegate respondsToSelector:@selector(actionForCellRow:)])
        [_mediaCellDelegate actionForCellRow:sender.tag];
}

@end
