//
//  OrdersTableCell.h
//  CheapSender
//
//  Created by Idrees on 2017/05/11.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JAProgressBar.h"

@interface OrdersTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet UILabel *milesLabel;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@property (weak, nonatomic) IBOutlet UILabel *packLabel;
@property (weak, nonatomic) IBOutlet UIImageView *packImageView;
@property (weak, nonatomic) IBOutlet UIView *container;
@property (weak, nonatomic) IBOutlet UIView *successfullView;
@property (weak, nonatomic) IBOutlet UIView *anotherCourierView;
@property (weak, nonatomic) IBOutlet UIView *cancelPackageView;
@property (weak, nonatomic) IBOutlet UILabel *senderFromLabel;
@property (weak, nonatomic) IBOutlet UILabel *senderToLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (nonatomic, assign) int totalTime;
@property (nonatomic, assign) int elapsedTime;

@property (nonatomic, assign) int flushTime;

@property (nonatomic, strong) NSTimer *timer;

- (void) startTimer:(NSString *)timerTag timerInterval:(int)timerInterval;

//Version1.5
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;

@property (weak, nonatomic) IBOutlet UIButton *openExternalMapsFrom;
@property (weak, nonatomic) IBOutlet UIButton *openExternalMapsTo;


@end
