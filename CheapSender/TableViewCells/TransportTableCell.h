//
//  TransportTableCell.h
//  CheapSender
//
//  Created by Idrees on 2017/07/10.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TransportTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *modelLabel;
@property (weak, nonatomic) IBOutlet UIImageView *vehicleImgView;


@end
