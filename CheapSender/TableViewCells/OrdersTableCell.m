//
//  OrdersTableCell.m
//  CheapSender
//
//  Created by Idrees on 2017/05/11.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "OrdersTableCell.h"
#import "UtilHelper.h"

@implementation OrdersTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.profileImageView = (UIImageView *)[UtilHelper makeCornerCurved:self.profileImageView withRadius:25.0 color:nil width:0];
    
    self.container = [UtilHelper makeCornerCurved:self.container withRadius:5.0 color:nil width:0];
    
    self.confirmButton = (UIButton *)[UtilHelper makeCornerCurved:self.confirmButton withRadius:14.5 color:nil width:0];
    
    self.cancelButton = (UIButton *)[UtilHelper makeCornerCurved:self.cancelButton withRadius:14.5 color:nil width:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) startTimer:(NSString *)timerTag timerInterval:(int)timerInterval
{
    // invalidate a previous timer in case of reuse
    if (self.timer)
        [self.timer invalidate];
    
    _progressBar.progress = 0.0;

    _totalTime = timerInterval;
    _elapsedTime = timerInterval;
    _flushTime = 0;
    
    // create a new timer
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1
                                                  target:self
                                                selector:@selector(calculateTimer:)
                                                userInfo:timerTag
                                                 repeats:YES];
    
    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    [self.timer fire];
}

- (void)calculateTimer:(NSTimer *)timer
{

    _elapsedTime --;
    _flushTime ++;
    _progressBar.progress = (float)_flushTime/_totalTime;

    if ( _elapsedTime <= 0) {
        
        _elapsedTime = 0;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ORDER_CANCEL_FROM_NEW_PACKS
                                                            object:@{@"Tag":self.timer.userInfo}];

        [self.timer invalidate];
    }
    
    self.timerLabel.text = [NSString stringWithFormat:@"%i s", _elapsedTime];//[self timeFormatted:_elapsedTime];

}

- (NSString *)timeFormatted:(int)totalSeconds
{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    return [NSString stringWithFormat:@"%02d:%02d", minutes, seconds];
}


@end
