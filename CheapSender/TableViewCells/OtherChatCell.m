//
//  OtherChatCell.m
//  CheapSender
//
//  Created by Idrees on 2017/05/04.
//  Copyright © 2017 CheapSender. All rights reserved.
//

#import "OtherChatCell.h"
#import "UtilHelper.h"
#import "NSDate+Utilities.h"

@implementation OtherChatCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.cellResizeView = [UtilHelper makeCornerCurved:self.cellResizeView withRadius:5.0f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareChatUI:(ChatListModel *)chatMsgObj
{
    self.messageLabel.text = chatMsgObj.chatText;
    NSDate *headerDate = [NSDate dateFromDateString:chatMsgObj.insertedDate];
    self.timeLabel.text = [UtilHelper time:headerDate];
    
    //NSLog(@"other timelabel %@",self.timeLabel.text);

}

@end
