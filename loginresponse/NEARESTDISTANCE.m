//
//  NEARESTDISTANCE.m
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "NEARESTDISTANCE.h"


NSString *const kNEARESTDISTANCEValue = @"value";
NSString *const kNEARESTDISTANCEDesc = @"desc";


@interface NEARESTDISTANCE ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation NEARESTDISTANCE

@synthesize value = _value;
@synthesize desc = _desc;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.value = [self objectOrNilForKey:kNEARESTDISTANCEValue fromDictionary:dict];
            self.desc = [self objectOrNilForKey:kNEARESTDISTANCEDesc fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.value forKey:kNEARESTDISTANCEValue];
    [mutableDict setValue:self.desc forKey:kNEARESTDISTANCEDesc];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.value = [aDecoder decodeObjectForKey:kNEARESTDISTANCEValue];
    self.desc = [aDecoder decodeObjectForKey:kNEARESTDISTANCEDesc];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_value forKey:kNEARESTDISTANCEValue];
    [aCoder encodeObject:_desc forKey:kNEARESTDISTANCEDesc];
}

- (id)copyWithZone:(NSZone *)zone {
    NEARESTDISTANCE *copy = [[NEARESTDISTANCE alloc] init];
    
    
    
    if (copy) {

        copy.value = [self.value copyWithZone:zone];
        copy.desc = [self.desc copyWithZone:zone];
    }
    
    return copy;
}


@end
