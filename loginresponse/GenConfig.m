//
//  GenConfig.m
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "GenConfig.h"
#import "IMAGEPATH.h"
#import "INSVALUE.h"
#import "NEARESTDAYS.h"
#import "NEARESTDISTANCE.h"
#import "MAXACTIVEPACKS.h"


NSString *const kGenConfigDEFAULTLAT = @"DEFAULT_LAT";
NSString *const kGenConfigIMAGEPATH = @"IMAGE_PATH";
NSString *const kGenConfigINSVALUE = @"INS_VALUE";
NSString *const kGenConfigNEARESTDAYS = @"NEAREST_DAYS";
NSString *const kGenConfigSTRIPEPUBLICAPIKEY = @"STRIPE_PUBLIC_API_KEY";
NSString *const kGenConfigMAXMILES = @"MAX_MILES";
NSString *const kGenConfigNEARESTDISTANCE = @"NEAREST_DISTANCE";
NSString *const kGenConfigDEFAULTLNG = @"DEFAULT_LNG";
NSString *const kGenConfigMAXACTIVEPACKS = @"MAX_ACTIVE_PACKS";


@interface GenConfig ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation GenConfig

@synthesize dEFAULTLAT = _dEFAULTLAT;
@synthesize iMAGEPATH = _iMAGEPATH;
@synthesize iNSVALUE = _iNSVALUE;
@synthesize nEARESTDAYS = _nEARESTDAYS;
@synthesize sTRIPEPUBLICAPIKEY = _sTRIPEPUBLICAPIKEY;
@synthesize mAXMILES = _mAXMILES;
@synthesize nEARESTDISTANCE = _nEARESTDISTANCE;
@synthesize dEFAULTLNG = _dEFAULTLNG;
@synthesize mAXACTIVEPACKS = _mAXACTIVEPACKS;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.dEFAULTLAT = [self objectOrNilForKey:kGenConfigDEFAULTLAT fromDictionary:dict];
            self.iMAGEPATH = [IMAGEPATH modelObjectWithDictionary:[dict objectForKey:kGenConfigIMAGEPATH]];
            self.iNSVALUE = [INSVALUE modelObjectWithDictionary:[dict objectForKey:kGenConfigINSVALUE]];
            self.nEARESTDAYS = [NEARESTDAYS modelObjectWithDictionary:[dict objectForKey:kGenConfigNEARESTDAYS]];
            self.sTRIPEPUBLICAPIKEY = [self objectOrNilForKey:kGenConfigSTRIPEPUBLICAPIKEY fromDictionary:dict];
            self.mAXMILES = [[self objectOrNilForKey:kGenConfigMAXMILES fromDictionary:dict] doubleValue];
            self.nEARESTDISTANCE = [NEARESTDISTANCE modelObjectWithDictionary:[dict objectForKey:kGenConfigNEARESTDISTANCE]];
            self.dEFAULTLNG = [self objectOrNilForKey:kGenConfigDEFAULTLNG fromDictionary:dict];
            self.mAXACTIVEPACKS = [MAXACTIVEPACKS modelObjectWithDictionary:[dict objectForKey:kGenConfigMAXACTIVEPACKS]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.dEFAULTLAT forKey:kGenConfigDEFAULTLAT];
    [mutableDict setValue:[self.iMAGEPATH dictionaryRepresentation] forKey:kGenConfigIMAGEPATH];
    [mutableDict setValue:[self.iNSVALUE dictionaryRepresentation] forKey:kGenConfigINSVALUE];
    [mutableDict setValue:[self.nEARESTDAYS dictionaryRepresentation] forKey:kGenConfigNEARESTDAYS];
    [mutableDict setValue:self.sTRIPEPUBLICAPIKEY forKey:kGenConfigSTRIPEPUBLICAPIKEY];
    [mutableDict setValue:[NSNumber numberWithDouble:self.mAXMILES] forKey:kGenConfigMAXMILES];
    [mutableDict setValue:[self.nEARESTDISTANCE dictionaryRepresentation] forKey:kGenConfigNEARESTDISTANCE];
    [mutableDict setValue:self.dEFAULTLNG forKey:kGenConfigDEFAULTLNG];
    [mutableDict setValue:[self.mAXACTIVEPACKS dictionaryRepresentation] forKey:kGenConfigMAXACTIVEPACKS];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.dEFAULTLAT = [aDecoder decodeObjectForKey:kGenConfigDEFAULTLAT];
    self.iMAGEPATH = [aDecoder decodeObjectForKey:kGenConfigIMAGEPATH];
    self.iNSVALUE = [aDecoder decodeObjectForKey:kGenConfigINSVALUE];
    self.nEARESTDAYS = [aDecoder decodeObjectForKey:kGenConfigNEARESTDAYS];
    self.sTRIPEPUBLICAPIKEY = [aDecoder decodeObjectForKey:kGenConfigSTRIPEPUBLICAPIKEY];
    self.mAXMILES = [aDecoder decodeDoubleForKey:kGenConfigMAXMILES];
    self.nEARESTDISTANCE = [aDecoder decodeObjectForKey:kGenConfigNEARESTDISTANCE];
    self.dEFAULTLNG = [aDecoder decodeObjectForKey:kGenConfigDEFAULTLNG];
    self.mAXACTIVEPACKS = [aDecoder decodeObjectForKey:kGenConfigMAXACTIVEPACKS];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_dEFAULTLAT forKey:kGenConfigDEFAULTLAT];
    [aCoder encodeObject:_iMAGEPATH forKey:kGenConfigIMAGEPATH];
    [aCoder encodeObject:_iNSVALUE forKey:kGenConfigINSVALUE];
    [aCoder encodeObject:_nEARESTDAYS forKey:kGenConfigNEARESTDAYS];
    [aCoder encodeObject:_sTRIPEPUBLICAPIKEY forKey:kGenConfigSTRIPEPUBLICAPIKEY];
    [aCoder encodeDouble:_mAXMILES forKey:kGenConfigMAXMILES];
    [aCoder encodeObject:_nEARESTDISTANCE forKey:kGenConfigNEARESTDISTANCE];
    [aCoder encodeObject:_dEFAULTLNG forKey:kGenConfigDEFAULTLNG];
    [aCoder encodeObject:_mAXACTIVEPACKS forKey:kGenConfigMAXACTIVEPACKS];
}

- (id)copyWithZone:(NSZone *)zone {
    GenConfig *copy = [[GenConfig alloc] init];
    
    
    
    if (copy) {

        copy.dEFAULTLAT = [self.dEFAULTLAT copyWithZone:zone];
        copy.iMAGEPATH = [self.iMAGEPATH copyWithZone:zone];
        copy.iNSVALUE = [self.iNSVALUE copyWithZone:zone];
        copy.nEARESTDAYS = [self.nEARESTDAYS copyWithZone:zone];
        copy.sTRIPEPUBLICAPIKEY = [self.sTRIPEPUBLICAPIKEY copyWithZone:zone];
        copy.mAXMILES = self.mAXMILES;
        copy.nEARESTDISTANCE = [self.nEARESTDISTANCE copyWithZone:zone];
        copy.dEFAULTLNG = [self.dEFAULTLNG copyWithZone:zone];
        copy.mAXACTIVEPACKS = [self.mAXACTIVEPACKS copyWithZone:zone];
    }
    
    return copy;
}


@end
