//
//  UserModel.h
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class GenConfig;

@interface UserModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *approveGuid;
@property (nonatomic, strong) NSString *cell;
@property (nonatomic, assign) id ssnNumber;
@property (nonatomic, assign) id useOffical;
@property (nonatomic, strong) NSString *logDeviceId;
@property (nonatomic, strong) NSString *dataIdentifier;
@property (nonatomic, assign) id lastLat;
@property (nonatomic, assign) id country;
@property (nonatomic, assign) id qstDisability;
@property (nonatomic, strong) NSString *userRate;
@property (nonatomic, strong) NSString *updatedBy;
@property (nonatomic, strong) NSString *isDisabled;
@property (nonatomic, assign) id useLocation;
@property (nonatomic, strong) NSString *logSessionKey;
@property (nonatomic, strong) NSString *approvedByAdmin;
@property (nonatomic, assign) id meritalStatus;
@property (nonatomic, strong) NSString *availStatus;
@property (nonatomic, assign) id qstCriminal;
@property (nonatomic, assign) id idcatdState;
@property (nonatomic, strong) NSArray *userProfiles;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) id idcardExpDate;
@property (nonatomic, strong) NSString *logUserArgent;
@property (nonatomic, strong) NSString *insertedDate;
@property (nonatomic, strong) NSString *surname;
@property (nonatomic, assign) id idcardNumber;
@property (nonatomic, strong) NSString *updatedDate;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) id lastLng;
@property (nonatomic, assign) id qstSexsual;
@property (nonatomic, assign) id referalName;
@property (nonatomic, assign) id city;
@property (nonatomic, strong) NSString *selfImg;
@property (nonatomic, strong) NSString *systemRow;
@property (nonatomic, strong) NSString *lastSeenDate;
@property (nonatomic, assign) id addressOffical;
@property (nonatomic, assign) id idcardImg2;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *isApproved;
@property (nonatomic, assign) id idcardImg1;
@property (nonatomic, strong) NSString *addressActual;
@property (nonatomic, assign) id paypalEmail;
@property (nonatomic, assign) id referalCell;
@property (nonatomic, strong) NSString *logDate;
@property (nonatomic, strong) NSString *lastLocationDate;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *insertedBy;
@property (nonatomic, assign) id qstHealth;
@property (nonatomic, strong) GenConfig *genConfig;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
