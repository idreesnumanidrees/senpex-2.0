//
//  UserProfiles.h
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface UserProfiles : NSObject <NSCoding, NSCopying>

@property (nonatomic, assign) id verifiedMsg;
@property (nonatomic, strong) NSString *userProfilesIdentifier;
@property (nonatomic, strong) NSString *isVerified;
@property (nonatomic, strong) NSString *payoutsEnabled;
@property (nonatomic, strong) NSString *userType;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *cardLast4;
@property (nonatomic, strong) NSString *stripeAccId;
@property (nonatomic, assign) id processedDate;
@property (nonatomic, strong) NSString *isDefault;
@property (nonatomic, strong) NSString *stripeCustId;
@property (nonatomic, strong) NSString *isProcessed;
@property (nonatomic, strong) NSString *approvedByAdmin;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
