//
//  GenConfig.h
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IMAGEPATH, INSVALUE, NEARESTDAYS, NEARESTDISTANCE, MAXACTIVEPACKS;

@interface GenConfig : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *dEFAULTLAT;
@property (nonatomic, strong) IMAGEPATH *iMAGEPATH;
@property (nonatomic, strong) INSVALUE *iNSVALUE;
@property (nonatomic, strong) NEARESTDAYS *nEARESTDAYS;
@property (nonatomic, strong) NSString *sTRIPEPUBLICAPIKEY;
@property (nonatomic, assign) double mAXMILES;
@property (nonatomic, strong) NEARESTDISTANCE *nEARESTDISTANCE;
@property (nonatomic, strong) NSString *dEFAULTLNG;
@property (nonatomic, strong) MAXACTIVEPACKS *mAXACTIVEPACKS;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
