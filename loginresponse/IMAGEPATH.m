//
//  IMAGEPATH.m
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "IMAGEPATH.h"


NSString *const kIMAGEPATHValue = @"value";
NSString *const kIMAGEPATHDesc = @"desc";


@interface IMAGEPATH ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation IMAGEPATH

@synthesize value = _value;
@synthesize desc = _desc;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.value = [self objectOrNilForKey:kIMAGEPATHValue fromDictionary:dict];
            self.desc = [self objectOrNilForKey:kIMAGEPATHDesc fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.value forKey:kIMAGEPATHValue];
    [mutableDict setValue:self.desc forKey:kIMAGEPATHDesc];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.value = [aDecoder decodeObjectForKey:kIMAGEPATHValue];
    self.desc = [aDecoder decodeObjectForKey:kIMAGEPATHDesc];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_value forKey:kIMAGEPATHValue];
    [aCoder encodeObject:_desc forKey:kIMAGEPATHDesc];
}

- (id)copyWithZone:(NSZone *)zone {
    IMAGEPATH *copy = [[IMAGEPATH alloc] init];
    
    
    
    if (copy) {

        copy.value = [self.value copyWithZone:zone];
        copy.desc = [self.desc copyWithZone:zone];
    }
    
    return copy;
}


@end
