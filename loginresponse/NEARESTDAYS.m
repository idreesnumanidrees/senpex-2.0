//
//  NEARESTDAYS.m
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "NEARESTDAYS.h"


NSString *const kNEARESTDAYSValue = @"value";
NSString *const kNEARESTDAYSDesc = @"desc";


@interface NEARESTDAYS ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation NEARESTDAYS

@synthesize value = _value;
@synthesize desc = _desc;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.value = [self objectOrNilForKey:kNEARESTDAYSValue fromDictionary:dict];
            self.desc = [self objectOrNilForKey:kNEARESTDAYSDesc fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.value forKey:kNEARESTDAYSValue];
    [mutableDict setValue:self.desc forKey:kNEARESTDAYSDesc];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.value = [aDecoder decodeObjectForKey:kNEARESTDAYSValue];
    self.desc = [aDecoder decodeObjectForKey:kNEARESTDAYSDesc];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_value forKey:kNEARESTDAYSValue];
    [aCoder encodeObject:_desc forKey:kNEARESTDAYSDesc];
}

- (id)copyWithZone:(NSZone *)zone {
    NEARESTDAYS *copy = [[NEARESTDAYS alloc] init];
    
    
    
    if (copy) {

        copy.value = [self.value copyWithZone:zone];
        copy.desc = [self.desc copyWithZone:zone];
    }
    
    return copy;
}


@end
