//
//  UserModel.m
//
//  Created by   on 9/11/17
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "UserModel.h"
#import "UserProfiles.h"
#import "GenConfig.h"


NSString *const kDataApproveGuid = @"approve_guid";
NSString *const kDataCell = @"cell";
NSString *const kDataSsnNumber = @"ssn_number";
NSString *const kDataUseOffical = @"use_offical";
NSString *const kDataLogDeviceId = @"log_device_id";
NSString *const kDataId = @"id";
NSString *const kDataLastLat = @"last_lat";
NSString *const kDataCountry = @"country";
NSString *const kDataQstDisability = @"qst_disability";
NSString *const kDataUserRate = @"user_rate";
NSString *const kDataUpdatedBy = @"updated_by";
NSString *const kDataIsDisabled = @"is_disabled";
NSString *const kDataUseLocation = @"use_location";
NSString *const kDataLogSessionKey = @"log_session_key";
NSString *const kDataApprovedByAdmin = @"approved_by_admin";
NSString *const kDataMeritalStatus = @"merital_status";
NSString *const kDataAvailStatus = @"avail_status";
NSString *const kDataQstCriminal = @"qst_criminal";
NSString *const kDataIdcatdState = @"idcatd_state";
NSString *const kDataUserProfiles = @"user_profiles";
NSString *const kDataEmail = @"email";
NSString *const kDataIdcardExpDate = @"idcard_exp_date";
NSString *const kDataLogUserArgent = @"log_user_argent";
NSString *const kDataInsertedDate = @"inserted_date";
NSString *const kDataSurname = @"surname";
NSString *const kDataIdcardNumber = @"idcard_number";
NSString *const kDataUpdatedDate = @"updated_date";
NSString *const kDataName = @"name";
NSString *const kDataLastLng = @"last_lng";
NSString *const kDataQstSexsual = @"qst_sexsual";
NSString *const kDataReferalName = @"referal_name";
NSString *const kDataCity = @"city";
NSString *const kDataSelfImg = @"self_img";
NSString *const kDataSystemRow = @"system_row";
NSString *const kDataLastSeenDate = @"last_seen_date";
NSString *const kDataAddressOffical = @"address_offical";
NSString *const kDataIdcardImg2 = @"idcard_img2";
NSString *const kDataGender = @"gender";
NSString *const kDataIsApproved = @"is_approved";
NSString *const kDataIdcardImg1 = @"idcard_img1";
NSString *const kDataAddressActual = @"address_actual";
NSString *const kDataPaypalEmail = @"paypal_email";
NSString *const kDataReferalCell = @"referal_cell";
NSString *const kDataLogDate = @"log_date";
NSString *const kDataLastLocationDate = @"last_location_date";
NSString *const kDataPassword = @"password";
NSString *const kDataInsertedBy = @"inserted_by";
NSString *const kDataQstHealth = @"qst_health";
NSString *const kDataGenConfig = @"gen_config";


@interface Data ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation Data

@synthesize approveGuid = _approveGuid;
@synthesize cell = _cell;
@synthesize ssnNumber = _ssnNumber;
@synthesize useOffical = _useOffical;
@synthesize logDeviceId = _logDeviceId;
@synthesize dataIdentifier = _dataIdentifier;
@synthesize lastLat = _lastLat;
@synthesize country = _country;
@synthesize qstDisability = _qstDisability;
@synthesize userRate = _userRate;
@synthesize updatedBy = _updatedBy;
@synthesize isDisabled = _isDisabled;
@synthesize useLocation = _useLocation;
@synthesize logSessionKey = _logSessionKey;
@synthesize approvedByAdmin = _approvedByAdmin;
@synthesize meritalStatus = _meritalStatus;
@synthesize availStatus = _availStatus;
@synthesize qstCriminal = _qstCriminal;
@synthesize idcatdState = _idcatdState;
@synthesize userProfiles = _userProfiles;
@synthesize email = _email;
@synthesize idcardExpDate = _idcardExpDate;
@synthesize logUserArgent = _logUserArgent;
@synthesize insertedDate = _insertedDate;
@synthesize surname = _surname;
@synthesize idcardNumber = _idcardNumber;
@synthesize updatedDate = _updatedDate;
@synthesize name = _name;
@synthesize lastLng = _lastLng;
@synthesize qstSexsual = _qstSexsual;
@synthesize referalName = _referalName;
@synthesize city = _city;
@synthesize selfImg = _selfImg;
@synthesize systemRow = _systemRow;
@synthesize lastSeenDate = _lastSeenDate;
@synthesize addressOffical = _addressOffical;
@synthesize idcardImg2 = _idcardImg2;
@synthesize gender = _gender;
@synthesize isApproved = _isApproved;
@synthesize idcardImg1 = _idcardImg1;
@synthesize addressActual = _addressActual;
@synthesize paypalEmail = _paypalEmail;
@synthesize referalCell = _referalCell;
@synthesize logDate = _logDate;
@synthesize lastLocationDate = _lastLocationDate;
@synthesize password = _password;
@synthesize insertedBy = _insertedBy;
@synthesize qstHealth = _qstHealth;
@synthesize genConfig = _genConfig;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.approveGuid = [self objectOrNilForKey:kDataApproveGuid fromDictionary:dict];
            self.cell = [self objectOrNilForKey:kDataCell fromDictionary:dict];
            self.ssnNumber = [self objectOrNilForKey:kDataSsnNumber fromDictionary:dict];
            self.useOffical = [self objectOrNilForKey:kDataUseOffical fromDictionary:dict];
            self.logDeviceId = [self objectOrNilForKey:kDataLogDeviceId fromDictionary:dict];
            self.dataIdentifier = [self objectOrNilForKey:kDataId fromDictionary:dict];
            self.lastLat = [self objectOrNilForKey:kDataLastLat fromDictionary:dict];
            self.country = [self objectOrNilForKey:kDataCountry fromDictionary:dict];
            self.qstDisability = [self objectOrNilForKey:kDataQstDisability fromDictionary:dict];
            self.userRate = [self objectOrNilForKey:kDataUserRate fromDictionary:dict];
            self.updatedBy = [self objectOrNilForKey:kDataUpdatedBy fromDictionary:dict];
            self.isDisabled = [self objectOrNilForKey:kDataIsDisabled fromDictionary:dict];
            self.useLocation = [self objectOrNilForKey:kDataUseLocation fromDictionary:dict];
            self.logSessionKey = [self objectOrNilForKey:kDataLogSessionKey fromDictionary:dict];
            self.approvedByAdmin = [self objectOrNilForKey:kDataApprovedByAdmin fromDictionary:dict];
            self.meritalStatus = [self objectOrNilForKey:kDataMeritalStatus fromDictionary:dict];
            self.availStatus = [self objectOrNilForKey:kDataAvailStatus fromDictionary:dict];
            self.qstCriminal = [self objectOrNilForKey:kDataQstCriminal fromDictionary:dict];
            self.idcatdState = [self objectOrNilForKey:kDataIdcatdState fromDictionary:dict];
    NSObject *receivedUserProfiles = [dict objectForKey:kDataUserProfiles];
    NSMutableArray *parsedUserProfiles = [NSMutableArray array];
    
    if ([receivedUserProfiles isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedUserProfiles) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedUserProfiles addObject:[UserProfiles modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedUserProfiles isKindOfClass:[NSDictionary class]]) {
       [parsedUserProfiles addObject:[UserProfiles modelObjectWithDictionary:(NSDictionary *)receivedUserProfiles]];
    }

    self.userProfiles = [NSArray arrayWithArray:parsedUserProfiles];
            self.email = [self objectOrNilForKey:kDataEmail fromDictionary:dict];
            self.idcardExpDate = [self objectOrNilForKey:kDataIdcardExpDate fromDictionary:dict];
            self.logUserArgent = [self objectOrNilForKey:kDataLogUserArgent fromDictionary:dict];
            self.insertedDate = [self objectOrNilForKey:kDataInsertedDate fromDictionary:dict];
            self.surname = [self objectOrNilForKey:kDataSurname fromDictionary:dict];
            self.idcardNumber = [self objectOrNilForKey:kDataIdcardNumber fromDictionary:dict];
            self.updatedDate = [self objectOrNilForKey:kDataUpdatedDate fromDictionary:dict];
            self.name = [self objectOrNilForKey:kDataName fromDictionary:dict];
            self.lastLng = [self objectOrNilForKey:kDataLastLng fromDictionary:dict];
            self.qstSexsual = [self objectOrNilForKey:kDataQstSexsual fromDictionary:dict];
            self.referalName = [self objectOrNilForKey:kDataReferalName fromDictionary:dict];
            self.city = [self objectOrNilForKey:kDataCity fromDictionary:dict];
            self.selfImg = [self objectOrNilForKey:kDataSelfImg fromDictionary:dict];
            self.systemRow = [self objectOrNilForKey:kDataSystemRow fromDictionary:dict];
            self.lastSeenDate = [self objectOrNilForKey:kDataLastSeenDate fromDictionary:dict];
            self.addressOffical = [self objectOrNilForKey:kDataAddressOffical fromDictionary:dict];
            self.idcardImg2 = [self objectOrNilForKey:kDataIdcardImg2 fromDictionary:dict];
            self.gender = [self objectOrNilForKey:kDataGender fromDictionary:dict];
            self.isApproved = [self objectOrNilForKey:kDataIsApproved fromDictionary:dict];
            self.idcardImg1 = [self objectOrNilForKey:kDataIdcardImg1 fromDictionary:dict];
            self.addressActual = [self objectOrNilForKey:kDataAddressActual fromDictionary:dict];
            self.paypalEmail = [self objectOrNilForKey:kDataPaypalEmail fromDictionary:dict];
            self.referalCell = [self objectOrNilForKey:kDataReferalCell fromDictionary:dict];
            self.logDate = [self objectOrNilForKey:kDataLogDate fromDictionary:dict];
            self.lastLocationDate = [self objectOrNilForKey:kDataLastLocationDate fromDictionary:dict];
            self.password = [self objectOrNilForKey:kDataPassword fromDictionary:dict];
            self.insertedBy = [self objectOrNilForKey:kDataInsertedBy fromDictionary:dict];
            self.qstHealth = [self objectOrNilForKey:kDataQstHealth fromDictionary:dict];
            self.genConfig = [GenConfig modelObjectWithDictionary:[dict objectForKey:kDataGenConfig]];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.approveGuid forKey:kDataApproveGuid];
    [mutableDict setValue:self.cell forKey:kDataCell];
    [mutableDict setValue:self.ssnNumber forKey:kDataSsnNumber];
    [mutableDict setValue:self.useOffical forKey:kDataUseOffical];
    [mutableDict setValue:self.logDeviceId forKey:kDataLogDeviceId];
    [mutableDict setValue:self.dataIdentifier forKey:kDataId];
    [mutableDict setValue:self.lastLat forKey:kDataLastLat];
    [mutableDict setValue:self.country forKey:kDataCountry];
    [mutableDict setValue:self.qstDisability forKey:kDataQstDisability];
    [mutableDict setValue:self.userRate forKey:kDataUserRate];
    [mutableDict setValue:self.updatedBy forKey:kDataUpdatedBy];
    [mutableDict setValue:self.isDisabled forKey:kDataIsDisabled];
    [mutableDict setValue:self.useLocation forKey:kDataUseLocation];
    [mutableDict setValue:self.logSessionKey forKey:kDataLogSessionKey];
    [mutableDict setValue:self.approvedByAdmin forKey:kDataApprovedByAdmin];
    [mutableDict setValue:self.meritalStatus forKey:kDataMeritalStatus];
    [mutableDict setValue:self.availStatus forKey:kDataAvailStatus];
    [mutableDict setValue:self.qstCriminal forKey:kDataQstCriminal];
    [mutableDict setValue:self.idcatdState forKey:kDataIdcatdState];
    NSMutableArray *tempArrayForUserProfiles = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.userProfiles) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForUserProfiles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForUserProfiles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForUserProfiles] forKey:kDataUserProfiles];
    [mutableDict setValue:self.email forKey:kDataEmail];
    [mutableDict setValue:self.idcardExpDate forKey:kDataIdcardExpDate];
    [mutableDict setValue:self.logUserArgent forKey:kDataLogUserArgent];
    [mutableDict setValue:self.insertedDate forKey:kDataInsertedDate];
    [mutableDict setValue:self.surname forKey:kDataSurname];
    [mutableDict setValue:self.idcardNumber forKey:kDataIdcardNumber];
    [mutableDict setValue:self.updatedDate forKey:kDataUpdatedDate];
    [mutableDict setValue:self.name forKey:kDataName];
    [mutableDict setValue:self.lastLng forKey:kDataLastLng];
    [mutableDict setValue:self.qstSexsual forKey:kDataQstSexsual];
    [mutableDict setValue:self.referalName forKey:kDataReferalName];
    [mutableDict setValue:self.city forKey:kDataCity];
    [mutableDict setValue:self.selfImg forKey:kDataSelfImg];
    [mutableDict setValue:self.systemRow forKey:kDataSystemRow];
    [mutableDict setValue:self.lastSeenDate forKey:kDataLastSeenDate];
    [mutableDict setValue:self.addressOffical forKey:kDataAddressOffical];
    [mutableDict setValue:self.idcardImg2 forKey:kDataIdcardImg2];
    [mutableDict setValue:self.gender forKey:kDataGender];
    [mutableDict setValue:self.isApproved forKey:kDataIsApproved];
    [mutableDict setValue:self.idcardImg1 forKey:kDataIdcardImg1];
    [mutableDict setValue:self.addressActual forKey:kDataAddressActual];
    [mutableDict setValue:self.paypalEmail forKey:kDataPaypalEmail];
    [mutableDict setValue:self.referalCell forKey:kDataReferalCell];
    [mutableDict setValue:self.logDate forKey:kDataLogDate];
    [mutableDict setValue:self.lastLocationDate forKey:kDataLastLocationDate];
    [mutableDict setValue:self.password forKey:kDataPassword];
    [mutableDict setValue:self.insertedBy forKey:kDataInsertedBy];
    [mutableDict setValue:self.qstHealth forKey:kDataQstHealth];
    [mutableDict setValue:[self.genConfig dictionaryRepresentation] forKey:kDataGenConfig];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.approveGuid = [aDecoder decodeObjectForKey:kDataApproveGuid];
    self.cell = [aDecoder decodeObjectForKey:kDataCell];
    self.ssnNumber = [aDecoder decodeObjectForKey:kDataSsnNumber];
    self.useOffical = [aDecoder decodeObjectForKey:kDataUseOffical];
    self.logDeviceId = [aDecoder decodeObjectForKey:kDataLogDeviceId];
    self.dataIdentifier = [aDecoder decodeObjectForKey:kDataId];
    self.lastLat = [aDecoder decodeObjectForKey:kDataLastLat];
    self.country = [aDecoder decodeObjectForKey:kDataCountry];
    self.qstDisability = [aDecoder decodeObjectForKey:kDataQstDisability];
    self.userRate = [aDecoder decodeObjectForKey:kDataUserRate];
    self.updatedBy = [aDecoder decodeObjectForKey:kDataUpdatedBy];
    self.isDisabled = [aDecoder decodeObjectForKey:kDataIsDisabled];
    self.useLocation = [aDecoder decodeObjectForKey:kDataUseLocation];
    self.logSessionKey = [aDecoder decodeObjectForKey:kDataLogSessionKey];
    self.approvedByAdmin = [aDecoder decodeObjectForKey:kDataApprovedByAdmin];
    self.meritalStatus = [aDecoder decodeObjectForKey:kDataMeritalStatus];
    self.availStatus = [aDecoder decodeObjectForKey:kDataAvailStatus];
    self.qstCriminal = [aDecoder decodeObjectForKey:kDataQstCriminal];
    self.idcatdState = [aDecoder decodeObjectForKey:kDataIdcatdState];
    self.userProfiles = [aDecoder decodeObjectForKey:kDataUserProfiles];
    self.email = [aDecoder decodeObjectForKey:kDataEmail];
    self.idcardExpDate = [aDecoder decodeObjectForKey:kDataIdcardExpDate];
    self.logUserArgent = [aDecoder decodeObjectForKey:kDataLogUserArgent];
    self.insertedDate = [aDecoder decodeObjectForKey:kDataInsertedDate];
    self.surname = [aDecoder decodeObjectForKey:kDataSurname];
    self.idcardNumber = [aDecoder decodeObjectForKey:kDataIdcardNumber];
    self.updatedDate = [aDecoder decodeObjectForKey:kDataUpdatedDate];
    self.name = [aDecoder decodeObjectForKey:kDataName];
    self.lastLng = [aDecoder decodeObjectForKey:kDataLastLng];
    self.qstSexsual = [aDecoder decodeObjectForKey:kDataQstSexsual];
    self.referalName = [aDecoder decodeObjectForKey:kDataReferalName];
    self.city = [aDecoder decodeObjectForKey:kDataCity];
    self.selfImg = [aDecoder decodeObjectForKey:kDataSelfImg];
    self.systemRow = [aDecoder decodeObjectForKey:kDataSystemRow];
    self.lastSeenDate = [aDecoder decodeObjectForKey:kDataLastSeenDate];
    self.addressOffical = [aDecoder decodeObjectForKey:kDataAddressOffical];
    self.idcardImg2 = [aDecoder decodeObjectForKey:kDataIdcardImg2];
    self.gender = [aDecoder decodeObjectForKey:kDataGender];
    self.isApproved = [aDecoder decodeObjectForKey:kDataIsApproved];
    self.idcardImg1 = [aDecoder decodeObjectForKey:kDataIdcardImg1];
    self.addressActual = [aDecoder decodeObjectForKey:kDataAddressActual];
    self.paypalEmail = [aDecoder decodeObjectForKey:kDataPaypalEmail];
    self.referalCell = [aDecoder decodeObjectForKey:kDataReferalCell];
    self.logDate = [aDecoder decodeObjectForKey:kDataLogDate];
    self.lastLocationDate = [aDecoder decodeObjectForKey:kDataLastLocationDate];
    self.password = [aDecoder decodeObjectForKey:kDataPassword];
    self.insertedBy = [aDecoder decodeObjectForKey:kDataInsertedBy];
    self.qstHealth = [aDecoder decodeObjectForKey:kDataQstHealth];
    self.genConfig = [aDecoder decodeObjectForKey:kDataGenConfig];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_approveGuid forKey:kDataApproveGuid];
    [aCoder encodeObject:_cell forKey:kDataCell];
    [aCoder encodeObject:_ssnNumber forKey:kDataSsnNumber];
    [aCoder encodeObject:_useOffical forKey:kDataUseOffical];
    [aCoder encodeObject:_logDeviceId forKey:kDataLogDeviceId];
    [aCoder encodeObject:_dataIdentifier forKey:kDataId];
    [aCoder encodeObject:_lastLat forKey:kDataLastLat];
    [aCoder encodeObject:_country forKey:kDataCountry];
    [aCoder encodeObject:_qstDisability forKey:kDataQstDisability];
    [aCoder encodeObject:_userRate forKey:kDataUserRate];
    [aCoder encodeObject:_updatedBy forKey:kDataUpdatedBy];
    [aCoder encodeObject:_isDisabled forKey:kDataIsDisabled];
    [aCoder encodeObject:_useLocation forKey:kDataUseLocation];
    [aCoder encodeObject:_logSessionKey forKey:kDataLogSessionKey];
    [aCoder encodeObject:_approvedByAdmin forKey:kDataApprovedByAdmin];
    [aCoder encodeObject:_meritalStatus forKey:kDataMeritalStatus];
    [aCoder encodeObject:_availStatus forKey:kDataAvailStatus];
    [aCoder encodeObject:_qstCriminal forKey:kDataQstCriminal];
    [aCoder encodeObject:_idcatdState forKey:kDataIdcatdState];
    [aCoder encodeObject:_userProfiles forKey:kDataUserProfiles];
    [aCoder encodeObject:_email forKey:kDataEmail];
    [aCoder encodeObject:_idcardExpDate forKey:kDataIdcardExpDate];
    [aCoder encodeObject:_logUserArgent forKey:kDataLogUserArgent];
    [aCoder encodeObject:_insertedDate forKey:kDataInsertedDate];
    [aCoder encodeObject:_surname forKey:kDataSurname];
    [aCoder encodeObject:_idcardNumber forKey:kDataIdcardNumber];
    [aCoder encodeObject:_updatedDate forKey:kDataUpdatedDate];
    [aCoder encodeObject:_name forKey:kDataName];
    [aCoder encodeObject:_lastLng forKey:kDataLastLng];
    [aCoder encodeObject:_qstSexsual forKey:kDataQstSexsual];
    [aCoder encodeObject:_referalName forKey:kDataReferalName];
    [aCoder encodeObject:_city forKey:kDataCity];
    [aCoder encodeObject:_selfImg forKey:kDataSelfImg];
    [aCoder encodeObject:_systemRow forKey:kDataSystemRow];
    [aCoder encodeObject:_lastSeenDate forKey:kDataLastSeenDate];
    [aCoder encodeObject:_addressOffical forKey:kDataAddressOffical];
    [aCoder encodeObject:_idcardImg2 forKey:kDataIdcardImg2];
    [aCoder encodeObject:_gender forKey:kDataGender];
    [aCoder encodeObject:_isApproved forKey:kDataIsApproved];
    [aCoder encodeObject:_idcardImg1 forKey:kDataIdcardImg1];
    [aCoder encodeObject:_addressActual forKey:kDataAddressActual];
    [aCoder encodeObject:_paypalEmail forKey:kDataPaypalEmail];
    [aCoder encodeObject:_referalCell forKey:kDataReferalCell];
    [aCoder encodeObject:_logDate forKey:kDataLogDate];
    [aCoder encodeObject:_lastLocationDate forKey:kDataLastLocationDate];
    [aCoder encodeObject:_password forKey:kDataPassword];
    [aCoder encodeObject:_insertedBy forKey:kDataInsertedBy];
    [aCoder encodeObject:_qstHealth forKey:kDataQstHealth];
    [aCoder encodeObject:_genConfig forKey:kDataGenConfig];
}

- (id)copyWithZone:(NSZone *)zone {
    Data *copy = [[Data alloc] init];
    
    
    
    if (copy) {

        copy.approveGuid = [self.approveGuid copyWithZone:zone];
        copy.cell = [self.cell copyWithZone:zone];
        copy.ssnNumber = [self.ssnNumber copyWithZone:zone];
        copy.useOffical = [self.useOffical copyWithZone:zone];
        copy.logDeviceId = [self.logDeviceId copyWithZone:zone];
        copy.dataIdentifier = [self.dataIdentifier copyWithZone:zone];
        copy.lastLat = [self.lastLat copyWithZone:zone];
        copy.country = [self.country copyWithZone:zone];
        copy.qstDisability = [self.qstDisability copyWithZone:zone];
        copy.userRate = [self.userRate copyWithZone:zone];
        copy.updatedBy = [self.updatedBy copyWithZone:zone];
        copy.isDisabled = [self.isDisabled copyWithZone:zone];
        copy.useLocation = [self.useLocation copyWithZone:zone];
        copy.logSessionKey = [self.logSessionKey copyWithZone:zone];
        copy.approvedByAdmin = [self.approvedByAdmin copyWithZone:zone];
        copy.meritalStatus = [self.meritalStatus copyWithZone:zone];
        copy.availStatus = [self.availStatus copyWithZone:zone];
        copy.qstCriminal = [self.qstCriminal copyWithZone:zone];
        copy.idcatdState = [self.idcatdState copyWithZone:zone];
        copy.userProfiles = [self.userProfiles copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.idcardExpDate = [self.idcardExpDate copyWithZone:zone];
        copy.logUserArgent = [self.logUserArgent copyWithZone:zone];
        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.surname = [self.surname copyWithZone:zone];
        copy.idcardNumber = [self.idcardNumber copyWithZone:zone];
        copy.updatedDate = [self.updatedDate copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.lastLng = [self.lastLng copyWithZone:zone];
        copy.qstSexsual = [self.qstSexsual copyWithZone:zone];
        copy.referalName = [self.referalName copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
        copy.selfImg = [self.selfImg copyWithZone:zone];
        copy.systemRow = [self.systemRow copyWithZone:zone];
        copy.lastSeenDate = [self.lastSeenDate copyWithZone:zone];
        copy.addressOffical = [self.addressOffical copyWithZone:zone];
        copy.idcardImg2 = [self.idcardImg2 copyWithZone:zone];
        copy.gender = [self.gender copyWithZone:zone];
        copy.isApproved = [self.isApproved copyWithZone:zone];
        copy.idcardImg1 = [self.idcardImg1 copyWithZone:zone];
        copy.addressActual = [self.addressActual copyWithZone:zone];
        copy.paypalEmail = [self.paypalEmail copyWithZone:zone];
        copy.referalCell = [self.referalCell copyWithZone:zone];
        copy.logDate = [self.logDate copyWithZone:zone];
        copy.lastLocationDate = [self.lastLocationDate copyWithZone:zone];
        copy.password = [self.password copyWithZone:zone];
        copy.insertedBy = [self.insertedBy copyWithZone:zone];
        copy.qstHealth = [self.qstHealth copyWithZone:zone];
        copy.genConfig = [self.genConfig copyWithZone:zone];
    }
    
    return copy;
}


@end
