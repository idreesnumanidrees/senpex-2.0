//
//  Session+CoreDataProperties.h
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "Session+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Session (CoreDataProperties)

+ (NSFetchRequest<Session *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *sessionKey;

@end

NS_ASSUME_NONNULL_END
