//
//  CheapSender+CoreDataModel.h
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "CardInfo+CoreDataClass.h"
#import "CourierInfo+CoreDataClass.h"
#import "LastChatMessage+CoreDataClass.h"
#import "Package+CoreDataClass.h"
#import "SenderInfo+CoreDataClass.h"
#import "Session+CoreDataClass.h"
#import "User+CoreDataClass.h"




