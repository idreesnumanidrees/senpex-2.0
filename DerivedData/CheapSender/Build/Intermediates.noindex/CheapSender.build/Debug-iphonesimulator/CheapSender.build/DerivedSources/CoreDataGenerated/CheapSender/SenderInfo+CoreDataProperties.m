//
//  SenderInfo+CoreDataProperties.m
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "SenderInfo+CoreDataProperties.h"

@implementation SenderInfo (CoreDataProperties)

+ (NSFetchRequest<SenderInfo *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"SenderInfo"];
}

@dynamic address;
@dynamic email;
@dynamic imageName;
@dynamic name;
@dynamic senderId;
@dynamic surname;

@end
