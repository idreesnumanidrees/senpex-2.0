//
//  Package+CoreDataProperties.h
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "Package+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Package (CoreDataProperties)

+ (NSFetchRequest<Package *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *dateTime;
@property (nonatomic) int64_t packID;
@property (nonatomic) int16_t timerValue;

@end

NS_ASSUME_NONNULL_END
