//
//  CardInfo+CoreDataProperties.m
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "CardInfo+CoreDataProperties.h"

@implementation CardInfo (CoreDataProperties)

+ (NSFetchRequest<CardInfo *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CardInfo"];
}

@dynamic cardID;
@dynamic cardNumber;
@dynamic stripToken;

@end
