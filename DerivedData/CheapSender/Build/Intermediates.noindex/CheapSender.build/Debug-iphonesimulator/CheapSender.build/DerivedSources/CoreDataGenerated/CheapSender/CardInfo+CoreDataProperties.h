//
//  CardInfo+CoreDataProperties.h
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "CardInfo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CardInfo (CoreDataProperties)

+ (NSFetchRequest<CardInfo *> *)fetchRequest;

@property (nonatomic) int64_t cardID;
@property (nullable, nonatomic, copy) NSString *cardNumber;
@property (nullable, nonatomic, copy) NSString *stripToken;

@end

NS_ASSUME_NONNULL_END
