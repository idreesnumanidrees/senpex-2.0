//
//  Session+CoreDataProperties.m
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "Session+CoreDataProperties.h"

@implementation Session (CoreDataProperties)

+ (NSFetchRequest<Session *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Session"];
}

@dynamic sessionKey;

@end
