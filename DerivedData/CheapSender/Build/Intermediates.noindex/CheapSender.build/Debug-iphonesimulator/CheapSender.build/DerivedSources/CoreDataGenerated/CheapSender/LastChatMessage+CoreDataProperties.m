//
//  LastChatMessage+CoreDataProperties.m
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "LastChatMessage+CoreDataProperties.h"

@implementation LastChatMessage (CoreDataProperties)

+ (NSFetchRequest<LastChatMessage *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"LastChatMessage"];
}

@dynamic message;
@dynamic userId;

@end
