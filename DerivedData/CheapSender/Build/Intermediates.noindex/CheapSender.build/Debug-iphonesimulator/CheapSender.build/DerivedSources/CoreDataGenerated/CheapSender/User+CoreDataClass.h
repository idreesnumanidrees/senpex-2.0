//
//  User+CoreDataClass.h
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface User : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "User+CoreDataProperties.h"
