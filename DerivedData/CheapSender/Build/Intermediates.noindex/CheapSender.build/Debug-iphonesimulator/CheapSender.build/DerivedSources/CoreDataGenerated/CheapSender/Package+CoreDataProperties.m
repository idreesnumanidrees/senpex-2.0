//
//  Package+CoreDataProperties.m
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "Package+CoreDataProperties.h"

@implementation Package (CoreDataProperties)

+ (NSFetchRequest<Package *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"Package"];
}

@dynamic dateTime;
@dynamic packID;
@dynamic timerValue;

@end
