//
//  CourierInfo+CoreDataProperties.h
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "CourierInfo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CourierInfo (CoreDataProperties)

+ (NSFetchRequest<CourierInfo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *address;
@property (nullable, nonatomic, copy) NSString *balance;
@property (nonatomic) int64_t courierId;
@property (nullable, nonatomic, copy) NSString *email;
@property (nullable, nonatomic, copy) NSString *imageName;
@property (nullable, nonatomic, copy) NSString *name;
@property (nullable, nonatomic, copy) NSString *rate;
@property (nullable, nonatomic, copy) NSString *surname;

@end

NS_ASSUME_NONNULL_END
