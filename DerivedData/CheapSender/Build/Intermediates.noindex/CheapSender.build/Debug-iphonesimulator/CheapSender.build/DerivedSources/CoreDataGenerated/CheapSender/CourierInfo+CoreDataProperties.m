//
//  CourierInfo+CoreDataProperties.m
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "CourierInfo+CoreDataProperties.h"

@implementation CourierInfo (CoreDataProperties)

+ (NSFetchRequest<CourierInfo *> *)fetchRequest {
	return [NSFetchRequest fetchRequestWithEntityName:@"CourierInfo"];
}

@dynamic address;
@dynamic balance;
@dynamic courierId;
@dynamic email;
@dynamic imageName;
@dynamic name;
@dynamic rate;
@dynamic surname;

@end
