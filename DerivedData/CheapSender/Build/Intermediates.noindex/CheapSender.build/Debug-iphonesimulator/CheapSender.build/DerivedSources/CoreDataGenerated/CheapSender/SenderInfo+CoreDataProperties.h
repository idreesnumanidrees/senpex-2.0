//
//  SenderInfo+CoreDataProperties.h
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "SenderInfo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface SenderInfo (CoreDataProperties)

+ (NSFetchRequest<SenderInfo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *address;
@property (nullable, nonatomic, copy) NSString *email;
@property (nullable, nonatomic, copy) NSString *imageName;
@property (nullable, nonatomic, copy) NSString *name;
@property (nonatomic) int64_t senderId;
@property (nullable, nonatomic, copy) NSString *surname;

@end

NS_ASSUME_NONNULL_END
