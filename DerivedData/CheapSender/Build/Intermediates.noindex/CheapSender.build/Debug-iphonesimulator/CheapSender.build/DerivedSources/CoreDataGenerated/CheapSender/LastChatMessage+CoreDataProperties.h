//
//  LastChatMessage+CoreDataProperties.h
//  
//
//  Created by devrgu on 5/2/18.
//
//  This file was automatically generated and should not be edited.
//

#import "LastChatMessage+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface LastChatMessage (CoreDataProperties)

+ (NSFetchRequest<LastChatMessage *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *message;
@property (nonatomic) int64_t userId;

@end

NS_ASSUME_NONNULL_END
