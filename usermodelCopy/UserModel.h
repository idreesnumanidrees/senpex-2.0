//
//  UserModel.h
//
//  Created by   on 2017/06/03
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>



@interface UserModel : NSObject <NSCoding, NSCopying>

@property (nonatomic, strong) NSString *approveGuid;
@property (nonatomic, strong) NSString *cell;
@property (nonatomic, assign) id ssnNumber;
@property (nonatomic, assign) id qstHealth;
@property (nonatomic, strong) NSString *internalBaseClassIdentifier;
@property (nonatomic, assign) id lastLat;
@property (nonatomic, assign) NSString *userRate;
@property (nonatomic, assign) id country;
@property (nonatomic, assign) id qstDisability;
@property (nonatomic, assign) id updatedBy;
@property (nonatomic, strong) NSString *isDisabled;
@property (nonatomic, assign) id useLocation;
@property (nonatomic, strong) NSString *logSessionKey;
@property (nonatomic, strong) NSString *approvedByAdmin;
@property (nonatomic, assign) id meritalStatus;
@property (nonatomic, assign) id availStatus;
@property (nonatomic, assign) id qstCriminal;
@property (nonatomic, assign) id idcatdState;
@property (nonatomic, strong) NSArray *userProfiles;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, assign) id idcardExpDate;
@property (nonatomic, strong) NSString *logUserArgent;
@property (nonatomic, strong) NSString *insertedDate;
@property (nonatomic, strong) NSString *surname;
@property (nonatomic, assign) id lastLng;
@property (nonatomic, assign) id idcardNumber;
@property (nonatomic, assign) id updatedDate;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) id qstSexsual;
@property (nonatomic, assign) id systemRow;
@property (nonatomic, assign) id city;
@property (nonatomic, assign) id referalName;
@property (nonatomic, assign) id selfImg;
@property (nonatomic, assign) id lastSeenDate;
@property (nonatomic, assign) id addressOffical;
@property (nonatomic, assign) id idcardImg2;
@property (nonatomic, assign) id gender;
@property (nonatomic, strong) NSString *isApproved;
@property (nonatomic, assign) id idcardImg1;
@property (nonatomic, strong) NSString *addressActual;
@property (nonatomic, assign) id paypalEmail;
@property (nonatomic, assign) id referalCell;
@property (nonatomic, strong) NSString *logDate;
@property (nonatomic, assign) id lastLocationDate;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, assign) id useOffical;
@property (nonatomic, strong) NSString *logDeviceId;
@property (nonatomic, strong) NSString *insertedBy;

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (instancetype)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end
