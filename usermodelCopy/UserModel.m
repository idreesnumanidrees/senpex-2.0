//
//  UserModel.m
//
//  Created by   on 2017/06/03
//  Copyright (c) 2017 __MyCompanyName__. All rights reserved.
//

#import "UserModel.h"
#import "UserProfiles.h"


NSString *const kUserModelApproveGuid = @"approve_guid";
NSString *const kUserModelCell = @"cell";
NSString *const kUserModelSsnNumber = @"ssn_number";
NSString *const kUserModelQstHealth = @"qst_health";
NSString *const kUserModelId = @"id";
NSString *const kUserModelLastLat = @"last_lat";
NSString *const kUserModelUserRate = @"user_rate";
NSString *const kUserModelCountry = @"country";
NSString *const kUserModelQstDisability = @"qst_disability";
NSString *const kUserModelUpdatedBy = @"updated_by";
NSString *const kUserModelIsDisabled = @"is_disabled";
NSString *const kUserModelUseLocation = @"use_location";
NSString *const kUserModelLogSessionKey = @"log_session_key";
NSString *const kUserModelApprovedByAdmin = @"approved_by_admin";
NSString *const kUserModelMeritalStatus = @"merital_status";
NSString *const kUserModelAvailStatus = @"avail_status";
NSString *const kUserModelQstCriminal = @"qst_criminal";
NSString *const kUserModelIdcatdState = @"idcatd_state";
NSString *const kUserModelUserProfiles = @"user_profiles";
NSString *const kUserModelEmail = @"email";
NSString *const kUserModelIdcardExpDate = @"idcard_exp_date";
NSString *const kUserModelLogUserArgent = @"log_user_argent";
NSString *const kUserModelInsertedDate = @"inserted_date";
NSString *const kUserModelSurname = @"surname";
NSString *const kUserModelLastLng = @"last_lng";
NSString *const kUserModelIdcardNumber = @"idcard_number";
NSString *const kUserModelUpdatedDate = @"updated_date";
NSString *const kUserModelName = @"name";
NSString *const kUserModelQstSexsual = @"qst_sexsual";
NSString *const kUserModelSystemRow = @"system_row";
NSString *const kUserModelCity = @"city";
NSString *const kUserModelReferalName = @"referal_name";
NSString *const kUserModelSelfImg = @"self_img";
NSString *const kUserModelLastSeenDate = @"last_seen_date";
NSString *const kUserModelAddressOffical = @"address_offical";
NSString *const kUserModelIdcardImg2 = @"idcard_img2";
NSString *const kUserModelGender = @"gender";
NSString *const kUserModelIsApproved = @"is_approved";
NSString *const kUserModelIdcardImg1 = @"idcard_img1";
NSString *const kUserModelAddressActual = @"address_actual";
NSString *const kUserModelPaypalEmail = @"paypal_email";
NSString *const kUserModelReferalCell = @"referal_cell";
NSString *const kUserModelLogDate = @"log_date";
NSString *const kUserModelLastLocationDate = @"last_location_date";
NSString *const kUserModelPassword = @"password";
NSString *const kUserModelUseOffical = @"use_offical";
NSString *const kUserModelLogDeviceId = @"log_device_id";
NSString *const kUserModelInsertedBy = @"inserted_by";


@interface UserModel ()

- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end

@implementation UserModel

@synthesize approveGuid = _approveGuid;
@synthesize cell = _cell;
@synthesize ssnNumber = _ssnNumber;
@synthesize qstHealth = _qstHealth;
@synthesize internalBaseClassIdentifier = _internalBaseClassIdentifier;
@synthesize lastLat = _lastLat;
@synthesize userRate = _userRate;
@synthesize country = _country;
@synthesize qstDisability = _qstDisability;
@synthesize updatedBy = _updatedBy;
@synthesize isDisabled = _isDisabled;
@synthesize useLocation = _useLocation;
@synthesize logSessionKey = _logSessionKey;
@synthesize approvedByAdmin = _approvedByAdmin;
@synthesize meritalStatus = _meritalStatus;
@synthesize availStatus = _availStatus;
@synthesize qstCriminal = _qstCriminal;
@synthesize idcatdState = _idcatdState;
@synthesize userProfiles = _userProfiles;
@synthesize email = _email;
@synthesize idcardExpDate = _idcardExpDate;
@synthesize logUserArgent = _logUserArgent;
@synthesize insertedDate = _insertedDate;
@synthesize surname = _surname;
@synthesize lastLng = _lastLng;
@synthesize idcardNumber = _idcardNumber;
@synthesize updatedDate = _updatedDate;
@synthesize name = _name;
@synthesize qstSexsual = _qstSexsual;
@synthesize systemRow = _systemRow;
@synthesize city = _city;
@synthesize referalName = _referalName;
@synthesize selfImg = _selfImg;
@synthesize lastSeenDate = _lastSeenDate;
@synthesize addressOffical = _addressOffical;
@synthesize idcardImg2 = _idcardImg2;
@synthesize gender = _gender;
@synthesize isApproved = _isApproved;
@synthesize idcardImg1 = _idcardImg1;
@synthesize addressActual = _addressActual;
@synthesize paypalEmail = _paypalEmail;
@synthesize referalCell = _referalCell;
@synthesize logDate = _logDate;
@synthesize lastLocationDate = _lastLocationDate;
@synthesize password = _password;
@synthesize useOffical = _useOffical;
@synthesize logDeviceId = _logDeviceId;
@synthesize insertedBy = _insertedBy;


+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict {
    return [[self alloc] initWithDictionary:dict];
}

- (instancetype)initWithDictionary:(NSDictionary *)dict {
    self = [super init];
    
    // This check serves to make sure that a non-NSDictionary object
    // passed into the model class doesn't break the parsing.
    if (self && [dict isKindOfClass:[NSDictionary class]]) {
            self.approveGuid = [self objectOrNilForKey:kUserModelApproveGuid fromDictionary:dict];
            self.cell = [self objectOrNilForKey:kUserModelCell fromDictionary:dict];
            self.ssnNumber = [self objectOrNilForKey:kUserModelSsnNumber fromDictionary:dict];
            self.qstHealth = [self objectOrNilForKey:kUserModelQstHealth fromDictionary:dict];
            self.internalBaseClassIdentifier = [self objectOrNilForKey:kUserModelId fromDictionary:dict];
            self.lastLat = [self objectOrNilForKey:kUserModelLastLat fromDictionary:dict];
           self.userRate = [self objectOrNilForKey:kUserModelUserRate fromDictionary:dict];
            self.country = [self objectOrNilForKey:kUserModelCountry fromDictionary:dict];
            self.qstDisability = [self objectOrNilForKey:kUserModelQstDisability fromDictionary:dict];
            self.updatedBy = [self objectOrNilForKey:kUserModelUpdatedBy fromDictionary:dict];
            self.isDisabled = [self objectOrNilForKey:kUserModelIsDisabled fromDictionary:dict];
            self.useLocation = [self objectOrNilForKey:kUserModelUseLocation fromDictionary:dict];
            self.logSessionKey = [self objectOrNilForKey:kUserModelLogSessionKey fromDictionary:dict];
            self.approvedByAdmin = [self objectOrNilForKey:kUserModelApprovedByAdmin fromDictionary:dict];
            self.meritalStatus = [self objectOrNilForKey:kUserModelMeritalStatus fromDictionary:dict];
            self.availStatus = [self objectOrNilForKey:kUserModelAvailStatus fromDictionary:dict];
            self.qstCriminal = [self objectOrNilForKey:kUserModelQstCriminal fromDictionary:dict];
            self.idcatdState = [self objectOrNilForKey:kUserModelIdcatdState fromDictionary:dict];
    NSObject *receivedUserProfiles = [dict objectForKey:kUserModelUserProfiles];
    NSMutableArray *parsedUserProfiles = [NSMutableArray array];
    
    if ([receivedUserProfiles isKindOfClass:[NSArray class]]) {
        for (NSDictionary *item in (NSArray *)receivedUserProfiles) {
            if ([item isKindOfClass:[NSDictionary class]]) {
                [parsedUserProfiles addObject:[UserProfiles modelObjectWithDictionary:item]];
            }
       }
    } else if ([receivedUserProfiles isKindOfClass:[NSDictionary class]]) {
       [parsedUserProfiles addObject:[UserProfiles modelObjectWithDictionary:(NSDictionary *)receivedUserProfiles]];
    }

    self.userProfiles = [NSArray arrayWithArray:parsedUserProfiles];
            self.email = [self objectOrNilForKey:kUserModelEmail fromDictionary:dict];
            self.idcardExpDate = [self objectOrNilForKey:kUserModelIdcardExpDate fromDictionary:dict];
            self.logUserArgent = [self objectOrNilForKey:kUserModelLogUserArgent fromDictionary:dict];
            self.insertedDate = [self objectOrNilForKey:kUserModelInsertedDate fromDictionary:dict];
            self.surname = [self objectOrNilForKey:kUserModelSurname fromDictionary:dict];
            self.lastLng = [self objectOrNilForKey:kUserModelLastLng fromDictionary:dict];
            self.idcardNumber = [self objectOrNilForKey:kUserModelIdcardNumber fromDictionary:dict];
            self.updatedDate = [self objectOrNilForKey:kUserModelUpdatedDate fromDictionary:dict];
            self.name = [self objectOrNilForKey:kUserModelName fromDictionary:dict];
            self.qstSexsual = [self objectOrNilForKey:kUserModelQstSexsual fromDictionary:dict];
            self.systemRow = [self objectOrNilForKey:kUserModelSystemRow fromDictionary:dict];
            self.city = [self objectOrNilForKey:kUserModelCity fromDictionary:dict];
            self.referalName = [self objectOrNilForKey:kUserModelReferalName fromDictionary:dict];
            self.selfImg = [self objectOrNilForKey:kUserModelSelfImg fromDictionary:dict];
            self.lastSeenDate = [self objectOrNilForKey:kUserModelLastSeenDate fromDictionary:dict];
            self.addressOffical = [self objectOrNilForKey:kUserModelAddressOffical fromDictionary:dict];
            self.idcardImg2 = [self objectOrNilForKey:kUserModelIdcardImg2 fromDictionary:dict];
            self.gender = [self objectOrNilForKey:kUserModelGender fromDictionary:dict];
            self.isApproved = [self objectOrNilForKey:kUserModelIsApproved fromDictionary:dict];
            self.idcardImg1 = [self objectOrNilForKey:kUserModelIdcardImg1 fromDictionary:dict];
            self.addressActual = [self objectOrNilForKey:kUserModelAddressActual fromDictionary:dict];
            self.paypalEmail = [self objectOrNilForKey:kUserModelPaypalEmail fromDictionary:dict];
            self.referalCell = [self objectOrNilForKey:kUserModelReferalCell fromDictionary:dict];
            self.logDate = [self objectOrNilForKey:kUserModelLogDate fromDictionary:dict];
            self.lastLocationDate = [self objectOrNilForKey:kUserModelLastLocationDate fromDictionary:dict];
            self.password = [self objectOrNilForKey:kUserModelPassword fromDictionary:dict];
            self.useOffical = [self objectOrNilForKey:kUserModelUseOffical fromDictionary:dict];
            self.logDeviceId = [self objectOrNilForKey:kUserModelLogDeviceId fromDictionary:dict];
            self.insertedBy = [self objectOrNilForKey:kUserModelInsertedBy fromDictionary:dict];

    }
    
    return self;
    
}

- (NSDictionary *)dictionaryRepresentation {
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.approveGuid forKey:kUserModelApproveGuid];
    [mutableDict setValue:self.cell forKey:kUserModelCell];
    [mutableDict setValue:self.ssnNumber forKey:kUserModelSsnNumber];
    [mutableDict setValue:self.qstHealth forKey:kUserModelQstHealth];
    [mutableDict setValue:self.internalBaseClassIdentifier forKey:kUserModelId];
    [mutableDict setValue:self.lastLat forKey:kUserModelLastLat];
    [mutableDict setValue:self.userRate forKey:kUserModelUserRate];
    [mutableDict setValue:self.country forKey:kUserModelCountry];
    [mutableDict setValue:self.qstDisability forKey:kUserModelQstDisability];
    [mutableDict setValue:self.updatedBy forKey:kUserModelUpdatedBy];
    [mutableDict setValue:self.isDisabled forKey:kUserModelIsDisabled];
    [mutableDict setValue:self.useLocation forKey:kUserModelUseLocation];
    [mutableDict setValue:self.logSessionKey forKey:kUserModelLogSessionKey];
    [mutableDict setValue:self.approvedByAdmin forKey:kUserModelApprovedByAdmin];
    [mutableDict setValue:self.meritalStatus forKey:kUserModelMeritalStatus];
    [mutableDict setValue:self.availStatus forKey:kUserModelAvailStatus];
    [mutableDict setValue:self.qstCriminal forKey:kUserModelQstCriminal];
    [mutableDict setValue:self.idcatdState forKey:kUserModelIdcatdState];
    NSMutableArray *tempArrayForUserProfiles = [NSMutableArray array];
    
    for (NSObject *subArrayObject in self.userProfiles) {
        if ([subArrayObject respondsToSelector:@selector(dictionaryRepresentation)]) {
            // This class is a model object
            [tempArrayForUserProfiles addObject:[subArrayObject performSelector:@selector(dictionaryRepresentation)]];
        } else {
            // Generic object
            [tempArrayForUserProfiles addObject:subArrayObject];
        }
    }
    [mutableDict setValue:[NSArray arrayWithArray:tempArrayForUserProfiles] forKey:kUserModelUserProfiles];
    [mutableDict setValue:self.email forKey:kUserModelEmail];
    [mutableDict setValue:self.idcardExpDate forKey:kUserModelIdcardExpDate];
    [mutableDict setValue:self.logUserArgent forKey:kUserModelLogUserArgent];
    [mutableDict setValue:self.insertedDate forKey:kUserModelInsertedDate];
    [mutableDict setValue:self.surname forKey:kUserModelSurname];
    [mutableDict setValue:self.lastLng forKey:kUserModelLastLng];
    [mutableDict setValue:self.idcardNumber forKey:kUserModelIdcardNumber];
    [mutableDict setValue:self.updatedDate forKey:kUserModelUpdatedDate];
    [mutableDict setValue:self.name forKey:kUserModelName];
    [mutableDict setValue:self.qstSexsual forKey:kUserModelQstSexsual];
    [mutableDict setValue:self.systemRow forKey:kUserModelSystemRow];
    [mutableDict setValue:self.city forKey:kUserModelCity];
    [mutableDict setValue:self.referalName forKey:kUserModelReferalName];
    [mutableDict setValue:self.selfImg forKey:kUserModelSelfImg];
    [mutableDict setValue:self.lastSeenDate forKey:kUserModelLastSeenDate];
    [mutableDict setValue:self.addressOffical forKey:kUserModelAddressOffical];
    [mutableDict setValue:self.idcardImg2 forKey:kUserModelIdcardImg2];
    [mutableDict setValue:self.gender forKey:kUserModelGender];
    [mutableDict setValue:self.isApproved forKey:kUserModelIsApproved];
    [mutableDict setValue:self.idcardImg1 forKey:kUserModelIdcardImg1];
    [mutableDict setValue:self.addressActual forKey:kUserModelAddressActual];
    [mutableDict setValue:self.paypalEmail forKey:kUserModelPaypalEmail];
    [mutableDict setValue:self.referalCell forKey:kUserModelReferalCell];
    [mutableDict setValue:self.logDate forKey:kUserModelLogDate];
    [mutableDict setValue:self.lastLocationDate forKey:kUserModelLastLocationDate];
    [mutableDict setValue:self.password forKey:kUserModelPassword];
    [mutableDict setValue:self.useOffical forKey:kUserModelUseOffical];
    [mutableDict setValue:self.logDeviceId forKey:kUserModelLogDeviceId];
    [mutableDict setValue:self.insertedBy forKey:kUserModelInsertedBy];

    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (NSString *)description  {
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}

#pragma mark - Helper Method
- (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict {
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];

    self.approveGuid = [aDecoder decodeObjectForKey:kUserModelApproveGuid];
    self.cell = [aDecoder decodeObjectForKey:kUserModelCell];
    self.ssnNumber = [aDecoder decodeObjectForKey:kUserModelSsnNumber];
    self.qstHealth = [aDecoder decodeObjectForKey:kUserModelQstHealth];
    self.internalBaseClassIdentifier = [aDecoder decodeObjectForKey:kUserModelId];
    self.lastLat = [aDecoder decodeObjectForKey:kUserModelLastLat];
    self.userRate = [aDecoder decodeObjectForKey:kUserModelUserRate];
    self.country = [aDecoder decodeObjectForKey:kUserModelCountry];
    self.qstDisability = [aDecoder decodeObjectForKey:kUserModelQstDisability];
    self.updatedBy = [aDecoder decodeObjectForKey:kUserModelUpdatedBy];
    self.isDisabled = [aDecoder decodeObjectForKey:kUserModelIsDisabled];
    self.useLocation = [aDecoder decodeObjectForKey:kUserModelUseLocation];
    self.logSessionKey = [aDecoder decodeObjectForKey:kUserModelLogSessionKey];
    self.approvedByAdmin = [aDecoder decodeObjectForKey:kUserModelApprovedByAdmin];
    self.meritalStatus = [aDecoder decodeObjectForKey:kUserModelMeritalStatus];
    self.availStatus = [aDecoder decodeObjectForKey:kUserModelAvailStatus];
    self.qstCriminal = [aDecoder decodeObjectForKey:kUserModelQstCriminal];
    self.idcatdState = [aDecoder decodeObjectForKey:kUserModelIdcatdState];
    self.userProfiles = [aDecoder decodeObjectForKey:kUserModelUserProfiles];
    self.email = [aDecoder decodeObjectForKey:kUserModelEmail];
    self.idcardExpDate = [aDecoder decodeObjectForKey:kUserModelIdcardExpDate];
    self.logUserArgent = [aDecoder decodeObjectForKey:kUserModelLogUserArgent];
    self.insertedDate = [aDecoder decodeObjectForKey:kUserModelInsertedDate];
    self.surname = [aDecoder decodeObjectForKey:kUserModelSurname];
    self.lastLng = [aDecoder decodeObjectForKey:kUserModelLastLng];
    self.idcardNumber = [aDecoder decodeObjectForKey:kUserModelIdcardNumber];
    self.updatedDate = [aDecoder decodeObjectForKey:kUserModelUpdatedDate];
    self.name = [aDecoder decodeObjectForKey:kUserModelName];
    self.qstSexsual = [aDecoder decodeObjectForKey:kUserModelQstSexsual];
    self.systemRow = [aDecoder decodeObjectForKey:kUserModelSystemRow];
    self.city = [aDecoder decodeObjectForKey:kUserModelCity];
    self.referalName = [aDecoder decodeObjectForKey:kUserModelReferalName];
    self.selfImg = [aDecoder decodeObjectForKey:kUserModelSelfImg];
    self.lastSeenDate = [aDecoder decodeObjectForKey:kUserModelLastSeenDate];
    self.addressOffical = [aDecoder decodeObjectForKey:kUserModelAddressOffical];
    self.idcardImg2 = [aDecoder decodeObjectForKey:kUserModelIdcardImg2];
    self.gender = [aDecoder decodeObjectForKey:kUserModelGender];
    self.isApproved = [aDecoder decodeObjectForKey:kUserModelIsApproved];
    self.idcardImg1 = [aDecoder decodeObjectForKey:kUserModelIdcardImg1];
    self.addressActual = [aDecoder decodeObjectForKey:kUserModelAddressActual];
    self.paypalEmail = [aDecoder decodeObjectForKey:kUserModelPaypalEmail];
    self.referalCell = [aDecoder decodeObjectForKey:kUserModelReferalCell];
    self.logDate = [aDecoder decodeObjectForKey:kUserModelLogDate];
    self.lastLocationDate = [aDecoder decodeObjectForKey:kUserModelLastLocationDate];
    self.password = [aDecoder decodeObjectForKey:kUserModelPassword];
    self.useOffical = [aDecoder decodeObjectForKey:kUserModelUseOffical];
    self.logDeviceId = [aDecoder decodeObjectForKey:kUserModelLogDeviceId];
    self.insertedBy = [aDecoder decodeObjectForKey:kUserModelInsertedBy];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{

    [aCoder encodeObject:_approveGuid forKey:kUserModelApproveGuid];
    [aCoder encodeObject:_cell forKey:kUserModelCell];
    [aCoder encodeObject:_ssnNumber forKey:kUserModelSsnNumber];
    [aCoder encodeObject:_qstHealth forKey:kUserModelQstHealth];
    [aCoder encodeObject:_internalBaseClassIdentifier forKey:kUserModelId];
    [aCoder encodeObject:_lastLat forKey:kUserModelLastLat];
    [aCoder encodeObject:_userRate forKey:kUserModelUserRate];
    [aCoder encodeObject:_country forKey:kUserModelCountry];
    [aCoder encodeObject:_qstDisability forKey:kUserModelQstDisability];
    [aCoder encodeObject:_updatedBy forKey:kUserModelUpdatedBy];
    [aCoder encodeObject:_isDisabled forKey:kUserModelIsDisabled];
    [aCoder encodeObject:_useLocation forKey:kUserModelUseLocation];
    [aCoder encodeObject:_logSessionKey forKey:kUserModelLogSessionKey];
    [aCoder encodeObject:_approvedByAdmin forKey:kUserModelApprovedByAdmin];
    [aCoder encodeObject:_meritalStatus forKey:kUserModelMeritalStatus];
    [aCoder encodeObject:_availStatus forKey:kUserModelAvailStatus];
    [aCoder encodeObject:_qstCriminal forKey:kUserModelQstCriminal];
    [aCoder encodeObject:_idcatdState forKey:kUserModelIdcatdState];
    [aCoder encodeObject:_userProfiles forKey:kUserModelUserProfiles];
    [aCoder encodeObject:_email forKey:kUserModelEmail];
    [aCoder encodeObject:_idcardExpDate forKey:kUserModelIdcardExpDate];
    [aCoder encodeObject:_logUserArgent forKey:kUserModelLogUserArgent];
    [aCoder encodeObject:_insertedDate forKey:kUserModelInsertedDate];
    [aCoder encodeObject:_surname forKey:kUserModelSurname];
    [aCoder encodeObject:_lastLng forKey:kUserModelLastLng];
    [aCoder encodeObject:_idcardNumber forKey:kUserModelIdcardNumber];
    [aCoder encodeObject:_updatedDate forKey:kUserModelUpdatedDate];
    [aCoder encodeObject:_name forKey:kUserModelName];
    [aCoder encodeObject:_qstSexsual forKey:kUserModelQstSexsual];
    [aCoder encodeObject:_systemRow forKey:kUserModelSystemRow];
    [aCoder encodeObject:_city forKey:kUserModelCity];
    [aCoder encodeObject:_referalName forKey:kUserModelReferalName];
    [aCoder encodeObject:_selfImg forKey:kUserModelSelfImg];
    [aCoder encodeObject:_lastSeenDate forKey:kUserModelLastSeenDate];
    [aCoder encodeObject:_addressOffical forKey:kUserModelAddressOffical];
    [aCoder encodeObject:_idcardImg2 forKey:kUserModelIdcardImg2];
    [aCoder encodeObject:_gender forKey:kUserModelGender];
    [aCoder encodeObject:_isApproved forKey:kUserModelIsApproved];
    [aCoder encodeObject:_idcardImg1 forKey:kUserModelIdcardImg1];
    [aCoder encodeObject:_addressActual forKey:kUserModelAddressActual];
    [aCoder encodeObject:_paypalEmail forKey:kUserModelPaypalEmail];
    [aCoder encodeObject:_referalCell forKey:kUserModelReferalCell];
    [aCoder encodeObject:_logDate forKey:kUserModelLogDate];
    [aCoder encodeObject:_lastLocationDate forKey:kUserModelLastLocationDate];
    [aCoder encodeObject:_password forKey:kUserModelPassword];
    [aCoder encodeObject:_useOffical forKey:kUserModelUseOffical];
    [aCoder encodeObject:_logDeviceId forKey:kUserModelLogDeviceId];
    [aCoder encodeObject:_insertedBy forKey:kUserModelInsertedBy];
}

- (id)copyWithZone:(NSZone *)zone {
    UserModel *copy = [[UserModel alloc] init];
    
    
    
    if (copy) {

        copy.approveGuid = [self.approveGuid copyWithZone:zone];
        copy.cell = [self.cell copyWithZone:zone];
        copy.ssnNumber = [self.ssnNumber copyWithZone:zone];
        copy.qstHealth = [self.qstHealth copyWithZone:zone];
        copy.internalBaseClassIdentifier = [self.internalBaseClassIdentifier copyWithZone:zone];
        copy.lastLat = [self.lastLat copyWithZone:zone];
        copy.userRate = [self.userRate copyWithZone:zone];
        copy.country = [self.country copyWithZone:zone];
        copy.qstDisability = [self.qstDisability copyWithZone:zone];
        copy.updatedBy = [self.updatedBy copyWithZone:zone];
        copy.isDisabled = [self.isDisabled copyWithZone:zone];
        copy.useLocation = [self.useLocation copyWithZone:zone];
        copy.logSessionKey = [self.logSessionKey copyWithZone:zone];
        copy.approvedByAdmin = [self.approvedByAdmin copyWithZone:zone];
        copy.meritalStatus = [self.meritalStatus copyWithZone:zone];
        copy.availStatus = [self.availStatus copyWithZone:zone];
        copy.qstCriminal = [self.qstCriminal copyWithZone:zone];
        copy.idcatdState = [self.idcatdState copyWithZone:zone];
        copy.userProfiles = [self.userProfiles copyWithZone:zone];
        copy.email = [self.email copyWithZone:zone];
        copy.idcardExpDate = [self.idcardExpDate copyWithZone:zone];
        copy.logUserArgent = [self.logUserArgent copyWithZone:zone];
        copy.insertedDate = [self.insertedDate copyWithZone:zone];
        copy.surname = [self.surname copyWithZone:zone];
        copy.lastLng = [self.lastLng copyWithZone:zone];
        copy.idcardNumber = [self.idcardNumber copyWithZone:zone];
        copy.updatedDate = [self.updatedDate copyWithZone:zone];
        copy.name = [self.name copyWithZone:zone];
        copy.qstSexsual = [self.qstSexsual copyWithZone:zone];
        copy.systemRow = [self.systemRow copyWithZone:zone];
        copy.city = [self.city copyWithZone:zone];
        copy.referalName = [self.referalName copyWithZone:zone];
        copy.selfImg = [self.selfImg copyWithZone:zone];
        copy.lastSeenDate = [self.lastSeenDate copyWithZone:zone];
        copy.addressOffical = [self.addressOffical copyWithZone:zone];
        copy.idcardImg2 = [self.idcardImg2 copyWithZone:zone];
        copy.gender = [self.gender copyWithZone:zone];
        copy.isApproved = [self.isApproved copyWithZone:zone];
        copy.idcardImg1 = [self.idcardImg1 copyWithZone:zone];
        copy.addressActual = [self.addressActual copyWithZone:zone];
        copy.paypalEmail = [self.paypalEmail copyWithZone:zone];
        copy.referalCell = [self.referalCell copyWithZone:zone];
        copy.logDate = [self.logDate copyWithZone:zone];
        copy.lastLocationDate = [self.lastLocationDate copyWithZone:zone];
        copy.password = [self.password copyWithZone:zone];
        copy.useOffical = [self.useOffical copyWithZone:zone];
        copy.logDeviceId = [self.logDeviceId copyWithZone:zone];
        copy.insertedBy = [self.insertedBy copyWithZone:zone];
    }
    
    return copy;
}


@end
